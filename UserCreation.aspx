﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserCreation.aspx.cs" Inherits="UserCreation" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

            
     <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableCustomer').dataTable();
        });
    </script>

                                        <%-- <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate>--%>
 <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">User Creation</li></h4> 
                    </ol>
                </div>
 
 <div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
    <div class="col-md-9">
        <div class="panel panel-white">
			   <div class="panel panel-primary">
            <div class="panel-heading clearfix">
                <h3 class="panel-title">Employee Details</h3>
            </div>
            </div>
				
               
			<div class="panel-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
					   <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Department</a></li>
                        <li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Designation</a></li>
                        <li role="presentation"><a href="#tab3" role="tab" data-toggle="tab">Employee Type</a></li>
                        <li role="presentation"><a href="#tab4" role="tab" data-toggle="tab">Wages Type</a></li>
                    </ul>
      					 <div class="tab-content">
      					   <!-- tab1 start -->
      					    <div role="tabpanel" class="tab-pane active" id="tab1">
      					       <form class="form-horizontal">
						           <div class="col-md-1"></div>
						           <div class="col-md-12">
			                          <div class="panel panel-white">
				                        <form class="form-horizontal">
				                          <div class="panel-body">
					                           <div class="form-group row">
						
						<div class="col-md-1"></div>
				        <label for="input-Default" class="col-sm-2 control-label">Dapartment Name</label>
						<div class="col-sm-3">
                            <asp:TextBox ID="txtDepartmentName" class="form-control" runat="server" required></asp:TextBox>
                       </div>
                   </div>                     
					                          
              
					                           <!-- Button start -->
						                       <div class="form-group row">
						                       </div>
                                               <div class="txtcenter">
                               <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                    onclick="btnSave_Click" />
                               <asp:LinkButton ID="BtnClear" runat="server" class="btn btn-danger" onclick="BtnClear_Click">Clear</asp:LinkButton>   
                   
                                <%--<asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" />--%>
                         </div>
                                               <!-- Button End -->
					                           <div class="panel-body">
                             <div class="table-responsive">
    
                              <asp:Repeater ID="rptrCustomer" runat="server" onitemcommand="Repeater1_ItemCommand">
            <HeaderTemplate>
                
                  <table id="tableCustomer" class="display table" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                                                   <%-- <th>SNo</th>--%>
                                                    <th>Location Name</th>
                                                    <th>User Name</th>  
                                                    <th>User Type</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    
                   <%-- <td>
                        <%# DataBinder.Eval(Container.DataItem, "SNo")%>
                    </td>--%>
                    
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "LocCode")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "UserName")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "UserType")%>
                    </td>
                    <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("SNo") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("SNo") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
                        </div>
                       
						                  </div>
						               </form>
						             </div>
						           </div>
						       </form>
						   </div>
						                  
						                 
						   <!-- tab1 end -->
						   
						   <!-- tab2 start -->
                             <div role="tabpanel" class="tab-pane" id="tab2">
                        <form class="form-horizontal">
						<div class="col-md-1"></div>
						<div class="col-md-12">
			            <div class="panel panel-white">
				           <%-- <div class="panel-heading clearfix">
					            <h4 class="panel-title">Other</h4>
				            </div>--%>
				            <form class="form-horizontal">
				            <div class="panel-body">
				            
				            <div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Designation Name<span class="mandatory">*</span></label>
						<div class="col-sm-4">
                           <asp:TextBox ID="txtDesignationName" class="form-control" runat="server"></asp:TextBox>
                            
						</div>
						<div class="col-sm-1"></div>
						<div class="col-sm-3">
						<div class="txtcenter">
						<asp:Button ID="btnSaveDesgination" class="btn btn-success"  runat="server" 
                                Text="save"  />
						</div>
						</div>
						</div>
					           
            					<div class="form-group row">
                    <div class="col-sm-10">
                      <div class="table-responsive scroll-table">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Employee Type</th>
                                                    </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    
                                                </tr>
                                                
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>
						   </div>
						   
                        </div> 

            						
            	
						
				            </div><!-- panel body end -->
				            </form>
			            </div><!-- panel white end -->
		                </div><!-- col-12 end -->
		            <div class="col-md-1"></div>
						</form> 
						</div> 
						   <!-- tab2 end -->
						   
						    <!-- tab3 start -->
						     <div role="tabpanel" class="tab-pane" id="tab3">
                        <form class="form-horizontal">
						<div class="col-md-1"></div>
						<div class="col-md-12">
			            <div class="panel panel-white">
				           <%-- <div class="panel-heading clearfix">
					            <h4 class="panel-title">Other</h4>
				            </div>--%>
				            <form class="form-horizontal">
				            <div class="panel-body">
				            
            					 <div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Employee Type<span class="mandatory">*</span></label>
						<div class="col-sm-4">
                           <asp:TextBox ID="txtEmployeeType" class="form-control" runat="server"></asp:TextBox>
                            
						</div>
						<div class="col-sm-1"></div>
						<div class="col-sm-3">
						<div class="txtcenter">
						<asp:Button ID="btnSaveEmployeeType" class="btn btn-success"  runat="server" 
                                Text="save" />
						</div>
						</div>
						</div>
					           
					           <div class="form-group row">
                    <div class="col-sm-10">
                      <div class="table-responsive scroll-table">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Employee Type</th>
                                                    </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    
                                                </tr>
                                                
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>
						   </div>
						   
                        </div>
            					 

            						
            	
						
				            </div><!-- panel body end -->
				            </form>
			            </div><!-- panel white end -->
		                </div><!-- col-12 end -->
		            <div class="col-md-1"></div>
						</form> 
						</div>
						    <!-- tab3 end -->
						  
						  <!-- tab4 start -->
                              <div role="tabpanel" class="tab-pane" id="tab4">
                        <form class="form-horizontal">
						<div class="col-md-1"></div>
						<div class="col-md-12">
			            <div class="panel panel-white">
				           <%-- <div class="panel-heading clearfix">
					            <h4 class="panel-title">Other</h4>
				            </div>--%>
				            <form class="form-horizontal">
				            <div class="panel-body">
            					
					            <div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Wages Type<span class="mandatory">*</span></label>
						<div class="col-sm-4">
                           <asp:TextBox ID="txtWagesType" class="form-control" runat="server"></asp:TextBox>
                            
						</div>
						<div class="col-sm-1"></div>
						<div class="col-sm-3">
						<div class="txtcenter">
						<asp:Button ID="btnSaveWagesType" class="btn btn-success"  runat="server" 
                                Text="save"/>
						</div>
						</div>
						</div>
					           
            					<div class="form-group row">
                    <div class="col-sm-10">
                      <div class="table-responsive scroll-table">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Wages Type</th>
                                                    </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    
                                                </tr>
                                                
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>
						   </div>
						   
                        </div> 

            						
            	
						
				            </div><!-- panel body end -->
				            </form>
			            </div><!-- panel white end -->
		                </div><!-- col-12 end -->
		            <div class="col-md-1"></div>
						</form> 
						</div> <!-- tab4 end -->
						  
						 </div><!-- tab -content end -->
                </div><!-- tab -panel end -->
            </div><!-- panel body end -->
        </div> <!-- panel  end -->
        </div><!-- col 9 end -->
        
	    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
             <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
</div><!-- col 12 end -->
</div><!-- row end -->

</div><!-- main-wrapper end -->

</asp:Content>

