﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManualAttendance_Add.aspx.cs" Inherits="ManualAttendance_Add" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
    
             $(document).ready(function () {
            $('#tableEmployee').dataTable();
        });
        </script>
        
      <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate> 
                                          
 <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Manual Attendance</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
            <div class="col-md-9">
			<div class="panel panel-white">
			    <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Manual Attendance</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
		                 
          
              <asp:Button ID="btnNEWEMPentry" class="btn btn-success" runat="server" 
                  Text="ADD MANUAL ATTENDANCE" onclick="btnNEWEMPentry_Click" />
              </div>
              
              
              <div class="panel-body">
                             <div class="table-responsive">
    
                              <asp:Repeater ID="rptrCustomer" runat="server" onitemcommand="Repeater1_ItemCommand">
            <HeaderTemplate>
                
                  <table id="tableCustomer" class="display table" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                                                                             
                                                   <%-- <th>SNo</th>--%>
                                                    <th>EmpNo</th>
                                                    <th>EmpName</th>  
                                                    <th>AttnDate</th>
                                                    <%--<th>Edit</th>--%>
                                                    <th>Delete</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    
                   <%-- <td>
                        <%# DataBinder.Eval(Container.DataItem, "SNo")%>
                    </td>--%>
                    
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "EmpNo")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "EmpName")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "AttnDate")%>
                    </td>
                   <%-- <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("EmpNo") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>--%>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("EmpNo")+","+ Eval("AttnDate") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
                        </div>
               
             
              </form>
        	    <div class="col-md-2"></div>
		    
            
           </div>
 </div> <!-- col-9 end -->
 <!-- Dashboard start -->
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>        
              
              </div>
            
           </div>
              
              </div>
            
              
              </ContentTemplate>
 </asp:UpdatePanel>
</asp:Content>

