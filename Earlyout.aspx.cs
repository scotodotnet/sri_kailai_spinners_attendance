﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class Earlyout : System.Web.UI.Page
{


    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    BALDataAccess objdata = new BALDataAccess();
    //string SessionAdmin = "admin";
    //string SessionCcode = "ESM";
    //string SessionLcode = "UNIT I";
    string cc = "EVEREADY SPINING MILL";
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string SSQL;
    DataTable mEmployeeDS = new DataTable();
    DataTable AutoDataTable = new DataTable();
    DataTable Datacells = new DataTable();
    DataTable mDataTable = new DataTable();
    DataSet ds = new DataSet();
    DateTime date1 = new DateTime();
 
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();


    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-EarlyOut";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("DownloadClear"));
                //li.Attributes.Add("class", "droplink active open");
            }


            Datacells.Columns.Add("EmployeeNo");
            Datacells.Columns.Add("ExistingCode");
            Datacells.Columns.Add("Dept");
            Datacells.Columns.Add("Name");
            Datacells.Columns.Add("TimeOUT");




            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();

            //ModeType = Request.QueryString["ModeType"].ToString();
            //ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            //ddlShiftType = Request.QueryString["ddlShiftType"].ToString();
            date1 = Convert.ToDateTime(Date.ToString());
            DataTable dtIPaddress = new DataTable();
            dtIPaddress = objdata.IPAddress(SessionCcode.ToString(), SessionLcode.ToString());

            if (dtIPaddress.Rows.Count > 0)
            {
                for (int i = 0; i < dtIPaddress.Rows.Count; i++)
                {
                    if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                    {
                        mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                    else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                    {
                        mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                }
            }
            Fill_EarlyGoers_Details();

            for (int iRow2 = 0; iRow2 < AutoDataTable.Rows.Count; iRow2++)
            {
                string MID = AutoDataTable.Rows[iRow2][9].ToString();



                SSQL = "";
                SSQL = "select Distinct MachineID,MachineID_Encrypt,isnull(DeptName,'') as [DeptName]";
                SSQL = SSQL + ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
                SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
                SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName]";
                SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'And MachineID_Encrypt = '" + MID + "'";

                //if (mvarUserType == "IF User")
                //{
                //    SSQL = SSQL + " And PFNo <> '' And PFNo IS Not Null And PFNo <> '0' And PFNo <> 'o' And PFNo <> 'A'";
                //    SSQL = SSQL + " And PFNo <> 'NULL'";
                //}

                mEmployeeDS = objdata.ReturnMultipleValue(SSQL);


                if (mEmployeeDS.Rows.Count > 0)
                {
                    for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                    {

                        string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();

                        AutoDataTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                        Datacells.Rows[iRow2]["Dept"] = mEmployeeDS.Rows[iRow1]["DeptName"];
                        AutoDataTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
                        //Datacells.Rows[iRow2]["Type"] = mEmployeeDS.Rows[iRow1]["TypeName"];
                        AutoDataTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                        Datacells.Rows[iRow2]["EmployeeNo"] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                        AutoDataTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                        Datacells.Rows[iRow2]["ExistingCode"] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                        AutoDataTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                        Datacells.Rows[iRow2]["Name"] = mEmployeeDS.Rows[iRow1]["FirstName"];
                        AutoDataTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                        // Datacells.Rows[iRow2][7] = mEmployeeDS.Rows[iRow1]["CatName"];
                        AutoDataTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                        // Datacells.Rows[iRow2][8] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                        AutoDataTable.Rows[iRow2][12] = SessionCcode.ToString();
                        AutoDataTable.Rows[iRow2][13] = SessionLcode.ToString();
                        AutoDataTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");

                    }
                }
            }



            grid.DataSource = Datacells;
            grid.DataBind();
            string attachment = "attachment;filename=EARLYOUT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + cc + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">EARLY-OUT REPORT &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\"> Date -" + Date + "</a>");
            Response.Write("&nbsp;&nbsp;&nbsp;");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
    }


    public void Fill_EarlyGoers_Details()
    {

        DataColumn auto = new DataColumn();
        auto.AutoIncrement = true;
        auto.AutoIncrementSeed = 1;


        AutoDataTable.Columns.Add("SNo");
        AutoDataTable.Columns.Add("Dept");
        AutoDataTable.Columns.Add("Type");
        AutoDataTable.Columns.Add("Shift");
        AutoDataTable.Columns.Add("EmpCode");
        AutoDataTable.Columns.Add("ExCode");
        AutoDataTable.Columns.Add("Name");
        AutoDataTable.Columns.Add("TimeOUT");
        AutoDataTable.Columns.Add("");
        AutoDataTable.Columns.Add("MachineID");
        AutoDataTable.Columns.Add("Category");
        AutoDataTable.Columns.Add("SubCategory");
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("ShiftDate");

       
        date1 = Convert.ToDateTime(Date.ToString());

        SSQL = "";
        SSQL = "select ShiftDet,MachineID,TimeOUT from";
        SSQL += "(";
        SSQL += " Select 'Shift1' as [ShiftDet],MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT";
        SSQL += " Where Compcode='" + SessionCcode.ToString() + "'";
        SSQL += " And LocCode='" + SessionLcode.ToString() + "'";
        SSQL += " And IPAddress='" + mIpAddress_OUT + "'";
        SSQL += " And TimeOUT  >'" + date1.ToString("yyyy/MM/dd") + " 10:00'";
        SSQL += " And TimeOUT <='" + date1.ToString("yyyy/MM/dd") + " 16:30'";
        SSQL += " Group By MachineID";
        SSQL += " union";
        SSQL += " Select 'Shift2' as [ShiftDet],MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT";
        SSQL += " Where Compcode='" + SessionCcode.ToString() + "'";
        SSQL += " And LocCode='" + SessionLcode.ToString() + "'";
        SSQL += " And IPAddress='" + mIpAddress_IN + "'";
        SSQL += " And TimeOUT  >'" + date1.ToString("yyyy/MM/dd") + " 17:30'";
        SSQL += " And TimeOUT <='" + date1.ToString("yyyy/MM/dd") + " 00:00'";
        SSQL += " Group By MachineID";
        SSQL += " union";
        SSQL += " Select 'Shift3' as [ShiftDet],MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT";
        SSQL += " Where Compcode='" + SessionCcode.ToString() + "'";
        SSQL += " And LocCode='" + SessionLcode.ToString() + "'";
        SSQL += " And IPAddress='" + mIpAddress_OUT + "'";
        SSQL += " And TimeOUT >'" + date1.AddDays(1).ToString("yyyy/MM/dd") + " 01:00'";
        SSQL += " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " 08:00'";
        SSQL += " Group By MachineID) a ";
        SSQL += " Order By ShiftDet,TimeOUT";
        

        mDataTable = objdata.ReturnMultipleValue(SSQL);

        if (mDataTable.Rows.Count <= 0)
        {
            return;
        }


        //AutoDataTable.Rows = mDataTable.Rows.Count + 2;

        for (int iRow = 0; iRow < mDataTable.Rows.Count; iRow++)
        {
            //AutoDataTable.Rows[irow1][0] = iRow + 1;

            AutoDataTable.NewRow();
            AutoDataTable.Rows.Add();
            
            Datacells.NewRow();
            Datacells.Rows.Add();
           
            AutoDataTable.Rows[iRow]["Shift"] = mDataTable.Rows[iRow]["ShiftDet"].ToString();
           // Datacells.Rows[iRow]["Shift"] = mDataTable.Rows[iRow]["ShiftDet"].ToString();
            AutoDataTable.Rows[iRow]["TimeOUT"] = string.Format("{0:hh:mm tt}", mDataTable.Rows[iRow]["TimeOUT"]);
            Datacells.Rows[iRow]["TimeOUT"] = string.Format("{0:hh:mm tt}", mDataTable.Rows[iRow]["TimeOUT"]);
            AutoDataTable.Rows[iRow]["MachineID"] = mDataTable.Rows[iRow]["MachineID"];
        }
    }
}
