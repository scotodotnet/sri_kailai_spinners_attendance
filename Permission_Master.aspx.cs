﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using System.Data.SqlClient;


public partial class Permission_Master : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    string SSQL;
    bool ErrFlag = false;
    static string tt;
    string timing;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Permission Master";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");

                DropDown_Department();
                Financial_Year();
                DisplayPermission();
            }
        }
    }

    public void DropDown_Department()
    {
        DataTable dt = new DataTable();
        dt = objdata.Dropdown_Department();
        ddlDepartment.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["DeptCode"] = 0;
        dr["DeptName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptCode";
        ddlDepartment.DataBind();
    }


    public void Financial_Year()
    {
        String CurrentYear1;
        int CurrentYear;
        CurrentYear1 = DateTime.Now.Year.ToString();
        CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());
        if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
        {
            CurrentYear = CurrentYear - 1;
        }
        for (int i = 0; i < 1; i++)
        {
            tt = CurrentYear1;
            // tt = (CurrentYear1 + "-" + (CurrentYear - 1));
            txtFinancialYear.Text = tt.ToString();
            //txtFinancialYear.Items.Add(tt.ToString());
            CurrentYear = CurrentYear - 1;
            string cy = Convert.ToString(CurrentYear);
            CurrentYear1 = cy;
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {

        bool ErrFlag = false;

        if (rdbMorning.Checked == true)
        {
            timing = "A.M";
        }
        else if (rdbEvening.Checked == true)
        {
            timing = "P.M";
        }
        

        if(rdbMorning.Checked == false && rdbEvening.Checked == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Morning or Evening');", true);
            ErrFlag = true;
        }
        else if (ddlDepartment.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Department');", true);
            ErrFlag = true;
        }
        else if (ddlMonth.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Month');", true);
            ErrFlag = true;
        }
        else
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "update PermissionType_Mst set Dept='" + ddlDepartment.SelectedItem.Text + "',PermissionType='" + txtPermissionType.Text + "'";
                SSQL = SSQL + ",NoofCount='" + txtCountofMonth.Text + "',Month='" + ddlMonth.SelectedItem.Text + "',Morning_evening='" + timing + "'";
                SSQL = SSQL + ",Hours='" + txtdays.Text + "',FinancialYear='" + txtFinancialYear.Text + "',CompCode='" + SessionCcode + "'";
                SSQL = SSQL + ",LocCode='" + SessionLcode + "'";
                objdata.ReturnMultipleValue(SSQL);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Permission Master Updated Successfully');", true);
                ErrFlag = true;
                Clear();
                DisplayPermission();
            }
            else
            {
                DataTable dtd = new DataTable();
                string SSQL = "select* from PermissionType_Mst where Dept='" + ddlDepartment.SelectedItem.Text + "' and PermissionType='" + txtPermissionType.Text + "'";
                SSQL = SSQL + " and Month='" + ddlMonth.SelectedItem.Text + "' and FinancialYear='" + txtFinancialYear.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                dtd = objdata.ReturnMultipleValue(SSQL);
                if (dtd.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Permission Master Already Exist');", true);
                    ErrFlag = true;
                    Clear();
                    DisplayPermission();
                }
                else
                {
                    DataTable dtdLMst = new DataTable();
                    SSQL = "insert into PermissionType_Mst(Dept,PermissionType,NoofCount,Month,Morning_evening,Hours,FinancialYear,CompCode,LocCode)values('" + ddlDepartment.SelectedItem.Text + "','" + txtPermissionType.Text + "',";
                    SSQL = SSQL + "'" + txtCountofMonth.Text + "','" + ddlMonth.SelectedItem.Text + "','" + timing.ToString() + "','" + txtdays.Text + "','" + txtFinancialYear.Text + "','" + SessionCcode + "','" + SessionLcode + "')";
                    objdata.ReturnMultipleValue(SSQL);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Permission Master Save Successfully');", true);
                    ErrFlag = true;
                    Clear();
                    DisplayPermission();
                }
            }
        }
    }

    public void Clear()
    {
        ddlDepartment.SelectedIndex = 0;
        txtPermissionType.Text = "";
        txtdays.Text = "";
        rdbEvening.Checked = false;
        rdbMorning.Checked = false;
        txtPermissionType.Text = "";
        txtCountofMonth.Text = "";
        ddlMonth.SelectedIndex = 0;
        btnSave.Text = "Save";

     }

    public void DisplayPermission()
    {
        DataTable dtdDisplay = new DataTable();
        SSQL = "select* from PermissionType_Mst";
        dtdDisplay = objdata.ReturnMultipleValue(SSQL);
        if (dtdDisplay.Rows.Count > 0)
        {
            rptrCustomer.DataSource = dtdDisplay;
            rptrCustomer.DataBind();
        }

    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string Dept = commandArgs[0];
        string SNo = commandArgs[1];


        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(Dept, SNo);
                break;
            case ("Edit"):
                EditRepeaterData(Dept, SNo);
                break;
        }
    }

    private void EditRepeaterData(string Dept, string SNo)
    {
        DataTable Dtd = new DataTable();
        SSQL = "select*from PermissionType_Mst where Dept='" + Dept + "' and SNo='" + SNo + "'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        if (Dtd.Rows.Count > 0)
        {
            DataTable dt = new DataTable();
            string dept = Dtd.Rows[0]["Dept"].ToString();
            SSQL = "select DeptCode from Department_Mst where DeptName='" + dept + "'";
            dt = objdata.ReturnMultipleValue(SSQL);
            if (dt.Rows.Count > 0)
            {
                string deptt = dt.Rows[0]["DeptCode"].ToString();
                if (deptt.ToString() != "")
                {
                    ddlDepartment.SelectedValue = deptt;
                }
            }
        }

        txtPermissionType.Text = Dtd.Rows[0]["PermissionType"].ToString();
        txtdays.Text = Dtd.Rows[0]["Hours"].ToString();
        btnSave.Text = "Update";
        txtCountofMonth.Text = Dtd.Rows[0]["NoofCount"].ToString();
        ddlMonth.SelectedItem.Text = Dtd.Rows[0]["Month"].ToString();
        string time = Dtd.Rows[0]["Morning_evening"].ToString();
        string timing;
        if (time == "A.M")
        {
            rdbMorning.Checked = true;
        }
        else
        {
            rdbMorning.Checked = false;
        }
     }

    private void DeleteRepeaterData(string Dept, string SNo)
    {
        DataTable Dtd = new DataTable();
        SSQL = "Delete PermissionType_Mst where Dept='" + Dept + "' and SNo='" + SNo + "'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        DisplayPermission();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
        ErrFlag = true;
        Clear();
        DisplayPermission();
    }




    protected void BtnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }

    protected void rdbMorning_CheckedChanged(object sender, EventArgs e)
    {
        rdbEvening.Checked = false;
    }
    protected void rdbEvening_CheckedChanged(object sender, EventArgs e)
    {
        rdbMorning.Checked = false;
    }
}
