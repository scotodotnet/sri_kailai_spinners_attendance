﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using Org.BouncyCastle.Utilities;
using System.IO;

public partial class Download_Clear : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string[] Time_Minus_Value_Check;
    public zkemkeeper.CZKEMClass axCZKEM1 = new zkemkeeper.CZKEMClass();
    private int iMachineNumber = 1;
    private bool bIsConnected = false;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string LocCode;
    bool Errflag;
    bool Berrflag;
    bool Connect = false;
    bool Check_Download_Clear_Error = false;
    DataTable mLocalDS = new DataTable();

   protected void Page_Load(object sender, EventArgs e)
   {
       if (Session["Isadmin"] == null)
       {
           Response.Redirect("Default.aspx");
           Response.Write("Your session expired");
       }
       else
       {
           SessionCcode = Session["Ccode"].ToString();
           SessionLcode = Session["Lcode"].ToString();
           SessionAdmin = Session["Isadmin"].ToString();
           SessionCompanyName = Session["CompanyName"].ToString();
           SessionLocationName = Session["LocationName"].ToString();

           con = new SqlConnection(constr);
           
       }

       if (!IsPostBack)
       {
           Page.Title = "Spay Module | Download Clear";
           HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("DownloadClear"));
           li.Attributes.Add("class", "droplink active open");

           Dropdown_Company();
           Dropdown_Location();
           IPAddress(SessionCcode, SessionLcode);
           lblDwnCmpltd.Text = "";
       }

     }


   public void Dropdown_Company()
   {
       DataTable dt = new DataTable();
       dt = objdata.Dropdown_Company();
       for (int i = 0; i < dt.Rows.Count; i++)
       {
           TxtCompcode.Text = dt.Rows[i]["Cname"].ToString();
       }
   }
   //public void CompanyCode()
   //{
   //    DataTable dtc = new DataTable();
   //    dtc = objdata.CompanyCode();
   //    for (int i = 0; i <= dtc.Rows.Count - 1; i++)
   //    {
   //        TxtCompcode.Text = dtc.Rows[i]["Cname"].ToString();


   //    }
   //    LocationCode();
   //}


   public void Dropdown_Location()
   {
       DataTable dt = new DataTable();
       dt = objdata.dropdown_ParticularLocation(SessionCcode, SessionLcode);
       for (int i = 0; i < dt.Rows.Count; i++)
       {
           ddlLocationCode.Items.Add(dt.Rows[i]["LocCode"].ToString());
       }
   }
   //public void LocationCode()
   //{
   //    string CCode = TxtCompcode.Text;
   //    string[] cc = CCode.Split('-');
   //    CompCode = cc[0];
   //    DataTable dtc = new DataTable();
   //    dtc = objdata.LocationCode(CompCode);
   //    for (int i = 0; i <= dtc.Rows.Count - 1; i++)
   //    {
   //        ddlLocationCode.Items.Add(dtc.Rows[i]["LocName"].ToString());
   //    }


   //}
   public void IPAddress(string Ccode, string LCode)
   {
       ddlIPAddress.Items.Clear();
       string CCode = Ccode;
       string LocCode = LCode;
       DataTable dtc = new DataTable();
       dtc = objdata.IPAddressALL(SessionCcode, SessionLcode);

       for (int i = 0; i <= dtc.Rows.Count - 1; i++)
       {

           ddlIPAddress.Items.Add(dtc.Rows[i]["IPAddress"].ToString());
       }
   }




   protected void ddlLocationCode_SelectedIndexChanged(object sender, EventArgs e)
   {
       string CC_str = TxtCompcode.Text;
       string[] CC = CC_str.Split('-');
       string CCode = CC[0];
       string lcode = ddlLocationCode.SelectedItem.Text;
       string[] ss = lcode.Split('-');
       LocCode = ss[0];
       IPAddress(CCode, LocCode);
   }







   public void Bin()
   {
       if (ddlIPAddress.SelectedValue.Trim() == "")
       {
           // MessageBox.Show("IP and Port cannot be null", "Error");
           return;
       }
       int idwErrorCode = 0;


       int port = 4370;


       if (Errflag == true)
       {
           lblDwnCmpltd.Text = "DOWNLOAD PROCESSING.......";

           Errflag = true;
       }



       bIsConnected = axCZKEM1.Connect_Net(ddlIPAddress.Text, Convert.ToInt32(port));
       if (bIsConnected == true)
       {

           iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
           axCZKEM1.RegEvent(iMachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
       }
       else
       {
           axCZKEM1.GetLastError(ref idwErrorCode);
           // MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");

           //System.Threading.Thread.Sleep(3000);
           ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Unable to connect the device');", true);
           Connect = true;

           
       }
       //Cursor = Cursors.Default;
   }




   public void StoreDays(string MachineID, DateTime DateStr)
   {
       string[] iStr1;
       string[] iStr2;
       int intI;
       int intK;
       int intCol;
       string Fin_Year = "";
       string Months_Full_Str = "";
       string Date_Col_Str = "";
       int Month_Int = 1;
       string Spin_Machine_ID_Str = "";
       //DataSet mLocalDS1 = new DataSet();


       DataTable mLocalDS1 = new DataTable();

       //Attn_Flex Col Add Var
       string[] Att_Date_Check;
       string Att_Already_Date_Check;
       string Att_Year_Check = "";
       int Month_Name_Change;
       string halfPresent = "0";
       int EPay_Total_Days_Get = 0;
       intCol = 4;
       Month_Name_Change = 1;
       int dayCount = 1;
       int daysAdded = 1;

       EPay_Total_Days_Get = 1;

       intI = 2;
       intK = 1;
       string Lcode = ddlLocationCode.SelectedItem.Text;
       string Ccode = TxtCompcode.Text;

       iStr1 = Lcode.Split('-');
       iStr2 = Ccode.Split('-');

       //get Employee Details
       //DataSet Emp_DS = new DataSet();
       string SSQL = "";
       bool ErrFlag = false;

       SSQL = "Select * from Employee_Mst where  MachineID_Encrypt='" + MachineID + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' ";
       // " Compcode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "' ";
       SqlCommand cmd = new SqlCommand(SSQL, con);
       SqlDataAdapter sda = new SqlDataAdapter(cmd);
       DataSet Emp_DS = new DataSet();
       DataTable dt_1 = new DataTable();
       con.Open();
       sda.Fill(Emp_DS);

       sda.Fill(dt_1);
       con.Close();

       if (Emp_DS.Tables[0].Rows.Count > 0)
       {
           ErrFlag = false;
       }
       else
       {
           ErrFlag = false;
           //ErrFlag = true;
       }
       if (!ErrFlag)
       {
           for (int intRow = 0; intRow < dt_1.Rows.Count; intRow++)
           {

               intK = 1;
               string Emp_Total_Work_Time_1 = "00:00";
               //Get Employee Week OF DAY
               DataTable DS_WH = new DataTable();
               string Emp_WH_Day;
               string DOJ_Date_Str;
               string MachineID1 = dt_1.Rows[intRow]["MachineID"].ToString();

               string Query = " Select * from Employee_Mst where MachineID='" + MachineID1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' ";
               //SSQL = "Select * from Employee_MST where MachineID='" + MachineID1 + "' And Compcode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'" +
               DS_WH = objdata.ReturnMultipleValue(Query);
               if (DS_WH.Rows.Count != 0)
               {
                   Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                   DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();
               }
               else
               {
                   Emp_WH_Day = "";
                   DOJ_Date_Str = "";
               }
               int colIndex = intK;
               bool isPresent = false;
               decimal Present_Count = 0;
               decimal Present_WH_Count = 0;
               int Appsent_Count = 0;
               decimal Final_Count = 0;
               decimal Total_Days_Count = 0;
               string Already_Date_Check;
               int Days_Insert_RowVal = 0;
               decimal FullNight_Shift_Count = 0;
               int NFH_Days_Count = 0;
               decimal NFH_Days_Present_Count = 0;
               Already_Date_Check = "";
               string Year_Check = "";
               string Time_IN_Str = "";
               string Time_Out_Str = "";
               int g = 1;
               for (intCol = 0; (intCol <= (daysAdded - 1)); intCol++)
               {
                   isPresent = false;
                   //string Emp_Total_Work_Time_1 = "00:00";
                   string Machine_ID_Str = "";
                   string OT_Week_OFF_Machine_No;
                   string Date_Value_Str = "";
                   string Total_Time_get = "";
                   string Final_OT_Work_Time_1 = "";
                   //DataSet mLocalDS = new DataSet();

                   //DataTable mLocalDS = new DataTable();
                   

                   int j = 0;
                   double time_Check_dbl = 0;
                   string Date_Value_Str1 = "";
                   string Employee_Shift_Name_DB = "No Shift";
                   isPresent = false;


                   //Shift Change Check Variable Declaration Start
                   DateTime InTime_Check = new DateTime();
                   DateTime InToTime_Check = new DateTime();
                   TimeSpan InTime_TimeSpan;
                   string From_Time_Str = "";
                   string To_Time_Str = "";
                   //DataSet DS_InTime = new DataSet();
                   DataTable DS_Time = new DataTable();
                   //DataSet DS_Time = new DataSet();
                   DataTable DS_InTime = new DataTable();
                    DateTime EmpdateIN_Change = new DateTime();

                   string Final_InTime = "";
                   string Final_OutTime = "";
                   string Final_Shift = "";

                   int K = 0;
                   bool Shift_Check_blb = false;
                   string Shift_Start_Time_Change;
                   string Shift_End_Time_Change;
                   string Employee_Time_Change = "";
                   DateTime ShiftdateStartIN_Change = new DateTime();
                   DateTime ShiftdateEndIN_Change = new DateTime();

                   //Shift Change Check Variable Declaration End
                   string Employee_Punch = "";
                   Spin_Machine_ID_Str = Emp_DS.Tables[0].Rows[intRow]["MachineID"].ToString();

                   if (Spin_Machine_ID_Str == "103")
                   {
                       Spin_Machine_ID_Str = "103";
                   }

                   // Machine_ID_Str = Encryption(Emp_DS.Tables[0].Rows[intRow]["MachineID"]);
                   Machine_ID_Str = Emp_DS.Tables[0].Rows[intRow]["MachineID_Encrypt"].ToString();
                   OT_Week_OFF_Machine_No = Emp_DS.Tables[0].Rows[intRow]["MachineID"].ToString();
                   Date_Value_Str = Convert.ToDateTime(DateStr).AddDays(0).ToShortDateString();
                   Date_Value_Str1 = Convert.ToDateTime(Date_Value_Str).AddDays(1).ToShortDateString();

                   //TimeIN Get

                   SSQL = "";
                   SSQL = "Select distinct TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                       //" And Compcode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "' "+
                         " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";

                   mLocalDS = objdata.ReturnMultipleValue(SSQL);

                   //SqlCommand cmd1 = new SqlCommand(SSQL, con);
                   //SqlDataAdapter sda1 = new SqlDataAdapter(cmd1);
                   //DataTable dt_11 = new DataTable();
                   //con.Open();
                   //sda1.Fill(mLocalDS);
                   //sda1.Fill(dt_11);
                   //con.Close();
                   if (mLocalDS.Rows.Count <= 0)
                   {
                       Time_IN_Str = "";

                   }
                   else
                   {
                       //Time_IN_Str = mLocalDS.Tables(0).Rows(0)(0)
                   }

                   //TimeOUT Get
                   SSQL = "";
                   SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                       // " And Compcode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'" +
                          " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                   mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                  // mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                   //SqlCommand cmd11 = new SqlCommand(SSQL, con);
                   // SqlDataAdapter sda11 = new SqlDataAdapter(cmd11);
                   // con.Open();
                   // sda1.Fill(mLocalDS1);
                   //sda1.Fill(dt_1);
                   //con.Close();
                   if (mLocalDS1.Rows.Count <= 0)
                   {
                       Time_Out_Str = "";
                   }
                   else
                   {
                       //Time_Out_Str = mLocalDS.Tables(0).Rows(0)(0)
                   }

                   //Shift Change Check Start
                   if (mLocalDS.Rows.Count != 0)
                   {
                       InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
                       InToTime_Check = InTime_Check.AddHours(2);


                       InTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InTime_Check), "HH:mm:ss"));
                       InToTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InToTime_Check), "HH:mm:ss"));

                       string TimeIN = InTime_Check.ToString();
                       string[] TimeIN_Split = TimeIN.Split(' ');
                       string TimeIN1 = TimeIN_Split[1].ToString();
                       string[] final = TimeIN1.Split(':');
                       string Final_IN = final[0] + ":" + final[1];


                       string TimeOUT = InToTime_Check.ToString();
                       string[] TimeOUT_Split = TimeOUT.Split(' ');
                       string TimeOUT1 = TimeOUT_Split[1].ToString();
                       string[] final1 = TimeOUT1.Split(':');
                       string Final_IN1 = final1[0] + ":" + final1[1];


                       //InTime_TimeSpan = TimeSpan.Parse(InTime_Check).ToString();
                       //From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;

                       //Two Hours OutTime Check
                       SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                           //" And Compcode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'" +
                               " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";


                       DS_Time = objdata.ReturnMultipleValue(SSQL);
                       if (DS_Time.Rows.Count != 0)
                       {
                           SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                               //" And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"+
                        " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                           DS_InTime = objdata.ReturnMultipleValue(SSQL);
                           if (DS_Time.Rows.Count != 0)
                           {
                               //DataSet Shift_DS_Change = new DataSet();

                               DataTable Shift_DS_Change = new DataTable();

                               Final_InTime = DS_InTime.Rows[0]["TimeIN"].ToString();
                               //Check With IN Time Shift
                               SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%'";
                               Shift_DS_Change = objdata.ReturnMultipleValue(SSQL);
                               Shift_Check_blb = false;
                               for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
                               {
                                   Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
                                   if (Shift_DS_Change.Rows[k]["EndIN_Days"].ToString() == "1")
                                   {
                                       Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                   }
                                   else
                                   {
                                       Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                   }

                                   ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                   ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);

                                   if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                   {
                                       Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                       Shift_Check_blb = true;
                                       break;
                                   }
                                   ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                   ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                   EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                   if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                   {
                                       Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                       Shift_Check_blb = true;
                                       break;
                                   }
                               }
                               if (Shift_Check_blb == true)
                               {

                                   //IN Time Query Update
                                   string Querys = "";
                                   Querys = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                       //SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                                  " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                   mLocalDS = objdata.ReturnMultipleValue(SSQL);
                                   if (Final_Shift == "SHIFT2")
                                   {
                                       Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                           //" And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                                              " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "03:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                                   }
                                   else
                                   {
                                       Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                           //" And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                                              " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "17:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";

                                   }
                                   mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                               }
                               else
                               {
                                   //Get Employee In Time
                                   SSQL = "";
                                   SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                       //" And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                                           " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                                   mLocalDS = objdata.ReturnMultipleValue(SSQL);
                                   //TimeOUT Get
                                   SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                       //SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                                         " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                                   mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                               }
                           }
                           else
                           {
                               //Get Employee In Time
                               SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                   //" And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                                      " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                               mLocalDS = objdata.ReturnMultipleValue(SSQL);

                               //TimeOUT Get
                               SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                   //SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                                     " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                               mLocalDS = objdata.ReturnMultipleValue(SSQL);

                           }
                       }
                   }
                   //Shift Change Check End 
                   if (mLocalDS.Rows.Count == 0)
                   {
                       Employee_Punch = "";
                   }
                   else
                   {
                       Employee_Punch = mLocalDS.Rows[0][0].ToString();
                   }
                   if (mLocalDS.Rows.Count >= 1)
                   {
                       for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                       {
                           Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                           if (mLocalDS1.Rows.Count > tin)
                           {
                               Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                           }
                           else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                           {

                           }
                           else
                           {
                               Time_Out_Str = "";
                           }
                           TimeSpan ts4 = new TimeSpan();
                           ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                           if (mLocalDS.Rows.Count <= 0)
                           {
                               Time_IN_Str = "";
                           }
                           else
                           {
                               Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                           }
                           if (Time_IN_Str == "" || Time_Out_Str == "")
                           {
                               time_Check_dbl = time_Check_dbl;
                           }
                           else
                           {

                               DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                               DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                               TimeSpan ts = new TimeSpan();
                               ts = date2.Subtract(date1);


                               //& ":" & Trim(ts.Minutes)
                               Total_Time_get = (ts.Hours).ToString();
                               ts4 = ts4.Add(ts);

                               //OT Time Get
                               Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                               Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);

                               if (Left_Val(ts4.Minutes.ToString(), 1) == "-" || Left_Val(ts4.Hours.ToString(), 1) == "-" || Emp_Total_Work_Time_1 == "0:-1")
                               {
                                   Emp_Total_Work_Time_1 = "00:00";

                               }
                               if (Left_Val(Total_Time_get, 1) == "-")
                               {

                                   date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                   ts = date2.Subtract(date1);
                                   ts = date2.Subtract(date1);
                                   Total_Time_get = (ts.Hours).ToString();
                                   time_Check_dbl = double.Parse(Total_Time_get);
                                   Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes).ToString();
                                   Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                   if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                   {
                                       Emp_Total_Work_Time_1 = "00:00";
                                   }
                                   if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:1" || Emp_Total_Work_Time_1 == "0:-3")
                                   {
                                       Emp_Total_Work_Time_1 = "00:00";
                                   }

                               }
                               else
                               {
                                   time_Check_dbl = time_Check_dbl + double.Parse(Total_Time_get);
                               }
                           }

                       }//For End
                   }
                   else
                   {
                       TimeSpan ts4 = new TimeSpan();

                       //ts4 = Convert.ToDateTime(String.Format("(0:hh:mm)", Emp_Total_Work_Time_1)).TimeOfDay;
                       if (mLocalDS.Rows.Count <= 0)
                       {
                           Time_IN_Str = "";
                       }
                       else
                       {
                           Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                       }
                       for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
                       {
                           if (mLocalDS1.Rows.Count <= 0)
                           {
                               Time_Out_Str = "";
                           }
                           else
                           {
                               Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                           }

                       }
                       //Emp_Total_Work_Time
                       if (Time_IN_Str == "" || Time_Out_Str == "")
                       {
                           time_Check_dbl = 0;
                       }
                       else
                       {
                           DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                           DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                           TimeSpan ts = new TimeSpan();
                           ts = date2.Subtract(date1);
                           ts = date2.Subtract(date1);
                           Total_Time_get = (ts.Hours).ToString();
                           //OT Time Get
                           Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                           Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                           if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                           {
                               Emp_Total_Work_Time_1 = "00:00";
                           }
                           if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                           {
                               Emp_Total_Work_Time_1 = "00:00";
                           }
                           if (Left_Val(Total_Time_get, 1) == "-")
                           {

                               date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                               ts = date2.Subtract(date1);
                               ts = date2.Subtract(date1);
                               Total_Time_get = ts.Hours.ToString();
                               time_Check_dbl = double.Parse(Total_Time_get);
                               Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes);
                               Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                               if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                               {
                                   Emp_Total_Work_Time_1 = "00:00";
                               }
                               if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                               {
                                   Emp_Total_Work_Time_1 = "00:00";
                               }
                           }
                           else
                           {
                               time_Check_dbl = double.Parse(Total_Time_get);
                           }
                       }
                   }
                   //Emp_Total_Work_Time

                   string Emp_Total_Work_Time = "";
                   string Final_OT_Work_Time = "00:00";
                   string Final_OT_Work_Time_Val = "00:00";
                   Emp_Total_Work_Time = Emp_Total_Work_Time_1;
                   //Find Week OFF
                   string Employee_Week_Name = "";
                   string Assign_Week_Name = "";
                   mLocalDS = null;
                   Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();

                   SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "' and  Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "'";
                   mLocalDS = objdata.ReturnMultipleValue(SSQL);
                   if (mLocalDS.Rows.Count <= 0)
                   {
                       Assign_Week_Name = "";
                   }
                   else
                   {
                       //Assign_Week_Name = mLocalDS.Tables(0).Rows(0)(0)
                       Assign_Week_Name = "";
                   }



                   if (Employee_Week_Name == Assign_Week_Name)
                   {

                   }
                   else
                   {
                       //Get Employee Work Time
                       double Calculate_Attd_Work_Time = 7;
                       double Calculate_Attd_Work_Time_half = 0;


                       Calculate_Attd_Work_Time_half = 4.0;

                       halfPresent = "0";//Half Days Calculate Start
                       if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                       {
                           isPresent = true;
                           halfPresent = "1";
                       }
                       //Half Days Calculate End
                       if (time_Check_dbl >= Calculate_Attd_Work_Time)
                       {
                           isPresent = true;
                           halfPresent = "0";
                       }
                   }
                   //Shift Check Code Start
                   //DataSet Shift_Ds = new DataSet();

                   DataTable Shift_Ds = new DataTable();
                   
                   string Start_IN = "";
                   string End_In = "";
                   string Shift_Name_Store = "";
                   bool Shift_Check_blb_Check = false;
                   DateTime ShiftdateStartIN_Check = new DateTime();
                   DateTime ShiftdateEndIN_Check = new DateTime();
                   DateTime EmpdateIN_Check = new DateTime();
                   if (isPresent == true)
                   {
                       Shift_Check_blb_Check = false;


                       //Shift Master Check Code Start
                       SSQL = "Select * from Shift_Mst where  ShiftDesc <> 'GENERAL' Order by ShiftDesc Asc";
                       //  "CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'";
                       Shift_Ds = objdata.ReturnMultipleValue(SSQL);
                       if (Shift_Ds.Rows.Count != 0)
                       {
                           SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%' ";
                           //CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "' And 
                           Shift_Ds = objdata.ReturnMultipleValue(SSQL);
                           Shift_Check_blb = false;
                           for (int f = 0; f < Shift_Ds.Rows.Count; f++)
                           {

                               int StartIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["StartIN_Days"]);
                               int EndIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["EndIN_Days"]);
                               Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(StartIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["StartIN"].ToString();
                               End_In = Convert.ToDateTime(Date_Value_Str).AddDays(EndIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["EndIN"].ToString();
                               ShiftdateStartIN_Check = System.Convert.ToDateTime(Start_IN);
                               ShiftdateEndIN_Check = System.Convert.ToDateTime(End_In);
                               EmpdateIN_Check = System.Convert.ToDateTime(Employee_Punch);
                               if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                               {
                                   Employee_Shift_Name_DB = Shift_Ds.Rows[K]["ShiftDesc"].ToString();
                                   Shift_Check_blb_Check = true;
                               }

                          }
                           if (Shift_Check_blb_Check == false)
                           {
                               Employee_Shift_Name_DB = "No Shift";
                           }
                       }
                       else
                       {
                           Employee_Shift_Name_DB = "No Shift";
                       }
                       //Shift Master Check Code End  

                   }
                   else
                   {
                       Employee_Shift_Name_DB = "No Shift";
                   }
                   //Shift Check Code End
                   if (isPresent == true)
                   {
                       if (halfPresent == "1")
                       {
                           Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
                       }
                       else if (halfPresent == "0")
                       {
                           Present_Count = Present_Count + 1;
                       }

                   }
                   else
                   {
                       Appsent_Count = Appsent_Count + 1;
                   }
                   intK += 1;
                   //Update Employee Worked_Days
                   //DataSet mDataSet = new DataSet();
                   DataTable mDataSet = new DataTable();
                   string Queryss = "";
                   DataSet Del_ds = new DataSet();
                   Queryss = "Select * from LogTime_Days where MachineID='" + UTF8Decryption(MachineID) + "' and  Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";

                   mDataSet = objdata.ReturnMultipleValue(Queryss);

                   //mDataSet = objdata.read(Queryss);
                   if (mDataSet.Rows.Count != 0)
                   {
                       Queryss = "Delete from LogTime_Days where MachineID='" + UTF8Decryption(MachineID) + "' and Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";

                       SqlCommand cmd_del = new SqlCommand(Queryss, con);
                       con.Open();
                       cmd_del.ExecuteNonQuery();
                       con.Close();
                   }
                   DataSet Insert_Ds = new DataSet();

                   Queryss = "";
                   Queryss = "Insert Into LogTime_Days(CompCode,LocCode,MachineID,ExistingCode,FirstName,DeptName," +
                   "Designation,DOJ,Present,Shift,Total_Hrs,Attn_Date_Str,Attn_Date,MachineID_Encrypt)" +
                    " Values('" + SessionCcode + "','" + SessionLcode + "','" + Emp_DS.Tables[0].Rows[intRow]["ExistingCode"] + "','" + Emp_DS.Tables[0].Rows[intRow]["ExistingCode"] + "'," +
                   "'" + Emp_DS.Tables[0].Rows[intRow]["FirstName"] + "'," +
                    "'" + Emp_DS.Tables[0].Rows[intRow]["DeptName"] + "','" + Emp_DS.Tables[0].Rows[intRow]["Designation"] + "'," +
                    "'" + Emp_DS.Tables[0].Rows[intRow]["DOJ"] + "','" + Present_Count + "'," +
                    "'" + Employee_Shift_Name_DB + "','" + time_Check_dbl + "','" + DateStr.ToString("yyyy/MM/dd") + "','" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "','" + MachineID + "')";
                   SqlCommand Cmdd = new SqlCommand(Queryss, con);
                   SqlDataAdapter sdaa = new SqlDataAdapter(Cmdd);
                   con.Open();
                   sdaa.Fill(Insert_Ds);
                   //sdaa.Fill(dt_1);
                   con.Close();
                   Present_Count = 0;
                   Present_WH_Count = 0;
                   Employee_Shift_Name_DB = "No Shift";

               }

               intI += 1; ;
               Total_Days_Count = 0;
               Final_Count = 0;
               Appsent_Count = 0;
               Present_Count = 0;
               Present_WH_Count = 0;
               NFH_Days_Count = 0;
               NFH_Days_Present_Count = 0;

           }
       }
   }


   private double IIf(bool p, object p_2, int p_3)
   {
       throw new NotImplementedException();
   }

   public string Left_Val(string Value, int Length)
   {

       if (Value.Length >= Length)
       {
           return Value.Substring(0, Length);
       }
       else
       {
           return Value;
       }
   }

   public string Right_Val(string Value, int Length)
   {

       int i = 0;
       i = 0;
       if (Value.Length >= Length)
       {
           //i = Value.Length - Length
           return Value.Substring(Value.Length - Length, Length);
       }
       else
       {
           return Value;
       }
   }

   private string Strings(int p)
   {
       throw new NotImplementedException();
   }

   protected void btndownload_Click(object sender, EventArgs e)
   {
       try
       {
           Errflag = true;
           //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ProgressBarShow();", true);
           if (bIsConnected == false)
           {
               //MessageBox.Show("Please connect the device first", "Error");
               //return
           }
           string sdwEnrollNumber = "";
           int idwVerifyMode = 0;
           int idwInOutMode = 0;
           int idwYear = 0;
           int idwMonth = 0;
           int idwDay = 0;
           int idwHour = 0;
           int idwMinute = 0;
           int idwSecond = 0;
           int idwWorkcode = 0;
           int idwErrorCode = 0;
           //bool download_completed;
           Bin();
           axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
           if (axCZKEM1.ReadGeneralLogData(iMachineNumber))//read all the attendance records to the memory
           {
               while (axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, out sdwEnrollNumber, out idwVerifyMode,
                          out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
               {
                   if (sdwEnrollNumber == "Mjc0Mg==")
                   {
                       sdwEnrollNumber = "Mjc0Mg==";
                   }
                   string Mode = "";
                   lblDwnCmpltd.Text = "Download Processing....";
                   DataTable IP_Da = new DataTable();
                   string IP_check = "select IPMode from IPAddress_Mst where IpAddress='" + ddlIPAddress.Text + "' and  CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";

                   IP_Da = objdata.ReturnMultipleValue(IP_check);
                   if (IP_Da.Rows.Count == 0)
                   {
                   }
                   else
                   {
                       Mode = IP_Da.Rows[0]["IPMode"].ToString();
                   }
                   DateTime date = new DateTime();
                   date = Convert.ToDateTime(idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString() + ":" + idwSecond.ToString());
                   string Query = "";
                   string Chck_Date = idwYear.ToString() + "/" + idwMonth.ToString() + "/" + idwDay.ToString();

                   //Encry Code
                   string strmsg = string.Empty;
                   byte[] encode = new byte[sdwEnrollNumber.Length];
                   encode = Encoding.UTF8.GetBytes(sdwEnrollNumber);
                   sdwEnrollNumber = Convert.ToBase64String(encode);

                   if (Mode == "IN")
                   {
                       Query = "delete from LogTime_IN where MachineID='" + sdwEnrollNumber + "' and TimeIN='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                       SqlCommand cmd_del = new SqlCommand(Query, con);
                       con.Open();
                       cmd_del.ExecuteNonQuery();
                       con.Close();

                       Query = "insert into LogTime_IN(CompCode,LocCode,IPAddress,MachineID,TimeIN)values('" + SessionCcode + "','" + SessionLcode + "','" + ddlIPAddress.Text + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                       SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                       con.Open();
                       cmd_upd1.ExecuteNonQuery();
                       con.Close();
                   }

                   if (Mode == "OUT")
                   {
                       Query = "delete from LogTime_OUT where MachineID='" + sdwEnrollNumber + "' and TimeOUT='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                       SqlCommand cmd_del = new SqlCommand(Query, con);
                       con.Open();
                       cmd_del.ExecuteNonQuery();
                       con.Close();

                       Query = "insert into LogTime_OUT(CompCode,LocCode,IPAddress,MachineID,TimeOUT)values('" + SessionCcode + "','" + SessionLcode + "','" + ddlIPAddress.Text + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                       SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                       con.Open();
                       cmd_upd1.ExecuteNonQuery();
                       con.Close();
                   }

                   if (Mode == "LUNCH")
                   {


                       Query = "delete from LogTime_Lunch where MachineID='" + sdwEnrollNumber + "' and TimeIN='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                       SqlCommand cmd_del = new SqlCommand(Query, con);
                       con.Open();
                       cmd_del.ExecuteNonQuery();
                       con.Close();

                       Query = "insert into LogTime_Lunch(CompCode,LocCode,IPAddress,MachineID,TimeIN)values('" + SessionCcode + "','" + SessionLcode + "','" + ddlIPAddress.Text + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                       SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                       con.Open();
                       cmd_upd1.ExecuteNonQuery();
                       con.Close();
                   }


                   //DataTable Chck_Ds = new DataTable();
                   //Query = "";
                   //Query = "select * from LogTime_Days where Attn_Date_Str='" + Chck_Date + "' and MachineID='" + UTF8Decryption(sdwEnrollNumber) + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                   //Chck_Ds = objdata.ReturnMultipleValue(Query);
                   //if (Chck_Ds.Rows.Count == 0)
                   //{
                   //    StoreDays(sdwEnrollNumber, date);
                   //}
                   //else
                   //{
                   //    StoreDays(sdwEnrollNumber, date);
                   //}
                   //System.Threading.Thread.Sleep(3000);
               }
           }
           else
           {
               Check_Download_Clear_Error = true;
               // Cursor = Cursors.Default;
               axCZKEM1.GetLastError(ref idwErrorCode);
               if (idwErrorCode != 0)
               {
                   //MessageBox.Show("Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString(), "Error");
               }
               else
               {
                   //MessageBox.Show("No data from terminal returns!", "Error");
               }
           }
           axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
           if (Errflag == true && Connect == false)
           {

               lblDwnCmpltd.Text = "DOWNLOAD COMPLETED....";
               ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
               ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('DOWNLOAD COMPLETED....');", true);
               Errflag = true;
           }
           else
           {
               Check_Download_Clear_Error = true;
               lblDwnCmpltd.Text = "Machine Can't Ping";
               ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
               //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Machine Can't Ping....');", true);
               Errflag = true;
           }

       }

       catch (Exception ex)
       {
           Check_Download_Clear_Error = true;
           throw new Exception("Error: " + ex.Message);
       }
   }

   private static string UTF8Encryption(string password)
   {
       string strmsg = string.Empty;
       byte[] encode = new byte[password.Length];
       encode = Encoding.UTF8.GetBytes(password);
       strmsg = Convert.ToBase64String(encode);
       return strmsg;
   }

   private static string UTF8Decryption(string encryptpwd)
   {
       string decryptpwd = string.Empty;
       UTF8Encoding encodepwd = new UTF8Encoding();
       Decoder Decode = encodepwd.GetDecoder();
       byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
       int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
       char[] decoded_char = new char[charCount];
       Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
       decryptpwd = new String(decoded_char);
       return decryptpwd;
   }
   protected void Button12_Click(object sender, EventArgs e)
   {
       if (TxtCompcode.Text == "")
       {
           ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Company Name');", true);
           Errflag = true;
       }
       if (ddlLocationCode.SelectedItem.Text == "")
       {
           ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Location Name');", true);
           Errflag = true;
       }
       if (ddlIPAddress.SelectedItem.Text == "")
       {
           ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give IP Address');", true);
           Errflag = true;
       }
       Check_Download_Clear_Error = false;
       btndownload_Click(sender, e);

       if (Check_Download_Clear_Error == false)
       {

           TFT_Machine_Attn_Log_Clear();
       }
   }
   public void TFT_Machine_Attn_Log_Clear()
   {
       bool bConn = false;
       int idwErrorCode = 0;
       int iMachineNumber = 0;
       int port = 4370;
       //the serial number of the device.After connecting the device ,this value will be changed.
       bConn = axCZKEM1.Connect_Net(ddlIPAddress.Text, Convert.ToInt32(port));

       //mdiMain.AxSB100PC1.SetIPAddress(Trim(iStr3(0)), CLng("5005"), CLng("0"))
       if (bConn == false)
       {
           //MessageBox.Show("Not connected.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
           return;
       }


       iMachineNumber = 1;
       axCZKEM1.EnableDevice(iMachineNumber, false);

       //disable the device
       if (axCZKEM1.ClearGLog(iMachineNumber) == true)
       {
           axCZKEM1.RefreshData(iMachineNumber);
           //the data in the device should be refreshed
           //MsgBox("All att Logs have been cleared from teiminal!", MsgBoxStyle.Information, "Success")
       }
       else
       {
           axCZKEM1.GetLastError(ref idwErrorCode);
           //MsgBox("Operation failed,ErrorCode=" & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
       }

       //enable the device
       axCZKEM1.EnableDevice(iMachineNumber, true);
   }
}
