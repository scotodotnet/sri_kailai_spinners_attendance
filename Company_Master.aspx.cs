﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;



public partial class CompanyMaster : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    CompanyMasterClass Comobj = new CompanyMasterClass();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Ccode;
    string Lcode;


    static int ss;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Page.Title = "Spay Module | Company Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
            //li.Attributes.Add("class", "droplink active open");
        }


    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable dtd = new DataTable();
        bool ErrFlag = false;

        dtd = objdata.CheckCompanyMst(txtCompanyCode.Text, txtCompanyName.Text);
        if (dtd.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Company Already Exist');", true);
            ErrFlag = true;
        }
        dtd = objdata.CheckLocationMst(txtCompanyCode.Text, txtLocationCode.Text);
        if (dtd.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Location Already Exist');", true);
            ErrFlag = true;
        }
        else
        {

            Comobj.Lcode = txtLocationCode.Text;
            Comobj.LocationName = txtLocationName.Text;
            Comobj.Ccode = txtCompanyCode.Text;
            Comobj.CompanyName = txtCompanyName.Text;
            Comobj.AddressOne = txtAddressone.Text;
            Comobj.AddressTwo = txtAddresstwo.Text;
            Comobj.PinCode = txtPincode.Text;
            Comobj.State = txtState.Text;
            Comobj.MobileNumber = txtPhoneNo.Text;
            Comobj.EMail = txtEmailID.Text;
            Comobj.EstablishmentCode = txtEstablishmentCode.Text;
            Comobj.UserDefI = txtUserDefOne.Text;
            Comobj.UserDefII = txtUserDefTwo.Text;
            Comobj.UserDefIII = txtUserDefThree.Text;
            objdata.CompanyRegistration(Comobj);
            objdata.LocationRegistration(Comobj);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Save Successfully');", true);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtLocationCode.Text = "";
        txtLocationName.Text = "";
        txtCompanyCode.Text = "";
        txtCompanyName.Text = "";
        txtAddressone.Text = "";
        txtAddresstwo.Text = "";
        txtPincode.Text = "";
        txtState.Text = "";
        txtPhoneNo.Text = "";
        txtEmailID.Text = "";
        txtEstablishmentCode.Text = "";
        txtUserDefOne.Text = "";
        txtUserDefTwo.Text = "";
        txtUserDefThree.Text = "";
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(id);
                break;
            case ("Edit"):
                EditRepeaterData(id);
                break;
        }
    }

    public void CreateUserDisplay()
    {
        DataTable dtdDisplay = new DataTable();
        //dtdDisplay = objdata.CompanyMasterDisplay();
        dtdDisplay = objdata.UserCreationDisplay(SessionCcode, SessionLcode);
        if (dtdDisplay.Rows.Count > 0)
        {
            rptrCustomer.DataSource = dtdDisplay;
            rptrCustomer.DataBind();
        }
    }

    private void DeleteRepeaterData(string id)
    {
        int ss = Convert.ToInt16(id);
        DataTable Dtd = new DataTable();
        Dtd = objdata.DeleteUserCreation(ss);
        CreateUserDisplay();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
        //string str = "delete from students where id=" + id;
        //try
        //{
        //    cmd = new SqlCommand(str, con);
        //    con.Open();
        //    cmd.ExecuteNonQuery();
        //    lblresult.Text = "Record Deleted Successfully";
        //    lblresult.ForeColor = System.Drawing.Color.Green;
        //}
        //catch (Exception ex)
        //{
        //    lblresult.Text = ex.Message.ToString();
        //    lblresult.ForeColor = System.Drawing.Color.Red;
        //}
        //finally
        //{
        //    Bind();
        //    con.Close();
        //}
    }



    private void EditRepeaterData(string id)
    {
        ss = Convert.ToInt16(id);
        DataTable Dtd = new DataTable();
        Dtd = objdata.EditUserCreation(ss);
        if (Dtd.Rows.Count > 0)
        {
            //String Utype = Dtd.Rows[0]["UserType"].ToString();
            //if (Utype == "Admin")
            //{
            //    ddlUserType.SelectedIndex = 1;
            //}
            //else if (Utype == "Non-Admin")
            //{
            //    ddlUserType.SelectedIndex = 2;
            //}
            //txtUserName.Text = Dtd.Rows[0]["UserName"].ToString();
            //txtPassword.Text = Dtd.Rows[0]["Password"].ToString();

            CreateUserDisplay();
        }

    }
}
