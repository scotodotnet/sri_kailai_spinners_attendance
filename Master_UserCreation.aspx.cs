﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;

public partial class Master_UserCreation : System.Web.UI.Page
{
    string Company_Code;
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    UserCreationClass objUsercreation = new UserCreationClass();
    DataTable dtdDisplay = new DataTable();
    string SSQL;
    string loc;
    string usertype ="0";
    string ccode;
    string lcode;
    string[] Company;
    string[] Location; 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
        
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Master User Creation";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");

                CreateUserDisplay();
                Dropdown_Company();

            }
        }
    }

    public void Dropdown_Company()
    {
        DataTable dt = new DataTable();
        dt = objdata.Dropdown_Company();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlCompany.Items.Add(dt.Rows[i]["Cname"].ToString());
       }
        Load_Location();
    }

    public void Load_Location()
    {

        

        Company_Code = ddlCompany.SelectedItem.Text;
        Company = Company_Code.Split('-');
        ccode = Company[0];

        DataTable dt = new DataTable();
        dt = objdata.dropdown_loc(ccode);
        ddlLocation.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["LocCode"] = "Location Name";
        dr["Location"] = "Location Name";
        dt.Rows.InsertAt(dr, 0);
        ddlLocation.DataTextField = "Location";
        ddlLocation.DataValueField = "LocCode";
        ddlLocation.DataBind();
      
     }

    public void CreateUserDisplay()
    {
        DataTable dtdUserType = new DataTable();
        SSQL = "select * from User_Login where UserType= '" + usertype +"'";
        dtdUserType = objdata.ReturnMultipleValue(SSQL);
     
            rptrCustomer.DataSource = dtdUserType;
            rptrCustomer.DataBind();
      
     }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        string Company_Code = ddlCompany.SelectedItem.Text;
        Company = Company_Code.Split('-');
        ccode = Company[0];
        string loc = ddlLocation.SelectedItem.Text;
        Location = loc.Split('-');
        lcode = Location[0];

        bool ErrFlag = false;

        if (txtPassword.Text != txtConformPwd.Text)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('UserName and Conform Username Must Be Same');", true);
            ErrFlag = true;
        }
        else
        {
                if (btnSave.Text == "Update")
                {
                    DataTable Dtdedit = new DataTable();
                    objUsercreation.Ccode = ccode;
                    objUsercreation.Lcode = SessionLcode.ToString();
                    objUsercreation.UserCode =usertype;
                    objUsercreation.UserName = txtUserName.Text;
                    objUsercreation.Password = UTF8Encryption(txtPassword.Text.Trim());
                    string pwd = UTF8Encryption(txtPassword.Text);
                    SSQL = "update User_Login set CompCode='"+ccode+"',LocCode='"+lcode+"',UserName='"+txtUserName.Text+"',Password='"+ pwd +"',UserType='"+ usertype.ToString() +"'";
                    Dtdedit = objdata.ReturnMultipleValue(SSQL);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Update sucessFully');", true);
                    CreateUserDisplay();
                    clear();
                    btnSave.Text = "Save";
                }
                else
                {


                    DataTable dtd = new DataTable();

                    SSQL = "select * from User_Login where UserType= '" + usertype + "' and LocCode='"+lcode+"'";
                    dtd = objdata.ReturnMultipleValue(SSQL);
                    if (dtd.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Super Admin Have Onlu One Rights');", true);
                        ErrFlag = true;
                    }
                    else
                    {

                        loc = ddlLocation.SelectedItem.Text;
                        Location = loc.Split('-');
                        lcode = Location[0];

                        Company_Code = ddlCompany.SelectedItem.Text;
                        Company = Company_Code.Split('-');
                        ccode = Company[0];
                      
                    

                        objUsercreation.Ccode = ccode;
                        objUsercreation.Lcode = lcode;
                        objUsercreation.UserCode =usertype;
                        objUsercreation.UserName = txtUserName.Text;
                        objUsercreation.Password = UTF8Encryption(txtPassword.Text.Trim());
                        string pwd = UTF8Encryption(txtPassword.Text);
                        objdata.UserCreationRegistration(objUsercreation);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Save sucessFully');", true);
                        clear();
                        CreateUserDisplay();
                    }
                }
            }
        }
    

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData();
                break;
            //case ("Edit"):
            //    EditRepeaterData();
            //    break;
        }
    }

    //private void EditRepeaterData()
    //{
    //    //ss = Convert.ToInt16(id);
    //    DataTable Dtd = new DataTable();
    //    SSQL = "select * from User_Login where UserType= '" + usertype + "' ";
    //    Dtd = objdata.ReturnMultipleValue(SSQL);
    //    if (Dtd.Rows.Count > 0)
    //    {
    //        DataTable dt = new DataTable();
    //        String Utype = Dtd.Rows[0]["UserType"].ToString();
    //        string loc = Dtd.Rows[0]["LocCode"].ToString();
    //        SSQL = "select*from Location_Mst where LocCode='" + loc.ToString() + "'";
    //        dt = objdata.ReturnMultipleValue(SSQL);
    //        string loca = dt.Rows[0]["LocName"].ToString();
    //        ddlLocation.SelectedItem.Text = loca.ToString();
    //        txtUserName.Text = Dtd.Rows[0]["UserName"].ToString();
    //        txtPassword.Text = Dtd.Rows[0]["Password"].ToString();
    //        CreateUserDisplay();
    //        btnSave.Text = "Update";
    //    }
    //}

    private void DeleteRepeaterData()
    {
        //int ss = Convert.ToInt16(id);
        DataTable Dtd = new DataTable();
        SSQL = "Delete User_Login where UserType='"+ usertype +"'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        CreateUserDisplay();
        clear();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
    }


    //public static String UTF8Encryption(String originalPassword)
    //{
    //    UTF8Encoding encoder = new UTF8Encoding();
    //    MD5 md5 = new MD5CryptoServiceProvider();

    //    Byte[] hashedbytes = md5.ComputeHash(encoder.GetBytes(originalPassword));
    //    return BitConverter.ToString(hashedbytes).Replace("-", "").ToLower();
    //}


    protected void BtnClear_Click(object sender, EventArgs e)
    {
        clear();
    }

    public void clear()
    {
        txtUserName.Text = "";
        txtPassword.Text = "";
        ddlLocation.SelectedIndex = 0;
    }

     private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

   

}
