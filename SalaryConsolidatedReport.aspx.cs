﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class SalaryConsolidatedReport : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];

    DataTable mDataSet = new DataTable();
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str = null;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    Int32 time_Check_dbl = 0;
    string Total_Time_get = "";

    DataTable Payroll_DS = new DataTable();
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    DataSet ds = new DataSet();
    string SSQL = "";
    string mIpAddress_IN;
    string mIpAddress_OUT;

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report Salary Consolidated Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();

            //ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            ddlShiftType = Request.QueryString["ShiftType1"].ToString(); ;

            
            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("Dept");
            AutoDTable.Columns.Add("Type");
            AutoDTable.Columns.Add("Shift");
            AutoDTable.Columns.Add("EmpCode");
            AutoDTable.Columns.Add("ExCode");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("TimeIN");
            AutoDTable.Columns.Add("TimeOUT");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Category");
            AutoDTable.Columns.Add("SubCategory");
            AutoDTable.Columns.Add("TotalMIN");
            AutoDTable.Columns.Add("GrandTOT");
            AutoDTable.Columns.Add("DayWages");
            AutoDTable.Columns.Add("BaseSalary");
            AutoDTable.Columns.Add("OT");
            AutoDTable.Columns.Add("Nominee");
            AutoDTable.Columns.Add("Leader");



            DataCells.Columns.Add("CompanyName");
            DataCells.Columns.Add("LocationName");
            DataCells.Columns.Add("ShiftDate");
            DataCells.Columns.Add("SNo");
            DataCells.Columns.Add("Dept");
            DataCells.Columns.Add("Type");
            DataCells.Columns.Add("Shift");
            DataCells.Columns.Add("Category");
            DataCells.Columns.Add("CatName");
            DataCells.Columns.Add("SubCategory");
            DataCells.Columns.Add("EmpCode");
            DataCells.Columns.Add("ExCode");
            DataCells.Columns.Add("Name");
            DataCells.Columns.Add("TimeIN");
            DataCells.Columns.Add("TimeOUT");
            DataCells.Columns.Add("MachineID");
            DataCells.Columns.Add("PrepBy");
            DataCells.Columns.Add("PrepDate");
            DataCells.Columns.Add("TotalMIN");
            DataCells.Columns.Add("GrandTOT");
            DataCells.Columns.Add("DayWages");
            DataCells.Columns.Add("Wages_Type");
            DataCells.Columns.Add("BaseSalary");
            DataCells.Columns.Add("OT");
            DataCells.Columns.Add("Grade");
            DataCells.Columns.Add("Leader");

            SSQL = "";
            SSQL = "Select IPAddress,IPMode from IPAddress_Mst Where Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";

            mDataSet = objdata.ReturnMultipleValue(SSQL);

            if (mDataSet.Rows.Count > 0)
            {
                for (int iRow = 0; iRow < mDataSet.Rows.Count - 1; iRow++)
                {
                    if ((mDataSet.Rows[iRow]["IPMode"].ToString()) == "IN")
                    {
                        mIpAddress_IN = mDataSet.Rows[iRow]["IPAddress"].ToString();
                    }
                    else if ((mDataSet.Rows[iRow]["IPMode"].ToString()) == "OUT")
                    {
                        mIpAddress_OUT = mDataSet.Rows[iRow]["IPAddress"].ToString();
                    }
                }
            }

            DataTable mLocalDS = new DataTable();
            string qry = "";

            qry = "";
            qry = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
            qry = qry + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
            qry += " from Shift_Mst Where CompCode='" + SessionCcode + "'";
            qry += " And LocCode='" + SessionLcode + "'";

            //'qry &= " And shiftDesc='" & Trim("All") & "'"

            //qry &= " And ShiftDesc like '" & "" & "%'" 'Shift%'"
            //qry &= " Order By shiftDesc"

            if (ShiftType1 != "ALL")
            {
                SSQL += " And shiftDesc='" + ShiftType1 + "'";
            }
            SSQL += " And ShiftDesc like '" + ddlShiftType + "%'";
            //Shift%'"
            SSQL += " Order By shiftDesc";

            mLocalDS = objdata.ReturnMultipleValue(qry);

            string ShiftType = "";
            string ng = string.Format(Date, "dd-MM-yyyy");
            DateTime date1 = Convert.ToDateTime(ng);


            int mStartINRow = 0;
            int mStartOUTRow = 0;

            for (int iTabRow = 0; iTabRow < mLocalDS.Rows.Count; iTabRow++)
            {
                if (AutoDTable.Rows.Count <= 1)
                {
                    mStartOUTRow = 0;
                }
                else
                {
                    mStartOUTRow = AutoDTable.Rows.Count - 1;
                }
                if (mLocalDS.Rows[iTabRow]["shiftDesc"].ToString() == "GENERAL")
                {
                    ShiftType = "GENERAL";
                }
                else
                {
                    ShiftType = "SHIFT";
                }
                string sIndays_str = mLocalDS.Rows[iTabRow]["StartIN_Days"].ToString();
                double sINdays = Convert.ToDouble(sIndays_str);
                string eIndays_str = mLocalDS.Rows[iTabRow]["EndIN_Days"].ToString();
                double eINdays = Convert.ToDouble(eIndays_str);

                if (ShiftType == "GENERAL")
                {
                    SSQL = "";
                    SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID ";
                    SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And LT.IPAddress='" + mIpAddress_IN + "'";

                    SSQL = SSQL + " And LT.TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";

                    SSQL = SSQL + " And LT.TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    SSQL = SSQL + " AND em.ShiftType='" + ShiftType + "' Group By LT.MachineID";
                    SSQL = SSQL + " Order By Min(LT.TimeIN)";



                }
                else if (ShiftType == "SHIFT")
                {
                    SSQL = "";
                    SSQL = "Select MachineID,Min(TimeIN) as [TimeIN] from LogTime_IN ";
                    SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And IPAddress='" + mIpAddress_IN + "'";

                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartIN"].ToString() + "'";

                    SSQL = SSQL + " And TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndIN"].ToString() + "'";
                    SSQL = SSQL + " Group By MachineID";
                    SSQL = SSQL + " Order By Min(TimeIN)";
                }
                else
                {
                    SSQL = "";
                    SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID ";
                    //SSQL = "Select MachineID,Min(convert( varchar(10), TimeIN, 102) +"
                    //SSQL = SSQL & " stuff( right( convert( varchar(26), TimeIN, 109 ), 15 ), 7, 7, ' ' )) as [TimeIN] from LogTime_IN "
                    SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And LT.IPAddress='" + mIpAddress_IN + "'";

                    SSQL = SSQL + " And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartIN"].ToString() + "'";

                    SSQL = SSQL + " And LT.TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndIN"].ToString() + "'";
                    SSQL = SSQL + " AND em.ShiftType='" + ShiftType + "' Group By LT.MachineID";
                    SSQL = SSQL + " Order By Min(LT.TimeIN)";
                }


                mDataSet = objdata.ReturnMultipleValue(SSQL);


                if (mDataSet.Rows.Count > 0)
                {
                    //fg.Rows = fg.Rows + mDataSet.Tables(0).Rows.Count

                    string MachineID;

                    for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
                    {

                        Boolean chkduplicate = false;
                        chkduplicate = false;

                        for (int ia = 0; ia < AutoDTable.Rows.Count; ia++)
                        {
                            string id = mDataSet.Rows[iRow]["MachineID"].ToString();

                            if (id == AutoDTable.Rows[ia][9].ToString())
                            {
                                chkduplicate = true;
                            }
                        }
                        if (chkduplicate == false)
                        {
                            AutoDTable.NewRow();
                            AutoDTable.Rows.Add();


                            // AutoDTable.Rows.Add();

                            // AutoDTable.Rows[mStartINRow][0] = iRow + 1;
                            AutoDTable.Rows[mStartINRow][3] = mLocalDS.Rows[iTabRow]["ShiftDesc"].ToString();
                            if (ShiftType == "SHIFT")
                            {
                                string str = mDataSet.Rows[iRow][1].ToString();
                                // string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1])
                                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);
                            }
                            else
                            {
                                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);
                            }


                            MachineID = mDataSet.Rows[iRow]["MachineID"].ToString();
                            AutoDTable.Rows[mStartINRow][9] = MachineID.ToString();
                            mStartINRow += 1;
                        }

                    }
                }


                mDataSet = new DataTable();

                string sOUTdays_str = mLocalDS.Rows[iTabRow]["StartOUT_Days"].ToString();
                double sOUTdays = Convert.ToDouble(sOUTdays_str);
                string eOUTdays_str = mLocalDS.Rows[iTabRow]["EndOUT_Days"].ToString();
                double eOUTdays = Convert.ToDouble(eOUTdays_str);



                SSQL = "";
                SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT ";
                SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(sOUTdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartOUT"].ToString() + "'";

                SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(eOUTdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndOUT"].ToString() + "'";
                SSQL = SSQL + " Group By MachineID";
                SSQL = SSQL + " Order By Max(TimeOUT)";

                mDataSet = objdata.ReturnMultipleValue(SSQL);

                string InMachine_IP = "";
                DataTable mLocalDS_out = new DataTable();

                for (int iRow2 = mStartOUTRow; iRow2 < AutoDTable.Rows.Count; iRow2++)
                {
                    InMachine_IP = AutoDTable.Rows[iRow2][9].ToString();

                    if (InMachine_IP == "FMrNe9XKN9Ejm/qY+QuVig==")
                    {
                        string str = "";

                    }
                    //TimeOUT Get
                    SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "' And Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(sOUTdays).ToString("yyyy/MM/dd") + " " + "00:00" + "'";
                    SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(eOUTdays).ToString("yyyy/MM/dd") + " " + "23:59" + "' Order by TimeOUT Desc";
                    mLocalDS_out = objdata.ReturnMultipleValue(SSQL);
                    if (mLocalDS_out.Rows.Count <= 0)
                    {
                        //Skip
                    }
                    else
                    {
                        if (AutoDTable.Rows[iRow2][9].ToString() == mLocalDS_out.Rows[0][0].ToString())
                        {
                            AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", (mLocalDS_out.Rows[0][1]));

                        }
                    }

                }


            }


            DataTable mEmployeeDS = new DataTable();

            SSQL = "";
            SSQL = "select isnull(MachineID_Encrypt,'') as [MachineID],isnull(DeptName,'') as [DeptName]";
            SSQL = SSQL + ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
            SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName] ,BaseSalary";
            SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName],isnull(OTEligible,'') as OT,";
            SSQL = SSQL + " isnull(Nominee,'') as Grade from Employee_Mst Where Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'";

            mEmployeeDS = objdata.ReturnMultipleValue(SSQL);



            if (mEmployeeDS.Rows.Count > 0)
            {

                for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                {
                    for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
                    {

                        if (AutoDTable.Rows[iRow2][9].ToString() == mEmployeeDS.Rows[iRow1]["MachineID"].ToString())
                        {
                            AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"].ToString();
                            AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"].ToString();
                            AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"].ToString();
                            AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"].ToString();
                            AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"].ToString();
                            AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"].ToString();
                            AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"].ToString();
                            AutoDTable.Rows[iRow2][15] = mEmployeeDS.Rows[iRow1]["BaseSalary"].ToString();
                            AutoDTable.Rows[iRow2][16] = mEmployeeDS.Rows[iRow1]["OT"].ToString();
                            AutoDTable.Rows[iRow2][17] = mEmployeeDS.Rows[iRow1]["Grade"].ToString();
                            //AutoDTable.Rows[iRow2][18] = mEmployeeDS.Rows[iRow1]["Leader"].ToString();







                            //ealert Wages
                            SSQL = "select BaseSalary as DaySalary,Wages as WagesType ,CatName as CatName from Employee_Mst where";
                            SSQL = SSQL + " MachineID_Encrypt='" + AutoDTable.Rows[iRow2][9].ToString() + "'";
                            Payroll_DS = objdata.ReturnMultipleValue(SSQL);


                            //Day Wages And Type Get

                            if (Payroll_DS.Rows.Count != 0)
                            {
                                AutoDTable.Rows[iRow2][15] = Payroll_DS.Rows[0]["WagesType"].ToString();
                                if (Payroll_DS.Rows[0]["CatName"].ToString() != "1" & Payroll_DS.Rows[0]["CatName"].ToString() != "2")
                                {
                                    AutoDTable.Rows[iRow2][14] = Payroll_DS.Rows[0]["DaySalary"].ToString();
                                }
                                else
                                {
                                    AutoDTable.Rows[iRow2][14] = "0.00";
                                }
                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][14] = "0.00";
                                AutoDTable.Rows[iRow2][15] = "";
                            }
                        }
                    }
                }
                for (int irow2 = 0; irow2 < AutoDTable.Rows.Count; irow2++)
                {
                    if (string.IsNullOrEmpty(AutoDTable.Rows[irow2][14].ToString()))
                    {
                        AutoDTable.Rows[irow2][14] = "0.00";
                        AutoDTable.Rows[irow2][15] = "";
                    }
                }

            }
            //fg.Cols = fg.Cols + 1
            for (int iRow = 0; iRow < AutoDTable.Rows.Count; iRow++)
            {
                DataTable mLocalDS_INTAB = new DataTable();
                DataTable mLocalDS_OUTTAB = new DataTable();
                string Date_Value_Str = null;
                string Time_IN_Str = "";
                string Time_Out_Str = "";
                Int32 time_Check_dbl = 0;
                string Total_Time_get = "";
                string Emp_Total_Work_Time_1 = "00:00";
                Date_Value_Str = date1.ToString("yyyy/MM/dd");
                if (!string.IsNullOrEmpty(AutoDTable.Rows[iRow][7].ToString()) & !string.IsNullOrEmpty(AutoDTable.Rows[iRow][8].ToString()))
                {
                    DateTime date11 = System.Convert.ToDateTime(AutoDTable.Rows[iRow][7]);
                    DateTime date12 = System.Convert.ToDateTime(AutoDTable.Rows[iRow][8]);
                    TimeSpan ts = new TimeSpan();
                    ts = date12.Subtract(date11);
                    ts = date12.Subtract(date11);
                    //Grand Total multi in and Multi out 


                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + AutoDTable.Rows[iRow][9].ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "00:00' And TimeIN <='" + Date_Value_Str + " " + "23:59' Order by TimeIN ASC";
                    mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + AutoDTable.Rows[iRow][9].ToString() + "'";
                    SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "00:00' And TimeOUT <='" + Date_Value_Str + " " + "23:59' Order by TimeOUT desc";
                    mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);
                    string[] Time_Error_Spilit;
                    if (mLocalDS_INTAB.Rows.Count > 1)
                    {
                        for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

                            if (mLocalDS_OUTTAB.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_Out_Str = "00:00";
                            }
                            TimeSpan ts4 = new TimeSpan();

                            Time_Error_Spilit = Emp_Total_Work_Time_1.Split(':');
                            if (Left_Val(Time_Error_Spilit[0], 1) == "-" | Left_Val(Time_Error_Spilit[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "00:00";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                            }


                            //Emp_Total_Work_Time
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);

                                if (date11 > date12)
                                {
                                    date12 = System.Convert.ToDateTime(mLocalDS_OUTTAB.Rows[0][0].ToString());
                                }
                                TimeSpan ts1 = new TimeSpan();
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                ts4 = ts4.Add(ts1);
                                //OT Time Get
                                Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date12.Subtract(date11);
                                    ts = date12.Subtract(date11);
                                    Total_Time_get = ts.Hours.ToString();
                                    //& ":" & Trim(ts.Minutes)
                                    time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                    //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                    Emp_Total_Work_Time_1 = ts.Hours.ToString() + ":" + ts.Minutes.ToString();
                                }
                                else
                                {
                                    time_Check_dbl = time_Check_dbl + Convert.ToInt32(Total_Time_get);
                                }
                            }
                        }

                    }
                    else
                    {
                        TimeSpan ts4 = new TimeSpan();
                        Time_Error_Spilit = Emp_Total_Work_Time_1.Split(':');
                        if (Left_Val(Time_Error_Spilit[0], 1) == "-" | Left_Val(Time_Error_Spilit[1], 1) == "-")
                        {
                            Emp_Total_Work_Time_1 = "00:00";
                        }
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS_INTAB.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout < mLocalDS_OUTTAB.Rows.Count; tout++)
                        {
                            if (mLocalDS_OUTTAB.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";
                            }
                            else
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                            }

                        }
                        //Emp_Total_Work_Time
                        if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts1 = new TimeSpan();
                            ts1 = date4.Subtract(date3);
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();
                            //& ":" & Trim(ts.Minutes)
                            ts1 = ts1.Add(ts);
                            //OT Time Get
                            Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                            //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date12 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date12.Subtract(date11);
                                ts = date12.Subtract(date11);
                                Total_Time_get = ts.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                Emp_Total_Work_Time_1 = ts1.Hours.ToString() + ":" + ts1.Minutes.ToString();
                            }
                            else
                            {
                                time_Check_dbl = Convert.ToInt32(Total_Time_get);
                            }
                        }
                    }

                    //End
                    if (Left_Val(ts.Hours.ToString(), 1) == "-")
                    {
                        date12 = System.Convert.ToDateTime(AutoDTable.Rows[iRow][8]).AddDays(1);
                        ts = date12.Subtract(date11);
                        ts = date12.Subtract(date11);
                        AutoDTable.Rows[iRow][12] = ts.Hours.ToString() + ":" + ts.Minutes.ToString();
                    }
                    else
                    {
                        AutoDTable.Rows[iRow][12] = ts.Hours.ToString() + ":" + ts.Minutes.ToString();
                    }

                    AutoDTable.Rows[iRow][13] = Emp_Total_Work_Time_1;
                    //fg.set_TextMatrix(iRow, fg.Cols - 1, Trim(ts.Hours) & ":" & Trim(ts.Minutes))
                }
            }
            int k = 0;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {

                DataCells.NewRow();
                DataCells.Rows.Add();

                DataCells.Rows[k]["CompanyName"] = SessionCcode;
                DataCells.Rows[k]["LocationName"] = SessionLcode;
                DataCells.Rows[k]["ShiftDate"] = Date;
                DataCells.Rows[k]["Dept"] = AutoDTable.Rows[i]["Dept"].ToString();
                DataCells.Rows[k]["Type"] = AutoDTable.Rows[i]["Type"];
                DataCells.Rows[k]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                DataCells.Rows[k]["Category"] = AutoDTable.Rows[i]["Category"].ToString();

                DataCells.Rows[k]["CatName"] = AutoDTable.Rows[i]["Category"].ToString();

                DataCells.Rows[k]["SubCategory"] = AutoDTable.Rows[i]["SubCategory"].ToString();
                DataCells.Rows[k]["EmpCode"] = AutoDTable.Rows[i]["EmpCode"].ToString();
                DataCells.Rows[k]["ExCode"] = AutoDTable.Rows[i]["ExCode"].ToString();
                DataCells.Rows[k]["Name"] = AutoDTable.Rows[i]["Name"].ToString();
                DataCells.Rows[k]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                DataCells.Rows[k]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                DataCells.Rows[k]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                DataCells.Rows[k]["PrepBy"] = "user";
                DataCells.Rows[k]["PrepDate"] = Date;
                DataCells.Rows[k]["TotalMIN"] = AutoDTable.Rows[i]["TotalMIN"].ToString();
                DataCells.Rows[k]["GrandTOT"] = AutoDTable.Rows[i]["GrandTOT"].ToString();

                DataCells.Rows[k]["DayWages"] = AutoDTable.Rows[i]["DayWages"].ToString();
                DataCells.Rows[k]["Wages_Type"] = AutoDTable.Rows[i]["DayWages"].ToString();
                DataCells.Rows[k]["BaseSalary"] = AutoDTable.Rows[i]["BaseSalary"].ToString();
                DataCells.Rows[k]["OT"] = AutoDTable.Rows[i]["OT"].ToString();

                k = k + 1;

            }









            ds.Tables.Add(DataCells);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Salary.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            CrystalReportViewer1.ReportSource = report;



        }


    }
}


