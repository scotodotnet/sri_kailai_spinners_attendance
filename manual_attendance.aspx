﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="manual_attendance.aspx.cs" Inherits="manual_attendance" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableCustomer').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tableCustomer').dataTable();               
            }
        });
    };
</script>

 <style type="text/css">
        .CalendarCSS
        {
            background-color:White;
            color:Black;
            border:1px solid #646464;
        }
        </style>
         <style type="text/css">
                 .ajax__calendar_container { z-index : 1000 ;background-color: White;}
         </style>


 <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate>

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Manual Attendance</li>
                       <h4>
                       </h4>
                        <h4>
                       </h4>
                        <h4>
                       </h4>
                        </h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Manual Attendance</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
					
        
                           
					<div class="form-group row">
					   <label for="input-Default" class="col-sm-2 control-label">Emp No</label>
						<div class="col-sm-4">
                             <asp:DropDownList ID="ddlTicketNo" class="form-control" runat="server" AutoPostBack="true"
                                 onselectedindexchanged="ddlTicketNo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
					
				    <div class="col-sm-1"></div>
				     <label for="input-Default" class="col-sm-2 control-label">Existing Code</label>
					 <div class="col-sm-3">
                         <asp:TextBox ID="TxtExstCode" class="form-control" Enabled="false" runat="server" required></asp:TextBox>               
					  </div>
			       </div>
					    
					    <div class="form-group row">
					    
					       <label for="input-Default" class="col-sm-2 control-label">Emp Name</label>
						<div class="col-sm-4">
                            <asp:TextBox ID="TxtEmpName" class="form-control" Enabled="false" runat="server" required></asp:TextBox>
                        </div>
					    
					   	<div class="col-md-1"></div>
						<label for="input-Default" class="col-sm-2 control-label">Attn Date</label>
						<div class="col-sm-3">
						 <asp:TextBox ID="TxtAttnDate" class="form-control col-md-6" AutoPostBack = "true" runat="server" required></asp:TextBox>
                           <cc1:CalendarExtender ID="CalendarExtender4" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="TxtAttnDate">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="TxtAttnDate" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
                        
                        
                      
                          
						</div>
				        </div> 
                         
                         <div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Time In</label>
						<div class="col-sm-4">
                            <asp:TextBox ID="TxtTimeIN" class="form-control" runat="server" required></asp:TextBox>
                        </div>
					     
						<div class="col-md-1"></div>
						<label for="input-Default" class="col-sm-2 control-label">Attn Status</label>
							<div class="col-sm-3">
                               <asp:DropDownList ID="ddlAttnStatus" runat="server" class="form-control" 
                                    AutoPostBack="true" 
                                    onselectedindexchanged="ddlAttnStatus_SelectedIndexChanged">
                                <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                <asp:ListItem Value="2" Text="P"></asp:ListItem>
                                <asp:ListItem Value="3" Text="H"></asp:ListItem>
                                <asp:ListItem Value="6" Text="A"></asp:ListItem>
                                   <asp:ListItem>OD</asp:ListItem>
                               </asp:DropDownList>
							</div>
						
                         </div>
					
					    <div class="form-group row">	
				     
                         <label for="input-Default" class="col-sm-2 control-label">Time Out</label>
						<div class="col-sm-4">
                            <asp:TextBox ID="TxtTimeOUT" class="form-control" runat="server" required></asp:TextBox>
                        </div>
                        
                        
                        
                        
                        <div class="col-md-1"></div>
                            <asp:Label ID="lblCompensationDate" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Compensation Date" Visible=false></asp:Label>
                           
						
						<div class="col-sm-3">
						                     <asp:TextBox ID="txtCompensationDate" class="form-control col-md-6" AutoPostBack = "true" runat="server" required visible=false ReadOnly="True"></asp:TextBox>
                                              <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtCompensationDate">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtCompensationDate" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
						</div>
                        
						</div>
						
						
						  <div class="form-group row">	
						         <asp:Label ID="lblCOagainstDate" for="input-Default" class="col-sm-2 control-label" runat="server" Text="CO/Against Date" Visible=false></asp:Label>
						 <%-- <label for="input-Default" class="col-sm-2 control-label">CO/Against Date</label>--%>
						          <div class="col-sm-4">
						                       <asp:TextBox ID="txtCOagainstDate" class="form-control" AutoPostBack = "true" 
                                                   runat="server" required visible=false 
                                                   ontextchanged="txtCOagainstDate_TextChanged"></asp:TextBox>
                                               <cc1:CalendarExtender ID="CalendarExtender2" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtCOagainstDate">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtCOagainstDate" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
                        
						            </div>
						  </div>
						
						<!-- Button start -->
						<div class="form-group row">
						</div>
                            <div class="txtcenter">
                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                    onclick="btnSave_Click" />
                                                       
                   <asp:LinkButton ID="btnClear" class="btn btn-danger" runat="server" onclick="btnClear_Click">Clear</asp:LinkButton>      
                    </div>
                    <!-- Button End -->	
						
						<div class="form-group row">
						</div>
						
						
						
       
       
						<!-- Button start -->
						<div class="form-group row">
						</div>
                           
                        
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
                       <!-- Dashboard start -->
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->

 </ContentTemplate>
 </asp:UpdatePanel>
 
 
</asp:Content>

