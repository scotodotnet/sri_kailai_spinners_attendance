﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DepartmentDetails.aspx.cs" Inherits="DepartmentDetails" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>



<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>


<script src='<%= ResolveUrl("assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

    <script>
        $(document).ready(function () {
            $('#tableCustomer').dataTable();
        });
    </script>



                           <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate> 
                                    
                                    
                 <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Department Details</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
            <div class="col-md-9">
			<div class="panel panel-white">
			    <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Department</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
           
                        <form class="form-horizontal">
                          <div class="panel-body">
                             <div class="col-md-20">
                                <div class="row">
				                          
				                                <div class="form-group row">
                                                <asp:Button ID="btnDept" class="btn btn-primary btn-rounded" runat="server" Text="ADD"/>
                                                <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panl1" TargetControlID="btnDept"
                                                 CancelControlID="" BackgroundCssClass="Background">
                                                </cc1:ModalPopupExtender>
                                                <asp:Panel ID="Panl1" runat="server" CssClass="Popup" align="center" BackColor="White" Height="269px" Width="400px" style="display:none">
				                          	        <table width="100%" style="border:Solid 3px #EEE8AA; width:100%; height:100%" cellpadding="0" cellspacing="0">
                                                      <tr style="background-color:#D3D3D3">
                                                        <td colspan="2" style=" height:10%; color:White; font-weight:bold; font-size:larger" align="center">DEPARTMENT</td>
                                                     </tr>
                                                     <tr>
                                                        <td align="right"> DepartmentName:</td>
                                                        <td>
                                                          <asp:TextBox ID="txtDepartmentName" class="form-control" runat="server" required></asp:TextBox>
                                                       </td>
                                                    </tr>
                                                    <tr>
                                                      <td></td>
                                                       <td><asp:Button ID="btnSaveDept" class="btn btn-success"  runat="server" Text="Save" 
                                                        onclick="btnSaveDept_Click" />
                                                       <asp:LinkButton ID="BtnClear" runat="server" class="btn btn-danger" onclick="BtnClear_Click" >Clear</asp:LinkButton>  
                                                   
                                                      </td>
                                                  </tr>
                                                  </table>						
                                                </asp:Panel>
                                                <div class="col-sm-1"></div>
                                                </div>
                                                                                
		                                        <div class="panel-body">
                                                 <div class="table-responsive">
                                                   <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
		                                        <HeaderTemplate>
                                                    <table id="example" class="display table">
                                                        <thead>
                                                            <tr>
                                                                
                                                                <th>Department Name</th>
                                                               
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        
                                                        <td><%# Eval("DeptName")%></td>
                                                       
                                                        <td>
                                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("DeptName")%>'>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("DeptName")%>' 
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this new user details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
		                                    </asp:Repeater>
                                                     <asp:HiddenField ID="DeptNameHide" runat="server" />
                                                </div>
                                               </div>
                                                         
						                        </div>
						                     </div>
						                    </div>
						                  </form>
						                        
						                 </div>
					</form>
						            	</div><!-- panel white end -->
		    
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
                       <!-- Dashboard start -->
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
      </div>        	                 
						            </ContentTemplate>
                             </asp:UpdatePanel>   


</asp:Content>

