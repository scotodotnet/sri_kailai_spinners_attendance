﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;

public partial class AdministrationRights : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    BALDataAccess objdata = new BALDataAccess();
    UserCreationClass objUsercreation = new UserCreationClass();
    string SSQL;
    string Dept;
    string DashBoard;
    string masterpage;
    string UserCreation;
    string AdministrationRight;
    string DepartmentDetails;
    string DesginationDetails;
    string EmployeeTypeDetails;
    string WagesTypeDetails;
    string EmployeeDetails;
    string LeaveMaster;

    string PermissionMaster;
    string GraceTime;
    string EarlyOutMaster;
    string LateINMaster;
    string Employee;
    string EmployeeApproval;
    string EmployeeStatus;
    string LeaveManagement;
    string TimeDelete;
    string LeaveDetails;

    string PermissionDetail;
    string ManualAttendance;
    string ManualShift;
    string ManualShiftDetails;
    string UploadDownload;
    string Deduction;
    string Incentive;
    string Report;
    string DownloadClear;
    string CompanyMaster="0";
    string SuperAdminCreation="0";
    string SalaryProcess;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            //if (!IsPostBack)
            //{
            //    Page.Title = "Spay module | Administration Rights";
            //    HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
            //    li.Attributes.Add("class", "droplink active open");
            //}

            SessionUserType = Session["UserType"].ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay module | Administration Rights";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");
                //Dropdown_Company();
                //Dropdown_Location();
                Dropdown_UserName();
                Clear();
            }
        }
    }


    //public void Dropdown_Company()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.Dropdown_Company();
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        ddlCompany.Items.Add(dt.Rows[i]["Cname"].ToString());
    //    }
    //}



    //public void Dropdown_Location()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.dropdown_ParticularLocation(SessionCcode, SessionLcode);
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        ddlLocation.Items.Add(dt.Rows[i]["LocName"].ToString());
    //    }
    // }

      public void Dropdown_UserName()
    {
        DataTable dt = new DataTable();
        dt = objdata.dropdown_UserName(SessionCcode, SessionLcode);
        ddlUserName.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["UserName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlUserName.Items.Add(dt.Rows[i]["UserName"].ToString());
        }
    }



      protected void btnSave_Click(object sender, EventArgs e)
      {
          //SSQL = "insert into Rights()values()";
          
        
          if (ChkDashBoard.Checked == true)
          {
              DashBoard = "1";             
              Dept = Dept + "1,";
          }
          else
          {
              DashBoard = "0";   
              Dept = Dept + "0,";
          }

          if (ChkMasterPage.Checked == true)
          {
              masterpage = "1";
              Dept = Dept + "1,";
          }
          else
          {
              masterpage = "0";
              Dept = Dept + "0,";
          }


          if (ChkUserCreation.Checked == true)
          {
              UserCreation = "1";
              Dept = Dept + "1,";
          }
          else
          {
              UserCreation = "0";
              Dept = Dept + "0,";
          }


          if (ChkAdministrationRights.Checked == true)
          {
              AdministrationRight = "1";
              Dept = Dept + "1,";

          }
          else
          {
              AdministrationRight = "0";
              Dept = Dept + "0,";
          }

          

          if (ChkDepartmentDetails.Checked == true)
          {
              DepartmentDetails = "1";
              Dept = Dept + "1,";
          }
          else
          {
              DepartmentDetails = "0";
              Dept = Dept + "0,";
          }

          if (ChkDesginationDetails.Checked == true)
          {
              DesginationDetails = "1";
              Dept = Dept + "1,";
          }
          else
          {
              Dept = Dept + "0,";
              DesginationDetails = "0";
          }


          if (ChkEmployeeTypeDetails.Checked == true)
          {
              EmployeeTypeDetails = "1";
              Dept = Dept + "1,";
          }
          else
          {
              EmployeeTypeDetails = "0";
              Dept = Dept + "0,";
          }

          

          if (ChkWagesTypeDetails.Checked == true)
          {
              WagesTypeDetails = "1";
              Dept = Dept + "1,";
          }
          else
          {
              WagesTypeDetails = "0";
              Dept = Dept + "0,";
          }


          if (ChkEmployeeDetails.Checked == true)
          {
              EmployeeDetails = "1";
              Dept = Dept + "1,";
          }
          else
          {
              EmployeeDetails = "0";
              Dept = Dept + "0,";
          }

          
          

          if (ChkLeaveMaster.Checked == true)
          {
              LeaveMaster = "1";
              Dept = Dept + "1,";
          }
          else
          {
              LeaveMaster = "0";
              Dept = Dept + "0,";
          }

          if (ChkPermissionMaster.Checked == true)
          {
              PermissionMaster = "1";
              Dept = Dept + "1,";
          }
          else
          {
              PermissionMaster = "0";
              Dept = Dept + "0,";
          }

          if (ChkGraceTime.Checked == true)
          {
              GraceTime = "1";
              Dept = Dept + "1,";
          }
          else
          {
              GraceTime = "0";
              Dept = Dept + "0,";
          }
          if (ChkEarlyOutMaster.Checked == true)
          {
              EarlyOutMaster = "1";
              Dept = Dept + "1,";
          }
          else
          {
              EarlyOutMaster = "0";
              Dept = Dept + "0,";
          }
         if (ChkLateINMaster.Checked == true)
          {
              LateINMaster = "1"; 
             Dept = Dept + "1,";
          }
          else
          {
              LateINMaster = "0"; 
              Dept = Dept + "0,";
          }
          if (ChkEmployee.Checked == true)
          {
              Employee = "1";
              Dept = Dept + "1,";
          }
          else
          {
              Employee = "0";
              Dept = Dept + "0,";
          }
         if (ChkEmployeeApproval.Checked == true)
          {
              EmployeeApproval = "1";
                Dept = Dept + "1,";
          }
          else
          {
              EmployeeApproval = "0";
              Dept = Dept + "0,";
          }
          if (ChkEmployeeStatus.Checked == true)
          {
              EmployeeStatus = "1";
              Dept = Dept + "1,";
          }
          else
          {
              Dept = Dept + "0,";
              EmployeeStatus = "0";
          }
          if (ChkLeaveManagement.Checked == true)
          {
              LeaveManagement = "1";
              Dept = Dept + "1,";
          }
          else
          {
              LeaveManagement = "0";
              Dept = Dept + "0,";
          }
          if (ChkTimeDelete.Checked == true)
          {
              TimeDelete = "1";
              Dept = Dept + "1,";
          }
          else
          {
              TimeDelete = "0";
              Dept = Dept + "0,";
          }

          if (ChkLeaveDetails.Checked == true)
          {
              LeaveDetails = "1";
              Dept = Dept + "1,";
          }
          else
          {
              LeaveDetails = "0";
              Dept = Dept + "0,";
          }
          if (ChkPermissionDetails.Checked == true)
          {
              PermissionDetail = "1";
              Dept = Dept + "1,";
          }
          else
          {
              PermissionDetail = "0";
              Dept = Dept + "0,";
          }

          

          if (ChkManualAttendance.Checked == true)
          {
              ManualAttendance = "1";
              Dept = Dept + "1,";
          }
          else
          {
              ManualAttendance = "0";
              Dept = Dept + "0,";
          }
          //if (ChkPermissionDetails.Checked == true)
          //{
          //    PermissionDetail = "1";
          //    Dept = Dept + "1,";
          //}
          //else
          //{
          //    PermissionDetail = "0";
          //    Dept = Dept + "0,";
          //}
          

          

          if (ChkManualShift.Checked == true)
          {
              ManualShift = "1";
              Dept = Dept + "1,";
          }
          else
          {
              ManualShift = "0";
              Dept = Dept + "0,";
          }
          if (ChkManualShiftDetails.Checked == true)
          {
              ManualShiftDetails = "1";
              Dept = Dept + "1,";
          }
          else
          {
              ManualShiftDetails = "0";
              Dept = Dept + "0,";
          }
          

          if (ChkUploadDownload.Checked == true)
          {
              UploadDownload = "1";
              Dept = Dept + "1,";
          }
          else
          {
              UploadDownload = "0";
              Dept = Dept + "0,";
          }

          if (ChkSalaryProcess.Checked == true)
          {
              SalaryProcess = "1";
              Dept = Dept + "1,";
          }
          else
          {
              SalaryProcess = "0";
              Dept = Dept + "0,";
          }

          if (ChkDeduction.Checked == true)
          {
              Deduction = "1";
              Dept = Dept + "1,";
          }
          else
          {
              Deduction = "0";
              Dept = Dept + "0,";
          }

          
          if (ChkIncentive.Checked == true)
          {
              Incentive = "1";
              Dept = Dept + "1,";
          }
          else
          {
              Incentive = "0";
              Dept = Dept + "0,";
          }
          if (ChkReports.Checked == true)
          {
              Report = "1";
              Dept = Dept + "1,";
          }
          else
          {
              Report = "0";
              Dept = Dept + "0,";
          }


          if (ChkDownloadClear.Checked == true)
          {
              DownloadClear = "1";
              Dept = Dept + "1,";
          }
          else
          {
              DownloadClear = "0";
              Dept = Dept + "0";
          }

          bool ErrFlag = false;



          if (Dept.ToString() == "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0")
          {
              ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Choose Your Rights');", true);
              ErrFlag = true;
          }

          if (ddlUserName.SelectedItem.Text == "- select -")
          {
              ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select UserName');", true);
              ErrFlag = true;
          }
          else
          {
              DataTable dtd = new DataTable();
              dtd = objdata.CheckAdminRights(SessionCcode, SessionLcode, ddlUserName.SelectedItem.Text);
              if (dtd.Rows.Count > 0)
              {
                  SSQL = "update Rights set DashBoard='" + DashBoard + "',masterpage='" + masterpage + "',UserCreation='" + UserCreation + "',";
                  SSQL = SSQL + " AdministrationRights='" + AdministrationRight + "',DepartmentDetails='" + DepartmentDetails + "',DesginationDetails='" + DesginationDetails + "',";
                  SSQL = SSQL + " EmployeeTypeDetails='" + EmployeeTypeDetails + "',WagesTypeDetails='" + WagesTypeDetails + "',EmployeeDetails='" + EmployeeDetails + "',";
                  SSQL = SSQL + " LeaveMaster='" + LeaveMaster + "',PermissionMaster='" + PermissionMaster + "',GraceTime='" + GraceTime + "',EarlyOutMaster='" + EarlyOutMaster + "',";
                  SSQL = SSQL + " LateINMaster='" + LateINMaster + "',Employee='" + Employee + "',EmployeeApproval='" + EmployeeApproval + "',EmployeeStatus='" + EmployeeStatus + "',";
                  SSQL = SSQL + " LeaveManagement='" + LeaveManagement + "',TimeDelete='" + TimeDelete + "',LeaveDetails='" + LeaveDetails + "',PermissionDetail='" + PermissionDetail + "',";
                  SSQL = SSQL + " ManualAttendance='" + ManualAttendance + "',ManualShift='" + ManualShift + "',ManualShiftDetails='" + ManualShiftDetails + "',UploadDownload='" + UploadDownload + "',";
                  SSQL = SSQL + " SalaryProcess='" + SalaryProcess +"',";
                  SSQL = SSQL + " Deduction='" + Deduction + "',Incentive='" + Incentive + "',";
                  SSQL = SSQL + " Report='" + Report + "',DownloadClear='" + DownloadClear + "',CompanyMaster='" + CompanyMaster + "',SuperAdminCreation='" + SuperAdminCreation + "'";
                  SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Username = '" + ddlUserName.SelectedItem.Text + "'";
                  objdata.ReturnMultipleValue(SSQL);
                                 
                  ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Updated sucessFully');", true);
                  ErrFlag = true;

              }
              else
              {
                  //EmployeeDetails,LeaveMaster,PermissionMaster,GraceTime

                  SSQL = "insert into Rights(CompCode,LocCode,Username,Data,DashBoard,masterpage,UserCreation, ";
                  SSQL = SSQL + "AdministrationRights,DepartmentDetails,DesginationDetails,EmployeeTypeDetails, ";
                  SSQL = SSQL + "WagesTypeDetails,";
                  SSQL = SSQL + "EmployeeDetails,LeaveMaster,PermissionMaster,GraceTime,EarlyOutMaster,LateINMaster,";
                  SSQL = SSQL + "Employee,EmployeeApproval,EmployeeStatus,LeaveManagement,TimeDelete,LeaveDetails,";
                  SSQL = SSQL + "PermissionDetail,ManualAttendance,ManualShift,ManualShiftDetails,UploadDownload,SalaryProcess,";
                  SSQL = SSQL + "Deduction,Incentive,Report,DownloadClear,CompanyMaster,SuperAdminCreation)";
                  SSQL = SSQL + "values('" + SessionCcode.ToString() + "','" + SessionLcode.ToString() + "','" + ddlUserName.SelectedItem.Text + "','" + Dept + "'";
                  SSQL = SSQL + ",'" + DashBoard + "','" + masterpage + "','" + UserCreation + "','" + AdministrationRight + "',";
                  SSQL = SSQL + "'" + DepartmentDetails + "','" + DesginationDetails + "','" + EmployeeTypeDetails + "','" + WagesTypeDetails + "',";
                  SSQL = SSQL + "'" + EmployeeDetails + "','" + LeaveMaster + "','" + PermissionMaster + "','" + GraceTime + "','"+ EarlyOutMaster +"',";
                  SSQL = SSQL + "'" + LateINMaster + "','" + Employee + "','" + EmployeeApproval + "','" + EmployeeStatus + "','" + LeaveManagement + "',";
                  SSQL = SSQL + "'" + TimeDelete + "','" + LeaveDetails + "','" + PermissionDetail + "','" + ManualAttendance + "','" + ManualShift + "',";
                  SSQL = SSQL + "'" + ManualShiftDetails + "','" + UploadDownload + "','"+ SalaryProcess +"','" + Deduction + "','" + Incentive + "','" + Report + "',";
                  SSQL = SSQL + "'" + DownloadClear + "','"+ CompanyMaster +"','" + SuperAdminCreation +"')";

                  objdata.ReturnMultipleValue(SSQL);

                  //objUsercreation.Ccode = SessionCcode.ToString();
                  //objUsercreation.Lcode = SessionLcode.ToString();
                  //objUsercreation.UserCode = ddlUserName.SelectedItem.Text;
                  //objUsercreation.Data = Dept.ToString();
                  //objUsercreation.AttendanceLogClear = Attn_Log_Clear.ToString();
                  //objUsercreation.ManualattendanceCheck = Man_Attn_Perm.ToString();
                  //objUsercreation.ManAttnUpload = Man_Attn_Upload_Perm.ToString();
                  //objUsercreation.EmpApproval = Employee_Approval_Perm.ToString();
                  //objUsercreation.EmpStatus = Employee_Status_Perm.ToString();
                  //objUsercreation.Emp_Mst_Data_Perm = Emp_Mst_Data_Perm.ToString();
                  //objdata.AdministrationRegistration(objUsercreation);
                  ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Save sucessFully');", true);
                  ddlUserName.SelectedIndex = 0;
              }
          }
      }

      protected void ddlUserName_SelectedIndexChanged(object sender, EventArgs e)
      {
          //Clear();

          Clear();

          DataTable dtd = new DataTable();
          SSQL = "select * from Rights where Username='"+ ddlUserName.SelectedItem.Text +"' and CompCode = '"+ SessionCcode +"' and LocCode='"+ SessionLcode +"'";
          dtd = objdata.ReturnMultipleValue(SSQL);
          if (dtd.Rows.Count > 0)
          {
              DashBoard = dtd.Rows[0]["DashBoard"].ToString();
              if (DashBoard.ToString() == "1")
              {
                  ChkDashBoard.Checked = true;
              }
                         
              
              masterpage = dtd.Rows[0]["masterpage"].ToString();
              if (masterpage.ToString() == "1")
              {
                  ChkMasterPage.Checked = true;
              }

              

              UserCreation = dtd.Rows[0]["UserCreation"].ToString();
              if (UserCreation.ToString() == "1")
              {
                  ChkUserCreation.Checked = true;
              }

              

              AdministrationRight = dtd.Rows[0]["AdministrationRights"].ToString();

              if (AdministrationRight.ToString() == "1")
              {
                  ChkAdministrationRights.Checked = true;
              }


              
              DepartmentDetails = dtd.Rows[0]["DepartmentDetails"].ToString();

              if (DepartmentDetails.ToString() == "1")
              {
                  ChkDepartmentDetails.Checked = true;
              }
              

              DesginationDetails = dtd.Rows[0]["DesginationDetails"].ToString();

              if (DesginationDetails.ToString() == "1")
              {
                  ChkDesginationDetails.Checked = true;
              }
              
              

              EmployeeTypeDetails = dtd.Rows[0]["EmployeeTypeDetails"].ToString();


              if (EmployeeTypeDetails.ToString() == "1")
              {
                  ChkEmployeeTypeDetails.Checked = true;
              }
              
              

              WagesTypeDetails = dtd.Rows[0]["WagesTypeDetails"].ToString();

              if (WagesTypeDetails.ToString() == "1")
              {
                  ChkWagesTypeDetails.Checked = true;
              }

              

              EmployeeDetails = dtd.Rows[0]["EmployeeDetails"].ToString();

              if (EmployeeDetails.ToString() == "1")
              {
                  ChkEmployeeDetails.Checked = true;
              }
              

              LeaveMaster = dtd.Rows[0]["LeaveMaster"].ToString();

              if (LeaveMaster.ToString() == "1")
              {
                  ChkLeaveMaster.Checked = true;
              }

              
              PermissionMaster = dtd.Rows[0]["PermissionMaster"].ToString();

              if (PermissionMaster.ToString() == "1")
              {
                  ChkPermissionMaster.Checked = true;
              }
              

              GraceTime = dtd.Rows[0]["GraceTime"].ToString();

              if (GraceTime.ToString() == "1")
              {
                  ChkGraceTime.Checked = true;
              }

              EarlyOutMaster = dtd.Rows[0]["EarlyOutMaster"].ToString();

              if (EarlyOutMaster.ToString() == "1")
              {
                  ChkEarlyOutMaster.Checked = true;
              }

              

              LateINMaster = dtd.Rows[0]["LateINMaster"].ToString();

              if (LateINMaster.ToString() == "1")
              {
                  ChkLateINMaster.Checked = true;
              }
              
              

              Employee = dtd.Rows[0]["Employee"].ToString();

              if (Employee.ToString() == "1")
              {
                  ChkEmployee.Checked = true;
              }
              
              
              EmployeeApproval = dtd.Rows[0]["EmployeeApproval"].ToString();

              if (EmployeeApproval.ToString() == "1")
              {
                  ChkEmployeeApproval.Checked = true;
              }
              

              EmployeeStatus = dtd.Rows[0]["EmployeeStatus"].ToString();

              if (EmployeeStatus.ToString() == "1")
              {
                  ChkEmployeeStatus.Checked = true;
              }
              

              LeaveManagement = dtd.Rows[0]["LeaveManagement"].ToString();

              if (LeaveManagement.ToString() == "1")
              {
                  ChkLeaveManagement.Checked = true;
              }

              TimeDelete = dtd.Rows[0]["TimeDelete"].ToString();

              if (TimeDelete.ToString() == "1")
              {
                  ChkTimeDelete.Checked = true;
              }
              
              

              LeaveDetails = dtd.Rows[0]["LeaveDetails"].ToString();

              if (LeaveDetails.ToString() == "1")
              {
                  ChkLeaveDetails.Checked = true;
              }
              

              PermissionDetail = dtd.Rows[0]["PermissionDetail"].ToString();

              if (PermissionDetail.ToString() == "1")
              {
                  ChkPermissionDetails.Checked = true;
              }

              string Manualattendance = dtd.Rows[0]["ManualAttendance"].ToString();
              if (Manualattendance.ToString() == "1")
              {
                  ChkManualAttendance.Checked = true;
              }
              

              ManualShift = dtd.Rows[0]["ManualShift"].ToString();

              if (ManualShift.ToString() == "1")
              {
                  ChkManualShift.Checked = true;
              }
              

              ManualShiftDetails = dtd.Rows[0]["ManualShiftDetails"].ToString();

              if (ManualShiftDetails.ToString() == "1")
              {
                  ChkManualShiftDetails.Checked = true;
              }

              UploadDownload = dtd.Rows[0]["UploadDownload"].ToString();

              if (UploadDownload.ToString() == "1")
              {
                  ChkUploadDownload.Checked = true;
              }


              SalaryProcess = dtd.Rows[0]["SalaryProcess"].ToString();

              if (SalaryProcess.ToString() == "1")
              {
                  ChkSalaryProcess.Checked = true;
              }
               
             
              Deduction = dtd.Rows[0]["Deduction"].ToString();

              if (Deduction.ToString() == "1")
              {
                  ChkDeduction.Checked = true;
              }
             

              Incentive = dtd.Rows[0]["Incentive"].ToString();

              if (Incentive.ToString() == "1")
              {
                  ChkIncentive.Checked = true;
              }
              
              Report = dtd.Rows[0]["Report"].ToString();

              if (Report.ToString() == "1")
              {
                  ChkReports.Checked = true;
              }
              DownloadClear = dtd.Rows[0]["DownloadClear"].ToString();

              if (DownloadClear.ToString() == "1")
              {
                  ChkDownloadClear.Checked = true;
              }

              

              //string ar = dtdAdmin.Rows[0]["Data"].ToString();
              //string[] arr = ar.Split(',');

              //if (arr[0].ToString() == "1")
              //{
              //    ChkBoxAdminRights.Checked = true;
              //}
              //if (arr[1].ToString() == "1")
              //{
              //    ChkBoxUserCreation.Checked = true;
              //}
              //if (arr[2].ToString() == "1")
              //{
              //    ChkBoxNewEmployee.Checked = true;
              //}
              //if (arr[3].ToString() == "1")
              //{
              //    ChkBoxEmployeeDetails.Checked = true;
              //}
              //if (arr[4].ToString() == "1")
              //{
              //    ChkBoxDownloadClear.Checked = true;
              //}
              //if (arr[5].ToString() == "1")
              //{
              //    ChkBoxEmployeeApproval.Checked = true;
              //}
              //if (arr[6].ToString() == "1")
              //{
              //    ChkBoxEmployeeStatus.Checked = true;
              //}
              //if (arr[7].ToString() == "1")
              //{
              //    ChkBoxManualAttendance.Checked = true;
              //}
              //if (arr[8].ToString() == "1")
              //{
              //    ChkBoxdeductionAndOT.Checked = true;
              //}
              //if (arr[9].ToString() == "1")
              //{
              //    ChkBoxDepartmentIncentive.Checked = true;
              //}
              //if (arr[10].ToString() == "1")
              //{
              //    ChkBoxSalaryCoverPhoto.Checked = true;
              //}
              //if (arr[11].ToString() == "1")
              //{
              //    ChkBoxSalaryDisbursement.Checked = true;
              //}
              //if (arr[12].ToString() == "1")
              //{
              //    ChkBoxTimeDelete.Checked = true;
              //}
              //if (arr[13].ToString() == "1")
              //{
              //    ChkBoxLeaveDetails.Checked = true;
              //}
              //if (arr[14].ToString() == "1")
              //{
              //    ChkBoxReport.Checked = true;
              //}
          }
      }

      protected void btnCancel_Click(object sender, EventArgs e)
      {
          Clear();
          ddlUserName.SelectedIndex = 0;
      }

      public void Clear()
      {
        
          //ChkBoxAdminRights.Checked = false;
          ChkAdministrationRights.Checked = false;
          ChkDashBoard.Checked = false;
          ChkDeduction.Checked = false;
          ChkDepartmentDetails.Checked = false;
          ChkDesginationDetails.Checked = false;

          ChkDownloadClear.Checked = false;
          ChkEarlyOutMaster.Checked = false;
          ChkEmployee.Checked = false;
          ChkEmployeeApproval.Checked = false;
          ChkEmployeeDetails.Checked = false;

          ChkEmployeeStatus.Checked = false;
          ChkEmployeeTypeDetails.Checked = false;
          ChkGraceTime.Checked = false;
          ChkIncentive.Checked = false;
          ChkSalaryProcess.Checked = false;
          ChkLateINMaster.Checked = false;

          ChkLeaveDetails.Checked = false;
          ChkLeaveManagement.Checked = false;
          ChkLeaveMaster.Checked = false;
          ChkManualAttendance.Checked = false;
          ChkManualShift.Checked = false;

          ChkManualShiftDetails.Checked = false;
          ChkMasterPage.Checked = false;
          ChkPermissionDetails.Checked = false;
          ChkPermissionMaster.Checked = false;
          ChkReports.Checked = false;

          ChkTimeDelete.Checked = false;
          ChkUploadDownload.Checked = false;
          ChkUserCreation.Checked = false;
          ChkWagesTypeDetails.Checked = false;
      }
}
