﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.OleDb;
using System.Data.SqlClient;


public partial class Upload_Download : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    string SSQL;
    bool ErrFlag = false;
    static int SNo;
    DataTable DataCell = new DataTable();
    DataTable mdatatable = new DataTable();
    System.Web.UI.WebControls.DataGrid grid =
                new System.Web.UI.WebControls.DataGrid();

    DateTime fromdate;
    DateTime todate;
    DataTable AutoDataTable = new DataTable();
    string Date1;
    string Date2;
    string Date3;
    string Date4;
    string Date5;
    string Date6;
    string Date7;
    string Date8;
    string Date9;
    string Date10;
    string Date11;
    string Date12;
    string Date13;
    string Date14;
    string Date15;
    string Date16;
    string Date17;
    string Date18;
    string Date19;
    string Date20;
    string Date21;
    string Date22;
    string Date23;
    string Date24;
    string Date25;
    string Date26;
    string Date27;
    string Date28;
    string Date29;
    string Date30;
    string Date31;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
       
         
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Upload Download";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("LeaveManagement"));
                li.Attributes.Add("class", "droplink active open");
            }
    }
    protected void btnDownload_Click(object sender, EventArgs e)
    {
        if (ddlCategory.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Employee Category');", true);
            ErrFlag = true;
        }
        else
        {
            DataTable dtdDownload = new DataTable();

            SSQL = "select MachineID,isnull(CatName,'') as Catgory,isnull(DeptName,'') as [DeptName]";
            SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' and CatName='" + ddlCategory.SelectedItem.Text + "'";
            SSQL = SSQL + " And  IsActive='Yes'";

            dtdDownload = objdata.ReturnMultipleValue(SSQL);

            fromdate = Convert.ToDateTime(txtfromdate.Text);
            todate = Convert.ToDateTime(txtToDate.Text);
            int dayCount3 = (int)((todate - fromdate).TotalDays) + 1;

            if (dtdDownload.Rows.Count > 0)
            {

                AutoDataTable.Columns.Add("MachineID");
                AutoDataTable.Columns.Add("Catgory");
                AutoDataTable.Columns.Add("DeptName");
                AutoDataTable.Columns.Add("FirstName");

                for (int i = 1; i <= dayCount3; i++)
                {
                    string columnname = "Date" + i;
                    AutoDataTable.Columns.Add(columnname);
                }


                Int32 DSVAL = 0;
                for (int i = 0; i < dtdDownload.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[DSVAL][0] = dtdDownload.Rows[i]["MachineID"].ToString();
                    AutoDataTable.Rows[DSVAL][1] = dtdDownload.Rows[i]["Catgory"].ToString();
                    AutoDataTable.Rows[DSVAL][2] = dtdDownload.Rows[i]["DeptName"].ToString();
                    AutoDataTable.Rows[DSVAL][3] = dtdDownload.Rows[i]["FirstName"].ToString();

                    DSVAL += 1;

                }
            }
        }

        DisplayExcel();
    }

    public void DisplayExcel()
    {
        grid.DataSource = AutoDataTable;
        grid.DataBind();
        string attachment = "attachment;filename=SHIFT REPORT BETWEEN DATES.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);
        //Response.Write("<table>");
        //Response.Write("<tr Font-Bold='true' align='center'>");
        //Response.Write("<td colspan='10'>");
        //Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
        //Response.Write("--");
        //Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        //Response.Write("<tr Font-Bold='true' align='center'>");
        //Response.Write("<td colspan='10'>");
        //Response.Write("<a style=\"font-weight:bold\">SHIFT REPORT BETWEEN DATES &nbsp;&nbsp;&nbsp;</a>");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        //Response.Write("<tr Font-Bold='true' align='center'>");
        //Response.Write("<td colspan='10'>");
        //Response.Write("<a style=\"font-weight:bold\"> FROM -" + txtfromdate.Text + "</a>");
        //Response.Write("&nbsp;&nbsp;&nbsp;");
        //Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        //Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
        //string attachment = "attachment;filename=ShiftMaster.xls";
        //Response.ClearContent();
        //Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", "Sheet1.xls"));
        //Response.ContentType = "application/ms-excel";
        //System.IO.StringWriter stw = new System.IO.StringWriter();
        ////StringWriter stw = new StringWriter();
        //HtmlTextWriter htextw = new HtmlTextWriter(stw);
        //grid.RenderControl(htextw);
        ////gvDownload.RenderControl(htextw);
        ////Response.Write("Contract Details");
        //Response.Write(stw.ToString());
        //Response.End();
        //Response.Clear();
       }



    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {
            if (fileUpload.HasFile)
            {
                fileUpload.SaveAs(Server.MapPath("Upload/" + fileUpload.FileName));

                string fname = fileUpload.FileName;

                string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + fileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                DataTable dts = new DataTable();
                using (sSourceConnection)
                {
                    sSourceConnection.Open();
                    OleDbCommand command = new OleDbCommand("Select MachineID,DeptName,FirstName,Months,Year,Date1,Date2,Date3,Date4,Date5,Date6,Date7,Date8,Date9,Date10,Date11,Date12,Date13,Date14,Date15,Date16,Date17,Date18,Date19,Date20,Date21,Date22,Date23,Date24,Date25,Date26,Date27,Date28,Date29,Date30,Date31 FROM [Sheet1$];", sSourceConnection);
                    sSourceConnection.Close();

                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        command.CommandText = "Select * FROM [Sheet1$]";
                       // command.CommandText = "Select * FROM ['"+ fname +"'$]";

                        command.CommandText = "Select * FROM Sheet1";
                        sSourceConnection.Open();
                    }
                    using (OleDbDataReader dr = command.ExecuteReader())
                    {

                        if (dr.HasRows)
                        {

                        }

                    }
                    OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                    objDataAdapter.SelectCommand = command;
                    DataSet ds = new DataSet();

                    objDataAdapter.Fill(ds);
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                    }
                    string constr = ConfigurationManager.AppSettings["ConnectionString"];

                    for (int j = 0; j < dt.Rows.Count; j++)
                    {

                        SqlConnection cn = new SqlConnection(constr);
                        //string EmpNo = dt.Rows[j]["EmpNo"].ToString();
                        string MachineID = dt.Rows[j]["MachineID"].ToString();
                        string DeptName = dt.Rows[j]["DeptName"].ToString();
                        string FirstName = dt.Rows[j]["FirstName"].ToString();
                        string Months = dt.Rows[j]["Months"].ToString();
                        string Year = dt.Rows[j]["Year"].ToString();
                        string Date1 = dt.Rows[j]["Date1"].ToString();
                        string Date2 = dt.Rows[j]["Date2"].ToString();
                        string Date3 = dt.Rows[j]["Date3"].ToString();
                        string Date4 = dt.Rows[j]["Date4"].ToString();
                        string Date5 = dt.Rows[j]["Date5"].ToString();
                        string Date6 = dt.Rows[j]["Date6"].ToString();
                        string Date7 = dt.Rows[j]["Date7"].ToString();
                        string Date8 = dt.Rows[j]["Date8"].ToString();
                        string Date9 = dt.Rows[j]["Date9"].ToString();
                        string Date10 = dt.Rows[j]["Date10"].ToString();
                        string Date11 = dt.Rows[j]["Date11"].ToString();
                        string Date12 = dt.Rows[j]["Date12"].ToString();
                        string Date13 = dt.Rows[j]["Date13"].ToString();
                        string Date14 = dt.Rows[j]["Date14"].ToString();
                        string Date15 = dt.Rows[j]["Date15"].ToString();
                        string Date16 = dt.Rows[j]["Date16"].ToString();
                        string Date17 = dt.Rows[j]["Date17"].ToString();
                        string Date18 = dt.Rows[j]["Date18"].ToString();
                        string Date19 = dt.Rows[j]["Date19"].ToString();
                        string Date20 = dt.Rows[j]["Date20"].ToString();
                        string Date21 = dt.Rows[j]["Date21"].ToString();
                        string Date22 = dt.Rows[j]["Date22"].ToString();
                        string Date23 = dt.Rows[j]["Date23"].ToString();
                        string Date24 = dt.Rows[j]["Date24"].ToString();
                        string Date25 = dt.Rows[j]["Date25"].ToString();
                        string Date26 = dt.Rows[j]["Date26"].ToString();
                        string Date27 = dt.Rows[j]["Date27"].ToString();
                        string Date28 = dt.Rows[j]["Date28"].ToString();
                        string Date29 = dt.Rows[j]["Date29"].ToString();
                        string Date30 = dt.Rows[j]["Date30"].ToString();
                        string Date31 = dt.Rows[j]["Date31"].ToString();

                        //string EMP = "";
                        //if (EmpName == "")
                        //{
                        //    j = j + 2;
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Employee Name. The Row Number is " + j + "');", true);
                        //    //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //    ErrFlag = true;
                        //}
                       
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }

    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        if (ddlCategory.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Employee Category');", true);
            ErrFlag = true;
        }
        else
        {
            string ss1 = txtfromdate.Text;
            DateTime da = Convert.ToDateTime(ss1.ToString());
            string Months = da.ToString("MMMM");

            DataCell.Columns.Add("MachineID");
            DataCell.Columns.Add("DeptName");
            DataCell.Columns.Add("FirstName");
            DataCell.Columns.Add("Months");
            DataCell.Columns.Add("Year");
            DataCell.Columns.Add("Date1");
            DataCell.Columns.Add("Date2");
            DataCell.Columns.Add("Date3");
            DataCell.Columns.Add("Date4");
            DataCell.Columns.Add("Date5");
            DataCell.Columns.Add("Date6");
            DataCell.Columns.Add("Date7");
            DataCell.Columns.Add("Date8");
            DataCell.Columns.Add("Date9");
            DataCell.Columns.Add("Date10");
            DataCell.Columns.Add("Date11");
            DataCell.Columns.Add("Date12");
            DataCell.Columns.Add("Date13");
            DataCell.Columns.Add("Date14");
            DataCell.Columns.Add("Date15");
            DataCell.Columns.Add("Date16");
            DataCell.Columns.Add("Date17");
            DataCell.Columns.Add("Date18");
            DataCell.Columns.Add("Date19");
            DataCell.Columns.Add("Date20");
            DataCell.Columns.Add("Date21");
            DataCell.Columns.Add("Date22");
            DataCell.Columns.Add("Date23");
            DataCell.Columns.Add("Date24");
            DataCell.Columns.Add("Date25");
            DataCell.Columns.Add("Date26");
            DataCell.Columns.Add("Date27");
            DataCell.Columns.Add("Date28");
            DataCell.Columns.Add("Date29");
            DataCell.Columns.Add("Date30");
            DataCell.Columns.Add("Date31");

            SSQL = SSQL + " select MachineID,DeptName,FirstName,Months,Year,Date1,";
            SSQL = SSQL + " Date2,Date3,Date4,Date5,Date6,Date7,Date8,Date9,Date10,";
            SSQL = SSQL + " Date11,Date12,Date13,Date14,Date15,Date16,Date17,Date18,Date19,Date20,";
            SSQL = SSQL + " Date21,Date22,Date23,Date24,Date25,Date26,Date27,Date28,Date29,isnull(Date30,'') as Date30,";
            SSQL = SSQL + " isnull(Date31,'') as Date31 from MstShifUpload  Where Ccode='" + SessionCcode + "' and EmpCatgory='" + ddlCategory.SelectedItem.Text + "' and Months='" + Months + "'";

            mdatatable = objdata.ReturnMultipleValue(SSQL);

            if (mdatatable.Rows.Count > 0)
            {
                for (int i = 0; i < mdatatable.Rows.Count; i++)
                {
                    DataCell.NewRow();
                    DataCell.Rows.Add();

                    DataCell.Rows[i]["MachineID"] = mdatatable.Rows[i]["MachineID"].ToString();
                    DataCell.Rows[i]["DeptName"] = mdatatable.Rows[i]["DeptName"].ToString();
                    DataCell.Rows[i]["FirstName"] = mdatatable.Rows[i]["FirstName"].ToString();
                    DataCell.Rows[i]["Months"] = mdatatable.Rows[i]["Months"].ToString();
                    DataCell.Rows[i]["Year"] = mdatatable.Rows[i]["Year"].ToString();
                    DataCell.Rows[i]["Date1"] = mdatatable.Rows[i]["Date1"].ToString();
                    DataCell.Rows[i]["Date2"] = mdatatable.Rows[i]["Date2"].ToString();
                    DataCell.Rows[i]["Date3"] = mdatatable.Rows[i]["Date3"].ToString();
                    DataCell.Rows[i]["Date4"] = mdatatable.Rows[i]["Date4"].ToString();
                    DataCell.Rows[i]["Date5"] = mdatatable.Rows[i]["Date5"].ToString();
                    DataCell.Rows[i]["Date6"] = mdatatable.Rows[i]["Date6"].ToString();
                    DataCell.Rows[i]["Date7"] = mdatatable.Rows[i]["Date7"].ToString();
                    DataCell.Rows[i]["Date8"] = mdatatable.Rows[i]["Date8"].ToString();
                    DataCell.Rows[i]["Date9"] = mdatatable.Rows[i]["Date9"].ToString();
                    DataCell.Rows[i]["Date10"] = mdatatable.Rows[i]["Date10"].ToString();
                    DataCell.Rows[i]["Date11"] = mdatatable.Rows[i]["Date11"].ToString();
                    DataCell.Rows[i]["Date12"] = mdatatable.Rows[i]["Date12"].ToString();
                    DataCell.Rows[i]["Date13"] = mdatatable.Rows[i]["Date13"].ToString();
                    DataCell.Rows[i]["Date14"] = mdatatable.Rows[i]["Date14"].ToString();
                    DataCell.Rows[i]["Date15"] = mdatatable.Rows[i]["Date15"].ToString();
                    DataCell.Rows[i]["Date16"] = mdatatable.Rows[i]["Date16"].ToString();
                    DataCell.Rows[i]["Date17"] = mdatatable.Rows[i]["Date17"].ToString();
                    DataCell.Rows[i]["Date18"] = mdatatable.Rows[i]["Date18"].ToString();
                    DataCell.Rows[i]["Date19"] = mdatatable.Rows[i]["Date19"].ToString();
                    DataCell.Rows[i]["Date20"] = mdatatable.Rows[i]["Date20"].ToString();
                    DataCell.Rows[i]["Date21"] = mdatatable.Rows[i]["Date21"].ToString();
                    DataCell.Rows[i]["Date22"] = mdatatable.Rows[i]["Date22"].ToString();
                    DataCell.Rows[i]["Date23"] = mdatatable.Rows[i]["Date23"].ToString();
                    DataCell.Rows[i]["Date24"] = mdatatable.Rows[i]["Date24"].ToString();
                    DataCell.Rows[i]["Date25"] = mdatatable.Rows[i]["Date25"].ToString();
                    DataCell.Rows[i]["Date26"] = mdatatable.Rows[i]["Date26"].ToString();
                    DataCell.Rows[i]["Date27"] = mdatatable.Rows[i]["Date27"].ToString();
                    DataCell.Rows[i]["Date28"] = mdatatable.Rows[i]["Date28"].ToString();
                    DataCell.Rows[i]["Date29"] = mdatatable.Rows[i]["Date29"].ToString();
                    DataCell.Rows[i]["Date30"] = mdatatable.Rows[i]["Date30"].ToString();
                    DataCell.Rows[i]["Date31"] = mdatatable.Rows[i]["Date31"].ToString();
                }
            }
            grid.DataSource = DataCell;
            grid.DataBind();
            string attachment = "attachment;filename=SHIFT REPORT BETWEEN DATES.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">SHIFT REPORT BETWEEN DATES &nbsp;&nbsp;&nbsp;</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\"> FROM -" + txtfromdate.Text + "</a>");
            Response.Write("&nbsp;&nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }

    }




    protected void Btupload_Click(object sender, EventArgs e)
    {
        string ss1 = txtfromdate.Text;
        DateTime da = Convert.ToDateTime(ss1.ToString());
        string UploadMonth = da.ToString("MMMM");
        string UploadYear = da.Year.ToString();
        DataTable dsEmployee = new DataTable();
        string Emp_Category = ddlCategory.SelectedItem.Text;
        SSQL = "select Year,Months from MstShifUpload where Months='" + UploadMonth + "' and Year='" + UploadYear + "' and  EmpCatgory='" + Emp_Category + "'";
        dsEmployee = objdata.ReturnMultipleValue(SSQL);

        if (dsEmployee.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Already Exist For This Month and Year');", true);
            ErrFlag = true;
        }
        else
        {


        //SSQL = "";

        string connectionString ="";
        if (fileUpload.HasFile)
        {
            string fileName = Server.MapPath(fileUpload.PostedFile.FileName);
            string fileExtension = System.IO.Path.GetExtension(this.fileUpload.PostedFile.FileName);
            string fname = fileUpload.FileName;
            string fileLocation = Server.MapPath("Upload/" + fname);
            fileUpload.SaveAs(Server.MapPath("Upload/" + fileUpload.FileName));

            //Check whether file extension is xls or xslx

            if (fileExtension == ".xls")
            {
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\""; 
            }
            else if (fileExtension == ".xlsx")
            {
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            }

            //Create OleDB Connection and OleDb Command

            OleDbConnection con = new OleDbConnection(connectionString);
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Connection = con;
            OleDbDataAdapter dAdapter = new OleDbDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            DataTable dtExcelSheetName = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            string getExcelSheetName = dtExcelSheetName.Rows[0]["Table_Name"].ToString();
            cmd.CommandText = "SELECT * FROM [" + getExcelSheetName +"]";
            dAdapter.SelectCommand = cmd;
            dAdapter.Fill(dt);
            con.Close();

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                string MachineID = dt.Rows[j]["MachineID"].ToString();
                string DeptName = dt.Rows[j]["DeptName"].ToString();
                string FirstName = dt.Rows[j]["FirstName"].ToString();
                //string  = dt.Rows[j]["Months"].ToString();
                //string Year = dt.Rows[j]["Year"].ToString();
                if (dt.Columns.Contains("Date1"))
                {
                   Date1 = dt.Rows[j]["Date1"].ToString();
                }
                if (dt.Columns.Contains("Date2"))
                {
                    Date2 = dt.Rows[j]["Date2"].ToString();
                }
                if (dt.Columns.Contains("Date3"))
                {
                     Date3 = dt.Rows[j]["Date3"].ToString();
                }
                if (dt.Columns.Contains("Date4"))
                {
                    Date4 = dt.Rows[j]["Date4"].ToString();
                }
                if (dt.Columns.Contains("Date5"))
                {
                     Date5 = dt.Rows[j]["Date5"].ToString();
                }
                if (dt.Columns.Contains("Date6"))
                {
                     Date6 = dt.Rows[j]["Date6"].ToString();
                }
                if (dt.Columns.Contains("Date7"))
                {
                     Date7 = dt.Rows[j]["Date7"].ToString();
                }
                if (dt.Columns.Contains("Date8"))
                {
                   Date8 = dt.Rows[j]["Date8"].ToString();
                }
                if (dt.Columns.Contains("Date9"))
                {
                     Date9 = dt.Rows[j]["Date9"].ToString();
                }
                if (dt.Columns.Contains("Date10"))
                {
                     Date10 = dt.Rows[j]["Date10"].ToString();
                }

                if (dt.Columns.Contains("Date11"))
                {
                    Date11 = dt.Rows[j]["Date11"].ToString();
                }

                if (dt.Columns.Contains("Date12"))
                {
                    Date12 = dt.Rows[j]["Date12"].ToString();
                }

                if (dt.Columns.Contains("Date13"))
                {
                    Date13 = dt.Rows[j]["Date13"].ToString();
                }

                if (dt.Columns.Contains("Date14"))
                {
                     Date14 = dt.Rows[j]["Date14"].ToString();
                }

                if (dt.Columns.Contains("Date15"))
                {
                     Date15 = dt.Rows[j]["Date15"].ToString();
                }
                if (dt.Columns.Contains("Date16"))
                {
                     Date16 = dt.Rows[j]["Date16"].ToString();
                }
                if (dt.Columns.Contains("Date17"))
                {
                    Date17 = dt.Rows[j]["Date17"].ToString();
                }
                if (dt.Columns.Contains("Date18"))
                {
                     Date18 = dt.Rows[j]["Date18"].ToString();
                }
                if (dt.Columns.Contains("Date19"))
                {
                     Date19 = dt.Rows[j]["Date19"].ToString();
                }

                if (dt.Columns.Contains("Date20"))
                {
                    Date20 = dt.Rows[j]["Date20"].ToString();
                }
                if (dt.Columns.Contains("Date21"))
                {
                    Date21 = dt.Rows[j]["Date21"].ToString();
                }
                if (dt.Columns.Contains("Date22"))
                {
                     Date22 = dt.Rows[j]["Date22"].ToString();
                }
                if (dt.Columns.Contains("Date23"))
                {
                     Date23 = dt.Rows[j]["Date23"].ToString();
                }
                if (dt.Columns.Contains("Date24"))
                {
                     Date24 = dt.Rows[j]["Date24"].ToString();
                }
                if (dt.Columns.Contains("Date25"))
                {
                     Date25 = dt.Rows[j]["Date25"].ToString();
                }
                if (dt.Columns.Contains("Date26"))
                {
                     Date26 = dt.Rows[j]["Date26"].ToString();
                }
                if (dt.Columns.Contains("Date27"))
                {
                     Date27 = dt.Rows[j]["Date27"].ToString();
                }
                if (dt.Columns.Contains("Date28"))
                {
                    Date28 = dt.Rows[j]["Date28"].ToString();
                }
                if (dt.Columns.Contains("Date29"))
                {
                    Date29 = dt.Rows[j]["Date29"].ToString();
                }
                if (dt.Columns.Contains("Date30"))
                {
                     Date30 = dt.Rows[j]["Date30"].ToString();
                }

                if (dt.Columns.Contains("Date31"))
                {
                    Date31 = dt.Rows[j]["Date31"].ToString();
                }

                    SSQL = "insert into MstShifUpload(MachineID,CatName,DeptName,EmpCatgory,FirstName,Ccode,";
                    SSQL = SSQL + "FromDate,ToDate,Year,Months,Date1,Date2,";
                    SSQL = SSQL + "Date3,Date4,Date5,Date6,Date7,Date8,Date9,Date10,Date11,Date12,Date13,Date14,Date15,";
                    SSQL = SSQL + " Date16,Date17,Date18,Date19,Date20,Date21,Date22,Date23,Date24,Date25,Date26,Date27,Date28,Date29,Date30)";
                    SSQL = SSQL + "values('" + MachineID + "','" + Emp_Category + "',";
                    SSQL = SSQL + "'" + DeptName + "','" + Emp_Category + "','" + FirstName + "','" + SessionCcode + "','" + txtfromdate.Text + "','" + txtToDate.Text + "','" + UploadYear + "','" + UploadMonth + "','" + Date1 + "','" + Date2 + "','" + Date3 + "',";
                    SSQL = SSQL + "'" + Date4 + "','" + Date5 + "','" + Date6 + "','" + Date7 + "','" + Date8 + "',";
                    SSQL = SSQL + "'" + Date9 + "','" + Date10 + "','" + Date11 + "','" + Date12 + "','" + Date13 + "',";
                    SSQL = SSQL + "'" + Date14 + "','" + Date15 + "','" + Date16 + "','" + Date17 + "','" + Date18 + "',";
                    SSQL = SSQL + "'" + Date19 + "','" + Date20 + "','" + Date21 + "','" + Date22 + "','" + Date23 + "',";
                    SSQL = SSQL + "'" + Date24 + "','" + Date25 + "','" + Date26 + "','" + Date27 + "','" + Date28 + "',";
                    SSQL = SSQL + "'" + Date29 + "','" + Date30 + "','" + Date31 + "')";

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('File Upload Successfully);", true);
                    ErrFlag = true;
                
              }

            }
          
        }
    }
    
}
