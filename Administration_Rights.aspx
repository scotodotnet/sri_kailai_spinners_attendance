﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Administration_Rights.aspx.cs" Inherits="AdministrationRights" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate>

 <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">Administration Rights</li></h4> 
                    </ol>
                </div>
   
 <div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
     
        
    
    <div class="page-inner">
   
                                       
            
            <div class="col-md-9">
			<div class="panel panel-white">
			         <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Administration Rights</h4>
				</div>
				</div>
				     <form class="form-horizontal">
				       <div class="panel-body">
					
					<%--<div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Company<span class="mandatory">*</span></label>
						<div class="col-sm-4">
						       <asp:DropDownList ID="ddlCompany" runat="server" class="form-control" AutoPostBack="true">
                               </asp:DropDownList> 
				        </div>
                      <div class="col-md-1"></div> 
						
				        <label for="input-Default" class="col-sm-2 control-label">Location<span class="mandatory">*</span></label>
						<div class="col-sm-4">
                              <asp:DropDownList ID="ddlLocation" runat="server" class="form-control" AutoPostBack="true">
                              </asp:DropDownList>
					   </div>
                   </div>--%>
                                   
                   <%-- <div class="col-md-1"></div> --%>                   
					<div class="form-group row">
						 <label for="input-Default" class="col-sm-2 control-label">User Name</label>
							<div class="col-sm-4">
                               <asp:DropDownList ID="ddlUserName" runat="server" class="form-control" 
                                    AutoPostBack="True" onselectedindexchanged="ddlUserName_SelectedIndexChanged">
                               </asp:DropDownList>
							</div>
					  </div>
					  
					  
					   <%--User Rights start--%> 
					   
					  <div class="col-sm-12"  style="padding: 0 1.4em 1.4em 1.4em !important;   border: 1px solid #e9e9e9 !important;">
					     <h4>User Rights</h4>
					     <br />
                         <div class="form-group row">
    					 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 <asp:CheckBox ID="ChkDashBoard" runat="server" /> DashBoard
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkPermissionMaster" runat="server" />  Permission Master 
					
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 <asp:CheckBox ID="ChkPermissionDetails" runat="server" />  Permission Detail 
					
						 </div>
						 </div>  
                         
                         <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
					      <asp:CheckBox ID="ChkMasterPage" runat="server" />  Master Page
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkGraceTime" runat="server" /> Grace Time 
					     </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						   <asp:CheckBox ID="ChkManualShift" runat="server" />  Manual Entry 
						 </div>
						 </div>
						 
						 <div class="form-group row">
					   
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 <asp:CheckBox ID="ChkUserCreation" runat="server" />  User Creation 	
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 <asp:CheckBox ID="ChkEarlyOutMaster" runat="server" />   EarlyOut Master 
						 </div>
						 <div class="col-sm-1"></div>
					     <div class="col-sm-3">
						  <asp:CheckBox ID="ChkManualAttendance" runat="server" />  Manual Attendance
						 </div>
						 </div>
						 
						 
						 <div class="form-group row">
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
					       <asp:CheckBox ID="ChkAdministrationRights" runat="server" /> Administration Rights 
						 </div>
						 
						 <div class="col-sm-1"></div>
					     <div class="col-sm-3">
					      <asp:CheckBox ID="ChkLateINMaster" runat="server" />  LateIN Master  
						
						 </div>
						
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 <asp:CheckBox ID="ChkManualShiftDetails" runat="server" />  ManualShift Details 
						  
						 
						 </div>
						 </div>
						
						 <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
						 <asp:CheckBox ID="ChkDepartmentDetails" runat="server" />  Department Details 
						 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkEmployee" runat="server" />  Employee
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 <asp:CheckBox ID="ChkUploadDownload" runat="server" />  Upload Download 
						 
						
						 </div>
						 </div>
						 
						 
						 
						 <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
						 <asp:CheckBox ID="ChkDesginationDetails" runat="server" />  Desgination Details
						 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkEmployeeApproval" runat="server" />  Employee Approval 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkSalaryProcess" runat="server" />  SalaryProcess
						
						
						
						 </div>
						 </div>
						 
						 <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
						 <asp:CheckBox ID="ChkEmployeeTypeDetails" runat="server" />  EmployeeType Details
						 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkEmployeeStatus" runat="server" />  Employee Status 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkIncentive" runat="server" />  Incentive 
						
						
						 </div>
						 </div>
						 
					     <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
						 <asp:CheckBox ID="ChkWagesTypeDetails" runat="server" />  WagesType Details
						 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkLeaveManagement" runat="server" /> Leave Management 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 
						 <asp:CheckBox ID="ChkDeduction" runat="server" />  Deduction 
						
						 </div>
						 </div>
					
						 <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
						 <asp:CheckBox ID="ChkEmployeeDetails" runat="server" />  Employee Details
						 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkTimeDelete" runat="server" /> Time Delete 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkReports" runat="server" />  Reports 
						
						 </div>
						 </div>
						 
						 <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
						 <asp:CheckBox ID="ChkLeaveMaster" runat="server" />  Leave Master
						 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkLeaveDetails" runat="server" />  Leave Details 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkDownloadClear" runat="server" />  Download Clear
						 </div>
						 </div>
						 </div>
						 
					 
					 
					       
						
						<!-- Button start -->
						<div class="form-group row">
						</div>
                            <div class="txtcenter">
                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                    onclick="btnSave_Click"/>
                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                    <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" 
                                    onclick="btnCancel_Click" />
                    </div>
                    <!-- Button End -->	
                         </div>

				</form>
			
			</div><!-- panel white end -->
		   
		    
		    <div class="col-md-2"></div>
		    </div> 
            
      

                         <!-- Dashboard start -->
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- Dashboard End -->   
                        </div>
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>
 
 </ContentTemplate>
                 <Triggers>
                     <asp:PostBackTrigger ControlID="ddlUserName"  />
                 </Triggers>
 
 </asp:UpdatePanel>
 
 
</asp:Content>

