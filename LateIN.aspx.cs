﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Collections.Generic;



public partial class LateIN : System.Web.UI.Page
{

    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    BALDataAccess objdata = new BALDataAccess();
  
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string SSQL;
    DataTable mEmployeeDS = new DataTable();
    DataTable AutoDataTable = new DataTable();
    DataTable mDataTable = new DataTable();
    DataSet ds = new DataSet();
    DateTime date1 = new DateTime();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();



            ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            ddlShiftType = Request.QueryString["ddlShiftType"].ToString();
        }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (mReportName.Text == "LATE IN")
        {
         ResponseHelper.Redirect("LateInReport.aspx?ModeType=" + ddlMode.SelectedItem.Text + "&Date=" + Txtfrom.Text, "_blank", "");
        }
    }




    protected void btnNext1_Click(object sender, EventArgs e)
    {
        if (mReportName.Text == "LATE IN")
        {
            ResponseHelper.Redirect("DayAttendanceSummary.aspx?ModeType=" + ddlMode.SelectedItem.Text + "&Date=" + Txtfrom.Text + "&Date2=" + TxtTo.Text, "_blank", "");
        }
    }


    protected void btnNext2_Click(object sender, EventArgs e)
    {
        if (btnNext2.Text == "EMPLOYEE MASTER")
        {
            ResponseHelper.Redirect("EmployeeMaster.aspx?Shift=" + ddlshifttype.SelectedItem.Text + "&Date=" + Txtfrom.Text + "&Date2=" + TxtTo.Text + "&CompanyCode=" + SessionCcode.ToString() + "&LocationCode=" + SessionLcode.ToString(), "_blank", "");
          
        }
    }

    protected void btnNext3_Click(object sender, EventArgs e)
    {
        if (btnNext3.Text == "EMPLOYEE PROFILE")
        {
            ResponseHelper.Redirect("EmployeeProfile.aspx?Shift=" + ddlshifttype.SelectedItem.Text + "&Date=" + Txtfrom.Text + "&Date2=" + TxtTo.Text + "&CompanyCode=" + SessionCcode.ToString() + "&LocationCode=" + SessionLcode.ToString(), "_blank", "");

        }
    }

    protected void btnNext4_Click(object sender, EventArgs e)
    {
        if (btnNext4.Text == "EMPLOYEE FULL PROFILE")
        {
            ResponseHelper.Redirect("EmployeeFull.aspx?Shift=" + ddlshifttype.SelectedItem.Text + "&Date=" + Txtfrom.Text + "&Date2=" + TxtTo.Text + "&Empcode=" + txtEmpcode.Text + "&CompanyCode=" + SessionCcode.ToString() + "&LocationCode=" + SessionLcode.ToString(), "_blank", "");

        }
    }



    protected void Unnamed1_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("Earlyout.aspx?ModeType=" + ddlMode.SelectedItem.Text + "&Date=" + Txtfrom.Text, "_blank", "");
    }

    protected void btnDayShiftWise_Click(object sender, EventArgs e)
    {
        //ResponseHelper.Redirect("DayAttendanceShiftWise.aspx?ModeType=" + ddlMode.SelectedItem.Text + "&ShiftType=" + ddlShift.SelectedItem.Text + "&Date=" + Txtfrom.Text, "_blanl", "");
        ResponseHelper.Redirect("AbsentReportDaywise.aspx?FromDate=" + Txtfrom.Text, "_blank", "");
    }
    protected void btnLongAbsent_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("EmployeeLongAbsentReport.aspx?FromDate=" + Txtfrom.Text + "&ToDate=" + TxtTo.Text + "&WagesType=" + ddlwagesType.SelectedItem.Text + "&LeaveDays=" + txtLeavedays.Text, "_blank", "");
    }
}




  



           




    

   

