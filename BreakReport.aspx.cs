﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using CrystalDecisions.Web;

public partial class BreakReport : System.Web.UI.Page
{

    private DataTable AutoDataTable = new DataTable();
   
    private DataTable DataCells = new DataTable();
    private string Date_Value_Str;
    private string Date_Value_Str1;
    private DateTime date1;
    private string Date1;
    private DateTime date2;
    private string Date2;
    private DateTime date3;
    private DateTime date4;
    private DateTime date5;
    private DateTime date6;
    private DateTime date7;
    private DateTime date8;
    private DataTable dsEmployee = new DataTable();
  
    private DateTime fromdate;
    private DataGrid GridView1 = new DataGrid();
    private int Heading;
    private int Heading_S = 3;
    private string Machine_ID_Str;
    private DataTable mEmployeeDS = new DataTable();
    private string mIpAddress_IN;
    private string mIpAddress_OUT;
    private DataTable mLocalDS = new DataTable();
    private DataTable mLocalDS1 = new DataTable();
    private string ModeType;
    private Altius.BusinessAccessLayer.BALDataAccess.BALDataAccess objdata = new Altius.BusinessAccessLayer.BALDataAccess.BALDataAccess();
    private string SessionAdmin;
    private string SessionCcode;
    private string SessionCompanyName;
    private string SessionLcode;
    private string SessionLocationName;
    private string SessionUserType;
    private int shiftCount;
    private string SSQL;
    private string SSQL_OUT;
    private DateTime todate;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.SessionAdmin == "2")
        {
            base.Response.Redirect("DayAttendanceSummary.aspx");
        }
        if (!base.IsPostBack)
        {
            this.Page.Title = "Spay Module | Report-Day Attendance Summary";
        }
        this.SessionCcode = this.Session["Ccode"].ToString();
        this.SessionLcode = this.Session["Lcode"].ToString();
        this.SessionAdmin = this.Session["Isadmin"].ToString();
        this.SessionCompanyName = this.Session["CompanyName"].ToString();
        this.SessionLocationName = this.Session["LocationName"].ToString();
        this.SessionUserType = this.Session["UserType"].ToString();
        this.Date1 = base.Request.QueryString["Date"].ToString();
        DataTable table = new DataTable();
        table = this.objdata.IPAddressForAll(this.SessionCcode.ToString(), this.SessionLcode.ToString());
        if (table.Rows.Count > 0)
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (table.Rows[i]["IPMode"].ToString() == "IN")
                {
                    this.mIpAddress_IN = table.Rows[i]["IPAddress"].ToString();
                }
                else if (table.Rows[i]["IPMode"].ToString() == "OUT")
                {
                    this.mIpAddress_OUT = table.Rows[i]["IPAddress"].ToString();
                }
            }
        }
        this.Fill_Multi_timeIN();
        this.Write_MultiIN();
        this.GridView1.HeaderStyle.Font.Bold = true;
        this.GridView1.DataSource = this.DataCells;
        this.GridView1.DataBind();
        string str = "attachment;filename=DAY ATTENDANCE SUMMARY.xls";
        base.Response.ClearContent();
        base.Response.AddHeader("content-disposition", str);
        base.Response.ContentType = "application/ms-excel";
        StringWriter writer = new StringWriter();
        HtmlTextWriter writer2 = new HtmlTextWriter(writer);
        this.GridView1.RenderControl(writer2);
        base.Response.Write("<table>");
        base.Response.Write("<tr Font-Bold='true' align='center'>");
        base.Response.Write("<td font-Bold='true' colspan='12'>");
        base.Response.Write("<a style=\"font-weight:bold\">" + this.SessionCompanyName + "</a>");
        base.Response.Write("--");
        base.Response.Write("<a style=\"font-weight:bold\">" + this.SessionLocationName + "</a>");
        base.Response.Write("</td>");
        base.Response.Write("</tr>");
        base.Response.Write("<tr Font-Bold='true' align='center'>");
        base.Response.Write("<td font-Bold='true' colspan='12'>");
        base.Response.Write("<a style=\"font-weight:bold\"> Break Time &nbsp;&nbsp;&nbsp;" + this.Date1 + "</a>");
        base.Response.Write("</td>");
        base.Response.Write("</tr>");
        base.Response.Write("<td style='background-color:lightgrey'></td>");
        base.Response.Write("</table>");
        base.Response.Write(writer.ToString());
        base.Response.End();
        base.Response.Clear();
    }
    public void Write_MultiIN()
    {
        try
        {
            int num = 1;
            int num2 = 1;
            int num3 = 0;
            this.DataCells.Columns.Add("SNo");
            this.DataCells.Columns.Add("DeptName");
            this.DataCells.Columns.Add("EmpNo");
            this.DataCells.Columns.Add("ExistingCode");
            this.DataCells.Columns.Add("FirstName");
            this.DataCells.Columns.Add("TimeOUT1");
            this.DataCells.Columns.Add("TimeIN1");
            this.DataCells.Columns.Add("TOTAL1");
            this.DataCells.Columns.Add("TimeOUT2");
            this.DataCells.Columns.Add("TimeIN2");
            this.DataCells.Columns.Add("TOTAL2");
            this.DataCells.Columns.Add("TimeOUT3");
            this.DataCells.Columns.Add("TimeIN3");
            this.DataCells.Columns.Add("TOTAL3");
            this.DataCells.Columns.Add("TimeOUT4");
            this.DataCells.Columns.Add("TimeIN4");
            this.DataCells.Columns.Add("TOTAL4");

            for (num3 = 0; num3 < this.AutoDataTable.Rows.Count; num3++)
            {
                this.DataCells.NewRow();
                this.DataCells.Rows.Add(new object[0]);
                this.DataCells.Rows[num3]["SNo"] = num3 + 1;
                this.DataCells.Rows[num3]["DeptName"] = this.AutoDataTable.Rows[num3]["DeptName"];
                this.DataCells.Rows[num3]["EmpNo"] = this.AutoDataTable.Rows[num3]["EmpNo"];
                this.DataCells.Rows[num3]["ExistingCode"] = this.AutoDataTable.Rows[num3]["ExistingCode"];
                this.DataCells.Rows[num3]["FirstName"] = this.AutoDataTable.Rows[num3]["FirstName"];
            }

            num3 = 5;
            this.fromdate = Convert.ToDateTime(this.Date1);
            num2 = 1;
            num = 1;

            for (int i = 0; i < this.AutoDataTable.Rows.Count; i++)
            {
                num2 = 1;
                int num5 = num2;
                string str = "";
                str = this.AutoDataTable.Rows[i][2].ToString();
                string.Format(Convert.ToString(Convert.ToDateTime(string.Format(this.Date1, "dd-MM-yyyy")).AddDays(1.0)), "dd-MM-yyyy");
                new DataTable();
                this.fromdate.DayOfWeek.ToString();
                this.SSQL = "Select TimeIN from LogTime_Lunch where MachineID='" + str + "'";
                this.SSQL = this.SSQL + " And Compcode='" + this.SessionCcode.ToString() + "' And LocCode='" + this.SessionLcode.ToString() + "'";
                this.SSQL = this.SSQL + " And TimeIN >='" + this.fromdate.AddDays(0.0).ToString("yyyy/MM/dd") + " 07:00' And TimeIN <='" + this.fromdate.AddDays(1.0).ToString("yyyy/MM/dd") + " 07:00' Order by TimeIN ASC";
                this.mLocalDS = this.objdata.ReturnMultipleValue(this.SSQL);
                if (this.mLocalDS.Rows.Count >= 1)
                {
                    string str3 = "00:00";
                    for (int j = 0; j < this.mLocalDS.Rows.Count; j++)
                    {
                        TimeSpan span;
                        str3 = "";
                        int num7 = 5 + j;
                        switch (j)
                        {
                            case 0:
                                this.DataCells.Rows[i][num7] = string.Format("{0:hh:mm tt}", this.mLocalDS.Rows[j]["TimeIN"]);
                                this.date1 = Convert.ToDateTime(this.DataCells.Rows[i][num7]);
                                break;

                            case 1:
                                num7 = 5 + j;
                                this.DataCells.Rows[i][num7] = string.Format("{0:hh:mm tt}", this.mLocalDS.Rows[j]["TimeIN"]);
                                this.date2 = Convert.ToDateTime(this.DataCells.Rows[i][num7]);
                                span = this.date2.Subtract(this.date1);
                                span.Hours.ToString();
                                str3 = span.Hours + ":" + span.Minutes;
                                num7 = 7;
                                this.DataCells.Rows[i][num7] = string.Format("{0:hh:mm tt}", str3);
                                break;

                            case 2:
                                num7 = (5 + j) + 1;
                                this.DataCells.Rows[i][num7] = string.Format("{0:hh:mm tt}", this.mLocalDS.Rows[j]["TimeIN"]);
                                this.date3 = Convert.ToDateTime(this.DataCells.Rows[i][num7]);
                                break;

                            case 3:
                                num7 = (5 + j) + 1;
                                this.DataCells.Rows[i][num7] = string.Format("{0:hh:mm tt}", this.mLocalDS.Rows[j]["TimeIN"]);
                                this.date4 = Convert.ToDateTime(this.DataCells.Rows[i][num7]);
                                span = this.date4.Subtract(this.date3);
                                span.Hours.ToString();
                                str3 = span.Hours + ":" + span.Minutes;
                                num7 = (5 + j) + 2;
                                this.DataCells.Rows[i][num7] = string.Format("{0:hh:mm tt}", str3);
                                break;

                            case 4:
                                num7 = (5 + j) + 2;
                                this.DataCells.Rows[i][num7] = string.Format("{0:hh:mm tt}", this.mLocalDS.Rows[j]["TimeIN"]);
                                this.date5 = Convert.ToDateTime(this.DataCells.Rows[i][num7]);
                                break;

                            case 5:
                                num7 = (5 + j) + 2;
                                this.DataCells.Rows[i][num7] = string.Format("{0:hh:mm tt}", this.mLocalDS.Rows[j]["TimeIN"]);
                                this.date6 = Convert.ToDateTime(this.DataCells.Rows[i][num7]);
                                span = this.date6.Subtract(this.date5);
                                span.Hours.ToString();
                                str3 = span.Hours + ":" + span.Minutes;
                                num7 = (5 + j) + 3;
                                this.DataCells.Rows[i][num7] = string.Format("{0:hh:mm tt}", str3);
                                break;

                            case 6:
                                num7 = (5 + j) + 3;
                                this.DataCells.Rows[i][num7] = string.Format("{0:hh:mm tt}", this.mLocalDS.Rows[j]["TimeIN"]);
                                this.date7 = Convert.ToDateTime(this.DataCells.Rows[i][num7]);
                                break;

                            case 7:
                                num7 = (5 + j) + 3;
                                this.DataCells.Rows[i][num7] = string.Format("{0:hh:mm tt}", this.mLocalDS.Rows[j]["TimeIN"]);
                                this.date8 = Convert.ToDateTime(this.DataCells.Rows[i][num7]);
                                span = this.date8.Subtract(this.date7);
                                span.Hours.ToString();
                                str3 = span.Hours + ":" + span.Minutes;
                                num7 = (5 + j) + 4;
                                this.DataCells.Rows[i][num7] = string.Format("{0:hh:mm tt}", str3);
                                break;
                        }
                    }
                }
                num5 += this.shiftCount;
                num2++;
                num++;
                this.Heading = num;
            }


        }
        catch (Exception)
        {
        }
    }
    protected void UploadDataTableToExcel(DataTable dtRecords)
    {
        string text1 = "Break Time-" + this.fromdate.AddDays(0.0);
        string str = base.Server.MapPath("~/Add_data/Break Time.xls");
        string str2 = string.Empty;
        if (str.IndexOf(@"\") != -1)
        {
            string[] strArray = str.Split(new char[] { '\\' });
            str2 = "attachment; filename=" + strArray[strArray.Length - 1];
        }
        else
        {
            str2 = "attachment; filename=" + str;
        }
        try
        {
            base.Response.ClearContent();
            base.Response.AddHeader("content-disposition", str2);
            base.Response.ContentType = "application/vnd.ms-excel";
            string str3 = string.Empty;
            foreach (DataColumn column in dtRecords.Columns)
            {
                base.Response.Write(str3 + column.ColumnName);
                str3 = "\t";
            }
            base.Response.Write("\n");
            foreach (DataRow row in dtRecords.Rows)
            {
                str3 = "";
                for (int i = 0; i < dtRecords.Columns.Count; i++)
                {
                    base.Response.Write(str3 + Convert.ToString(row[i]));
                    str3 = "\t";
                }
                base.Response.Write("\n");
            }
            base.Response.End();
        }
        catch (Exception)
        {
        }
    }
    public void Fill_Multi_timeIN()
    {
        try
        {
            this.AutoDataTable.Columns.Add("DeptName");
            this.AutoDataTable.Columns.Add("MachineID");
            this.AutoDataTable.Columns.Add("MachineID_Encrypt");
            this.AutoDataTable.Columns.Add("EmpNo");
            this.AutoDataTable.Columns.Add("ExistingCode");
            this.AutoDataTable.Columns.Add("FirstName");
            this.AutoDataTable.Columns.Add("TimeIN1");
            this.AutoDataTable.Columns.Add("TimeIN2");
            this.AutoDataTable.Columns.Add("TimeIN3");
            this.AutoDataTable.Columns.Add("TimeIN4");
            this.AutoDataTable.Columns.Add("TimeOUT1");
            this.AutoDataTable.Columns.Add("TimeOUT2");
            this.AutoDataTable.Columns.Add("TimeOUT3");
            this.AutoDataTable.Columns.Add("TimeOUT4");
            this.SSQL = "";
            this.SSQL = "select isnull(DeptName,'') as [DeptName], Cast(MachineID As int) As MachineID,MachineID_Encrypt";
            this.SSQL = this.SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
            this.SSQL = this.SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
            this.SSQL = this.SSQL + " from Employee_Mst Where Compcode='" + this.SessionCcode + "'";
            this.SSQL = this.SSQL + " And LocCode='" + this.SessionLcode + "' And IsActive='Yes'";
            if (this.SessionUserType == "2")
            {
                this.SSQL = this.SSQL + " And IsNonAdmin='1'";
            }
            this.SSQL = this.SSQL + " Order By DeptName, MachineID";
            this.dsEmployee = this.objdata.ReturnMultipleValue(this.SSQL);
            if (this.dsEmployee.Rows.Count > 0)
            {
                int num = 0;
                for (int i = 0; i < this.dsEmployee.Rows.Count; i++)
                {
                    this.AutoDataTable.NewRow();
                    this.AutoDataTable.Rows.Add(new object[0]);
                    this.AutoDataTable.Rows[num][0] = this.dsEmployee.Rows[i]["DeptName"].ToString();
                    this.AutoDataTable.Rows[num][1] = this.dsEmployee.Rows[i]["MachineID"].ToString();
                    this.AutoDataTable.Rows[num][2] = this.dsEmployee.Rows[i]["MachineID_Encrypt"].ToString();
                    this.AutoDataTable.Rows[num][3] = this.dsEmployee.Rows[i]["EmpNo"].ToString();
                    this.AutoDataTable.Rows[num][4] = this.dsEmployee.Rows[i]["ExistingCode"].ToString();
                    this.AutoDataTable.Rows[num][5] = this.dsEmployee.Rows[i]["FirstName"].ToString();
                    num++;
                }
            }
        }
        catch (Exception)
        {
        }
    }
}
