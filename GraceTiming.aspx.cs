﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;


public partial class GraceTiming : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    string SSQL;
    bool ErrFlag = false;
    static int SNo;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
          
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Grace Time Master";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");

                CreateUserDisplay();
                Drop_Shift();
            }
        }
    }


    protected void BtnClear_Click(object sender, EventArgs e)
    {
        clear();
    }

    public void clear()
    {
        txtDeductMinutes.Text = "";
        txtStartMinutes.Text = "";
        txtToMinutes.Text = "";
    }
    public void Drop_Shift()
    {
        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = "select Sno,ShiftDesc from Shift_Mst Where Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
        dt = objdata.ReturnMultipleValue(SSQL);
        ddlshift.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["Sno"] = 0;
        dr["ShiftDesc"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        ddlshift.DataTextField = "ShiftDesc";
        ddlshift.DataValueField = "Sno";
        ddlshift.DataBind();

    }
    public void  CreateUserDisplay()
    {
        DataTable dtdDisplay = new DataTable();
        SSQL = "select*from Grace_Shiftmst";
        dtdDisplay = objdata.ReturnMultipleValue(SSQL);
        rptrCustomer.DataSource = dtdDisplay;
        rptrCustomer.DataBind();
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(id);
                break;
            case ("Edit"):
                EditRepeaterData(id);
                break;
         }
    }

    private void EditRepeaterData(string id)
    {
        DataTable Dtd = new DataTable();
        SSQL = "select*from Grace_Shiftmst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and SNo='" + id + "'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        if (Dtd.Rows.Count > 0) 
        {
            ddlshift.SelectedItem.Text  = Dtd.Rows[0]["ShiftDesc"].ToString();
            txtStartMinutes.Text = Dtd.Rows[0]["StartTime"].ToString();
            txtToMinutes.Text = Dtd.Rows[0]["ToTime"].ToString();
            txtDeductMinutes.Text = Dtd.Rows[0]["mintus"].ToString();
            CreateUserDisplay();
            btnSave.Text = "Update";
          
        }
    }

    private void DeleteRepeaterData(string id)
    {
        DataTable Dtd = new DataTable();
        SSQL = "Delete Grace_Shiftmst where SNo='" + id + "'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        CreateUserDisplay();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;

       
            if (btnSave.Text == "Update")
            {
                DataTable dtdLMst = new DataTable();
                SSQL = "update Grace_Shiftmst set StartTime='" + txtStartMinutes.Text + "',ToTime='" + txtToMinutes.Text + "',mintus='" + txtDeductMinutes.Text + "' where ShiftDesc='"+ddlshift.Text +"'";
                dtdLMst = objdata.ReturnMultipleValue(SSQL);
                CreateUserDisplay();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Update GraceTime Successfully');", true);
                ErrFlag = true;
                btnSave.Text = "Save";
                clear();
            }
            else
            {
                DataTable da = new DataTable(); 
                SSQL = "";
                SSQL = "select * from Grace_Shiftmst where ShiftDesc='" + ddlshift.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and StartTime='" + txtStartMinutes.Text + "' and ToTime='" + txtToMinutes.Text + "'";
                da = objdata.ReturnMultipleValue(SSQL);
                if (da.Rows.Count == 0)
                {
                    SSQL = "insert into Grace_Shiftmst(ShiftDesc,StartTime,ToTime,mintus,CompCode,LocCode)values('" + ddlshift.SelectedItem.Text + "','" + txtStartMinutes.Text + "','" + txtToMinutes.Text + "'";
                    SSQL = SSQL + ",'" + txtDeductMinutes.Text + "','" + SessionCcode + "','" + SessionLcode + "')";
                    objdata.ReturnMultipleValue(SSQL);
                    CreateUserDisplay();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GraceTime Save Successfully');", true);
                    ErrFlag = true;
                    clear();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Allready Saved');", true);
                    ErrFlag = true;
                }

                
                }
            }
        
    


}
