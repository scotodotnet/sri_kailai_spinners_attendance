﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class GraceTimeDeduction : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    //string SessionAdmin = "admin";
    //string SessionCcode = "VIJAY";
    //string SessionLcode = "UNIT I";

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType; 

    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    string EmpCategory = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];


    DataTable mLocalDS_INTAB = new DataTable();
	DataTable mLocalDS_OUTTAB = new DataTable();
	string Date_Value_Str = null;
	string Time_IN_Str = "";
	string Time_Out_Str = "";
	Int32 time_Check_dbl = 0;
	string Total_Time_get = "";
	bool Errflag = false;
	string SName = "";
	string ShiftstartTime = "";
	DateTime ShiftStartDT = default(DateTime);
	DateTime ShiftTimeINadd = default(DateTime);
	DateTime ShiftTimeOUTadd = default(DateTime);
	DateTime GraceMins_InAdd1 = default(DateTime);
	DateTime GraceMins_OutAdd1 = default(DateTime);
	DateTime GraceMins_InAdd2 = default(DateTime);
	DateTime GraceMins_OutAdd2 = default(DateTime);
	DateTime GraceMins_InAdd3 = default(DateTime);
	DateTime GraceMins_OutAdd3 = default(DateTime);


	DateTime EmpdateIN_Change = default(DateTime);
    DataTable AutoDTable = new DataTable();
    DataTable Datacells = new DataTable();
    DataSet ds = new DataSet();
    string SSQL;
    string mIpAddress_IN;
    string mIpAddress_OUT;

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Grace Time Deduction";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();




            ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            ddlShiftType = Request.QueryString["ddlShiftType"].ToString();
            EmpCategory = Request.QueryString["EmpCategory"].ToString();




            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("Dept");
            AutoDTable.Columns.Add("Type");
            AutoDTable.Columns.Add("Shift");
            AutoDTable.Columns.Add("EmpCode");
            AutoDTable.Columns.Add("ExCode");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("TimeIN");
            AutoDTable.Columns.Add("TimeOUT");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Category");
            AutoDTable.Columns.Add("SubCategory");
            AutoDTable.Columns.Add("TotalMIN");
            AutoDTable.Columns.Add("GrandTOT");
            AutoDTable.Columns.Add("Deduction");
            AutoDTable.Columns.Add("Status");





            Datacells.Columns.Add("CompanyName");
            Datacells.Columns.Add("LocationName");
            Datacells.Columns.Add("ShiftDate");
            Datacells.Columns.Add("SNo");
            Datacells.Columns.Add("Dept");
            Datacells.Columns.Add("Type");
            Datacells.Columns.Add("Shift");
            Datacells.Columns.Add("Category");
            Datacells.Columns.Add("SubCategory");
            Datacells.Columns.Add("EmpCode");
            Datacells.Columns.Add("ExCode");
            Datacells.Columns.Add("Name");
            Datacells.Columns.Add("TimeIN");
            Datacells.Columns.Add("TimeOUT");
            Datacells.Columns.Add("MachineID");
            Datacells.Columns.Add("PrepBy");
            Datacells.Columns.Add("PrepDate");
            Datacells.Columns.Add("TotalMIN");
            Datacells.Columns.Add("GrandTOT");
            Datacells.Columns.Add("Deduction");
            Datacells.Columns.Add("Status");


            DataTable mDataSet = new DataTable();

            SSQL = "";
            SSQL = "Select IPAddress,IPMode from IPAddress_Mst Where Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";

            mDataSet = objdata.ReturnMultipleValue(SSQL);

            if (mDataSet.Rows.Count > 0)
            {
                for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
                {
                    if ((mDataSet.Rows[iRow]["IPMode"].ToString()) == "IN")
                    {
                        mIpAddress_IN = (mDataSet.Rows[iRow]["IPAddress"].ToString());
                    }
                    else if ((mDataSet.Rows[iRow]["IPMode"].ToString()) == "OUT")
                    {
                        mIpAddress_OUT = (mDataSet.Rows[iRow]["IPAddress"].ToString());
                    }
                }
            }


            DataTable mLocalDS = new DataTable();
            DataTable mcheckGenDS = new DataTable();


            if (ModeType == "IN/OUT")
            {
                SSQL = "";
                SSQL = "Select StartTime,shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
                SSQL = SSQL + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
                SSQL += " from Shift_Mst Where CompCode='" + SessionCcode + "'";
                SSQL += " And LocCode='" + SessionLcode + "'";
                if (ShiftType1 != "ALL")
                {
                    SSQL += " And shiftDesc='" + ShiftType1 + "'";
                }
                SSQL += " And ShiftDesc like '" + ddlShiftType + "%'";
                //Shift%'"
                SSQL += " Order By shiftDesc";

            }
            else
            {
                return;
            }


            string ShiftType = "";

            string ng = string.Format(Date, "dd-MM-yyyy");
            DateTime date1 = Convert.ToDateTime(ng);

            mLocalDS = objdata.ReturnMultipleValue(SSQL);

            if (mLocalDS.Rows.Count < 0)
                return;
            int mStartINRow = 0;
            int mStartOUTRow = 0;

            for (int iTabRow = 0; iTabRow < mLocalDS.Rows.Count; iTabRow++)
            {
                if (AutoDTable.Rows.Count <= 1)
                {
                    mStartOUTRow = 0;
                }
                else
                {
                    mStartOUTRow = AutoDTable.Rows.Count - 1;
                }
                if (mLocalDS.Rows[iTabRow]["shiftDesc"].ToString() == "GENERAL")
                {
                    ShiftType = "GENERAL";
                }
                else
                {
                    ShiftType = "SHIFT";
                }

                string sIndays_str = mLocalDS.Rows[iTabRow]["StartIN_Days"].ToString();
                double sINdays = Convert.ToDouble(sIndays_str);
                string eIndays_str = mLocalDS.Rows[iTabRow]["EndIN_Days"].ToString();
                double eINdays = Convert.ToDouble(eIndays_str);



                if (ShiftType == "GENERAL")
                {
                    SSQL = "";
                    SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID ";
                    SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "' and EM.CatName='" + EmpCategory + "'";
                    SSQL = SSQL + " And LT.TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    SSQL = SSQL + " And LT.TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    SSQL = SSQL + " AND em.ShiftType='" + ShiftType + "' Group By LT.MachineID";
                    SSQL = SSQL + " Order By Min(LT.TimeIN)";


                }
                else if (ShiftType == "SHIFT")
                {
                    SSQL = "";
                    SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID ";
                    SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "' and EM.CatName='" + EmpCategory + "'";
                    SSQL = SSQL + " And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartIN"].ToString() + "'";
                    SSQL = SSQL + " And LT.TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndIN"].ToString() + "'";
                    SSQL = SSQL + " Group By LT.MachineID";
                    SSQL = SSQL + " Order By Min(LT.TimeIN)";
                    ShiftstartTime = mLocalDS.Rows[iTabRow]["StartTime"].ToString();
                    string ShiftStartDT_str = date1.AddDays(0).ToString("yyyy/MM/dd") + " " + ShiftstartTime;
                    ShiftStartDT = Convert.ToDateTime(ShiftStartDT_str);

                }
                else
                {
                    SSQL = "";
                    SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID ";
                    SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "' and EM.CatName='" + EmpCategory + "' ";


                    SSQL = SSQL + " And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartIN"].ToString() + "'";

                    SSQL = SSQL + " And LT.TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndIN"].ToString() + "'";
                    SSQL = SSQL + " AND em.ShiftType='" + ShiftType + "' Group By LT.MachineID";
                    SSQL = SSQL + " Order By Min(LT.TimeIN)";

                    ShiftstartTime = mLocalDS.Rows[iTabRow]["StartTime"].ToString();
                    string ShiftStartDT_str = date1.AddDays(0).ToString("yyyy/MM/dd") + " " + ShiftstartTime;
                    ShiftStartDT = Convert.ToDateTime(ShiftStartDT_str);
                    //ShiftStartDT = date1.AddDays(0).ToString("yyyy/MM/dd") + " " + ShiftstartTime;
                }

                //ShiftStartDT = ShiftStartDT.AddMinutes()

                mDataSet = objdata.ReturnMultipleValue(SSQL);

                if (mDataSet.Rows.Count > 0)
                {
                    //fg.Rows = fg.Rows + mDataSet.Tables(0).Rows.Count

                    string MID = "";
                    //int MachineID = 0;

                    for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
                    {
                        bool chkduplicate = false;
                        chkduplicate = false;


                        for (int ia = 0; ia < AutoDTable.Rows.Count; ia++)
                        {
                            MID = mDataSet.Rows[iRow]["MachineID"].ToString();


                            if (MID == AutoDTable.Rows[ia][9].ToString())
                            {
                                chkduplicate = true;
                            }

                        }
                        if (chkduplicate == false)
                        {
                            AutoDTable.NewRow();
                            AutoDTable.Rows.Add();

                            AutoDTable.Rows[mStartINRow][3] = mLocalDS.Rows[iTabRow]["ShiftDesc"].ToString();
                            //fg.set_TextMatrix(mStartINRow, 7, String.Format("{0:hh:mm tt}", (.Rows(iRow)("TimeIN"))))
                            if (ShiftType == "SHIFT")
                            {
                                string str = mDataSet.Rows[iRow][1].ToString();

                                DataTable MstartTime = new DataTable();
                                bool Grace1 = false;
                                bool Grace2 = false;
                                bool Grace3 = false;
                                string Query = "";
                                string LeaveDate = "";
                                DataTable Leave_DS = new DataTable();
                                LeaveDate = date1.AddDays(0).ToString("dd/MM/yyyy");

                                SSQL = "select StartTime,ToTime from Shiftmst ";
                                MstartTime = objdata.ReturnMultipleValue(SSQL);

                                if (MstartTime.Rows.Count > 0)
                                {
                                    string StartTime = MstartTime.Rows[0][0].ToString();
                                    string ToTime = MstartTime.Rows[0][1].ToString();
                                    string StartTime1 = MstartTime.Rows[1][0].ToString();
                                    string ToTime1 = MstartTime.Rows[1][1].ToString();
                                    string StartTime2 = MstartTime.Rows[2][0].ToString();
                                    string ToTime3 = MstartTime.Rows[2][1].ToString();

                                    ShiftTimeINadd = ShiftStartDT;
                                    ShiftTimeOUTadd = ShiftStartDT;

                                    if (MID == "9850")
                                    {
                                        MID = "9850";
                                    }

                                    Query = "select * from Leave_Register_Mst  where ExistingCode='" + MID + "'";
                                    Query = Query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    Query = Query + " And FromDate >=convert(datetime,'" + LeaveDate + "',105)";
                                    Query = Query + " And ToDate <=convert(datetime,'" + LeaveDate + "',105) and LeaveType='PM' ";
                                    Leave_DS = objdata.ReturnMultipleValue(Query);

                                    if (Leave_DS.Rows.Count != 0)
                                    {
                                        AutoDTable.Rows[mStartINRow][14] = "PM";
                                        //fg.set_TextMatrix(mStartINRow, 14, "PM");
                                        Grace1 = true;
                                        Grace2 = true;
                                        Grace3 = true;
                                        AutoDTable.Rows[mStartINRow][14] = "0";
                                        //fg.set_TextMatrix(mStartINRow, 13, "0");

                                    }
                                    else
                                    {
                                        AutoDTable.Rows[mStartINRow][15] = "--";
                                        //fg.set_TextMatrix(mStartINRow, 14, "--");
                                    }



                                    if (Grace1 == false)
                                    {
                                        EmpdateIN_Change = System.Convert.ToDateTime(str);
                                        double StartTime_dou = Convert.ToDouble(StartTime);
                                        GraceMins_InAdd1 = ShiftTimeINadd.AddMinutes(StartTime_dou);
                                        double ToTime_dou = Convert.ToDouble(ToTime);
                                        GraceMins_OutAdd1 = ShiftTimeOUTadd.AddMinutes(ToTime_dou);
                                        if (EmpdateIN_Change >= GraceMins_InAdd1 & EmpdateIN_Change <= GraceMins_OutAdd1)
                                        {
                                            AutoDTable.Rows[mStartINRow][14] = "30Mins";
                                            //fg.set_TextMatrix(mStartINRow, 13, "30Mins");
                                            Grace2 = true;
                                            Grace3 = true;
                                        }
                                        else
                                        {
                                        }
                                    }



                                    if (Grace2 == false)
                                    {
                                        double StartTime1_dou = Convert.ToDouble(StartTime1);
                                        GraceMins_InAdd2 = ShiftTimeINadd.AddMinutes(StartTime1_dou);
                                        double ToTime1_dou = Convert.ToDouble(ToTime1);
                                        GraceMins_OutAdd2 = ShiftTimeOUTadd.AddMinutes(ToTime1_dou);
                                        if (EmpdateIN_Change >= GraceMins_InAdd2 & EmpdateIN_Change <= GraceMins_OutAdd2)
                                        {
                                            AutoDTable.Rows[mStartINRow][14] = "1Hrs";
                                            //fg.set_TextMatrix(mStartINRow, 13, "1Hrs");
                                            Grace3 = true;

                                        }
                                        else
                                        {
                                        }
                                    }


                                    if (Grace3 == false)
                                    {
                                        double StartTime2_dou = Convert.ToDouble(StartTime2);
                                        GraceMins_InAdd3 = ShiftTimeINadd.AddMinutes(StartTime2_dou);


                                        if (EmpdateIN_Change > GraceMins_InAdd3)
                                        {
                                            AutoDTable.Rows[mStartINRow][14] = "1/2Day";
                                            //fg.set_TextMatrix(mStartINRow, 13, "1/2Day");
                                        }
                                        else
                                        {
                                            AutoDTable.Rows[mStartINRow][14] = "0";
                                            //fg.set_TextMatrix(mStartINRow, 13, "0");

                                        }
                                    }
                                }





                                if (Convert.ToDateTime(mDataSet.Rows[iRow][1].ToString()).TimeOfDay >= Convert.ToDateTime("07:01").TimeOfDay & Convert.ToDateTime(mDataSet.Rows[iRow][1].ToString()).TimeOfDay < Convert.ToDateTime("07:30").TimeOfDay)
                                {
                                    AutoDTable.Rows[mStartINRow][7] = "07:30";
                                    //fg.set_TextMatrix(mStartINRow, 7, "07:30");
                                }
                                else
                                {
                                    AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);
                                    //	fg.set_TextMatrix(mStartINRow, 7, (_with3.Rows(iRow)("TimeIN")));
                                }
                            }
                            else
                            {

                                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);
                                //fg.set_TextMatrix(mStartINRow, 7, (_with3.Rows(iRow)("TimeIN")));
                            }
                            string MachineID = mDataSet.Rows[iRow]["MachineID"].ToString();
                            AutoDTable.Rows[mStartINRow][9] = MachineID;
                            //fg.set_TextMatrix(mStartINRow, 9, MachineID);

                            if (MachineID == "1058")
                            {
                                MachineID = "1058";
                            }
                            mStartINRow += 1;
                        }
                    }
                }

                mDataSet = new DataTable();


                SSQL = "";
                SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT ";
                SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                SSQL = SSQL + " Group By MachineID";
                SSQL = SSQL + " Order By Max(TimeOUT)";


                mDataSet = objdata.ReturnMultipleValue(SSQL);




                DataTable mLocalDS_out = new DataTable();
                //var _with4 = mDataSet.Tables(0);
                //For iRow1 = 0 To .Rows.Count - 1
                // fg1.set_TextMatrix(iRow1 + 1, 0, Trim(Decryption(.Rows(iRow1)("MachineID"))))
                for (int iRow2 = mStartOUTRow; iRow2 < AutoDTable.Rows.Count; iRow2++)
                {
                    bool Grace1 = false;
                    bool Grace2 = false;
                    bool Grace3 = false;
                    string InMachine_IP = "";
                    int MachineID = 0;




                    InMachine_IP = AutoDTable.Rows[iRow2][9].ToString();

                    SName = mLocalDS.Rows[iTabRow]["shiftDesc"].ToString();

                    if (SName == "SHIFT1")
                    {
                        SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeOUT ASC";

                        mLocalDS_out = objdata.ReturnMultipleValue(SSQL);
                    }
                    else
                    {
                        SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT ASC";

                        mLocalDS_out = objdata.ReturnMultipleValue(SSQL);
                    }


                    mLocalDS_out = objdata.ReturnMultipleValue(SSQL);
                    if (mLocalDS_out.Rows.Count <= 0)
                    {
                        //Skip
                    }
                    else
                    {
                        if (AutoDTable.Rows[iRow2][9].ToString() == mLocalDS_out.Rows[0][0].ToString())
                        {
                            AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1]);

                            //fg.set_TextMatrix(iRow2, 8, string.Format("{0:hh:mm tt}", (mLocalDS_out.Tables(0).Rows(0)(1))));
                            //GoTo 1
                        }
                    }


                    //Grand Total value Display
                    Time_IN_Str = "";
                    Time_Out_Str = "";
                    string Mid = "";
                    string Mid2 = "";
                    string Midvalue1 = "";
                    string MidValue = "";
                    string Staff = "STAFF";
                    string ShiftTypes = "";
                    TimeSpan H = new TimeSpan();
                    string[] hour = null;
                    string hoursp = "";

                    string minsp = "";
                    string hoursval = "08";
                    string Minval = "06";

                    string zero = "";
                    string Zero1 = "";
                    string Time = "09";
                    string Time1 = "03";


                    //8:15 Split
                    string[] sTime = null;
                    string stime_Hrs = "";
                    string stime_Min = "";

                    //8:30 Split
                    string[] sTime1 = null;
                    string sTime1_Hrs = "";
                    string sTime1_Min = "";


                    //8:45 Split
                    string[] sTime2 = null;
                    string sTime2_hrs = "";
                    string sTime2_Min = "";



                    //Mid = Decryption(Strings.Trim(fg.get_TextMatrix(iRow2, 9)));
                    //Mid2 = Decryption(Strings.Trim(fg.get_TextMatrix(iRow2, 9)));

                    Mid = AutoDTable.Rows[iRow2][9].ToString();
                    Mid2 = AutoDTable.Rows[iRow2][9].ToString();

                    if (Mid == "1058")
                    {
                        Mid = "1058";
                    }





                    Date_Value_Str = date1.ToString("yyyy/MM/dd");
                    SSQL = "Select min(TimeIN) as TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                    mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);


                    SName = mLocalDS.Rows[iTabRow]["shiftDesc"].ToString();

                    if (SName == "SHIFT1")
                    {
                        SSQL = "Select  TimeOUT   from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeOUT ASC";

                        mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);
                    }
                    else
                    {
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT ASC";

                        mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);
                    }


                    string Emp_Total_Work_Time_1 = "00:00";
                    if (mLocalDS_INTAB.Rows.Count > 1)
                    {

                        for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

                            if (mLocalDS_OUTTAB.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_Out_Str = "";
                            }
                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            //ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay()
                            //ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay()
                            if (mLocalDS.Rows.Count < 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                            }






                            //Emp_Total_Work_Time
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
                                //If date3 > date4 Then

                                //    date4 = System.Convert.ToDateTime(mLocalDS_OUTTAB.Tables(0).Rows(tin + 1)(0))
                                //End If

                                TimeSpan ts1 = new TimeSpan();
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                //ts4 = ts4.Add(ts1)
                                //OT Time Get
                                Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")

                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    //& ":" & Trim(ts.Minutes)
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                    //Emp_Total_Work_Time_1 = Trim(ts1.Hours) & ":" & Trim(ts1.Minutes)
                                    Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                }
                                else
                                {
                                    ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                }
                            }
                            AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;
                            //fg.set_TextMatrix(iRow2, fg.Cols - 1, Emp_Total_Work_Time_1);
                        }

                    }
                    else
                    {
                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS_INTAB.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout < mLocalDS_OUTTAB.Rows.Count; tout++)
                        {
                            if (mLocalDS_OUTTAB.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";
                            }
                            else
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                            }

                        }
                        //Emp_Total_Work_Time
                        if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts1 = new TimeSpan();
                            ts1 = date4.Subtract(date3);
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();


                            Mid = AutoDTable.Rows[iRow2][9].ToString();
                            //Mid = Decryption(Strings.Trim(fg.get_TextMatrix(iRow2, 9)));
                            string SQL = "";

                            if (Mid == "1112")
                            {
                                string sd = "";
                            }



                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                //Emp_Total_Work_Time_1 = Trim(ts1.Hours) & ":" & Trim(ts1.Minutes)
                                Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                            }
                            else
                            {
                                ts4 = ts4.Add(ts1);
                                Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                            }
                            AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;
                            //fg.set_TextMatrix(iRow2, 12, Emp_Total_Work_Time_1);
                        }
                    }
                    AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;
                    //fg.set_TextMatrix(iRow2, fg.Cols - 1, Emp_Total_Work_Time_1);
                    //fg.set_TextMatrix(iRow2, 13, Emp_Total_Work_Time_1)
                    //End
                }



            }


            //'No Shift (Shift Timing Not Match Employee Add
            //Call NoShift_Add_All_Shift_Wise_GetAttdDayWise()

            DataTable mEmployeeDS = new DataTable();

            SSQL = "";
            SSQL = "select Distinct isnull(EM.MachineID_Encrypt,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
            SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
            SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + ",isnull(EM.CatName,'') as [CatName], isnull(EM.SubCatName,'') as [SubCatName]";
            SSQL = SSQL + ",DM.DeptCode from Employee_Mst EM inner Join Department_Mst DM on DM.DeptName=EM.DeptName Where EM.Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "'And  EM.IsActive='Yes'" + " order by DM.DeptCode Asc";

            mEmployeeDS = objdata.ReturnMultipleValue(SSQL);



            if (mEmployeeDS.Rows.Count > 0)
            {

                for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                {
                    for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
                    {
                        if (AutoDTable.Rows[iRow2][9].ToString() == mEmployeeDS.Rows[iRow1]["MachineID"].ToString())
                        {
                            AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"].ToString();
                            AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"].ToString();
                            AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"].ToString();
                            AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"].ToString();
                            AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"].ToString();
                            AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"].ToString();
                            AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"].ToString();

                        }
                    }
                }

            }

            int k = 0;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                if (AutoDTable.Rows[i]["Deduction"].ToString() != "0")
                {
                    Datacells.NewRow();
                    Datacells.Rows.Add();

                    Datacells.Rows[k]["CompanyName"] = SessionCcode;
                    Datacells.Rows[k]["LocationName"] = SessionLcode;
                    Datacells.Rows[k]["ShiftDate"] = Date;
                    Datacells.Rows[k]["Dept"] = AutoDTable.Rows[i]["Dept"].ToString();
                    Datacells.Rows[k]["Type"] = AutoDTable.Rows[i]["Type"].ToString();
                    Datacells.Rows[k]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                    Datacells.Rows[k]["Category"] = AutoDTable.Rows[i]["Category"].ToString();
                    Datacells.Rows[k]["SubCategory"] = AutoDTable.Rows[i]["SubCategory"].ToString();
                    Datacells.Rows[k]["EmpCode"] = AutoDTable.Rows[i]["EmpCode"].ToString();
                    Datacells.Rows[k]["ExCode"] = AutoDTable.Rows[i]["ExCode"].ToString();
                    Datacells.Rows[k]["Name"] = AutoDTable.Rows[i]["Name"].ToString();
                    Datacells.Rows[k]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                    Datacells.Rows[k]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                    Datacells.Rows[k]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                    Datacells.Rows[k]["TotalMIN"] = AutoDTable.Rows[i]["TotalMIN"].ToString();
                    Datacells.Rows[k]["GrandTOT"] = AutoDTable.Rows[i]["GrandTOT"].ToString();
                    Datacells.Rows[k]["Deduction"] = AutoDTable.Rows[i]["Deduction"].ToString();
                    Datacells.Rows[k]["Status"] = AutoDTable.Rows[i]["Status"].ToString();
                    k = k + 1;

                }
            }






            ds.Tables.Add(Datacells);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/GraceTime.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            CrystalReportViewer1.ReportSource = report;


        }
    }
}