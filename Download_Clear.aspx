<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Download_Clear.aspx.cs" Inherits="Download_Clear" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>


<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>



<script type="text/javascript">
    function ProgressBarShow() {
        $('#Download_loader').show();
    }
</script>

<script type="text/javascript">
    function ProgressBarHide() {
        $('#Download_loader').hide();
    }
</script>



<%--

     <style type="text/css">
.modalPopup
{
background-color: #696969;
filter: alpha(opacity=40);
opacity: 0.7;
xindex:-1;
}
</style>
     <script type="text/javascript">
var prm = Sys.WebForms.PageRequestManager.getInstance();
//Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
prm.add_beginRequest(BeginRequestHandler);
// Raised after an asynchronous postback is finished and control has been returned to the browser.
prm.add_endRequest(EndRequestHandler);
function BeginRequestHandler(sender, args) {
//Shows the modal popup - the update progress
var popup = $find('<%= modalPopup.ClientID %>');
if (popup != null) {
popup.show();
}

}

function EndRequestHandler(sender, args) {
//Hide the modal popup - the update progress
var popup = $find('<%= modalPopup.ClientID %>');
if (popup != null) {
    popup.hide();
    
}
}
</script>--%>

<script type="text/javascript">
    $(document).ready(function() {
        bindButton();
    });
    function bindButton() {
        $('#<%=btndownload.ClientID%>').on('click', function() {
        $('#<%=lblDwnCmpltd.ClientID%>').html('Please Wait...');
        });
    }
</script>

<script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            function f() {
                $('#<%=lblDwnCmpltd.ClientID%>').html("DOWNLOAD PROCESSING....");
            }
        </script>


<%--<asp:UpdateProgress ID="UpdateProgress" runat="server">  
<ProgressTemplate>
<asp:Image ID="Image1" ImageUrl="D:\ND\SPaySastha\assets\images\waiting.gif" AlternateText="Processing" runat="server" />

<img src="assets/images/reload.GIF" alt="" />

</ProgressTemplate>
</asp:UpdateProgress>
<cc1:ModalPopupExtender ID="modalPopup" runat="server" TargetControlID="UpdateProgress"
PopupControlID="UpdateProgress" BackgroundCssClass="modalPopup" />--%>

     <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                      <ContentTemplate>
                <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">Download Clear</li>
                           <h4>
                           </h4>
                           <h4>
                           </h4>
                           <h4>
                           </h4>
                           <h4>
                           </h4>
                           <h4>
                           </h4>
                           <h4>
                           </h4>
                        </h4> 
                    </ol>
                </div>
                <div id="main-wrapper" class="container">
                <div class="row">
                <div class="col-md-12">
                <div class="page-inner">
                <div class="col-md-9">
                <div class="panel panel-white">
		    	<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Download Clear</h4>
				</div>
				</div>
				<form class="form-horizontal">
				 
                  
				<div class="panel-body">
					
					<div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Company Code<span class="mandatory">*</span></label>
						<div class="col-sm-4">
                           <asp:TextBox ID="TxtCompcode" class="form-control" runat="server" AutoPostBack="true">
                                </asp:TextBox>
                            
						</div>
						<div class="col-md-1"></div>
				        <label for="input-Default" class="col-sm-2 control-label">Location Code<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                            <asp:DropDownList ID="ddlLocationCode" runat="server" class="form-control" AutoPostBack="true"
                               >
                            
                            </asp:DropDownList>
                            
						</div>
               </div>                     
					<div class="form-group row">
						 <label for="input-Default" class="col-sm-2 control-label">IP Address</label>
							<div class="col-sm-4">
                                 <asp:DropDownList ID="ddlIPAddress" runat="server" class="form-control" AutoPostBack="true">
                                
                                </asp:DropDownList>
							</div>
							
						</div>
					    
					    
                         
                        
                         
					
					<!-- Button start -->
			  <div class="form-group row">
		       </div>
						
						
                 <div class="txtcenter">
                    <asp:Button ID="btndownload" class="btn btn-success"  runat="server" Text="Download" 
                    onclick="btndownload_Click" OnClientClick="ProgressBarShow();"
                                    />
                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                    <asp:Button ID="Button12" class="btn btn-danger" runat="server" 
                         Text="Download/Clear" onclick="Button12_Click"/>
                    
                 </div>
                 <div id="Download_loader" style="display:none"/></div>

                    
                    
                 <div class="form-group row">
			    </div>
                    
                            <div class="txtcenter" align="center">
                              <asp:Label ID="lblDwnCmpltd" runat="server" Font-Bold="True" Font-Italic="True" 
                                    Font-Size="Medium" ForeColor="Red"></asp:Label>
					      </div>
                    
                    
                    <!-- Button End -->	
                    
                    
                    
					 </div>
					
					
					
					
				</form>
			
			</div>
                </div>
               
                 <div class="col-md-2"></div>
		    
            
      
 
  
                <!-- Dashboard start -->
                <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>
  
                <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                   <!-- Dashboard End -->   
                   
                   </div>
                  <%-- <div class="form-group row"></div>  
					  <div class="form-group row"></div>
					  <div class="form-group row"></div> 
					  <div class="form-group row"></div>
					 <div class="form-group row"></div>--%>
 </div><!-- col 12 end -->
      
               </div><!-- row end -->
               </div>
 
              </ContentTemplate>
                 <Triggers>
                   <%-- <asp:AsyncPostBackTrigger ControlID="btndownload" />--%>
                    <asp:PostBackTrigger ControlID="btndownload"  />
                 </Triggers>
              </asp:UpdatePanel>    
                    
</asp:Content>

