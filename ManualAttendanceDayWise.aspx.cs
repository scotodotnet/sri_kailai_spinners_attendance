﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



public partial class ManualAttendanceDayWise : System.Web.UI.Page
{

    
    DataTable mDataSet = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();

    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";

    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();

    DateTime ShiftdateStartIN;
    DateTime ShiftdateEndIN;
    DateTime InTime_Check;
    DateTime InToTime_Check;
    DateTime EmpdateIN;

    DataTable mEmployeeDS = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable DS_InTime = new DataTable();
    DataTable DS_Time = new DataTable();
    string SSQL = "";
    string Final_InTime;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string Time_IN_Str;
    string Time_OUT_Str;
    string Date_Value_Str;
    string Shift_Start_Time;
    string Shift_End_Time;
    string Final_Shift;
    string Total_Time_get;
    string[] Time_Minus_Value_Check;
    string[] OThour_Str;
    DataSet ds = new DataSet();
    int time_Check_dbl;
    DataTable mdatatable = new DataTable();
    DataTable LocalDT = new DataTable();
    DataTable Shift_DS = new DataTable();
    TimeSpan InTime_TimeSpan;
    string From_Time_Str = "";
    string To_Time_Str = "";
    int OT_Hour;
    int OT_Min;
    int OT_Time;
    string OT_Hour1 = "";
    string mUser = "";
    string Datestr = "";
    string Datestr1 = "";

    Boolean Shift_Check_blb = false;
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();

            //ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["FromDate"].ToString();
            //ddlShiftType = Request.QueryString["ShiftType1"].ToString();
            //mUser = Request.QueryString["userType"].ToString();

            //if (SessionUserType == "1")
            //{
            //    AdminGetAttdDayWise_Change();
            //}
            if (SessionUserType == "3")
            {
                NonAdminGetAttdDayWise_Change();
            }
            else
            {
                DataSet ds = new DataSet();
                DataTable AutoDTable = new DataTable();


                AutoDTable.Columns.Add("MachineID");
                AutoDTable.Columns.Add("Dept");
                AutoDTable.Columns.Add("EmpName");
                AutoDTable.Columns.Add("TimeIN");
                AutoDTable.Columns.Add("TimeOUT");
                AutoDTable.Columns.Add("GrandTOT");
            

                DataTable Da_Employee = new DataTable();
                DataTable Da_Manual=new DataTable(); 
                string Time_IN_Check_Shift = "";
                string Time_Out_Check_Shift = "";

                SSQL = "Select Distinct Machine_No from ManAttn_Details where Compcode='" + SessionCcode  + "'"+
                    " and LocCode='" + SessionLcode + "' and AttnDateStr='" + Date + "'";
                Da_Employee = objdata.RptEmployeeMultipleDetails(SSQL);
                   string ng = string.Format(Date, "MM-dd-yyyy");
                    Datestr = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
                    Datestr1 = Convert.ToDateTime(Date).AddDays(1).ToShortDateString();

                    DateTime date1 = Convert.ToDateTime(ng);
                 if (Da_Employee.Rows.Count != 0)
                 {
                     for (int iRow2 = 0; iRow2 < Da_Employee.Rows.Count-1; iRow2++)
                    {
                        string MachineID = Da_Employee.Rows[iRow2][0].ToString();
                        string EmpName = "";
                        string Dept = "";
                         DataTable Da_Emp=new DataTable();
                         SSQL = "select FirstName ,DeptName from Employee_Mst where MachineID='" + MachineID + "'";
                        Da_Emp= objdata.RptEmployeeMultipleDetails(SSQL);
                        if (Da_Emp.Rows.Count != 0)
                        {
                            EmpName = Da_Emp.Rows[0][0].ToString();
                            Dept = Da_Emp.Rows[0][1].ToString();
                        }


                   
                    SSQL = "Select * from ManAttn_Details where Machine_No='" + MachineID + "' And Compcode='" + SessionCcode + "'" +
                           " And LocCode='" + SessionLcode + "' and AttnDateStr='" + Date + "'";
                    Da_Manual = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (Da_Manual.Rows.Count != 0)
                    {
                        AutoDTable.NewRow();
                        AutoDTable.Rows.Add();
                          Time_IN_Check_Shift = Da_Manual.Rows[0]["LogTimeIn"].ToString();
                          Time_Out_Check_Shift = Da_Manual.Rows[0]["LogTimeOut"].ToString();
                          AutoDTable.Rows[iRow2][0]= MachineID;
                          AutoDTable.Rows[iRow2][1] = Dept;
                          AutoDTable.Rows[iRow2][2] = EmpName;
                          AutoDTable.Rows[iRow2][3] = String.Format("{0:hh:mm tt}", Da_Manual.Rows[0]["LogTimeIn"]);
                          AutoDTable.Rows[iRow2][4] = string.Format("{0:hh:mm tt}", Da_Manual.Rows[0]["LogTimeOut"]);

                      
                    SSQL = "Select LogTimeIn from ManAttn_Details where Machine_No='" + MachineID + "'"+
                           " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode  + "'"+
                           " And AttnDateStr ='" + Datestr + "'";
                            mLocalDS_INTAB = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select LogTimeOut from ManAttn_Details where Machine_No='" + MachineID + "'" +
                           " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'" +
                           " And AttnDateStr ='" + Datestr + "'";
                    mLocalDS_OUTTAB = objdata.RptEmployeeMultipleDetails(SSQL);
                    String Emp_Total_Work_Time_1 = "00:00";

                    if (mLocalDS_INTAB.Rows.Count > 0)
                    {
                        for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

                            if (mLocalDS_OUTTAB.Rows.Count > tin)
                            {
                                Time_OUT_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                            {
                                Time_OUT_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_OUT_Str = "";
                            }

                            TimeSpan ts4;
                            ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS_INTAB.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                            }

                            if (Time_IN_Str == "" || Time_OUT_Str == "")
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date3 = Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = Convert.ToDateTime(Time_OUT_Str);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }

                                }
                                else
                                {
                                    ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                            }

                            AutoDTable.Rows[iRow2][5] = Emp_Total_Work_Time_1;
                            
                            

                        }
                    }





                        
                    }
                }


                    








                }

            
                  


     
              
            


               



                ds.Tables.Add(AutoDTable);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/Manual Att_Daywise.rpt"));
                report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
               
                CrystalReportViewer1.ReportSource = report;
            }
        }
    }

 

    public void NonAdminGetAttdDayWise_Change()
    {
        DataSet ds = new DataSet();
        DataTable AutoDTable = new DataTable();


        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Type");
        AutoDTable.Columns.Add("Shift");
        AutoDTable.Columns.Add("EmpCode");
        AutoDTable.Columns.Add("ExCode");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("TimeIN");
        AutoDTable.Columns.Add("TimeOUT");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("Category");
        AutoDTable.Columns.Add("SubCategory");
        AutoDTable.Columns.Add("TotalMIN");
        AutoDTable.Columns.Add("GrandTOT");
        AutoDTable.Columns.Add("ShiftDate");
        AutoDTable.Columns.Add("OTHour");
        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
        AutoDTable.Columns.Add("Desgination");
        AutoDTable.Columns.Add("MachineIDNew");

        //if (mReportName.Text == "DAY ATTENDANCE - DAY WISE :: BELOW 4 HOURS")
        //{
        DataTable dtIPaddress = new DataTable();
        dtIPaddress = objdata.IPAddressForAll(SessionCcode.ToString(), SessionLcode.ToString());

        if (dtIPaddress.Rows.Count > 0)
        {
            for (int i = 0; i < dtIPaddress.Rows.Count; i++)
            {
                if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                {
                    mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
                else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                {
                    mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
            }
        }


        SSQL = "";
        SSQL = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
        SSQL = SSQL + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
        SSQL = SSQL + " from Shift_Mst Where CompCode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
        if (ShiftType1 != "ALL")
        {
            SSQL = SSQL + " And shiftDesc='" + ShiftType1 + "'";

        }
        //else
        //{
        //    SSQL = SSQL + " And ShiftDesc like '" + ddlShiftType + "%'";

        //}   
        SSQL = SSQL + " Order By shiftDesc";
        LocalDT = objdata.ReturnMultipleValue(SSQL);


        string ShiftType;

        if (LocalDT.Rows.Count > 0)
        {
            string ng = string.Format(Date, "MM-dd-yyyy");
            Datestr = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
            Datestr1 = Convert.ToDateTime(Date).AddDays(1).ToShortDateString();
            DateTime date1 = Convert.ToDateTime(ng);
            DateTime date2 = date1.AddDays(1);
            string Start_IN = "";
            string End_In;

            string End_In_Days = "";
            string Start_In_Days = "";

            int mStartINRow = 0;
            int mStartOUTRow = 0;

            for (int iTabRow = 0; iTabRow < LocalDT.Rows.Count; iTabRow++)
            {

                if (AutoDTable.Rows.Count <= 1)
                {
                    mStartOUTRow = 0;
                }
                else
                {
                    mStartOUTRow = AutoDTable.Rows.Count - 1;
                }

                if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "GENERAL")
                {
                    ShiftType = "GENERAL";
                }
                else
                {
                    ShiftType = "SHIFT";
                }


                string sIndays_str = LocalDT.Rows[iTabRow]["StartIN_Days"].ToString();
                double sINdays = Convert.ToDouble(sIndays_str);
                string eIndays_str = LocalDT.Rows[iTabRow]["EndIN_Days"].ToString();
                double eINdays = Convert.ToDouble(eIndays_str);
                string ss = "";

                if (ShiftType == "GENERAL")
                {

                    //edited here
                    ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                    ss = ss + " Where LT.Compcode='" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + "And LT.TimeIN >='" + date1.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    ss = ss + "And LT.TimeIN <='" + date2.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    ss = ss + "AND EM.ShiftType='" + ShiftType + "'and EM.IsNonAdmin='1'";
                    ss = ss + "And EM.IsActive='yes' Group By LT.MachineID Order By Min(LT.TimeIN)";
                }


                else if (ShiftType == "SHIFT")
                {

                    //ss = "select MachineID,Min(TimeIN) as [TimeIN] from LogTime_IN";
                    //ss = ss + " Where Compcode= '" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    //ss = ss + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    //ss = ss + " And TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["StartIN"].ToString() + "' ";
                    //ss = ss + " And TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["EndIN"].ToString() + "'";
                    //ss = ss + " Group By MachineID Order By Min(TimeIN)";

                    ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                    ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And EM.IsActive='yes' And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["StartIN"].ToString() + "' ";
                    ss = ss + " And LT.TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["EndIN"].ToString() + "'";
                    ss = ss + " and EM.IsNonAdmin='1'";
                    ss = ss + " Group By LT.MachineID Order By Min(LT.TimeIN)";
                }
                else
                {
                    ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                    ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And EM.IsActive='yes' And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["StartIN"].ToString() + "' ";
                    ss = ss + " And LT.TimeIN <='" + date2.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["EndIN"].ToString() + "' ";
                    ss = ss + " AND EM.ShiftType='" + ShiftType + "' and EM.IsNonAdmin='1'";
                    ss = ss + " Group By LT.MachineID Order By Min(LT.TimeIN)";
                }


                mdatatable = objdata.ReturnMultipleValue(ss);

                if (mdatatable.Rows.Count > 0)
                {
                    string MachineID;

                    for (int iRow = 0; iRow < mdatatable.Rows.Count; iRow++)
                    {
                        Boolean chkduplicate = false;
                        chkduplicate = false;

                        for (int ia = 0; ia < AutoDTable.Rows.Count; ia++)
                        {
                            string id = mdatatable.Rows[iRow]["MachineID"].ToString();

                            if (id == AutoDTable.Rows[ia][19].ToString())
                            {
                                chkduplicate = true;
                            }
                        }
                        if (chkduplicate == false)
                        {
                            AutoDTable.NewRow();
                            AutoDTable.Rows.Add();


                            // AutoDTable.Rows.Add();

                            // AutoDTable.Rows[mStartINRow][0] = iRow + 1;
                            AutoDTable.Rows[mStartINRow][3] = LocalDT.Rows[iTabRow]["ShiftDesc"].ToString();
                            AutoDTable.Rows[mStartINRow][16] = SessionCompanyName.ToString();
                            AutoDTable.Rows[mStartINRow][17] = SessionLocationName.ToString();
                            AutoDTable.Rows[mStartINRow][14] = date1.ToString("dd/MM/yyyy");
                            if (ShiftType == "SHIFT")
                            {
                                string str = mdatatable.Rows[iRow][1].ToString();
                                // string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1])
                                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mdatatable.Rows[iRow]["TimeIN"]);
                            }
                            else
                            {
                                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mdatatable.Rows[iRow]["TimeIN"]);
                            }


                            MachineID = mdatatable.Rows[iRow]["MachineID"].ToString();

                            ss = UTF8Decryption(MachineID.ToString());

                            //AutoDTable.Rows[iRow][9] = ss.ToString();


                            AutoDTable.Rows[mStartINRow][9] = ss.ToString();

                            AutoDTable.Rows[mStartINRow][19] = MachineID.ToString();
                            mStartINRow += 1;
                        }
                    }
                }

                DataTable nnDatatable = new DataTable();

                SSQL = "";
                SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT  Where Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " and LocCode = ' " + SessionLcode + "'";
                if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "SHIFT")
                {
                    SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                    SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                }
                else
                {
                    SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                    SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                }
                SSQL = SSQL + " Group By MachineID";
                SSQL = SSQL + " Order By Max(TimeOUT)";

                mdatatable = objdata.ReturnMultipleValue(SSQL);


                string InMachine_IP;
                DataTable mLocalDS_out = new DataTable();

                for (int iRow2 = mStartOUTRow; iRow2 < AutoDTable.Rows.Count; iRow2++)
                {
                    string mach = AutoDTable.Rows[iRow2][19].ToString();

                    InMachine_IP = mach.ToString();

                    SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "SHIFT1")
                    {
                        SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                        SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Asc";
                    }
                    else
                    {
                        SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                        SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Asc";
                    }
                    mLocalDS_out = objdata.ReturnMultipleValue(SSQL);

                    if (mLocalDS_out.Rows.Count <= 0)
                    {

                    }
                    else
                    {
                        if (AutoDTable.Rows[iRow2][19].ToString() == mLocalDS_out.Rows[0][0].ToString())
                        {
                            AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1]);
                        }

                    }
                    // Above coddings are correct:

                    Time_IN_Str = "";
                    Time_OUT_Str = "";
                    Date_Value_Str = string.Format(Date, "yyyy/MM/dd");


                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";



                    mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

                    if (AutoDTable.Rows[iRow2][3].ToString() == "SHIFT1" || AutoDTable.Rows[iRow2][3].ToString() == "SHIFT2")
                    {


                        InTime_Check = Convert.ToDateTime(AutoDTable.Rows[iRow2][7].ToString());
                        InToTime_Check = InTime_Check.AddHours(2);





                        ////string s1 =  string.Format("{0:hh:mm tt}",InTime_Check.ToString());

                        ////string s2 = string.Format("{0:hh:mm tt}", InToTime_Check.ToString());

                        string s1 = InTime_Check.ToString("HH:mm:ss");
                        string s2 = InToTime_Check.ToString("HH:mm:ss");


                        string InTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InTime_Check);
                        string InToTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InToTime_Check);
                        InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
                        From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                        InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
                        To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;


                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + From_Time_Str + "'";
                        SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";

                        DS_Time = objdata.ReturnMultipleValue(SSQL);

                        if (DS_Time.Rows.Count != 0)
                        {
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' ";
                            SSQL = SSQL + " And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                            DS_InTime = objdata.ReturnMultipleValue(SSQL);

                            if (DS_InTime.Rows.Count != 0)
                            {
                                Final_InTime = DS_InTime.Rows[0][0].ToString();
                                SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And ShiftDesc like '%SHIFT%'";
                                Shift_DS = objdata.ReturnMultipleValue(SSQL);
                                Shift_Check_blb = false;



                                for (int k = 0; k < Shift_DS.Rows.Count; k++)
                                {
                                    string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                    int b = Convert.ToInt16(a.ToString());
                                    Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                    string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                    int b1 = Convert.ToInt16(a1.ToString());
                                    Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                    ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                                    ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                                    EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());
                                    if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                    {
                                        Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                    }
                                }

                                if (Shift_Check_blb == true)
                                {
                                    AutoDTable.Rows[iRow2][3] = Final_Shift.ToString();
                                    AutoDTable.Rows[iRow2][16] = SessionCompanyName.ToString();
                                    AutoDTable.Rows[iRow2][17] = SessionLocationName.ToString();
                                    AutoDTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");



                                    AutoDTable.Rows[iRow2][7] = string.Format("{0:hh:mm tt}", Final_InTime);

                                    // AutoDTable.Rows[iRow2][7] = String.Format("HH:mm:ss", Final_InTime);

                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "'";
                                    mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

                                    if (Final_Shift == "SHIFT2")
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
                                    }
                                    else
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "17:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                                    }
                                    DS_Time = objdata.ReturnMultipleValue(SSQL);
                                    if (DS_Time.Rows.Count != 0)
                                    {
                                        AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);


                                    }
                                }

                                else
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' Order by TimeOUT Asc";
                                    DS_Time = objdata.ReturnMultipleValue(SSQL);

                                    if (DS_Time.Rows.Count != 0)
                                    {
                                        AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);


                                    }

                                }
                            }
                            else
                            {
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00' Order by TimeOUT Asc";
                                DS_Time = objdata.ReturnMultipleValue(SSQL);

                                if (DS_Time.Rows.Count != 0)
                                {
                                    AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);



                                }
                            }
                        }
                        else
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                        }
                    }


                    else
                    {
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                    }



                    mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);
                    String Emp_Total_Work_Time_1 = "00:00";
                    if (mLocalDS_INTAB.Rows.Count > 1)
                    {
                        for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

                            if (mLocalDS_OUTTAB.Rows.Count > tin)
                            {
                                Time_OUT_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                            {
                                Time_OUT_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_OUT_Str = "";
                            }

                            TimeSpan ts4;
                            ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (LocalDT.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                            }

                            if (Time_IN_Str == "" || Time_OUT_Str == "")
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date3 = Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = Convert.ToDateTime(Time_OUT_Str);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }

                                }
                                else
                                {
                                    ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                            }

                            AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;
                            AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                            OThour_Str = Emp_Total_Work_Time_1.Split(':');
                            OT_Hour = Convert.ToInt32(OThour_Str[0]);
                            OT_Min = Convert.ToInt32(OThour_Str[1]);
                            if (OT_Hour >= 9)
                            {
                                OT_Time = OT_Hour - 8;
                                OT_Hour1 = Convert.ToString(OT_Time) + " : " + Convert.ToString(OT_Min);
                                AutoDTable.Rows[iRow2][15] = OT_Hour1;
                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][15] = "00:00";
                            }

                        }
                    }
                    else
                    {
                        TimeSpan ts4;
                        ts4 = Convert.ToDateTime(String.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS_INTAB.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout <= mLocalDS_OUTTAB.Rows.Count; tout++)
                        {
                            if (mLocalDS_OUTTAB.Rows.Count <= 0)
                            {
                                Time_OUT_Str = "";
                            }
                            else
                            {
                                Time_OUT_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                            }

                        }
                        if (Time_IN_Str == "" || Time_OUT_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4 = System.Convert.ToDateTime(Time_OUT_Str);
                            TimeSpan ts1;
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                            }
                            else
                            {
                                ts4 = ts4.Add(ts1);
                                string s1 = ts4.Minutes.ToString();
                                if (s1.Length == 1)
                                {
                                    string ts_str = "0" + ts4.Minutes;
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts_str;
                                }
                                else
                                {
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                }
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }

                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                            }
                            AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;

                            AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                            OThour_Str = Emp_Total_Work_Time_1.Split(':');
                            OT_Hour = Convert.ToInt32(OThour_Str[0]);
                            OT_Min = Convert.ToInt32(OThour_Str[1]);
                            if (OT_Hour >= 9)
                            {
                                OT_Time = OT_Hour - 8;
                                OT_Hour1 = Convert.ToString(OT_Time) + " : " + Convert.ToString(OT_Min);
                                AutoDTable.Rows[iRow2][15] = OT_Hour1;
                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][15] = "00:00";
                            }


                        }

                    }
                    // Bellow codings are correct:

                }
                for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
                {
                    string MID = AutoDTable.Rows[iRow2][19].ToString();

                    //SSQL = "";
                    //SSQL = "select Distinct isnull(EM.MachineID_Encrypt,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
                    //SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
                    //SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
                    //SSQL = SSQL + ",isnull(EM.CatName,'') as [CatName], isnull(EM.SubCatName,'') as [SubCatName],isnull(EM.Designation,'') as Desgination";
                    //SSQL = SSQL + ",DM.DeptCode from Employee_Mst EM inner Join Department_Mst DM on DM.DeptName=EM.DeptName Where EM.MachineID_Encrypt='" + MID + "' And EM.Compcode='" +SessionCcode+ "'";
                    //SSQL = SSQL + " And EM.LocCode='" +SessionLcode+ "' And EM.IsActive='Yes'" + " order by DM.DeptCode Asc";

                    SSQL = "";
                    SSQL = "select Distinct isnull(MachineID_Encrypt,'') as [MachineID],isnull(DeptName,'') as [DeptName]";
                    SSQL = SSQL + ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
                    SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
                    SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName],isnull(Designation,'') as Desgination";
                    SSQL = SSQL + " from Employee_Mst  Where MachineID_Encrypt='" + MID + "' And Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'" + " order by ExistingCode Asc";

                    mEmployeeDS = objdata.ReturnMultipleValue(SSQL);

                    if (mEmployeeDS.Rows.Count > 0)
                    {
                        for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                        {

                            ss = UTF8Decryption(MID.ToString());
                            AutoDTable.Rows[iRow2][19] = ss.ToString();
                            string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();

                            //if (AutoDTable.Rows[iRow2][9] == mEmployeeDS.Rows[iRow1]["MachineID"])
                            //{


                            AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                            AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
                            AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                            AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                            AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                            AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                            AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                            AutoDTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");
                            AutoDTable.Rows[iRow2][16] = SessionCompanyName.ToString();
                            AutoDTable.Rows[iRow2][17] = SessionLocationName.ToString();
                            AutoDTable.Rows[iRow2][18] = mEmployeeDS.Rows[iRow1]["Desgination"];

                            //}
                        }
                    }
                }

            }
        }
        //  }


        ds.Tables.Add(AutoDTable);

        //report.Load(Server.MapPath("crystal/DayWise1.rpt"));
        //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
        //report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        //CrystalReportViewer1.ReportSource = report;


    }


    public static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    public static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }
    private void CloseReports(ReportDocument reportDocument)
    {
        Sections sections = reportDocument.ReportDefinition.Sections;
        foreach (Section section in sections)
        {
            ReportObjects reportObjects = section.ReportObjects;
            foreach (ReportObject reportObject in reportObjects)
            {
                if (reportObject.Kind == ReportObjectKind.SubreportObject)
                {
                    SubreportObject subreportObject = (SubreportObject)reportObject;
                    ReportDocument subReportDocument = subreportObject.OpenSubreport(subreportObject.SubreportName);
                    subReportDocument.Close();
                }
            }
        }
        reportDocument.Close();
    }
    //private void Page_Unload(object sender, EventArgs e)
    //{
    //    if (report.FileName != "")
    //    {
    //        CloseReports(report);
    //        report.Dispose();
    //        CrystalReportViewer1.Dispose();
    //        CrystalReportViewer1 = null;
    //    }

    //}


}
