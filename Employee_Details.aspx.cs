﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class EmployeeDetails : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    UserCreationClass objUsercreation = new UserCreationClass();
    DataTable dtdDisplay = new DataTable();

    static int ss;
    bool ErrFlag = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
                     
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Employee Details";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");

                CreateUserDisplay();

                CreateUserDesginationDisplay();
                CreateEmployeeType();

                CreateWagesType();
                //Dropdown_Company();
                //Dropdown_Location();
            }
        }

    }
    protected void btnSaveDept_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable dtt = new DataTable();



        if (btnSaveDept.Text == "Update")
        {
            dt = objdata.DepartmmentUpdate(txtDepartmentName.Text, ss);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Sussessfully');", true);
        }
        else
        {

            dtt = objdata.CheckDepartment_AlreadyExist(txtDepartmentName.Text);
            if (dtt.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department Already Exist');", true);
                txtDepartmentName.Text = "";
            }
            else
            {
                dt = objdata.DepartmentRegister(txtDepartmentName.Text);
                txtDepartmentName.Text = "";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Save Successfully');", true);
            }
        }
        CreateUserDisplay();
        btnSaveDept.Text = "Save";
        clear();
    }

    public void CreateUserDisplay()
    {
        DataTable dtDisplay = new DataTable();

        dtdDisplay = objdata.DepartmentDisplay();
        if (dtdDisplay.Rows.Count > 0)
        {
            rptrCustomer.DataSource = dtdDisplay;
            rptrCustomer.DataBind();
        }
    }

    protected void BtnClear_Click(object sender, EventArgs e)
    {
        clear();
        btnSaveDept.Text = "Save";

    }

    public void clear()
    {
        txtDepartmentName.Text = "";
    }

    protected void rptrCustomer_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(id);
                break;
            case ("Edit"):
                mp1.Show();
                EditRepeaterData(id);
                break;
        }
    }

    private void EditRepeaterData(string DeptCode)
    {
        string ss = DeptCode;
        DataTable Dtd = new DataTable();
        Dtd = objdata.EditDepartment(ss);
        if (Dtd.Rows.Count > 0)
        {
            txtDepartmentName.Text = Dtd.Rows[0]["DeptName"].ToString();
        }
        btnSaveDept.Text = "Update";

    }

    private void DeleteRepeaterData(string DeptCode)
    {
        int ss = Convert.ToInt16(DeptCode);

        DataTable DtdCheck = new DataTable();
        DtdCheck = objdata.CheckEmployeeDepartment(ss);

        if (DtdCheck.Rows.Count > 0)
        {
            string DeptName = DtdCheck.Rows[0]["DeptName"].ToString();

            DataTable CheckInEmployee = new DataTable();
            CheckInEmployee = objdata.CheckingEmployee(DeptName);
            if (CheckInEmployee.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Department Name Allocated to Employee');", true);
            }

            else
            {
                DataTable Dtd = new DataTable();
                Dtd = objdata.DeleteDepartment(ss);
                CreateUserDisplay();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
            }
        }

        CreateUserDisplay();
    }
   



    public void CreateUserDesginationDisplay()
    {
        DataTable dtDesginationDisplay = new DataTable();
        dtDesginationDisplay = objdata.DesginationDisplay();

        if (dtDesginationDisplay.Rows.Count > 0)
        {
            RepeaterDesign.DataSource = dtDesginationDisplay;
            RepeaterDesign.DataBind();
        }
    }


    protected void btnSaveDesgination_Click(object sender, EventArgs e)
    {
        if (txtDesginationName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Desgination Name');", true);
            ErrFlag = true;
        }
        else
        {

            DataTable dt = new DataTable();
            DataTable dtt = new DataTable();


            if (btnSaveDesgination.Text == "Update")
            {
                dt = objdata.DesignationUpdate(txtDesginationName.Text, ss);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Sussessfully');", true);
            }
            else
            {
                dtt = objdata.CheckEmpType_AlreadyExist(txtDesginationName.Text);
                if (dtt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Desgination Name Already Exist');", true);
                    txtDesginationName.Text = "";
                }
                else
                {
                    dt = objdata.DesginationRegister(txtDesginationName.Text);
                    txtDesginationName.Text = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Save Successfully');", true);
                }
            }

            CreateUserDesginationDisplay();
            btnSaveDesgination.Text = "Save";
            ClearDegination();
        }
    }
    protected void btnClearDesgination_Click(object sender, EventArgs e)
    {
        ClearDegination();

        btnSaveDesgination.Text = "Save";
    }

    public void ClearDegination()
    {
        txtDesginationName.Text = "";
    }

    protected void RepeaterDesign_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterDataDesgination(id);
                break;
            case ("Edit"):
                MPE1.Show();
                EditRepeaterDataDesgination(id);
                break;
        }
    }

    private void EditRepeaterDataDesgination(string DeptCode)
    {
        ss = Convert.ToInt16(DeptCode);
        DataTable Dtd = new DataTable();
        Dtd = objdata.EditDesignation(ss);
        if (Dtd.Rows.Count > 0)
        {
            txtDesginationName.Text = Dtd.Rows[0]["DesignName"].ToString();
        }

        btnSaveDesgination.Text = "Update";
    }

    private void DeleteRepeaterDataDesgination(string DeptCode)
    {
        int ss = Convert.ToInt16(DeptCode);

        DataTable DtdCheck = new DataTable();
        DtdCheck = objdata.CheckEmployeeDesignation(ss);

        if (DtdCheck.Rows.Count > 0)
        {
            string DesignName = DtdCheck.Rows[0]["DesignName"].ToString();

            DataTable CheckInEmployee = new DataTable();
            CheckInEmployee = objdata.CheckingDesignEmployee(DesignName);
            if (CheckInEmployee.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Department Name Allocated to Employee');", true);
            }
            else
            {
                DataTable Dtd = new DataTable();
                Dtd = objdata.DeleteDesgination(ss);
                CreateUserDesginationDisplay();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
            }
        }
    }




    public void CreateEmployeeType()
    {
        DataTable dtEmployeeTypeDisplay = new DataTable();
        dtEmployeeTypeDisplay = objdata.EmployeeTypeDisplay();

        if (dtEmployeeTypeDisplay.Rows.Count > 0)
        {
            rptrEmplyeeType.DataSource = dtEmployeeTypeDisplay;
            rptrEmplyeeType.DataBind();
        }


    }

  

    protected void btnSaveEmployeeType_Click(object sender, EventArgs e)
    {
        if (txtEmployeeType.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Emplyee Type');", true);
            ErrFlag = true;
        }
        else
        {


            DataTable dt = new DataTable();
            DataTable dtt = new DataTable();
            if (btnSaveEmployeeType.Text == "Update")
            {
                dt = objdata.EmployeeTypeUpdate(txtEmployeeType.Text, ss);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Sussessfully');", true);
            }
            else
            {
                dtt = objdata.CheckEmployeeType_AlreadyExist(txtEmployeeType.Text);
                if (dtt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Type Already Exist');", true);
                    txtDesginationName.Text = "";
                }

                else
                {
                    dt = objdata.EmployeeTypeRegister(txtEmployeeType.Text);
                    txtEmployeeType.Text = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Save Successfully');", true);
                }
            }
            CreateEmployeeType();
            btnSaveEmployeeType.Text = "Save";
            ClearEmployeeType();
        }
    }
    protected void BtnClearEmployeeType_Click(object sender, EventArgs e)
    {
        ClearEmployeeType();
        btnSaveEmployeeType.Text = "Save";
        
    }

    public void ClearEmployeeType()
    {
        txtEmployeeType.Text = "";
    }

    protected void rptrEmplyeeType_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterDataEmployeeType(id);
                break;
            case ("Edit"):
                MPE2.Show();
                EditRepeaterDataEmployeeType(id);
                break;
        }
    }

    private void EditRepeaterDataEmployeeType(string TypeID)
    {
        ss = Convert.ToInt16(TypeID);
        DataTable Dtd = new DataTable();
        Dtd = objdata.EditEmployeeType(ss);
        if (Dtd.Rows.Count > 0)
        {
            txtEmployeeType.Text = Dtd.Rows[0]["TypeName"].ToString();
        }
        btnSaveEmployeeType.Text = "Update";
    }

    private void DeleteRepeaterDataEmployeeType(string TypeID)
    {
        int ss = Convert.ToInt16(TypeID);

        DataTable DtdCheck = new DataTable();
        DtdCheck = objdata.EditEmployeeType(ss);
        if (DtdCheck.Rows.Count > 0)
        {
            string TypeName = DtdCheck.Rows[0]["TypeName"].ToString();
            DataTable CheckInEmployee = new DataTable();
            CheckInEmployee = objdata.CheckingEmployeeType(TypeName);
            if (CheckInEmployee.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' Employee Type Allocated to Employee');", true);
            }
            else
            {
                DataTable Dtd = new DataTable();
                Dtd = objdata.DeleteEmployeeeType(ss);
                CreateEmployeeType();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
            }
        }
        CreateEmployeeType();

    }

    protected void btnSaveWagesType_Click(object sender, EventArgs e)
    {
        if (txtEmployeeType.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
            ErrFlag = true;
        }
        else
        {
            DataTable dtt = new DataTable();
            if (btnSaveWagesType.Text == "Update")
            {
                dtt = objdata.WagesTypeUpdate(txtWagesType.Text, ss);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Sussessfully');", true);
            }
            else
            {
                DataTable dt = new DataTable();
                DataTable dtble = new DataTable();

                dtble = objdata.CheckWagesType_AlreadyExist(txtWagesType.Text);
                if (dtble.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('EmployeeType Already Exist');", true);
                    txtWagesType.Text = "";
                }
                else
                {
                    dt = objdata.WagesTypeRegister(txtWagesType.Text);
                    txtWagesType.Text = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Save Successfully');", true);
                }

                CreateWagesType();
            }
        }
    }

  
    protected void BtnClearWagesType_Click(object sender, EventArgs e)
    {
        ClearWages();
        btnSaveWagesType.Text = "Save";
    }

    public void ClearWages()
    {
        txtWagesType.Text = "";
    }

    public void CreateWagesType()
    {
        DataTable dtWagesTypeDisplay = new DataTable();
        dtWagesTypeDisplay = objdata.WagesTypeDisplay();

        if (dtWagesTypeDisplay.Rows.Count > 0)
        {
            rptrWagesType.DataSource = dtWagesTypeDisplay;
            rptrWagesType.DataBind();
        }

    }

    protected void rptrWagesType_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterDataWagesType(id);
                break;
            case ("Edit"):
                MPE3.Show();
                EditRepeaterDataWagesType(id);
                break;
        }
    }

    private void EditRepeaterDataWagesType(string WgsTypeNo)
    {
        string ss = WgsTypeNo;
        DataTable Dtd = new DataTable();
        Dtd = objdata.EditWagesType(ss);
        if (Dtd.Rows.Count > 0)
        {
            txtWagesType.Text = Dtd.Rows[0]["WagesTypeName"].ToString();
        }
        //btnSaveEmployeeType.Text = "Update";
        btnSaveWagesType.Text = "Update";

    }
    private void DeleteRepeaterDataWagesType(string WgsTypeNo)
    {
        string ss = WgsTypeNo;
        DataTable DtdCheck = new DataTable();
        DtdCheck = objdata.EditWagesType(ss);
        if (DtdCheck.Rows.Count > 0)
        {
            string TypeName = DtdCheck.Rows[0]["WagesTypeName"].ToString();
            DataTable CheckInWages = new DataTable();
            CheckInWages = objdata.CheckingWagesType(TypeName);
            if (CheckInWages.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' Wages Type Allocated to Employee');", true);
            }
            else
            {
                DataTable Dtd = new DataTable();
                Dtd = objdata.DeleteWagesType(ss);
                CreateWagesType();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
            }
        }
        CreateWagesType();

    }

   
    
}
    




