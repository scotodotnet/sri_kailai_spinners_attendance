﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;



public partial class LeaveDetail_Master : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    string SSQL;
    bool ErrFlag = false;
    static string Dept;
    static string ss;
    static string tt;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
           

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Leave Master";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");

                CreateUserDisplay();
                Financial_Year();
            }
        }
    }


    public void Financial_Year()
    {
        String CurrentYear1;
        int CurrentYear;
        CurrentYear1 = DateTime.Now.Year.ToString();
        CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());
        if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
        {
            CurrentYear = CurrentYear - 1;
        }
        for (int i = 0; i < 1; i++)
        {
            tt = CurrentYear1;

            // tt = (CurrentYear1 + "-" + (CurrentYear - 1));
            txtFinancialYear.Text = tt.ToString();
            //txtFinancialYear.Items.Add(tt.ToString());
            CurrentYear = CurrentYear - 1;
            string cy = Convert.ToString(CurrentYear);
            CurrentYear1 = cy;
            
        }
    }


   

   

    

    public void CreateUserDisplay()
    {
        DataTable dtdDisplay = new DataTable();
        SSQL = "select*from LeaveType_Mst";
        dtdDisplay = objdata.ReturnMultipleValue(SSQL);
        if (dtdDisplay.Rows.Count > 0)
        {
            rptrCustomer.DataSource = dtdDisplay;
            rptrCustomer.DataBind();
        }

    }


    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string Dept = commandArgs[0];
        string LeaveType = commandArgs[1];
  
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(Dept,LeaveType);
                break;
            case ("Edit"):
                EditRepeaterData(Dept, LeaveType);
                break;
        }
    }

    private void EditRepeaterData(string Dept,  string SNo)
    {
        ss = Convert.ToString(SNo);
        DataTable Dtd = new DataTable();
        SSQL = "select*from LeaveType_Mst where CatName='" + Dept + "' and SNo='" + SNo + "'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        if (Dtd.Rows.Count > 0)
        {
            DataTable dt = new DataTable();
            string dept = Dtd.Rows[0]["CatName"].ToString();
            SSQL = "select CatName from Employee_Mst where CatName='" + dept + "'";
            dt = objdata.ReturnMultipleValue(SSQL);
            if (dt.Rows.Count > 0)
            {
                string deptt = dt.Rows[0]["CatName"].ToString();
                if (deptt.ToString() != "")
                {
                    ddlCate.SelectedValue = deptt;
                }
            }
        }

        txtleaveType.Text = Dtd.Rows[0]["LeaveType"].ToString();
        txtdays.Text = Dtd.Rows[0]["Days"].ToString();
        txtFinancialYear.Text = Dtd.Rows[0]["FinancialYear"].ToString();
        CreateUserDisplay();
        btnSave.Text = "Update";
        
    }

    private void DeleteRepeaterData(string Dept, string SNo)
    {
        DataTable Dtd = new DataTable();
        SSQL = "Delete LeaveType_Mst where CatName='" + Dept + "' and SNo='" + SNo + "'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        CreateUserDisplay();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
    }




    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        if (ddlCate.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Department');", true);
            ErrFlag = true;
        }
        else
        {
            if (btnSave.Text == "Update")
            {
                DataTable dtdLMst = new DataTable();
                SSQL = "update LeaveType_Mst set LeaveType='" + txtleaveType.Text + "',Days='" + txtdays.Text + "',FinancialYear='" + txtFinancialYear.Text + "' where CatName='" + ddlCate.SelectedItem.Text + "' and SNo = '" + ss + "'";
                dtdLMst = objdata.ReturnMultipleValue(SSQL);
                CreateUserDisplay();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Update Successfully');", true);
                ErrFlag = true;
                btnSave.Text = "Save";
                clear();
            }
            else
            {
                DataTable dtdLMst = new DataTable();
                SSQL = "select * from LeaveType_Mst where CatName='" + ddlCate.SelectedItem.Text + "' and LeaveType='" + txtleaveType.Text + "' and FinancialYear ='" + txtFinancialYear.Text + "'";
                dtdLMst = objdata.ReturnMultipleValue(SSQL);
                if (dtdLMst.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Leave Master Already Exist');", true);
                    ErrFlag = true;
                }
                else
                {
                    SSQL = "insert into LeaveType_Mst(CatName,LeaveType,Days,FinancialYear,CompCode,LocCode)values('" + ddlCate.SelectedItem.Text + "','" + txtleaveType.Text + "'";
                    SSQL = SSQL + ",'" + txtdays.Text + "','" + txtFinancialYear.Text + "','" + SessionCcode + "','" + SessionLcode + "')";
                    objdata.ReturnMultipleValue(SSQL);
                    CreateUserDisplay();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Leave Master Save Successfully');", true);
                    ErrFlag = true;
                    clear();
                }
            }
        }
    }
    protected void BtnClear_Click(object sender, EventArgs e)
    {
        clear();
    }

    public void clear()
    {
        ddlCate.SelectedIndex = 0;
        txtleaveType.Text = "";
        txtdays.Text = "";
    }
}
