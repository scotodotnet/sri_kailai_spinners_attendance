﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using System.Net.Mail;
using CrystalDecisions.Shared;
using System.Text;


public partial class MailPage : System.Web.UI.Page
{
    MailMessage mail = new MailMessage();
    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    ReportDocument rd = new ReportDocument();
     ReportDocument report = new ReportDocument();
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionUserType;
    DataTable AutoDTable = new DataTable();
    BALDataAccess objdata = new BALDataAccess();

    DateTime dtime;
    DateTime dtime1;
    DateTime date1;
    DateTime date2 = new DateTime();
    DataTable table = new DataTable();
    string Encry_MachineID;
    DataTable AutoDataTable = new DataTable();
    bool isPresent = false;
    string Time_IN_Str;
    string Time_Out_Str;
    int j = 0;
    Double time_Check_dbl = 0;
    string Date_Value_Str1;
    DateTime InTime_Check;
    DateTime InTime_Check1;
    DateTime InToTime_Check;
    TimeSpan InTime_TimeSpan;
    string From_Time_Str;

    DataTable mLocalDS = new DataTable();


    string Machine_ID_Str = "";
    string OT_Week_OFF_Machine_No = null;
    string Date_Value_Str = "";
    string Total_Time_get = "";
    string Final_OT_Work_Time_1 = "00:00";
    // Decimal Month_Mid_Join_Total_Days_Count;
    DateTime NewDate1;
    string[] Time_Minus_Value_Check;

    //DateTime InTime_Check = default(DateTime);
    //DateTime InToTime_Check = default(DateTime);
    //TimeSpan InTime_TimeSpan = default(TimeSpan);
    //string From_Time_Str = "";
    string To_Time_Str = "";
    DataTable DS_Time = new DataTable();
    DataTable DS_InTime = new DataTable();
    string Final_InTime = "";
    string Final_OutTime = "";
    string Final_Shift = "";
    DataTable Shift_DS_Change = new DataTable();
    Int32 K = 0;
    bool Shift_Check_blb = false;
    string Shift_Start_Time_Change = null;
    string Shift_End_Time_Change = null;
    string Employee_Time_Change = "";
    DateTime ShiftdateStartIN_Change = default(DateTime);
    DateTime ShiftdateEndIN_Change = default(DateTime);
    DateTime EmpdateIN_Change = default(DateTime);

    string Fin_Year;
    string Months_Full_Str;
    //string Months_Full_Str;
    string Date_Col_Str;
    string Month_Int;
    string Spin_Wages;
    string Wages;
    string Spin_Machine_ID_Str;
    string[] Att_Date_Check;
    string Att_Already_Date_Check;
    string Att_Year_Check;
    string Month_Name_Change;
    string halfPresent;


    //string Att_Year_Check = "";
    string Token_No_Get;
    TimeSpan ts4 = new TimeSpan();

    Decimal Total_Days_Count;
    double Final_Count;

    double Present_Count;
    double EHours_Count;
    decimal Fixed_Work_Days;
    decimal Late_Days_Count;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;
    double FullNight_Shift_Count;
    double NFH_Week_Off_Minus_Days;
    decimal Oneday_salary;
    decimal Hrsalary;

    Boolean NFH_Day_Check = false;
    Boolean Check_Week_Off_DOJ = false;
    int aintI = 1;
    int aintK = 1;
    int aintCol;
    int Appsent_Count;
    int Impro_Punch_Count;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionCompanyName;
    string SessionLocationName;

    //String Att_Already_Date_Check;
    //String Att_Year_Check;
    //String[] Att_Date_Check;

    int shiftCount = 0;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string todate;
    string fromdate;
    string SSQL;
    string Emp_WH_Day;

    string OT_Final_Count;
    //string Spin_Machine_ID_Str = "";

    DateTime WH_DOJ_Date_Format;
    DateTime Week_Off_Date;
    DateTime Date1;
    DataTable Datacells = new DataTable();
    DataTable mEmployeeDS = new DataTable();
    DataTable dsEmployee = new DataTable();
    // DataTable AutoDataTable = new DataTable();
    Double NFH_Final_Disp_Days;

    DataTable LocalDT = new DataTable();
    DataTable Shift_DS = new DataTable();
    string Datestr = "";
    string Datestr1 = "";
    DataTable mdatatable = new DataTable();
    string Time_OUT_Str;
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Shift_Start_Time;
    string Shift_End_Time;
    DateTime ShiftdateStartIN;
    DateTime ShiftdateEndIN;
    string[] OThour_Str;
    int OT_Hour;
    int OT_Min;
    int OT_Time;
    string OT_Hour1 = "";
    string mUser = "";
    //DateTime InTime_Check;
    //DateTime InToTime_Check;
    DateTime EmpdateIN;

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = "SKS";
        SessionLcode = "UNIT I";
        SessionAdmin = "1";
        SessionCompanyName = "SRI KAILAI SPINNERS";
        SessionLocationName = "UNIT I";
        SessionUserType = "1";
        Date1 = DateTime.Now.AddDays(-1);
        DateTime Date2 = DateTime.Now.AddDays(-1);

        DateTime dateGiven = DateTime.Now;
        dateGiven = dateGiven.AddMonths(-1);
        DateTime thisMonth=new DateTime(dateGiven.Year, dateGiven.Month, 1); 
        //thisMonth.ToString("yyyy-MM-01");
        //fromdate = "01-" + dateGiven.Month + "-" + dateGiven.Year;
        //todate = thisMonth.AddMonths(1).AddDays(-1).Day + "-" + dateGiven.Month + "-" + dateGiven.Year;
        fromdate = Convert.ToString(Date1.ToString("dd/MM/yyyy"));
        todate = Convert.ToString(Date2.ToString("dd/MM/yyyy"));

        //fromdate = fromdate;
        //todate = todate;


        //
        //Daywise_Mail();
        //Mismatch();
        Impropare_Punch(); 

    }

    public void Daywise_Mail()
    {
        DataSet ds = new DataSet();
                DataTable AutoDTable = new DataTable();


                AutoDTable.Columns.Add("SNo");
                AutoDTable.Columns.Add("Dept");
                AutoDTable.Columns.Add("Type");
                AutoDTable.Columns.Add("Shift");
                AutoDTable.Columns.Add("EmpCode");
                AutoDTable.Columns.Add("ExCode");
                AutoDTable.Columns.Add("Name");
                AutoDTable.Columns.Add("TimeIN");
                AutoDTable.Columns.Add("TimeOUT");
                AutoDTable.Columns.Add("MachineID");
                AutoDTable.Columns.Add("Category");
                AutoDTable.Columns.Add("SubCategory");
                AutoDTable.Columns.Add("TotalMIN");
                AutoDTable.Columns.Add("GrandTOT");
                AutoDTable.Columns.Add("ShiftDate");
                AutoDTable.Columns.Add("OTHour");
                AutoDTable.Columns.Add("CompanyName");
                AutoDTable.Columns.Add("LocationName");
                AutoDTable.Columns.Add("Desgination");
                AutoDTable.Columns.Add("MachineIDNew");
                AutoDTable.Columns.Add("Status");

                //if (mReportName.Text == "DAY ATTENDANCE - DAY WISE :: BELOW 4 HOURS")
                //{
                DataTable dtIPaddress = new DataTable();
                dtIPaddress = objdata.IPAddressForAll(SessionCcode.ToString(), SessionLcode.ToString());

                if (dtIPaddress.Rows.Count > 0)
                {
                    for (int i = 0; i < dtIPaddress.Rows.Count; i++)
                    {
                        if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                        {
                            mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                        }
                        else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                        {
                            mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                        }
                    }
                }

                SSQL = "";
                SSQL = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
                SSQL = SSQL + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
                SSQL = SSQL + " from Shift_Mst Where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
       
                //else
                //{
                //    SSQL = SSQL + " And ShiftDesc like '" + ddlShiftType + "%'";

                //}   
                SSQL = SSQL + " Order By shiftDesc";
                LocalDT = objdata.ReturnMultipleValue(SSQL);


                string ShiftType;




                string ng = string.Format(Date, "MM-dd-yyyy");
                Datestr = Convert.ToDateTime(fromdate).AddDays(0).ToShortDateString();
                Datestr1 = Convert.ToDateTime(fromdate).AddDays(1).ToShortDateString();
                DateTime date1 = Convert.ToDateTime(Datestr);
                DateTime date2 = date1.AddDays(1);

                if (LocalDT.Rows.Count > 0)
                {

                   
               
                    string Start_IN = "";
                    string End_In;

                    string End_In_Days = "";
                    string Start_In_Days = "";

                    int mStartINRow = 0;
                    int mStartOUTRow = 0;

                    for (int iTabRow = 0; iTabRow < LocalDT.Rows.Count; iTabRow++)
                    {

                        if (AutoDTable.Rows.Count <= 1)
                        {
                            mStartOUTRow = 0;
                        }
                        else
                        {
                            mStartOUTRow = AutoDTable.Rows.Count - 1;
                        }

                        if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "GENERAL")
                        {
                            ShiftType = "GENERAL";
                        }
                        else
                        {
                            ShiftType = "SHIFT";
                        }


                        string sIndays_str = LocalDT.Rows[iTabRow]["StartIN_Days"].ToString();
                        double sINdays = Convert.ToDouble(sIndays_str);
                        string eIndays_str = LocalDT.Rows[iTabRow]["EndIN_Days"].ToString();
                        double eINdays = Convert.ToDouble(eIndays_str);
                        string ss = "";

                        if (ShiftType == "GENERAL")
                        {

                            //edited here
                            ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                            ss = ss + " Where LT.Compcode='" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                            ss = ss + "And LT.TimeIN >='" + date1.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                            ss = ss + "And LT.TimeIN <='" + date2.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                            ss = ss + "AND EM.ShiftType='" + ShiftType + "' ";
                            ss = ss + "And EM.IsActive='yes' Group By LT.MachineID Order By Min(LT.TimeIN)";
                        }


                        else if (ShiftType == "SHIFT")
                        {

                            //ss = "select MachineID,Min(TimeIN) as [TimeIN] from LogTime_IN";
                            //ss = ss + " Where Compcode= '" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            //ss = ss + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            //ss = ss + " And TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["StartIN"].ToString() + "' ";
                            //ss = ss + " And TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["EndIN"].ToString() + "'";
                            //ss = ss + " Group By MachineID Order By Min(TimeIN)";

                            ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                            ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                            ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                            ss = ss + " And EM.IsActive='yes' And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["StartIN"].ToString() + "' ";
                            ss = ss + " And LT.TimeIN <='" + date2.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["EndIN"].ToString() + "' ";
                            ss = ss + " AND EM.ShiftType='" + ShiftType + "' ";
                            ss = ss + " Group By LT.MachineID Order By Min(LT.TimeIN)";

                        }
                        else
                        {
                            ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                            ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                            ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                            ss = ss + " And EM.IsActive='yes' And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["StartIN"].ToString() + "' ";
                            ss = ss + " And LT.TimeIN <='" + date2.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["EndIN"].ToString() + "' ";
                            ss = ss + " AND EM.ShiftType='" + ShiftType + "' ";
                            ss = ss + " Group By LT.MachineID Order By Min(LT.TimeIN)";
                        }


                        mdatatable = objdata.ReturnMultipleValue(ss);

                        if (mdatatable.Rows.Count > 0)
                        {
                            string MachineID;

                            for (int iRow = 0; iRow < mdatatable.Rows.Count; iRow++)
                            {
                                Boolean chkduplicate = false;
                                chkduplicate = false;
                                string id = ""; //mdatatable.Rows[iRow]["MachineID"].ToString();
                                id = UTF8Decryption(mdatatable.Rows[iRow]["MachineID"].ToString());
                                for (int ia = 0; ia < AutoDTable.Rows.Count; ia++)
                                {
                                   
                                    //string id = mdatatable.Rows[iRow]["MachineID"].ToString();

                                    if (id == AutoDTable.Rows[ia][9].ToString())
                                    {
                                        chkduplicate = true;
                                    }
                                }
                                if (chkduplicate == false)
                                {
                                    AutoDTable.NewRow();
                                    AutoDTable.Rows.Add();


                                    // AutoDTable.Rows.Add();

                                    // AutoDTable.Rows[mStartINRow][0] = iRow + 1;
                                    AutoDTable.Rows[mStartINRow][3] = LocalDT.Rows[iTabRow]["ShiftDesc"].ToString();
                                    AutoDTable.Rows[mStartINRow][16] = SessionCompanyName.ToString();
                                    AutoDTable.Rows[mStartINRow][17] = SessionLocationName.ToString();
                                    AutoDTable.Rows[mStartINRow][14] = date1.ToString("dd/MM/yyyy");
                                    if (ShiftType == "SHIFT")
                                    {
                                        string str = mdatatable.Rows[iRow][1].ToString();
                                        // string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1])
                                        AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mdatatable.Rows[iRow]["TimeIN"]);
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mdatatable.Rows[iRow]["TimeIN"]);
                                    }


                                    MachineID = mdatatable.Rows[iRow]["MachineID"].ToString();

                                    ss = UTF8Decryption(MachineID.ToString());

                                    //AutoDTable.Rows[iRow][9] = ss.ToString();


                                    AutoDTable.Rows[mStartINRow][9] = ss.ToString();

                                    AutoDTable.Rows[mStartINRow][19] = MachineID.ToString();
                                    mStartINRow += 1;
                                }
                            }
                        }

                        DataTable nnDatatable = new DataTable();

                        SSQL = "";
                        SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT  Where Compcode='" + SessionCcode + "'";
                        SSQL = SSQL + " and LocCode = ' " + SessionLcode + "'";
                        if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "SHIFT")
                        {
                            SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                            SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                        }
                        else
                        {
                            SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                            SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                        }
                        SSQL = SSQL + " Group By MachineID";
                        SSQL = SSQL + " Order By Max(TimeOUT)";

                        mdatatable = objdata.ReturnMultipleValue(SSQL);


                        string InMachine_IP;
                        DataTable mLocalDS_out = new DataTable();

                        for (int iRow2 = mStartOUTRow; iRow2 < AutoDTable.Rows.Count; iRow2++)
                        {
                            string mach = AutoDTable.Rows[iRow2][19].ToString();

                            InMachine_IP = mach.ToString();

                            SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "SHIFT1")
                            {
                                SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                                SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT desc";
                            }
                            else
                            {
                                SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                                SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT desc";
                            }
                            mLocalDS_out = objdata.ReturnMultipleValue(SSQL);

                            if (mLocalDS_out.Rows.Count <= 0)
                            {

                            }
                            else
                            {
                                if (AutoDTable.Rows[iRow2][19].ToString() == mLocalDS_out.Rows[0][0].ToString())
                                {
                                    AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1]);
                                }

                            }
                            // Above coddings are correct:

                            Time_IN_Str = "";
                            Time_OUT_Str = "";
                            Date_Value_Str = string.Format(Date, "yyyy/MM/dd");

                            if (InMachine_IP == "ODUwNQ==")
                            {
                                InMachine_IP = "ODUwNQ==";
                            }
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";



                            mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

                            if (AutoDTable.Rows[iRow2][3].ToString() == "SHIFT1" || AutoDTable.Rows[iRow2][3].ToString() == "SHIFT2")
                            {
                                
                                InTime_Check = Convert.ToDateTime(AutoDTable.Rows[iRow2][7].ToString());
                                InToTime_Check = InTime_Check.AddHours(2);

                                string s1 = InTime_Check.ToString("HH:mm:ss");
                                string s2 = InToTime_Check.ToString("HH:mm:ss");
                                
                                string InTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InTime_Check);
                                string InToTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InToTime_Check);
                                InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
                                From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                                InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
                                To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;


                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + From_Time_Str + "'";
                                SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";

                                DS_Time = objdata.ReturnMultipleValue(SSQL);

                                if (DS_Time.Rows.Count != 0)
                                {
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";


                                    DS_InTime = objdata.ReturnMultipleValue(SSQL);

                                    if (DS_InTime.Rows.Count != 0)
                                    {
                                        Final_InTime = DS_InTime.Rows[0][0].ToString();
                                        SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And ShiftDesc like '%SHIFT%'";
                                        Shift_DS = objdata.ReturnMultipleValue(SSQL);
                                        Shift_Check_blb = false;



                                        for (int k = 0; k < Shift_DS.Rows.Count; k++)
                                        {
                                            string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                            int b = Convert.ToInt16(a.ToString());
                                            Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                            string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                            int b1 = Convert.ToInt16(a1.ToString());
                                            Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                            ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                                            ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                                            EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());
                                            if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                            {
                                                Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                                                Shift_Check_blb = true;
                                                
                                            }
                                        }

                                        DayOfWeek day = date1.DayOfWeek;
                                        string WeekofDay = day.ToString();

                                        if (Shift_Check_blb == true)
                                        {
                                            AutoDTable.Rows[iRow2][3] = Final_Shift.ToString();
                                            AutoDTable.Rows[iRow2][16] = SessionCompanyName.ToString();
                                            AutoDTable.Rows[iRow2][17] = SessionLocationName.ToString();
                                            AutoDTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");
                                            AutoDTable.Rows[iRow2][7] =  Final_InTime;
                                            if (Final_Shift == "SHIFT1")
                                            {
                                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                                                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                                SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                                            }
                                           
                                            else
                                            {
                                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                                                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                                SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "'";
                                                mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);
                                            }

                                            if (Final_Shift == "SHIFT1")
                                            {
                                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                                SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                                            }
                                            else
                                            {
                                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                                SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

                                            }
                                            DS_Time = objdata.ReturnMultipleValue(SSQL);
                                            if (DS_Time.Rows.Count != 0)
                                            {
                                                AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);


                                            }
                                        }

                                        else
                                        {
                                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' Order by TimeOUT Asc";
                                            DS_Time = objdata.ReturnMultipleValue(SSQL);

                                            if (DS_Time.Rows.Count != 0)
                                            {
                                                AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);


                                            }

                                        }
                                    }
                                    else
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00' Order by TimeOUT Asc";
                                        DS_Time = objdata.ReturnMultipleValue(SSQL);

                                        if (DS_Time.Rows.Count != 0)
                                        {
                                            AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);

                                        }
                                    }
                                }
                                else
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT ASC";
                                }
                            }


                            else
                            {
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT ASC";
                            }



                            mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);
                            String Emp_Total_Work_Time_1 = "00:00";
                            if (mLocalDS_INTAB.Rows.Count > 1)
                            {
                                for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                                {
                                    Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

                                    if (mLocalDS_OUTTAB.Rows.Count > tin)
                                    {
                                        Time_OUT_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                                    }
                                    else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                                    {
                                        Time_OUT_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                                    }
                                    else
                                    {
                                        Time_OUT_Str = "";
                                    }

                                    TimeSpan ts4;
                                    ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;
                                    if (LocalDT.Rows.Count <= 0)
                                    {
                                        Time_IN_Str = "";
                                    }
                                    else
                                    {
                                        Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                                    }

                                    if (Time_IN_Str == "" || Time_OUT_Str == "")
                                    {
                                        time_Check_dbl = time_Check_dbl;
                                    }
                                    else
                                    {
                                        DateTime date3 = Convert.ToDateTime(Time_IN_Str);
                                        DateTime date4 = Convert.ToDateTime(Time_OUT_Str);
                                        TimeSpan ts1;
                                        ts1 = date4.Subtract(date3);
                                        Total_Time_get = ts1.Hours.ToString();
                                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

                                        string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Total_Time_get, 1) == "-")
                                        {
                                            date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                            ts1 = date4.Subtract(date3);
                                            ts1 = date4.Subtract(date3);
                                            ts4 = ts4.Add(ts1);
                                            Total_Time_get = ts1.Hours.ToString();
                                            time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                            {

                                                Emp_Total_Work_Time_1 = "00:00";
                                            }
                                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                            {

                                                Emp_Total_Work_Time_1 = "00:00";
                                            }

                                        }
                                        else
                                        {
                                            ts4 = ts4.Add(ts1);
                                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                            {
                                                Emp_Total_Work_Time_1 = "00:00";
                                            }
                                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                            {
                                                Emp_Total_Work_Time_1 = "00:00";
                                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                            }
                                        }
                                    }

                                    AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;
                                    AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                                    OThour_Str = Emp_Total_Work_Time_1.Split(':');
                                    OT_Hour = Convert.ToInt32(OThour_Str[0]);
                                    OT_Min = Convert.ToInt32(OThour_Str[1]);
                                    if (OT_Hour >= 9)
                                    {
                                        OT_Time = OT_Hour - 8;
                                        OT_Hour1 = Convert.ToString(OT_Time) + " : " + Convert.ToString(OT_Min);
                                        AutoDTable.Rows[iRow2][15] = OT_Hour1;
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[iRow2][15] = "00:00";
                                    }

                                }
                            }
                            else
                            {
                                TimeSpan ts4;
                                ts4 = Convert.ToDateTime(String.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                                if (mLocalDS_INTAB.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "";
                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                                }
                                for (int tout = 0; tout <= mLocalDS_OUTTAB.Rows.Count; tout++)
                                {
                                    if (mLocalDS_OUTTAB.Rows.Count <= 0)
                                    {
                                        Time_OUT_Str = "";
                                    }
                                    else
                                    {
                                        Time_OUT_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                                    }

                                }
                                if (Time_IN_Str == "" || Time_OUT_Str == "")
                                {
                                    time_Check_dbl = 0;
                                }
                                else
                                {
                                    DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                                    DateTime date4 = System.Convert.ToDateTime(Time_OUT_Str);
                                    TimeSpan ts1;
                                    ts1 = date4.Subtract(date3);
                                    Total_Time_get = ts1.Hours.ToString();

                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {
                                        date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                        ts1 = date4.Subtract(date3);
                                        ts1 = date4.Subtract(date3);
                                        ts4 = ts4.Add(ts1);
                                        Total_Time_get = ts1.Hours.ToString();
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                    }
                                    else
                                    {
                                        ts4 = ts4.Add(ts1);
                                        string s1 = ts4.Minutes.ToString();
                                        if (s1.Length == 1)
                                        {
                                            string ts_str = "0" + ts4.Minutes;
                                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts_str;
                                        }
                                        else
                                        {
                                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                        }
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }

                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                    AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;

                                    AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                                    OThour_Str = Emp_Total_Work_Time_1.Split(':');
                                    OT_Hour = Convert.ToInt32(OThour_Str[0]);
                                    OT_Min = Convert.ToInt32(OThour_Str[1]);
                                    if (OT_Hour >= 9)
                                    {
                                        OT_Time = OT_Hour - 8;
                                        OT_Hour1 = Convert.ToString(OT_Time) + " : " + Convert.ToString(OT_Min);
                                        AutoDTable.Rows[iRow2][15] = OT_Hour1;
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[iRow2][15] = "00:00";
                                    }


                                }
                               
                            }
                            string OD = "";
                            DataTable da_OD = new DataTable(); 
                            SSQL = "select AttnStatus from ManAttn_Details MA inner join Employee_Mst EM on " +
                                   " Em.MachineID=MA.Machine_No where  AttnStatus='OD' and  AttnDate='" + Date_Value_Str + "' and EM.MachineID_Encrypt='" + InMachine_IP + "' ";
                            da_OD = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (da_OD.Rows.Count != 0)
                            {
                           

                                AutoDTable.Rows[iRow2][20] = "OD";
                            }
                           
                            // Bellow codings are correct:

                        }
                        for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
                        {
                            string MID = AutoDTable.Rows[iRow2][19].ToString();

                            SSQL = "";
                            SSQL = "select Distinct isnull(MachineID_Encrypt,'') as [MachineID],isnull(DeptName,'') as [DeptName]";
                            SSQL = SSQL + ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
                            SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
                            SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName],isnull(Designation,'') as Desgination";
                            SSQL = SSQL + " from Employee_Mst  Where MachineID_Encrypt='" + MID + "' And Compcode='" + SessionCcode + "'";
                            SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'" + " order by ExistingCode Asc";

                            mEmployeeDS = objdata.ReturnMultipleValue(SSQL);

                            if (mEmployeeDS.Rows.Count > 0)
                            {
                                for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                                {

                                    ss = UTF8Decryption(MID.ToString());
                                    AutoDTable.Rows[iRow2][19] = ss.ToString();
                                    string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();

                                    
                                    AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                                    AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
                                    AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                                    AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                                    AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                                    AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                                    AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                                    AutoDTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");
                                    AutoDTable.Rows[iRow2][16] = SessionCompanyName.ToString();
                                    AutoDTable.Rows[iRow2][17] = SessionLocationName.ToString();
                                    AutoDTable.Rows[iRow2][18] = mEmployeeDS.Rows[iRow1]["Desgination"];

                                }
                            }
                        }

                    }
                }
             


                ds.Tables.Add(AutoDTable);
                //ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/DayWise1.rpt"));
                report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                CrystalReportViewer1.ReportSource = report;

                 
            //SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            //dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            //if (dtdCompanyAddress.Rows.Count != 0)
            //{
            //    report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
            //    report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
            //    report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            //}
            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

            //Sent Mail Code Start
            //MailMessage mail = new MailMessage();
            //mail.To.Add("cpsubbu1@gmail.com");
            //mail.To.Add("krsenthilatm@gmail.com");


            ////mail.To.Add("yarntms@gmail.com");
            ////mail.CC.Add("kmoorthi77@gmail.com");
            //mail.Bcc.Add("padmanabhanscoto@gmail.com");

            //mail.From = new MailAddress("aatm2005@gmail.com");
            //mail.Subject = "Daily Report";
            //mail.Body = "Find the attachment for Daily Report";
            //mail.IsBodyHtml = true;

            //mail.Attachments.Add(new Attachment(ms, "DayWise1.rpt", "crystal/DayWise1.rpt"));
            mail.Attachments.Add(new Attachment(report.ExportToStream(ExportFormatType.PortableDocFormat), "Day-Wise-Report.pdf"));
            //SmtpClient smtp = new SmtpClient();
            //smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
            //smtp.Credentials = new System.Net.NetworkCredential("aatm2005@gmail.com", "Altius2005");  //Or your Smtp Email ID and Password
            //smtp.EnableSsl = true;
            //smtp.Send(mail);
        
    }
    public void Impropare_Punch()
    {
        
        
        DataSet ds = new DataSet();
        DataTable AutoDTable = new DataTable();
        DataTable DataCells = new DataTable();
        DataTable dtIPaddress = new DataTable();



        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Type");
        AutoDTable.Columns.Add("Shift");
        AutoDTable.Columns.Add("EmpCode");
        AutoDTable.Columns.Add("ExCode");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("TimeIN");
        AutoDTable.Columns.Add("TimeOUT");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("Category");
        AutoDTable.Columns.Add("SubCategory");
        AutoDTable.Columns.Add("TotalMIN");
        AutoDTable.Columns.Add("GrandTOT");
        AutoDTable.Columns.Add("ShiftDate");
        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
        AutoDTable.Columns.Add("Desgination");




        DataCells.Columns.Add("SNo");
        DataCells.Columns.Add("Dept");
        DataCells.Columns.Add("Type");
        DataCells.Columns.Add("Shift");
        DataCells.Columns.Add("EmpCode");
        DataCells.Columns.Add("ExCode");
        DataCells.Columns.Add("Name");
        DataCells.Columns.Add("TimeIN");
        DataCells.Columns.Add("TimeOUT");
        DataCells.Columns.Add("MachineID");
        DataCells.Columns.Add("Category");
        DataCells.Columns.Add("SubCategory");
        DataCells.Columns.Add("TotalMIN");
        DataCells.Columns.Add("GrandTOT");
        DataCells.Columns.Add("ShiftDate");
        DataCells.Columns.Add("CompanyName");
        DataCells.Columns.Add("LocationName");
        DataCells.Columns.Add("Desgination");



        string ng = string.Format(Date, "MM-dd-yyyy");
        Datestr = Convert.ToDateTime(fromdate).AddDays(0).ToShortDateString();
        Datestr1 = Convert.ToDateTime(fromdate).AddDays(1).ToShortDateString();
        DateTime date1 = Convert.ToDateTime(Datestr);
        DateTime date2 = date1.AddDays(1);
    



        dtIPaddress = objdata.BelowFourHours(SessionCcode.ToString(), SessionLcode.ToString());

        if (dtIPaddress.Rows.Count > 0)
        {
            for (int i = 0; i < dtIPaddress.Rows.Count; i++)
            {
                if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                {
                    mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
                else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                {
                    mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
            }
        }





        string S = "";


        // if (ModeType == "IN/OUT")
        //{
        S = "";
        S = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
        S = S + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
        S += " from Shift_Mst Where CompCode='" + SessionCcode.ToString() + "'";
        S += " And LocCode='" + SessionLcode.ToString() + "'";
        S += " Order By shiftDesc";

        // }
        LocalDT = objdata.ReturnMultipleValue(S);
        string ShiftType;

        if (LocalDT.Rows.Count < 0)
        {
            return;

        }

        string Start_IN = "";
        string End_In;

        string End_In_Days = "";
        string Start_In_Days = "";



        int mStartINRow = 0;
        int mStartOUTRow = 0;

        for (int iTabRow = 0; iTabRow < LocalDT.Rows.Count; iTabRow++)
        {
            if (AutoDTable.Rows.Count <= 1)
            {
                mStartOUTRow = 0;
            }
            else
            {
                mStartOUTRow = AutoDTable.Rows.Count - 1;
            }

            if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "GENERAL")
            {
                ShiftType = "GENERAL";
            }
            else
            {
                ShiftType = "SHIFT";
            }


            string sIndays_str = LocalDT.Rows[iTabRow]["StartIN_Days"].ToString();
            double sINdays = Convert.ToDouble(sIndays_str);
            string eIndays_str = LocalDT.Rows[iTabRow]["EndIN_Days"].ToString();
            double eINdays = Convert.ToDouble(eIndays_str);
            string ss = "";

            if (ShiftType == "GENERAL")
            {

                ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                ss = ss + " Where LT.Compcode='" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                ss = ss + "And LT.TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                ss = ss + "And LT.TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                ss = ss + "AND EM.ShiftType='" + ShiftType + "' And EM.Compcode='" + SessionCcode.ToString() + "'";

                if (SessionUserType == "2")
                {
                    ss = ss + " And EM.IsNonAdmin='1'";
                }

                ss = ss + "And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsActive='yes' Group By LT.MachineID Order By Min(LT.TimeIN)";


            }


            else if (ShiftType == "SHIFT")
            {
                ss = "select MachineID,Min(TimeIN) as [TimeIN] from LogTime_IN";
                ss = ss + " Where Compcode= '" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                ss = ss + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                ss = ss + " And TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["StartIN"].ToString() + "' ";
                ss = ss + " And TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["EndIN"].ToString() + "'";
                ss = ss + " Group By MachineID Order By Min(TimeIN)";


            }
            else
            {
                ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                ss = ss + " And EM.IsActive='yes' And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["StartIN"].ToString() + "' ";
                ss = ss + " And LT.TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["EndIN"].ToString() + "' ";
                ss = ss + " AND EM.ShiftType='" + ShiftType + "' ";

                if (SessionUserType == "2")
                {
                    ss = ss + " And EM.IsNonAdmin='1'";
                }

                ss = ss + " Group By LT.MachineID Order By Min(LT.TimeIN)";
            }


            mdatatable = objdata.ReturnMultipleValue(ss);

            if (mdatatable.Rows.Count > 0)
            {
                string MachineID;

                for (int iRow = 0; iRow < mdatatable.Rows.Count; iRow++)
                {
                    Boolean chkduplicate = false;
                    chkduplicate = false;

                    for (int ia = 0; ia < AutoDTable.Rows.Count; ia++)
                    {
                        string id = mdatatable.Rows[iRow]["MachineID"].ToString();

                        if (id == AutoDTable.Rows[ia][9].ToString())
                        {
                            chkduplicate = true;
                        }
                    }
                    if (chkduplicate == false)
                    {
                        AutoDTable.NewRow();
                        AutoDTable.Rows.Add();


                        // AutoDTable.Rows.Add();

                        // AutoDTable.Rows[mStartINRow][0] = iRow + 1;
                        AutoDTable.Rows[mStartINRow][3] = LocalDT.Rows[iTabRow]["ShiftDesc"].ToString();
                        if (ShiftType == "SHIFT")
                        {
                            string str = mdatatable.Rows[iRow][1].ToString();
                            // string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1])
                            AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mdatatable.Rows[iRow]["TimeIN"]);
                        }
                        else
                        {
                            AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mdatatable.Rows[iRow]["TimeIN"]);
                        }


                        MachineID = mdatatable.Rows[iRow]["MachineID"].ToString();
                        AutoDTable.Rows[mStartINRow][9] = MachineID.ToString();
                        mStartINRow += 1;
                    }
                }
            }

            DataTable nnDatatable = new DataTable();

            string SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT  Where Compcode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " and LocCode = '" + SessionLcode.ToString() + "'";
            if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "SHIFT1")
            {
                SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
            }
            else
            {
                SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
            }
            SSQL = SSQL + " Group By MachineID";
            SSQL = SSQL + " Order By Max(TimeOUT)";

            mdatatable = objdata.ReturnMultipleValue(SSQL);


            string InMachine_IP;
            DataTable mLocalDS_out = new DataTable();

            for (int iRow2 = mStartOUTRow; iRow2 < AutoDTable.Rows.Count; iRow2++)
            {
                string mach = AutoDTable.Rows[iRow2][9].ToString();

                InMachine_IP = mach.ToString();

                SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "SHIFT1")
                {
                    SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                    SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Asc";
                }
                else
                {
                    SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                    SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Asc";
                }
                mLocalDS_out = objdata.ReturnMultipleValue(SSQL);

                if (mLocalDS_out.Rows.Count <= 0)
                {
                    AutoDTable.Rows[iRow2][8] = "";
                }
                else
                {
                    if (AutoDTable.Rows[iRow2][9].ToString() == mLocalDS_out.Rows[0][0].ToString())
                    {
                        AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1]);

                    }

                }
                // Above coddings are correct:

                if (InMachine_IP == "ODU0OQ==")
                {
                    InMachine_IP = "ODU0OQ==";
                }
                Time_IN_Str = "";
                Time_OUT_Str = "";
                Date_Value_Str = string.Format(Date, "yyyy/MM/dd");

                if (SessionLcode == "UNIT I" && LocalDT.Rows[iTabRow]["ShiftDesc"].ToString() == "SHIFT10")
                {
                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(-1).ToString("yyyy/MM/dd") + " " + "23:00' And TimeIN <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:59' Order by TimeIN ASC";
                }
                else
                {
                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

                }

                mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

                if (AutoDTable.Rows[iRow2][3].ToString() == "SHIFT11" || AutoDTable.Rows[iRow2][3].ToString() == "SHIFT12")
                {
                    InTime_Check = Convert.ToDateTime(AutoDTable.Rows[iRow2][7].ToString());
                    InToTime_Check = InTime_Check.AddHours(2);

                    string s1 = InTime_Check.ToString("HH:mm:ss");
                    string s2 = InToTime_Check.ToString("HH:mm:ss");

                    string InTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InTime_Check);
                    string InToTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InToTime_Check);
                    InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
                    From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                    InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
                    To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;


                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + From_Time_Str + "'";
                    SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";

                    DS_Time = objdata.ReturnMultipleValue(SSQL);

                    if (DS_Time.Rows.Count != 0)
                    {
                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' ";
                        SSQL = SSQL + " And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                        DS_InTime = objdata.ReturnMultipleValue(SSQL);

                        if (DS_InTime.Rows.Count != 0)
                        {
                            Final_InTime = DS_InTime.Rows[0][0].ToString();
                            SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And ShiftDesc like '%SHIFT%'";
                            Shift_DS = objdata.ReturnMultipleValue(SSQL);
                            Shift_Check_blb = false;

                            for (int k = 0; k < Shift_DS.Rows.Count; k++)
                            {
                                string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                int b = Convert.ToInt16(a.ToString());
                                Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                int b1 = Convert.ToInt16(a1.ToString());
                                Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());

                                ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                                EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());
                                if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                {
                                    Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                                    Shift_Check_blb = true;
                                }
                            }

                            if (Shift_Check_blb == true)
                            {
                                AutoDTable.Rows[iRow2][3] = Final_Shift.ToString();
                                //AutoDTable.Rows[iRow2][7] = string.Format("{0:hh:mm tt}", Final_InTime);

                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "'";
                                mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

                                if (Final_Shift == "SHIFT2")
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
                                }
                                else
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "17:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                                }
                                DS_Time = objdata.ReturnMultipleValue(SSQL);
                                if (DS_Time.Rows.Count != 0)
                                {

                                    AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);

                                }
                            }

                            else
                            {
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00' Order by TimeOUT Asc";
                                DS_Time = objdata.ReturnMultipleValue(SSQL);

                                if (DS_Time.Rows.Count != 0)
                                {

                                    AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);

                                }

                            }
                        }
                        else
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' Order by TimeOUT Asc";
                            DS_Time = objdata.ReturnMultipleValue(SSQL);

                            if (DS_Time.Rows.Count != 0)
                            {

                                AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);
                                // string.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);

                            }
                        }
                    }
                    else
                    {
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                    }
                }


                else
                {
                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                }



                mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);
                String Emp_Total_Work_Time_1 = "00:00";
                if (mLocalDS_INTAB.Rows.Count >= 1)
                {
                    for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                    {
                        Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

                        if (mLocalDS_OUTTAB.Rows.Count > tin)
                        {
                            Time_OUT_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                        }
                        else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                        {
                            Time_OUT_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                        }
                        else
                        {
                            Time_OUT_Str = "";
                        }

                        TimeSpan ts4;
                        ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (LocalDT.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                        }

                        if (Time_IN_Str == "" || Time_OUT_Str == "")
                        {
                            time_Check_dbl = time_Check_dbl;
                        }
                        else
                        {
                            DateTime date3 = Convert.ToDateTime(Time_IN_Str);
                            DateTime date4 = Convert.ToDateTime(Time_OUT_Str);
                            TimeSpan ts1;
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();
                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

                            string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {

                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {

                                    Emp_Total_Work_Time_1 = "00:00";
                                }

                            }
                            else
                            {
                                ts4 = ts4.Add(ts1);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                }
                            }
                        }

                        AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;

                    }
                }
                else
                {
                    TimeSpan ts4;
                    ts4 = Convert.ToDateTime(String.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                    if (mLocalDS_INTAB.Rows.Count <= 0)
                    {
                        Time_IN_Str = "";
                    }
                    else
                    {
                        Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                    }
                    for (int tout = 0; tout <= mLocalDS_OUTTAB.Rows.Count; tout++)
                    {
                        if (mLocalDS_OUTTAB.Rows.Count <= 0)
                        {
                            Time_OUT_Str = "";
                        }
                        else
                        {
                            Time_OUT_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                        }

                    }
                    if (Time_IN_Str == "" || Time_OUT_Str == "")
                    {
                        time_Check_dbl = 0;
                    }
                    else
                    {
                        DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                        DateTime date4 = System.Convert.ToDateTime(Time_OUT_Str);
                        TimeSpan ts1;
                        ts1 = date4.Subtract(date3);
                        Total_Time_get = ts1.Hours.ToString();

                        if (Left_Val(Total_Time_get, 1) == "-")
                        {
                            date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                            ts1 = date4.Subtract(date3);
                            ts1 = date4.Subtract(date3);
                            ts4 = ts4.Add(ts1);
                            Total_Time_get = ts1.Hours.ToString();
                            time_Check_dbl = Convert.ToInt16(Total_Time_get);
                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                        }
                        else
                        {
                            ts4 = ts4.Add(ts1);
                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }

                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            time_Check_dbl = Convert.ToInt16(Total_Time_get);
                        }
                        AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;

                        AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;

                    }

                }
                // Bellow codings are correct:

            }
        }

        // Out_Punch_GetAttdDayWise();


        for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
        {
            string MID = AutoDTable.Rows[iRow2][9].ToString();
            string SSQL = "";
            SSQL = "";
            SSQL = "select Distinct isnull(EM.MachineID_Encrypt,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
            SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
            SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + ",isnull(EM.CatName,'') as [CatName], isnull(EM.SubCatName,'') as [SubCatName],isnull(EM.Designation,'') as Desgination";
            SSQL = SSQL + ",DM.DeptCode from Employee_Mst EM inner Join Department_Mst DM on DM.DeptName=EM.DeptName Where EM.MachineID_Encrypt='" + MID + "' And EM.Compcode='" + SessionCcode.ToString() + "'";
            if (SessionUserType == "2")
            {
                SSQL = SSQL + " And EM.IsNonAdmin='1'";
            }

            SSQL = SSQL + " And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsActive='Yes'" + " order by DM.DeptCode Asc";


            mEmployeeDS = objdata.ReturnMultipleValue(SSQL);

            if (mEmployeeDS.Rows.Count > 0)
            {
                for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                {


                    string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();
                    if (MID1 == "ODAyOA==")
                    {
                        MID1 = "ODAyOA==";
                    }

                    //if (AutoDTable.Rows[iRow2][9] == mEmployeeDS.Rows[iRow1]["MachineID"])
                    //{
                    AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                    AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
                    AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                    AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                    AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                    AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                    AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                    AutoDTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");
                    AutoDTable.Rows[iRow2][15] = SessionCompanyName.ToString();
                    AutoDTable.Rows[iRow2][16] = SessionLcode.ToString();
                    AutoDTable.Rows[iRow2][17] = mEmployeeDS.Rows[iRow1]["Desgination"];


                    //}
                }
            }
        }





        ds.Tables.Add(AutoDTable);
        ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("crystal/PresentAbstract.rpt"));
        report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
        report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        CrystalReportViewer1.ReportSource = report;

        

        //mail.To.Add("cpsubbu1@gmail.com,krsenthilatm@gmail.com");
        //mail.To.Add("aatm2005@gmail.com");

        mail.To.Add("kailaispinners@gmail.com");
        //mail.CC.Add("kmoorthi77@gmail.com");
        mail.Bcc.Add("padmanabhanscoto@gmail.com");

        mail.From = new MailAddress("aatm2005@gmail.com");
        mail.Subject = "SRI KAILAI SPINNERS - UNIT I";
        mail.Body = "Find the attachment for Daily Report";
        mail.IsBodyHtml = true;
              
        //mail.Attachments.Add(new Attachment(ms, "DayWise1.rpt", "crystal/DayWise1.rpt"));
        mail.Attachments.Add(new Attachment(report.ExportToStream(ExportFormatType.PortableDocFormat), "Impropare Punch Report.pdf"));

        Daywise_Mail();

        SmtpClient smtp = new SmtpClient();
        smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
        smtp.Credentials = new System.Net.NetworkCredential("aatm2005@gmail.com", "Altius2005");  //Or your Smtp Email ID and Password
        smtp.EnableSsl = true;
        smtp.Send(mail);

       

    }
    public void Mismatch()
    {
        DataSet ds = new DataSet();
        DataTable mLocalDS = new DataTable();
        DataTable mLocalDT = new DataTable();
        DataTable dtIPaddress = new DataTable();
       
        DataColumn auto = new DataColumn();
        auto.AutoIncrement = true;
        auto.AutoIncrementSeed = 1;

        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Type");
        AutoDTable.Columns.Add("Shift");
        AutoDTable.Columns.Add("EmpCode");
        AutoDTable.Columns.Add("ExCode");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("TimeIN");
        AutoDTable.Columns.Add("TimeOUT");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("Category");
        AutoDTable.Columns.Add("SubCategory");
        AutoDTable.Columns.Add("TotalMIN");
        AutoDTable.Columns.Add("GrandTOT");
        AutoDTable.Columns.Add("ShiftDate");
        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
        AutoDTable.Columns.Add("Desgination");

        string ng = string.Format(Date, "MM-dd-yyyy");
        Datestr = Convert.ToDateTime(fromdate).AddDays(0).ToShortDateString();
        Datestr1 = Convert.ToDateTime(fromdate).AddDays(1).ToShortDateString();
        DateTime date1 = Convert.ToDateTime(Datestr);
        DateTime date2 = date1.AddDays(1);



        dtIPaddress = objdata.IPAddressForAll(SessionCcode.ToString(), SessionLcode.ToString());



        if (dtIPaddress.Rows.Count > 0)
        {
            for (int i = 0; i < dtIPaddress.Rows.Count; i++)
            {
                if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                {
                    mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
                else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                {
                    mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
            }
        }

        //if (ModeType == "IN/OUT")
        //{
        SSQL = "";
        SSQL = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
        SSQL = SSQL + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
        SSQL = SSQL + " from Shift_Mst Where CompCode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
      
        //else
        //{
        //    SSQL = SSQL + " And ShiftDesc like '" + ddlShiftType + "%'";

        //}
        SSQL = SSQL + " Order By shiftDesc";
        LocalDT = objdata.ReturnMultipleValue(SSQL);
        //}
        //else
        //{
        //    return;
        //}
        string ShiftType = "";

        if (SessionLcode.ToString() == "UNIT I")
        {
            for (int i = 0; i <= LocalDT.Rows.Count - 1; i++)
            {
                if (LocalDT.Rows[i]["shiftDesc"] == "SHIFT10")
                {
                    LocalDT.Rows[i]["StartIN_Days"] = -1;
                    LocalDT.Rows[i]["EndIN_Days"] = 0;
                }
            }
        }

        if (LocalDT.Rows.Count < 0)
            return;

        ng = string.Format(fromdate, "dd-MM-yyyy");
        date1 = Convert.ToDateTime(ng);
        date2 = date1.AddDays(1);
        string Start_IN = "";
        string End_In;

        string End_In_Days = "";
        string Start_In_Days = "";


        int mStartINRow = 0;
        int mStartOUTRow = 0;

        NoShift_Add_All_Shift_Wise_GetAttdDayWise();

        Early_Out_Check();

        for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
        {
            string MID = AutoDTable.Rows[iRow2][9].ToString();

            SSQL = "";
            SSQL = "select Distinct isnull(EM.MachineID_Encrypt,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
            SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
            SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + ",isnull(EM.CatName,'') as [CatName], isnull(EM.SubCatName,'') as [SubCatName],isnull(EM.Designation,'') as Desgination";
            SSQL = SSQL + ",DM.DeptCode from Employee_Mst EM inner Join Department_Mst DM on DM.DeptName=EM.DeptName Where EM.MachineID_Encrypt='" + MID + "' And EM.Compcode='" + SessionCcode.ToString() + "'";

            if (SessionUserType == "3")
            {
                SSQL = SSQL + " And EM.IsNonAdmin='1'";
            }

            SSQL = SSQL + " And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsActive='Yes'" + " order by DM.DeptCode Asc";
            mEmployeeDS = objdata.ReturnMultipleValue(SSQL);

            if (mEmployeeDS.Rows.Count > 0)
            {
                for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                {
                    string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();

                    //if (AutoDTable.Rows[iRow2][9] == mEmployeeDS.Rows[iRow1]["MachineID"])
                    //{

                    AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                    AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
                    AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                    AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                    AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                    AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                    AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                    AutoDTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");
                    AutoDTable.Rows[iRow2][15] = SessionCompanyName.ToString();
                    AutoDTable.Rows[iRow2][16] = SessionLocationName.ToString();
                    AutoDTable.Rows[iRow2][17] = mEmployeeDS.Rows[iRow1]["Desgination"];

                    //}
                }
            }
        }



        ds.Tables.Add(AutoDTable);
        ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("crystal/Mismatch.rpt"));
        report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
        report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        CrystalReportViewer1.ReportSource = report;
        mail.Attachments.Add(new Attachment(report.ExportToStream(ExportFormatType.PortableDocFormat), "MisMatch-Report.pdf"));


    }


    public void NoShift_Add_All_Shift_Wise_GetAttdDayWise()
    {

        string To_Time_Str = "";
        DataTable mLocalDS = new DataTable();
        DataTable mLocalDT = new DataTable();
        DataTable dtIPaddress = new DataTable();
      
        int itab = 0;

      
        date1 = Convert.ToDateTime(fromdate);
        date2 = date1.AddDays(1);

        dtIPaddress = objdata.IPAddressForAll(SessionCcode.ToString(), SessionLcode.ToString());

        if (dtIPaddress.Rows.Count > 0)
        {
            for (int i = 0; i < dtIPaddress.Rows.Count; i++)
            {
                if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                {
                    mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
                else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                {
                    mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
            }
        }



        string SSQL = "";
        SSQL = "Select Distinct MachineID_Encrypt as MachineID,ShiftType from Employee_Mst where Compcode='" + SessionCcode.ToString() + "'";
        SSQL = SSQL + "and Loccode='" + SessionLcode.ToString() + "' And IsActive='Yes'";

        if (SessionUserType == "3")
        {
            SSQL = SSQL + " And IsNonAdmin='1'";
        }


        mLocalDS = objdata.ReturnMultipleValue(SSQL);

        if (mLocalDS.Rows.Count < 0)
            return;




        string Time_IN_Check_Shift = "";
        string Machine_ID_Check = "";
        string Emp_ShiftType = "";
        DataTable Shift_DataSet = new DataTable();
        DataTable mLocalDS_out = new DataTable();


        for (int iTabRow = 0; iTabRow < mLocalDS.Rows.Count; iTabRow++)
        {
            Machine_ID_Check = mLocalDS.Rows[iTabRow]["MachineID"].ToString();
            if (Machine_ID_Check == "MTA0")
            {
                Machine_ID_Check = "MTA0";
            }
            Emp_ShiftType = mLocalDS.Rows[iTabRow]["ShiftType"].ToString();

            SSQL = "Select MachineID,TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
            SSQL = SSQL + " And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "' Order by TimeIN ASC";

            mLocalDT = objdata.ReturnMultipleValue(SSQL);


            if (mLocalDT.Rows.Count <= 0)
            {
                //Skip
            }
            else
            {
                Time_IN_Check_Shift = mLocalDT.Rows[0]["TimeIN"].ToString();
                //Shift Check
                //2nd Shift Check
                string Shift_Start_Time = "";
                string Shift_End_Time = "";
                string Employee_Time = "";
                DataTable Shift_Ds = new DataTable();
                bool Shift_Add_or_Check = false;

                DateTime ShiftdateStartIN;
                DateTime ShiftdateEndIN;
                DateTime EmpdateIN;

                SSQL = "Select * from Shift_Mst where Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And ShiftDesc like '%%'";
                Shift_Ds = objdata.ReturnMultipleValue(SSQL);

                if (Shift_Ds.Rows.Count != 0)
                {
                    Shift_Add_or_Check = false;
                    for (int Z = 0; Z <= Shift_Ds.Rows.Count - 1; Z++)
                    {
                        if (Emp_ShiftType == "GENERAL")
                        {
                            Shift_Start_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["StartIN"].ToString();
                            Shift_End_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["EndIN"].ToString();

                            Employee_Time = mLocalDT.Rows[0]["TimeIN"].ToString();

                            ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time);
                            ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time);
                            EmpdateIN = Convert.ToDateTime(Employee_Time);

                            if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                            {
                                Shift_Add_or_Check = true;
                                break;
                            }
                        }
                        else if (Emp_ShiftType == "GENERAL1")
                        {
                            Shift_Start_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["StartIN"].ToString();
                            Shift_End_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["EndIN"].ToString();

                            Employee_Time = mLocalDT.Rows[0]["TimeIN"].ToString();

                            ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time);
                            ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time);
                            EmpdateIN = Convert.ToDateTime(Employee_Time);

                            if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                            {
                                Shift_Add_or_Check = true;
                                break;
                            }
                        }
                        else
                        {
                            if (Shift_Ds.Rows[Z]["ShiftDesc"].ToString() == "SHIFT3")
                            {
                                Shift_Start_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["StartIN"].ToString();
                                Shift_End_Time = date1.AddDays(1).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["EndIN"].ToString();
                            }
                            else
                            {
                                if (Shift_Ds.Rows[Z]["ShiftDesc"].ToString() == "GENERAL")
                                {
                                }
                                else
                                {
                                    Shift_Start_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["StartIN"].ToString();
                                    Shift_End_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["EndIN"].ToString();
                                }
                            }
                            Employee_Time = mLocalDT.Rows[0]["TimeIN"].ToString();
                            if (Shift_Start_Time != "" & Shift_End_Time != "")
                            {
                                ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time);
                                ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time);
                                EmpdateIN = Convert.ToDateTime(Employee_Time);

                                if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                                {
                                    Shift_Add_or_Check = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (Shift_Add_or_Check == false & Shift_Ds.Rows.Count != 0)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();

                    AutoDTable.Rows[itab][3] = "SHIFT-MISMATCH";
                    DateTime TimeIN_Str = Convert.ToDateTime(Time_IN_Check_Shift);
                    AutoDTable.Rows[itab][7] = String.Format("{0:hh:mm tt}", TimeIN_Str);

                    AutoDTable.Rows[itab][9] = Machine_ID_Check;

                    //Time OUT Add
                    SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode.ToString() + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                    SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Desc";
                    mLocalDS_out = objdata.ReturnMultipleValue(SSQL);

                    if (mLocalDS_out.Rows.Count <= 0)
                    {
                        //Skip
                    }
                    else
                    {
                        string machine_outSTR = mLocalDS_out.Rows[0][0].ToString();
                        if (AutoDTable.Rows[itab][9].ToString() == machine_outSTR)
                        {
                            string aa = mLocalDS_out.Rows[0][1].ToString();

                            AutoDTable.Rows[itab][8] = String.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0]["TimeOUT"]);
                            //AutoDTable.Rows[iRow2][8] = String.Format("HH:mm:ss", DS_Time.Rows[0][0]);			
                        }
                    }


                    //Grand Total value Display
                    Time_IN_Str = "";
                    Time_Out_Str = "";

                    Date_Value_Str = String.Format("yyyy/MM/dd", date1);

                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                    mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Check + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Desc";
                    mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);


                    string Emp_Total_Work_Time_1 = "00:00";
                    if (mLocalDS_INTAB.Rows.Count > 1)
                    {
                        for (int tin = 0; tin <= mLocalDS_INTAB.Rows.Count - 1; tin++)
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                            if (mLocalDS_OUTTAB.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_Out_Str = "";
                            }
                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                            }
                            //Emp_Total_Work_Time
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);

                                TimeSpan ts1 = new TimeSpan();
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    //& ":" & Trim(ts.Minutes)
                                    time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        Emp_Total_Work_Time_1 = "00:00";
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        Emp_Total_Work_Time_1 = "00:00";

                                }
                                else
                                {
                                    ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        Emp_Total_Work_Time_1 = "00:00";
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        Emp_Total_Work_Time_1 = "00:00";
                                    time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                }
                            }
                            AutoDTable.Rows[itab][12] = Emp_Total_Work_Time_1;


                            //fg.set_TextMatrix(fg.Rows - 1, fg.Cols - 1, Emp_Total_Work_Time_1);
                        }
                    }
                    else
                    {

                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS_INTAB.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                        }

                        for (int tout = 0; tout <= mLocalDS_OUTTAB.Rows.Count - 1; tout++)
                        {
                            if (mLocalDS_OUTTAB.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";
                            }
                            else
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                            }

                        }
                        //Emp_Total_Work_Time
                        if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts1 = new TimeSpan();
                            ts1 = date4.Subtract(date3);
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                //Emp_Total_Work_Time_1 = Trim(ts1.Hours) & ":" & Trim(ts1.Minutes)
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Emp_Total_Work_Time_1 = "00:00";
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    Emp_Total_Work_Time_1 = "00:00";
                            }
                            else
                            {
                                ts4 = ts4.Add(ts1);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Emp_Total_Work_Time_1 = "00:00";
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    Emp_Total_Work_Time_1 = "00:00";
                                time_Check_dbl = Convert.ToInt32(Total_Time_get);
                            }
                            AutoDTable.Rows[itab][12] = Emp_Total_Work_Time_1;
                            //fg.set_TextMatrix(fg.Rows - 1, fg.Cols - 1, Emp_Total_Work_Time_1);
                        }
                    }
                    itab = itab + 1;
                }
                else
                {
                    //Skip
                }
            }
        }
     }


    public void Early_Out_Check()
    {

        string To_Time_Str = "";
        DataTable mLocalDS = new DataTable();
        DataTable mLocalDT = new DataTable();
        DataTable dtIPaddress = new DataTable();

        int itab = 0;

        dtIPaddress = objdata.IPAddressForAll(SessionCcode.ToString(), SessionLcode.ToString());

        if (dtIPaddress.Rows.Count > 0)
        {
            for (int i = 0; i < dtIPaddress.Rows.Count; i++)
            {
                if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                {
                    mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
                else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                {
                    mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
            }
        }



        string SSQL = "";
        SSQL = "Select Distinct MachineID_Encrypt as MachineID,ShiftType from Employee_Mst where Compcode='" + SessionCcode.ToString() + "'";
        SSQL = SSQL + "and Loccode='" + SessionLcode.ToString() + "' And IsActive='Yes'";

        if (SessionUserType == "3")
        {
            SSQL = SSQL + " And IsNonAdmin='1'";
        }


        mLocalDS = objdata.ReturnMultipleValue(SSQL);

        if (mLocalDS.Rows.Count < 0)
            return;




        string Time_IN_Check_Shift = "";
        string Machine_ID_Check = "";
        string Emp_ShiftType = "";
        DataTable Shift_DataSet = new DataTable();
        DataTable mLocalDS_out = new DataTable();

        date1 = Convert.ToDateTime(fromdate);
        date2 = date1.AddDays(1);

        for (int iTabRow = 0; iTabRow < mLocalDS.Rows.Count; iTabRow++)
        {
            Machine_ID_Check = mLocalDS.Rows[iTabRow]["MachineID"].ToString();
            if (Machine_ID_Check == "MTA0")
            {
                Machine_ID_Check = "MTA0";
            }
            Emp_ShiftType = mLocalDS.Rows[iTabRow]["ShiftType"].ToString();

            SSQL = "Select MachineID,TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
            SSQL = SSQL + " And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "' Order by TimeIN ASC";

            mLocalDT = objdata.ReturnMultipleValue(SSQL);


            if (mLocalDT.Rows.Count <= 0)
            {
                //Skip
            }
            else
            {
                Time_IN_Check_Shift = mLocalDT.Rows[0]["TimeIN"].ToString();
                //Shift Check
                //2nd Shift Check
                string Shift_Start_Time = "";
                string Shift_End_Time = "";
                string Employee_Time = "";
                DataTable Shift_Ds = new DataTable();
                bool Shift_Add_or_Check = false;
                bool Shift_Out_Check = false;
                string Shift_Val = "";

                DateTime ShiftdateStartIN;
                DateTime ShiftdateEndIN;
                DateTime EmpdateIN;

                SSQL = "Select * from Shift_Mst where Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And ShiftDesc like '%%'";
                Shift_Ds = objdata.ReturnMultipleValue(SSQL);

                if (Shift_Ds.Rows.Count != 0)
                {
                    Shift_Add_or_Check = false;
                    for (int Z = 0; Z <= Shift_Ds.Rows.Count - 1; Z++)
                    {
                        if (Emp_ShiftType == "GENERAL")
                        {


                            Shift_Start_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["StartIN"].ToString();
                            Shift_End_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["EndIN"].ToString();

                            Employee_Time = mLocalDT.Rows[0]["TimeIN"].ToString();

                            ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time);
                            ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time);
                            EmpdateIN = Convert.ToDateTime(Employee_Time);

                            if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                            {
                                Shift_Val = Shift_Ds.Rows[Z]["ShiftDesc"].ToString();
                                Shift_Add_or_Check = true;
                                break;
                            }
                        }
                        else if (Emp_ShiftType == "GENERAL1")
                        {

                            Shift_Start_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["StartIN"].ToString();
                            Shift_End_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["EndIN"].ToString();

                            Employee_Time = mLocalDT.Rows[0]["TimeIN"].ToString();

                            ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time);
                            ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time);
                            EmpdateIN = Convert.ToDateTime(Employee_Time);

                            if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                            {
                                Shift_Val = Shift_Ds.Rows[Z]["ShiftDesc"].ToString();
                                Shift_Add_or_Check = true;
                                break;
                            }
                        }
                        else
                        {
                            if (Shift_Ds.Rows[Z]["ShiftDesc"].ToString() == "SHIFT3")
                            {
                                Shift_Start_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["StartIN"].ToString();
                                Shift_End_Time = date1.AddDays(1).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["EndIN"].ToString();
                            }
                            else
                            {
                                if (Shift_Ds.Rows[Z]["ShiftDesc"].ToString() == "GENERAL")
                                {
                                }
                                else
                                {
                                    Shift_Start_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["StartIN"].ToString();
                                    Shift_End_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["EndIN"].ToString();
                                }
                            }
                            Employee_Time = mLocalDT.Rows[0]["TimeIN"].ToString();
                            if (Shift_Start_Time != "" & Shift_End_Time != "")
                            {
                                ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time);
                                ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time);
                                EmpdateIN = Convert.ToDateTime(Employee_Time);

                                if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                                {
                                    Shift_Val = Shift_Ds.Rows[Z]["ShiftDesc"].ToString();
                                    Shift_Add_or_Check = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (Shift_Add_or_Check == true)
                {
                    Shift_Out_Check = false;

                    //Time OUT Add
                    SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode.ToString() + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                    SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Desc";
                    mLocalDS_out = objdata.ReturnMultipleValue(SSQL);

                    if (mLocalDS_out.Rows.Count <= 0)
                    {
                        //Skip
                    }
                    else
                    {
                        string machine_outSTR = mLocalDS_out.Rows[0][0].ToString();

                        string aa = mLocalDS_out.Rows[0][1].ToString();
                        Employee_Time = mLocalDS_out.Rows[0]["TimeOUT"].ToString();

                        SSQL = "Select * from Shift_Mst where Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And ShiftDesc='" + Shift_Val + "'";
                        Shift_Ds = objdata.ReturnMultipleValue(SSQL);

                        Shift_Start_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[0]["StartOUT"].ToString();
                        Shift_End_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[0]["EndOUT"].ToString();

                        ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time);
                        ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time);
                        EmpdateIN = Convert.ToDateTime(Employee_Time);

                        if (EmpdateIN < ShiftdateStartIN)
                        {
                            Shift_Val = Shift_Ds.Rows[0]["ShiftDesc"].ToString();
                            Shift_Out_Check = true;

                        }

                    }

                    if (Shift_Out_Check == true)
                    {

                        AutoDTable.NewRow();
                        AutoDTable.Rows.Add();

                        AutoDTable.Rows[itab][3] = "EARLY-OUT";
                        DateTime TimeIN_Str = Convert.ToDateTime(Time_IN_Check_Shift);
                        AutoDTable.Rows[itab][7] = String.Format("{0:hh:mm tt}", TimeIN_Str);

                        AutoDTable.Rows[itab][9] = Machine_ID_Check;
                        AutoDTable.Rows[itab][8] = String.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0]["TimeOUT"]);

                        //Grand Total value Display
                        Time_IN_Str = "";
                        Time_Out_Str = "";

                        Date_Value_Str = String.Format("yyyy/MM/dd", date1);

                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                        mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Check + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Desc";
                        mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);


                        string Emp_Total_Work_Time_1 = "00:00";
                        if (mLocalDS_INTAB.Rows.Count > 1)
                        {
                            for (int tin = 0; tin <= mLocalDS_INTAB.Rows.Count - 1; tin++)
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                                if (mLocalDS_OUTTAB.Rows.Count > tin)
                                {
                                    Time_Out_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                                }
                                else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                                {
                                    Time_Out_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                                }
                                else
                                {
                                    Time_Out_Str = "";
                                }
                                TimeSpan ts4 = new TimeSpan();
                                ts4 = Convert.ToDateTime(string.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                                if (mLocalDS.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "";
                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                                }
                                //Emp_Total_Work_Time
                                if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                                {
                                    time_Check_dbl = time_Check_dbl;
                                }
                                else
                                {
                                    DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                                    DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);

                                    TimeSpan ts1 = new TimeSpan();
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    Total_Time_get = ts1.Hours.ToString();
                                    //& ":" & Trim(ts.Minutes)
                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {
                                        date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                        ts1 = date4.Subtract(date3);
                                        ts1 = date4.Subtract(date3);
                                        ts4 = ts4.Add(ts1);
                                        Total_Time_get = ts1.Hours.ToString();
                                        //& ":" & Trim(ts.Minutes)
                                        time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                            Emp_Total_Work_Time_1 = "00:00";
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                            Emp_Total_Work_Time_1 = "00:00";

                                    }
                                    else
                                    {
                                        ts4 = ts4.Add(ts1);
                                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                            Emp_Total_Work_Time_1 = "00:00";
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                            Emp_Total_Work_Time_1 = "00:00";
                                        time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                    }
                                }
                                AutoDTable.Rows[itab][12] = Emp_Total_Work_Time_1;


                                //fg.set_TextMatrix(fg.Rows - 1, fg.Cols - 1, Emp_Total_Work_Time_1);
                            }
                        }
                        else
                        {

                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS_INTAB.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                            }

                            for (int tout = 0; tout <= mLocalDS_OUTTAB.Rows.Count - 1; tout++)
                            {
                                if (mLocalDS_OUTTAB.Rows.Count <= 0)
                                {
                                    Time_Out_Str = "";
                                }
                                else
                                {
                                    Time_Out_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                                }

                            }
                            //Emp_Total_Work_Time
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = 0;
                            }
                            else
                            {
                                DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts1 = new TimeSpan();
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();

                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    //& ":" & Trim(ts.Minutes)
                                    time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                    //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                    //Emp_Total_Work_Time_1 = Trim(ts1.Hours) & ":" & Trim(ts1.Minutes)
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        Emp_Total_Work_Time_1 = "00:00";
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        Emp_Total_Work_Time_1 = "00:00";
                                }
                                else
                                {
                                    ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        Emp_Total_Work_Time_1 = "00:00";
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        Emp_Total_Work_Time_1 = "00:00";
                                    time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                }
                                AutoDTable.Rows[itab][12] = Emp_Total_Work_Time_1;
                                //fg.set_TextMatrix(fg.Rows - 1, fg.Cols - 1, Emp_Total_Work_Time_1);
                            }
                        }

                        itab = itab + 1;
                    }
                }

                else
                {
                    //Skip
                }
            }
        }
    }



   


    public string Right_Val(string Value, int Length)
    {
        // Recreate a RIGHT function for string manipulation
        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }
    public static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    public static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }
}
