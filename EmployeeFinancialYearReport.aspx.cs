﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;



public partial class EmployeeFinancialYearReport : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string Date1_str;
    string Date2_str;
    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    DataTable mEmployee = new DataTable();
    DataTable mLogTime = new DataTable();

    DataTable DataDT = new DataTable();
    DataTable mLocalDT = new DataTable();
    DataSet ds = new DataSet();
    int k1 = 0;
    string DeptName;
    string FinancialYear;
    System.Web.UI.WebControls.DataGrid grid =
                             new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Employee Financial Year Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();


            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("ExistingNo");
            AutoDTable.Columns.Add("MachineEncry");
            AutoDTable.Columns.Add("EmpName");


            DataCells.Columns.Add("EmployeeNo");
            DataCells.Columns.Add("ExistingCode");
            DataCells.Columns.Add("DeptName");
            DataCells.Columns.Add("FirstName");


            //Date1_str = Request.QueryString["FromDate"].ToString();
            //Date2_str = Request.QueryString["ToDate"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            FinancialYear = Request.QueryString["Year"].ToString();
            // DayAttendance_BWDates();
            NEW_rep();






            ds.Tables.Add(DataDT);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/YearlyCalc.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            CrystalReportViewer1.ReportSource = report;


        }
    }



    public void NEW_rep()
    {
        string SSQL;
        string MID;
        string[] year = FinancialYear.Split('-');
        DataTable mEmployeeDT = new DataTable();


        DataDT.Columns.Add("MachineID");
        DataDT.Columns.Add("Name");
        DataDT.Columns.Add("Dept");
        DataDT.Columns.Add("YEAR1");
        DataDT.Columns.Add("YEAR2");
        DataDT.Columns.Add("JAN");
        DataDT.Columns.Add("FEB");
        DataDT.Columns.Add("MARCH");
        DataDT.Columns.Add("APRIL");
        DataDT.Columns.Add("MAY");
        DataDT.Columns.Add("JUNE");
        DataDT.Columns.Add("JULY");
        DataDT.Columns.Add("AUGUST");
        DataDT.Columns.Add("SEPTEMBER");
        DataDT.Columns.Add("OCTOBER");
        DataDT.Columns.Add("NOVEMBER");
        DataDT.Columns.Add("DECEMBER");
        DataDT.Columns.Add("TotalDays");
        DataDT.Columns.Add("CompanyName");
        DataDT.Columns.Add("LocationName");

        SSQL = "";
        SSQL = "select Distinct isnull(EM.MachineID,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
        SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
        SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
        SSQL = SSQL + " from Employee_Mst EM Where EM.Compcode='" + SessionCcode.ToString() + "'";
        SSQL = SSQL + " And EM.LocCode='" + SessionLcode.ToString() + "' And EM.DeptName='" + DeptName + "'And EM.IsActive='Yes'";

        mEmployeeDT = objdata.ReturnMultipleValue(SSQL);


        int k1 = 0;

        for (int j = 0; j < mEmployeeDT.Rows.Count; j++)
        {

            for (int i = 0; i < year.Length; i++)
            {
                MID = mEmployeeDT.Rows[j][0].ToString();

                int sum = 0;
                DataDT.NewRow();
                DataDT.Rows.Add();
                int k = i + 1;

                DataDT.Rows[k1]["MachineID"] = MID;
                DataDT.Rows[k1]["Name"] = mEmployeeDT.Rows[j]["FirstName"].ToString();
                DataDT.Rows[k1]["Dept"] = mEmployeeDT.Rows[j]["DeptName"].ToString();

                DataDT.Rows[k1]["YEAR" + k] = year[i];

                SSQL = "select Count(Present) from LogTime_Days where CompCode= '" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Present='1.0'";
                SSQL = SSQL + "and MachineID='" + MID + "' and Attn_Date between '" + year[i] + "-01-01' and '" + year[i] + "-01-31'";

                mLocalDT = objdata.ReturnMultipleValue(SSQL);

                if (DataDT.Rows.Count > 0)
                {

                    DataDT.Rows[k1]["JAN"] = mLocalDT.Rows[0][0].ToString();

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["JAN"]);
                }
                else
                {
                    DataDT.Rows[k1]["JAN"] = "0";
                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["JAN"]);
                }

                SSQL = "select Count(Present) from LogTime_Days where CompCode= '" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Present='1.0'";
                SSQL = SSQL + "and MachineID='" + MID + "' and Attn_Date between '" + year[i] + "-02-01' and '" + year[i] + "-02-28'";

                mLocalDT = objdata.ReturnMultipleValue(SSQL);

                if (DataDT.Rows.Count > 0)
                {

                    DataDT.Rows[k1]["FEB"] = mLocalDT.Rows[0][0].ToString();

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["FEB"]);
                }
                else
                {
                    DataDT.Rows[k1]["FEB"] = "0";
                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["FEB"]);
                }

                SSQL = "select Count(Present) from LogTime_Days where CompCode= '" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Present='1.0'";
                SSQL = SSQL + "and MachineID='" + MID + "' and Attn_Date between '" + year[i] + "-03-01' and '" + year[i] + "-03-31'";

                mLocalDT = objdata.ReturnMultipleValue(SSQL);

                if (DataDT.Rows.Count > 0)
                {

                    DataDT.Rows[k1]["MARCH"] = mLocalDT.Rows[0][0].ToString();

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["MARCH"]);
                }
                else
                {
                    DataDT.Rows[k1]["MARCH"] = "0";

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["MARCH"]);
                }

                SSQL = "select Count(Present) from LogTime_Days where CompCode= '" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Present='1.0'";
                SSQL = SSQL + "and MachineID='" + MID + "' and Attn_Date between '" + year[i] + "-04-01' and '" + year[i] + "-04-30'";

                mLocalDT = objdata.ReturnMultipleValue(SSQL);

                if (DataDT.Rows.Count > 0)
                {
                    DataDT.Rows[k1]["APRIL"] = mLocalDT.Rows[0][0].ToString();

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["APRIL"]);
                }
                else
                {
                    DataDT.Rows[k1]["APRIL"] = "0";

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["APRIL"]);
                }

                SSQL = "select Count(Present) from LogTime_Days where CompCode= '" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Present='1.0'";
                SSQL = SSQL + "and MachineID='" + MID + "' and Attn_Date between '" + year[i] + "-05-01' and '" + year[i] + "-05-31'";

                mLocalDT = objdata.ReturnMultipleValue(SSQL);

                if (DataDT.Rows.Count > 0)
                {
                    DataDT.Rows[k1]["MAY"] = mLocalDT.Rows[0][0].ToString();

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["MAY"]);
                }
                else
                {
                    DataDT.Rows[k1]["MAY"] = "0";

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["MAY"]);
                }


                SSQL = "select Count(Present) from LogTime_Days where CompCode= '" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Present='1.0'";
                SSQL = SSQL + "and MachineID='" + MID + "' and Attn_Date between '" + year[i] + "-06-01' and '" + year[i] + "-06-30'";

                mLocalDT = objdata.ReturnMultipleValue(SSQL);

                if (DataDT.Rows.Count > 0)
                {
                    DataDT.Rows[k1]["JUNE"] = mLocalDT.Rows[0][0].ToString();

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["JUNE"]);
                }
                else
                {
                    DataDT.Rows[k1]["JUNE"] = "0";

                    sum = sum + Convert.ToInt32(DataDT.Rows[j]["JUNE"]);
                }

                SSQL = "select Count(Present) from LogTime_Days where CompCode= '" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Present='1.0'";
                SSQL = SSQL + "and MachineID='" + MID + "' and Attn_Date between '" + year[i] + "-07-01' and '" + year[i] + "-07-31'";

                mLocalDT = objdata.ReturnMultipleValue(SSQL);

                if (DataDT.Rows.Count > 0)
                {
                    DataDT.Rows[k1]["JULY"] = mLocalDT.Rows[0][0].ToString();

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["JULY"]);
                }
                else
                {
                    DataDT.Rows[k1]["JULY"] = "0";
                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["JULY"]);
                }

                SSQL = "select Count(Present) from LogTime_Days where CompCode= '" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Present='1.0'";
                SSQL = SSQL + "and MachineID='" + MID + "' and Attn_Date between '" + year[i] + "-08-01' and '" + year[i] + "-08-31'";

                mLocalDT = objdata.ReturnMultipleValue(SSQL);

                if (DataDT.Rows.Count > 0)
                {
                    DataDT.Rows[k1]["AUGUST"] = mLocalDT.Rows[0][0].ToString();

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["AUGUST"]);
                }
                else
                {
                    DataDT.Rows[k1]["AUGUST"] = "0";
                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["AUGUST"]);
                }

                SSQL = "select Count(Present) from LogTime_Days where CompCode= '" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Present='1.0'";
                SSQL = SSQL + "and MachineID='" + MID + "' and Attn_Date between '" + year[i] + "-09-01' and '" + year[i] + "-09-30'";

                mLocalDT = objdata.ReturnMultipleValue(SSQL);

                if (DataDT.Rows.Count > 0)
                {
                    DataDT.Rows[k1]["SEPTEMBER"] = mLocalDT.Rows[0][0].ToString();

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["SEPTEMBER"]);
                }
                else
                {
                    DataDT.Rows[k1]["SEPTEMBER"] = "0";

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["SEPTEMBER"]);
                }

                SSQL = "select Count(Present) from LogTime_Days where CompCode= '" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Present='1.0'";
                SSQL = SSQL + "and MachineID='" + MID + "' and Attn_Date between '" + year[i] + "-10-01' and '" + year[i] + "-10-31'";

                mLocalDT = objdata.ReturnMultipleValue(SSQL);

                if (DataDT.Rows.Count > 0)
                {
                    DataDT.Rows[k1]["OCTOBER"] = mLocalDT.Rows[0][0].ToString();

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["OCTOBER"]);
                }
                else
                {
                    DataDT.Rows[k1]["OCTOBER"] = "0";

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["OCTOBER"]);
                }

                SSQL = "select Count(Present) from LogTime_Days where CompCode= '" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Present='1.0'";
                SSQL = SSQL + "and MachineID='" + MID + "' and Attn_Date between '" + year[i] + "-11-01' and '" + year[i] + "-11-30'";

                mLocalDT = objdata.ReturnMultipleValue(SSQL);

                if (DataDT.Rows.Count > 0)
                {
                    DataDT.Rows[k1]["NOVEMBER"] = mLocalDT.Rows[0][0].ToString();

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["NOVEMBER"]);
                }
                else
                {
                    DataDT.Rows[k1]["NOVEMBER"] = "0";

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["NOVEMBER"]);
                }


                SSQL = "select Count(Present) from LogTime_Days where CompCode= '" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Present='1.0'";
                SSQL = SSQL + "and MachineID='" + MID + "' and Attn_Date between '" + year[i] + "-12-01' and '" + year[i] + "-12-31'";

                mLocalDT = objdata.ReturnMultipleValue(SSQL);

                if (DataDT.Rows.Count > 0)
                {
                    DataDT.Rows[k1]["DECEMBER"] = mLocalDT.Rows[0][0].ToString();

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["DECEMBER"]);

                }
                else
                {
                    DataDT.Rows[k1]["DECEMBER"] = "0";

                    sum = sum + Convert.ToInt32(DataDT.Rows[k1]["DECEMBER"]);

                }
                DataDT.Rows[k1]["TotalDays"] = sum;
                DataDT.Rows[k1]["CompanyName"] = SessionCompanyName;
                DataDT.Rows[k1]["LocationName"] = SessionLcode;
                k1 = k1 + 1;
            }
        }

    }

}
