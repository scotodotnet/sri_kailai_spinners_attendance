﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="User_Creation.aspx.cs" Inherits="Default2" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
             
      
    <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tableCustomer').dataTable();               
            }
        }); 
    };
    </script>
      
             
     <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableCustomer').dataTable();
        });
    </script>


                               <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate>
                                        
            <div class="page-breadcrumb">
                            
                    <ol class="breadcrumb container">
                       <h4><li class="active">User Creation</li></h4> 
                    </ol>
                </div>

            <div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
    <div class="page-inner">
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">User Creation</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
				    	<%--<div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Company<span class="mandatory">*</span></label>
						<div class="col-sm-4">
                            <asp:DropDownList ID="ddlCompany" runat="server" class="form-control">
                            </asp:DropDownList>
						</div>
						
						<label for="input-Default" class="col-sm-2 control-label">Location</label>
							<div class="col-sm-4">
                               <asp:DropDownList ID="ddlLocation" runat="server" class="form-control">
                               </asp:DropDownList>
							</div>
                   </div>--%> 
                           
				    	<div class="form-group row">
					      <label for="input-Default" class="col-sm-2 control-label">User Type</label>
							<div class="col-sm-4">
                                 <asp:DropDownList ID="ddlUserType" runat="server" class="form-control">
                                 <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                 <asp:ListItem Value="2" Text="Admin"></asp:ListItem>
                                 <asp:ListItem Value="3" Text="Non-Admin"></asp:ListItem>
                                 <asp:ListItem Value="4" Text="IF"></asp:ListItem>
                                </asp:DropDownList>
							</div>
					
					    
					      <label for="input-Default" class="col-sm-2 control-label">User Name</label>
						   <div class="col-sm-4">
                              <asp:TextBox ID="txtUserName" class="form-control" runat="server" required></asp:TextBox>
                          </div>
                       </div>
                       
                        <div class="form-group row">
							<label for="input-Default" class="col-sm-2 control-label">Password</label>
							<div class="col-sm-4">
                                 <asp:TextBox ID="txtPassword" class="form-control" runat="server" TextMode="Password" required></asp:TextBox>
							</div>
						
				          <%--<div class="col-sm-1"></div>  --%>
			              <label for="input-Default" class="col-sm-2 control-label">Confirm Password</label>
							<div class="col-sm-4">
                                 <asp:TextBox ID="txtConformPwd" class="form-control" runat="server" TextMode="Password" required></asp:TextBox>
							</div>
					    
				       </div>
					    
					      
                         
                        
					<!-- Button start -->
						<div class="form-group row">
						</div>
                         <div class="txtcenter">
                               <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                    onclick="btnSave_Click" />
                               <asp:LinkButton ID="BtnClear" runat="server" class="btn btn-danger" onclick="BtnClear_Click">Clear</asp:LinkButton>   
                         </div>
                    <!-- Button End -->
					
					       <div class="form-group row">
						  </div>


                 

                        <%-- <div class="panel-body">--%>
                             <div class="table-responsive">
    
                              <asp:Repeater ID="rptrCustomer" runat="server" onitemcommand="Repeater1_ItemCommand">
            <HeaderTemplate>
                
                  <table id="tableCustomer" class="display table" style="width: 100%;">
                    <thead>
                        <tr>
                                                   <%-- <th>SNo</th>--%>
                                                    <th>Company Name</th>
                                                    <th>Location Name</th>
                                                    <th>User Name</th>  
                                                  <%--  <th>Edit</th>--%>
                                                    <th>Delete</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    
                   <%-- <td>
                        <%# DataBinder.Eval(Container.DataItem, "SNo")%>
                    </td>--%>
                    
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "CompCode")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "LocationCode")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "UserName")%>
                    </td>
                   <%-- <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("SNo") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>--%>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("CompCode") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
                        <%--</div>--%>
                       
						  
						 </div>
 
	             </form>
			
			
			
                                                        
			
			
			
			
			
			
			
			
			</div><!-- panel white end -->
		    </div>
		    <div class="col-md-2"></div>
		    
            
      
        <!-- Dashboard start -->
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- Dashboard End -->   
                        
  </div>
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
  
 </div>
                                       
                                       </ContentTemplate>
                                </asp:UpdatePanel>
  

</asp:Content>

