﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class EmployeeWiseMusterReport : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;


    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    //string cc = "EVEREADY SPINING MILL";
    string SessionCcode;
    string SessionLcode;
    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    DataTable mEmployee = new DataTable();
    DataTable mLogTime = new DataTable();
    int k1 = 0;
    string EmpCode1 = "";
    string iEmpDet = "";
    string ShiftType1 = "";
    string Date1 = "";
    string Date2 = "";
    string EmpName = "";
    string SessionCompanyName;
    string SessionLocationName;
    System.Web.UI.WebControls.DataGrid grid =
                             new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Employee Wise Muster";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();


            EmpCode1 = Request.QueryString["EmpCode"].ToString();
            EmpName = Request.QueryString["EmpName"].ToString();
            Date1 = Request.QueryString["Date1"].ToString();
            Date2 = Request.QueryString["Date2"].ToString();


            AutoDTable.Columns.Add("Date");
            AutoDTable.Columns.Add("Attendance");


            MusterReport_BWDates();


            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=EmployeeWiseMusterReportBWDates.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
            Response.Write(" &nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">EMPLOYEE WISE MUSTER REPORT BETWEEN DATES</a>");
            Response.Write(" &nbsp;&nbsp;&nbsp; ");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">From:" + Date1 + "</a>");
            Response.Write(" &nbsp;-- &nbsp;");
            Response.Write("<a style=\"font-weight:bold\">To:" + Date2 + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write(" <a style=\"font-weight:bold\">EMPCODE: ");
            Response.Write("<a style=\"font-weight:bold\">" + EmpCode1 + "</a>");
            Response.Write(" &nbsp; &nbsp; &nbsp; ");
            Response.Write("<a style=\"font-weight:bold\">EMPNAME: ");
            Response.Write("<a style=\"font-weight:bold\">" + EmpName + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");

            ////Response.Write("Contract Details");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);





        }

    }

    public void MusterReport_BWDates()
    {
        DataTable mLocalDS = new DataTable();
        DateTime date1;
        date1 = Convert.ToDateTime(Date1);
        string dat = Date2;
        DateTime date2 = Convert.ToDateTime(dat);
        int daycount = (int)((date2 - date1).TotalDays);
        int daysAdded = 0;
        iEmpDet = EmpCode1;
        int daycount1 = (int)((date2 - date1).TotalDays);
        int daysAdded1 = 0;
        SSQL = "select DeptName,MachineID,ExistingCode,MachineID_Encrypt,FirstName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And EmpNo='" + iEmpDet + "'";

        mEmployee = objdata.ReturnMultipleValue(SSQL);

        if (mEmployee.Rows.Count > 0)
        {
            EmpName = mEmployee.Rows[0][4].ToString();
        }
        else
        {
            EmpName = "";
        }

        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();
            AutoDTable.Rows[daysAdded][0] = Convert.ToString(dayy.ToShortDateString());


            daycount -= 1;
            daysAdded += 1;
        }
        int count = 0;
        int day_col = 0;
        while (daycount1 >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded1).ToShortDateString());
            string Date_Value_Str = dayy.ToString("yyyy/MM/dd");
            string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
            mLocalDS = objdata.ReturnMultipleValue(qry_nfh);
            if (mLocalDS.Rows.Count > 0)
            {
                AutoDTable.Rows[day_col][1] = "NH";
            }
            else
            {
                string Employee_Week_Name = "";
                string Assign_Week_Name = "";
                DataTable mLocalDS1 = new DataTable();
                Employee_Week_Name = dayy.DayOfWeek.ToString();

                SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + iEmpDet + "'";
                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                if (mLocalDS1.Rows.Count <= 0)
                {
                    Assign_Week_Name = "";
                }
                else
                {
                    Assign_Week_Name = mLocalDS1.Rows[0][0].ToString();
                }
                if (Employee_Week_Name == Assign_Week_Name)
                {
                    AutoDTable.Rows[day_col][1] = "WH";
                }
                else
                {

                    SSQL = "select MachineID,ExistingCode,FirstName,DeptName,Present from LogTime_Days where MachineID='" + iEmpDet + "'";
                    SSQL = SSQL + " And CompCode= '" + SessionCcode + "' And LocCode= '" + SessionLcode + "'";
                    SSQL = SSQL + " And Attn_Date='" + dayy.ToString("MM/d/yyyy") + "'";

                    mLogTime = objdata.ReturnMultipleValue(SSQL);

                    if (mLogTime.Rows.Count > 0)
                    {
                        if (mLogTime.Rows[0]["Present"].ToString() == "1.0" | mLogTime.Rows[0]["Present"].ToString() == "0.5")
                        {

                            AutoDTable.Rows[day_col][1] = "<span style=color:green>" + 'x' + "</span>";
                            //   DataCells.Rows[k1][day_col1] = 'x';

                        }
                        else
                        {
                            AutoDTable.Rows[day_col][1] = "<span style=color:red>" + 'A' + "</span>";
                        }
                    }
                    daycount1 -= 1;
                    daysAdded1 += 1;
                    day_col += 1;
                }
            }
        }
    }
}
