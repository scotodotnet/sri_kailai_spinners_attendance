﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;

public partial class Default2 : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    UserCreationClass objUsercreation = new UserCreationClass();
    DataTable dtdDisplay = new DataTable();
    string utype;
    static int ss;
    string usert = "1";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | User Creation";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");
                
                CreateUserDisplay();
                //Dropdown_Company();
                //Dropdown_Location();
            }
        }
    }

    //public void Dropdown_Company()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.Dropdown_Company();
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        ddlCompany.Items.Add(dt.Rows[i]["Cname"].ToString());
    //    }
    //}

    //public void Dropdown_Location()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.dropdown_ParticularLocation(SessionCcode, SessionLcode);
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        ddlLocation.Items.Add(dt.Rows[i]["LocName"].ToString());
    //    }
    //}

    public void CreateUserDisplay()
    {
        string query = "Select * from [HR_Rights]..MstUsers where LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "' And UserCode <> 'Scoto'";
        dtdDisplay = objdata.RptEmployeeMultipleDetails(query);
        //dtdDisplay = objdata.UserCreationDisplay(SessionCcode,SessionLcode);
        if (dtdDisplay.Rows.Count > 0)
        {
            rptrCustomer.DataSource = dtdDisplay;
            rptrCustomer.DataBind();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string query = "";

        if (txtPassword.Text != txtConformPwd.Text)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Password and Conform Password Must Be Same');", true);
            ErrFlag = true;
        }
        else
        {
            if (ddlUserType.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select UserType');", true);
                ErrFlag = true;
            }
            else
            {

                if (btnSave.Text == "Update")
                {
                    DataTable Dtdedit = new DataTable();
                    objUsercreation.Ccode = SessionCcode.ToString();
                    objUsercreation.Lcode = SessionLcode.ToString();
                    if (ddlUserType.SelectedItem.Text == "Admin")
                    {
                        utype = "1";

                    }
                    else if (ddlUserType.SelectedItem.Text == "Non-Admin")
                    {
                        utype = "2";
                    }

                    objUsercreation.UserCode = utype;
                    objUsercreation.UserName = txtUserName.Text;
                    objUsercreation.Password = UTF8Encryption(txtPassword.Text.Trim());
                    string pwd = UTF8Encryption(txtPassword.Text);
                    objUsercreation.UserID = ss;
                    //Update Query
                    query = "Update [HR_Rights]..MstUsers set UserName='" + txtUserName.Text + "',Password='" + UTF8Encryption(txtPassword.Text) + "',IsAdmin='" + utype + "',";
                    query = query + " Mobile='',Department='',Designation='' Where UserCode='" + txtUserName.Text + "' And CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    //Dtdedit = objdata.EditUserCreation(objUsercreation);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Update sucessFully');", true);
                    CreateUserDisplay();
                    btnSave.Text = "Save";
                    clear();

                }
                else
                {
                    DataTable dtd = new DataTable();
                    DataTable DT_test = new DataTable();
                    query = "Select * from [HR_Rights]..MstUsers where UserCode='" + txtUserName.Text + "' And CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "'";
                    dtd = objdata.RptEmployeeMultipleDetails(query);
                    //dtd = objdata.CheckUser_Login(SessionCcode, SessionLcode, txtUserName.Text,usert);
                    if (dtd.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name Already Exist');", true);
                        ErrFlag = true;
                    }
                    else
                    {
                        objUsercreation.Ccode = SessionCcode.ToString();
                        objUsercreation.Lcode = SessionLcode.ToString();
                        if (ddlUserType.SelectedItem.Text == "Admin")
                        {
                            utype = "1";

                        }
                        else if (ddlUserType.SelectedItem.Text == "Non-Admin")
                        {
                            utype = "2";
                        }
                        else if (ddlUserType.SelectedItem.Text == "IF")
                        {
                            utype = "3";
                        }
                        objUsercreation.UserCode = utype.ToString();
                        objUsercreation.UserName = txtUserName.Text;
                        objUsercreation.Password = UTF8Encryption(txtPassword.Text.Trim());
                        string pwd = UTF8Encryption(txtPassword.Text);
                        //Insert Data
                        query = "Insert Into [HR_Rights]..MstUsers(UserCode,UserName,Password,IsAdmin,Mobile,Department,Designation,LocationCode,CompCode)";
                        query = query + " Values('" + txtUserName.Text + "','" + txtUserName.Text + "','" + UTF8Encryption(txtPassword.Text) + "','" + utype + "',";
                        query = query + " '','','','" + SessionLcode + "','" + SessionCcode + "')";
                        objdata.RptEmployeeMultipleDetails(query);

                        //objdata.UserCreationRegistration(objUsercreation);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Creation Save sucessFully');", true);
                        clear();
                        CreateUserDisplay();
                    }
                }
            }
        }
                   
    }

    
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(id);
                break;
            case ("Edit"):
                EditRepeaterData(id);
                break;
        }
    }

    private void EditRepeaterData(string id)
    {
        DataTable Dtd = new DataTable();
        string query = "Select * from [HR_Rights]..MstUsers where UserCode='" + id + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
        Dtd = objdata.RptEmployeeMultipleDetails(query);
        //Dtd = objdata.EditUserCreation(ss);
        if (Dtd.Rows.Count > 0)
        {
            String Utype = Dtd.Rows[0]["IsAdmin"].ToString();
            if (Utype == "1")
            {
                ddlUserType.SelectedIndex = 1;
            }
            else if (Utype == "2")
            {
                ddlUserType.SelectedIndex = 2;
            }
            txtUserName.Text = Dtd.Rows[0]["UserCode"].ToString();
            txtPassword.Text = Dtd.Rows[0]["Password"].ToString();

            CreateUserDisplay();
            btnSave.Text = "Update";
        }
    }

    private void DeleteRepeaterData(string id)
    {
        //string ss = Convert.ToString(id);
        //int ss = Convert.ToInt16(id);
        DataTable Dtd = new DataTable();
        string query = "Delete from [HR_Rights]..MstUsers where UserCode='" + id + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
        objdata.RptEmployeeMultipleDetails(query);
        //Dtd = objdata.DeleteUserCreation(ss);
        CreateUserDisplay();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
         
    }

    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

   
    protected void BtnClear_Click(object sender, EventArgs e)
    {
        clear();
    }

    public void clear()
    {
        ddlUserType.SelectedIndex = 0;
        txtUserName.Text = "";
        txtPassword.Text = "";
        btnSave.Text = "Save";
    }
}
