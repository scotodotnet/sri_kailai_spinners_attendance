﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class EmployeeLongAbsentReport : System.Web.UI.Page
{
    DataTable AutoDataTable = new DataTable();

    string SSQL;
    string Date1_str;
    string Date2_str;
    string LeaveDays;
    string WagesType;
    string[] Time_Minus_Value_Check;
    int j1 = 0;
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    DataTable mEmployeeDS = new DataTable();
    DataSet ds = new DataSet();

    System.Web.UI.WebControls.DataGrid grid =
                          new System.Web.UI.WebControls.DataGrid();


    protected void Page_Load(object sender, EventArgs e)
    {
        //SessionCcode = "ESM";
        //SessionLcode = "UNIT I";

       if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Emp Long Absent";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                li.Attributes.Add("class", "droplink active open");
            }

        SessionCompanyName = "EVEREADY SPINING MILL";
       
        Date1_str = Request.QueryString["FromDate"].ToString();
        Date2_str = Request.QueryString["ToDate"].ToString();
        LeaveDays = Request.QueryString["LeaveDays"].ToString();
        WagesType = Request.QueryString["WagesType"].ToString();
        //WagesType = "MONTHLY-STAFF";
        //ModeType = Request.QueryString["ModeType"].ToString();
        //ShiftType1 = Request.QueryString["ShiftType1"].ToString();
        //Date = Request.QueryString["Date"].ToString();
        //ddlShiftType = Request.QueryString["ddlShiftType"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();

       
        AutoDataTable.Columns.Add("EmpNo");
        AutoDataTable.Columns.Add("ExistingCode");
        AutoDataTable.Columns.Add("Dept");
        AutoDataTable.Columns.Add("EmpName");
        AutoDataTable.Columns.Add("Designation");
        AutoDataTable.Columns.Add("AbsentCount");
       

        Long_Leave_Absent_Report();



        grid.DataSource = AutoDataTable;
        grid.DataBind();
        string attachment = "attachment;filename=LONGABSENTREPORT.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);

        Response.Write("<table>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
        Response.Write("--");
        Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">LONG ABSENT REPORT &nbsp;&nbsp;&nbsp;</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\"> FromDate: -" + Date1_str + "</a>");
        Response.Write("&nbsp;&nbsp;&nbsp;");
        Response.Write("<a style=\"font-weight:bold\"> ToDate: -" + Date2_str + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();

       }

    }
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    public void Long_Leave_Absent_Report()
    {
        DateTime date1;
        date1 = Convert.ToDateTime(Date1_str);
        string dat = Date2_str;
        DateTime Date2 = Convert.ToDateTime(dat);


        int intI = 1;
        int intK = 1;
        int intCol = 0;
        //Spinning Incentive Var
        string Fin_Year = "";
        string Months_Full_Str = "";
        string Date_Col_Str = "";
        Int32 Month_Int = 1;
        string Spin_Wages = "";
        string Spin_Machine_ID_Str = "";


        //Attn_Flex Col Add Var
        string Att_Already_Date_Check = null;
        string Att_Year_Check = null;
        int Month_Name_Change = 0;
        string halfPresent = "0";
        Att_Year_Check = "";
        Int32 EPay_Total_Days_Get = 0;

       DateTime dtpLong_Leave_Date = Date2;
       string Leave_Days_Min = "-" + LeaveDays;
       int Leave_Days_Minus = Convert.ToInt16(Leave_Days_Min);
       dtpLong_Leave_Date = dtpLong_Leave_Date.AddDays(Leave_Days_Minus);

       intCol = 4;
       Month_Name_Change = 1;
       int daycount = (int)((Date2 - date1).TotalDays);
       int daysAdded = 0;
       Att_Already_Date_Check = "";

       while (daycount >= 0)
       {
           daycount -= 1;
           daysAdded += 1;
       }

       //get Employee Details
       DataTable Emp_DS = new DataTable();
       SSQL = "Select * from Employee_MST where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='YES'";
       SSQL = SSQL + " And Wages='" + WagesType + "'";
       SSQL = SSQL + " And DOJ <='" + Date2.ToString("yyyy/MM/dd") + "'";
       SSQL = SSQL + " Order by ExistingCode Asc";
       Emp_DS = objdata.ReturnMultipleValue(SSQL);
       if (Emp_DS.Rows.Count == 0)
       {
          return;
       }

       bool Transfer_Check_blb = false;
       string Leave_Days_Check_Date = dtpLong_Leave_Date.AddDays(0).ToString("yyyy/MM/dd");



                for (int intRow = 0; intRow < Emp_DS.Rows.Count; intRow++) 
                {
                    intK = 1;

                    //Get Employee Week OF DAY
                    DataTable DS_WH = new DataTable();
                    string Emp_WH_Day = "";
                    SSQL = "Select * from Employee_MST where MachineID='" + Emp_DS.Rows[intRow]["MachineID"] + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    DS_WH = objdata.ReturnMultipleValue(SSQL);
                    if (DS_WH.Rows.Count != 0) 
                    {
                        Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                    } else 
                    {
                        Emp_WH_Day = "";
                    }
            int colIndex = intK;
            bool isPresent = false;
            decimal Present_Count = 0;
            decimal Leave_Days_Present_Count = 0;
            decimal Month_WH_Count = 0;
            decimal Present_WH_Count = 0;
            Int32 Appsent_Count = 0;
            decimal Final_Count = 0;
            decimal Total_Days_Count = 0;

            string Already_Date_Check = null;
            string Year_Check = null;
            Int32 Days_Insert_RowVal = 0;

            decimal FullNight_Shift_Count = 0;
            Int32 NFH_Days_Count = 0;
            Already_Date_Check = "";
            Year_Check = "";
            bool Leave_Days_bool = false;
            for (intCol = 0; intCol < daysAdded; intCol++) 
            {
                isPresent = false;
                string Emp_Total_Work_Time_1 = "00:00";
                string Machine_ID_Str = "";
                string OT_Week_OFF_Machine_No = null;
                string Date_Value_Str = "";
                string Total_Time_get = "";
                string Final_OT_Work_Time_1 = "00:00";
            
                string Time_IN_Str = "";
                string Time_Out_Str = "";
                //Dim Total_Time_get As String = ""
                Int32 j = 0;
                double time_Check_dbl = 0;
                string Date_Value_Str1 = "";
                isPresent = false;


                //Shift Change Check Variable Declaration Start

                string From_Time_Str = "";
                string To_Time_Str = "";
                string Final_InTime = "";
                string Final_OutTime = "";
                string Final_Shift = "";
                Int32 K = 0;
                bool Shift_Check_blb = false;
                string Employee_Time_Change = "";
                //Shift Change Check Variable Declaration End


                Spin_Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID"].ToString();

                Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID_Encrypt"].ToString();
                OT_Week_OFF_Machine_No = Emp_DS.Rows[intRow]["MachineID"].ToString();
                Date_Value_Str = date1.AddDays(intCol).ToString("yyyy/MM/dd");
                
                DateTime Date_Value1 = Convert.ToDateTime(Date_Value_Str);
                DateTime Date_Value2 = Date_Value1.AddDays(1);
                Date_Value_Str1 = Date_Value2.ToString("yyyy/MM/dd");

                //TimeIN Get
                DataTable mLocalDS = new DataTable();
                DataTable mLocalDS1 = default(DataTable);
                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
                mLocalDS = objdata.ReturnMultipleValue(SSQL);
                if (mLocalDS.Rows.Count <= 0) 
                {
                    Time_IN_Str = "";
                } else 
                {
                    //Time_IN_Str = mLocalDS.Tables(0).Rows(0)(0)
                }
                //TimeOUT Get
                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";
                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                if (mLocalDS.Rows.Count <= 0) 
                {
                    Time_Out_Str = "";
                } else 
                {
                    //Time_Out_Str = mLocalDS.Tables(0).Rows(0)(0)
                }

                 if (mLocalDS.Rows.Count > 1) 
                 {
                    for (int tin = 0; tin < mLocalDS.Rows.Count; tin++) 
                    {
                        Time_IN_Str = mLocalDS.Rows[tin][0].ToString();

                        if (mLocalDS1.Rows.Count > tin) 
                        {
                            Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                        } else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count) 
                        {
                            Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                        } else 
                        {
                            Time_Out_Str = "";
                        }
                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS.Rows.Count <= 0) {
                            Time_IN_Str = "";
                        } else 
                        {
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                        }
                        if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str)) 
                        {
                            time_Check_dbl = time_Check_dbl;
                        } else 
                        {
                            DateTime date11 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date12 = System.Convert.ToDateTime(Time_Out_Str);
                            //If date1 > date2 Then

                            //    date2 = System.Convert.ToDateTime(mLocalDS1.Tables(0).Rows(tin + 1)(0))
                            //End If
                            TimeSpan ts = new TimeSpan();
                            ts = date12.Subtract(date11);
                            ts = date12.Subtract(date11);
                            Total_Time_get = Convert.ToString(ts.Hours);
                            //& ":" & Trim(ts.Minutes)
                            ts4 = ts4.Add(ts);
                            //OT Time Get
                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                            if (Left_Val(Convert.ToString(ts4.Minutes), 1) == "-" | Left_Val(Convert.ToString(ts4.Hours), 1) == "-" | Emp_Total_Work_Time_1 == "0:-1") {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Total_Time_get, 1) == "-") {
                                date12 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date12.Subtract(date11);
                                ts = date12.Subtract(date11);
                                Total_Time_get = Convert.ToString(ts.Hours);
                                //& ":" & Trim(ts.Minutes)
                                time_Check_dbl = Convert.ToDouble(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Emp_Total_Work_Time_1 = "00:00";
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    Emp_Total_Work_Time_1 = "00:00";
                            } else {
                                time_Check_dbl = time_Check_dbl + Convert.ToDouble(Total_Time_get);
                            }
                        }
                    }
                } else {
                    TimeSpan ts4 = new TimeSpan();
                    ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                    if (mLocalDS.Rows.Count <= 0) {
                        Time_IN_Str = "";
                    } else {
                        Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                    }
                    for (int tout = 0; tout <= mLocalDS1.Rows.Count - 1; tout++) {
                        if (mLocalDS1.Rows.Count <= 0) {
                            Time_Out_Str = "";
                        } else {
                            Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                        }

                    }
                    //Emp_Total_Work_Time
                    if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str)) {
                        time_Check_dbl = 0;
                    } else {
                        DateTime date11 = System.Convert.ToDateTime(Time_IN_Str);
                        DateTime date12 = System.Convert.ToDateTime(Time_Out_Str);
                        TimeSpan ts = new TimeSpan();
                        ts = date12.Subtract(date11);
                        ts = date12.Subtract(date11);
                        Total_Time_get = Convert.ToString(ts.Hours);
                        //& ":" & Trim(ts.Minutes)
                        ts4 = ts4.Add(ts);
                        //OT Time Get
                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            Emp_Total_Work_Time_1 = "00:00";
                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                            Emp_Total_Work_Time_1 = "00:00";
                        //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                        if (Left_Val(Total_Time_get, 1) == "-") {
                            date12 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                            ts = date12.Subtract(date11);
                            ts = date12.Subtract(date11);
                            Total_Time_get = Convert.ToString(ts.Hours);
                            //& ":" & Trim(ts.Minutes)
                            time_Check_dbl = Convert.ToDouble(Total_Time_get);
                            //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                            Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                Emp_Total_Work_Time_1 = "00:00";
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                Emp_Total_Work_Time_1 = "00:00";
                        } else {
                            time_Check_dbl = Convert.ToDouble(Total_Time_get);
                        }
                    }
                }
                //Emp_Total_Work_Time
                string Emp_Total_Work_Time = "";
                string Final_OT_Work_Time = "00:00";
                string Final_OT_Work_Time_Val = "00:00";
                Emp_Total_Work_Time = Emp_Total_Work_Time_1;
                //Find Week OFF
                string Employee_Week_Name = "";
                string Assign_Week_Name = "";
                mLocalDS = null;
                Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "'";
                mLocalDS = objdata.ReturnMultipleValue(SSQL);
                if (mLocalDS.Rows.Count <= 0) {
                    Assign_Week_Name = "";
                } else {
                    Assign_Week_Name = mLocalDS.Rows[0][0].ToString();
                }
                string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                mLocalDS = objdata.ReturnMultipleValue(qry_nfh);
                if (mLocalDS.Rows.Count > 0) {
                    isPresent = true;
                    NFH_Days_Count = NFH_Days_Count + 1;
                } else {
                    //Get Employee Work Time
                    double Calculate_Attd_Work_Time = 0;
                    double Calculate_Attd_Work_Time_half = 0;
                    SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and Calculate_Work_Hours <> '' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                    if (mLocalDS.Rows.Count <= 0) {
                        Calculate_Attd_Work_Time = 7;
                    } else {
                        if(!string.IsNullOrEmpty(mLocalDS.Rows[0]["Calculate_Work_Hours"].ToString()))
                        {
                        Calculate_Attd_Work_Time = Convert.ToDouble(mLocalDS.Rows[0]["Calculate_Work_Hours"].ToString());
                        }
                        else
                        {
                            Calculate_Attd_Work_Time = 0;
                        }
                        if (Calculate_Attd_Work_Time == 0) {
                            Calculate_Attd_Work_Time = 7;
                        } else {
                            Calculate_Attd_Work_Time = 7;
                        }
                    }
                    Calculate_Attd_Work_Time_half = 4.0;
                    halfPresent = "0";
                    //Half Days Calculate Start
                    if (time_Check_dbl >= Calculate_Attd_Work_Time_half & time_Check_dbl < Calculate_Attd_Work_Time) {
                        isPresent = true;
                        halfPresent = "1";
                    }
                    //Half Days Calculate End
                    if (time_Check_dbl >= Calculate_Attd_Work_Time) {
                        isPresent = true;
                        halfPresent = "0";
                    }

                }
                if (Date_Value_Str == Leave_Days_Check_Date)
                    Leave_Days_bool = true;
                if (isPresent == true) {
                    if (Leave_Days_bool == true) {
                        if (halfPresent == "1") {
                            Leave_Days_Present_Count = Leave_Days_Present_Count + Convert.ToDecimal(0.5);
                        } else if (halfPresent == "0") {
                            Leave_Days_Present_Count = Leave_Days_Present_Count + 1;
                        }
                    }
                    if (halfPresent == "1") {
                        Present_Count = Present_Count + Convert.ToDecimal(0.5);
                    } else if (halfPresent == "0") {
                        Present_Count = Present_Count + 1;
                    }
                } else {
                    Appsent_Count = Appsent_Count + 1;
                }
                //colIndex += shiftCount
                intK += 1;
                Total_Days_Count = Total_Days_Count + 1;
            }
            Final_Count = Present_Count;

        

            //EPay Update

            //Update Long Leave Details
            double Apsent_Leave_Days = Convert.ToDouble(LeaveDays);
            if (Appsent_Count >= Apsent_Leave_Days) {
                //If Leave_Days_Present_Count = 0 Then
                //Insert Long Leave Person
               
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();
               
                AutoDataTable.Rows[j1]["EmpNo"]=Emp_DS.Rows[intRow]["EmpNo"].ToString();
                AutoDataTable.Rows[j1]["ExistingCode"] = Emp_DS.Rows[intRow]["ExistingCode"].ToString();
                AutoDataTable.Rows[j1]["EmpName"] = Emp_DS.Rows[intRow]["FirstName"].ToString();
                AutoDataTable.Rows[j1]["Dept"] = Emp_DS.Rows[intRow]["DeptName"].ToString();
                AutoDataTable.Rows[j1]["Designation"] = Emp_DS.Rows[intRow]["Designation"].ToString();
                AutoDataTable.Rows[j1]["AbsentCount"]=Appsent_Count;
              
                j1 = j1 + 1;

            }

            intI += 1;
            Total_Days_Count = 0;
            Final_Count = 0;
            Appsent_Count = 0;
            Present_Count = 0;
            Month_WH_Count = 0;
            Present_WH_Count = 0;
            Leave_Days_Present_Count = 0;
        }
       
        return;

    }


    }






