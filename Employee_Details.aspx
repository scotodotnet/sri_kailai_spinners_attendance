﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Employee_Details.aspx.cs" Inherits="EmployeeDetails" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tableCustomer').dataTable();               
            }
        });
    };
    </script>
    
    
    <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tableWagesType').dataTable();               
            }
        });
    };
    </script>
    
    
    
    <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tableEmpType').dataTable();               
            }
        });
    };
    </script>
    
    
    <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tableDesign').dataTable();               
            }
        });
    };
    </script>

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

    <script>
        $(document).ready(function () {
            $('#tableWagesType').dataTable();
        });
    </script>
     
     
     
     
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

    <script>
        $(document).ready(function () {
            $('#tableEmpType').dataTable();
        });
    </script>
    

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

    <script>
        $(document).ready(function () {
            $('#tableDesign').dataTable();
        });
    </script>

         
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

    <script>
        $(document).ready(function () {
            $('#tableCustomer').dataTable();
        });
    </script>


 
                                       
 <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">User Creation</li></h4> 
                    </ol>
                </div>
 
 <div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
    <div class="col-md-9">
        <div class="panel panel-white">
			   <div class="panel panel-primary">
            <div class="panel-heading clearfix">
                <h3 class="panel-title">Employee Details</h3>
            </div>
            </div>
				
               
			<div class="panel-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
					   <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Department</a></li>
                        <li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Designation</a></li>
                        <li role="presentation"><a href="#tab3" role="tab" data-toggle="tab">Employee Type</a></li>
                        <li role="presentation"><a href="#tab4" role="tab" data-toggle="tab">Wages Type</a></li>
                    </ul>
      					 <div class="tab-content">
      					   <!-- tab1 start -->
      					    <div role="tabpanel" class="tab-pane active" id="tab1">
      					    					                      
                              <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>                   
        				         <form class="form-horizontal">
						           <div class="col-md-1"></div>
						            <div class="col-md-12">
			                          <div class="panel panel-white">
				                        <form class="form-horizontal">
				                          <div class="panel-body">
				                          
				                                <div class="form-group row">
                                                <asp:Button ID="btnDept" class="btn btn-primary btn-rounded" runat="server" Text="ADD"/>
                                                <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panl1" TargetControlID="btnDept"
                                                 CancelControlID="" BackgroundCssClass="Background">
                                                </cc1:ModalPopupExtender>
                                                <asp:Panel ID="Panl1" runat="server" CssClass="Popup" align="center" BackColor="White" Height="269px" Width="400px" style="display:none">
				                          	        <table width="100%" style="border:Solid 3px #EEE8AA; width:100%; height:100%" cellpadding="0" cellspacing="0">
                                                      <tr style="background-color:#D3D3D3">
                                                        <td colspan="2" style=" height:10%; color:White; font-weight:bold; font-size:larger" align="center">DEPARTMENT</td>
                                                     </tr>
                                                     <tr>
                                                        <td align="right"> DepartmentName:</td>
                                                        <td>
                                                          <asp:TextBox ID="txtDepartmentName" class="form-control" runat="server" AutoPostBack="true" required></asp:TextBox>
                                                       </td>
                                                    </tr>
                                                    <tr>
                                                      <td></td>
                                                       <td><asp:Button ID="btnSaveDept" class="btn btn-success"  runat="server" Text="Save" 
                                                        onclick="btnSaveDept_Click" />
                                                       <asp:LinkButton ID="BtnClear" runat="server" class="btn btn-danger" onclick="BtnClear_Click" >Clear</asp:LinkButton>  
                                                   
                                                      </td>
                                                  </tr>
                                                  </table>						
                                                </asp:Panel>
                                                <div class="col-sm-1"></div>
                                                </div>
                                                                                
		                                        <div class="panel-body">
                                                 <div class="table-responsive">
                                                   <asp:Repeater ID="rptrCustomer" runat="server" onitemcommand="rptrCustomer_ItemCommand">
                                                  <HeaderTemplate>
                                                    <table id="tableCustomer" class="display table" style="width: 100%; cellspacing: 0;">
                                                    <thead>
                                                      <tr>
                                                     <%-- <th>DeptCode</th>--%>
                                                      <th>DeptName</th>
                                                      <th>Edit</th>
                                                      <th>Delete</th>
                                                      </tr>
                                                   </thead>
                                                 </HeaderTemplate>
                                                 <ItemTemplate>
                                                    <tr>
                    
                  
                    
                  <%--  <td>
                        <%# DataBinder.Eval(Container.DataItem, "DeptCode")%>
                    </td>--%>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "DeptName")%>
                    </td>
                  
                    <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("DeptCode") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("DeptCode") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
                                               </ItemTemplate>
                                                <FooterTemplate>
              
                                                    </table>
                                            </FooterTemplate>
                                            </asp:Repeater>
                                                </div>
                                               </div>
                                                         
						                        </div>
						                        </form>
						                        
						                       </div>
						                     </div>
						         </form>
						                 
						            </ContentTemplate>
                             </asp:UpdatePanel>   
						  	                 
						   </div>
						                  
						                 
						   <!-- tab1 end -->
						   
						   <!-- tab2 start -->
                            <div role="tabpanel" class="tab-pane" id="tab2">
                                                                 
                              <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                   <ContentTemplate>       
                   
                              <form class="form-horizontal">
						           <div class="col-md-1"></div>
						            <div class="col-md-12">
			                          <div class="panel panel-white">
				                        <form class="form-horizontal">
				                          <div class="panel-body">
				                          
				                          
					                             <div class="form-group row">
			                                  <asp:Button ID="btnDesgn" class="btn btn-primary btn-rounded" runat="server" Text="ADD"/>
                                              <cc1:ModalPopupExtender ID="MPE1" runat="server" PopupControlID="Panel1" TargetControlID="btnDesgn"
                                              CancelControlID="" BackgroundCssClass="Background">
                                              </cc1:ModalPopupExtender>
                                              <asp:Panel ID="Panel1" runat="server" CssClass="Popup" align="center" BackColor="White" Height="269px" Width="400px" style="display:none">
				                            	<table width="100%" style="border:Solid 3px #EEE8AA; width:100%; height:100%" cellpadding="0" cellspacing="0">
                                                 <tr style="background-color:#D3D3D3">
                                                   <td colspan="2" style=" height:10%; color:White; font-weight:bold; font-size:larger" align="center">DESIGNATION</td>
                                                 </tr>
                                                 <tr>
                                                   <td align="right">Designation Name</td>
                                                   <td><asp:TextBox ID="txtDesginationName" class="form-control" runat="server" required></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                <td></td>
                                                <td>
                                                   <%-- <asp:Button ID="btnSaveDesgination" runat="server" class="btn btn-success" onclick="btnSaveDesgination_Click" Text="Save" />--%>			
                                                 <asp:LinkButton ID="btnSaveDesgination" runat="server" class="btn btn-success" onclick="btnSaveDesgination_Click">Save</asp:LinkButton>        
                                                 <asp:LinkButton ID="btnClearDesgination" runat="server" class="btn btn-danger" onclick="btnClearDesgination_Click">Close</asp:LinkButton>   
                                               </td>
                                               </tr>
                                             </table>						
                                             </asp:Panel>
                                               <div class="col-sm-1"></div>
                                                </div>
                                
                                              <div class="col-md-12">
		                                      <div class="row">
                                                 <asp:Repeater ID="RepeaterDesign" runat="server" onitemcommand="RepeaterDesign_ItemCommand">
                                                 <HeaderTemplate>
                                                  <table id="tableDesign" class="display table" style="width: 100%; cellspacing: 0;">
                                                  <thead>
                                                   <tr>
                                                    <th>DesignName</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                        </tr>
                                                  </thead>
                                                 </HeaderTemplate>
                                                 <ItemTemplate>
                                                   <tr>
                    
                     <%--<td>
                        <%# DataBinder.Eval(Container.DataItem, "DesignNo")%>
                    </td>--%>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "DesignName")%>
                    </td>
                  
                    <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("DesignNo") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("DesignNo") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
                                                 </ItemTemplate>
                                                 <FooterTemplate>
              
                                                 </table>
                                                 </FooterTemplate>
                                                 </asp:Repeater>
                                                </div>
                                             </div>
                                          </div>
                                        </form>
                                             
                                           </div>
                                       </div>
		                           
			                  </form>
                               </ContentTemplate>
                              </asp:UpdatePanel>    
                     
                           </div>
						
						   <!-- tab2 end -->
						   
						    <!-- tab3 start -->
						     <div role="tabpanel" class="tab-pane" id="tab3">
                        
                              <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                         <ContentTemplate>                   
      					    
      					       <div class="form-horizontal">
						           <div class="col-md-1"></div>
						           <div class="col-md-12">
			                          <div class="panel panel-white">
				                        <div class="form-horizontal">
				                          <div class="panel-body">
				                          			                          
					                           <div class="form-group row">
																	
						<asp:Button ID="btnEmpType" class="btn btn-primary btn-rounded" runat="server" Text="ADD"/>
						
						  
						
						
						 
<cc1:ModalPopupExtender ID="MPE2" runat="server" PopupControlID="Panel2" TargetControlID="btnEmpType"
CancelControlID="" BackgroundCssClass="Background">
</cc1:ModalPopupExtender>
<asp:Panel ID="Panel2" runat="server" CssClass="Popup" align="center" BackColor="White" Height="269px" Width="400px" style="display:none">
				                          	<table width="100%" style="border:Solid 3px #EEE8AA; width:100%; height:100%" cellpadding="0" cellspacing="0">
<tr style="background-color:#D3D3D3">
<td colspan="2" style=" height:10%; color:White; font-weight:bold; font-size:larger" align="center">Employee Type</td>
</tr>
<tr>
<td align="right">
Employee Type
</td>
<td>
<asp:TextBox ID="txtEmployeeType" class="form-control" runat="server" AutoPostBack="true" required></asp:TextBox>
</td>
</tr>
<tr>
<td>
</td>
<td>				                           <!-- Button start -->
                               
                            
                           <asp:LinkButton ID="btnSaveEmployeeType" runat="server" class="btn btn-success" onclick="btnSaveEmployeeType_Click" >Save</asp:LinkButton>
                           <asp:LinkButton ID="BtnClearEmployeeType" runat="server" class="btn btn-danger" onclick="BtnClearEmployeeType_Click"  >Close</asp:LinkButton>  
                            
                    </td>
</tr>
</table>						
</asp:Panel>
</div>
                                                                              
		                                       <div class="col-md-12">
		                                      <div class="row">
                      <asp:Repeater ID="rptrEmplyeeType" runat="server" onitemcommand="rptrEmplyeeType_ItemCommand">
                    <HeaderTemplate>
                       <table id="tableEmpType" class="display table" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                                                   
                                                    <th>TypeName</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    
                  
                    
                  
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "TypeName")%>
                    </td>
                  
                    <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("TypeID") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("TypeID") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
                        </div>
                       
                                   
						           </div>
						                      </div>
						                     </div>
						                     </div>
						                 </div>
						                 
						                </ContentTemplate>
                              </asp:UpdatePanel>  
                        
                        
                        
						    </div>
						    <!-- tab3 end -->
						  
						  <!-- tab4 start -->
						  
						  
						  
						  
						  
						  
                              <div role="tabpanel" class="tab-pane" id="tab4">
                         
                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                         <ContentTemplate>                   
      					    
      					       <div class="form-horizontal">
						           <div class="col-md-1"></div>
						           <div class="col-md-12">
			                          <div class="panel panel-white">
				                        <div class="form-horizontal">
				                          <div class="panel-body">
				                          			                          
					                           <div class="form-group row">
																	
						<asp:Button ID="btnWgsType" class="btn btn-primary btn-rounded" runat="server" Text="ADD"/>
<cc1:ModalPopupExtender ID="MPE3" runat="server" PopupControlID="Panel3" TargetControlID="btnWgsType"
CancelControlID="" BackgroundCssClass="Background">
</cc1:ModalPopupExtender>
<asp:Panel ID="Panel3" runat="server" CssClass="Popup" align="center" BackColor="White" Height="269px" Width="400px" style="display:none">
				                          	<table width="100%" style="border:Solid 3px #EEE8AA; width:100%; height:100%" cellpadding="0" cellspacing="0">
<tr style="background-color:#D3D3D3">
<td colspan="2" style=" height:10%; color:White; font-weight:bold; font-size:larger" align="center">Employee Type</td>
</tr>
<tr>
<td align="right">
Wages Type
</td>
<td>
<asp:TextBox ID="txtWagesType" class="form-control" runat="server" AutoPostBack="true" required></asp:TextBox>
 </td>
</tr>
<tr>
<td>
</td>
<td>                                          
                                            <!-- Button start -->

    <asp:LinkButton ID="btnSaveWagesType" class="btn btn-success" runat="server" onclick="btnSaveWagesType_Click">Save</asp:LinkButton>
    
    <asp:LinkButton ID="BtnClearWagesType" runat="server" class="btn btn-danger" 
     onclick="BtnClearWagesType_Click"  >Close</asp:LinkButton>   
                          
   </td>
</tr>
</table>						
</asp:Panel>
</div>                            
<div class="col-md-12">
		                                      <div class="row">
                      <asp:Repeater ID="rptrWagesType" runat="server" onitemcommand="rptrWagesType_ItemCommand">
                    <HeaderTemplate>
                       <table id="tableWagesType" class="display table" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                                                  <%--  <th>WagesTypeNo</th>--%>
                                                    <th>WagesTypeName</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    
                  
                    
                  <%--  <td>
                        <%# DataBinder.Eval(Container.DataItem, "WagesTypeNo")%>
                    </td>--%>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "WagesTypeName")%>
                    </td>
                  
                    <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("WagesTypeNo") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("WagesTypeNo") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
                        </div>
                       
                                   
						           </div>
						                      </div>
						                     </div>
						                     </div>
						                 </div>
						                 
						                </ContentTemplate>
                              </asp:UpdatePanel>  
                                 
                               
                         
                         
                         
						      </div> <!-- tab4 end -->
						  
						 </div><!-- tab -content end -->
                </div><!-- tab -panel end -->
            </div><!-- panel body end -->
        </div> <!-- panel  end -->
        </div><!-- col 9 end -->
        
	    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
             <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
</div><!-- col 12 end -->
</div><!-- row end -->

</div><!-- main-wrapper end -->


</asp:Content>

