﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class WagesTypeDetail : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    UserCreationClass objUsercreation = new UserCreationClass();
    DataTable dtdDisplay = new DataTable();

    static int ss;
    bool ErrFlag = false;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | WagesType Details";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");
                
                CreateWagesType();
            }
        }
    }

    protected void btnSaveWagesType_Click(object sender, EventArgs e)
    {

        if (txtWagesType.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
            ErrFlag = true;
        }
        else
        {
            DataTable dtt = new DataTable();
            if (btnSaveWagesType.Text == "Update")
            {
                dtt = objdata.WagesTypeUpdate(txtWagesType.Text, ss);
                CreateWagesType();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Sussessfully');", true);

            }
            else
            {
                DataTable dt = new DataTable();
                DataTable dtble = new DataTable();

                dtble = objdata.CheckWagesType_AlreadyExist(txtWagesType.Text);
                if (dtble.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('EmployeeType Already Exist');", true);
                    txtWagesType.Text = "";
                }
                else
                {
                    dt = objdata.WagesTypeRegister(txtWagesType.Text);
                    txtWagesType.Text = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Save Successfully');", true);
                }

                CreateWagesType();
            }
        }
    }


    protected void BtnClearWagesType_Click(object sender, EventArgs e)
    {
        ClearWages();
        btnSaveWagesType.Text = "Save";
    }

    public void ClearWages()
    {
        txtWagesType.Text = "";
    }

    public void CreateWagesType()
    {
        DataTable dtWagesTypeDisplay = new DataTable();
        dtWagesTypeDisplay = objdata.WagesTypeDisplay();

        if (dtWagesTypeDisplay.Rows.Count > 0)
        {
            rptrWagesType.DataSource = dtWagesTypeDisplay;
            rptrWagesType.DataBind();
        }

    }

    protected void rptrWagesType_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterDataWagesType(id);
                break;
            case ("Edit"):
                MPE3.Show();
                EditRepeaterDataWagesType(id);
                break;
        }
    }

    private void EditRepeaterDataWagesType(string WgsTypeNo)
    {
        string ss = WgsTypeNo;
        DataTable Dtd = new DataTable();
        Dtd = objdata.EditWagesType(ss);
        if (Dtd.Rows.Count > 0)
        {
            txtWagesType.Text = Dtd.Rows[0]["WagesTypeName"].ToString();
        }
        //btnSaveEmployeeType.Text = "Update";
        btnSaveWagesType.Text = "Update";

    }
    private void DeleteRepeaterDataWagesType(string WgsTypeNo)
    {
        string ss = WgsTypeNo;

        DataTable DtdCheck = new DataTable();
        DtdCheck = objdata.EditWagesType(ss);
        if (DtdCheck.Rows.Count > 0)
        {
            string TypeName = DtdCheck.Rows[0]["WagesTypeName"].ToString();
            DataTable CheckInWages = new DataTable();
            CheckInWages = objdata.CheckingWagesType(TypeName);
            if (CheckInWages.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' Wages Type Allocated to Employee');", true);
            }
            else
            {
                DataTable Dtd = new DataTable();
                Dtd = objdata.DeleteWagesType(ss);
                CreateWagesType();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
            }
        }
        CreateWagesType();

    }





    
}
