<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="report.aspx.cs" Inherits="report" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
        $(document).ready(function () {
            $("ul[id*=myid] li").click(function () {
                alert($(this).html()); // gets innerHTML of clicked li
                alert($(this).text()); // gets text contents of clicked li
            });
        });
</script>

              <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Report</li></h4> 
                    </ol>
                </div>

                                     
        <div id="main-wrapper" class="container">
          <div class="row">
                 <div class="col-md-12">
                 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                       <ContentTemplate>
                  <div class="col-sm-6">
                         <div class="panel panel-white">
                                <div class="panel panel-primary">
                                <div class="panel-heading clearfix">
                                    <h3 class="panel-title">Report Type</h3>
                                </div>
                                </div>
                                <div class="panel-body">
                                 
                                    <div>
                                    <div class="form-group col-md-6">
                                                    <label for="exampleInputName">REPORT NAME</label>
                                                      <asp:DropDownList ID="ddlRptName" runat="server" 
                                                        class="form-control col-md-6" AutoPostBack="true" 
                                                        onselectedindexchanged="ddlRptName_SelectedIndexChanged">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="ALL"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="SPECIFIC"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="ABSTRACT"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="OTHERS"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            
                                                            
                                                            </div>
                                                     <!-- <asp:ListItem Value="6" Text="EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES"></asp:ListItem>
                                                            <asp:ListItem Value="7" Text="DAY ATTENDANCE SUMMARY"></asp:ListItem>
                                                            <asp:ListItem Value="8" Text="EMPLOYEE MASTER"></asp:ListItem>
                                                            <asp:ListItem Value="9" Text="EMPLOYEE FULL PROFILE"></asp:ListItem>-->
                                                            
                                               
                                    <div>
                                    
                                        <asp:ListBox ID="ListRptName" runat="server" Width="346px" Height="409px" 
                                            Font-Bold="True" Font-Names="Times New Roman" Font-Size="Medium" 
                                            onselectedindexchanged="ListRptName_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem></asp:ListItem>
                                                </asp:ListBox>
                                                
                                    </div>
                                 
                                    </div>
                                    
                                </div>
                            </div>
                         </div> <!-- col-sm-6 -->  
                         
                                <div class="col-sm-6">
                                <div class="panel panel-white">
                                <div class="panel panel-primary">
                                <div class="panel-heading clearfix">
                                <h3 class="panel-title">Report</h3>
                                </div>
                                </div>
                                <div class="panel-body">
                                <div>
                                    <asp:Label ID="RptLabel" runat="server" Font-Bold="True" Height="100%" 
                                        Width="100%" align="center">Option</asp:Label>
                                </div>
                                <div class="form-group row">
						        </div>
                                <div class="col-md-12">
                                            <div class="row">
										    
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Department</label>
                                                            <asp:DropDownList ID="ddlDepartment" runat="server" class="form-control col-md-6">
                                                            </asp:DropDownList>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Wages Type</label>
                                                            <asp:DropDownList ID="ddlWagesType" runat="server" class="form-control col-md-6">
                                                             <%--<asp:ListItem>STAFF</asp:ListItem>
                                                             <asp:ListItem>LABOUR</asp:ListItem>--%>
                                                            </asp:DropDownList>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Employee Name</label>
                                                            <asp:DropDownList ID="ddlEmpName" runat="server" class="form-control col-md-6">
                                                            </asp:DropDownList>
                                               </div>
                                               
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Shift</label>
                                                            <asp:DropDownList ID="ddlShift" runat="server" class="form-control col-md-6">
                                                            </asp:DropDownList>
                                               </div>
                                               
                                              
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Year</label>
                                                            <asp:DropDownList ID="ddlYear" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                           
                                                           
                                                            </asp:DropDownList>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">IsActive</label>
                                                            <asp:DropDownList ID="ddlIsAct" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="Yes"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="No"></asp:ListItem>
                                                            </asp:DropDownList>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Category</label>
                                                            <asp:DropDownList ID="ddlCategory" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="STAFF"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="LABOUR"></asp:ListItem>
                                                           
                                                            </asp:DropDownList>
                                               </div>
                                               
                                               
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Type Of Certificate</label>
                                                    <asp:TextBox ID="txtTypeOfCertificate" class="form-control col-md-6" runat="server"></asp:TextBox>
												</div>
												<div class="form-group col-md-6">
                                                   
												
												 <label for="exampleInputName" >From Date</label>
                                                    <asp:TextBox ID="txtFrmdate" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                 <cc1:CalendarExtender ID="CalendarExtender2" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtFrmdate">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtFrmdate" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
												
												</div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName" >To Date</label>
                                                    <asp:TextBox ID="txtTodate" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                 <cc1:CalendarExtender ID="CalendarExtender3" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtTodate">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtTodate" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
												</div>
												<div class="form-group col-md-6">
                                                    <label for="exampleInputName">Leave Days</label>
                                                    <asp:TextBox ID="txtleavedays" class="form-control col-md-6" runat="server"></asp:TextBox>
												
												</div>
                                               
                                                 <div class="form-group col-md-6">
                                                    <label for="exampleInputName" >OT Eligible</label>
                                                    <asp:RadioButton ID="OTYes" runat="server" Text ="Yes" AutoPostBack="true" 
                                                        oncheckedchanged="OTYes_CheckedChanged"/>
													<asp:RadioButton ID="OTNo" runat="server" Text ="No" AutoPostBack="true" 
                                                        oncheckedchanged="OTNo_CheckedChanged"/> 
                                                 </div>
                                               
                                                  <div class="form-group col-md-6"   >
                                                   <%-- <label for="exampleInputName" >OT Eligible</label>--%>
                                                    <asp:Label ID="Label1" runat="server" Text="Gender" ></asp:Label>
                                                    <asp:RadioButton ID="RdbGendarM" runat="server" Text ="Male" AutoPostBack="true" 
                                                        oncheckedchanged="RdbGendarM_CheckedChanged"/>
													<asp:RadioButton ID="RdbGendarF" runat="server" Text ="Female" AutoPostBack="true" 
                                                        oncheckedchanged="RdbGendarF_CheckedChanged"/> 
                                                 </div>
											</div>
										</div>
										
										<!-- Button start -->
						<div class="form-group row">
						<div class="txtcenter">
			        <%--<asp:Button ID="btn30days" class="btn btn-info"  runat="server" Text="30 days" />
                    <asp:Button ID="btn60days" class="btn btn-info" runat="server" Text="60 days" />--%>
                   
                    </div>
                    </div>
                 <div class="txtcenter">
					<div class="form-group row">
					     <asp:Button ID="btnExcel" class="btn btn-info" runat="server" Text="Excel" 
                                onclick="btnExcel_Click" />
						<asp:Button ID="btnReport" class="btn btn-success" runat="server" Text="Report" 
                            onclick="btnReport_Click" />
                        <asp:Button ID="btnClear" class="btn btn-danger" runat="server" Text="Clear" onclick="btnClear_Click"/>
                       <asp:Button ID="btnEmployee" class="btn btn-info" runat="server" 
                            Text="Employee" onclick="btnEmployee_Click" />
                    </div>
                    
                   <div class="form-group row">
                   
                           <asp:Button ID="btnAttendanceDetails" class="btn btn-info" runat="server" 
                            Text="Attendance Details" onclick="btnAttendanceDetails_Click" visible="false"/>
                    </div> 
                    </div>
                    <!-- Button End -->
                    
                                </div>
                                
                                </div>
                                </div>       
                      </ContentTemplate>
                                    </asp:UpdatePanel>
                       
                    </div>   
                 </div>   
              </div>      
                                     
       
</asp:Content>

