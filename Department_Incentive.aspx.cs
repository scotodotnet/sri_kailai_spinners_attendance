﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;

public partial class DepartmentIncentive : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    ManualAttendanceClass objManual = new ManualAttendanceClass();
    bool ErrFlag = false;
    string SSQL;
    string MonthDays_Get;

    DataTable dtdDisplay = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Department Incentive";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                li.Attributes.Add("class", "droplink active open");

                Dropdown_Company();
                Dropdown_Location();
                Dropdown_WagesType();
                Financial_Year();
                DropDown_TokenNumber();


                DisplayDepartment_Incentive();
            }
        }
    }


    public void DisplayDepartment_Incentive()
    {
        dtdDisplay = objdata.DepartmentIncentiveDisplay(SessionCcode, SessionLcode);
        if (dtdDisplay.Rows.Count > 0)
        {
            rptrCustomer.DataSource = dtdDisplay;
            rptrCustomer.DataBind();
        }
    }

   


    public void Dropdown_WagesType()
    {
        DataTable dt = new DataTable();
        dt = objdata.dropdown_WagesType();
        ddlWages.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["WagesTypeName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlWages.Items.Add(dt.Rows[i]["WagesTypeName"].ToString());
        }
    }


    public void Dropdown_Company()
    {
        DataTable dt = new DataTable();
        dt = objdata.Dropdown_Company();
        for (int i = 0; i < dt.Rows.Count; i++)
        {


            ddlCompanyCode.Items.Add(dt.Rows[i]["Cname"].ToString());
        }
    }

    public void Dropdown_Location()
    {
        DataTable dt = new DataTable();
        dt = objdata.dropdown_ParticularLocation(SessionCcode, SessionLcode);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlLocationCode.Items.Add(dt.Rows[i]["LocCode"].ToString());
        }
    }

    public void DropDown_TokenNumber()
    {

        bool ErrFlag = false;
        DataTable dt = new DataTable();
        dt = objdata.dropdown_TokenNumber(SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {
            ddlTicketNo.DataSource = dt;
            DataRow dr = dt.NewRow();
            dr["EmpName"] = "- select -";
            dt.Rows.InsertAt(dr, 0);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlTicketNo.Items.Add(dt.Rows[i]["EmpName"].ToString());
            }
        }
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('');", true);
        //    ErrFlag = true;
        //}
    }


    public void Financial_Year()
    {
        String CurrentYear1;
        int CurrentYear;
        CurrentYear1 = DateTime.Now.Year.ToString();
        CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());
        if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
        {
            CurrentYear = CurrentYear - 1;
        }
        for (int i = 0; i < 11; i++)
        {
            string tt = (CurrentYear1 + "-" + (CurrentYear - 1));
            ddlFinancialYear.Items.Add(tt.ToString());
            CurrentYear = CurrentYear - 1;
            string cy = Convert.ToString(CurrentYear);
            CurrentYear1 = cy;
        }
    }


   
    protected void ddlTicketNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        //string s = ddlTicketNo.SelectedItem.Text;
        //string[] delimiters = new string[] { "-->" };
        //string[] items = s.Split(delimiters, StringSplitOptions.None);
        //string ddlTicketNo.SelectedItem.Text = items[0];
        //string ss1 = items[1];
        //txtEmployeeName.Text = ss1.ToString();
        //txtEmployeeName.Text = items[1];

        DataTable dted = new DataTable();
        dted = objdata.Manual_Data(ddlTicketNo.SelectedItem.Text);
        if (dted.Rows.Count > 0)
        {
            txtExistingCode.Text = dted.Rows[0]["ExistingCode"].ToString();
            txtDepartment.Text = dted.Rows[0]["DeptName"].ToString();
            txtEmployeeName.Text = dted.Rows[0]["FirstName"].ToString();
        }
   }

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {
        //string s = ddlTicketNo.SelectedItem.Text;
        //string[] delimiters = new string[] { "-->" };
        //string[] items = s.Split(delimiters, StringSplitOptions.None);
        //string ddlTicketNo.SelectedItem.Text = items[0];
        //string ss1 = items[1];
       

        string st = "true";
        string st1 = "false";
        string MonthDays_Getting = "30";


         DataTable dtbdelete = new DataTable();
         SSQL = " select * from SpinIncentiveDet where CompCode ='"+SessionCcode+"' and LocCode ='"+SessionLcode+"'and Months='"+ddlMonth.SelectedItem.Text+"' and FinYear='"+ddlFinancialYear.SelectedItem.Text+"'and Wages='"+ddlWages.SelectedItem.Text+"'";
         dtbdelete = objdata.ReturnMultipleValue(SSQL);

         if (dtbdelete.Rows.Count > 0)
         {
             SSQL = "Delete SpinIncentiveDet where CompCode ='" + SessionCcode + "' and LocCode ='" + SessionLcode + "'and Months='" + ddlMonth.SelectedItem.Text + "' and FinYear='" + ddlFinancialYear.SelectedItem.Text + "'and Wages='" + ddlWages.SelectedItem.Text + "'";
         }
         else
         {
             SSQL = "insert into SpinIncentiveDet(CompCode,LocCode,Months,FinYear,Wages)Values('" + SessionCcode + "','" + SessionLcode + "','" + ddlMonth.SelectedItem.Text + "','" + ddlFinancialYear.SelectedItem.Text + "','" + ddlWages.SelectedItem.Text + "')";
             objdata.ReturnMultipleValue(SSQL);
         }

        DataTable dtbCheck = new DataTable();
        SSQL = " select * from SpinIncentiveDetPart where CompCode ='"+SessionCcode+"' and LocCode ='"+SessionLcode+"'and Months='"+ddlMonth.SelectedItem.Text+"' and FinYear='"+ddlFinancialYear.SelectedItem.Text+"' and MachineID = '" + ddlTicketNo.SelectedItem.Text +"'";
        dtbCheck = objdata.ReturnMultipleValue(SSQL);

        if(dtbCheck.Rows.Count < 1)
        {
             if (ddlMonth.SelectedItem.Text == "January" || ddlMonth.SelectedItem.Text == "March" || ddlMonth.SelectedItem.Text == "May" ||
             ddlMonth.SelectedItem.Text == "July" || ddlMonth.SelectedItem.Text == "August" || ddlMonth.SelectedItem.Text == "October" ||
             ddlMonth.SelectedItem.Text == "December")
             {
                 MonthDays_Get = "31";
                 SSQL = "Insert Into SpinIncentiveDetPart(CompCode,LocCode,Months,FinYear,Wages,TokenNo,MachineID,EmpName,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,";
                 SSQL = SSQL + "D11,D12,D13,D14,D15,D16,D17,D18,D19,D20,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31,TotalDays) Values('" + SessionCcode + "',";
                 SSQL = SSQL + "'" + SessionLcode + "','" + ddlMonth.SelectedItem.Text + "','" + ddlFinancialYear.SelectedItem.Text + "','" + ddlWages.SelectedItem.Text + "',";
                 SSQL = SSQL + "'" + ddlTicketNo.SelectedItem.Text + "','" + ddlTicketNo.SelectedItem.Text + "','" + txtEmployeeName.Text + "','" + st + "',";
                 SSQL = SSQL + "'" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "',";
                 SSQL = SSQL + "'" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "',";
                 SSQL = SSQL + "'" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + MonthDays_Get + "')";
                 objdata.ReturnMultipleValue(SSQL);
             }
             else if (ddlMonth.SelectedItem.Text == "April" || ddlMonth.SelectedItem.Text == "June" || ddlMonth.SelectedItem.Text == "September" || ddlMonth.SelectedItem.Text == "November")
             {

                 SSQL = "Insert Into SpinIncentiveDetPart(CompCode,LocCode,Months,FinYear,Wages,TokenNo,MachineID,EmpName,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,";
                 SSQL = SSQL + "D11,D12,D13,D14,D15,D16,D17,D18,D19,D20,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31,TotalDays) Values('" + SessionCcode + "',";
                 SSQL = SSQL + "'" + SessionLcode + "','" + ddlMonth.SelectedItem.Text + "','" + ddlFinancialYear.SelectedItem.Text + "','" + ddlWages.SelectedItem.Text + "',";
                 SSQL = SSQL + "'" + ddlTicketNo.SelectedItem.Text + "','" + ddlTicketNo.SelectedItem.Text + "','" + txtEmployeeName.Text + "','" + st + "',";
                 SSQL = SSQL + "'" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "',";
                 SSQL = SSQL + "'" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "',";
                 SSQL = SSQL + "'" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st1 + "','" + MonthDays_Getting + "')";
                 objdata.ReturnMultipleValue(SSQL);
             }
             else if (ddlMonth.SelectedItem.Text == "February")
             {
                 string MonthDays_GetFeb = "28";
                 SSQL = "Insert Into SpinIncentiveDetPart(CompCode,LocCode,Months,FinYear,Wages,TokenNo,MachineID,EmpName,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,";
                 SSQL = SSQL + "D11,D12,D13,D14,D15,D16,D17,D18,D19,D20,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31,TotalDays) Values('" + SessionCcode + "',";
                 SSQL = SSQL + "'" + SessionLcode + "','" + ddlMonth.SelectedItem.Text + "','" + ddlFinancialYear.SelectedItem.Text + "','" + ddlWages.SelectedItem.Text + "',";
                 SSQL = SSQL + "'" + ddlTicketNo.SelectedItem.Text + "','" + ddlTicketNo.SelectedItem.Text + "','" + txtEmployeeName.Text + "','" + st + "',";
                 SSQL = SSQL + "'" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "',";
                 SSQL = SSQL + "'" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "',";
                 SSQL = SSQL + "'" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st + "','" + st1 + "','" + st1 + "','" + st1 + "','" + MonthDays_GetFeb + "')";
                 objdata.ReturnMultipleValue(SSQL);

             }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' sorry! Machine ID already exist in this finacial Year and Month');", true);
            ErrFlag = true;
        }
        }
     }

