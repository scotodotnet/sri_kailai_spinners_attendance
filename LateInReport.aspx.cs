﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class LateInReport : System.Web.UI.Page
{
    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;



    BALDataAccess objdata = new BALDataAccess();
   
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string SSQL;
    DataTable mEmployeeDS = new DataTable();
    DataTable AutoDataTable = new DataTable();
    DataTable mDataTable = new DataTable();
    DataSet ds = new DataSet();
    DateTime date1 = new DateTime();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "ERP Stores Module :: Spay Module | Late In Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();

            //ModeType = Request.QueryString["ModeType"].ToString();
            //ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            //ddlShiftType = Request.QueryString["ddlShiftType"].ToString();


            date1 = Convert.ToDateTime(Date.ToString());
            DataTable dtIPaddress = new DataTable();
            dtIPaddress = objdata.LateINIPAddress(SessionCcode.ToString(), SessionLcode.ToString());

            if (dtIPaddress.Rows.Count > 0)
            {
                for (int i = 0; i < dtIPaddress.Rows.Count; i++)
                {
                    if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                    {
                        mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                    else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                    {
                        mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                }
            }


            Fill_Latecomers_Details();

            for (int iRow2 = 0; iRow2 < AutoDataTable.Rows.Count; iRow2++)
            {
                string MID = AutoDataTable.Rows[iRow2][9].ToString();

                SSQL = "";
                SSQL = "select Distinct MachineID,MachineID_Encrypt,isnull(DeptName,'') as [DeptName]";
                SSQL = SSQL + ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
                SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
                SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName]";
                SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode + "'";
                //if (SessionUserType == "2")
                //{
                //    SSQL = SSQL + " And EM.IsNonAdmin='1'";
                //}
                SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'And MachineID_Encrypt = '" + MID + "'";

                //if (mvarUserType == "IF User")
                //{
                //    SSQL = SSQL + " And PFNo <> '' And PFNo IS Not Null And PFNo <> '0' And PFNo <> 'o' And PFNo <> 'A'";
                //    SSQL = SSQL + " And PFNo <> 'NULL'";
                //}

                mEmployeeDS = objdata.ReturnMultipleValue(SSQL);


                if (mEmployeeDS.Rows.Count > 0)
                {
                    for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                    {

                        string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();

                        AutoDataTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                        AutoDataTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
                        AutoDataTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                        AutoDataTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                        AutoDataTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                        AutoDataTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                        AutoDataTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                        AutoDataTable.Rows[iRow2][12] = SessionCcode.ToString();
                        AutoDataTable.Rows[iRow2][13] = SessionLcode.ToString();
                        AutoDataTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");
                    }
                }
            }



            ds.Tables.Add(AutoDataTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/latein.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            CrystalReportViewer1.ReportSource = report;

        }
    }

         public void Fill_Latecomers_Details()
    {

        DataColumn auto = new DataColumn();
        auto.AutoIncrement = true;
        auto.AutoIncrementSeed = 1;

       
         AutoDataTable.Columns.Add("SNo");
         AutoDataTable.Columns.Add("Dept");
         AutoDataTable.Columns.Add("Type");
         AutoDataTable.Columns.Add("Shift");
         AutoDataTable.Columns.Add("EmpCode");
         AutoDataTable.Columns.Add("ExCode");
         AutoDataTable.Columns.Add("Name");
         AutoDataTable.Columns.Add("TimeIN");
         AutoDataTable.Columns.Add("");
         AutoDataTable.Columns.Add("MachineID");
         AutoDataTable.Columns.Add("Category");
         AutoDataTable.Columns.Add("SubCategory");
         AutoDataTable.Columns.Add("CompanyName");
         AutoDataTable.Columns.Add("LocationName");
         AutoDataTable.Columns.Add("ShiftDate");


         date1 = Convert.ToDateTime(Date.ToString());

         SSQL = "";
         SSQL = "select ShiftDet,MachineID,TimeIN from";
         SSQL += "(";
         SSQL += " Select 'Shift1' as [ShiftDet],MachineID,Min(TimeIN) as [TimeIN] from LogTime_IN";
         SSQL += " Where Compcode='" + SessionCcode.ToString() + "'";
         SSQL += " And LocCode='" + SessionLcode.ToString() + "'";
         SSQL += " And IPAddress='" + mIpAddress_IN + "'";
         SSQL += " And TimeIN  >'" + date1.ToString("yyyy/MM/dd") + " 09:30'";
         SSQL += " And TimeIN <='" + date1.ToString("yyyy/MM/dd") + " 12:00'";
         SSQL += " Group By MachineID";
         SSQL += " union";
         SSQL += " Select 'Shift2' as [ShiftDet],MachineID,Min(TimeIN) as [TimeIN] from LogTime_IN";
         SSQL += " Where Compcode='" + SessionCcode.ToString() + "'";
         SSQL += " And LocCode='" + SessionLcode.ToString() + "'";
         SSQL += " And IPAddress='" + mIpAddress_IN + "'";
         SSQL += " And TimeIN  >'" + date1.ToString("yyyy/MM/dd") + " 17:30'";
         SSQL += " And TimeIN <='" + date1.ToString("yyyy/MM/dd") + " 19:30'";
         SSQL += " Group By MachineID";
         SSQL += " union";
         SSQL += " Select 'Shift3' as [ShiftDet],MachineID,Min(TimeIN) as [TimeIN] from LogTime_IN";
         SSQL += " Where Compcode='" + SessionCcode.ToString() + "'";
         SSQL += " And LocCode='" + SessionLcode.ToString() + "'";
         SSQL += " And IPAddress='" + mIpAddress_IN + "'";
         SSQL += " And TimeIN >'" + date1.AddDays(1).ToString("yyyy/MM/dd") + " 01:00'";
         SSQL += " And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " 03:30'";
         SSQL += " Group By MachineID) a ";
         SSQL += " Order By ShiftDet,TimeIN";

         mDataTable = objdata.ReturnMultipleValue(SSQL); 

        if(mDataTable.Rows.Count <= 0)
        {
            return;
        }


         //AutoDataTable.Rows = mDataTable.Rows.Count + 2;
          int i = 0;
         for (int iRow = 0; iRow < mDataTable.Rows.Count; iRow++)
         {
             //AutoDataTable.Rows[irow1][0] = iRow + 1;
             string MID = mDataTable.Rows[iRow]["MachineID"].ToString();
             DataTable dtdmach = new DataTable();
             SSQL = "select MachineID from Employee_Mst where MachineID_Encrypt = '" + MID + "' And IsActive='Yes'";
             //if (SessionUserType == "2")
             //{
             //    SSQL = SSQL + " And EM.IsNonAdmin='1'";
             //}

             dtdmach = objdata.ReturnMultipleValue(SSQL);
             if (dtdmach.Rows.Count > 0 || dtdmach.Rows.Count == 0)
             {
                
                 AutoDataTable.NewRow();
                 AutoDataTable.Rows.Add();

                 AutoDataTable.Rows[i]["Shift"] = mDataTable.Rows[iRow]["ShiftDet"].ToString();
                 AutoDataTable.Rows[i]["TimeIN"] = string.Format("{0:hh:mm tt}", mDataTable.Rows[iRow]["TimeIN"]);
                 AutoDataTable.Rows[i]["MachineID"] = mDataTable.Rows[iRow]["MachineID"];

                 i = i + 1;
             }
         }
    }



    }

