﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
using System.Globalization;


public partial class manual_attendance : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    ManualAttendanceClass objManual = new ManualAttendanceClass();
    bool ErrFlag = false;
    string SSQL;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string MachineID;
    string LogTimeIN;
    string LogTimeOUT;
    DateTime Attendate;
    string s;
    string ss;
    string ss1;
    DataTable CheckingAlreadyExist = new DataTable();
   
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Manual Attendance";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                li.Attributes.Add("class", "droplink active open");
                //Dropdown_Company();
                //Dropdown_Location();
                DropDown_TokenNumber();
            }
        }

    }




    //public void Dropdown_Company()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.Dropdown_Company();
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        ddlCompCode.Items.Add(dt.Rows[i]["Cname"].ToString());
    //    }
    //}

    //public void Dropdown_Location()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.dropdown_ParticularLocation(SessionCcode, SessionLcode);
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        ddlLocCode.Items.Add(dt.Rows[i]["LocName"].ToString());
    //    }
    //}


    protected void btnSave_Click(object sender, EventArgs e)
    {
   
        string s = ddlTicketNo.SelectedItem.Text;
        string[] delimiters = new string[] { "-->" };
        string[] items = s.Split(delimiters, StringSplitOptions.None);
        string ss = items[0].Trim();
        string ss1 = items[1].Trim();
        string datetext = TxtAttnDate.Text;
        string attdatenw = String.Format("{0:u}", datetext);
        DateTime date1;
        date1 = Convert.ToDateTime(TxtAttnDate.Text);

        if (ddlTicketNo.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Your TicketNo');", true);
            ErrFlag = true;
        }

        else if (ddlAttnStatus.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Your Attendance Status');", true);
            ErrFlag = true;
        }
   

        if(!ErrFlag)
        {
            DataTable Da_Imp_IN=new DataTable();
            DataTable Da_Imp_OUT=new DataTable();
            DataTable Da_Manu_IN = new DataTable();
            DataTable Da_Manu_OUT = new DataTable();
 
 
            DateTime TimeIN = Convert.ToDateTime(TxtTimeIN.Text);
            DateTime TimeOUT=Convert.ToDateTime(TxtTimeOUT.Text);
            MachineID = UTF8Encryption(ss);

           //Delete Improper Punches
            SSQL = "";
            SSQL = "Delete from LogTime_IN where MachineID='" + MachineID + "'" +
                   " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'" +
                   " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'" +
                   " And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
            Da_Imp_IN = objdata.ReturnMultipleValue(SSQL);
            SSQL = "";
            SSQL = "Delete from LogTime_OUT where MachineID='" + MachineID + "'" +
                   " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'" +
                   " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'" +
                   " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
            Da_Imp_OUT= objdata.ReturnMultipleValue(SSQL);
            //Delete Manual Attendance Punches
            SSQL = "";
            SSQL = "delete from ManAttn_Details where Machine_No='" + ss + "' "+
                    " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'" +
                     " And LogTimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'" +
                   " And LogTimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
            Da_Manu_IN = objdata.ReturnMultipleValue(SSQL);

            SSQL = "";
            SSQL = "Delete from ManAttn_Details where Machine_No='" + MachineID + "'" +
                   " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'" +
                   " And LogTimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'" +
                   " And LogTimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
            Da_Manu_OUT = objdata.ReturnMultipleValue(SSQL);



            
                objManual.Ccode = SessionCcode.ToString();
                objManual.Lcode = SessionLcode.ToString();
                objManual.EmpName = TxtEmpName.Text;
                objManual.ExistingCode = TxtExstCode.Text;
                objManual.TktNo = ss.ToString();
                string attdate = TxtAttnDate.Text;
                objManual.AttendanceDate = String.Format("{0:u}", attdate);
                objManual.TimeIn = TxtTimeIN.Text;
                objManual.AttendanceStatus = ddlAttnStatus.SelectedItem.Text;
                objManual.TimeOut = TxtTimeOUT.Text;
                objManual.LogTimeIN = TxtAttnDate.Text + " " + TxtTimeIN.Text;
                objManual.LogTimeOUT = TxtAttnDate.Text + " " + TxtTimeOUT.Text;
                LogTimeIN = TxtAttnDate.Text + " " + TxtTimeIN.Text;
                LogTimeOUT = TxtAttnDate.Text + " " + TxtTimeOUT.Text;
                objManual.CompensationDate = "Null";
                objManual.CoAgainstDate = "Null";
                objManual.CurrentDate = DateTime.Now.ToString();
                objManual.UserName = SessionAdmin.ToString();
                objdata.ManualAttendanceRegister(objManual);//Insert Manual attendance 
                objManual.EncryptID = ddlTicketNo.SelectedItem.Text; ;
                MachineID = UTF8Encryption(ss.ToString());
                MachineIDFunction();
                clearfunction();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Save Successfully');", true);
                Response.Redirect("ManualAttendance_Add.aspx");

        }
          
        
  }

    public void MachineIDFunction()
    {
        DataTable dtIPaddress = new DataTable();
        SSQL = "Select * from IPAddress_Mst where CompCode = '" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'";
        dtIPaddress = objdata.ReturnMultipleValue(SSQL);

        if (dtIPaddress.Rows.Count > 0)
        {
            for (int i = 0; i < dtIPaddress.Rows.Count; i++)
            {
                if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                {
                    mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
                else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                {
                    mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
            }
        }

        DateTime dt1 = DateTime.ParseExact(TxtAttnDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        string login = dt1.ToString("MM/dd/yyyy");
        string Logout = dt1.ToString("dd/MM/yyyy");
       
        string[] Sp_Out ;
       string logtimeout ="";
       string Datestr1 = "";

        string OUT=TxtTimeOUT.Text;
        Sp_Out = OUT.Split(':');

        if (Convert.ToDecimal(Sp_Out[0]) <= 12)
        {

            Datestr1 = Convert.ToDateTime(Logout).AddDays(1).ToShortDateString();

            DateTime OUT_DATE = DateTime.ParseExact(Datestr1, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            Datestr1 = OUT_DATE.ToString("MM/dd/yyyy");
            logtimeout = Datestr1 + " " + TxtTimeOUT.Text;

        }
        else
        {
            logtimeout = login + " " + TxtTimeOUT.Text;
        }

      
              
        string logtimein = login + " " + TxtTimeIN.Text;
        
            
        DataTable dtLogTimeIN = new DataTable();
        SSQL = "insert into LogTime_IN(CompCode,LocCode,IPAddress,MachineID,TimeIN) values('" + SessionCcode + "','" + SessionLcode + "','" + mIpAddress_IN + "','" + MachineID + "','"+ logtimein +"')";
        dtLogTimeIN = objdata.ReturnMultipleValue(SSQL);

        DataTable dtLogTimeOUT = new DataTable();
        SSQL = "insert into LogTime_OUT(CompCode,LocCode,IPAddress,MachineID,TimeOUT) values('" + SessionCcode + "','" + SessionLcode + "','" + mIpAddress_IN + "','" + MachineID + "','" + logtimeout + "')";
        dtLogTimeOUT = objdata.ReturnMultipleValue(SSQL);
    }

    public void logINandOT()
    {

    }

    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    public void DeleteManualAttendance()
    {



    }


    public void DropDown_TokenNumber()
    {
        DataTable dt = new DataTable();
        dt = objdata.dropdown_TokenNumber(SessionCcode, SessionLcode);
        ddlTicketNo.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["EmpName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlTicketNo.Items.Add(dt.Rows[i]["EmpName"].ToString());
        }
    }




    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtCompensationDate.Visible = false;
        txtCOagainstDate.Visible = false;
        lblCOagainstDate.Visible = false;
        lblCompensationDate.Visible = false;

        txtCompensationDate.Text = "";
        txtCOagainstDate.Text = "";
        lblCOagainstDate.Text = "";
        lblCompensationDate.Text = "";
        clearfunction();
    }

    public void clearfunction()
    {
        TxtAttnDate.Text = "";
        TxtEmpName.Text = "";
        TxtExstCode.Text = "";
        ddlTicketNo.SelectedIndex = 0;
        TxtTimeIN.Text = "";
        TxtTimeOUT.Text = "";
        ddlAttnStatus.SelectedIndex = 0;
        txtCompensationDate.Text = "";
        txtCOagainstDate.Text = "";
    }



    protected void ddlTicketNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string s = ddlTicketNo.SelectedItem.Text;
        string[] delimiters = new string[] { "-->" };
        string[] items = s.Split(delimiters, StringSplitOptions.None);
        string ss = items[0];
        string ss1 = items[1];
        TxtEmpName.Text = ss1.ToString();
        TxtEmpName.Text = items[1];


        DataTable dted = new DataTable();
        dted = objdata.Manual_Data(ss.ToString());
        if (dted.Rows.Count > 0)
        {
            TxtExstCode.Text = dted.Rows[0]["ExistingCode"].ToString();
            //txtMachineNo.Text = dted.Rows[0]["MachineID"].ToString();
            //TxtEmpName.Text = dted.Rows[0]["FirstName"].ToString();


        }

    }
    protected void ddlAttnStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlAttnStatus.SelectedItem.Text == "Compensation OFF")
        {
            txtCompensationDate.Visible = true;
            txtCOagainstDate.Visible = true;
            lblCOagainstDate.Visible = true;
            lblCompensationDate.Visible = true;
            txtCompensationDate.Text = TxtAttnDate.Text;

            lblCOagainstDate.Text = "CO/Against Date";
            lblCompensationDate.Text = "Compensation Date";

        }
        else
        {
            txtCompensationDate.Visible = false;
            txtCOagainstDate.Visible = false;
            lblCOagainstDate.Visible = false;
            lblCompensationDate.Visible = false;


        }
    }
    protected void txtCOagainstDate_TextChanged(object sender, EventArgs e)
    {
        DateTime date1 = Convert.ToDateTime(txtCompensationDate.Text);
        DateTime date2 = Convert.ToDateTime(txtCOagainstDate.Text);
        if (date1 > date2)
        {
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Compensation Date Greater Than Agains Date');", true);
            ErrFlag = true;
        }
    }
}


    

