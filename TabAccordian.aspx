﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TabAccordian.aspx.cs" Inherits="TabAccordian" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tableCustomer').dataTable();               
            }
        }); 
    };
    </script>
      
             
     <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableCustomer').dataTable();
        });
    </script>





<div class="panel panel-white">
                                        <body class="page-header-fixed compact-menu page-horizontal-bar">
        <div class="overlay"></div>
      
       
        <form class="search-form" action="#" method="GET">
            
        </form><!-- Search Form -->
        <main class="page-content content-wrap">
           
                       
                              
            
            <div class="page-inner">
                
                  <div id="main-wrapper" class="container">
                        <div class="row">
                            <div class="col-md-12">
                                 <div class="panel panel-white">
                                 <div class="panel panel-primary">
                                    <div class="panel-heading clearfix">
                                        <h3 class="panel-title">Left tabs</h3>
                                    </div>
                                    </div>
                                    <div class="panel-body">
                                    
                                    <%--<div class="form-group row">
                                    
                                   
                                                                <div class="col-sm-3">  
                                                                     <asp:Label ID="Label6" runat="server" Text="EmpNumber" Font-Bold="True" ></asp:Label>
                                                                 </div>
                                                                     <div class="col-sm-3">
                                                                        <asp:DropDownList ID="ddlLocationCode" runat="server" class="form-control"></asp:DropDownList> 
                                                         
                                                                    </div>
                                                                    
                                                                        <div class="col-sm-3">                                                           
                                                                         <asp:Label ID="Label21" runat="server" Text="ExCode" Font-Bold="True" ></asp:Label>
                                                                      </div>
                                                                     <div class="col-sm-3">
                                                                        <asp:DropDownList ID="DropDownList1" runat="server" class="form-control"></asp:DropDownList> 
                                                         
                                                                    </div>
                                                                   
                                                                   
                                                                    <div class="col-sm-2">
                                                                     <asp:Button ID="btndocumentsave" class="btn btn-success" runat="server" 
                                                                        Text="SEARCH" />
                                                                    </div>
                                                                 
                                                                 
                                    
                                    </div>--%>
                                    
                                    
                                    
                                  
                                                                  
                                                                  
                                                                     
                                                                      
                                                                    
                                    
                                    
                                    
                                    
                                    
                                        <div class="tabs-left" role="tabpanel">
                                        
                                        
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#tab9" role="tab" data-toggle="tab">Employee Details</a></li>
                                                <li role="presentation"><a href="#tab10" role="tab" data-toggle="tab">User Profile</a></li>
                                                <li role="presentation"><a href="#tab11" role="tab" data-toggle="tab">Tab 3</a></li>
                                                <li role="presentation"><a href="#tab12" role="tab" data-toggle="tab">Tab 4</a></li>
                                                <li role="presentation">
                                                   <div class="panel panel-white" style="height: 100%;">
                                                     <div class="panel panel-primary">
                                                        <div class="panel-heading clearfix">
                                                           <h3 class="panel-title">Employee Photo</h3>
                                                        </div>
                                                     </div>
                                                     <div class="panel-body">
                                                    <div class="profile-image-container">
                                                   <img src="assets/images/avatar4.png" alt="">
                                                  </div>
                                                    <h3 class="text-center">Employee Name</h3>
                                                   <p class="text-center">Ticket Number</p>
                                       
                                                   <hr>
                                                   <button class="btn btn-success btn-block"><i class="fa fa-plus m-r-xs"></i>Upload Photo</button>
                                                </div>	
                                                       
                                                </div>
                                               </li>
                                               
                                            </ul>
                                            
                                             
                                            
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active fade in" id="tab9">
                                                   
                                                
                                                    <div class="row m-b-lg">
                                                    
                                                                              
                                                            <div class="col-md-6">
                                                                                                                           
                                                               <div class="row">
                                                                 <div class="form-group col-md-10">
                                                                     <div class="col-md-4">
                                                                        <asp:DropDownList ID="ddlEmpNo" runat="server" class="form-control"></asp:DropDownList> 
                                                                    </div>
                                                                    
                                                                     <div class="col-md-4">
                                                                        <asp:DropDownList ID="ddlExCode" runat="server" class="form-control"></asp:DropDownList> 
                                                         
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                     <asp:Button ID="btndocumentsave" class="btn btn-success" runat="server" 
                                                                        Text="SEARCH" />
                                                                    </div>
                                                                 
                                                                 
                                    
                                                                   </div>
                                                                  
                                                                  
                                                                  
                                                                  
                                                                  
                                                                  
                                                               </div>   
                                                                  
                                                                  
                                                               
                                                                  
                                                                  
                                                                  
                                                       
                                                               <div class="row">
                                                                 <div class="form-group col-md-10">
                                                                      <div class="col-md-3">
                                                                       <asp:Label ID="Label13" runat="server" Text="EmpNo" Font-Bold="True" ></asp:Label>
                                                                        
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                         <asp:Label ID="Label1" runat="server" ></asp:Label>
                                                                      </div>
                                                                      
                                                                      
                                                                     
                                                                  </div>
                                                                  </div>
                                                                     
                                                                   <div class="row">
                                                                       <div class="form-group col-md-10"> 
                                                                       <div class="col-md-3">
                                                                       <asp:Label ID="Label14" runat="server" Text="ExCode" Font-Bold="True" ></asp:Label>
                                                                         
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                          <asp:Label ID="Label3" runat="server" Text="123dfgfhfgfj" ></asp:Label>
                                                                      </div>
                                                                      </div>
                                                                 </div>
                                                                
                                                                
                                                                <div class="row">
                                                                    <div class="form-group  col-md-10">
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label15" runat="server" Text="EmpName" Font-Bold="True" ></asp:Label>
                                                                      
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                         <asp:Label ID="Label2" runat="server" Text="Nithyakumari" for="exampleInputName"></asp:Label>
                                                                      </div>
                                                                    </div>
                                                                 </div>
                                                                      
                                                                 <div class="row">
                                                                    <div class="form-group  col-md-10">    
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label16" runat="server" Text="Gender" Font-Bold="True" ></asp:Label>
                                                                       
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                         <asp:Label ID="Label4" runat="server" Text="Nithyakumari" for="exampleInputName"></asp:Label>
                                                                      </div>
                                                                                    
                                                                </div>
                                                                </div>
                                                                
                                                                <div class="row">
                                                                <div class="form-group  col-md-10">
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label17" runat="server" Text="DOB" Font-Bold="True" ></asp:Label>
                                                                      
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                         <asp:Label ID="Label5" runat="server" Text="Nithyakumari" for="exampleInputName"></asp:Label>
                                                                      </div>
                                                                   </div>
                                                                </div>
                                                                   
                                                                   <div class="row">
                                                                      <div class="form-group  col-md-10"> 
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label18" runat="server" Text="Age" Font-Bold="True" ></asp:Label>
                                                                        
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                       <asp:Label ID="Label19" runat="server" Text="123"></asp:Label>
                                                                                                                                               </div>
                                                                                                                                
                                                                </div>
                                                                </div>
                                                                
                                                                <div class="row">
                                                                <div class="form-group  col-md-10">
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label20" runat="server" Text="DOJ" Font-Bold="True" ></asp:Label>
                                                                        
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                      
                                                                         <asp:Label ID="Label7" runat="server" Text="Nithyakumari" for="exampleInputName"></asp:Label>
                                                                      </div>
                                                                      
                                                                      </div>
                                                                      </div>
                                                                  <div class="row">
                                                                     <div class="form-group  col-md-10"> 
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label22" runat="server" Text="Category" Font-Bold="True" ></asp:Label>
                                                                       
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                      
                                                                         <asp:Label ID="Label8" runat="server" Text="Nithyakumari" for="exampleInputName"></asp:Label>
                                                                      </div>
                                                                                                                                
                                                                   </div>
                                                                   </div>
                                                                
                                                                <div class="row">
                                                                
                                                                <div class="form-group  col-md-10">
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label24" runat="server" Text="Department" Font-Bold="True" ></asp:Label>
                                                                       
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                       <asp:Label ID="Label9" runat="server" Text="Nithyakumari" for="exampleInputName"></asp:Label>
                                                                      </div>
                                                                      </div>
                                                                      </div>
                                                                      
                                                                   <div class="row">
                                                                     <div class="form-group  col-md-10">
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label26" runat="server" Text="Shift Type" Font-Bold="True" ></asp:Label>
                                                                      
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                     
                                                                         <asp:Label ID="Label10" runat="server" Text="Nithyakumari" for="exampleInputName"></asp:Label>
                                                                      </div>
                                                                                                                                
                                                                </div>
                                                                </div>
                                                                
                                                                <div class="row">
                                                                
                                                                <div class="form-group  col-md-10">
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label28" runat="server" Text="IsActive" Font-Bold="True" ></asp:Label>
                                                                       
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                       
                                                                         <asp:Label ID="Label11" runat="server" Text="Nithyakumari" for="exampleInputName"></asp:Label>
                                                                      </div>
                                                                     </div>
                                                                     </div>
                                                                     
                                                                   <div class="row">
                                                                
                                                                    <div class="form-group  col-md-10"> 
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label30" runat="server" Text="Blood Group" Font-Bold="True" ></asp:Label>
                                                                       
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                      
                                                                         <asp:Label ID="Label12" runat="server" Text="Nithyakumari" for="exampleInputName"></asp:Label>
                                                                      </div>
                                                                                                                                
                                                                </div>
                                                                
                                                                </div>
                                                                
                                                                
                                                                                     
                                                            </div>
                                                        </div>
                                                
                                                   
                                                       
                                                  </div>
                                                  <%--</div>--%>
                                                <div role="tabpanel" class="tab-pane fade" id="tab10">
                                                    <div class="row m-b-lg">
                                                    
                                                                              
                                                            <div class="col-md-6">
                                                        
                                                       
                                                               <div class="row">
                                                                 <div class="form-group col-md-10">
                                                                      <div class="col-md-3">
                                                                       <asp:Label ID="Label6" runat="server" Text="EmpNo" Font-Bold="True" ></asp:Label>
                                                                        
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                         <asp:Label ID="Label21" runat="server" Text="123"></asp:Label>
                                                                      </div>
                                                                      
                                                                      
                                                                     
                                                                  </div>
                                                                  </div>
                                                                     
                                                                   <div class="row">
                                                                       <div class="form-group col-md-10"> 
                                                                       <div class="col-md-3">
                                                                       <asp:Label ID="Label23" runat="server" Text="ExCode" Font-Bold="True" ></asp:Label>
                                                                         
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                          <asp:Label ID="Label25" runat="server" Text="123dfgfhfgfj" ></asp:Label>
                                                                      </div>
                                                                      </div>
                                                                 </div>
                                                                
                                                                
                                                                <div class="row">
                                                                    <div class="form-group  col-md-10">
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label27" runat="server" Text="EmpName" Font-Bold="True" ></asp:Label>
                                                                      
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                         <asp:Label ID="Label29" runat="server" Text="Nithyakumari" for="exampleInputName"></asp:Label>
                                                                      </div>
                                                                    </div>
                                                                 </div>
                                                                      
                                                                 <div class="row">
                                                                    <div class="form-group  col-md-10">    
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label31" runat="server" Text="Basic Salary" Font-Bold="True" ></asp:Label>
                                                                       
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                         <asp:Label ID="Label32" runat="server" Text="Nithyakumari" for="exampleInputName"></asp:Label>
                                                                      </div>
                                                                                    
                                                                </div>
                                                                </div>
                                                                
                                                                <div class="row">
                                                                <div class="form-group  col-md-10">
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label33" runat="server" Text="Wages Type" Font-Bold="True" ></asp:Label>
                                                                      
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                         <asp:Label ID="Label34" runat="server" Text="Nithyakumari" for="exampleInputName"></asp:Label>
                                                                      </div>
                                                                   </div>
                                                                </div>
                                                                   
                                                                   <div class="row">
                                                                      <div class="form-group  col-md-10"> 
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label35" runat="server" Text="PF No" Font-Bold="True" ></asp:Label>
                                                                        
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                       <asp:Label ID="Label36" runat="server" Text="123"></asp:Label>
                                                                                                                                               </div>
                                                                                                                                
                                                                </div>
                                                                </div>
                                                                
                                                                <div class="row">
                                                                <div class="form-group  col-md-10">
                                                                      <div class="col-md-3">
                                                                      <asp:Label ID="Label37" runat="server" Text="ESI No" Font-Bold="True" ></asp:Label>
                                                                        
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                      
                                                                         <asp:Label ID="Label38" runat="server" Text="Nithyakumari" for="exampleInputName"></asp:Label>
                                                                      </div>
                                                                      
                                                                      </div>
                                                                      </div>
                                                                  
                                                                
                                                               
                                                                
                                                                        </div>                                                       
                                                                                     
                                                            </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab11">
                                                   
                                                   <div class="col-md-6">
                                                                                                                           
                                                               <div class="row">
                                                                 <div class="form-group col-md-10">
                                                                     <div class="col-md-4">
                                                                        <asp:DropDownList ID="DropDownList1" runat="server" class="form-control"></asp:DropDownList> 
                                                                    </div>
                                                                    
                                                                     <div class="col-md-4">
                                                                        <asp:DropDownList ID="DropDownList2" runat="server" class="form-control"></asp:DropDownList> 
                                                         
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                     <asp:Button ID="Button1" class="btn btn-success" runat="server" 
                                                                        Text="SEARCH" />
                                                                    </div>
                                                                 </div>
                                                   </div>
                                                   
                                                  
                                                   
                                                   <div class="col-md-6">
                                                  <div class="table-responsive">
    
                              <asp:Repeater ID="rptrCustomer" runat="server">
            <HeaderTemplate>
                
                  <table id="tableCustomer" class="display table" style="width: 100%;">
                    <thead>
                        <tr>
                                                   <%-- <th>SNo</th>--%>
                                                    <th>Company Name</th>
                                                    <th>Location Name</th>
                                                    <th>User Name</th>  
                                                  <%--  <th>Edit</th>--%>
                                                    <th>Delete</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    
                   <%-- <td>
                        <%# DataBinder.Eval(Container.DataItem, "SNo")%>
                    </td>--%>
                    
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "CompCode")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "LocCode")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "UserName")%>
                    </td>
                   <%-- <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("SNo") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>--%>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("SNo") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
                                                   
                                                   </div>
                                                   
                                                   
                                                    </div>
                                                   
                                                
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab12">
                                                    <p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                
                        </div>
                    </div><!-- Main Wrapper -->
               
            </div><!-- Page Inner -->
        </main><!-- Page Content -->
       
        <div class="cd-overlay"></div>
        
        
        
                                				
                              </div>
                         
        
        
        
	
</asp:Content>

