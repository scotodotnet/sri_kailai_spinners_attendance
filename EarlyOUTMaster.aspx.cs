﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;


public partial class EarlyOUTMaster : System.Web.UI.Page
{

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    string SSQL = "";
    int ss;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
         
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | EarlyOut Master";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");

                Drop_Shift();
                CreateUserDisplay();
            }
        }
    }

    
        //DataTable dt = new DataTable();
        //dt = objdata.Dropdown_Department();
        //ddlDepartment.DataSource = dt;
        //DataRow dr = dt.NewRow();
        //dr["DeptCode"] = 0;
        //dr["DeptName"] = "- select -";
        //dt.Rows.InsertAt(dr, 0);
        //ddlDepartment.DataTextField = "DeptName";
        //ddlDepartment.DataValueField = "DeptCode";
        //ddlDepartment.DataBind();





    public void Drop_Shift()
    {
        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = "select Sno,ShiftDesc from Shift_Mst Where Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
        dt = objdata.ReturnMultipleValue(SSQL);
        ddlshift.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["Sno"] = 0;
        dr["ShiftDesc"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        ddlshift.DataTextField = "ShiftDesc";
        ddlshift.DataValueField = "Sno";
        ddlshift.DataBind();

    }




    protected void btnSave_Click(object sender, EventArgs e)
    {

        bool ErrFlag = false;
        if (btnSave.Text == "Update")
        {
            DataTable dtdLMst = new DataTable();
            SSQL = "update MstEarlyOut set StartTime='" + txtStartTime.Text + "',EndTime='" + txtEndTime.Text + "' where ShiftDesc='" + ddlshift.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dtdLMst = objdata.ReturnMultipleValue(SSQL);
            CreateUserDisplay();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Update EarlyOUT Time Successfully');", true);
            ErrFlag = true;
            btnSave.Text = "Save";
            clear();
        }
        else
        {
            DataTable DTDShift = new DataTable();
            SSQL = "select *from MstEarlyOut where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and ShiftDesc='" + ddlshift.SelectedItem.Text + "'";
            DTDShift = objdata.ReturnMultipleValue(SSQL);
            if (DTDShift.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('EarlyOUT Time Already Exists');", true);
            }
            else
            {
                SSQL = "insert into MstEarlyOut(CompCode,LocCode,ShiftDesc,StartTime,EndTime)values('" + SessionCcode + "','" + SessionLcode + "'";
                SSQL = SSQL + ",'" + ddlshift.SelectedItem.Text + "','" + txtStartTime.Text + "','" + txtEndTime.Text + "')";
                objdata.ReturnMultipleValue(SSQL);
                CreateUserDisplay();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('EarlyOUT Time Save Successfully');", true);
               
            }
            ErrFlag = true;
            clear();
        }


    }
    protected void BtnClear_Click(object sender, EventArgs e)
    {
        clear();
    }
    public void clear()
    {
        ddlshift.SelectedIndex = 0;
        txtStartTime.Text = "";
        txtEndTime.Text = "";
       
    }

    public void CreateUserDisplay()
    {
        DataTable dtdDisplay = new DataTable();
        SSQL = "select*from MstEarlyOut";
        dtdDisplay = objdata.ReturnMultipleValue(SSQL);
        rptrCustomer.DataSource = dtdDisplay;
        rptrCustomer.DataBind();
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(id);
                break;
            case ("Edit"):
                EditRepeaterData(id);
                break;
        }
    }

    private void EditRepeaterData(string id)
    {
        //ss = Convert.ToInt32(id);

        DataTable Dtd = new DataTable();
        DataTable DT = new DataTable();
        SSQL = "select*from MstEarlyOut where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and Sno='" + id + "'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        if (Dtd.Rows.Count > 0)
        {
            txtStartTime.Text = Dtd.Rows[0]["StartTime"].ToString();
            txtEndTime.Text = Dtd.Rows[0]["EndTime"].ToString();

            string shift=Dtd.Rows[0]["ShiftDesc"].ToString();

            SSQL = "select *from Shift_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and ShiftDesc='" + shift + "'";
            DT = objdata.ReturnMultipleValue(SSQL);
            if (DT.Rows.Count > 0)
            {
                string ShiftSno = DT.Rows[0]["Sno"].ToString();
                if (ShiftSno.ToString() != "")
                {
                    ddlshift.SelectedValue = ShiftSno;
                }

           }

            CreateUserDisplay();
            btnSave.Text = "Update";
           
        }
    }

    private void DeleteRepeaterData(string id)
    {
        DataTable Dtd = new DataTable();
        SSQL = "Delete MstEarlyOut where Sno='" + id + "'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        CreateUserDisplay();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
    }




}
