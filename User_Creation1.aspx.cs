﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;

public partial class UserCreation : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    UserCreationClass objUsercreation = new UserCreationClass();
    DataTable dtdDisplay = new DataTable();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "ERP Stores Module :: Spay Module | User Creation";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");
                

                Dropdown_Company();
                Dropdown_Location();

                CreateUserDisplay();
            }
        }
    }

    public void Dropdown_Company()
    {
        DataTable dt = new DataTable();
        dt = objdata.Dropdown_Company();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlCompany.Items.Add(dt.Rows[i]["Cname"].ToString());
        }
    }

    public void Dropdown_Location()
    {
        DataTable dt = new DataTable();
        dt = objdata.dropdown_ParticularLocation(SessionCcode, SessionLcode);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlLocation.Items.Add(dt.Rows[i]["LocName"].ToString());
        }
    }

    public void CreateUserDisplay()
    {
        //DataTable dtdDisplay = new DataTable();
        //dtdDisplay = objdata.UserCreationDisplay(SessionCcode, SessionLcode);
        //if (dtdDisplay.Rows.Count > 0)
        //{
        //    rptrCustomer.DataSource = dtdDisplay;
        //    rptrCustomer.DataBind();
        //}

    }

    public DataTable GetData()
    {
        DataTable dt = new DataTable();
        dt = objdata.UserCreationDisplay(SessionCcode,SessionLcode);
        return dt;
    } 

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlUserType.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select UserType');", true);
            ErrFlag = true;
        }
        else
        {
            DataTable dtd = new DataTable();
            //dtd = objdata.CheckUser_Login(SessionCcode,SessionLcode,txtUserName.Text);
            if (dtd.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Already Exist');", true);
                ErrFlag = true;

            }
            else
            {
                    objUsercreation.Ccode = SessionCcode.ToString();
                    objUsercreation.Lcode = SessionLcode.ToString();
                    objUsercreation.UserCode = ddlUserType.SelectedItem.Text;
                    objUsercreation.UserName = txtUserName.Text;
                    objUsercreation.Password = s_hex_md5(txtPassword.Text.Trim());

                    string pwd = s_hex_md5(txtPassword.Text);
                    objdata.UserCreationRegistration(objUsercreation);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Save sucessFully');", true);
                    CreateUserDisplay();
                    
                    
            }
        }
    }


    public static String s_hex_md5(String originalPassword)
    {
        UTF8Encoding encoder = new UTF8Encoding();
        MD5 md5 = new MD5CryptoServiceProvider();

        Byte[] hashedbytes = md5.ComputeHash(encoder.GetBytes(originalPassword));
        return BitConverter.ToString(hashedbytes).Replace("-", "").ToLower();
    }

}
  
