﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Net.Mail;

public partial class BelowFourHours : System.Web.UI.Page
{
    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;


    BALDataAccess objdata = new BALDataAccess();
  
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();

    DateTime ShiftdateStartIN;
    DateTime ShiftdateEndIN;
    DateTime InTime_Check;
    DateTime InToTime_Check;
    DateTime EmpdateIN;

    DataTable mEmployeeDS = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable DS_InTime = new DataTable();
    DataTable DS_Time = new DataTable();
    string Final_InTime;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string Time_IN_Str;
    string Time_OUT_Str;
    string Date_Value_Str;
    string Shift_Start_Time;
    string Shift_End_Time;
    string Final_Shift;
    string Total_Time_get;
    string[] Time_Minus_Value_Check;
    DataSet ds = new DataSet();
    int time_Check_dbl;
    DataTable mdatatable = new DataTable();
    DataTable LocalDT = new DataTable();
    DataTable Shift_DS = new DataTable();
    TimeSpan InTime_TimeSpan;
    string From_Time_Str = "";
    string To_Time_Str = "";
    //string ddlShiftType;
    Boolean Shift_Check_blb = false;
    string SSQL;
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }




    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Below Four Hours";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            DataSet ds = new DataSet();
            DataTable AutoDTable = new DataTable();
            DataColumn auto = new DataColumn();
            auto.AutoIncrement = true;
            auto.AutoIncrementSeed = 1;

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();



            //ModeType = Request.QueryString["ModeType"].ToString();
            ddlShiftType = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();

            //ddlShiftType = Request.QueryString["ddlShiftType"].ToString();

            //string Date2 = Request.QueryString["Date2"].ToString();
            //string SessionCcode = Session["Ccode"].ToString();
            //string SessionLcode = Session["Lcode"].ToString();

            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("Dept");
            AutoDTable.Columns.Add("Type");
            AutoDTable.Columns.Add("Shift");
            AutoDTable.Columns.Add("EmpCode");

            AutoDTable.Columns.Add("ExCode");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("TimeIN");
            AutoDTable.Columns.Add("TimeOUT");
            AutoDTable.Columns.Add("MachineID");

            AutoDTable.Columns.Add("Category");
            AutoDTable.Columns.Add("SubCategory");
            AutoDTable.Columns.Add("TotalMIN");
            AutoDTable.Columns.Add("GrandTOT");
            AutoDTable.Columns.Add("CompanyName");

            AutoDTable.Columns.Add("LocationName");
            AutoDTable.Columns.Add("ShiftDate");
            AutoDTable.Columns.Add("MachineIDNew");


            //if (mReportName.Text == "DAY ATTENDANCE - DAY WISE :: BELOW 4 HOURS")
            //{
            DataTable dtIPaddress = new DataTable();
            dtIPaddress = objdata.BelowFourHours(SessionCcode.ToString(), SessionLcode.ToString());

            if (dtIPaddress.Rows.Count > 0)
            {
                for (int i = 0; i < dtIPaddress.Rows.Count; i++)
                {
                    if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                    {
                        mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                    else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                    {
                        mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                }
            }

            SSQL = "";
            SSQL = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
            SSQL = SSQL + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
            SSQL = SSQL + " from Shift_Mst Where CompCode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
            if (ddlShiftType != "ALL")
            {
                SSQL = SSQL + " And shiftDesc='" + ddlShiftType + "'";

            }
            //else
            //{
            //    SSQL = SSQL + " And ShiftDesc like '" + ddlShiftType + "%'";

            //}   
            SSQL = SSQL + " Order By shiftDesc";
            LocalDT = objdata.ReturnMultipleValue(SSQL);

            string ShiftType;

            if (LocalDT.Rows.Count > 0)
            {
                string ng = string.Format(Date, "dd-MM-yyyy");
                DateTime date1 = Convert.ToDateTime(ng);
                DateTime date2 = date1.AddDays(1);
                string Start_IN = "";
                string End_In;

                string End_In_Days = "";
                string Start_In_Days = "";
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();


                int mStartINRow = 0;
                int mStartOUTRow = 0;

                for (int iTabRow = 0; iTabRow < LocalDT.Rows.Count; iTabRow++)
                {
                    if (AutoDTable.Rows.Count <= 1)
                    {
                        mStartOUTRow = 0;
                    }
                    else
                    {
                        mStartOUTRow = AutoDTable.Rows.Count - 1;
                    }

                    if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "GENERAL")
                    {
                        ShiftType = "GENERAL";
                    }
                    else
                    {
                        ShiftType = "SHIFT";
                    }


                    string sIndays_str = LocalDT.Rows[iTabRow]["StartIN_Days"].ToString();
                    double sINdays = Convert.ToDouble(sIndays_str);
                    string eIndays_str = LocalDT.Rows[iTabRow]["EndIN_Days"].ToString();
                    double eINdays = Convert.ToDouble(eIndays_str);
                    string ss = "";

                    if (ShiftType == "GENERAL")
                    {
                        ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                        ss = ss + " Where LT.Compcode='" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                        ss = ss + "And LT.TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                        ss = ss + "And LT.TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                        ss = ss + "AND em.ShiftType='" + ddlShiftType + "' And EM.Compcode='" + SessionCcode.ToString() + "'";
                        //if (SessionUserType == "2")
                        //{
                        //    ss = ss + " And EM.IsNonAdmin='1'";
                        //}
                        ss = ss + "And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsActive='yes' Group By LT.MachineID Order By Min(LT.TimeIN)";
                    }


                    else if (ShiftType == "SHIFT")
                    {
                        ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                        ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                        ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                        ss = ss + " And EM.IsActive='yes' And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["StartIN"].ToString() + "' ";
                        ss = ss + " And LT.TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["EndIN"].ToString() + "'";
                        //if (SessionUserType == "2")
                        //{
                        //    ss = ss + " And EM.IsNonAdmin='1'";
                        //}
                        ss = ss + " Group By LT.MachineID Order By Min(LT.TimeIN)";
                    }
                    else
                    {
                        ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                        ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                        ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                        ss = ss + " And EM.IsActive='yes' And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["StartIN"].ToString() + "' ";
                        ss = ss + " And LT.TimeIN <='" + date2.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["EndIN"].ToString() + "' ";
                        ss = ss + " AND EM.ShiftType='" + ddlShiftType + "' ";
                        //if (SessionUserType == "2")
                        //{
                        //    ss = ss + " And EM.IsNonAdmin='1'";
                        //}
                        ss = ss + " Group By LT.MachineID Order By Min(LT.TimeIN)";
                    }


                    mdatatable = objdata.ReturnMultipleValue(ss);

                    if (mdatatable.Rows.Count > 0)
                    {
                        string MachineID;

                        for (int iRow = 0; iRow < mdatatable.Rows.Count - 1; iRow++)
                        {
                            Boolean chkduplicate = false;
                            chkduplicate = false;

                            for (int ia = 0; ia < AutoDTable.Rows.Count - 1; ia++)
                            {
                                string id = mdatatable.Rows[iRow]["MachineID"].ToString();

                                if (id == AutoDTable.Rows[ia][17].ToString())
                                {
                                    chkduplicate = true;
                                }
                            }
                            if (chkduplicate == false)
                            {
                                AutoDTable.NewRow();
                                AutoDTable.Rows.Add();

                                // AutoDTable.Rows.Add();

                                // AutoDTable.Rows[mStartINRow][0] = iRow + 1;
                                AutoDTable.Rows[mStartINRow][3] = LocalDT.Rows[iTabRow]["ShiftDesc"].ToString();
                                if (ShiftType == "SHIFT")
                                {
                                    string str = mdatatable.Rows[iRow][1].ToString();
                                    // string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1])
                                    AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mdatatable.Rows[iRow]["TimeIN"]);
                                }
                                else
                                {
                                    AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mdatatable.Rows[iRow]["TimeIN"]);
                                }


                                MachineID = mdatatable.Rows[iRow]["MachineID"].ToString();

                                ss = UTF8Decryption(MachineID.ToString());

                                AutoDTable.Rows[mStartINRow][9] = ss.ToString();

                                AutoDTable.Rows[mStartINRow][17] = MachineID.ToString();
                                mStartINRow += 1;
                            }
                        }
                    }

                    DataTable nnDatatable = new DataTable();

                    SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT  Where Compcode='" + SessionCcode.ToString() + "'";
                    SSQL = SSQL + " and LocCode = ' " + SessionLcode.ToString() + "'";
                    if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "SHIFT")
                    {
                        SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                        SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                    }
                    else
                    {
                        SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                        SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                    }
                    SSQL = SSQL + " Group By MachineID";
                    SSQL = SSQL + " Order By Max(TimeOUT)";

                    mdatatable = objdata.ReturnMultipleValue(SSQL);


                    string InMachine_IP;
                    DataTable mLocalDS_out = new DataTable();

                    for (int iRow2 = mStartOUTRow; iRow2 < AutoDTable.Rows.Count; iRow2++)
                    {
                        string mach = AutoDTable.Rows[iRow2][17].ToString();

                        InMachine_IP = mach.ToString();

                        SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "SHIFT1")
                        {
                            SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                            SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Asc";
                        }
                        else
                        {
                            SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                            SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Asc";
                        }
                        mLocalDS_out = objdata.ReturnMultipleValue(SSQL);

                        if (mLocalDS_out.Rows.Count <= 0)
                        {

                        }
                        else
                        {
                            if (AutoDTable.Rows[iRow2][17].ToString() == mLocalDS_out.Rows[0][0].ToString())
                            {
                                AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1]);
                            }

                        }
                        // Above coddings are correct:

                        Time_IN_Str = "";
                        Time_OUT_Str = "";
                        Date_Value_Str = string.Format(Date, "yyyy/MM/dd");

                        if (SessionLcode == "UNIT I" && LocalDT.Rows[iTabRow]["ShiftDesc"].ToString() == "SHIFT10")
                        {
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeIN >='" + date1.AddDays(-1).ToString("yyyy/MM/dd") + " " + "23:00' And TimeIN <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:59' Order by TimeIN ASC";
                        }
                        else
                        {
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

                        }

                        mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

                        if (AutoDTable.Rows[iRow2][3] == "SHIFT1" || AutoDTable.Rows[iRow2][3] == "SHIFT2")
                        {


                            InTime_Check = Convert.ToDateTime(AutoDTable.Rows[iRow2][7].ToString());
                            InToTime_Check = InTime_Check.AddHours(2);

                            string s1 = InTime_Check.ToString("HH:mm:ss");
                            string s2 = InToTime_Check.ToString("HH:mm:ss");

                            string InTime_Check_str = String.Format("HH:mm:ss", InTime_Check);
                            string InToTime_Check_str = String.Format("HH:mm:ss", InToTime_Check);
                            InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
                            From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                            InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
                            To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;


                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + From_Time_Str + "'";
                            SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";

                            DS_Time = objdata.ReturnMultipleValue(SSQL);

                            if (DS_Time.Rows.Count != 0)
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' ";
                                SSQL = SSQL + " And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                                DS_InTime = objdata.ReturnMultipleValue(SSQL);

                                if (DS_InTime.Rows.Count != 0)
                                {
                                    Final_InTime = DS_InTime.Rows[0][0].ToString();
                                    SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And ShiftDesc like '%SHIFT%'";
                                    Shift_DS = objdata.ReturnMultipleValue(SSQL);
                                    Shift_Check_blb = false;



                                    for (int k = 0; k <= Shift_DS.Rows.Count; k++)
                                    {
                                        string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                        int b = Convert.ToInt16(a.ToString());
                                        Shift_Start_Time = date1.AddDays(b).ToString() + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                        string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                        int b1 = Convert.ToInt16(a1.ToString());
                                        Shift_End_Time = date1.AddDays(b1).ToString() + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                        ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                                        ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                                        EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());
                                        if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                        {
                                            Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                                            Shift_Check_blb = true;
                                        }
                                    }

                                    if (Shift_Check_blb == true)
                                    {
                                        AutoDTable.Rows[iRow2][3] = Final_Shift.ToString();
                                        AutoDTable.Rows[iRow2][7] = String.Format("HH:mm:ss", Final_InTime);

                                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                        SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "'";
                                        mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

                                        if (Final_Shift == "SHIFT2")
                                        {
                                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
                                        }
                                        else
                                        {
                                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "17:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                                        }
                                        DS_Time = objdata.ReturnMultipleValue(SSQL);
                                        if (DS_Time.Rows.Count != 0)
                                        {
                                            AutoDTable.Rows[iRow2][8] = String.Format("HH:mm:ss", DS_Time.Rows[0][0]);
                                        }
                                    }

                                    else
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' Order by TimeOUT Asc";
                                        DS_Time = objdata.ReturnMultipleValue(SSQL);

                                        if (DS_Time.Rows.Count != 0)
                                        {
                                            AutoDTable.Rows[iRow2][8] = String.Format("HH:mm:ss", DS_Time.Rows[0][0]);
                                        }

                                    }
                                }
                                else
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00' Order by TimeOUT Asc";
                                    DS_Time = objdata.ReturnMultipleValue(SSQL);

                                    if (DS_Time.Rows.Count != 0)
                                    {
                                        AutoDTable.Rows[iRow2][8] = String.Format("HH:mm:ss", DS_Time.Rows[0][0]);

                                    }
                                }
                            }
                            else
                            {
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                            }
                        }


                        else
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                        }



                        mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);
                        String Emp_Total_Work_Time_1 = "00:00";
                        if (mLocalDS_INTAB.Rows.Count > 1)
                        {
                            for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

                                if (mLocalDS_OUTTAB.Rows.Count > tin)
                                {
                                    Time_OUT_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                                }
                                else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                                {
                                    Time_OUT_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                                }
                                else
                                {
                                    Time_OUT_Str = "";
                                }

                                TimeSpan ts4;
                                ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;
                                if (LocalDT.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "";
                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                                }

                                if (Time_IN_Str == "" || Time_OUT_Str == "")
                                {
                                    time_Check_dbl = time_Check_dbl;
                                }
                                else
                                {
                                    DateTime date3 = Convert.ToDateTime(Time_IN_Str);
                                    DateTime date4 = Convert.ToDateTime(Time_OUT_Str);
                                    TimeSpan ts1;
                                    ts1 = date4.Subtract(date3);
                                    Total_Time_get = ts1.Hours.ToString();
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

                                    string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {
                                        date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                        ts1 = date4.Subtract(date3);
                                        ts1 = date4.Subtract(date3);
                                        ts4 = ts4.Add(ts1);
                                        Total_Time_get = ts1.Hours.ToString();
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {

                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        {

                                            Emp_Total_Work_Time_1 = "00:00";
                                        }

                                    }
                                    else
                                    {
                                        ts4 = ts4.Add(ts1);
                                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                            time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                        }
                                    }
                                }

                                AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;

                            }
                        }
                        else
                        {
                            TimeSpan ts4;
                            ts4 = Convert.ToDateTime(String.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS_INTAB.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                            }
                            for (int tout = 0; tout <= mLocalDS_OUTTAB.Rows.Count; tout++)
                            {
                                if (mLocalDS_OUTTAB.Rows.Count <= 0)
                                {
                                    Time_OUT_Str = "";
                                }
                                else
                                {
                                    Time_OUT_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                                }

                            }
                            if (Time_IN_Str == "" || Time_OUT_Str == "")
                            {
                                time_Check_dbl = 0;
                            }
                            else
                            {
                                DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = System.Convert.ToDateTime(Time_OUT_Str);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();

                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                }
                                else
                                {
                                    ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }

                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                }
                                AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;

                                AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;

                            }

                        }
                        // Bellow codings are correct:

                    }
                    for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
                    {

                        string MID = AutoDTable.Rows[iRow2][17].ToString();
                     

                        SSQL = "";
                        SSQL = "select Distinct isnull(EM.MachineID_Encrypt,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
                        SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
                        SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
                        SSQL = SSQL + ",isnull(EM.CatName,'') as [CatName], isnull(EM.SubCatName,'') as [SubCatName]";
                        SSQL = SSQL + ",DM.DeptCode from Employee_Mst EM inner Join Department_Mst DM on DM.DeptName=EM.DeptName Where EM.MachineID_Encrypt='" + MID + "' And EM.Compcode='" + SessionCcode.ToString() + "'";
                        //if (SessionUserType == "2")
                        //{
                        //    SSQL = SSQL + " And EM.IsNonAdmin='1'";
                        //}
                        SSQL = SSQL + " And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsActive='Yes'" + " order by DM.DeptCode Asc";

                        mEmployeeDS = objdata.ReturnMultipleValue(SSQL);

                        if (mEmployeeDS.Rows.Count > 0)
                        {
                            for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                            {
                                ss = UTF8Decryption(MID.ToString());
                                //AutoDTable.Rows[iRow2][17] = ss.ToString();

                                string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();

                                //if (AutoDTable.Rows[iRow2][9] == mEmployeeDS.Rows[iRow1]["MachineID"])
                                //{
                                AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                                AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
                                AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                                AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                                AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                                AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                                AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                                AutoDTable.Rows[iRow2][14] = SessionCompanyName.ToString();
                                AutoDTable.Rows[iRow2][15] = SessionLcode.ToString();
                                AutoDTable.Rows[iRow2][16] = Date;
                                //}
                            }
                        }
                    }
                }
            }
            //  }


            ds.Tables.Add(AutoDTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Below4Hours.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            CrystalReportViewer1.ReportSource = report;
            //CrystalReportViewer1.RefreshReport();


            //ExportOptions CrExportOptions;
            //DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
            //PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
            //CrDiskFileDestinationOptions.DiskFileName = "D:\\ND\\narmatha\\csharp.net-informations.pdf";
            //CrExportOptions = report.ExportOptions;
            //{
            //    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
            //    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
            //    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
            //    CrExportOptions.FormatOptions = CrFormatTypeOptions;
            //}
            //report.Export();



            //MailMessage mailMessage = new MailMessage();

            //mailMessage.To.Add("narmatha.k@scoto.in");
            //mailMessage.From = new MailAddress("nithya.s@scoto.in");
            //mailMessage.Subject = "ASP.NET e-mail test";
            //mailMessage.Body = "Hello world,\n\nThis is an ASP.NET test e-mail!";

            //System.Net.Mail.Attachment attachment;
            //attachment = new System.Net.Mail.Attachment("D:\\ND\\narmatha\\csharp.net-informations.pdf");
            //mailMessage.Attachments.Add(attachment);
            //SmtpClient Smtp = new SmtpClient("smtp.scoto.in");
            //Smtp.Port = 587;
            //Smtp.Host = "smtp.gmail.com";
            //Smtp.EnableSsl = true;
            //System.Net.NetworkCredential uidpwd = new System.Net.NetworkCredential("nithya.s@scoto.in", "Scoto1234#1#");
            //Smtp.Credentials = uidpwd;

            //Smtp.Send(mailMessage);

            //Response.Write("E-mail sent!");

        }

    }


    public static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    public static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }
}

