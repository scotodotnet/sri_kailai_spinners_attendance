﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class dashboard : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCompanyName = Session["CompanyName"].ToString();
        SessionLocationName = Session["LocationName"].ToString();
        SessionUserType = Session["UserType"].ToString();


        if (!IsPostBack)
        {

            Page.Title = "Spay Module | DashBoard";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
            li.Attributes.Add("class", "droplink active open");
        }
    }
}
