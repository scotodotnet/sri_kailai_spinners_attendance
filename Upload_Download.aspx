﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Upload_Download.aspx.cs" Inherits="Upload_Download" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                         <ContentTemplate>--%>
                                        
            <div class="page-breadcrumb">
                            
                    <ol class="breadcrumb container">
                       <h4><li class="active">Upload Download</li>
                           <h4>
                           </h4>
                           <h4>
                           </h4>
                        </h4> 
                    </ol>
                </div>

            <div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
    <div class="page-inner">
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Upload Download</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
				    	
                       
                        <div class="form-group row">
							<label for="input-Default" class="col-sm-2 control-label">From Date<span class="mandatory">*</span></label>
							<div class="col-sm-4">
                               <asp:TextBox ID="txtfromdate" class="form-control" runat="server" required></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender12" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtfromdate">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtfromdate" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
                               
							</div>
						
				         
			                    
				        
						 <label for="input-Default" class="col-sm-2 control-label">To Date<span class="mandatory">*</span></label>
						   <div class="col-sm-4">
                              <asp:TextBox ID="txtToDate" class="form-control" runat="server" required></asp:TextBox>
                               <cc1:CalendarExtender ID="CalendarExtender13" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtToDate">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtToDate" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
						  </div>
							
							</div>
							
					    <div class="form-group row">
							<label for="input-Default" class="col-sm-2 control-label">Employee Category<span class="mandatory">*</span></label>
							<div class="col-sm-4">
                            
                                                    <asp:DropDownList ID="ddlCategory" runat="server" class="form-control col-md-6">
                                                        <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="1">STAFF</asp:ListItem>
                                                        <asp:ListItem Value="3" Text="2">LABOUR</asp:ListItem>
                                                   </asp:DropDownList>       
							</div>
							
							<div class="col-sm-4">
						       <asp:FileUpload ID="fileUpload" runat="server" class="btn btn-default btn-rounded" name="filUpload"/>
						     </div>
				    	</div>
	
	
	
	
					<!-- Button start -->
						<div class="form-group row">
						</div>
                         <div class="txtcenter">
                         
                               
                               
                                <asp:Button ID="Btupload" class="btn btn-success"  runat="server" 
                                   Text="Upload" onclick="Btupload_Click"/>  
                                   
                               <%--<asp:Button ID="btnUpload" class="btn btn-success"  runat="server" 
                                   Text="Upload" onclick="btnUpload_Click"/> --%>
                                
                                <asp:Button ID="btnDownload" class="btn btn-danger"  runat="server" 
                                   Text="Download" onclick="btnDownload_Click"/> 
                                   
                               <asp:Button ID="btnReport" class="btn btn-success"  runat="server" 
                                   Text="Report" onclick="btnReport_Click"/> 
                                    
                                   
                                    
                              <%-- <asp:LinkButton ID="BtnClear" runat="server" class="btn btn-danger" 
                                   >Clear</asp:LinkButton>  --%> 
                           
                         </div>
                    <!-- Button End -->
					
					      <div class="form-group row">
						  </div>
                      
						  
				     </div>
 
	             </form>
				
			</div><!-- panel white end -->
		    </div>
		    <div class="col-md-2"></div>
	
                       <!-- Dashboard start -->
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- Dashboard End -->   
                        
  </div>
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
  
 </div>
                                     <%--</ContentTemplate>
                                              <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnDownload"  />
                                                   
                                             </Triggers>
 --%>
 
                                       
                                       
                                <%--</asp:UpdatePanel>--%>
</asp:Content>

