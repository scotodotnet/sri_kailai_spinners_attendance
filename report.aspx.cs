﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
using System.IO;

public partial class report : System.Web.UI.Page
{

    System.Web.UI.WebControls.DataGrid grid =
                    new System.Web.UI.WebControls.DataGrid();
     String CurrentYear1;
    static int CurrentYear;
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    //string SSQL = "";
    string ss1 = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionUserType;
    string Date1_str;
    string Date2_str;
    string Wages_Type;
    bool Errflag = false;
    bool Errflag1 = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
          
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();
          

            con = new SqlConnection(constr);
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");

                Drop_EmpCode();
                Drop_Shift();

                DropDown_WagesType();
                DropDown_Department();
                Financial_Year();
                btnAttendanceDetails.Visible = false;
                //ListRptName_SelectedIndexChanged(sender, e);  
            }
        }
    }


    public void DropDown_WagesType()
    {
        DataTable dt = new DataTable();
        dt = objdata.DropDown_WagesType();
        ddlWagesType.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["WagesTypeNo"] = 0;
        dr["WagesTypeName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "WagesTypeName";
        ddlWagesType.DataValueField = "WagesTypeNo";
        ddlWagesType.DataBind();

    }

    public void DropDown_Department()
    {
        DataTable dt = new DataTable();
        dt = objdata.Dropdown_Department();
        ddlDepartment.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["DeptCode"] = 0;
        dr["DeptName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptCode";
        ddlDepartment.DataBind();

    }

   public void Drop_EmpCode()
   {
       DataTable dt = new DataTable();

       string SSQL;

       SSQL = "";
       SSQL = "Select ExistingCode +'->'+ MachineID +'->'+ FirstName As EmpName From Employee_Mst";
       SSQL = SSQL + " Where CompCode='" + SessionCcode + "'";
       SSQL = SSQL + " and LocCode='" + SessionLcode + "' And IsActive='Yes'";
       //If UCase(mvarUserType) = UCase("IF User") Then
       //    SSQL = SSQL & " And PFNo <> '' And PFNo IS Not Null And PFNo <> '0' And PFNo <> 'o' And PFNo <> 'A'"
       //    SSQL = SSQL & " And PFNo <> 'NULL'"
       //End If

       //If mUserLocation <> "" Then
       //if (SessionUserType == "2")
       //{
       //    SSQL = SSQL + " And IsNonAdmin='1'";
       //}
      
       // End If

       SSQL = SSQL + " Order By EmpNo";

       dt = objdata.ReturnMultipleValue(SSQL);
       if (dt.Rows.Count > 0)
       {
           ddlEmpName.Items.Clear();
           ddlEmpName.Items.Add("- select -");
         
           for (int i = 0; i < dt.Rows.Count; i++)
           {
               ddlEmpName.Items.Add(dt.Rows[i]["EmpName"].ToString());
           }
       }
   }

   public void Financial_Year()
   {
      
       CurrentYear1 = DateTime.Now.Year.ToString();
       CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());
       if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
       {
           CurrentYear = CurrentYear - 1;
       }
       for (int i = 0; i < 11; i++)
       {
           
           string tt = (CurrentYear1 + "-" + (CurrentYear - 1));
           ddlYear.Items.Add(tt.ToString());
           CurrentYear = CurrentYear - 1;
           string cy = Convert.ToString(CurrentYear);
           CurrentYear1 = cy;
       }
   }
    


   public void Drop_Shift()
   {
       string SSQL;
        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = "select ShiftDesc,StartTime,EndTime from Shift_Mst Where Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
        dt = objdata.ReturnMultipleValue(SSQL);
       if(dt.Rows.Count>0)
       {
           ddlShift.Items.Clear();
           ddlShift.Items.Add("- select -");
           ddlShift.Items.Add("ALL");
           for(int i=0;i<dt.Rows.Count;i++)
           {
               ddlShift.Items.Add(dt.Rows[i]["ShiftDesc"].ToString());
           }
          // ddlShift.Items.Add("NO SHIFT");
       }

   }




   protected void ddlRptName_SelectedIndexChanged(object sender, EventArgs e)
   {
       if (SessionUserType == "2")
       {
           if (ddlRptName.SelectedItem.Text == "ALL")
           {
               ListRptName.Items.Clear();
               ListRptName.Items.Add("DAY ATTENDANCE - BETWEEN DATES");
               ListRptName.Items.Add("DAY ATTENDANCE - DAY WISE");
               ListRptName.Items.Add("ABSENT REPORT - BETWEEN DATES");
               ListRptName.Items.Add("OT REPORT - BETWEEN DATES");
               ListRptName.Items.Add("PAYROLL ATTENDANCE");
               ListRptName.Items.Add("PAYROLL ATTENDANCE NEW");
               //ListRptName.Items.Add("NEW PAYROLL ATTENDANCE");
               ListRptName.Items.Add("MANUAL ATTENDANCE - BETWEEN DATES");
               ////ListRptName.Items.Add("BELOW FOUR HOURS");
               ////ListRptName.Items.Add("BELOW EIGHT HOURS");
               ////ListRptName.Items.Add("ABOVE FOURTEEN HOURS");
               ////ListRptName.Items.Add("ABSENT REPORT DAY WISE");
               ListRptName.Items.Add("DAY ATTENDANCE SHIFT WISE");
               ////ListRptName.Items.Add("LATE IN");
               ////ListRptName.Items.Add("LATE IN NEW");
               ////ListRptName.Items.Add("EARLY OUT");
               ////ListRptName.Items.Add("EARLY OUT NEW");
               ////ListRptName.Items.Add("EMPLOYEE LONG ABSENT REPORT");
               //ListRptName.Items.Add("MANUAL ATTENDANCE DAY WISE");
               // ListRptName.Items.Add("PAYROLL OT HOURS");
               ////ListRptName.Items.Add("DAY ATTENDANCE WITH OT HOURS");
               ////ListRptName.Items.Add("EMPLOYEE WISE MUSTER REPORT");
               ////ListRptName.Items.Add("LUNCH TIME REPORT");
               ////ListRptName.Items.Add("DEPARTMENT SALARY CONSOLDIDATE REPORT");
               ////ListRptName.Items.Add("GRACE TIME DEDUCTION");
               ////ListRptName.Items.Add("OT EMPLOYEE LIST");
               ListRptName.Items.Add("SALARY CONSOLDIDATE REPORT");
               ListRptName.Items.Add("TOTAL DAYS SUMMARY REPORT");
               ////ListRptName.Items.Add("WEEKLY OT SLIP");
           }
           if (ddlRptName.SelectedItem.Text == "SPECIFIC")
           {
               ListRptName.Items.Clear();
               ListRptName.Items.Add("EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES");
               ListRptName.Items.Add("EMPLOYEE FULL PROFILE");
               //ListRptName.Items.Add("DEPARTMENT WISE EMPLOYEE DETAILS");
               ListRptName.Items.Add("DEPARTMENT SALARY CONSOLDIDATE REPORT");
              // ListRptName.Items.Add("TOTAL DAYS SUMMARY REPORT");

           }

           if (ddlRptName.SelectedItem.Text == "ABSTRACT")
           {
               ListRptName.Items.Clear();
               ListRptName.Items.Add("DAY EMPLOYEE SUMMARY");
               ListRptName.Items.Add("DAY ATTENDANCE SUMMARY");
               ListRptName.Items.Add("MISMATCH SHIFT REPORT - DAY WISE");
               //ListRptName.Items.Add("MALE FEMALE COUNT REPORT");
               //ListRptName.Items.Add("PRESENT ABSENT STRENGTH REPORT");
               //ListRptName.Items.Add("SALARY COVER REPORT");              
           }
           if (ddlRptName.SelectedItem.Text == "OTHERS")
           {
               ListRptName.Items.Clear();
               ListRptName.Items.Add("EMPLOYEE MASTER");
               ListRptName.Items.Add("BREAK TIME");
               //ListRptName.Items.Add("DEACTIVATED EMPLOYEE REPORT");
               //ListRptName.Items.Add("HOSTEL MALE FEMALE REPORT");
               //ListRptName.Items.Add("MACHINE MANIPULATION REPORT");
               //ListRptName.Items.Add("DAY ATTENDANCE CHART");
               //ListRptName.Items.Add("MALE FEMALE CHAT");
               //ListRptName.Items.Add("TEST1");
               //ListRptName.Items.Add("TEST2");
           }
                          
                
       }
       else
       {
           if (ddlRptName.SelectedItem.Text == "ALL")
           {
               ListRptName.Items.Clear();
               ListRptName.Items.Add("DAY ATTENDANCE - BETWEEN DATES");
               ListRptName.Items.Add("DAY ATTENDANCE - DAY WISE");
               ListRptName.Items.Add("ABSENT REPORT - BETWEEN DATES");
               ListRptName.Items.Add("OT REPORT - BETWEEN DATES");
               ListRptName.Items.Add("PAYROLL ATTENDANCE");
               ListRptName.Items.Add("PAYROLL ATTENDANCE NEW");
               //ListRptName.Items.Add("NEW PAYROLL ATTENDANCE");
               ListRptName.Items.Add("MANUAL ATTENDANCE - BETWEEN DATES");
               ////ListRptName.Items.Add("BELOW FOUR HOURS");
               ////ListRptName.Items.Add("BELOW EIGHT HOURS");
               ////ListRptName.Items.Add("ABOVE FOURTEEN HOURS");
               ////ListRptName.Items.Add("ABSENT REPORT DAY WISE");
               ListRptName.Items.Add("DAY ATTENDANCE SHIFT WISE");
               ////ListRptName.Items.Add("LATE IN");
               ////ListRptName.Items.Add("LATE IN NEW");
               ////ListRptName.Items.Add("EARLY OUT");
               ////ListRptName.Items.Add("EARLY OUT NEW");
               ////ListRptName.Items.Add("EMPLOYEE LONG ABSENT REPORT");
               ListRptName.Items.Add("MANUAL ATTENDANCE DAY WISE");
               // ListRptName.Items.Add("PAYROLL OT HOURS");
               ////ListRptName.Items.Add("DAY ATTENDANCE WITH OT HOURS");
               ////ListRptName.Items.Add("EMPLOYEE WISE MUSTER REPORT");
               ////ListRptName.Items.Add("LUNCH TIME REPORT");
               ////ListRptName.Items.Add("DEPARTMENT SALARY CONSOLDIDATE REPORT");
               ////ListRptName.Items.Add("GRACE TIME DEDUCTION");
               ////ListRptName.Items.Add("OT EMPLOYEE LIST");
               ListRptName.Items.Add("SALARY CONSOLDIDATE REPORT");
               ListRptName.Items.Add("TOTAL DAYS SUMMARY REPORT");
               ////ListRptName.Items.Add("WEEKLY OT SLIP");
           }
           if (ddlRptName.SelectedItem.Text == "SPECIFIC")
           {
               ListRptName.Items.Clear();
               ListRptName.Items.Add("EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES");
               ListRptName.Items.Add("EMPLOYEE FULL PROFILE");
               //ListRptName.Items.Add("DEPARTMENT WISE EMPLOYEE DETAILS");
               ListRptName.Items.Add("DEPARTMENT SALARY CONSOLDIDATE REPORT");

           }

           if (ddlRptName.SelectedItem.Text == "ABSTRACT")
           {
               ListRptName.Items.Clear();
               ListRptName.Items.Add("DAY EMPLOYEE SUMMARY");
               ListRptName.Items.Add("DAY ATTENDANCE SUMMARY");
               ListRptName.Items.Add("MISMATCH SHIFT REPORT - DAY WISE");
               //ListRptName.Items.Add("MALE FEMALE COUNT REPORT");
               //ListRptName.Items.Add("PRESENT ABSENT STRENGTH REPORT");
               //ListRptName.Items.Add("SALARY COVER REPORT");              
           }
           if (ddlRptName.SelectedItem.Text == "OTHERS")
           {
               ListRptName.Items.Clear();
               ListRptName.Items.Add("EMPLOYEE MASTER");
               ListRptName.Items.Add("BREAK TIME");
               //ListRptName.Items.Add("DEACTIVATED EMPLOYEE REPORT");
               //ListRptName.Items.Add("HOSTEL MALE FEMALE REPORT");
               //ListRptName.Items.Add("MACHINE MANIPULATION REPORT");
               //ListRptName.Items.Add("DAY ATTENDANCE CHART");
               //ListRptName.Items.Add("MALE FEMALE CHAT");
               //ListRptName.Items.Add("TEST1");
               //ListRptName.Items.Add("TEST2");
           }
       }
      
   }
   protected void btnReport_Click(object sender, EventArgs e)
       
   {
      
       if (RptLabel.Text == "TEST1")
       {
           if (ddlDepartment.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Department');", true);
           }
           else if (ddlYear.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Financial Year');", true);
           }
           
           else
           {
               ResponseHelper.Redirect("EmployeeFinancialYearReport.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Year=" + ddlYear.SelectedItem.Text, "_blank", "");
           }
       }
       if (RptLabel.Text == "MANUAL ATTENDANCE DAY WISE")
       {
           if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("ManualAttendanceDayWise.aspx?ShiftType1=" + ddlShift.SelectedValue + "&FromDate=" + txtFrmdate.Text, "_blank", "");
           }
       }

       if (RptLabel.Text == "TEST2")
       {

           if (ddlDepartment.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Department');", true);
           }
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else if (txtTodate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("DayAttendanceHours.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Date1=" + txtFrmdate.Text + "&Date2=" + txtTodate.Text, "_blank", "");
           }
       }

       if (RptLabel.Text == "BREAK TIME")
       {

           if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("LunchTimeBreak_New.aspx?Date=" + txtFrmdate.Text, "_blank", "");
           }


       }

       if (RptLabel.Text == "DAY ATTENDANCE CHAT")
       {
          
           if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
        
           else
           {
               ResponseHelper.Redirect("DayAttendanceChart.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
       }

       if (RptLabel.Text == "MALE FEMALE CHAT")
       {
       
          if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("MaleFemaleCountReport.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
       }
       
       
       if (RptLabel.Text == "EMPLOYEE FULL PROFILE")
       {
           if (ddlEmpName.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select EmpName');", true);
           }
           else
           {
               ResponseHelper.Redirect("EmployeeFull.aspx?Empcode=" + ddlEmpName.SelectedItem.Text, "_blank", "");
           }
       }

       if (RptLabel.Text == "DAY EMPLOYEE SUMMARY")
       {
          if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("improper.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
       }


      
       
       if (RptLabel.Text == "WEEKLY OT SLIP")
       {
           if (ddlWagesType.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
           }
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else if (txtTodate.Text == "")
           {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("PayRollOTHoursSalaryCalculation.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + ddlWagesType.SelectedItem.Text, "_blank", "");
           }
       }
     
       
       if (RptLabel.Text == "LATE IN NEW")
       {
          
           if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("LateINNew.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
       }


       if (RptLabel.Text == "MISMATCH SHIFT REPORT - DAY WISE")
       {
         
           if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
         
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
              ResponseHelper.Redirect("MismatchReport.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
       }


       if (RptLabel.Text == "DAY ATTENDANCE - DAY WISE")
       {
         
           if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("DayAttendanceDayWise.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
       }

       if (RptLabel.Text == "BELOW FOUR HOURS")
       {
      
           if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           //else if (txtTodate.Text == "")
           //{
           //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
           //}
           else
           {
              ResponseHelper.Redirect("BelowFourHours.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
       }

       if (RptLabel.Text == "BELOW EIGHT HOURS")
       {
      
          if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
         
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           //else if (txtTodate.Text == "")
           //{
           //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
           //}
           else
           {
               ResponseHelper.Redirect("BelowEighthrs.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
       }

       if (RptLabel.Text == "ABOVE FOURTEEN HOURS")
       {
          
           if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           //else if (txtTodate.Text == "")
           //{
           //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
           //}
           else
           {
               ResponseHelper.Redirect("Above14hrs.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
       }

     

     
       if (RptLabel.Text == "LATE IN")
       {

           if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("LateInReport.aspx?Date=" + txtFrmdate.Text,"_blank", "");
           }
       }

      

       //if (RptLabel.Text == "EMPLOYEE LONG ABSENT REPORT")
       //{
       //    ResponseHelper.Redirect("EmployeeLongAbsentReport.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&LeaveDays=" + txtleavedays.Text + "&WagesType=" + ddlWagesType.SelectedItem.Text, "_blank", "");
       //}

      

       if (RptLabel.Text == "DAY ATTENDANCE WITH OT HOURS")
       {
           
            if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
         
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           //else if (txtTodate.Text == "")
           //{
           //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
           //}
           else
           {
             ResponseHelper.Redirect("DayattendancewithOTHr.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
       }



       if (RptLabel.Text == "LUNCH TIME REPORT")
       {
           if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
           
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("LunchTimeReport.aspx?EmpCode=" + ss1 + "&ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
       }


       if (RptLabel.Text == "MALE FEMALE COUNT REPORT")
       {
         
           if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("MaleFemaleCountReport.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text + "&Date2=" + txtTodate.Text , "_blank", "");
           }
       }


       if (RptLabel.Text == "PRESENT ABSENT STRENGTH REPORT")
       {
           
           if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("PresentAbsentStrengthReport.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text + "&Date2=" + txtTodate.Text , "_blank", "");
           }
       }

       if (RptLabel.Text == "DEPARTMENT SALARY CONSOLDIDATE REPORT")
       {
        if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
          
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("DeptSalaryConsolidateReport.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
       }


       if (RptLabel.Text == "GRACE TIME DEDUCTION")
       {
           
           if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
          
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else if (ddlCategory.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Category');", true);
           }
           else
           {
              ResponseHelper.Redirect("GraceTimeDeduction.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text + "&ddlShiftType=" + "&EmpCategory=" + ddlCategory.SelectedItem.Text, "_blank", "");
           }
       }

       if (RptLabel.Text == "SALARY COVER REPORT")
       {
           if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else if (txtTodate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("SalaryCoverReport.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
           }
       }

       if (RptLabel.Text == "DEPARTMENT WISE EMPLOYEE DETAILS")
       {

           if (ddlDepartment.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter In/Out Mode Type');", true);
           }
           else
           {
               ResponseHelper.Redirect("DepartmentWiseEmployeeList.aspx?Dept=" + ddlDepartment.SelectedItem.Text, "_blank", "");
           }
       }

       if (RptLabel.Text == "SALARY CONSOLDIDATE REPORT")
       {
           if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
       
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("SalaryConsolidatedReport.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
       }
   }


   protected void btnExcel_Click(object sender, EventArgs e)
   {

       if (RptLabel.Text == "TOTAL DAYS SUMMARY REPORT")
       {
           if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
               Errflag1=true ;
           }

           else if (txtTodate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
               Errflag1=true;
           }

           string fromDate = txtFrmdate.Text;
           string toDate = txtTodate.Text;
           string[] DateChk = fromDate.Split('/');
           string ToDateChk;
           string FromDateChk;
           string MonthCalculate = DateChk[1];
           Int64 finyear = Convert.ToInt64(DateChk[2]);
           string Lastdate = "";

           DateTime Date1 = Convert.ToDateTime(fromDate);
           DateTime Date2 = Convert.ToDateTime(toDate);
           

           //if (MonthCalculate == "01" || MonthCalculate == "03" || MonthCalculate == "05" || MonthCalculate == "07" || MonthCalculate == "08" || MonthCalculate == "10" || MonthCalculate == "12")
           //{
           //    Lastdate = "31";
           //}
           //else if (MonthCalculate == "04" || MonthCalculate == "06" || MonthCalculate == "09" || MonthCalculate == "11")
           //{
           //    Lastdate = "30";
           //}
           //else if (finyear % 4 == 0 && finyear % 100 != 0 || finyear % 400 == 0)
           //{
           //    Lastdate = "29";
           //}
           //else
           //{
           //    Lastdate = "28";
           //}

           //FromDateChk = "01/" + DateChk[1] + "/" + DateChk[2];
           //ToDateChk = Lastdate + "/" + DateChk[1] + "/" + DateChk[2];


           //int daycount = (int)((Date2 - Date1).TotalDays) + 1;


           //if (daycount != Convert.ToInt16(Lastdate) && daycount != 7)
           //{
           //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Given Date Must Be Weekly or Monthly');", true);
           //}


           if (!Errflag1)
           {
               ResponseHelper.Redirect("TotalDaySummaryReportBetweenDays.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
           }
           else
           {
               ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with given Date...');", true);
           }

       }
       if (RptLabel.Text == "BREAK TIME")
       {

           if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("BreakReport.aspx?Date=" + txtFrmdate.Text, "_blank", "");
           }


       }
       if (RptLabel.Text == "PAYROLL OT HOURS")
       {
           if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else if (txtTodate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("PayRollOTHours.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
           }
       }

       if (RptLabel.Text == "DAY ATTENDANCE SHIFT WISE")
       {

           if (ddlShift.SelectedItem.Text == "- select -" || ddlShift.SelectedItem.Text == "ALL")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift Type');", true);
           }
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("DayAttendanceShiftWise.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
       }


       if (RptLabel.Text == "ABSENT REPORT DAY WISE")
       {
           if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("AbsentReport.aspx?Date=" + txtFrmdate.Text, "_blank", "");
           }
       }

       if (RptLabel.Text == "MACHINE MANIPULATION REPORT")
       {
           Response.Redirect("MachineManipulationReport.aspx");
       }

       if (RptLabel.Text == "HOSTEL MALE FEMALE REPORT")
       {
           if (RdbGendarM.Checked == true || RdbGendarF.Checked == true)
           {
               string Gender = RdbGendarM.Text;
               ResponseHelper.Redirect("HostelMaleFemaleReport.aspx?Gender=" + Gender.ToString(), "_blank", "");
           }
           else if (RdbGendarM.Checked == false && RdbGendarF.Checked == false)
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Gender');", true);
           }
       }
       if (RptLabel.Text == "EARLY OUT")
       {
           if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               ResponseHelper.Redirect("Earlyout.aspx?Date=" + txtFrmdate.Text, "_blank", "");
           }
       }

       if (RptLabel.Text == "EARLY OUT NEW")
        {
           if (ddlShift.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           }
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
       
           else 
           {
               ResponseHelper.Redirect("EarlyOUTReportNew.aspx?ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text , "_blank", "");
           }
        }
       if (RptLabel.Text == "DEACTIVATED EMPLOYEE REPORT")
       {
           Response.Redirect("DeactivatedEmployeeReport.aspx");
       }

       if (RptLabel.Text == "OT EMPLOYEE LIST")
       {
           if (OTYes.Checked == true)
           {
               Response.Redirect("OTEmployeeList.aspx");
           }
           else if(OTYes.Checked == false && OTNo.Checked ==false)
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select OT Eligible Yes/No');", true);
           }
       }
       if (RptLabel.Text == "NON ELIGIBLE OT REPORT")
       {
           if (OTNo.Checked == true)
           {
               Response.Redirect("NonOTEmployeeList.aspx");
           }
           else if(OTYes.Checked == false && OTNo.Checked == false)
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select OT Eligible Yes/No');", true);
           }
       }

       if (RptLabel.Text == "OT REPORT - BETWEEN DATES")
       {
          if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else if (txtTodate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
           }
           else
           {
              
               ResponseHelper.Redirect("OTReportBetweendates.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
           }
       }

     
       if (RptLabel.Text == "DAY ATTENDANCE SUMMARY")
       {
            if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter From Date');", true);
           }
           //else if (txtTodate.Text == "")
           //{
           //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter To Date');", true);
           //}
          else
           {
               ResponseHelper.Redirect("DayAttendanceSummary.aspx?Date1=" + txtFrmdate.Text, "_blank", "");
           }

       }

       if (RptLabel.Text == "EMPLOYEE MASTER")
       {
           //if (ddlIsAct.SelectedItem.Text == "- select -")
           //{
           //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Active or Not');", true);
           //}
           //else if (ddlCategory.SelectedItem.Text == "- select -")
           //{
           //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Category');", true);
           //}
           //else
           //{
               ResponseHelper.Redirect("EmployeeMaster.aspx?IsActive=" + ddlIsAct.SelectedItem.Text + "&Category=" + ddlCategory.SelectedItem.Text, "_blank", "");
           //}
       }

       //new report(use logtimeIN & logtime OUT table)
       if (RptLabel.Text == "DAY ATTENDANCE - BETWEEN DATES")
       {
           if (txtFrmdate.Text != "" && txtTodate.Text != "")
           {
               ResponseHelper.Redirect("MusterReportBWDates.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text +"&Wages=" +ddlWagesType.SelectedItem.Text, "_blank", "");
           }
           else
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
           }
       }

       if (RptLabel.Text == "PAYROLL ATTENDANCE")
       {


           if (txtFrmdate.Text != "" && txtTodate.Text != "")
           {
               ResponseHelper.Redirect("PayrollAttendance.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + ddlWagesType.SelectedItem.Text, "_blank", "");
           }
           else if (ddlWagesType.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
           }
           else
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
           }
       }

       if (RptLabel.Text == "PAYROLL ATTENDANCE NEW")
       {
           if (txtFrmdate.Text != "" && txtTodate.Text != "")
           {
               ResponseHelper.Redirect("PayrollAttendance_New.aspx?FromDate=" + txtFrmdate.Text + "&Todate=" + txtTodate.Text + "&wages=" + ddlWagesType.SelectedItem.Text, "_blank", "");   
           }
           else if (ddlWagesType.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
           }
           else
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
           }
           
       }
       if (RptLabel.Text == "NEW PAYROLL ATTENDANCE")
       {
           if (txtFrmdate.Text != "" && txtTodate.Text != "")
           {
               ResponseHelper.Redirect("NewPayRollattendance.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + ddlWagesType.SelectedItem.Text, "_blank", "");
           }
           else if (ddlWagesType.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
           }
           else
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
           }
       }


       if (RptLabel.Text == "ABSENT REPORT - BETWEEN DATES")
       {
           if (txtFrmdate.Text == "" || txtTodate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
               Errflag = true;
           }
           else if (ddlWagesType.SelectedItem.Text == "" || ddlWagesType.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('$$ - Select The WagesType - $$');", true);
               Errflag = true;
           }
           if (!Errflag)
           {
               ResponseHelper.Redirect("AbsentReportBetweenDatesReport_New.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
               Session["Wages"] = ddlWagesType.SelectedItem.Text;
           }

       }
       if (RptLabel.Text == "MANUAL ATTENDANCE - BETWEEN DATES")
       {
           if (txtFrmdate.Text != "" && txtTodate.Text != "")
           {
               ResponseHelper.Redirect("ManualAttendanceReport.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
               
           }
           else
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
           }
       }

       if (RptLabel.Text == "EMPLOYEE LONG ABSENT REPORT")
       {
          if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else if (txtTodate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
           }
           else if (txtleavedays.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Leave Days');", true);
           }
           else if (ddlWagesType.SelectedItem.Text == "- select -")
           {
              ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
           }
           else
           {
               ResponseHelper.Redirect("EmployeeLongAbsentReport.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&LeaveDays=" + txtleavedays.Text + "&WagesType=" + ddlWagesType.SelectedItem.Text, "_blank", "");
           }
       }

       if (RptLabel.Text == "EMPLOYEE WISE MUSTER REPORT")
       {
           //if (ddlMode.SelectedItem.Text == "- select -")
           //{
           //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter In/Out Mode Type');", true);
           //}
           if (ddlEmpName.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Employee Name');", true);
           }
           //else if (ddlShift.SelectedItem.Text == "- select -")
           //{
           //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
           //}
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else if (txtTodate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
           }
           else
           {
               string s = ddlEmpName.SelectedItem.Text;
               string[] delimiters = new string[] { "->" };
               string[] items = s.Split(delimiters, StringSplitOptions.None);
               string ss = items[2];
               ss1 = items[1];
               ResponseHelper.Redirect("EmployeeWiseMusterReport.aspx?EmpName=" + ss + "&EmpCode=" + ss1 + "&ShiftType1=" + ddlShift.SelectedValue + "&Date1=" + txtFrmdate.Text + "&Date2=" + txtTodate.Text, "_blank", "");
           }
       }


       

      


       if (RptLabel.Text == "EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES")
       {
           if (ddlEmpName.SelectedItem.Text == "- select -")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select EmpName');", true);
           }
           else if (txtFrmdate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }

           else if (txtTodate.Text == "")
           {
               ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
           }
           else
           {
               string s = ddlEmpName.SelectedItem.Text;
               string[] delimiters = new string[] { "->" };
               string[] items = s.Split(delimiters, StringSplitOptions.None);
               string ss = items[0];
               ss1 = items[1];

               ResponseHelper.Redirect("EmployeeDaywiseAttendance.aspx?EmpCode=" + ss1 + "&Date1=" + txtFrmdate.Text + "&Date2=" + txtTodate.Text, "_blank", "");
           }

       }

   }

   protected void OTYes_CheckedChanged(object sender, EventArgs e)
   {
       if (OTYes.Checked == true)
       {
           OTNo.Checked = false;
       }
   }
   protected void OTNo_CheckedChanged(object sender, EventArgs e)
   {
       if (OTNo.Checked == true)
       {
           OTYes.Checked = false;
       }
   }


   protected void RdbGendarM_CheckedChanged(object sender, EventArgs e)
   {
       if (RdbGendarM.Checked == true)
       {
           RdbGendarF.Checked = false;
       }
   }
   protected void RdbGendarF_CheckedChanged(object sender, EventArgs e)
   {
       if (RdbGendarF.Checked == true)
       {
           RdbGendarM.Checked = false;
       }
   }



   protected void ListRptName_SelectedIndexChanged(object sender, EventArgs e)
   {
       RptLabel.Text = ListRptName.SelectedItem.Text;

       //if (RptLabel.Text == "DAY ATTENDANCE - DAY WISE" || RptLabel.Text == "MISMATCH SHIFT REPORT - DAY WISE" || RptLabel.Text == "EMPLOYEE FULL PROFILE")
       //{
       //    btnExcel.Enabled = false;
       //    btnReport.Enabled = true;

       //}
       //if (RptLabel.Text == "OT REPORT - BETWEEN DATES" || RptLabel.Text == "DAY ATTENDANCE SUMMARY" || RptLabel.Text == "EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES" || RptLabel.Text == "EMPLOYEE MASTER" || RptLabel.Text == "DAY ATTENDANCE - BETWEEN DATES" || RptLabel.Text == "PAYROLL ATTENDANCE" || RptLabel.Text == "ABSENT REPORT - BETWEEN DATES" || RptLabel.Text == "MANUAL ATTENDANCE - BETWEEN DATES")
       //{
       //    btnReport.Enabled = false;
       //    btnExcel.Enabled = true;
       //}

       if (RptLabel.Text == "DAY ATTENDANCE - BETWEEN DATES" || RptLabel.Text == "TOTAL DAYS SUMMARY REPORT" || RptLabel.Text == "ABSENT REPORT - BETWEEN DATES" || RptLabel.Text == "PAYROLL ATTENDANCE" || RptLabel.Text == "MANUAL ATTENDANCE - BETWEEN DATES" || RptLabel.Text == "NEW PAYROLL ATTENDANCE")
       {

           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = true;
         
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = true;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           btnAttendanceDetails.Visible = true;
           Clear();
       }
       if (RptLabel.Text == "PAYROLL ATTENDANCE" || RptLabel.Text == "NEW PAYROLL ATTENDANCE")
       {
           ddlWagesType.Enabled = true;
           btnAttendanceDetails.Visible = true;
       }
       if (RptLabel.Text == "DAY ATTENDANCE - DAY WISE" || RptLabel.Text == "MANUAL ATTENDANCE DAY WISE" || RptLabel.Text == "BELOW FOUR HOURS" || RptLabel.Text == "BELOW EIGHT HOURS" || RptLabel.Text == "ABOVE FOURTEEN HOURS" || RptLabel.Text == "DAY ATTENDANCE WITH OT HOURS" || RptLabel.Text == "LUNCH TIME REPORT" || RptLabel.Text == "DEPARTMENT SALARY CONSOLDIDATE REPORT" || RptLabel.Text == "SALARY CONSOLDIDATE REPORT" || RptLabel.Text == "MISMATCH SHIFT REPORT - DAY WISE" || RptLabel.Text == "MALE FEMALE COUNT REPORT" || RptLabel.Text == "PRESENT ABSENT STRENGTH REPORT" || RptLabel.Text == "LATE IN NEW" || RptLabel.Text == "DAY EMPLOYEE SUMMARY" || RptLabel.Text == "DAY ATTENDANCE CHART" || RptLabel.Text == "MALE FEMALE CHAT")
       {
           btnReport.Enabled = true;
           btnExcel.Enabled = false;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
      
           ddlShift.Enabled = true;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           btnAttendanceDetails.Visible = false;
           Clear();
       }
       if (RptLabel.Text == "OT REPORT - BETWEEN DATES" || RptLabel.Text == "PAYROLL OT HOURS")
       {
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
           btnAttendanceDetails.Visible = false;
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = true;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           //Clear();
       }

       if (RptLabel.Text == "ABSENT REPORT DAY WISE" || RptLabel.Text == "EARLY OUT")
       {
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
           btnAttendanceDetails.Visible = false;
           ddlShift.Enabled = true;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           Clear();
       }

       if (RptLabel.Text == "DAY ATTENDANCE SHIFT WISE")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
        
           ddlShift.Enabled = true;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           Clear();
       }
       if (RptLabel.Text == "EMPLOYEE WISE MUSTER REPORT")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = true;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
           ddlShift.Enabled = true;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = true;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           Clear();
       }
       if (RptLabel.Text == "LATE IN")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = true;
           btnExcel.Enabled = false;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           Clear();
       }

       if (RptLabel.Text == "EMPLOYEE LONG ABSENT REPORT")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = true;
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = true;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = true;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           Clear();
       }
       if (RptLabel.Text == "GRACE TIME DEDUCTION")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = true;
           btnExcel.Enabled = false;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
           ddlShift.Enabled = true;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = true;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           Clear();
       }
       if (RptLabel.Text == "BREAK TIME")
       {
           //ddlStateMaster.Enabled = false;
           btnReport.Enabled = true;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;

           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           //txtsearch.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           btnAttendanceDetails.Visible = false;
           Clear();
       }
       if (RptLabel.Text == "OT EMPLOYEE LIST")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = false;
           txtTodate.Enabled = false;
           OTYes.Enabled = true;
           OTNo.Enabled = true;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           Clear();
       }


       if (RptLabel.Text == "EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = true;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = true;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           Clear();
       }

       if (RptLabel.Text == "EMPLOYEE FULL PROFILE")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = true;
           btnExcel.Enabled = false;
           ddlEmpName.Enabled = true;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
   
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = false;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           Clear();
       }
       if (RptLabel.Text == "DEPARTMENT WISE EMPLOYEE DETAILS")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = true;
           btnExcel.Enabled = false;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = true;
           ddlWagesType.Enabled = false;
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = false;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           Clear();
       }

       if (RptLabel.Text == "DAY ATTENDANCE SUMMARY")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
          
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           Clear();
       }
       if (RptLabel.Text == "EMPLOYEE MASTER")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
          
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = true;
           ddlCategory.Enabled = true;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = false;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           Clear();
       }

       if (RptLabel.Text == "DEACTIVATED EMPLOYEE REPORT")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
       
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = false;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           Clear();
       }

       if (RptLabel.Text == "EARLY OUT NEW")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
           
           ddlShift.Enabled = true;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarM.Enabled = false;
           RdbGendarF.Enabled = false;
           Clear();
       }
       if (RptLabel.Text == "HOSTEL MALE FEMALE REPORT")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
        
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = false;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarF.Enabled = true;
           RdbGendarM.Enabled = true;
           Clear();
       }

       if (RptLabel.Text == "MACHINE MANIPULATION REPORT")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
         
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = false;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarF.Enabled = false;
           RdbGendarM.Enabled = false;
           Clear();
       }

       if (RptLabel.Text == "WEEKLY OT SLIP")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = true;
           btnExcel.Enabled = false;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = true;
       
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = true;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarF.Enabled = false;
           RdbGendarM.Enabled = false;
           Clear();
       }
       if (RptLabel.Text == "TEST2")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = true;
           btnExcel.Enabled = false;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = true;
           ddlWagesType.Enabled = false;
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = true;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarF.Enabled = false;
           RdbGendarM.Enabled = false;
           Clear();
       }

      

         if (RptLabel.Text == "SALARY COVER REPORT")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = true;
           btnExcel.Enabled = false;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = false;
           ddlWagesType.Enabled = false;
           ddlShift.Enabled = false;
           ddlYear.Enabled = false;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = true;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarF.Enabled = false;
           RdbGendarM.Enabled = false;
           Clear();

       }


       if (RptLabel.Text == "TEST1")
       {
           btnAttendanceDetails.Visible = false;
           btnReport.Enabled = true;
           btnExcel.Enabled = false;
           ddlEmpName.Enabled = false;
           ddlDepartment.Enabled = true;
           ddlWagesType.Enabled = false;
 
           ddlShift.Enabled = false;
           ddlYear.Enabled = true;
           ddlIsAct.Enabled = false;
           ddlCategory.Enabled = false;
           txtTypeOfCertificate.Enabled = false;
           txtleavedays.Enabled = false;
           txtFrmdate.Enabled = false;
           txtTodate.Enabled = false;
           OTYes.Enabled = false;
           OTNo.Enabled = false;
           RdbGendarF.Enabled = false;
           RdbGendarM.Enabled = false;
           Clear();

       }

   }
     
    public void Clear()
      {
           ddlCategory.SelectedIndex = 0;
           ddlDepartment.SelectedIndex = 0;
           ddlEmpName.SelectedIndex = 0;
           ddlIsAct.SelectedIndex = 0;
          
             ddlShift.SelectedIndex = 0;
       
           ddlWagesType.SelectedIndex = 0;
           ddlYear.SelectedIndex = 0;
           
           txtFrmdate.Text = "";
           txtleavedays.Text = "";
           txtTypeOfCertificate.Text = "";
           txtTodate.Text = "";

           RdbGendarF.Checked = false;
           RdbGendarM.Checked = false;
           OTNo.Checked = false;
           OTYes.Checked = false;

           btnAttendanceDetails.Visible = false;
   }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }

    protected void btnAttendanceDetails_Click(object sender, EventArgs e)
    {
        if (txtFrmdate.Text != "" && txtTodate.Text != "")
        {
            AttendanceDetailsTable_INEpay();
        }
        else if (ddlWagesType.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
        }
    }
   


    public void AttendanceDetailsTable_INEpay()
    {
       
        BALDataAccess objdata = new BALDataAccess();
        DataTable AutoDTable = new DataTable();
        DataTable DataCells = new DataTable();
        DataTable mLocalDS = new DataTable();


        AutoDTable.Columns.Add("DeptName");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("ExistingCode");
        AutoDTable.Columns.Add("MachineEncry");
        AutoDTable.Columns.Add("EmpName");


        DataCells.Columns.Add("MachineID");
        DataCells.Columns.Add("ExistingCode");
        DataCells.Columns.Add("FirstName");
        DataCells.Columns.Add("Days");
        DataCells.Columns.Add("H.Allowed");
        DataCells.Columns.Add("N/FH");
        DataCells.Columns.Add("OT Days");
        DataCells.Columns.Add("SPG Allow");
        DataCells.Columns.Add("Canteen Days Minus");
        DataCells.Columns.Add("OT Hours");
        DataCells.Columns.Add("W.H");
        DataCells.Columns.Add("Fixed W.Days");
        DataCells.Columns.Add("NFH W.Days");
        DataCells.Columns.Add("Total Month Days");

       
        DataTable mEmployee = new DataTable();
        DataTable mLogTime = new DataTable();
        string SSQL;
        DateTime date1;
        date1 = Convert.ToDateTime(txtFrmdate.Text);
        string dat = txtTodate.Text;
        DateTime Date2 = Convert.ToDateTime(dat);

        SSQL = "select DeptName,MachineID,ExistingCode,MachineID_Encrypt,FirstName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode= '" + SessionLcode + "'";
        SSQL = SSQL + " And Wages = '" + ddlWagesType.SelectedItem.Text + "'";
        
        //if (SessionUserType == "2")
        //{
        //    SSQL = SSQL + " And IsNonAdmin='1'";
        //}

        mEmployee = objdata.ReturnMultipleValue(SSQL);


        if (mEmployee.Rows.Count < 0)
            return;
        int i1 = 0;

        for (int j1 = 0; j1 < mEmployee.Rows.Count; j1++)
        {

            AutoDTable.NewRow();
            AutoDTable.Rows.Add();
            AutoDTable.Rows[i1][0] = mEmployee.Rows[j1]["DeptName"].ToString();
            AutoDTable.Rows[i1][1] = mEmployee.Rows[j1]["MachineID"].ToString();
            AutoDTable.Rows[i1][2] = mEmployee.Rows[j1]["ExistingCode"].ToString();
            AutoDTable.Rows[i1][3] = mEmployee.Rows[j1]["MachineID_Encrypt"].ToString();
            AutoDTable.Rows[i1][4] = mEmployee.Rows[j1]["FirstName"].ToString();
            i1++;
        }


         for (int i = 0; i < AutoDTable.Rows.Count; i++)
        {
            string mID = AutoDTable.Rows[i][1].ToString();
            string OT_Week_OFF_Machine_No=AutoDTable.Rows[i][1].ToString();
            int daycount1 = (int)((Date2 - date1).TotalDays);
            int daysAdded1 = 0;
            
            double Present_Count=0;
            double Final_Count=0;
            int Total_Month_Days=0;
            while (daycount1 >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded1));

                SSQL = "select Present from LogTime_Days where MachineID='" + mID + "'";
                SSQL = SSQL + " And CompCode= '" + SessionCcode + "' And LocCode= '" + SessionLcode + "'";
                SSQL = SSQL + " And Attn_Date='" + dayy.ToString("yyyy/MM/dd") + "'";

                mLogTime = objdata.ReturnMultipleValue(SSQL);
              
                                                                
                if (mLogTime.Rows.Count > 0)
                {

                    if (mLogTime.Rows[0]["Present"].ToString() == "1.0" )
                    {

                        Present_Count = Present_Count + 1;
                    }
                    else if(mLogTime.Rows[0]["Present"].ToString() == "0.5")
                    {

                        Present_Count = Present_Count + 0.5;
                    }
                    else
                    {
                        Present_Count = Present_Count + 0;
                    }
                }

                 daycount1 -= 1;
                 daysAdded1 += 1;
       
            }
            Final_Count=Present_Count;
            Total_Month_Days=daysAdded1;

             DataCells.NewRow();
             DataCells.Rows.Add();
             DataCells.Rows[i]["MachineID"]=AutoDTable.Rows[i]["MachineID"];
             DataCells.Rows[i]["ExistingCode"] = AutoDTable.Rows[i]["ExistingCode"];
             DataCells.Rows[i]["FirstName"] = AutoDTable.Rows[i]["EmpName"];
             DataCells.Rows[i]["Days"] = Final_Count;
             DataCells.Rows[i]["H.Allowed"] = "0";
             DataCells.Rows[i]["N/FH"] = "0";          //NFH_Days_Count
             DataCells.Rows[i]["OT Days"] = "0";
             DataCells.Rows[i]["SPG Allow"] = "0";
             DataCells.Rows[i]["Canteen Days Minus"] = "0";
             DataCells.Rows[i]["OT Hours"] = "0";
             DataCells.Rows[i]["W.H"] = "0";
             DataCells.Rows[i]["Fixed W.Days"] = "0";
             DataCells.Rows[i]["NFH W.Days"] = "0";
             DataCells.Rows[i]["Total Month Days"] = Total_Month_Days;


             DataTable dept = new DataTable();
             string deptname = AutoDTable.Rows[i]["DeptName"].ToString();
             SSQL = "select DepartmentCd from [ESM-EPay]..MstDepartment where DepartmentNm = '" + deptname.ToString() + "'";
             dept = objdata.ReturnMultipleValue(SSQL);

             SSQL = "insert into [ESM-EPay]..AttenanceDetails(DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,";
             SSQL = SSQL + "CreatedDate,Years,TotalDays,Ccode,Lcode,NFh,WorkingDays,CL,AbsentDays,weekoff,";
             SSQL = SSQL + "FromDate,ToDate,Modeval,home,halfNight,FullNight,ThreeSided,OTHoursNew,Fixed_Work_Days,";
             SSQL = SSQL + "WH_Work_Days,NFH_Work_Days,NFH_Work_Days_Incentive,NFH_Work_Days_Statutory)";
             SSQL = SSQL + "values(";

             if(dept.Rows.Count > 0)
             {
                  SSQL = SSQL + "'" + dept.Rows[0]["DepartmentCd"] + "',";
             }
             else
             {
                 SSQL = SSQL + "'0',";
             }

             SSQL = SSQL + "'" + AutoDTable.Rows[i]["MachineID"].ToString() + "',";
             SSQL = SSQL + "'"+ AutoDTable.Rows[i]["ExistingCode"] +"','"+ Final_Count +"',";
             SSQL = SSQL + "'"+ date1.ToString("MMMM") +"','"+ CurrentYear +"',";
             SSQL = SSQL + "'" + DateTime.Now.ToString("yyyy/MM/dd") + "','" + DateTime.Now.Year + "',";
             SSQL = SSQL + "'"+ Total_Month_Days +"','"+ SessionCcode +"','"+ SessionLcode +"',";
             SSQL = SSQL + "'0','0','0','0','0','" + date1.ToString("yyyy/MM/dd") + "','" + Date2.ToString("yyyy/MM/dd") + "',";
             SSQL = SSQL + "'0','0','0','0','0','0','0',";
             SSQL = SSQL + "'0','0','0','0')";

             objdata.ReturnMultipleValue(SSQL);
        }

         ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Attendance Details');", true);


         //grid.DataSource = DataCells;
         //grid.DataBind();
         //string attachment = "attachment;filename=PAYROLL ATTENDANCE.xls";
         //Response.ClearContent();
         //Response.AddHeader("content-disposition", attachment);
         //Response.ContentType = "application/ms-excel";
       
         //grid.HeaderStyle.Font.Bold = true;
         //System.IO.StringWriter stw = new System.IO.StringWriter();
         //HtmlTextWriter htextw = new HtmlTextWriter(stw);
         //grid.RenderControl(htextw);

         //Response.Write("<table>");
         //Response.Write("<tr Font-Bold='true' align='center'>");
         //Response.Write("<td colspan='10'>");
         //Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
         //Response.Write("  ");
         //Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
         //Response.Write("</td>");
         //Response.Write("</tr>");
         //Response.Write("<tr Font-Bold='true' align='center'>");
         //Response.Write("<td colspan='10'>");
         //Response.Write("<a style=\"font-weight:bold\">MONTHLY ATTENDANCE REPORT</a>");
         //Response.Write("  ");
         //Response.Write("</td>");
         //Response.Write("</tr>");
         //Response.Write("<tr Font-Bold='true' align='center'>");
         //Response.Write("<td colspan='10'>");
         //Response.Write("<a style=\"font-weight:bold\">FROM DATE:" + Date1_str + "</a>");
         //Response.Write("--");
         //Response.Write("<a style=\"font-weight:bold\">TO DATE:" + Date2_str + "</a>");
         //Response.Write("</td>");
         //Response.Write("</tr>");
         //Response.Write("</table>");
         //Response.Write(stw.ToString());
         //Response.End();
         //Response.Clear();
    }

    protected void btnEmployee_Click(object sender, EventArgs e)
    {
        DataTable da = new DataTable(); 
        string SSQL = "";
        SSQL = "select distinct MachineID from Employee_Mst";
        da = objdata.ReturnMultipleValue(SSQL);

        if (da.Rows.Count > 0)
        {
            string MID = "";
            for (int k = 0; k < da.Rows.Count; k++)
            {
                MID = da.Rows[k]["MachineID"].ToString();
                if (MID == "119")
                {
                    MID = "119";
                }
                string strmsg = string.Empty;
                byte[] encode = new byte[MID.Length];
                encode = Encoding.UTF8.GetBytes(MID);
                strmsg = Convert.ToBase64String(encode);

                string SS = "";
                SS = "update Employee_Mst set MachineID_Encrypt='" + strmsg + "'where MachineID='" + MID + "'";
                SqlCommand cmd_upd1 = new SqlCommand(SS, con);
                con.Open();
                cmd_upd1.ExecuteNonQuery();
                con.Close();

            }

            for (int j = 0; j < da.Rows.Count - 1; j++)
            {
                MID = da.Rows[j]["MachineID"].ToString();
                string strmsg = string.Empty;
                byte[] encode = new byte[MID.Length];
                encode = Encoding.UTF8.GetBytes(MID);
                strmsg = Convert.ToBase64String(encode);

                string SS = "";
                SS = "update LogTime_OUT set MachineID='" + strmsg + "'where MachineID_new='" + MID + "'";
                SqlCommand cmd_upd1 = new SqlCommand(SS, con);
                con.Open();
                cmd_upd1.ExecuteNonQuery();
                con.Close();

            }
            
        }


    }
}
