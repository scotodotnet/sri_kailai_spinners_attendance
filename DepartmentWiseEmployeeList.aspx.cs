﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class DepartmentWiseEmployeeList : System.Web.UI.Page
{
    DataTable AutoDataTable = new DataTable();
    string Dept;
 
    string SSQL;
   
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    
    DataTable mEmployeeDS = new DataTable();
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Department Wise Employee Details";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();

            Dept = Request.QueryString["Dept"].ToString();

            AutoDataTable.Columns.Add("ShiftType");
            AutoDataTable.Columns.Add("TypeName");
            AutoDataTable.Columns.Add("EmpNo");
            AutoDataTable.Columns.Add("CatName");
            AutoDataTable.Columns.Add("SubCatName");
            AutoDataTable.Columns.Add("ExistingCode");
            AutoDataTable.Columns.Add("MachineID");
            AutoDataTable.Columns.Add("FirstName");
            AutoDataTable.Columns.Add("LastName");
            AutoDataTable.Columns.Add("MiddleInitial");
            AutoDataTable.Columns.Add("DeptName");
            AutoDataTable.Columns.Add("Designation");
            AutoDataTable.Columns.Add("CompanyName");
            AutoDataTable.Columns.Add("LocationName");





            SSQL = "";
            SSQL = "select isnull(ShiftType,'') as [ShiftType]";
            SSQL += ",isnull(TypeName,'') as [TypeName]";
            SSQL += ",isnull(EmpPrefix,'') as [EmpPrefix]";
            SSQL += ",isnull(EmpNo,'') as [EmpNo]";
            SSQL += ",isnull(CatName,'') as [CatName]";
            SSQL += ",isnull(SubCatName,'') as [SubCatName]";
            SSQL += ",isnull(ExistingCode,'') as [ExistingCode]";
            SSQL += ",isnull(MachineID,'') as [MachineID]";
            SSQL += ",isnull(FirstName,'') as [FirstName]";
            SSQL += ",isnull(LastName,'') as [LastName]";
            SSQL += ",isnull(MiddleInitial,'') as [MiddleInitial]";
            SSQL += ",isnull(DeptName,'') as [DeptName]";
            SSQL += ",isnull(Designation,'') as [Designation]";
            SSQL += ",isnull(EmpStatus,'') as [EmpStatus]";
            SSQL += ",isnull(IsActive,'') as [IsActive]";
            SSQL += " from employee_mst Where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and DeptName='" + Dept + "' And IsActive='Yes'";

            mEmployeeDS = objdata.ReturnMultipleValue(SSQL);

            if (mEmployeeDS.Rows.Count > 0)
            {
                for (int i = 0; i < mEmployeeDS.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[i]["ShiftType"] = mEmployeeDS.Rows[i]["ShiftType"].ToString();
                    AutoDataTable.Rows[i]["TypeName"] = mEmployeeDS.Rows[i]["TypeName"].ToString();
                    AutoDataTable.Rows[i]["EmpNo"] = mEmployeeDS.Rows[i]["EmpNo"].ToString();
                    AutoDataTable.Rows[i]["CatName"] = mEmployeeDS.Rows[i]["CatName"].ToString();
                    AutoDataTable.Rows[i]["SubCatName"] = mEmployeeDS.Rows[i]["SubCatName"].ToString();
                    AutoDataTable.Rows[i]["ExistingCode"] = mEmployeeDS.Rows[i]["ExistingCode"].ToString();
                    AutoDataTable.Rows[i]["MachineID"] = mEmployeeDS.Rows[i]["MachineID"].ToString();
                    AutoDataTable.Rows[i]["FirstName"] = mEmployeeDS.Rows[i]["FirstName"].ToString();
                    AutoDataTable.Rows[i]["LastName"] = mEmployeeDS.Rows[i]["LastName"].ToString();
                    AutoDataTable.Rows[i]["MiddleInitial"] = mEmployeeDS.Rows[i]["MiddleInitial"].ToString();
                    AutoDataTable.Rows[i]["DeptName"] = mEmployeeDS.Rows[i]["DeptName"].ToString();
                    AutoDataTable.Rows[i]["Designation"] = mEmployeeDS.Rows[i]["Designation"].ToString();
                    AutoDataTable.Rows[i]["CompanyName"] = SessionCompanyName;
                    AutoDataTable.Rows[i]["LocationName"] = SessionLocationName;

                }

            }
            ds.Tables.Add(AutoDataTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Department_Wise_Emp_Det.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            CrystalReportViewer1.ReportSource = report;
        }
    }
}
