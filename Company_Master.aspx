﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Company_Master.aspx.cs" Inherits="CompanyMaster" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

   <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableCustomer').dataTable();
        });
    </script>

    <script language="javascript" type="text/javascript">
        function ValidateEmailId() {
            var value = document.getElementById("<%=txtEmailID.ClientID %>").value;
            var atposition = value.indexOf("@");
            var dotposition = value.lastIndexOf(".");
            if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= value.length) {
                alert("Please enter a valid e-mail address");
                return false;
            }
            else {
                return true;
            }
        }
    </script>

<div>
<!-- Page Sidebar -->
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">Company Master</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
     
        
    
    <div class="page-inner">
   
                                       
            
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Company Master</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
					<div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Company Code<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                           <asp:TextBox ID="txtCompanyCode" class="form-control" runat="server" required></asp:TextBox>
                            
						</div>
						<%--<div class="col-md-1"></div>--%>
				        <label for="input-Default" class="col-sm-2 control-label">Company Name<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                              <asp:TextBox ID="txtCompanyName" class="form-control" runat="server" required></asp:TextBox>
                            
						</div>
               </div>                     
				
					<div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Location Code<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                           <asp:TextBox ID="txtLocationCode" class="form-control" runat="server" required></asp:TextBox>
                            
						</div>
						<%--<div class="col-md-1"></div>--%>
				        <label for="input-Default" class="col-sm-2 control-label">Location Name<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                              <asp:TextBox ID="txtLocationName" class="form-control" runat="server" required></asp:TextBox>
                            
						</div>
                   </div>     
					    
					<div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Address1<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                           <asp:TextBox ID="txtAddressone" class="form-control" runat="server" required></asp:TextBox>
                            
						</div>
					<%--	<div class="col-md-1"></div>--%>
				        <label for="input-Default" class="col-sm-2 control-label">Address2<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                              <asp:TextBox ID="txtAddresstwo" class="form-control" runat="server" required></asp:TextBox>
                            
						</div>
                   </div>    
                         
                    <div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">PinCode<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                           <asp:TextBox ID="txtPincode" class="form-control" runat="server" required></asp:TextBox>
                            
						</div>
					<%--	<div class="col-md-1"></div>--%>
				        <label for="input-Default" class="col-sm-2 control-label">State<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                              <asp:TextBox ID="txtState" class="form-control" runat="server" required></asp:TextBox>
                            
						</div>
                   </div>  
                   
                    <div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Mobile Number<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                           <asp:TextBox ID="txtPhoneNo" class="form-control" runat="server" required MaxLength="10"  onkeypress="return NumberOnly()"></asp:TextBox>
                            
						</div>
					<%--	<div class="col-md-1"></div>--%>
				        <label for="input-Default" class="col-sm-2 control-label">Email ID<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                              <asp:TextBox ID="txtEmailID" type="email" class="form-control" runat="server" OnClientClick="javascript:return ValidateEmailId();" required></asp:TextBox>
                            
						</div>
                   </div>  
                   
                   <div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Establishment Code<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                           <asp:TextBox ID="txtEstablishmentCode" class="form-control" runat="server" required></asp:TextBox>
                            
						</div>
						
						  <label for="input-Default" class="col-sm-2 control-label">UserDef I<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                              <asp:TextBox ID="txtUserDefOne" class="form-control" runat="server" required></asp:TextBox>
                            
						</div>
						
						
                   </div>
                   
                    
                   <div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">UserDef II<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                           <asp:TextBox ID="txtUserDefTwo" class="form-control" runat="server" required></asp:TextBox>
                            
						</div>
						
						  <label for="input-Default" class="col-sm-2 control-label">UserDef III<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                              <asp:TextBox ID="txtUserDefThree" class="form-control" runat="server" required></asp:TextBox>
                       </div>
				   </div>
                   
                   
                   
                         <div class="form-group row"></div>
					
					<!-- Button start -->
						<div class="form-group row">
						</div>
                            <div class="txtcenter">
                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="SAVE" onclick="btnSave_Click" 
                                    />
                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                  <%--  <asp:Button ID="Button12" class="btn btn-danger" runat="server" Text="CLEAR" />--%>
                                <asp:LinkButton ID="btnClear" runat="server" class="btn btn-danger" 
                                    onclick="btnClear_Click">CLEAR</asp:LinkButton>
                    </div>
                    <!-- Button End -->	
					     
					     
					     <div class="panel-body">
                             <div class="table-responsive">
    
                              <asp:Repeater ID="rptrCustomer" runat="server" onitemcommand="Repeater1_ItemCommand">
                        <HeaderTemplate>
                
                  <table id="tableCustomer" class="display table" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                                                   
                                                    <th>Company Code</th>
                                                    <th>Location Code</th>  
                                                    <th>Location Name</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    
                  
                    
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "CompCode")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "LocCode")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "LocName")%>
                    </td>
                    <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("SNo") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("SNo") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
             
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
                        </div>
					     
					     
					     
					     
					     
					     
					     
					     
					     
					     
					     
					     
					     
					     
					    
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 <!-- Dashboard start -->
 

 
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Company Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
                        
                        
                        
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>
</div>

       
</asp:Content>

