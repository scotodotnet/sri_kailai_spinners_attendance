﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class DeptSalaryConsolidateReport : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionCompanyName;
    string SessionLocationName;
    string SessionUserType;
    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];

    DataTable mDataSet = new DataTable();
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str = null;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    Int32 time_Check_dbl = 0;
    string Total_Time_get = "";

    DataTable Payroll_DS = new DataTable();
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    DataSet ds = new DataSet();
    string SSQL = "";
    string mIpAddress_IN;
    string mIpAddress_OUT;
    DateTime date1;
    DateTime date11;
    DateTime date12;


    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Dept Salary Consolidate";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();


            // ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            // ddlShiftType = Request.QueryString["ddlShiftType"].ToString();

            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("Dept");
            AutoDTable.Columns.Add("Type");
            AutoDTable.Columns.Add("Shift");
            AutoDTable.Columns.Add("EmpCode");
            AutoDTable.Columns.Add("ExCode");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("TimeIN");
            AutoDTable.Columns.Add("TimeOUT");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Category");
            AutoDTable.Columns.Add("SubCategory");
            AutoDTable.Columns.Add("TotalMIN");
            AutoDTable.Columns.Add("GrandTOT");
            AutoDTable.Columns.Add("DayWages");
            AutoDTable.Columns.Add("BaseSalary");
            AutoDTable.Columns.Add("OT");
            AutoDTable.Columns.Add("Nominee");
            AutoDTable.Columns.Add("Leader");
            AutoDTable.Columns.Add("Gender");


            DataCells.Columns.Add("CompanyName");
            DataCells.Columns.Add("LocationName");
            DataCells.Columns.Add("ShiftDate");
            DataCells.Columns.Add("SNo");
            DataCells.Columns.Add("Dept");
            DataCells.Columns.Add("Type");
            DataCells.Columns.Add("Shift");
            DataCells.Columns.Add("Category");
            DataCells.Columns.Add("CatName");
            DataCells.Columns.Add("SubCategory");
            DataCells.Columns.Add("EmpCode");
            DataCells.Columns.Add("ExCode");
            DataCells.Columns.Add("Name");
            DataCells.Columns.Add("TimeIN");
            DataCells.Columns.Add("TimeOUT");
            DataCells.Columns.Add("MachineID");
            DataCells.Columns.Add("PrepBy");
            DataCells.Columns.Add("PrepDate");
            DataCells.Columns.Add("TotalMIN");
            DataCells.Columns.Add("GrandTOT");
            DataCells.Columns.Add("DayWages");
            DataCells.Columns.Add("Wages_Type");
            DataCells.Columns.Add("BaseSalary");
            DataCells.Columns.Add("OT");
            DataCells.Columns.Add("Grade");
            DataCells.Columns.Add("Gender");


            SSQL = "";
            SSQL = "Select IPAddress,IPMode from IPAddress_Mst Where Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";

            mDataSet = objdata.ReturnMultipleValue(SSQL);

            if (mDataSet.Rows.Count > 0)
            {
                for (int iRow = 0; iRow < mDataSet.Rows.Count - 1; iRow++)
                {
                    if ((mDataSet.Rows[iRow]["IPMode"].ToString()) == "IN")
                    {
                        mIpAddress_IN = mDataSet.Rows[iRow]["IPAddress"].ToString();
                    }
                    else if ((mDataSet.Rows[iRow]["IPMode"].ToString()) == "OUT")
                    {
                        mIpAddress_OUT = mDataSet.Rows[iRow]["IPAddress"].ToString();
                    }
                }
            }

            DataTable mLocalDS = new DataTable();
            string qry = "";

            qry = "";
            qry = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
            qry = qry + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
            qry += " from Shift_Mst Where CompCode='" + SessionCcode + "'";
            qry += " And LocCode='" + SessionLcode + "'";

            //'qry &= " And shiftDesc='" & Trim("All") & "'"

            //qry &= " And ShiftDesc like '" & "" & "%'" 'Shift%'"
            //qry &= " Order By shiftDesc"

            if (ShiftType1 != "ALL")
            {
                SSQL += " And shiftDesc='" + ShiftType1 + "'";
            }
            SSQL += " And ShiftDesc like '" + ddlShiftType + "%'";
            //Shift%'"
            SSQL += " Order By shiftDesc";

            mLocalDS = objdata.ReturnMultipleValue(qry);

            string ShiftType = "";
            string ng = string.Format(Date, "dd-MM-yyyy");
            DateTime date1 = Convert.ToDateTime(ng);


            int mStartINRow = 0;
            int mStartOUTRow = 0;

      

                DataTable mEmployeeDS = new DataTable();

                SSQL = "";
                SSQL = "select isnull(MachineID_Encrypt,'') as [MachineID],isnull(DeptName,'') as [DeptName]";
                SSQL = SSQL + ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
                SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName] ,BaseSalary";
                SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName],isnull(OTEligible,'') as OT,";
                SSQL = SSQL + " isnull(Nominee,'') as Grade,isnull(Gender,'') as Gender,MachineID_Encrypt from Employee_Mst Where Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'";

                mEmployeeDS = objdata.ReturnMultipleValue(SSQL);



                if (mEmployeeDS.Rows.Count > 0)
                {
                    int iRow2 = 0;
                   
                    
                    for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                    {
                        
                            AutoDTable.NewRow();
                            AutoDTable.Rows.Add();

                            AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"].ToString();
                            AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"].ToString();
                            AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"].ToString();
                            AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"].ToString();
                            AutoDTable.Rows[iRow2][9] = mEmployeeDS.Rows[iRow1]["MachineID_Encrypt"].ToString();
                            AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"].ToString();
                            AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"].ToString();
                            AutoDTable.Rows[iRow2][15] = mEmployeeDS.Rows[iRow1]["BaseSalary"].ToString();
                            AutoDTable.Rows[iRow2][16] = mEmployeeDS.Rows[iRow1]["OT"].ToString();
                            AutoDTable.Rows[iRow2][17] = mEmployeeDS.Rows[iRow1]["Grade"].ToString();
                            AutoDTable.Rows[iRow2][18] = mEmployeeDS.Rows[iRow1]["Gender"].ToString();



                            //ealert Wages
                            SSQL = "select BaseSalary as DaySalary,Wages as WagesType ,CatName as CatName from Employee_Mst where";
                            SSQL = SSQL + " ExistingCode='" + AutoDTable.Rows[iRow2][5].ToString() + "'";
                            Payroll_DS = objdata.ReturnMultipleValue(SSQL);

                            //Day Wages And Type Get
                            if (Payroll_DS.Rows.Count != 0)
                            {
                                //AutoDTable.Rows[iRow2][14] = Payroll_DS.Rows[0]["WagesType"].ToString();
                                if (Payroll_DS.Rows[0]["CatName"].ToString() != "1" & Payroll_DS.Rows[0]["CatName"].ToString() != "2")
                                {
                                    AutoDTable.Rows[iRow2][15] = Payroll_DS.Rows[0]["DaySalary"].ToString();
                                }
                                else
                                {
                                    AutoDTable.Rows[iRow2][15] = "0.00";
                                }
                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][14] = "0.00";
                                AutoDTable.Rows[iRow2][15] = "";
                            }
                            iRow2 = iRow2 + 1;
                            //iRow1 = iRow1 + 1;
                        }
                  
                    for (int irow2 = 0; irow2 < AutoDTable.Rows.Count; irow2++)
                    {
                        if (string.IsNullOrEmpty(AutoDTable.Rows[irow2][14].ToString()))
                        {
                            AutoDTable.Rows[irow2][14] = "0.00";
                            AutoDTable.Rows[irow2][15] = "";
                        }
                    }

                }
                //fg.Cols = fg.Cols + 1
                for (int iRow = 0; iRow < AutoDTable.Rows.Count; iRow++)
                {
                    DataTable mLocalDS_INTAB = new DataTable();
                    DataTable mLocalDS_OUTTAB = new DataTable();
                    string Date_Value_Str = null;
                    string Date_value_str1 = null;
                    string Time_IN_Str = "";
                    string Time_Out_Str = "";
                    Int32 time_Check_dbl = 0;
                    string Total_Time_get = "";
                    string Emp_Total_Work_Time_1 = "00:00";
                   

                    //DateTime dtime1 = dtime.AddDays(1);

                    DateTime dtime = date1.AddDays(0);
                    DateTime dtime1 = dtime.AddDays(1);

                    Date_Value_Str = dtime.ToString("yyyy/MM/dd");
                    Date_value_str1 = dtime1.ToString("yyyy/MM/dd");


                    //Shift Check
                    string Final_InTime = "";
                    string Final_OutTime = "";
                    DataTable Shift_DS_Change = new DataTable();
                    bool Shift_Check_blb = false;
                    string Shift_Start_Time_Change = null;
                    string Shift_End_Time_Change = null;
                    DateTime ShiftdateStartIN_Change = default(DateTime);
                    DateTime ShiftdateEndIN_Change = default(DateTime);
                    DateTime EmpdateIN_Change = default(DateTime);
                    string Final_Shift = "";

                        SSQL = "";
                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + AutoDTable.Rows[iRow][9].ToString() + "'";
                        SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_value_str1 + " " + "02:00' Order by TimeIN ASC";
                        mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);
                        if (mLocalDS_INTAB.Rows.Count != 0)
                        {

                            Final_InTime = mLocalDS_INTAB.Rows[0][0].ToString();
                            //Check With IN Time Shift
                            SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                            Shift_DS_Change = objdata.ReturnMultipleValue(SSQL);
                            Shift_Check_blb = false;
                            for (int K = 0; K <= Shift_DS_Change.Rows.Count - 1; K++)
                            {
                                Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
                                if (Shift_DS_Change.Rows[K]["EndIN_Days"].ToString() == "1")
                                {
                                    Shift_End_Time_Change = Date_value_str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                }
                                else
                                {
                                    Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                }

                                ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                if (EmpdateIN_Change >= ShiftdateStartIN_Change & EmpdateIN_Change <= ShiftdateEndIN_Change)
                                {
                                    Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                    Shift_Check_blb = true;
                                    AutoDTable.Rows[iRow][3] = Final_Shift;
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                            }


                        }

                        
                        TimeSpan ts = new TimeSpan();
                        //DateTime date11 = System.Convert.ToDateTime(AutoDTable.Rows[iRow][7]);
                        //DateTime date12 = System.Convert.ToDateTime(AutoDTable.Rows[iRow][8]);
                        //TimeSpan ts = new TimeSpan();
                        //ts = date12.Subtract(date11);
                        //ts = date12.Subtract(date11);

                        SSQL = "";

                        if (Final_Shift == "SHIFT1")
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + AutoDTable.Rows[iRow][9].ToString() + "'";
                            SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "00:00' And TimeOUT <='" + Date_Value_Str + " " + "23:00' Order by TimeOUT desc";
                        }
                        else
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + AutoDTable.Rows[iRow][9].ToString() + "'";
                            SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_value_str1 + " " + "10:00' Order by TimeOUT desc";
                        }

                        
                        mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);
                        string[] Time_Error_Spilit;
                        if (mLocalDS_INTAB.Rows.Count > 1)
                        {
                            for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

                                if (mLocalDS_OUTTAB.Rows.Count > tin)
                                {
                                    Time_Out_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                                }
                                else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                                {
                                    Time_Out_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                                }
                                else
                                {
                                    Time_Out_Str = "00:00";
                                }
                                TimeSpan ts4 = new TimeSpan();

                                Time_Error_Spilit = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Error_Spilit[0], 1) == "-" | Left_Val(Time_Error_Spilit[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                                if (mLocalDS.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "00:00";
                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                                }


                                //Emp_Total_Work_Time
                                if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                                {
                                    time_Check_dbl = time_Check_dbl;
                                }
                                else
                                {
                                    DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                                    DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);


                                    TimeSpan ts1 = new TimeSpan();
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts = date4.Subtract(date3);
                                    Total_Time_get = ts1.Hours.ToString();
                                    //& ":" & Trim(ts.Minutes)
                                    ts4 = ts4.Add(ts1);
                                    //OT Time Get
                                    Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                    //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {
                                        date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                        ts = date4.Subtract(date3);
                                        ts = date4.Subtract(date3);

                                        Total_Time_get = ts.Hours.ToString();
                                        //& ":" & Trim(ts.Minutes)
                                        time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                        //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                        Emp_Total_Work_Time_1 = ts.Hours.ToString() + ":" + ts.Minutes.ToString();
                                    }
                                    else
                                    {
                                        time_Check_dbl = time_Check_dbl + Convert.ToInt32(Total_Time_get);
                                    }
                                }
                            }
                            AutoDTable.Rows[iRow][7] = Time_IN_Str;
                            AutoDTable.Rows[iRow][8] = Time_Out_Str;
                        }
                        else
                        {
                            TimeSpan ts4 = new TimeSpan();
                            Time_Error_Spilit = Emp_Total_Work_Time_1.Split(':');
                            if (Left_Val(Time_Error_Spilit[0], 1) == "-" | Left_Val(Time_Error_Spilit[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS_INTAB.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                                date11 = System.Convert.ToDateTime(Time_IN_Str);
                                AutoDTable.Rows[iRow][7] = Time_IN_Str;
                                

                            }
                            for (int tout = 0; tout < mLocalDS_OUTTAB.Rows.Count; tout++)
                            {
                                if (mLocalDS_OUTTAB.Rows.Count <= 0)
                                {
                                    Time_Out_Str = "";
                                }
                                else
                                {
                                    Time_Out_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                                    date12 = System.Convert.ToDateTime(Time_Out_Str);
                                    AutoDTable.Rows[iRow][8] = Time_Out_Str;
                                }

                            }
                            //Emp_Total_Work_Time
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = 0;
                            }
                            else
                            {
                                DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts1 = new TimeSpan();
                                ts1 = date4.Subtract(date3);
                                ts = date4.Subtract(date3);

                                Total_Time_get = ts1.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                ts1 = ts1.Add(ts);
                                //OT Time Get
                                Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date12 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date4.Subtract(date3);
                                    ts = date4.Subtract(date3);
                                    Total_Time_get = ts.Hours.ToString();
                                    //& ":" & Trim(ts.Minutes)
                                    time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                    //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                    Emp_Total_Work_Time_1 = ts1.Hours.ToString() + ":" + ts1.Minutes.ToString();
                                }
                                else
                                {
                                    time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                }
                            }
                        }

                        //End
                        if (Left_Val(ts.Hours.ToString(), 1) == "-")
                        {
                            date12 = System.Convert.ToDateTime(AutoDTable.Rows[iRow][8]).AddDays(1);
                            ts = date12.Subtract(date11);
                            ts = date12.Subtract(date11);
                            AutoDTable.Rows[iRow][12] = ts.Hours.ToString() + ":" + ts.Minutes.ToString();
                        }
                        else
                        {
                            AutoDTable.Rows[iRow][12] = ts.Hours.ToString() + ":" + ts.Minutes.ToString();
                        }

                        AutoDTable.Rows[iRow][13] = Emp_Total_Work_Time_1;
                        //fg.set_TextMatrix(iRow, fg.Cols - 1, Trim(ts.Hours) & ":" & Trim(ts.Minutes))
                   
                }
                int k = 0;
                for (int i = 0; i < AutoDTable.Rows.Count; i++)
                {
                    DataTable da_Insert = new DataTable();
                    DataCells.NewRow();
                    DataCells.Rows.Add();
                    date1 = Convert.ToDateTime(Date);
                    DataCells.Rows[k]["CompanyName"] = SessionCcode;
                    DataCells.Rows[k]["LocationName"] = SessionLcode;
                    DataCells.Rows[k]["ShiftDate"] = date1.ToString("dd/MM/yyyy");
                    DataCells.Rows[k]["Dept"] = AutoDTable.Rows[i]["Dept"].ToString();
                    DataCells.Rows[k]["Type"] = AutoDTable.Rows[i]["Type"];
                    DataCells.Rows[k]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                    DataCells.Rows[k]["Category"] = AutoDTable.Rows[i]["Category"].ToString();

                    DataCells.Rows[k]["CatName"] = AutoDTable.Rows[i]["Category"].ToString();

                    DataCells.Rows[k]["SubCategory"] = AutoDTable.Rows[i]["SubCategory"].ToString();
                    DataCells.Rows[k]["EmpCode"] = AutoDTable.Rows[i]["EmpCode"].ToString();
                    DataCells.Rows[k]["ExCode"] = AutoDTable.Rows[i]["ExCode"].ToString();
                    DataCells.Rows[k]["Name"] = AutoDTable.Rows[i]["Name"].ToString();
                    DataCells.Rows[k]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                    DataCells.Rows[k]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                    DataCells.Rows[k]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                    DataCells.Rows[k]["PrepBy"] = "user";
                    DataCells.Rows[k]["PrepDate"] = Date;
                    DataCells.Rows[k]["TotalMIN"] = AutoDTable.Rows[i]["TotalMIN"].ToString();
                    DataCells.Rows[k]["GrandTOT"] = AutoDTable.Rows[i]["GrandTOT"].ToString();

                    DataCells.Rows[k]["DayWages"] = AutoDTable.Rows[i]["DayWages"].ToString();
                    DataCells.Rows[k]["Wages_Type"] = AutoDTable.Rows[i]["BaseSalary"].ToString();
                    DataCells.Rows[k]["BaseSalary"] = AutoDTable.Rows[i]["DayWages"].ToString();
                    DataCells.Rows[k]["OT"] = AutoDTable.Rows[i]["OT"].ToString();
                    DataCells.Rows[k]["Gender"] = AutoDTable.Rows[i]["Leader"].ToString();


                    k = k + 1;

                    //SSQL="delete from Dept_Salary where CompanyName='"+SessionCcode+"',LocationName='"+SessionLcode+"'"+
                    //     " "


                    //SSQL = "insert into Dept_Salary(CompanyName,LocationName,ShiftDate,Dept,Type,Shift,Category,SubCategory," +
                    //     "  EmpCode,ExCode,Name,TimeIN,TimeOUT,MachineID,PrepBy,PrepDate,TotalMIN,GrandTOT,DayWages,Wages_Type,BaseSalary,OT)" +
                    //     " values('" + SessionCcode + "','" + SessionLcode + "','" + Date + "','" + AutoDTable.Rows[i]["Dept"] + "', " +
                    //     " '" + AutoDTable.Rows[i]["Type"] + "','" + AutoDTable.Rows[i]["Shift"].ToString() + "','" + AutoDTable.Rows[i]["Category"].ToString() + "'," +
                    //     " '" + AutoDTable.Rows[i]["SubCategory"].ToString() + "','" + AutoDTable.Rows[i]["EmpCode"].ToString() + "','" + AutoDTable.Rows[i]["ExCode"].ToString() + "'," +
                    //     " '" + AutoDTable.Rows[i]["Name"].ToString() + "','" + AutoDTable.Rows[i]["TimeIN"].ToString() + "','" + AutoDTable.Rows[i]["TimeOUT"].ToString() + "'," +
                    //     " '" + AutoDTable.Rows[i]["MachineID"].ToString() + "','user','" + Date + "','" + AutoDTable.Rows[i]["TotalMIN"].ToString() + "','" + AutoDTable.Rows[i]["GrandTOT"].ToString() + "'," +
                    //     " '" + AutoDTable.Rows[i]["DayWages"].ToString() + "','" + AutoDTable.Rows[i]["BaseSalary"].ToString() + "','" + AutoDTable.Rows[i]["DayWages"].ToString() + "','" + AutoDTable.Rows[i]["OT"].ToString() + "')";
                    //da_Insert = objdata.ReturnMultipleValue(SSQL);

                }

                DataTable Da_Delete = new DataTable();

                SSQL = "delete from Dept_Salary";
                Da_Delete = objdata.ReturnMultipleValue(SSQL);





                ds.Tables.Add(DataCells);
                DataTable da_Dept = new DataTable();
                DataTable Da_Values = new DataTable();
                for (int S = 0; S < DataCells.Rows.Count; S++)
                {
                    DataTable da_Insert = new DataTable();

                    if (DataCells.Rows[S]["TimeOUT"].ToString() == "")
                    {
                        string str = "kalyan";
                    }
                  else{

                        SSQL = "insert into Dept_Salary(CompanyName,LocationName,ShiftDate,Dept,Type,Shift,Category,SubCategory," +
                             "  EmpCode,ExCode,Name,TimeIN,TimeOUT,MachineID,PrepBy,PrepDate,TotalMIN,GrandTOT,Wages_Type,BaseSalary,OT,Gender)" +
                             " values('" + SessionCcode + "','" + SessionLcode + "',convert(DateTime,'" + Date + "',103),'" + DataCells.Rows[S]["Dept"] + "', " +
                             " '" + DataCells.Rows[S]["Type"] + "','" + DataCells.Rows[S]["Shift"].ToString() + "','" + DataCells.Rows[S]["Category"].ToString() + "'," +
                             " '" + DataCells.Rows[S]["SubCategory"].ToString() + "','" + DataCells.Rows[S]["EmpCode"].ToString() + "','" + DataCells.Rows[S]["ExCode"].ToString() + "'," +
                             " '" + DataCells.Rows[S]["Name"].ToString() + "','" + DataCells.Rows[S]["TimeIN"].ToString() + "','" + DataCells.Rows[S]["TimeOUT"].ToString() + "'," +
                             " '" + DataCells.Rows[S]["MachineID"].ToString() + "','user','" + Date + "','" + DataCells.Rows[S]["TotalMIN"].ToString() + "','" + DataCells.Rows[S]["GrandTOT"].ToString() + "'," +
                             " '" + DataCells.Rows[S]["BaseSalary"].ToString() + "','" + DataCells.Rows[S]["DayWages"].ToString() + "','" + DataCells.Rows[S]["OT"].ToString() + "'," +
                             " '" + DataCells.Rows[S]["Gender"].ToString() + "')";
                    
                    da_Insert = objdata.ReturnMultipleValue(SSQL);
                  }
                }

                SSQL = "select * from Dept_Salary";
                da_Dept = objdata.ReturnMultipleValue(SSQL);

                if (da_Dept.Rows.Count != 0)
                {

                    SSQL = "SELECT ShiftDate,dept AS Dept,[Shift1], [Shift2], [Shift3]" +
                         " FROM (SELECT ShiftDate,Dept, Shift FROM Dept_Salary) AS SourceTable" +
                         " PIVOT(count(Shift)FOR Shift IN ([Shift1], [Shift2], [Shift3])" +
                         " ) AS PivotTable;";
                    Da_Values = objdata.ReturnMultipleValue(SSQL);


                }
                DataTable da_S_Male_IN = new DataTable();
                DataTable da_S_Male_Out = new DataTable();
                int S_Male_In = 0;
                int S_Male_Out = 0;
                int S_Female_IN = 0;
                int S_Female_Out = 0;
                //Absent Varible
                DataTable da_A_Male_IN = new DataTable();
                DataTable da_A_Male_OUT = new DataTable();
                DataTable da_A_Female_IN = new DataTable();
                DataTable da_A_Female_OUT = new DataTable();

            
                DataTable da_P_Female_IN = new DataTable();
                DataTable da_P_Female_OUT = new DataTable();
                DataTable da__P_Male_IN = new DataTable();
                DataTable da_P_Male_OUT = new DataTable(); 



                int A_Male_In = 0;
                int A_Male_Out = 0;
                int A_Female_In = 0;
                int A_Female_Out = 0;
                int P_Male_IN = 0;
                int P_Male_OUT = 0;
                int P_Female_IN = 0;
                int P_Female_OUT = 0;
             

                // Over All strength  Male (INSIDER)
                SSQL = "select COUNT(Gender) from Employee_Mst where SubCatName='INSIDER' and IsActive='Yes' and Gender='Male' ";
                da_S_Male_IN = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da_S_Male_IN.Rows.Count != 0)
                {
                    S_Male_In = Convert.ToInt32(da_S_Male_IN.Rows[0][0]);
                }
                else
                {
                    S_Male_In = 0;
                }

                // Over All strength  Male (OUTSIDER)
                SSQL = "select COUNT(Gender) from Employee_Mst where SubCatName='OUTSIDER' and IsActive='Yes' and Gender='Male' ";
                da_S_Male_Out = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da_S_Male_IN.Rows.Count != 0)
                {
                    S_Male_Out = Convert.ToInt32(da_S_Male_Out.Rows[0][0]);
                }
                else
                {
                    S_Male_Out = 0;
                }


                // Over All strength  FeMale (INSIDER)
                SSQL = "select COUNT(Gender) from Employee_Mst where SubCatName='INSIDER' and IsActive='Yes' and Gender='FeMale' ";
                da_S_Male_IN = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da_S_Male_IN.Rows.Count != 0)
                {
                    S_Female_IN = Convert.ToInt32(da_S_Male_IN.Rows[0][0]);
                }
                else
                {
                    S_Female_IN = 0;
                }

                // Over All strength  FeMale (OUTSIDER)
                SSQL = "select COUNT(Gender) from Employee_Mst where SubCatName='OUTSIDER' and IsActive='Yes' and Gender='FeMale' ";
                da_S_Male_Out = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da_S_Male_IN.Rows.Count != 0)
                {
                    S_Female_Out = Convert.ToInt32(da_S_Male_Out.Rows[0][0]);
                }
                else
                {
                    S_Female_Out = 0;

                }



                // Absent  Male (INSIDER)
                SSQL = "select COUNT(Gender) from Dept_Salary where SubCategory='INSIDER' and Gender='Male' and TotalMIN='0:0' ";
                da_A_Male_IN = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da_S_Male_IN.Rows.Count != 0)
                {
                    A_Male_In = Convert.ToInt32(da_A_Male_IN.Rows[0][0]);
                }
                else
                {
                    A_Male_In = 0;
                }

                // Absent  Male (OUTSIDER)
                SSQL = "select COUNT(Gender) from Dept_Salary where SubCategory='OUTSIDER'  and Gender='Male' and TotalMIN='0:0' ";
                da_A_Male_OUT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da_A_Male_OUT.Rows.Count != 0)
                {
                    A_Male_Out = Convert.ToInt32(da_A_Male_OUT.Rows[0][0]);
                }
                else
                {
                    A_Male_Out = 0;
                }

                // Absent  FeMale (INSIDER)
                SSQL = "select COUNT(Gender) from Dept_Salary where SubCategory='INSIDER' and Gender='FeMale' and TotalMIN='0:0' ";
                da_A_Female_IN = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da_A_Female_IN.Rows.Count != 0)
                {
                    A_Female_In = Convert.ToInt32(da_A_Female_IN.Rows[0][0]);
                }
                else
                {
                    A_Male_In = 0;
                }

                // Absent  FeMale (OUTSIDER)
                SSQL = "select COUNT(Gender) from Dept_Salary where SubCategory='OUTSIDER'  and Gender='FeMale' and TotalMIN='0:0'";
                da_A_Female_OUT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da_A_Female_OUT.Rows.Count != 0)
                {
                    A_Female_Out = Convert.ToInt32(da_A_Female_OUT.Rows[0][0]);
                }
                else
                {
                    A_Female_Out = 0;
                }



                // Present  Male (INSIDER)
                SSQL = "select COUNT(Gender) from Dept_Salary where SubCategory='INSIDER' and Gender='Male' and TotalMIN<>'0:0'  and Shift<>'' ";
                da__P_Male_IN = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da__P_Male_IN.Rows.Count != 0)
                {
                    P_Male_IN  = Convert.ToInt32(da__P_Male_IN.Rows[0][0]);
                }
                else
                {
                    P_Male_IN = 0;
                }

                // Present  Male (OUTSIDER)
                SSQL = "select COUNT(Gender) from Dept_Salary where SubCategory='OUTSIDER'  and Gender='Male' and TotalMIN<>'0:0'  and Shift<>'' ";
                da_P_Male_OUT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da_P_Male_OUT.Rows.Count != 0)
                {
                    P_Male_OUT  = Convert.ToInt32(da_P_Male_OUT.Rows[0][0]);
                }
                else
                {
                    P_Male_OUT = 0;
                }

                // Present  FeMale (INSIDER)
                SSQL = "select COUNT(Gender) from Dept_Salary where SubCategory='INSIDER' and Gender='FeMale' and TotalMIN<>'0:0'  and Shift<>'' ";
                da_P_Female_IN = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da_P_Female_IN.Rows.Count != 0)
                {
                    P_Female_IN = Convert.ToInt32(da_P_Female_IN.Rows[0][0]);
                }
                else
                {
                    P_Female_IN = 0;
                }

                // Present  FeMale (OUTSIDER)
                SSQL = "select COUNT(Gender) from Dept_Salary where SubCategory='OUTSIDER'  and Gender='FeMale' and TotalMIN<>'0:0'  and Shift<>''";
                da_P_Female_OUT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da_P_Female_OUT.Rows.Count != 0)
                {
                    P_Female_OUT = Convert.ToInt32(da_P_Female_OUT.Rows[0][0]);
                }
                else
                {
                    P_Female_OUT = 0;
                }







                ReportDocument report = new ReportDocument();
                if (Da_Values.Rows.Count != 0)
                {
                    ds.Tables.Add(Da_Values);
                    report.Load(Server.MapPath("crystal/Dept.rpt"));
                    //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[1]);

                    //report.SetParameterValue("@S_Male_In", S_Male_In);

                    report.DataDefinition.FormulaFields["S_Male_In"].Text = "'" + S_Male_In + "'";
                    report.DataDefinition.FormulaFields["S_Male_Out"].Text = "'" + S_Male_Out + "'";
                    report.DataDefinition.FormulaFields["S_Female_IN"].Text = "'" + S_Female_IN + "'";
                    report.DataDefinition.FormulaFields["S_Female_Out"].Text = "'" + S_Female_Out + "'";

                    report.DataDefinition.FormulaFields["P_Male_IN"].Text = "'" + P_Male_IN + "'";
                    report.DataDefinition.FormulaFields["P_Male_OUT"].Text = "'" + P_Male_OUT + "'";
                    report.DataDefinition.FormulaFields["P_Female_IN"].Text = "'" + P_Female_IN + "'";
                    report.DataDefinition.FormulaFields["P_Female_OUT"].Text = "'" + P_Female_OUT + "'";


                    report.DataDefinition.FormulaFields["A_Male_In"].Text = "'" + A_Male_In + "'";
                    report.DataDefinition.FormulaFields["A_Male_Out"].Text = "'" + A_Male_Out + "'";
                    report.DataDefinition.FormulaFields["A_Female_In"].Text = "'" + A_Female_In + "'";
                    report.DataDefinition.FormulaFields["A_Female_Out"].Text = "'" + A_Female_Out + "'";


                }
              //  report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;

           
        }
    }

    public void Absent_Functrion()

    {
        DataTable da_Check = new DataTable();
        SSQL = "Select distinct CompCode,LocCode,MachineID,ExistingCode,EmpNo from Employee_Mst where IsActive='Yes'";
        da_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (da_Check.Rows.Count != 0)
        {
            for(int k=0;k<da_Check.Rows.Count ;k++)
            {
                string MachineID = da_Check.Rows[k][""].ToString();
            }

        }
    }
}


