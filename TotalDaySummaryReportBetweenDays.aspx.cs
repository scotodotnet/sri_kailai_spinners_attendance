﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;


public partial class TotalDaySummaryReportBetweenDays : System.Web.UI.Page
{
    string SessionCompanyName; string SessionLocationName; string SessionAdmin; string SessionCcode; string SessionLcode;
    string SessionUserType; string Date_Value_Str1; BALDataAccess objdata = new BALDataAccess();
    string SSQL = ""; DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable(); DataTable MEmployeeDS = new DataTable();
    DateTime date1; DateTime Date2; int intK; string FromDate; string ToDate;
    string Date_Value_Str; string Date_value_str1; string[] Time_Minus_Value_Check; string WagesType;
    string ColumnName = ""; string ModeType = ""; string ShiftType1 = ""; string Date = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    DateTime date2 = new DateTime(); SqlConnection con; string ss = ""; string Rname;
    DateTime ShiftdateStartIN; DateTime ShiftdateEndIN; DateTime InTime_Check; DateTime InToTime_Check;
    DataTable mLocalDS = new DataTable(); DataTable mEmployeeDS = new DataTable();
    DataTable mLocalDS_INTAB = new DataTable(); DataTable DS_InTime = new DataTable();
    string Final_InTime; string mIpAddress_IN; string mIpAddress_OUT; string Time_IN_Str; string Time_OUT_Str;
    string Shift_Start_Time; string Shift_End_Time; string Final_Shift; string Total_Time_get;
    string[] OThour_Str; DataSet ds = new DataSet(); int time_Check_dbl; DataTable mdatatable = new DataTable();
    DataTable LocalDT = new DataTable(); DataTable Shift_DS = new DataTable(); TimeSpan InTime_TimeSpan;
    string To_Time_Str = ""; int OT_Hour; int OT_Min; int OT_Time; string OT_Hour1 = ""; string mUser = "";
    string Datestr = ""; string Datestr1 = ""; string[] Time_Split_Str; string Time_IN_Get;
    Double Totol_Hours_Check = 0; int Random_No_Get; int Random_No_Fixed; Double Totol_Hours_Check_1 = 8.2;
    Boolean Shift_Check_blb = false; string OT_Week_OFF_Machine_No; string halfPresent = "0";
    bool isPresent = false; double Worked_Days_Count = 0; string InMachine_IP = ""; string From_Time_Str = "";
    DateTime EmpdateIN; DataTable DS_Time = new DataTable(); string Time_Out_Update;
    DataTable mLocalDS_OUTTAB = new DataTable(); string ddlShiftType = ""; Boolean Late_Errflag = false;
    string WorkedDays = ""; string Day;
    string AbsentDays = "";
    string ImproperPunch = "";
    string LateINDays = "";
    string ManualDays = "";

   double TotalPresentDays = 0;
   double TotalAbsentDays = 0;


    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Muster Report Between Dates";
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();



            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();


            AutoDTable.Columns.Add("EmployeeNo");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("ExistingCode");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("FirstName");
            AutoDTable.Columns.Add("Designation");

            AutoDTable.Columns.Add("TimeIN");
            AutoDTable.Columns.Add("TimeOUT");
            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("WorkedDays");
            AutoDTable.Columns.Add("AbsentDays");
            AutoDTable.Columns.Add("ImproparePunch");
            AutoDTable.Columns.Add("LateINDays");
            AutoDTable.Columns.Add("ManualDays");
            AutoDTable.Columns.Add("TotalPresentDays");
            AutoDTable.Columns.Add("TotalAbsentDays");
            AutoDTable.Columns.Add("LateIn");

            mEmployeeDT.Columns.Add("SNo");
            mEmployeeDT.Columns.Add("EmployeeNo");
            mEmployeeDT.Columns.Add("ExistingCode");
            mEmployeeDT.Columns.Add("Name");
            mEmployeeDT.Columns.Add("DeptName");


           
            mEmployeeDT.Columns.Add("WorkedDays");
            mEmployeeDT.Columns.Add("AbsentDays");
            mEmployeeDT.Columns.Add("ImproparePunch");
            mEmployeeDT.Columns.Add("LateINDays");
            mEmployeeDT.Columns.Add("ManualDays");
            mEmployeeDT.Columns.Add("TotalPresentDays");
            mEmployeeDT.Columns.Add("TotalAbsentDays");



            SSQL = "";
            SSQL = "select MachineID,MachineID_Encrypt,ExistingCode,DeptName,isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + ",Designation,OTEligible from Employee_Mst Where Compcode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "' ";

            if (SessionUserType == "2")
            {
                SSQL = SSQL + " And IsNonAdmin='1'";
            }
            else if (SessionUserType == "3")
            {
                SSQL = SSQL + " And PFNo <> '' And PFNo IS Not Null And PFNo <> '0' And PFNo <> 'o' And PFNo <> 'A'";
                SSQL = SSQL + " And PFNo <> 'NULL'";
            }
            
            else
            {
                SSQL = SSQL + " And IsActive='Yes' order by MachineID Asc";
            }

            SSQL = SSQL + " ";
            MEmployeeDS = objdata.ReturnMultipleValue(SSQL);


            if (MEmployeeDS.Rows.Count > 0)
            {
                for (int i = 0; i < MEmployeeDS.Rows.Count; i++)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[i]["EmployeeNo"] = MEmployeeDS.Rows[i]["MachineID"].ToString();
                    AutoDTable.Rows[i]["MachineID"] = MEmployeeDS.Rows[i]["MachineID_Encrypt"].ToString();
                    AutoDTable.Rows[i]["ExistingCode"] = MEmployeeDS.Rows[i]["ExistingCode"].ToString();
                    AutoDTable.Rows[i]["DeptName"] = MEmployeeDS.Rows[i]["DeptName"].ToString();
                    AutoDTable.Rows[i]["FirstName"] = MEmployeeDS.Rows[i]["FirstName"].ToString();
                    AutoDTable.Rows[i]["Designation"] = MEmployeeDS.Rows[i]["Designation"].ToString();



                }

                DayAttndBWDates();

                grid.DataSource = mEmployeeDT;
                grid.DataBind();
                string attachment = "attachment;filename=TOTAL DAY SUMMARY REPORT BETWEEN DATES.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">TOTAL DAY SUMMARY REPORT BETWEEN DATES &nbsp;&nbsp;&nbsp;</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
                Response.Write("&nbsp;&nbsp;&nbsp;");
                Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();

            }
        }
    }

    public void DayAttndBWDates()
    {
        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);
        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        bool isPresent = false;
        int intI = 0;
        int intK = 0;
        string halfPresent = "0";

        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //mEmployeeDT.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

            daycount -= 1;
            daysAdded += 1;
        }

        //mEmployeeDT.Columns.Add("Total Days");

        for (int j1 = 0; j1 < AutoDTable.Rows.Count; j1++)
        {
            intI = 4;
            double Worked_Days_Count = 0;
            

            for (int intCol = 0; intCol <= daysAdded - 1; intCol++)
            {
                isPresent = false;
                string Machine_ID_Str = "";
                string OT_Week_OFF_Machine_No = "";
                string Date_Value_Str = "";
                DataTable mLocalDS = new DataTable();
                string Time_IN_Str = "";
                string Time_Out_Str = "";
                string Total_Time_get = "";
                Int32 j = 0;
                double time_Check_dbl = 0;
                string Emp_Total_Work_Time_1 = "00:00";
                isPresent = false;

                Machine_ID_Str = AutoDTable.Rows[j1]["MachineID"].ToString();
                OT_Week_OFF_Machine_No = AutoDTable.Rows[j1]["EmployeeNo"].ToString();

                if (OT_Week_OFF_Machine_No == "114")
                {
                    string QQ = "Select * from Employee_Mst";

                }
                else if (OT_Week_OFF_Machine_No == "2001")
                {
                    string QQ = "Select * from Employee_Mst";
                }


                DateTime dtime = date1.AddDays(intCol);
                DateTime dtime1 = dtime.AddDays(1);

                Date_Value_Str = dtime.ToString("yyyy/MM/dd");
                Date_value_str1 = dtime1.ToString("yyyy/MM/dd");


                int Days = (int)dtime.Day;

                Day = Convert.ToString(Days);


                //Get Employee Week OF DAY
                DataTable DS_WH = new DataTable();
                string Emp_WH_Day = "";
                SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                DS_WH = objdata.ReturnMultipleValue(SSQL);
                if (DS_WH.Rows.Count != 0)
                {
                    Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                }
                else
                {
                    Emp_WH_Day = "";
                }

                DataTable mLocalDS1 = new DataTable();
                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_value_str1 + " " + "02:00' Order by TimeIN ASC";
                mLocalDS = objdata.ReturnMultipleValue(SSQL);
                if (mLocalDS.Rows.Count <= 0)
                {
                    Time_IN_Str = "";
                }
                else
                {
                    //Time_IN_Str = mLocalDS.Tables(0).Rows(0)(0)
                }
                //TimeOUT Get
                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_value_str1 + " " + "10:00' Order by TimeOUT Asc";
                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                if (mLocalDS.Rows.Count <= 0)
                {
                    Time_Out_Str = "";
                }
                else
                {
                    //Time_Out_Str = mLocalDS.Tables(0).Rows(0)(0)
                }

                //Shift Change Check Start
                DateTime InTime_Check = default(DateTime);
                DateTime InToTime_Check = default(DateTime);
                TimeSpan InTime_TimeSpan = default(TimeSpan);
                string From_Time_Str = "";
                string To_Time_Str = "";
                DataTable DS_Time = new DataTable();
                DataTable DS_InTime = new DataTable();
                string Final_InTime = "";
                string Final_OutTime = "";
                string Final_Shift = "";
                DataTable Shift_DS_Change = new DataTable();
                Int32 K = 0;
                bool Shift_Check_blb = false;
                string Shift_Start_Time_Change = null;
                string Shift_End_Time_Change = null;
                string Employee_Time_Change = "";
                DateTime ShiftdateStartIN_Change = default(DateTime);
                DateTime ShiftdateEndIN_Change = default(DateTime);
                DateTime EmpdateIN_Change = default(DateTime);

                if (mLocalDS.Rows.Count != 0)
                {
                    InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"]);
                    InToTime_Check = InTime_Check.AddHours(2);

                    string s1 = InTime_Check.ToString("HH:mm:ss");
                    string s2 = InToTime_Check.ToString("HH:mm:ss");

                    InTime_TimeSpan = TimeSpan.Parse(s1);
                    From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                    TimeSpan InTime_TimeSpan1 = TimeSpan.Parse(s2);
                    To_Time_Str = InTime_TimeSpan1.Hours + ":" + InTime_TimeSpan1.Minutes;


                    //Two Hours Time Check
                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + From_Time_Str + "' And TimeOUT <='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";
                    DS_Time = objdata.ReturnMultipleValue(SSQL);

                    if (DS_Time.Rows.Count != 0)
                    {
                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                        DS_InTime = objdata.ReturnMultipleValue(SSQL);

                        if (DS_InTime.Rows.Count != 0)
                        {
                            Final_InTime = mLocalDS.Rows[0][0].ToString();
                            SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                            Shift_DS_Change = objdata.ReturnMultipleValue(SSQL);
                            Shift_Check_blb = false;

                            for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
                            {
                                DateTime DataValueStr = Convert.ToDateTime(Date_Value_Str.ToString());
                                DateTime DataValueStr1 = Convert.ToDateTime(Date_value_str1.ToString());


                                Shift_Start_Time_Change = DataValueStr.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[k]["StartIN"].ToString();

                                if (Shift_DS_Change.Rows[K]["EndIN_Days"].ToString() == "1")
                                {
                                    Shift_End_Time_Change = DataValueStr1.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[k]["EndIN"].ToString();
                                }
                                else
                                {
                                    Shift_End_Time_Change = DataValueStr.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[k]["EndIN"].ToString();
                                }

                                ShiftdateStartIN_Change = Convert.ToDateTime(Shift_Start_Time_Change);
                                ShiftdateEndIN_Change = Convert.ToDateTime(Shift_End_Time_Change);
                                EmpdateIN_Change = Convert.ToDateTime(Final_InTime);

                                if (EmpdateIN_Change >= ShiftdateStartIN_Change & EmpdateIN_Change <= ShiftdateEndIN_Change)
                                {
                                    Final_Shift = Shift_DS_Change.Rows[k]["ShiftDesc"].ToString();
                                    Shift_Check_blb = true;
                                    break;
                                }
                            }
                            if (Shift_Check_blb == true)
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                                mLocalDS = objdata.ReturnMultipleValue(SSQL);

                                if (Final_Shift == "SHIFT2")
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
                                }
                                else if (Final_Shift == "SHIFT1")
                                {
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "00:00' And TimeIN <='" + dtime.ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeIN ASC";

                                    mLocalDS = objdata.ReturnMultipleValue(SSQL);



                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + dtime.ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeOUT Asc";
                                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                                }
                                else
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "17:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                                }


                            }
                            else
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

                                mLocalDS = objdata.ReturnMultipleValue(SSQL);

                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

                                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);

                            }
                        }

                    }
                    else
                    {
                        //Time IN
                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                        mLocalDS = objdata.ReturnMultipleValue(SSQL);

                        //Time OUT
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT ASC";
                        mLocalDS1 = objdata.ReturnMultipleValue(SSQL);


                    }
                }
                //'Shift Change Check End

                if (mLocalDS.Rows.Count > 1)
                {
                    for (int tin = 0; tin <= mLocalDS.Rows.Count - 1; tin++)
                    {
                        AutoDTable.Rows[j1]["TimeIN"] = String.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0]);

                        Time_IN_Str = mLocalDS.Rows[tin][0].ToString();

                        if (mLocalDS1.Rows.Count > tin)
                        {
                            Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                        }
                        else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                        {
                            Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                        }
                        else
                        {
                            Time_Out_Str = "";
                        }
                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                        }
                        if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                        {
                            time_Check_dbl = time_Check_dbl;
                        }
                        else
                        {
                            DateTime date11 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts = new TimeSpan();
                            ts = date2.Subtract(date11);
                            ts = date2.Subtract(date11);
                            Total_Time_get = ts.Hours.ToString();
                            ts4 = ts4.Add(ts);
                            //OT Time Get
                            Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                Emp_Total_Work_Time_1 = "00:00";
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                Emp_Total_Work_Time_1 = "00:00";
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date2.Subtract(date1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = ts.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts.Hours.ToString() + ":" + ts.Minutes.ToString();
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Emp_Total_Work_Time_1 = "00:00";
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    Emp_Total_Work_Time_1 = "00:00";
                            }
                            else
                            {
                                time_Check_dbl = time_Check_dbl + Convert.ToInt16(Total_Time_get);
                            }
                        }
                    }
                }
                else
                {
                    TimeSpan ts4 = new TimeSpan();
                    ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Time_IN_Str = "";
                    }
                    else
                    {
                        Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                    }
                    for (int tout = 0; tout <= mLocalDS1.Rows.Count - 1; tout++)
                    {
                        if (mLocalDS1.Rows.Count <= 0)
                        {
                            Time_Out_Str = "";
                        }
                        else
                        {
                            Time_Out_Str = mLocalDS1.Rows[0][0].ToString();

                            AutoDTable.Rows[j1]["TimeOUT"] = String.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0]);
                           
                        }

                    }
                    //Emp_Total_Work_Time
                    if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                    {
                        time_Check_dbl = 0;

                        if (ImproperPunch != "")
                        {
                            ImproperPunch = ImproperPunch + "," + Day;
                            AutoDTable.Rows[j1]["ImproparePunch"] = ImproperPunch;
                            //mEmployeeDT.Rows[intK]["ImproparePunch"] = ImproperPunch;
                        }
                        else
                        {
                            ImproperPunch = ImproperPunch + Day;
                            AutoDTable.Rows[j1]["ImproparePunch"] = ImproperPunch;
                            //mEmployeeDT.Rows[intK]["ImproparePunch"] = ImproperPunch;
                            
                        }
                    }
                    else
                    {
                        DateTime date11 = System.Convert.ToDateTime(Time_IN_Str);
                        DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                        TimeSpan ts = new TimeSpan();
                        ts = date2.Subtract(date11);
                        ts = date2.Subtract(date11);
                        Total_Time_get = ts.Hours.ToString();
                        ts4 = ts4.Add(ts);
                        //OT Time Get
                        Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            Emp_Total_Work_Time_1 = "00:00";
                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                            Emp_Total_Work_Time_1 = "00:00";
                        if (Left_Val(Total_Time_get, 1) == "-")
                        {
                            date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                            ts = date2.Subtract(date11);
                            ts = date2.Subtract(date11);
                            Total_Time_get = ts.Hours.ToString();
                            time_Check_dbl = Convert.ToInt16(Total_Time_get);
                            Emp_Total_Work_Time_1 = ts.Hours.ToString() + ":" + ts.Minutes.ToString();
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                Emp_Total_Work_Time_1 = "00:00";
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                Emp_Total_Work_Time_1 = "00:00";
                        }
                        else
                        {
                            time_Check_dbl = Convert.ToInt16(Total_Time_get);
                        }
                    }
                }

                if (OT_Week_OFF_Machine_No == "121")
                {
                    string s = "";
                }



                double Calculate_Attd_Work_Time = 0;
                double Calculate_Attd_Work_Time_1 = 4.0;
                SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and Calculate_Work_Hours <> '' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
                mLocalDS = objdata.ReturnMultipleValue(SSQL);
                if (mLocalDS.Rows.Count <= 0)
                {
                    Calculate_Attd_Work_Time = 8;
                }
                else
                {
                    Calculate_Attd_Work_Time = (!string.IsNullOrEmpty(mLocalDS.Rows[0]["Calculate_Work_Hours"].ToString()) ? Convert.ToInt16(mLocalDS.Rows[0]["Calculate_Work_Hours"]) : 0);
                    if (Calculate_Attd_Work_Time == 0)
                    {
                        Calculate_Attd_Work_Time = 8;
                    }
                    else
                    {
                        Calculate_Attd_Work_Time = 8;
                    }
                }


                string qry_nfhcheck = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                mLocalDS = objdata.ReturnMultipleValue(qry_nfhcheck);
                bool NFH_CHeck_Bol = false;
                bool Half_Day_Count_Check = false;
                NFH_CHeck_Bol = false;
                if (mLocalDS.Rows.Count > 0)
                {
                    NFH_CHeck_Bol = true;
                }

                halfPresent = "0";
                Half_Day_Count_Check = false;
                if (NFH_CHeck_Bol == true)
                {
                    //Skip
                    if (time_Check_dbl >= Calculate_Attd_Work_Time_1 & time_Check_dbl < Calculate_Attd_Work_Time)
                    {
                        isPresent = true;
                        halfPresent = "1";
                        Half_Day_Count_Check = true;
                    }
                    if (time_Check_dbl >= Calculate_Attd_Work_Time)
                    {
                        isPresent = true;
                        halfPresent = "0";
                    }
                }
                else
                {
                    if (time_Check_dbl >= Calculate_Attd_Work_Time_1 & time_Check_dbl < Calculate_Attd_Work_Time)
                    {
                        isPresent = true;
                        halfPresent = "1";
                        Half_Day_Count_Check = true;
                        Worked_Days_Count = Worked_Days_Count + 0.5;
                    }
                    if (time_Check_dbl >= Calculate_Attd_Work_Time)
                    {
                        isPresent = true;
                        halfPresent = "0";
                        Worked_Days_Count = Worked_Days_Count + 1;
                    }
                }

                //Find Week OFF
                string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                mLocalDS = objdata.ReturnMultipleValue(qry_nfh);
                if (mLocalDS.Rows.Count > 0)
                {
                    if (isPresent == true)
                    {
                        if (halfPresent == "0")
                        {
                            int red = 255;
                            int green = 255;
                            int blue = 255;

                            string hexColor = "#" + blue.ToString("X") + green.ToString("X") + blue.ToString("X");
                            //mEmployeeDT.Rows[intK][intI] = "NH / X";

                            if (WorkedDays != "")
                            {
                                WorkedDays = WorkedDays + "," + Day;
                                AutoDTable.Rows[j1]["WorkedDays"] = WorkedDays;
                                //mEmployeeDT.Rows[intK]["WorkedDays"] = WorkedDays;
                                TotalPresentDays = TotalPresentDays + 1;
                            }
                            else
                            {
                                WorkedDays = WorkedDays + Day;
                                AutoDTable.Rows[j1]["WorkedDays"] = WorkedDays;
                                //mEmployeeDT.Rows[intK]["WorkedDays"] = WorkedDays;
                                TotalPresentDays = TotalPresentDays + 1;
                            }

                        }
                        else if (Half_Day_Count_Check == true)
                        {
                            //mEmployeeDT.Rows[intK][intI] = "NH / H";

                            if (WorkedDays != "")
                            {
                                WorkedDays = WorkedDays + "," + Day;
                                AutoDTable.Rows[j1]["WorkedDays"] = WorkedDays;
                                //mEmployeeDT.Rows[intK]["WorkedDays"] = WorkedDays;
                                TotalPresentDays = TotalPresentDays + 1;
                            }
                            else
                            {
                                WorkedDays = WorkedDays + Day;
                                AutoDTable.Rows[j1]["WorkedDays"] = WorkedDays;
                                //mEmployeeDT.Rows[intK]["WorkedDays"] = WorkedDays;
                                TotalPresentDays = TotalPresentDays + 1;
                            }

                        }
                    }
                    else
                    {
                        //mEmployeeDT.Rows[intK][intI] = "NH / A";
                        if (AbsentDays != "")
                        {
                            AbsentDays = AbsentDays + "," + Day;
                            AutoDTable.Rows[j1]["AbsentDays"] = AbsentDays;
                            //mEmployeeDT.Rows[intK]["AbsentDays"] = AbsentDays;
                            TotalAbsentDays = TotalAbsentDays + 1;
                        }
                        else
                        {
                            AbsentDays = AbsentDays + Day;
                            AutoDTable.Rows[j1]["AbsentDays"] = AbsentDays;
                            //mEmployeeDT.Rows[intK]["AbsentDays"] = AbsentDays;
                            TotalAbsentDays = TotalAbsentDays + 1;
                        }
                    }
                }
                else
                {
                    if (isPresent == true)
                    {
                        if (halfPresent == "0")
                        {
                            int red = 255;
                            int green = 255;
                            int blue = 255;
                            //mEmployeeDT.Rows[intK][intI] = "<span style=color:green><b>" + "X" + "</b></span>";

                            if (WorkedDays != "")
                            {
                                WorkedDays = WorkedDays + "," + Day;
                                AutoDTable.Rows[j1]["WorkedDays"] = WorkedDays;
                                //mEmployeeDT.Rows[intK]["WorkedDays"] = WorkedDays;
                                TotalPresentDays = TotalPresentDays + 1;
                            }
                            else
                            {
                                WorkedDays = WorkedDays + Day;
                                AutoDTable.Rows[j1]["WorkedDays"] = WorkedDays;
                                //mEmployeeDT.Rows[intK]["WorkedDays"] = WorkedDays;
                                TotalPresentDays = TotalPresentDays + 1;
                            }
                        }
                        else if (Half_Day_Count_Check == true)
                        {
                            //mEmployeeDT.Rows[intK][intI] = "H";

                            if (WorkedDays != "")
                            {
                                WorkedDays = WorkedDays + "," + Day;
                                //AutoDTable.Rows[j1]["WorkedDays"] = WorkedDays;
                                //mEmployeeDT.Rows[intK]["WorkedDays"] = WorkedDays;
                                TotalPresentDays = TotalPresentDays + 1;
                            }
                            else
                            {
                                WorkedDays = WorkedDays + Day;
                                AutoDTable.Rows[j1]["WorkedDays"] = WorkedDays;
                                //mEmployeeDT.Rows[intK]["WorkedDays"] = WorkedDays;
                                TotalPresentDays = TotalPresentDays + 1;
                            }
                        }


                        if (OT_Week_OFF_Machine_No == "121")
                        {
                            string ss = "1";
                        }



                        //Check Manual Attendance
                        DataTable Manual_DS = new DataTable();
                        DataTable Mattend_DS = new DataTable();
                        string Manual_Attendance = "";
                        SSQL = "Select * from ManAttn_Details where Machine_No='" + OT_Week_OFF_Machine_No + "'";
                        SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And CONVERT(DATETIME,AttnDateStr, 103)= CONVERT(DATETIME,'" + dtime.ToString("dd/MM/yyyy") + "',103)";

                        Manual_DS = objdata.ReturnMultipleValue(SSQL);
                        if (Manual_DS.Rows.Count != 0)
                        {
                            if (ManualDays != "")
                            {
                                ManualDays = ManualDays + "," + Day;
                                AutoDTable.Rows[j1]["ManualDays"] = ManualDays;
                            }
                            else
                            {
                                ManualDays = ManualDays + Day;
                                AutoDTable.Rows[j1]["ManualDays"] = ManualDays;
                            }
                        }
                    }
                    else
                    {
                        //Check Leave Master
                        DataTable Leave_DS = new DataTable();
                        string Leave_Short_Name = "A";
                        SSQL = "Select * from Leave_Register_Mst where Machine_No='" + OT_Week_OFF_Machine_No + "'";
                        SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And From_Date_Dt >='" + Date_Value_Str + "'";
                        SSQL = SSQL + " And To_Date_Dt <='" + Date_Value_Str + "'";
                        Leave_DS = objdata.ReturnMultipleValue(SSQL);
                        if (Leave_DS.Rows.Count != 0)
                        {
                            //Get Leave Short Name
                            SSQL = "Select * from LeaveType_Mst Where LeaveType='" + Leave_DS.Rows[0]["LeaveType"] + "'";
                            Leave_DS = objdata.ReturnMultipleValue(SSQL);
                            if (Leave_DS.Rows.Count != 0)
                                Leave_Short_Name = Leave_DS.Rows[0]["Short_Name"].ToString();
                            //mEmployeeDT.Rows[intK][intI] = Leave_Short_Name;

                        }
                        else
                        {
                            //mEmployeeDT.Rows[intK][intI] = "<span style=color:red><b>" + "A" + "</b></span>";
                        }

                        //mEmployeeDT.Rows[intK][intI] = "NH / A";
                        if (AbsentDays != "")
                        {
                            AbsentDays = AbsentDays + "," + Day;
                            AutoDTable.Rows[j1]["AbsentDays"] = AbsentDays;
                            //mEmployeeDT.Rows[intK]["AbsentDays"] = AbsentDays;
                            TotalAbsentDays = TotalAbsentDays + 1;
                        }
                        else
                        {
                            AbsentDays = AbsentDays + Day;
                            AutoDTable.Rows[j1]["AbsentDays"] = AbsentDays;
                            //mEmployeeDT.Rows[intK]["AbsentDays"] = AbsentDays;
                            TotalAbsentDays = TotalAbsentDays + 1;
                        }
                    }
                }

                // late in start

                Final_InTime = dtime.AddDays(0).ToString("yyyy/MM/dd") + " " + AutoDTable.Rows[intK]["TimeIN"].ToString();
                //Check With IN Time Shift
                SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                Shift_DS = objdata.ReturnMultipleValue(SSQL);
                Shift_Check_blb = false;
                for (int K1 = 0; K1 <= Shift_DS.Rows.Count - 1; K1++)
                {
                    string a = Shift_DS.Rows[K1]["StartIN_Days"].ToString();
                    int b = Convert.ToInt16(a.ToString());
                    Shift_Start_Time = dtime.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[K1]["StartIN"].ToString();
                    string a1 = Shift_DS.Rows[K1]["EndIN_Days"].ToString();
                    int b1 = Convert.ToInt16(a1.ToString());
                    Shift_End_Time = dtime.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[K1]["EndIN"].ToString();

                    ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                    ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());
                    EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());

                    if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                    {
                        Final_Shift = Shift_DS.Rows[K]["ShiftDesc"].ToString();
                        Shift_Check_blb = true;
                    }
                }

                if (Shift_Check_blb == true)
                {
                    DataTable Ds_Shift = new DataTable();
                    DataTable Ds_MstIN = new DataTable();
                    string MSt_Stime = "";
                    string STime = "";
                    string Time = "";
                    string Time1 = "";
                    string[] SP_Shift = null;
                    string[] SP_Minus = null;
                    string EmpTime = "";
                    string Val_Hours = "";
                    string Val_Minus = "";
                    string Val1_Hours = "";
                    string Val1_Minus = "";

                    //LateIN Time Checking 
                    SSQL = "";
                    SSQL = "select StartTime from MstLateIN where ShiftDesc='" + Final_Shift + "' ";
                    Ds_MstIN = objdata.ReturnMultipleValue(SSQL);
                    MSt_Stime = Ds_MstIN.Rows[0][0].ToString();
                    SP_Shift = MSt_Stime.Split(':');
                    Val_Hours = SP_Shift[0];
                    Val_Minus = SP_Shift[1];


                    //Shift Stime check
                    SSQL = "select StartTime from Shift_Mst  Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and ShiftDesc='" + Final_Shift + "'";
                    Ds_Shift = objdata.ReturnMultipleValue(SSQL);
                    STime = Ds_Shift.Rows[0][0].ToString();
                    SP_Minus = STime.Split(':');
                    Val1_Hours = SP_Minus[0];
                    Val1_Minus = SP_Minus[1];

                    //Add Hours and Minus
                    Val_Hours = (Convert.ToInt32(Val_Hours) + Convert.ToInt32(Val1_Hours)).ToString();
                    Val_Minus = (Convert.ToInt32(Val_Minus) + Convert.ToInt32(Val1_Minus)).ToString();
                    Time = Val_Hours + ":" + Val_Minus;

                    ShiftdateStartIN = Convert.ToDateTime(Date_Value_Str + " " + Time);
                    if (EmpdateIN >= ShiftdateStartIN)
                    {
                        Late_Errflag = true;
                        DateTime date11 = System.Convert.ToDateTime(ShiftdateStartIN);
                        DateTime date12 = System.Convert.ToDateTime(EmpdateIN);
                        TimeSpan ts = new TimeSpan();
                        ts = date12.Subtract(date11);
                        ts = date12.Subtract(date11);
                        Total_Time_get = ts.Hours + ":" + ts.Minutes;
                        AutoDTable.Rows[intK]["LateIn"] = Total_Time_get;

                        if (LateINDays != "")
                        {
                            LateINDays = LateINDays + "," + Day;
                            AutoDTable.Rows[j1]["LateINDays"] = LateINDays;
                            //mEmployeeDT.Rows[intK]["LateINDays"] = LateINDays;
                        }
                        else
                        {
                            LateINDays = LateINDays + Day;
                            AutoDTable.Rows[j1]["LateINDays"] = LateINDays;
                            //mEmployeeDT.Rows[intK]["LateINDays"] = LateINDays;
                        }
                    }
                    else
                    {
                        Total_Time_get = "00:00";
                     
                    }
                }
                else
                {
                    Total_Time_get = "00:00";
                  
                }
                // Late In completed 


                AutoDTable.Rows[j1]["TotalPresentDays"] = TotalPresentDays;
                //mEmployeeDT.Rows[intK]["TotalAbsentDays"] = TotalAbsentDays;

                intI += 1;
            }
            //mEmployeeDT.Rows[intK][intI] = Worked_Days_Count;
          


            mEmployeeDT.NewRow();
            mEmployeeDT.Rows.Add();
            mEmployeeDT.Rows[intK]["SNo"] = (intK + 1).ToString();
            mEmployeeDT.Rows[intK]["EmployeeNo"] = AutoDTable.Rows[intK]["EmployeeNo"].ToString();
            mEmployeeDT.Rows[intK]["ExistingCode"] = AutoDTable.Rows[intK]["ExistingCode"].ToString();
            mEmployeeDT.Rows[intK]["Name"] = AutoDTable.Rows[intK]["FirstName"].ToString();
            mEmployeeDT.Rows[intK]["DeptName"] = AutoDTable.Rows[intK]["DeptName"].ToString();
            mEmployeeDT.Rows[intK]["WorkedDays"] = WorkedDays;
            mEmployeeDT.Rows[intK]["AbsentDays"] = AbsentDays;
            mEmployeeDT.Rows[intK]["ImproparePunch"] = ImproperPunch;
            mEmployeeDT.Rows[intK]["LateINDays"] = LateINDays;
            mEmployeeDT.Rows[intK]["ManualDays"] = AutoDTable.Rows[intK]["ManualDays"].ToString();
            mEmployeeDT.Rows[intK]["TotalPresentDays"] = TotalPresentDays;
            mEmployeeDT.Rows[intK]["TotalAbsentDays"] = TotalAbsentDays;


            intK += 1;
            AbsentDays = "";
            WorkedDays = "";
            ImproperPunch = "";
            LateINDays = "";
            ManualDays = "";
            TotalPresentDays = 0;
            TotalAbsentDays = 0;
       }
    }

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    public string Right_Val(string Value, int Length)
    {
        // Recreate a RIGHT function for string manipulation
        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }

}

