﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class DayAttendanceHours : System.Web.UI.Page
{
    DataTable mStatus = new DataTable();
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    string ModeType = "";
    string ShiftType1 = "";
    string Date1 = "";
    string Date2 = "";
    string ddlShiftType = "";
    string SSQL = "";
    DateTime date1 = new DateTime();
    DateTime date2 = new DateTime();
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    DataSet ds = new DataSet();
    string DeptName;
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDS = new DataTable();
    string SessionUserType;

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Hours";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();

            //  ModeType = Request.QueryString["ModeType"].ToString();
            // ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date1 = Request.QueryString["Date1"].ToString();
            Date2 = Request.QueryString["Date2"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();


            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("Dept");
            AutoDTable.Columns.Add("Type");
            AutoDTable.Columns.Add("Shift");
            AutoDTable.Columns.Add("EmpCode");
            AutoDTable.Columns.Add("ExCode");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("TimeIN");
            AutoDTable.Columns.Add("TimeOUT");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Category");
            AutoDTable.Columns.Add("SubCategory");
            AutoDTable.Columns.Add("TotalMIN");
            AutoDTable.Columns.Add("GrandTOT");



            GetAttdDayWiseHours();

            ds.Tables.Add(mEmployeeDS);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/DayHours1.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            CrystalReportViewer1.ReportSource = report;

        }
    }

    public void GetAttdDayWiseHours()
    {

        date1 = Convert.ToDateTime(Date1);
        date2 = Convert.ToDateTime(Date2);


        DataTable mLocalDS_INTAB = new DataTable();
        DataTable mLocalDS_OUTTAB = new DataTable();
        DataTable datasetintime = new DataTable();
        string Date_Value_Str = null;
        string Time_IN_Str = "";
        string Time_Out_Str = "";
        Int32 time_Check_dbl = 0;
        string Total_Time_get = "";

        string[] spdate = { "00:00 00:00" };
        string[] spdate1 = { "00:00 00:00" };






        DataTable Payroll_DS = new DataTable();

        DataTable newDT = new DataTable();

        //Delete Temp and temp1 Table
        SSQL = "Delete from Temp";
        newDT = objdata.ReturnMultipleValue(SSQL);

        SSQL = "Delete from Temp1";
        newDT = objdata.ReturnMultipleValue(SSQL);




        DataTable mDataSet = new DataTable();
        string mIpAddress_IN;
        string mIpAddress_OUT;

        //SSQL = "";
        //SSQL = "Select IPAddress,IPMode from IPAddress_Mst Where Compcode='" + SessionCcode + "'";
        //SSQL = SSQL + " And LocCode='" + SessionLcode + "'";

        //mDataSet = objdata.ReturnMultipleValue(SSQL);



        //if (mDataSet.Rows.Count > 0)
        //{
        //    for (int iRow = 0; iRow <= mDataSet.Rows.Count - 1; iRow++)
        //    {
        //        if ((mDataSet.Rows[iRow]["IPMode"]) == "IN")
        //        {
        //            mIpAddress_IN = mDataSet.Rows[iRow]["IPAddress"].ToString();
        //        }
        //        else if ((mDataSet.Rows[iRow]["IPMode"]) == "OUT")
        //        {
        //            mIpAddress_OUT = mDataSet.Rows[iRow]["IPAddress"].ToString();
        //        }
        //    }
        //}



        DataTable mLocalDS = new DataTable();

        //if (ModeType == "IN/OUT")
        //{
        //    SSQL = "";
        //    SSQL = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
        //    SSQL = SSQL + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
        //    SSQL += " from Shift_Mst Where CompCode='" + SessionCcode + "'";
        //    SSQL += " And LocCode='" + SessionLcode + "'";
        //    if (ShiftType1 != "ALL")
        //    {
        //        SSQL += " And shiftDesc='" + ShiftType1 + "'";
        //    }
        //    SSQL += " And ShiftDesc like '" + ddlShiftType + "%'";
        //    //Shift%'"
        //    SSQL += " Order By shiftDesc";

        //}
        //else
        //{
        //    return;
        //}
        //string ShiftType = "";

        //mLocalDS = objdata.ReturnMultipleValue(SSQL);


        //if (mLocalDS.Rows.Count < 0)
        //    return;



        //int mStartINRow = 0;
        //int mStartOUTRow = 0;


        //for (int iTabRow = 0; iTabRow < mLocalDS.Rows.Count; iTabRow++)
        //{



        //string sIndays_str = mLocalDS.Rows[iTabRow]["StartIN_Days"].ToString();
        //double sINdays = Convert.ToDouble(sIndays_str);
        //string eIndays_str = mLocalDS.Rows[iTabRow]["EndIN_Days"].ToString();
        //double eINdays = Convert.ToDouble(eIndays_str);





        //if (AutoDTable.Rows.Count <= 1)
        //{
        //    mStartOUTRow = 1;
        //}
        //else
        //{
        //    mStartOUTRow = AutoDTable.Rows.Count - 1;
        //}
        //if (mLocalDS.Rows[iTabRow]["shiftDesc"].ToString() == "GENERAL")
        //{
        //    ShiftType = "GENERAL";
        //}
        //else
        //{
        //    ShiftType = "SHIFT";
        //}
        //if (ShiftType == "GENERAL")
        //{
        //    SSQL = "";
        //    SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID ";
        //    SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
        //    SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "'";
        //    SSQL = SSQL + " And LT.TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";

        //    SSQL = SSQL + " And LT.TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
        //    SSQL = SSQL + " AND em.ShiftType='" + ShiftType + "' Group By LT.MachineID";
        //    SSQL = SSQL + " Order By Min(LT.TimeIN)";

        //}
        //else if (ShiftType == "SHIFT")
        //{
        //    SSQL = "";
        //    SSQL = "Select MachineID,Min(TimeIN) as [TimeIN] from LogTime_IN ";
        //    SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
        //    SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
        //    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartIN"].ToString() + "'";
        //    SSQL = SSQL + " And TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndIN"].ToString() + "'";
        //    SSQL = SSQL + " Group By MachineID";
        //    SSQL = SSQL + " Order By Min(TimeIN)";
        //}
        //else
        //{
        //    SSQL = "";
        //    SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID ";
        //    SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
        //    SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "'";
        //    SSQL = SSQL + " And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartIN"] + "'";

        //    SSQL = SSQL + " And LT.TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndIN"] + "'";
        //    SSQL = SSQL + " AND em.ShiftType='" + ShiftType + "' Group By LT.MachineID";
        //    SSQL = SSQL + " Order By Min(LT.TimeIN)";
        //}

        //mDataSet = objdata.ReturnMultipleValue(SSQL);



        //if (mDataSet.Rows.Count > 0)
        //{
        //    string MachineID;

        //    for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
        //    {
        //        bool chkduplicate = false;
        //        chkduplicate = false;
        //        //Check Duplicate Entry
        //        for (int ia = 0; ia < AutoDTable.Rows.Count - 1; ia++)
        //        {
        //            string id = mDataSet.Rows[iRow]["MachineID"].ToString();

        //            if (id == AutoDTable.Rows[ia][9].ToString())
        //            {
        //                chkduplicate = true;
        //                break; // TODO: might not be correct. Was : Exit For
        //            }
        //        }
        //        if (chkduplicate == false)
        //        {
        //            AutoDTable.NewRow();
        //            AutoDTable.Rows.Add();
        //            AutoDTable.Rows[mStartINRow][3] = mLocalDS.Rows[iTabRow]["ShiftDesc"].ToString();


        //            if (ShiftType == "SHIFT")
        //            {

        //                string str = mDataSet.Rows[iRow][1].ToString();

        //                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);


        //            }
        //            else
        //            {
        //                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);


        //            }


        //            MachineID = mDataSet.Rows[iRow]["MachineID"].ToString();
        //            AutoDTable.Rows[mStartINRow][9] = MachineID.ToString();
        //            mStartINRow += 1;

        //        }
        //    }
        //}


        //  MsgBox(mStartOUTRow)


        //mDataSet = new DataTable();



        //SSQL = "";
        //SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT ";
        //SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
        //SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
        //SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";

        //SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
        //SSQL = SSQL + " Group By MachineID";
        //SSQL = SSQL + " Order By min(TimeOUT)";


        //mDataSet = objdata.ReturnMultipleValue(SSQL);


        DataTable mDataTable = new DataTable();

        string InMachine_IP = "";
        DataTable mLocalDS_out = new DataTable();
        DataTable mdayDataset = new DataTable();

        SSQL = "select MachineID_Encrypt from Employee_Mst where Compcode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and DeptName='" + DeptName + "'";
        mDataTable = objdata.ReturnMultipleValue(SSQL);

        for (int iRow2 = 0; iRow2 < mDataTable.Rows.Count; iRow2++)
        {

            InMachine_IP = mDataTable.Rows[iRow2][0].ToString();

            SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "' And Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
            SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT desc";

            mLocalDS_out = objdata.ReturnMultipleValue(SSQL);
            //if (mLocalDS_out.Rows.Count <= 0)
            //{
            //    //Skip
            //}
            //else
            //{
            //    if (AutoDTable.Rows[iRow2][9].ToString() == mLocalDS_out.Rows[0][0].ToString())
            //    {
            //        AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1]);

            //    }
            //}


            //Grand Total value Display
            Time_IN_Str = "";
            Time_Out_Str = "";


            //Int32 c = default(Int32);
            string D1 = null;
            string Table1 = "Temp1";
            string Table2 = "Temp";
            string Sname = null;
            DataTable datasetname = default(DataTable);
            SSQL = "select distinct   convert(varchar(10),TimeIN,111) as TimeIN  from LogTime_IN";
            SSQL = SSQL + " where TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date2.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
            mdayDataset = objdata.ReturnMultipleValue(SSQL);
            for (int c = 0; c < mdayDataset.Rows.Count; c++)
            {
                D1 = mdayDataset.Rows[c][0].ToString();

                InMachine_IP = mDataTable.Rows[iRow2][0].ToString();


                SSQL = "";
                Date_Value_Str = date1.ToString("yyyy/MM/dd");
                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                SSQL = SSQL + " And TimeIN >='" + D1 + " " + "01:00' And TimeIN <='" + D1 + " " + "23:00' Order by TimeIN ASC";
                mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                SSQL = SSQL + " And TimeOUT >='" + D1 + " " + "01:00' And TimeOUT <='" + D1 + " " + "23:00' Order by TimeOUT desc";
                //SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" & InMachine_IP & "'"
                //SSQL = SSQL & " And TimeOUT >='" & dtpFromDate.Value.AddDays(mLocalDS.Tables(0).Rows(iTabRow)("StartOUT_Days")).ToString("yyyy/MM/dd") & " " & "00:00' And TimeOUT <='" & dtpFromDate.Value.AddDays(mLocalDS.Tables(0).Rows(iTabRow)("EndOUT_Days")).ToString("yyyy/MM/dd") & " " & "23:59' Order by TimeOUT Desc"
                mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);
                string Emp_Total_Work_Time_1 = "00:00";
                if (mLocalDS_INTAB.Rows.Count > 1)
                {

                    for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                    {
                        Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

                        if (mLocalDS_OUTTAB.Rows.Count > tin)
                        {
                            Time_Out_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                        }
                        else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                        {
                            Time_Out_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                        }
                        else
                        {
                            Time_Out_Str = "";
                        }
                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        //ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay()
                        //ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay()
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                        }

                        if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                        {
                            time_Check_dbl = time_Check_dbl;
                        }
                        else
                        {
                            DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);

                            TimeSpan ts1 = new TimeSpan();
                            ts1 = date4.Subtract(date3);
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();

                            //OT Time Get
                            Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == ":-1" | Emp_Total_Work_Time_1 == "0:-4")
                                Emp_Total_Work_Time_1 = "00:00";


                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();

                                time_Check_dbl = Convert.ToInt32(Total_Time_get);

                                Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == ":-1" | Emp_Total_Work_Time_1 == "0:-4")
                                    Emp_Total_Work_Time_1 = "00:00";
                            }
                            else
                            {
                                ts4 = ts4.Add(ts1);
                                Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == ":-1" | Emp_Total_Work_Time_1 == "0:-4")
                                    Emp_Total_Work_Time_1 = "00:00";
                                time_Check_dbl = Convert.ToInt32(Total_Time_get);
                            }
                        }
                        //AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;



                    }


                }
                else
                {
                    TimeSpan ts4 = new TimeSpan();

                    ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                    if (mLocalDS_INTAB.Rows.Count <= 0)
                    {
                        Time_IN_Str = "00/00/0000 00:00:00";
                    }
                    else
                    {
                        Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                    }
                    //For tout = 0 To mLocalDS_OUTTAB.Tables(0).Rows.Count - 1
                    if (mLocalDS_OUTTAB.Rows.Count <= 0)
                    {
                        Time_Out_Str = "00/00/0000 00:00:00";
                    }
                    else
                    {
                        Time_Out_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                    }

                    //Next


                    //Emp_Total_Work_Time
                    if (Time_IN_Str == "00/00/0000 00:00:00" | Time_Out_Str == "00/00/0000 00:00:00")
                    {
                        time_Check_dbl = 0;
                    }
                    else
                    {
                        DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                        DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
                        TimeSpan ts1 = new TimeSpan();
                        ts1 = date3.Subtract(date4);
                        ts1 = date3.Subtract(date4);
                        Total_Time_get = ts1.Hours.ToString();
                        //& ":" & Trim(ts.Minutes)
                        //ts4 = ts4.Add(ts1)
                        //OT Time Get
                        //Emp_Total_Work_Time_1 = Trim(ts4.Hours) & ":" & Trim(ts4.Minutes)
                        //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")

                        if (Left_Val(Total_Time_get, 1) == "-")
                        {
                            date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                            ts1 = date4.Subtract(date3);
                            ts1 = date4.Subtract(date3);
                            ts4 = ts4.Add(ts1);
                            Total_Time_get = ts1.Hours.ToString();
                            //& ":" & Trim(ts.Minutes)
                            time_Check_dbl = Convert.ToInt32(Total_Time_get);
                            //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                            //Emp_Total_Work_Time_1 = Trim(ts1.Hours) & ":" & Trim(ts1.Minutes)
                            Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == ":-1" | Emp_Total_Work_Time_1 == "0:-4")
                                Emp_Total_Work_Time_1 = "00:00";
                        }
                        else
                        {
                            ts4 = ts4.Add(ts1);
                            Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == ":-1" | Emp_Total_Work_Time_1 == "0:-4")
                                Emp_Total_Work_Time_1 = "00:00";
                            time_Check_dbl = Convert.ToInt32(Total_Time_get);
                        }

                        // AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;

                        //fg.set_TextMatrix(iRow2, 12, Emp_Total_Work_Time_1);
                    }
                }
                //AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;


                if (string.IsNullOrEmpty(Time_IN_Str))
                {
                    Time_IN_Str = "00:00 00:00";
                }


                if (string.IsNullOrEmpty(Time_Out_Str))
                {
                    Time_Out_Str = "00:00 00:00";
                }


                spdate = Time_IN_Str.Split(' ');
                spdate1 = Time_Out_Str.Split(' ');

                InMachine_IP = mDataTable.Rows[iRow2][0].ToString();


                string namecheck = null;

                //namecheck = AutoDTable.Rows[iRow2][1].ToString();


                InMachine_IP = mDataTable.Rows[iRow2][0].ToString();
                SSQL = "select MachineID,FirstName from Employee_Mst where MachineID_Encrypt='" + InMachine_IP + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";

                datasetname = objdata.ReturnMultipleValue(SSQL);

                string MachineID = "";

                if (datasetname.Rows.Count > 0)
                {

                    MachineID = datasetname.Rows[0][0].ToString();
                    Sname = datasetname.Rows[0][1].ToString();


                    SSQL = "Insert into " + Table1 + " Values(";
                    SSQL = SSQL + "'" + Sname + "',";
                    SSQL = SSQL + "'" + MachineID + "',";
                    SSQL = SSQL + "'" + D1 + "',";
                    SSQL = SSQL + "'" + spdate[1] + "',";
                    SSQL = SSQL + "'" + spdate1[1] + "',";
                    SSQL = SSQL + "'" + Emp_Total_Work_Time_1 + "')";

                    mStatus = objdata.ReturnMultipleValue(SSQL);
                    //InsertDeleteUpdate(SSQL);


                }
                else
                {
                }






            }

        }




        //Next
        string columin = null;
        string columout = null;
        Int32 Inpunch = default(Int32);
        string inpunch1 = null;
        string outpuch = null;
        string outpuch1 = null;
        string name = null;
        string Table = "Temp";
        string Date11 = null;
        string hours = null;
        string FirstName = "EmpName";
        string Machine = "Machineid";
        string hr1 = null;
        string hr = null;

        //SSQL = "select EmpName,MachineID,Date,In1,Out1,Hr1 from Temp1 where date>='" & InMachine_IP & "'"


        SSQL = "select EmpName,MachineID,Date,In1,Out1,Hr1 from Temp1 where Date>='" + date1.ToString("yyyy/MM/dd") + "'";
        SSQL = SSQL + " and Date<='" + date2.ToString("yyyy/MM/dd") + "' Order by MachineID Asc";




        datasetintime = objdata.ReturnMultipleValue(SSQL);
        string InColName = null;
        string OutColName = null;
        string HoursColName = null;

        string InTimeValue = null;
        string OutTimeValue = null;
        string HoursTimeValue = null;
        string CurrentMachineID = null;
        string PreviousMachineID = "";
        string alldate = null;
        string alldateName = "";

        for (int i = 0; i < datasetintime.Rows.Count; i++)
        {
            CurrentMachineID = datasetintime.Rows[i]["MachineID"].ToString();
            HoursTimeValue = datasetintime.Rows[i]["Hr1"].ToString();
            Date11 = datasetintime.Rows[i]["Date"].ToString();
            InTimeValue = datasetintime.Rows[i]["In1"].ToString();
            OutTimeValue = datasetintime.Rows[i]["Out1"].ToString();
            //alldate = datasetintime.Tables(0).Rows(i)("Date")

            DataTable InsertDT = new DataTable();

            if (i == 0)
            {
                Inpunch = 1;
                InColName = "In" + Inpunch;
                OutColName = "Out" + Inpunch;
                HoursColName = "Hr" + Inpunch;
                alldateName = "Date" + Inpunch;
                //Check Already Inserted or Not
                SSQL = "Select * from Temp where MachineId='" + CurrentMachineID + "'";
                mDataSet = objdata.ReturnMultipleValue(SSQL);
                if (mDataSet.Rows.Count != 0)
                {
                    //Update Query
                    SSQL = "Update Temp Set " + InColName + "='" + InTimeValue + "'," + alldate + "='" + datasetintime.Rows[i]["Date"] + "',Date='" + datasetintime.Rows[i]["Date"] + "'," + OutColName + "='" + OutTimeValue + "'," + HoursColName + "='" + HoursTimeValue + "' where MachineId='" + CurrentMachineID + "'";

                    InsertDT = objdata.ReturnMultipleValue(SSQL);

                    //InsertDeleteUpdate(SSQL);
                }
                else
                {
                    //Insert Query
                    SSQL = "Insert Into Temp(EmpName,MachineId,Date," + alldateName + "," + InColName + "," + OutColName + "," + HoursColName + ") Values('" + datasetintime.Rows[i]["EmpName"] + "','" + CurrentMachineID + "','" + datasetintime.Rows[i]["Date"] + "','" + datasetintime.Rows[i]["Date"] + "','" + InTimeValue + "','" + OutTimeValue + "','" + HoursTimeValue + "')";
                    InsertDT = objdata.ReturnMultipleValue(SSQL);
                }
                PreviousMachineID = datasetintime.Rows[i]["MachineID"].ToString();
            }
            else
            {

                if (CurrentMachineID == PreviousMachineID)
                {
                    Inpunch = Inpunch + 1;
                    InColName = "In" + Inpunch;
                    OutColName = "Out" + Inpunch;
                    HoursColName = "Hr" + Inpunch;
                    alldate = "Date" + Inpunch;

                    //Check Already Inserted or Not
                    SSQL = "Select * from Temp where MachineId='" + CurrentMachineID + "'";
                    mDataSet = objdata.ReturnMultipleValue(SSQL);
                    if (mDataSet.Rows.Count != 0)
                    {
                        //Update Query
                        SSQL = "Update Temp Set " + InColName + "='" + InTimeValue + "'," + OutColName + "='" + OutTimeValue + "'," + alldate + "='" + datasetintime.Rows[i]["Date"] + "' ,Date='" + datasetintime.Rows[i]["Date"] + "'," + HoursColName + "='" + HoursTimeValue + "' where MachineId='" + CurrentMachineID + "'";
                        InsertDT = objdata.ReturnMultipleValue(SSQL);
                    }
                    else
                    {
                        //Insert Query
                        //SSQL = "Insert Into Temp(EmpName,MachineId,Date," + alldate + "," + InColName + "," + OutColName + "," + HoursColName + ") Values('" + datasetintime.Rows[i]["EmpName"] + "','" + CurrentMachineID + "','" + datasetintime.Rows[i]["Date"] + "','" + datasetintime.Rows[i]["Date"] + "','" + InTimeValue + "','" + OutTimeValue + "','" + HoursTimeValue + "')";
                        SSQL = "Insert Into Temp(EmpName,MachineId,Date," + alldateName + "," + InColName + "," + OutColName + "," + HoursColName + ") Values('" + datasetintime.Rows[i]["EmpName"] + "','" + CurrentMachineID + "','" + datasetintime.Rows[i]["Date"] + "','" + datasetintime.Rows[i]["Date"] + "','" + InTimeValue + "','" + OutTimeValue + "','" + HoursTimeValue + "')";
                        InsertDT = objdata.ReturnMultipleValue(SSQL);
                    }
                    PreviousMachineID = datasetintime.Rows[i]["MachineID"].ToString();
                }
                else
                {
                    Inpunch = 1;
                    InColName = "In" + Inpunch;
                    OutColName = "Out" + Inpunch;
                    HoursColName = "Hr" + Inpunch;
                    alldate = "Date" + Inpunch;
                    //Check Already Inserted or Not
                    SSQL = "Select * from Temp where MachineId='" + CurrentMachineID + "'";
                    mDataSet = objdata.ReturnMultipleValue(SSQL);
                    if (mDataSet.Rows.Count != 0)
                    {
                        //Update Query
                        SSQL = "Update Temp Set " + InColName + "='" + InTimeValue + "'," + OutColName + "='" + OutTimeValue + "'," + alldate + "='" + datasetintime.Rows[i]["Date"] + "'," + HoursColName + "='" + HoursTimeValue + "' where MachineId='" + CurrentMachineID + "'";
                        InsertDT = objdata.ReturnMultipleValue(SSQL);
                    }
                    else
                    {
                        //Insert Query
                        SSQL = "Insert Into Temp(EmpName,MachineId,Date," + alldateName + "," + InColName + "," + OutColName + "," + HoursColName + ") Values('" + datasetintime.Rows[i]["EmpName"] + "','" + CurrentMachineID + "','" + datasetintime.Rows[i]["Date"] + "','" + datasetintime.Rows[i]["Date"] + "','" + InTimeValue + "','" + OutTimeValue + "','" + HoursTimeValue + "')";
                        InsertDT = objdata.ReturnMultipleValue(SSQL);
                    }
                    PreviousMachineID = datasetintime.Rows[i]["MachineID"].ToString();
                }
            }

            //SSQL = "Delete from Temp1";
            //InsertDT = objdata.ReturnMultipleValue(SSQL);
        }


        //'No Shift (Shift Timing Not Match Employee Add
        //Call NoShift_Add_All_Shift_Wise_GetAttdDayWise()



        SSQL = "";
        SSQL = "select * from Temp where date>='" + date1.ToString("yyyy/MM/dd") + "'";
        SSQL = SSQL + " and date<='" + date2.ToString("yyyy/MM/dd") + "'";

        mEmployeeDS = objdata.ReturnMultipleValue(SSQL);



        //}
    }
}
