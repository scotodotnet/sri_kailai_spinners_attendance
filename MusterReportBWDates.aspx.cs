﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;

public partial class MusterReportBWDates : System.Web.UI.Page
{
    
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
   

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Muster Report Between Dates";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();

            WagesType = Request.QueryString["Wages"].ToString();

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            //if (SessionUserType == "2")
            //{
               
            //}
            //else
            //{


                AutoDTable.Columns.Add("EmployeeNo");
                AutoDTable.Columns.Add("MachineID");
                AutoDTable.Columns.Add("ExistingCode");
                AutoDTable.Columns.Add("DeptName");
                AutoDTable.Columns.Add("FirstName");
                AutoDTable.Columns.Add("Designation");

                mEmployeeDT.Columns.Add("EmployeeNo");
                mEmployeeDT.Columns.Add("ExistingCode");
                mEmployeeDT.Columns.Add("Name");
                mEmployeeDT.Columns.Add("DeptName");

                SSQL = "";
                SSQL = "select MachineID,MachineID_Encrypt,ExistingCode,DeptName,isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
                SSQL = SSQL + ",Designation,OTEligible from Employee_Mst Where Compcode='" + SessionCcode.ToString() + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "' ";
                
                //if (SessionUserType == "2")
                //{
                //    SSQL = SSQL + " And IsNonAdmin='1'";
                //}


                if (WagesType != "- select -")
                {
                    SSQL = SSQL + " and  Wages='" + WagesType + "' And IsActive='Yes' order by DeptName Asc";
                }
                else
                {
                    SSQL = SSQL + " And IsActive='Yes' order by DeptName Asc";
                }
               
                SSQL = SSQL + " ";
                MEmployeeDS = objdata.ReturnMultipleValue(SSQL);


                if (MEmployeeDS.Rows.Count > 0)
                {
                    for (int i = 0; i < MEmployeeDS.Rows.Count; i++)
                    {
                        AutoDTable.NewRow();
                        AutoDTable.Rows.Add();
                        AutoDTable.Rows[i]["EmployeeNo"] = MEmployeeDS.Rows[i]["MachineID"].ToString();
                        AutoDTable.Rows[i]["MachineID"] = MEmployeeDS.Rows[i]["MachineID_Encrypt"].ToString();
                        AutoDTable.Rows[i]["ExistingCode"] = MEmployeeDS.Rows[i]["ExistingCode"].ToString();
                        AutoDTable.Rows[i]["DeptName"] = MEmployeeDS.Rows[i]["DeptName"].ToString();
                        AutoDTable.Rows[i]["FirstName"] = MEmployeeDS.Rows[i]["FirstName"].ToString();
                        AutoDTable.Rows[i]["Designation"] = MEmployeeDS.Rows[i]["Designation"].ToString();



                    }

                    //if (SessionUserType == "2")
                    //{
                    //    Non_DayAttndBWDate(); 
                        
                    //}
                    //else
                    //{
                    //    DayAttndBWDates(); 
                    //}
                    DayAttndBWDates(); 

                    grid.DataSource = mEmployeeDT;
                    grid.DataBind();
                    string attachment = "attachment;filename=DAY ATTENDANCE BETWEEN DATES.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    grid.HeaderStyle.Font.Bold = true;
                    System.IO.StringWriter stw = new System.IO.StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    grid.RenderControl(htextw);
                    Response.Write("<table>");
                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td colspan='10'>");
                    Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
                    Response.Write("--");
                    Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td colspan='10'>");
                    Response.Write("<a style=\"font-weight:bold\">DAY ATTENANCE BETWEEN DATES REPORT &nbsp;&nbsp;&nbsp;</a>");

                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td colspan='10'>");
                    Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
                    Response.Write("&nbsp;&nbsp;&nbsp;");
                    Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("</table>");
                    Response.Write(stw.ToString());
                    Response.End();
                    Response.Clear();

                

            }

        }
    }

    public void Non_DayAttndBWDate()
    {
        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        bool isPresent = false;
        int intI = 0;
        int intK = 0;
       
        string halfPresent = "0";


        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            mEmployeeDT.Columns.Add(Convert.ToString(dayy.ToShortDateString()));
                       
            daycount -= 1;
            daysAdded += 1;
        }
        
        mEmployeeDT.Columns.Add("Total Days");




for (int j1 = 0; j1 < AutoDTable.Rows.Count; j1++)
{
    intI = 4;
    double Worked_Days_Count = 0;
    mEmployeeDT.NewRow();
    mEmployeeDT.Rows.Add();
    mEmployeeDT.Rows[intK]["EmployeeNo"] = AutoDTable.Rows[j1]["EmployeeNo"].ToString();
    mEmployeeDT.Rows[intK]["ExistingCode"] = AutoDTable.Rows[j1]["ExistingCode"].ToString();
    mEmployeeDT.Rows[intK]["Name"] = AutoDTable.Rows[j1]["FirstName"].ToString();
    mEmployeeDT.Rows[intK]["DeptName"] = AutoDTable.Rows[j1]["DeptName"].ToString();


        for (int intCol = 0; intCol <= daysAdded - 1; intCol++)
        {

            isPresent = false;
            string Machine_ID_Str = "";
            string OT_Week_OFF_Machine_No = "";
            string Date_Value_Str = "";
            DataTable mLocalDS = new DataTable();
            string Time_IN_Str = "";
            string Time_Out_Str = "";
            string Total_Time_get = "";
            Int32 j = 0;
            double time_Check_dbl = 0;
            //Dim Emp_Total_Work_Time_1 As Double
            string Emp_Total_Work_Time_1 = "00:00";
            isPresent = false;

            Machine_ID_Str = AutoDTable.Rows[j1]["MachineID"].ToString();
            OT_Week_OFF_Machine_No = AutoDTable.Rows[j1]["EmployeeNo"].ToString();

            DateTime dtime = date1.AddDays(intCol);
            DateTime dtime1 = dtime.AddDays(1);

            Date_Value_Str = dtime.ToString("yyyy/MM/dd");
            Date_value_str1 = dtime1.ToString("yyyy/MM/dd");


           
            DataTable mLocalDS1 = new DataTable();
            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_value_str1 + " " + "02:00' Order by TimeIN ASC";
            mLocalDS = objdata.ReturnMultipleValue(SSQL);
            if (mLocalDS.Rows.Count <= 0)
            {
                Time_IN_Str = "";
            }
            else
            {
                //Time_IN_Str = mLocalDS.Tables(0).Rows(0)(0)
            }
            //TimeOUT Get
            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_value_str1 + " " + "10:00' Order by TimeOUT Asc";
            mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
            if (mLocalDS.Rows.Count <= 0)
            {
                Time_Out_Str = "";
            }
            else
            {
                //Time_Out_Str = mLocalDS.Tables(0).Rows(0)(0)
            }

            //Shift Change Check Start
            DateTime InTime_Check = default(DateTime);
            DateTime InToTime_Check = default(DateTime);
            TimeSpan InTime_TimeSpan = default(TimeSpan);
            string From_Time_Str = "";
            string To_Time_Str = "";
            DataTable DS_Time = new DataTable();
            DataTable DS_InTime = new DataTable();
            string Final_InTime = "";
            string Final_OutTime = "";
            string Final_Shift = "";
            DataTable Shift_DS_Change = new DataTable();
            Int32 K = 0;
            bool Shift_Check_blb = false;
            string Shift_Start_Time_Change = null;
            string Shift_End_Time_Change = null;
            string Employee_Time_Change = "";
            DateTime ShiftdateStartIN_Change = default(DateTime);
            DateTime ShiftdateEndIN_Change = default(DateTime);
            DateTime EmpdateIN_Change = default(DateTime);

            if (mLocalDS.Rows.Count != 0)
                    {
                        InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
                        InToTime_Check = InTime_Check.AddHours(2);


                        string s1 = InTime_Check.ToString("HH:mm:ss");
                        string s2 = InToTime_Check.ToString("HH:mm:ss");

                        string InTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InTime_Check);
                        string InToTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InToTime_Check);
                        InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
                        From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                        InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
                        To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;

                

                        //Two Hours OutTime Check
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + From_Time_Str + "' And TimeOUT <='" + Date_Value_Str + " " + To_Time_Str + "' Order by TimeOUT Asc";
                        DS_Time = objdata.ReturnMultipleValue(SSQL);
                        if (DS_Time.Rows.Count != 0)
                        {
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + To_Time_Str + "' And TimeIN <='" + Date_value_str1 + " " + "02:00' Order by TimeIN ASC";
                            DS_InTime = objdata.ReturnMultipleValue(SSQL);
                            if (DS_InTime.Rows.Count != 0)
                            {
                                Final_InTime = DS_InTime.Rows[0][0].ToString();
                                //Check With IN Time Shift
                                SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                                Shift_DS_Change = objdata.ReturnMultipleValue(SSQL);
                                Shift_Check_blb = false;
                                for (K = 0; K <= Shift_DS_Change.Rows.Count - 1; K++)
                                {
                                    Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
                                    if (Shift_DS_Change.Rows[K]["EndIN_Days"].ToString() == "1")
                                    {
                                        Shift_End_Time_Change = Date_value_str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                    }
                                    else
                                    {
                                        Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                    }

                                    ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                    ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                    EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                    if (EmpdateIN_Change >= ShiftdateStartIN_Change & EmpdateIN_Change <= ShiftdateEndIN_Change)
                                    {
                                        Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                        break; // TODO: might not be correct. Was : Exit For
                                    }
                                }
                                if (Shift_Check_blb == true)
                                {
                                    //IN Time Query Update
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + To_Time_Str + "' And TimeIN <='" + Date_value_str1 + " " + "02:00' Order by TimeIN ASC";
                                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                                    if (Final_Shift == "SHIFT2")
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_value_str1 + " " + "10:00' Order by TimeOUT Asc";
                                    }
                                    else
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "17:00' And TimeOUT <='" + Date_value_str1 + " " + "10:00' Order by TimeOUT Asc";
                                    }
                                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                                }
                                else
                                {
                                    //Get Employee In Time
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_value_str1 + " " + "02:00' Order by TimeIN ASC";
                                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                                    //TimeOUT Get
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_value_str1 + " " + "10:00' Order by TimeOUT Asc";
                                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                                }
                            }
                            else
                            {
                                //Get Employee In Time
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_value_str1 + " " + "02:00' Order by TimeIN ASC";
                                mLocalDS = objdata.ReturnMultipleValue(SSQL);

                                //TimeOUT Get
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_value_str1 + " " + "10:00' Order by TimeOUT Asc";
                                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                            }
                        }
                        else
                        {
                            //Get Employee In Time
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_value_str1 + " " + "02:00' Order by TimeIN ASC";
                            mLocalDS = objdata.ReturnMultipleValue(SSQL);

                            //TimeOUT Get
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_value_str1 + " " + "10:00' Order by TimeOUT Asc";
                            mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                        }
                        //New Code End
                    }
             //'Shift Change Check End

              if (mLocalDS.Rows.Count > 1)
                    {
                        for (int tin = 0; tin <= mLocalDS.Rows.Count - 1; tin++)
                        {

                            //Shift Starting Time Calculation 
                            if (Shift_Check_blb == true)
                            {
                                SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc='" + Final_Shift + "'";
                                Shift_DS_Change = objdata.ReturnMultipleValue(SSQL);
                                Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[0]["StartTime"].ToString();
                                ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);



                                if (EmpdateIN_Change <= ShiftdateStartIN_Change)
                                {
                                    Time_IN_Str = ShiftdateStartIN_Change.ToString();
                                    Shift_Check_blb = false;
                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                }
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                            }
                            

                           

                            if (mLocalDS1.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_Out_Str = "";
                            }
                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                
                            }
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date11 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                //If date1 > date2 Then

                                //    date2 = System.Convert.ToDateTime(mLocalDS1.Tables(0).Rows(tin + 1)(0))
                                //End If
                                TimeSpan ts = new TimeSpan();
                                ts = date2.Subtract(date11);
                                ts = date2.Subtract(date11);
                                Total_Time_get = ts.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                ts4 = ts4.Add(ts);
                                //OT Time Get
                                Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Emp_Total_Work_Time_1 = "00:00";
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    Emp_Total_Work_Time_1 = "00:00";
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = ts.Hours.ToString();
                                    //& ":" & Trim(ts.Minutes)
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts.Hours.ToString() + ":" + ts.Minutes.ToString();
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        Emp_Total_Work_Time_1 = "00:00";
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        Emp_Total_Work_Time_1 = "00:00";
                                }
                                else
                                {
                                    time_Check_dbl = time_Check_dbl + Convert.ToInt16(Total_Time_get);
                                }
                            }
                        }
                    }
            else
                    {
                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout <= mLocalDS1.Rows.Count - 1; tout++)
                        {
                            if (mLocalDS1.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";
                            }
                            else
                            {
                                Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                            }

                        }
                        //Emp_Total_Work_Time
                        if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date11 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts = new TimeSpan();
                            ts = date2.Subtract(date11);
                            ts = date2.Subtract(date11);
                            Total_Time_get = ts.Hours.ToString();
                            //& ":" & Trim(ts.Minutes)
                            ts4 = ts4.Add(ts);
                            //OT Time Get
                            Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                Emp_Total_Work_Time_1 = "00:00";
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                Emp_Total_Work_Time_1 = "00:00";
                            //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date2.Subtract(date11);
                                ts = date2.Subtract(date11);
                                Total_Time_get = ts.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                Emp_Total_Work_Time_1 = ts.Hours.ToString() + ":" + ts.Minutes.ToString();
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Emp_Total_Work_Time_1 = "00:00";
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    Emp_Total_Work_Time_1 = "00:00";
                            }
                            else
                            {
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                            }
                        }
                    }
              if (OT_Week_OFF_Machine_No == "9515")
              {
                  OT_Week_OFF_Machine_No = "9515";
              }
                    double Calculate_Attd_Work_Time = 0;
                    double Calculate_Attd_Work_Time_1 = 4.0;
                    SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and Calculate_Work_Hours <> '' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Calculate_Attd_Work_Time = 7;
                    }
                    else
                    {
                        Calculate_Attd_Work_Time = (!string.IsNullOrEmpty(mLocalDS.Rows[0]["Calculate_Work_Hours"].ToString()) ? Convert.ToInt16(mLocalDS.Rows[0]["Calculate_Work_Hours"]) : 0);
                        if (Calculate_Attd_Work_Time == 0)
                        {
                            Calculate_Attd_Work_Time = 7;
                        }
                        else
                        {
                            //Calculate_Attd_Work_Time
                            Calculate_Attd_Work_Time = 7;
                        }
                    }
                    SSQL = "Select * from Employee_MST where Wages='" + WagesType + "' And MachineID='" + OT_Week_OFF_Machine_No + "' and  CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                    if (mLocalDS.Rows.Count != 0)
                    {
                        Calculate_Attd_Work_Time = 6;
                    }


                    string qry_nfhcheck = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                    mLocalDS = objdata.ReturnMultipleValue(qry_nfhcheck);
                    bool NFH_CHeck_Bol = false;
                    bool Half_Day_Count_Check = false;
                    NFH_CHeck_Bol = false;
                    if (mLocalDS.Rows.Count > 0)
                    {
                        NFH_CHeck_Bol = true;
                    }

                    halfPresent = "0";
                    Half_Day_Count_Check = false;
                    if (NFH_CHeck_Bol == true)
                    {
                        //Skip
                        if (time_Check_dbl >= Calculate_Attd_Work_Time_1 & time_Check_dbl < Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "1";
                            Half_Day_Count_Check = true;
                        }
                        if (time_Check_dbl >= Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "0";
                        }
                    }
                    else
                    {
                        if (time_Check_dbl >= Calculate_Attd_Work_Time_1 & time_Check_dbl < Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "1";
                            Half_Day_Count_Check = true;
                            Worked_Days_Count = Worked_Days_Count + 0.5;
                        }
                        if (time_Check_dbl >= Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "0";
                            Worked_Days_Count = Worked_Days_Count + 1;
                        }
                    }

             //Find Week OFF

                    //Get Employee Week OF DAY
                    DataTable DS_WH = new DataTable();
                    string Emp_WH_Day = "";
                    string Employee_Week_Name = "";
                    string Assign_Week_Name = "";

                    Employee_Week_Name = dtime.DayOfWeek.ToString();
                    SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    DS_WH = objdata.ReturnMultipleValue(SSQL);
                    if (DS_WH.Rows.Count != 0)
                    {
                        Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                    }
                    else
                    {
                        Emp_WH_Day = "";
                    }




                    string qry_nfh = "Select WeekOff from Employee_MST where WeekOff='" + Employee_Week_Name + "' and MachineID='" + OT_Week_OFF_Machine_No + "'";
                    mLocalDS = objdata.ReturnMultipleValue(qry_nfh);
                    if (mLocalDS.Rows.Count > 0)
                    {



                        //.Cells(intI, intK).value = "NH"
                        if (isPresent == true)
                        {
                            if (halfPresent == "0")
                            {
                                int red = 255;
                                int green = 255;
                                int blue = 255;

                                string hexColor = "#" + blue.ToString("X") + green.ToString("X") + blue.ToString("X");

                                mEmployeeDT.Rows[intK][intI] =   "WH";
                                Worked_Days_Count = Worked_Days_Count - 1;
                               
                            }
                            else if (Half_Day_Count_Check == true)
                            {
                                mEmployeeDT.Rows[intK][intI] = "WH";
                                Worked_Days_Count = Worked_Days_Count - 0.5;

                            }
                        }
                        else
                        {
                            mEmployeeDT.Rows[intK][intI] = "WH";
                          
                        }
                       

                    }
                   else
                        {
                            if (isPresent == true)
                            {
                                if (halfPresent == "0")
                                {
                                    int red = 255;
                                    int green = 255;
                                    int blue = 255;

                                    //string hexColor = "#" + blue.ToString("X") + green.ToString("X") + blue.ToString("X");
                                    //mEmployeeDT.Rows[intK][intI] = System.Drawing.ColorTranslator.FromHtml("X" + hexColor);

                                    //mEmployeeDT.Rows[intK][intI] = System.Drawing.ColorTranslator.FromHtml("#E9E9E9");

                                    mEmployeeDT.Rows[intK][intI] = "<span style=color:green><b>" + "X" + "</b></span>";

                                    // mEmployeeDT.Rows[intK][intI] = "X";
                                    //.Cells(intI, intK).Font.Name = "Webdings"
                                    //_with1.Cells(intI, intK).Font.Bold = true;
                                    //_with1.Cells(intI, intK).Font.Size = 10;
                                    //ElseIf halfPresent = "1" Then
                                }
                                else if (Half_Day_Count_Check == true)
                                {
                                    mEmployeeDT.Rows[intK][intI] = "H";
                                    //.Cells(intI, intK).Font.Name = "Webdings"
                                    //_with1.Cells(intI, intK).Font.Bold = true;
                                    //_with1.Cells(intI, intK).Font.Size = 10;
                                }

                            }
                            else
                            {
                                //Check Leave Master
                                DataTable Leave_DS = new DataTable();
                                string Leave_Short_Name = "A";
                                SSQL = "Select * from Leave_Register_Mst where Machine_No='" + OT_Week_OFF_Machine_No + "'";
                                SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And From_Date_Dt >='" + Date_Value_Str + "'";
                                SSQL = SSQL + " And To_Date_Dt <='" + Date_Value_Str + "'";
                                Leave_DS = objdata.ReturnMultipleValue(SSQL);
                                if (Leave_DS.Rows.Count != 0)
                                {
                                    //Get Leave Short Name
                                    SSQL = "Select * from LeaveType_Mst Where LeaveType='" + Leave_DS.Rows[0]["LeaveType"] + "'";
                                    Leave_DS = objdata.ReturnMultipleValue(SSQL);
                                    if (Leave_DS.Rows.Count != 0)
                                        Leave_Short_Name = Leave_DS.Rows[0]["Short_Name"].ToString();
                                    mEmployeeDT.Rows[intK][intI] = Leave_Short_Name;
                                }
                                else
                                {
                                    mEmployeeDT.Rows[intK][intI] = "<span style=color:red><b>"+ "A" +"</b></span>"; 
                                }

                                //.Cells(intI, intK).value = "A"
                                //.Cells(intI, intK).Font.Bold = True
                                //.cells(intI, intK).Font.Color = RGB(255, 0, 0)
                                //_with1.cells(intI, intK).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                                //_with1.Cells(intI, intK).Font.Size = 10;
                            }
                        }
                    
   
                    intI += 1;


        }
         //Total Days Added
                mEmployeeDT.Rows[intK][intI] = Worked_Days_Count;
                //_with1.Cells(intI, intK).Font.Bold = true;
                //_with1.Cells(intI, intK).Font.Size = 10;

                intK += 1;
}


    }

    public void DayAttndBWDates()
    {
        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        bool isPresent = false;
        int intI = 0;
        int intK = 0;
       
        string halfPresent = "0";


        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            mEmployeeDT.Columns.Add(Convert.ToString(dayy.ToShortDateString()));
                       
            daycount -= 1;
            daysAdded += 1;
        }
        
        mEmployeeDT.Columns.Add("Total Days");




for (int j1 = 0; j1 < AutoDTable.Rows.Count; j1++)
{
    intI = 4;
    double Worked_Days_Count = 0;
    mEmployeeDT.NewRow();
    mEmployeeDT.Rows.Add();
    mEmployeeDT.Rows[intK]["EmployeeNo"] = AutoDTable.Rows[j1]["EmployeeNo"].ToString();
    mEmployeeDT.Rows[intK]["ExistingCode"] = AutoDTable.Rows[j1]["ExistingCode"].ToString();
    mEmployeeDT.Rows[intK]["Name"] = AutoDTable.Rows[j1]["FirstName"].ToString();
    mEmployeeDT.Rows[intK]["DeptName"] = AutoDTable.Rows[j1]["DeptName"].ToString();


        for (int intCol = 0; intCol <= daysAdded - 1; intCol++)
        {

            isPresent = false;
            string Machine_ID_Str = "";
            string OT_Week_OFF_Machine_No = "";
            string Date_Value_Str = "";
            DataTable mLocalDS = new DataTable();
            string Time_IN_Str = "";
            string Time_Out_Str = "";
            string Total_Time_get = "";
            Int32 j = 0;
            double time_Check_dbl = 0;
            //Dim Emp_Total_Work_Time_1 As Double
            string Emp_Total_Work_Time_1 = "00:00";
            isPresent = false;

            Machine_ID_Str = AutoDTable.Rows[j1]["MachineID"].ToString();
            OT_Week_OFF_Machine_No = AutoDTable.Rows[j1]["EmployeeNo"].ToString();

            DateTime dtime = date1.AddDays(intCol);
            DateTime dtime1 = dtime.AddDays(1);

            Date_Value_Str = dtime.ToString("yyyy/MM/dd");
            Date_value_str1 = dtime1.ToString("yyyy/MM/dd");


            //Get Employee Week OF DAY
            DataTable DS_WH = new DataTable();
            string Emp_WH_Day = "";
            SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DS_WH = objdata.ReturnMultipleValue(SSQL);
            if (DS_WH.Rows.Count != 0)
            {
                Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
            }
            else
            {
                Emp_WH_Day = "";
            }

            DataTable mLocalDS1 = new DataTable();
            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_value_str1 + " " + "02:00' Order by TimeIN ASC";
            mLocalDS = objdata.ReturnMultipleValue(SSQL);
            if (mLocalDS.Rows.Count <= 0)
            {
                Time_IN_Str = "";
            }
            else
            {
                //Time_IN_Str = mLocalDS.Tables(0).Rows(0)(0)
            }
            //TimeOUT Get
            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_value_str1 + " " + "10:00' Order by TimeOUT Asc";
            mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
            if (mLocalDS.Rows.Count <= 0)
            {
                Time_Out_Str = "";
            }
            else
            {
                //Time_Out_Str = mLocalDS.Tables(0).Rows(0)(0)
            }

            //Shift Change Check Start
            DateTime InTime_Check = default(DateTime);
            DateTime InToTime_Check = default(DateTime);
            TimeSpan InTime_TimeSpan = default(TimeSpan);
            string From_Time_Str = "";
            string To_Time_Str = "";
            DataTable DS_Time = new DataTable();
            DataTable DS_InTime = new DataTable();
            string Final_InTime = "";
            string Final_OutTime = "";
            string Final_Shift = "";
            DataTable Shift_DS_Change = new DataTable();
            Int32 K = 0;
            bool Shift_Check_blb = false;
            string Shift_Start_Time_Change = null;
            string Shift_End_Time_Change = null;
            string Employee_Time_Change = "";
            DateTime ShiftdateStartIN_Change = default(DateTime);
            DateTime ShiftdateEndIN_Change = default(DateTime);
            DateTime EmpdateIN_Change = default(DateTime);

            if (mLocalDS.Rows.Count != 0)
                    {
                        InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
                        InToTime_Check = InTime_Check.AddHours(2);


                        string s1 = InTime_Check.ToString("HH:mm:ss");
                        string s2 = InToTime_Check.ToString("HH:mm:ss");

                        string InTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InTime_Check);
                        string InToTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InToTime_Check);
                        InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
                        From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                        InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
                        To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;

                

                        //Two Hours OutTime Check
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + From_Time_Str + "' And TimeOUT <='" + Date_Value_Str + " " + To_Time_Str + "' Order by TimeOUT Asc";
                        DS_Time = objdata.ReturnMultipleValue(SSQL);
                        if (DS_Time.Rows.Count != 0)
                        {
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + To_Time_Str + "' And TimeIN <='" + Date_value_str1 + " " + "02:00' Order by TimeIN ASC";
                            DS_InTime = objdata.ReturnMultipleValue(SSQL);
                            if (DS_InTime.Rows.Count != 0)
                            {
                                Final_InTime = DS_InTime.Rows[0][0].ToString();
                                //Check With IN Time Shift
                                SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                                Shift_DS_Change = objdata.ReturnMultipleValue(SSQL);
                                Shift_Check_blb = false;
                                for (K = 0; K <= Shift_DS_Change.Rows.Count - 1; K++)
                                {
                                    Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
                                    if (Shift_DS_Change.Rows[K]["EndIN_Days"].ToString() == "1")
                                    {
                                        Shift_End_Time_Change = Date_value_str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                    }
                                    else
                                    {
                                        Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                    }

                                    ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                    ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                    EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                    if (EmpdateIN_Change >= ShiftdateStartIN_Change & EmpdateIN_Change <= ShiftdateEndIN_Change)
                                    {
                                        Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                        break; // TODO: might not be correct. Was : Exit For
                                    }
                                }
                                if (Shift_Check_blb == true)
                                {
                                    //IN Time Query Update
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + To_Time_Str + "' And TimeIN <='" + Date_value_str1 + " " + "02:00' Order by TimeIN ASC";
                                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                                    if (Final_Shift == "SHIFT2")
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_value_str1 + " " + "10:00' Order by TimeOUT Asc";
                                    }
                                    else
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "17:00' And TimeOUT <='" + Date_value_str1 + " " + "10:00' Order by TimeOUT Asc";
                                    }
                                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                                }
                                else
                                {
                                    //Get Employee In Time
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_value_str1 + " " + "02:00' Order by TimeIN ASC";
                                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                                    //TimeOUT Get
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_value_str1 + " " + "10:00' Order by TimeOUT Asc";
                                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                                }
                            }
                            else
                            {
                                //Get Employee In Time
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_value_str1 + " " + "02:00' Order by TimeIN ASC";
                                mLocalDS = objdata.ReturnMultipleValue(SSQL);

                                //TimeOUT Get
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_value_str1 + " " + "10:00' Order by TimeOUT Asc";
                                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                            }
                        }
                        else
                        {
                            //Get Employee In Time
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_value_str1 + " " + "02:00' Order by TimeIN ASC";
                            mLocalDS = objdata.ReturnMultipleValue(SSQL);

                            //TimeOUT Get
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_value_str1 + " " + "10:00' Order by TimeOUT Asc";
                            mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                        }
                        //New Code End
                    }
             //'Shift Change Check End

              if (mLocalDS.Rows.Count > 1)
                    {
                        for (int tin = 0; tin <= mLocalDS.Rows.Count - 1; tin++)
                        {

                            //Shift Starting Time Calculation 
                            if (Shift_Check_blb == true)
                            {
                                SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc='" + Final_Shift + "'";
                                Shift_DS_Change = objdata.ReturnMultipleValue(SSQL);
                                Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[0]["StartTime"].ToString();
                                ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);



                                if (EmpdateIN_Change <= ShiftdateStartIN_Change)
                                {
                                    Time_IN_Str = ShiftdateStartIN_Change.ToString();
                                    Shift_Check_blb = false;
                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                }
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                            }
                            

                           

                            if (mLocalDS1.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_Out_Str = "";
                            }
                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                
                            }
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date11 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                //If date1 > date2 Then

                                //    date2 = System.Convert.ToDateTime(mLocalDS1.Tables(0).Rows(tin + 1)(0))
                                //End If
                                TimeSpan ts = new TimeSpan();
                                ts = date2.Subtract(date11);
                                ts = date2.Subtract(date11);
                                Total_Time_get = ts.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                ts4 = ts4.Add(ts);
                                //OT Time Get
                                Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Emp_Total_Work_Time_1 = "00:00";
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    Emp_Total_Work_Time_1 = "00:00";
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = ts.Hours.ToString();
                                    //& ":" & Trim(ts.Minutes)
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts.Hours.ToString() + ":" + ts.Minutes.ToString();
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        Emp_Total_Work_Time_1 = "00:00";
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        Emp_Total_Work_Time_1 = "00:00";
                                }
                                else
                                {
                                    time_Check_dbl = time_Check_dbl + Convert.ToInt16(Total_Time_get);
                                }
                            }
                        }
                    }
            else
                    {
                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout <= mLocalDS1.Rows.Count - 1; tout++)
                        {
                            if (mLocalDS1.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";
                            }
                            else
                            {
                                Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                            }

                        }
                        //Emp_Total_Work_Time
                        if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date11 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts = new TimeSpan();
                            ts = date2.Subtract(date11);
                            ts = date2.Subtract(date11);
                            Total_Time_get = ts.Hours.ToString();
                            //& ":" & Trim(ts.Minutes)
                            ts4 = ts4.Add(ts);
                            //OT Time Get
                            Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                Emp_Total_Work_Time_1 = "00:00";
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                Emp_Total_Work_Time_1 = "00:00";
                            //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date2.Subtract(date11);
                                ts = date2.Subtract(date11);
                                Total_Time_get = ts.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                Emp_Total_Work_Time_1 = ts.Hours.ToString() + ":" + ts.Minutes.ToString();
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Emp_Total_Work_Time_1 = "00:00";
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    Emp_Total_Work_Time_1 = "00:00";
                            }
                            else
                            {
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                            }
                        }
                    }
                    double Calculate_Attd_Work_Time = 0;
                    double Calculate_Attd_Work_Time_1 = 4.0;
                    SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and Calculate_Work_Hours <> '' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Calculate_Attd_Work_Time = 7;
                    }
                    else
                    {
                        Calculate_Attd_Work_Time = (!string.IsNullOrEmpty(mLocalDS.Rows[0]["Calculate_Work_Hours"].ToString()) ? Convert.ToInt16(mLocalDS.Rows[0]["Calculate_Work_Hours"]) : 0);
                        if (Calculate_Attd_Work_Time == 0)
                        {
                            Calculate_Attd_Work_Time = 7;
                        }
                        else
                        {
                            //Calculate_Attd_Work_Time
                            Calculate_Attd_Work_Time = 7;
                        }
                    }
                    SSQL = "Select * from Employee_MST where Wages='" + WagesType + "' And MachineID='" + OT_Week_OFF_Machine_No + "' and  CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                    if (mLocalDS.Rows.Count != 0)
                    {
                        Calculate_Attd_Work_Time = 6;
                    }


                    string qry_nfhcheck = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                    mLocalDS = objdata.ReturnMultipleValue(qry_nfhcheck);
                    bool NFH_CHeck_Bol = false;
                    bool Half_Day_Count_Check = false;
                    NFH_CHeck_Bol = false;
                    if (mLocalDS.Rows.Count > 0)
                    {
                        NFH_CHeck_Bol = true;
                    }

                    halfPresent = "0";
                    Half_Day_Count_Check = false;
                    if (NFH_CHeck_Bol == true)
                    {
                        //Skip
                        if (time_Check_dbl >= Calculate_Attd_Work_Time_1 & time_Check_dbl < Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "1";
                            Half_Day_Count_Check = true;
                        }
                        if (time_Check_dbl >= Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "0";
                        }
                    }
                    else
                    {
                        if (time_Check_dbl >= Calculate_Attd_Work_Time_1 & time_Check_dbl < Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "1";
                            Half_Day_Count_Check = true;
                            Worked_Days_Count = Worked_Days_Count + 0.5;
                        }
                        if (time_Check_dbl >= Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "0";
                            Worked_Days_Count = Worked_Days_Count + 1;
                        }
                    }

             //Find Week OFF
                    string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                    mLocalDS = objdata.ReturnMultipleValue(qry_nfh);
                    if (mLocalDS.Rows.Count > 0)
                    {
                        //.Cells(intI, intK).value = "NH"
                        if (isPresent == true)
                        {
                            if (halfPresent == "0")
                            {
                                int red = 255;
                                int green = 255;
                                int blue = 255;

                                string hexColor = "#" + blue.ToString("X") + green.ToString("X") + blue.ToString("X");

                                mEmployeeDT.Rows[intK][intI] =   "NH / X";

                                //mEmployeeDT.Rows[intK][intI] =  "<span style=color:red>" + "NH / X" + "</span>";


                                //"<span style=color:red>" + AutoDTable.Rows[i][day_col2] + "</span>";
                               
                            }
                            else if (Half_Day_Count_Check == true)
                            {
                                mEmployeeDT.Rows[intK][intI] = "NH / H";

                                ////_with1.cells(intI, intK).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green);

                                //mEmployeeDT.Rows[intK][intI] = "<span style=color:red>" + "NH / H" + "</span>";
                            
                            }
                        }
                        else
                        {
                            mEmployeeDT.Rows[intK][intI] = "NH / A";
                            //_with1.cells(intI, intK).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);

                            //mEmployeeDT.Rows[intK][intI] = "<span style=color:red>" + "NH / A" + "</span>";
                        }
                       

                    }
                   else
                        {
                            if (isPresent == true)
                            {
                                if (halfPresent == "0")
                                {
                                    int red = 255;
                                    int green = 255;
                                    int blue = 255;

                                    //string hexColor = "#" + blue.ToString("X") + green.ToString("X") + blue.ToString("X");
                                    //mEmployeeDT.Rows[intK][intI] = System.Drawing.ColorTranslator.FromHtml("X" + hexColor);

                                    //mEmployeeDT.Rows[intK][intI] = System.Drawing.ColorTranslator.FromHtml("#E9E9E9");

                                    mEmployeeDT.Rows[intK][intI] = "<span style=color:green><b>" + "X" + "</b></span>";

                                    // mEmployeeDT.Rows[intK][intI] = "X";
                                    //.Cells(intI, intK).Font.Name = "Webdings"
                                    //_with1.Cells(intI, intK).Font.Bold = true;
                                    //_with1.Cells(intI, intK).Font.Size = 10;
                                    //ElseIf halfPresent = "1" Then
                                }
                                else if (Half_Day_Count_Check == true)
                                {
                                    mEmployeeDT.Rows[intK][intI] = "H";
                                    //.Cells(intI, intK).Font.Name = "Webdings"
                                    //_with1.Cells(intI, intK).Font.Bold = true;
                                    //_with1.Cells(intI, intK).Font.Size = 10;
                                }

                            }
                            else
                            {
                                //Check Leave Master
                                DataTable Leave_DS = new DataTable();
                                string Leave_Short_Name = "A";
                                SSQL = "Select * from Leave_Register_Mst where Machine_No='" + OT_Week_OFF_Machine_No + "'";
                                SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And From_Date_Dt >='" + Date_Value_Str + "'";
                                SSQL = SSQL + " And To_Date_Dt <='" + Date_Value_Str + "'";
                                Leave_DS = objdata.ReturnMultipleValue(SSQL);
                                if (Leave_DS.Rows.Count != 0)
                                {
                                    //Get Leave Short Name
                                    SSQL = "Select * from LeaveType_Mst Where LeaveType='" + Leave_DS.Rows[0]["LeaveType"] + "'";
                                    Leave_DS = objdata.ReturnMultipleValue(SSQL);
                                    if (Leave_DS.Rows.Count != 0)
                                        Leave_Short_Name = Leave_DS.Rows[0]["Short_Name"].ToString();
                                    mEmployeeDT.Rows[intK][intI] = Leave_Short_Name;
                                }
                                else
                                {
                                    mEmployeeDT.Rows[intK][intI] = "<span style=color:red><b>"+ "A" +"</b></span>"; 
                                }

                                //.Cells(intI, intK).value = "A"
                                //.Cells(intI, intK).Font.Bold = True
                                //.cells(intI, intK).Font.Color = RGB(255, 0, 0)
                                //_with1.cells(intI, intK).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                                //_with1.Cells(intI, intK).Font.Size = 10;
                            }
                        }
                    
   
                    intI += 1;


        }
         //Total Days Added
                mEmployeeDT.Rows[intK][intI] = Worked_Days_Count;
                //_with1.Cells(intI, intK).Font.Bold = true;
                //_with1.Cells(intI, intK).Font.Size = 10;

                intK += 1;
}


}
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    public string Right_Val(string Value, int Length)
    {
        // Recreate a RIGHT function for string manipulation
        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }

}
