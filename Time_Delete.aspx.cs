﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Globalization;

public partial class Time_Delete : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    TimeDelete objUsercreation = new TimeDelete();
    string Machine_ID_Encrypt;
    string SSQL;
    DateTime AtteDate;
    string ss;
    string ss1;
    DataTable AutoDataTable = new DataTable();
    DataTable AutoDataTableNew = new DataTable();
    bool ErrFlag = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Time Delete";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("LeaveManagement"));
                li.Attributes.Add("class", "droplink active open");
                DropDown_TokenNumber();

                DisplayDataTable();
            }
        }
    }

    public void DisplayDataTable()
    {
        rptrCustomer.DataSource = AutoDataTable;
        rptrCustomer.DataBind();

        rptrLogtimeOUT.DataSource = AutoDataTableNew;
        rptrLogtimeOUT.DataBind();
    }


    public void LogTimeINFunction()
    {

        if (ddlTicketNo.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Your TicketNo');", true);
            ErrFlag = true;
        }
        else
        {
            DataTable dtLogTimeIN = new DataTable();
            string s = ddlTicketNo.SelectedItem.Text;
            string[] delimiters = new string[] { "-->" };
            string[] items = s.Split(delimiters, StringSplitOptions.None);
            ss = items[0].Trim();
            ss1 = items[1].Trim();

            Machine_ID_Encrypt = UTF8Encryption(ss.ToString().Trim());

            string attdate = txtAttendDate.Text;
            AtteDate = Convert.ToDateTime(attdate.ToString());

            AutoDataTable.Columns.Add("EmpNo");
            AutoDataTable.Columns.Add("EmpName");
            AutoDataTable.Columns.Add("Date");
            AutoDataTable.Columns.Add("TimeIN");
            AutoDataTable.Columns.Add("TimeOUT");

            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Encrypt + "'";
            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And TimeIN >='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01" + "'";
            SSQL = SSQL + " And TimeIN <='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59" + "'";
            SSQL = SSQL + " Order by TimeIN ASC";

            dtLogTimeIN = objdata.ReturnMultipleValue(SSQL);

            if (dtLogTimeIN.Rows.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Records Found In IN Time');", true);
                rptrCustomer.DataSource = AutoDataTable;
                rptrCustomer.DataBind();
                ErrFlag = true;
            }
            else
            {
                Int32 DSVAL = 0;
                for (int i = 0; i < dtLogTimeIN.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();
                    AutoDataTable.Rows[DSVAL][0] = ss.ToString();
                    AutoDataTable.Rows[DSVAL][1] = txtName.Text;
                    AutoDataTable.Rows[DSVAL][2] = txtAttendDate.Text;
                    AutoDataTable.Rows[DSVAL][3] = dtLogTimeIN.Rows[i]["TimeIN"].ToString();

                    DSVAL += 1;
                }
                rptrCustomer.DataSource = AutoDataTable;
                rptrCustomer.DataBind();
            }
        }
    }
    public void LogTimeOUTFunction()
    {
        if (ddlTicketNo.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Your TicketNo');", true);
            ErrFlag = true;
        }
        else
        {

            DataTable dtLogTimeIN = new DataTable();
            string s = ddlTicketNo.SelectedItem.Text;
            string[] delimiters = new string[] { "-->" };
            string[] items = s.Split(delimiters, StringSplitOptions.None);
            ss = items[0];
            ss1 = items[1];

            Machine_ID_Encrypt = UTF8Encryption(ss.ToString().Trim());

            string attdate = txtAttendDate.Text;
            AtteDate = Convert.ToDateTime(attdate.ToString());

            AutoDataTableNew.Columns.Add("EmpNo");
            AutoDataTableNew.Columns.Add("EmpName");
            AutoDataTableNew.Columns.Add("Date");
            AutoDataTableNew.Columns.Add("TimeIN");
            AutoDataTableNew.Columns.Add("TimeOUT");

            DataTable dtLogTimeOUT = new DataTable();

            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Encrypt + "'";
            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And TimeOUT >='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01" + "'";
            SSQL = SSQL + " And TimeOUT <='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59" + "'";
            SSQL = SSQL + " Order by TimeOUT ASC";

            dtLogTimeOUT = objdata.ReturnMultipleValue(SSQL);

            if (dtLogTimeOUT.Rows.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Records Found In OUT Time');", true);
                rptrLogtimeOUT.DataSource = AutoDataTableNew;
                rptrLogtimeOUT.DataBind();
                ErrFlag = true;
            }
            else
            {
                Int32 DSVAL = 0;
                for (int i = 0; i < dtLogTimeOUT.Rows.Count; i++)
                {
                    AutoDataTableNew.NewRow();
                    AutoDataTableNew.Rows.Add();
                    AutoDataTableNew.Rows[DSVAL][0] = ss.ToString();
                    AutoDataTableNew.Rows[DSVAL][1] = txtName.Text;
                    AutoDataTableNew.Rows[DSVAL][2] = txtAttendDate.Text;
                    AutoDataTableNew.Rows[DSVAL][4] = dtLogTimeOUT.Rows[i]["TimeOUT"].ToString();

                    DSVAL += 1;
                }
                rptrLogtimeOUT.DataSource = AutoDataTableNew;
                rptrLogtimeOUT.DataBind();
            }

        }

    }


    public void DisplayTimeDelete()
    {
        LogTimeINFunction();
        LogTimeOUTFunction();
    }


    //public void Dropdown_Company()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.Dropdown_Company();
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //     ddlCompanyCode.Items.Add(dt.Rows[i]["Cname"].ToString());
    //    }
    //}

    //public void Dropdown_Location()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.dropdown_ParticularLocation(SessionCcode, SessionLcode);
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        ddlLocationcode.Items.Add(dt.Rows[i]["LocName"].ToString());
    //    }
    //}


    protected void rptrLogtimeOUT_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string id1 = commandArgs[0];
        string Date = commandArgs[1];
        //string TimeIn = commandArgs[1];
        string[] DateTime = Date.Split(' ');
        string id2 = DateTime[0] + " " + DateTime[1] + " " + DateTime[2];


        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterDataLogTimeOUT(id1, id2);
                break;

        }
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string id1 = commandArgs[0];
        string Date = commandArgs[1];
        //string TimeIn = commandArgs[1];
        string[] DateTime = Date.Split(' ');
        string id2 = DateTime[0] + " " + DateTime[1] + " " + DateTime[2];


        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(id1, id2);
                break;
        }
    }

    private void DeleteRepeaterData(string id1, string id2)
    {
        DataTable Dtd = new DataTable();
        string Encr_id = UTF8Encryption(id1.ToString().Trim());

        //string timein = Convert.ToString(id2);

        //DateTime Dateid = Convert.ToDateTime(id2);
        string DB_Time_Out = id2;
        string[] Time_Spilit_1 = DB_Time_Out.Split(':');
        string[] Time_Spilit_2 = Time_Spilit_1[0].Split(' ');
        string DB_Time_Out_Join = "";
        if (Time_Spilit_2[1].Length == 1)
        {
            Time_Spilit_2[1] = "0" + Time_Spilit_2[1];
            DB_Time_Out_Join = Time_Spilit_2[0] + " " + Time_Spilit_2[1] + ":" + Time_Spilit_1[1] + ":" + Time_Spilit_1[2];
        }
        else
        {
            DB_Time_Out_Join = DB_Time_Out;
        }

        var dateTime_Check = DateTime.ParseExact(DB_Time_Out_Join, "dd/MM/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
        var dateTimeString = dateTime_Check.ToString("dd/MM/yyyy HH:mm:ss");




        SSQL = "Delete from LogTime_IN where MachineID='" + Encr_id + "'";
        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And TimeIN =convert(datetime,'" + dateTimeString + "',103)";
        objdata.ReturnMultipleValue(SSQL);


        //SSQL = "Delete LogTime_IN where MachineID='" + Encr_id + "' and TimeIN='"+ id2 +"'";

        //objdata.ReturnMultipleValue(SSQL);
        DisplayTimeDelete();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);

        //DataTable dbInTime = new DataTable();
        //SSQL = "select * from LogTime_IN where MachineID='" + Encr_id + "' and TimeIN='" + Dateid.AddDays(0).ToString("yyyy/MM/dd") + "'";
        //dbInTime = objdata.ReturnMultipleValue(SSQL);
        //rptrCustomer.DataSource = dbInTime;
        //rptrCustomer.DataBind();

    }



    private void DeleteRepeaterDataLogTimeOUT(string id1, string id2)
    {
        DataTable Dtd = new DataTable();
        string Encr_id = UTF8Encryption(id1.ToString().Trim());


        //DateTime Dateid = System.DateTime.ParseExact("11/08/2016 06:00:00 PM", "dd/MM/yyyy HH:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);

        //DateTime date2 = System.Convert.ToDateTime(id2);

        //SSQL = "Delete LogTime_OUT where MachineID='" + Encr_id + "' and TimeOUT = '" + id2 +"'";

        SSQL = "Delete from  LogTime_OUT where MachineID='" + Encr_id + "'";
        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And TimeOUT =convert(datetime,'" + id2 + "',103)";

        objdata.ReturnMultipleValue(SSQL);
        DisplayTimeDelete();

        //DataTable dbOutTime = new DataTable();
        //SSQL = "select*from LogTime_OUT where MachineID='" + Encr_id + "' and TimeOUT = '" + id2 + "'";
        //dbOutTime = objdata.ReturnMultipleValue(SSQL);
        //rptrLogtimeOUT.DataSource = AutoDataTable;
        //rptrLogtimeOUT.DataBind();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
    }

    public void DropDown_TokenNumber()
    {
        DataTable dt = new DataTable();
        dt = objdata.dropdown_TokenNumber(SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {

            ddlTicketNo.DataSource = dt;
            DataRow dr = dt.NewRow();
            dr["EmpName"] = "- select -";
            dt.Rows.InsertAt(dr, 0);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlTicketNo.Items.Add(dt.Rows[i]["EmpName"].ToString());
            }
        }
    }

    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }



    protected void ddlTicketNo_SelectedIndexChanged(object sender, EventArgs e)
    {

        DataTable dt = new DataTable();
        string s = ddlTicketNo.SelectedItem.Text;
        string[] delimiters = new string[] { "-->" };
        string[] items = s.Split(delimiters, StringSplitOptions.None);
        string ss = items[0].Trim();
        string ss1 = items[1].Trim();
        txtName.Text = ss1.ToString();
        DataTable dted = new DataTable();
        dted = objdata.Manual_Data(ss.ToString());
        if (dted.Rows.Count > 0)
        {
            txtExistingCode.Text = dted.Rows[0]["ExistingCode"].ToString();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ddlTicketNo.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Your TicketNo');", true);
            ErrFlag = true;
        }

        DisplayTimeDelete();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ddlTicketNo.SelectedIndex = 0;
        txtExistingCode.Text = "";
        txtName.Text = "";
        txtAttendDate.Text = "";
    }
}

