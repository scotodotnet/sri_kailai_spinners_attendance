﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;

public partial class PayrollAttendance : System.Web.UI.Page
{

    DateTime dtime;
    DateTime dtime1;
    DateTime date1;
    DateTime date2 = new DateTime();
    string Encry_MachineID;
    DataTable AutoDataTable = new DataTable();
    bool isPresent = false;
    string Time_IN_Str;
    string Time_Out_Str;
    int j = 0;
    Double time_Check_dbl = 0;
    Double Hrs_Below = 0;
    string Date_Value_Str1;
    DateTime InTime_Check;
    DateTime InTime_Check1;
    DateTime InToTime_Check;
    TimeSpan InTime_TimeSpan;
    string From_Time_Str;

    DataTable mLocalDS = new DataTable();


    string Machine_ID_Str = "";
    string OT_Week_OFF_Machine_No = null;
    string Date_Value_Str = "";
    string Total_Time_get = "";
    string Final_OT_Work_Time_1 = "00:00";
    // Decimal Month_Mid_Join_Total_Days_Count;
    DateTime NewDate1;
    string[] Time_Minus_Value_Check;

    //DateTime InTime_Check = default(DateTime);
    //DateTime InToTime_Check = default(DateTime);
    //TimeSpan InTime_TimeSpan = default(TimeSpan);
    //string From_Time_Str = "";
    string To_Time_Str = "";
    DataTable DS_Time = new DataTable();
    DataTable DS_InTime = new DataTable();
    string Final_InTime = "";
    string Final_OutTime = "";
    string Final_Shift = "";
    DataTable Shift_DS_Change = new DataTable();
    Int32 K = 0;
    bool Shift_Check_blb = false;
    string Shift_Start_Time_Change = null;
    string Shift_End_Time_Change = null;
    string Employee_Time_Change = "";
    DateTime ShiftdateStartIN_Change = default(DateTime);
    DateTime ShiftdateEndIN_Change = default(DateTime);
    DateTime EmpdateIN_Change = default(DateTime);

    string Fin_Year;
    string Months_Full_Str;
    //string Months_Full_Str;
    string Date_Col_Str;
    string Month_Int;
    string Spin_Wages;
    string Wages;
    string Spin_Machine_ID_Str;
    string[] Att_Date_Check;
    string Att_Already_Date_Check;
    string Att_Year_Check;
    string Month_Name_Change;
    string halfPresent;

    //string Att_Year_Check = "";
    string Token_No_Get;
    TimeSpan ts4 = new TimeSpan();

    Decimal Total_Days_Count;
    double Final_Count;

    double Present_Count;
    double EHours_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;
    double FullNight_Shift_Count;
    double NFH_Week_Off_Minus_Days;

    Boolean NFH_Day_Check = false;
    Boolean Check_Week_Off_DOJ = false;
    int aintI = 1;
    int aintK = 1;
    int aintCol;
    int Appsent_Count;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionCompanyName;
    string SessionLocationName;

    //String Att_Already_Date_Check;
    //String Att_Year_Check;
    //String[] Att_Date_Check;
    BALDataAccess objdata = new BALDataAccess();
    int shiftCount = 0;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string todate;
    string fromdate;
    string SSQL;
    string Emp_WH_Day;
    //string Spin_Machine_ID_Str = "";

    DateTime WH_DOJ_Date_Format;
    DateTime Week_Off_Date;

    DataTable Datacells = new DataTable();
    DataTable mEmployeeDS = new DataTable();
    DataTable dsEmployee = new DataTable();
    // DataTable AutoDataTable = new DataTable();
    Double NFH_Final_Disp_Days;

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
 


    protected void Page_Load(object sender, EventArgs e)
    {

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCompanyName = Session["CompanyName"].ToString();
        SessionLocationName = Session["LocationName"].ToString();

        DataColumn auto = new DataColumn();
        auto.AutoIncrement = true;
        auto.AutoIncrementSeed = 1;


        AutoDataTable.Columns.Add("MachineID");
        AutoDataTable.Columns.Add("Token No");
        AutoDataTable.Columns.Add("EmpName");
        AutoDataTable.Columns.Add("8 Hours");
        AutoDataTable.Columns.Add("N/FH");
        AutoDataTable.Columns.Add("OT Days");
        AutoDataTable.Columns.Add("SPG Allow");
        AutoDataTable.Columns.Add("Canteen Days Minus");
        AutoDataTable.Columns.Add("OT Hours");
        AutoDataTable.Columns.Add("W.H");
        AutoDataTable.Columns.Add("Fixed W.Days");
        AutoDataTable.Columns.Add("NFH W.Days");
        AutoDataTable.Columns.Add("Total Month Days");
        AutoDataTable.Columns.Add("MachineID_Encrypt");
        AutoDataTable.Columns.Add("NFH Worked Days");
        AutoDataTable.Columns.Add("NFH D.W Statutory");

        fromdate = Request.QueryString["FromDate"].ToString();
        todate = Request.QueryString["ToDate"].ToString();
        Spin_Wages = Request.QueryString["Wages"].ToString();
        Wages = Request.QueryString["Wages"].ToString();


        DataTable dtIPaddress = new DataTable();
        dtIPaddress = objdata.BelowFourHours(SessionCcode.ToString(), SessionLcode.ToString());


        if (dtIPaddress.Rows.Count > 0)
        {
            for (int i = 0; i < dtIPaddress.Rows.Count; i++)
            {
                if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                {
                    mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
                else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                {
                    mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
            }
        }
        Payroll_Att_Fill();
        Write_payroll_attenance();
       

    }

    protected void UploadDataTableToExcel(DataTable dtRecords)
    {
        string XlsPath = Server.MapPath(@"~/Add_data/EmployeePayroll.xls");
        string attachment = string.Empty;
        if (XlsPath.IndexOf("\\") != -1)
        {
            string[] strFileName = XlsPath.Split(new char[] { '\\' });
            attachment = "attachment; filename=" + strFileName[strFileName.Length - 1];
        }
        else
            attachment = "attachment; filename=" + XlsPath;
        try
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            string tab = string.Empty;

            foreach (DataColumn datacol in dtRecords.Columns)
            {
                Response.Write(tab + datacol.ColumnName);
                tab = "\t";
            }
            Response.Write("\n");

            foreach (DataRow dr in dtRecords.Rows)
            {
                tab = "";
                for (int j = 0; j < dtRecords.Columns.Count; j++)
                {
                    Response.Write(tab + Convert.ToString(dr[j]));
                    tab = "\t";
                }

                Response.Write("\n");
            }
            Response.End();
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }
    }
    public void Payroll_Att_Fill()
    {
        if (fromdate != "" && todate != "")
        {

            SSQL = "";
            SSQL = "select MachineID,MachineID_Encrypt";
            SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + ",ExistingCode";
            SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "' And IsActive='Yes' and Wages='" + Wages + "' order by MachineID asc";

            dsEmployee = objdata.ReturnMultipleValue(SSQL);
            int i1 = 0;
            if (dsEmployee.Rows.Count > 0)
            {
                //AutoDataTable.Rows.Count = 0;
                //AutoDataTable.Columns.Count = 0;
                //int rcount = dsEmployee.Rows.Count + 3;
                //int ccount = dsEmployee.Columns.Count + 1;
                //int ccounted = dsEmployee.Columns.Count;

                //AutoDataTable.Rows.Add();
                DataColumn dc = new DataColumn();

                for (int j = 0; j < dsEmployee.Rows.Count; j++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[i1][0] = dsEmployee.Rows[j]["MachineID"].ToString();

                    AutoDataTable.Rows[i1][1] = dsEmployee.Rows[j]["ExistingCode"].ToString();

                    //Datacells.Rows[i1][0] = dsEmployee.Rows[j]["MachineID"].ToString();
                    AutoDataTable.Rows[i1][13] = dsEmployee.Rows[j]["MachineID_Encrypt"].ToString();
                    //Datacells.Rows[i1][13] = dsEmployee.Rows[j]["MachineID_Encrypt"].ToString();
                    AutoDataTable.Rows[i1][2] = dsEmployee.Rows[j]["FirstName"].ToString();
                    //Datacells.Rows[i1][2] = dsEmployee.Rows[j]["FirstName"].ToString();
                    Encry_MachineID = dsEmployee.Rows[j]["MachineID_Encrypt"].ToString();
                    i1++;
                }

            }

        }
    }
    public void Write_payroll_attenance()
    {
        try
        {

            DataColumn auto = new DataColumn();
            auto.AutoIncrement = true;
            auto.AutoIncrementSeed = 1;



            Datacells.Columns.Add("MachineID");
            Datacells.Columns.Add("Token No");
            Datacells.Columns.Add("EmpName");
            Datacells.Columns.Add("Days");
            Datacells.Columns.Add("8 Hours");

            Datacells.Columns.Add("N/FH");
            Datacells.Columns.Add("OT Days");
            Datacells.Columns.Add("SPG Allow");
            Datacells.Columns.Add("Canteen Days Minus");
            Datacells.Columns.Add("OT Hours");

            Datacells.Columns.Add("W.H");
            Datacells.Columns.Add("Fixed W.Days");
            Datacells.Columns.Add("NFH W.Days");
            Datacells.Columns.Add("Total Month Days");
            Datacells.Columns.Add("NFH Worked Days");
            Datacells.Columns.Add("NFH D.W Statutory");

            //Datacells.Columns.Add("MachineID_Encrypt");
            //Datacells.Columns.Add("Total");

            int aintCol = 4;
            int Month_Name_Change = 1;


            //string ds1 = string.Format("{dd/MM/yyyy}", Txtfrom.Text);
            date1 = Convert.ToDateTime(fromdate);
            // string dd =string.Format("{dd/MM/yyyy}",TxtTo.Text);
            date2 = Convert.ToDateTime(todate);
            int dayCount = (int)((date2 - date1).TotalDays);
            int daysAdded = 0;
            Att_Already_Date_Check = "";

            AutoDataTable.Columns.Add("Days");

            while (dayCount >= 0)
            {
                DateTime d1 = date1.AddDays(daysAdded);
                string date_str = Convert.ToString(d1);

                Att_Date_Check = date_str.Split('/');
                if (aintCol == 3)
                {
                    Att_Already_Date_Check = Att_Date_Check[1];
                    Att_Year_Check = Att_Date_Check[2];

                    Month_Name_Change = Convert.ToUInt16(Att_Already_Date_Check);
                    aintCol = aintCol + 1;
                }
                else
                {
                }

                dayCount = dayCount - 1;
                daysAdded = daysAdded + 1;
            }
            aintI = 0;
            aintK = 1;

            for (int intRow = 0; intRow < AutoDataTable.Rows.Count; intRow++)
            {
                aintK = 1;

                Datacells.NewRow();
                Datacells.Rows.Add();

                //for (int j = 0; j < dsEmployee.Rows.Count; j++)
                //{
                //    AutoDataTable.NewRow();
                //    AutoDataTable.Rows.Add();
                //    AutoDataTable.Rows[i1][0] = dsEmployee.Rows[j]["MachineID"].ToString();
                //    AutoDataTable.Rows[i1][2] = dsEmployee.Rows[j]["FirstName"].ToString();
                //    i1++;
                //}

                for (aintCol = 0; aintCol <= 1; aintCol++)
                {
                    aintK += 1;
                }
                Hrs_Below = 0;
                string Emp_WH_Day;
                DataTable DS_WH = new DataTable();
                string DOJ_Date_Str;
                SSQL = "Select * from Employee_MST where MachineID='" + AutoDataTable.Rows[intRow][0].ToString() + "' And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                DS_WH = objdata.ReturnMultipleValue(SSQL);

                if (DS_WH.Rows.Count != 0)
                {
                    Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                    DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();

                }
                else
                {
                    Emp_WH_Day = "";
                    DOJ_Date_Str = "";
                }

                int colIndex = aintK;

                //decimal Present_Count = 0;
                decimal Month_WH_Count = 0;
                //decimal Present_WH_Count = 0;
                //Int32 Appsent_Count = 0;
                //decimal Final_Count = 0;
                decimal Total_Days_Count = 0;
                decimal Hrs = 0;
                decimal Month_Mid_Join_Total_Days_Count = 0;

                string Already_Date_Check = null;
                string Year_Check = null;
                Int32 Days_Insert_RowVal = 0;

                decimal OT_Hours = 0;


                //decimal FullNight_Shift_Count = 0;
                // Int32 NFH_Days_Count = 0;
                //decimal NFH_Days_Present_Count = 0;
                Already_Date_Check = "";
                Year_Check = "";


                for (aintCol = 0; aintCol < daysAdded; aintCol++)
                {
                    time_Check_dbl = 0;

                    Spin_Machine_ID_Str = AutoDataTable.Rows[intRow]["MachineID"].ToString();

                    // Machine_ID_Str = Datacells.Rows[aintI]["MachineID"].ToString();
                    Machine_ID_Str = AutoDataTable.Rows[intRow]["MachineID_Encrypt"].ToString();
                    OT_Week_OFF_Machine_No = AutoDataTable.Rows[intRow]["MachineID"].ToString();

                    if (OT_Week_OFF_Machine_No == "1025")
                    {
                        string QQ = "Select * from Employee_Mst";

                    }
                    else if (OT_Week_OFF_Machine_No == "2001")
                    {
                        string QQ = "Select * from Employee_Mst";
                    }

                    NewDate1 = Convert.ToDateTime(fromdate);

                    dtime = NewDate1.AddDays(aintCol);
                    dtime1 = NewDate1.AddDays(aintCol).AddDays(1);
                    Date_Value_Str = String.Format(Convert.ToString(dtime), "yyyy/MM/dd");
                    Date_Value_Str1 = String.Format(Convert.ToString(dtime1), "yyyy/MM/dd");

                    DataTable mLocalDS1 = new DataTable();

                    //ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                    //ss = ss + " Where LT.Compcode='" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    //ss = ss + "And LT.TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    //ss = ss + "And LT.TimeIN <='" + dtime1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    //ss = ss + "AND em.ShiftType='" + ddlshifttype.SelectedItem.Text + "' And EM.Compcode='" + SessionCcode.ToString() + "'";
                    //ss = ss + "And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsActive='yes' Group By LT.MachineID Order By Min(LT.TimeIN)";





                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);


                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Time_IN_Str = "";
                    }

                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT ASC";
                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);

                    if (mLocalDS1.Rows.Count <= 0)
                    {
                        Time_Out_Str = "";
                    }


                    if (mLocalDS.Rows.Count != 0)
                    {

                        InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"]);

                        InToTime_Check = InTime_Check.AddHours(2);

                        string s1 = InTime_Check.ToString("HH:mm:ss");
                        string s2 = InToTime_Check.ToString("HH:mm:ss");

                        //string InTime_Check_str = String.Format("HH:mm:ss", s1);
                        //string InToTime_Check_str = String.Format("HH:mm:ss", s2);

                        InTime_TimeSpan = TimeSpan.Parse(s1);
                        // From_Time_Str = Convert.ToString(InTime_TimeSpan.Hour) + ":" + Convert.ToString(InTime_TimeSpan.Minute);
                        From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                        TimeSpan InTime_TimeSpan1 = TimeSpan.Parse(s2);
                        To_Time_Str = InTime_TimeSpan1.Hours + ":" + InTime_TimeSpan1.Minutes;

                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + From_Time_Str + "' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";
                        DS_Time = objdata.ReturnMultipleValue(SSQL);

                        //if (DS_Time.Rows.Count != 0)
                        //{
                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                        DS_InTime = objdata.ReturnMultipleValue(SSQL);

                        //if (DS_InTime.Rows.Count != 0)
                        //{
                        Final_InTime = mLocalDS.Rows[0][0].ToString();
                        SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                        Shift_DS_Change = objdata.ReturnMultipleValue(SSQL);
                        Shift_Check_blb = false;

                        for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
                        {
                            DateTime DataValueStr = Convert.ToDateTime(Date_Value_Str.ToString());
                            DateTime DataValueStr1 = Convert.ToDateTime(Date_Value_Str1.ToString());


                            Shift_Start_Time_Change = DataValueStr.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();

                            if (Shift_DS_Change.Rows[K]["EndIN_Days"].ToString() == "1")
                            {
                                Shift_End_Time_Change = DataValueStr1.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                            }
                            else
                            {
                                Shift_End_Time_Change = DataValueStr.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                            }




                            ShiftdateStartIN_Change = Convert.ToDateTime(Shift_Start_Time_Change);
                            ShiftdateEndIN_Change = Convert.ToDateTime(Shift_End_Time_Change);
                            EmpdateIN_Change = Convert.ToDateTime(Final_InTime);

                            if (EmpdateIN_Change >= ShiftdateStartIN_Change & EmpdateIN_Change <= ShiftdateEndIN_Change)
                            {
                                Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                Shift_Check_blb = true;
                                break;
                            }
                        }
                        if (Shift_Check_blb == true)
                        {
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                            mLocalDS = objdata.ReturnMultipleValue(SSQL);

                            if (Final_Shift == "SHIFT2")
                            {
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
                            }
                            else if (Final_Shift == "SHIFT1")
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "00:00' And TimeIN <='" + dtime.ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeIN ASC";

                                mLocalDS = objdata.ReturnMultipleValue(SSQL);



                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + dtime.ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeOUT Asc";
                                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                            }
                            else
                            {
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "17:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                            }


                        }
                        else
                        {
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

                            mLocalDS = objdata.ReturnMultipleValue(SSQL);

                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

                            mLocalDS1 = objdata.ReturnMultipleValue(SSQL);

                        }
                    
                    }

                    string Emp_Total_Work_Time_1 = "00:00";

                    if (mLocalDS.Rows.Count > 1)
                    {
                        for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();

                            if (mLocalDS1.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_Out_Str = "";
                            }


                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                            }
                            if (Time_IN_Str == "" || Time_Out_Str == "")
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {

                                date1 = System.Convert.ToDateTime(Time_IN_Str);
                                date2 = System.Convert.ToDateTime(Time_Out_Str);

                                TimeSpan ts = new TimeSpan();
                                ts = date2.Subtract(date1);
                                Total_Time_get = Convert.ToString(ts.Hours);
                                ts4 = ts4.Add(ts);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;



                                string tsfour = Convert.ToString(ts4.Minutes);
                                string tsfour1 = Convert.ToString(ts4.Hours);



                                if (Left_Val(tsfour, 1) == "-" | Left_Val(tsfour1, 1) == "-" | Emp_Total_Work_Time_1 == "0:0")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }

                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = ts.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
                                    Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                }
                                else
                                {
                                    time_Check_dbl = Convert.ToInt16(time_Check_dbl) + Convert.ToInt16(Total_Time_get.ToString());

                                    OT_Hours = (Convert.ToDecimal(OT_Hours) + Convert.ToDecimal(time_Check_dbl));
                                }

                            }
                        }
                    }
                    else
                    {
                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout <= mLocalDS1.Rows.Count - 1; tout++)
                        {
                            if (mLocalDS1.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";
                            }
                            else
                            {
                                Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                            }
                        }

                        if (Time_IN_Str == "" || Time_Out_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {


                            date1 = System.Convert.ToDateTime(Time_IN_Str);
                            date2 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts;
                            ts = date2.Subtract(date1);
                            Total_Time_get = ts.Hours.ToString();
                            ts4 = ts4.Add(ts);

                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = ts.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
                                Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";

                                }
                            }
                            else
                            {
                                time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
                                OT_Hours = (Convert.ToDecimal(OT_Hours) + Convert.ToDecimal(time_Check_dbl));
                            }
                        }
                    }

                    if (OT_Week_OFF_Machine_No == "6034")
                    {
                        OT_Week_OFF_Machine_No = "6034";
                    }

                 
                   
                    string Employee_Week_Name;
                    string Assign_Week_Name;
                 
                    Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                    SSQL = "Select WeekOff from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "'";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Assign_Week_Name = "";
                    }
                    else
                    {
                        Assign_Week_Name = "";
                    }
                    Boolean NFH_Day_Check = false;
                    DateTime NFH_Date;
                    DateTime DOJ_Date_Format;
                    DateTime Date_Value = new DateTime();

                    Date_Value = Convert.ToDateTime(Date_Value_Str.ToString());
                    string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value.ToString("yyyy/MM/dd") + "'";

                    // string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                    mLocalDS = objdata.ReturnMultipleValue(qry_nfh);

             
                    if (Employee_Week_Name.ToString() == Assign_Week_Name.ToString())
                    {

                    }
                    else
                    {
                        double Calculate_Attd_Work_Time = 0;
                        double Calculate_Attd_Work_Time_half = 0;
                        SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and CatName='STAFF'  And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And IsActive='Yes'";
                        mLocalDS = objdata.ReturnMultipleValue(SSQL);

                        if (mLocalDS.Rows.Count != 0)
                        {
                            Calculate_Attd_Work_Time = 8;
                        }
                        else
                        {
                            Calculate_Attd_Work_Time = 12;                        
                        }






                        Calculate_Attd_Work_Time_half = 4.0;
                        halfPresent = "0";

                        if (time_Check_dbl >=Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "0";
                        }
                                                   
                        else
                        {
                            Hrs_Below = time_Check_dbl + Hrs_Below;
                            isPresent = false;
                        }
                    
                    }

                    string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                    if (Emp_WH_Day == Attn_Date_Day)
                    {
                      
                            Month_WH_Count = Month_WH_Count + 1;
                            if (isPresent == true)
                            {
                                
                                if (halfPresent == "1")
                                {
                                    Present_WH_Count = Convert.ToDouble(Present_WH_Count + 0.5);
                                }
                                else if (halfPresent == "0")
                                {
                                    Present_WH_Count = Present_WH_Count + 1;
                                }
                               
                            }
                        
                    }

                    if (isPresent == true)
                    {

                         if (halfPresent == "0")
                        {
                            Present_Count = Present_Count + 1;
                            if (NFH_Day_Check == true)
                            {
                                NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                            }
                        }

                
                    }
                    else
                    {
                        Appsent_Count = Appsent_Count + 1;
                    }


                    colIndex += shiftCount;
                    aintK += 1;
                    Total_Days_Count = Total_Days_Count + 1;

                    if (Convert.ToDecimal(OT_Hours) > 0)
                    {
                        OT_Hours = OT_Hours - 8;
                    }
                    else
                    {
                        OT_Hours = 0;
                    }

                    

                    //System.DateTime Report_Date = default(System.DateTime);
                    //System.DateTime DOJ_Date_Format_Check = default(System.DateTime);
                    //if (Wages == "STAFF salary" | Wages == "Watch & Ward")
                    //{
                    //    if (!string.IsNullOrEmpty(DOJ_Date_Str))
                    //    {
                    //        Report_Date = Convert.ToDateTime(Date_Value_Str.ToString());
                    //        DOJ_Date_Format_Check = Convert.ToDateTime(DOJ_Date_Str.ToString());
                    //        if (DOJ_Date_Format_Check.Date <= Report_Date.Date)
                    //        {
                    //            Month_Mid_Join_Total_Days_Count = Month_Mid_Join_Total_Days_Count + 1;
                    //        }
                    //        else
                    //        {
                    //        }
                    //    }
                    //    else
                    //    {
                    //        Month_Mid_Join_Total_Days_Count = Month_Mid_Join_Total_Days_Count + 1;
                    //    }
                    //    //Month_Mid_Join_Total_Days_Count = Month_Mid_Join_Total_Days_Count + 1;
                    //    Total_Days_Count = Total_Days_Count + 1;
                    //}
                    //else
                    //{
                    //    Month_Mid_Join_Total_Days_Count = Month_Mid_Join_Total_Days_Count + 1;
                    //    Total_Days_Count = Total_Days_Count + 1;
                    //}
                }


                Final_Count = Present_Count;
               
                //if (NFH_Days_Count == 0)
                //{
                //    NFH_Days_Present_Count = 0;
                //}
                string Token_No;
                string Machine_ID_Str_Excel;
                DataTable Token_DS = new DataTable();



                Machine_ID_Str_Excel = Datacells.Rows[aintI][1].ToString();
                decimal  Fixed_Work_Days = 0;
                SSQL = "Select * from Employee_Mst where MachineID='" + Datacells.Rows[aintI][1].ToString() + "' and Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
                Token_DS = objdata.ReturnMultipleValue(SSQL);
                if (Token_DS.Rows.Count != 0)
                {
                    Token_No_Get = Token_DS.Rows[0]["ExistingCode"].ToString();
                    
                }

                
                    Fixed_Work_Days = Total_Days_Count - Month_WH_Count;
                    Final_Count = Final_Count - Present_WH_Count;
                    Final_Count = Final_Count - NFH_Days_Present_Count;
                    Present_WH_Count = Present_WH_Count + NFH_Days_Present_Count;
                



                string NFH_Year_Str = "0";
                string NFH_Month_Str = "";
                string NFH_Rule_OLD = "";
                int NFH_Display_Days = 0;
                string NFH_Double_Wages_Rule = "";
                DataTable mDataSet = new DataTable();
                NFH_Year_Str = Convert.ToString(date1.Year);
                NFH_Month_Str = Convert.ToString(date1.Month);
                //SSQL = "Select * from NFH_Rule_Det where NFHYear='" + NFH_Year_Str + "' And Months='" + NFH_Month_Str + "'";
             

                if (Final_Count != 0)
                {

                    Datacells.Rows[aintI][0] = AutoDataTable.Rows[intRow]["MachineID"].ToString();
                    Datacells.Rows[aintI][2] = AutoDataTable.Rows[intRow]["EmpName"].ToString();
                    //Datacells.Rows[aintI][13] = AutoDataTable.Rows[intRow]["MachineID_Encrypt"].ToString();
                    Datacells.Rows[aintI][1] = AutoDataTable.Rows[intRow]["Token No"].ToString();
                    Datacells.Rows[aintI]["NFH Worked Days"] = "0";
                    Datacells.Rows[aintI]["NFH D.W Statutory"] = "0";

                    //Datacells.Rows[aintI][1] = Machine_ID_Str_Excel;
                    Datacells.Rows[aintI][3] = Final_Count;
                    Datacells.Rows[aintI][4] = Hrs_Below;
                    Datacells.Rows[aintI][5] = NFH_Days_Count;
                    Datacells.Rows[aintI][6] = "0";
                    Datacells.Rows[aintI][7] = "0";
                    Datacells.Rows[aintI][8] = "0";
                    Datacells.Rows[aintI][9] = Present_WH_Count;
                    Datacells.Rows[aintI][10] = Month_WH_Count;
                    Datacells.Rows[aintI][11] = Fixed_Work_Days;
                    Datacells.Rows[aintI][12] = NFH_Days_Present_Count;
                    Datacells.Rows[aintI][13] = Total_Days_Count;
                    Datacells.Rows[aintI][14] = "0";
                    Datacells.Rows[aintI][15] = "0";

                }
                else
                {
                    Datacells.Rows[aintI][0] = AutoDataTable.Rows[intRow]["MachineID"].ToString();
                    Datacells.Rows[aintI][2] = AutoDataTable.Rows[intRow]["EmpName"].ToString();
                    // Datacells.Rows[aintI][13] = AutoDataTable.Rows[intRow]["MachineID_Encrypt"].ToString();
                    Datacells.Rows[aintI][1] = AutoDataTable.Rows[intRow]["Token No"].ToString();
                    Datacells.Rows[aintI]["NFH Worked Days"] = "0";
                    Datacells.Rows[aintI]["NFH D.W Statutory"] = "0";


                    //Datacells.Rows[aintI][1] = Machine_ID_Str_Excel;
                    //Datacells.Rows[aintI][2] = Token_No_Get;
                    Datacells.Rows[aintI][3] = Final_Count;
                    Datacells.Rows[aintI][4] = "0";
                    Datacells.Rows[aintI][5] = NFH_Days_Count;
                    Datacells.Rows[aintI][6] = "0";
                    Datacells.Rows[aintI][7] = "0";
                    Datacells.Rows[aintI][9] = Present_WH_Count;
                    Datacells.Rows[aintI][10] = Month_WH_Count;
                    Datacells.Rows[aintI][11] = Fixed_Work_Days;

                    if (Wages == "STAFF salary" || Wages == "Watch & Ward")
                    {
                        //Datacells.Rows[aintI][8] = 0;
                        //Datacells.Rows[aintI][12] = Present_WH_Count;
                        //Datacells.Rows[aintI][13] = Fixed_Work_Days;
                        Datacells.Rows[aintI][8] = "0";
                        //Datacells.Rows[aintI][12] = Present_WH_Count;
                        Datacells.Rows[aintI][12] = NFH_Days_Present_Count;
                        Datacells.Rows[aintI][13] = Total_Days_Count;
                    }
                    else
                    {

                        Datacells.Rows[aintI][8] = Present_WH_Count;
                        Datacells.Rows[aintI][12] = 0;
                        Datacells.Rows[aintI][13] = 0;
                    }

                    //Datacells.Rows[aintI][14] = NFH_Days_Present_Count;
                    //  Datacells.Rows[aintI][15] = Total_Days_Count;


                }

                aintI = aintI + 1;
                Total_Days_Count = 0;
                Final_Count = 0;
                Appsent_Count = 0;
                Present_Count = 0;
                Fixed_Work_Days = 0;
                Month_WH_Count = 0;
                Present_WH_Count = 0;
                NFH_Days_Count = 0;
                NFH_Days_Present_Count = 0;
                EHours_Count = 0;
                OT_Hours = 0;


            }


        }


        catch (Exception e)
        {
        }

        UploadDataTableToExcel(Datacells);
    }
 
}
