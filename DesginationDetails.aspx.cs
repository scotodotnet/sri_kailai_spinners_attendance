﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class DesginationDetails : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    UserCreationClass objUsercreation = new UserCreationClass();
    DataTable dtdDisplay = new DataTable();

    static int ss;
    bool ErrFlag = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Desgination Details";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");

                CreateUserDesginationDisplay();
            }
        }
    }


    public void CreateUserDesginationDisplay()
    {
        DataTable dtDesginationDisplay = new DataTable();
        dtDesginationDisplay = objdata.DesginationDisplay();

        if (dtDesginationDisplay.Rows.Count > 0)
        {
            RepeaterDesign.DataSource = dtDesginationDisplay;
            RepeaterDesign.DataBind();
        }
    }


    protected void btnSaveDesgination_Click(object sender, EventArgs e)
    {
        if (txtDesginationName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Desgination Name');", true);
            ErrFlag = true;
        }
        else
        {

            DataTable dt = new DataTable();
            DataTable dtt = new DataTable();


            if (btnSaveDesgination.Text == "Update")
            {
                dt = objdata.DesignationUpdate(txtDesginationName.Text, ss);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Sussessfully');", true);
            }
            else
            {
                dtt = objdata.CheckEmpType_AlreadyExist(txtDesginationName.Text);
                if (dtt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Desgination Name Already Exist');", true);
                    txtDesginationName.Text = "";
                }
                else
                {
                    dt = objdata.DesginationRegister(txtDesginationName.Text);
                    txtDesginationName.Text = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Save Successfully');", true);
                }
            }

            CreateUserDesginationDisplay();
            btnSaveDesgination.Text = "Save";
            ClearDegination();
        }
    }
    protected void btnClearDesgination_Click(object sender, EventArgs e)
    {
        ClearDegination();

        btnSaveDesgination.Text = "Save";
    }

    public void ClearDegination()
    {
        txtDesginationName.Text = "";
    }

    protected void RepeaterDesign_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterDataDesgination(id);
                break;
            case ("Edit"):
                MPE1.Show();
                EditRepeaterDataDesgination(id);
                break;
        }
    }

    private void EditRepeaterDataDesgination(string DeptCode)
    {
        ss = Convert.ToInt16(DeptCode);
        DataTable Dtd = new DataTable();
        Dtd = objdata.EditDesignation(ss);
        if (Dtd.Rows.Count > 0)
        {
            txtDesginationName.Text = Dtd.Rows[0]["DesignName"].ToString();
        }

        btnSaveDesgination.Text = "Update";
    }

    private void DeleteRepeaterDataDesgination(string DeptCode)
    {
        int ss = Convert.ToInt16(DeptCode);

        DataTable DtdCheck = new DataTable();
        DtdCheck = objdata.CheckEmployeeDesignation(ss);

        if (DtdCheck.Rows.Count > 0)
        {
            string DesignName = DtdCheck.Rows[0]["DesignName"].ToString();

            DataTable CheckInEmployee = new DataTable();
            CheckInEmployee = objdata.CheckingDesignEmployee(DesignName);
            if (CheckInEmployee.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Department Name Allocated to Employee');", true);
            }
            else
            {
                DataTable Dtd = new DataTable();
                Dtd = objdata.DeleteDesgination(ss);
                CreateUserDesginationDisplay();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
            }
        }

    }
}
