﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Leave_Details.aspx.cs" Inherits="Leave_Details" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    
    <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tableCustomer').dataTable();               
            }
        }); 
    };
    </script>
      
             
     <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableCustomer').dataTable();
        });
    </script>


                               <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate>

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">Leave Details</li>
                           <h4>
                           </h4>
                           <h4>
                           </h4>
                           <h4>
                           </h4>
                           <h4>
                           </h4>
                        </h4> 
                    </ol>
                </div>
                
                        
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
     
        
    
    <div class="page-inner">
   
                                       
            
            <div class="col-md-9">
			   <div class="panel panel-white">
			    <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Leave Details</h4>
				</div>
				</div>
				
				
				<form class="form-horizontal">
				<div class="panel-body">
					
				    	<%--<div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Company Code<span class="mandatory">*</span></label>
						<div class="col-sm-4">
                            <asp:DropDownList ID="ddlCompanyCode" runat="server" class="form-control">
                              
                                </asp:DropDownList>
                            
						</div>
						<div class="col-md-1"></div>
				        <label for="input-Default" class="col-sm-2 control-label">Location Code<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                            <asp:DropDownList ID="ddlLocationcode" runat="server" class="form-control">
                               
                                </asp:DropDownList>
                            
						</div>
               </div>--%>                     
					  
					    <div class="form-group row">
					
                         <label for="input-Default" class="col-sm-2 control-label">Emp No</label>
							<div class="col-sm-4">
                                    <asp:DropDownList ID="ddlTicketno" runat="server" class="form-control" 
                                        onselectedindexchanged="ddlTicketno_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>           
							</div>					
					       <div class="col-sm-1"></div> 
							
						 <label for="input-Default" class="col-sm-2 control-label">Existing Code</label>
							<div class="col-sm-3">
                                 <asp:TextBox ID="txtExistingCode" class="form-control" runat="server" required></asp:TextBox>
							</div>
							
						</div>
					    
					    <div class="form-group row">
					       <label for="input-Default" class="col-sm-2 control-label">Employee Name</label>
							<div class="col-sm-4">
                                 <asp:TextBox ID="txtEmpName" class="form-control" runat="server" required></asp:TextBox>
							</div>
					    
					    
					   <div class="col-md-1"></div> 
						<label for="input-Default" class="col-sm-2 control-label">From Date</label>
						<div class="col-sm-3">
                        	<asp:TextBox ID="txtFromDate" type="text" runat="server" class="form-control col-md-6" required></asp:TextBox> 

                                 <cc1:CalendarExtender ID="CalendarExtender12" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtFromDate">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtFromDate" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
                            
						</div>
						</div>
					
					
				       	<div class="form-group row">	
						<label for="input-Default" class="col-sm-2 control-label">To Date</label>
						<div class="col-sm-4">
						<asp:TextBox ID="txtToDate" type="text" runat="server" class="form-control col-md-6" required 
                                ontextchanged="txtToDate_TextChanged" AutoPostBack="true"></asp:TextBox> 
				      <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtToDate">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtToDate" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
                            
                         </div>
                         
                         
                       <div class="col-md-1"></div>
                         
						<label for="input-Default" class="col-sm-2 control-label">Total Days<span class="mandatory">*</span></label>
						<div class="col-sm-3">
						<asp:TextBox ID="txtTotalDays" runat="server" class="form-control col-md-6" required></asp:TextBox> 
                           <%-- <input name="ctl00$ContentPlaceHolder1$tin_date" type="text" id="Text2" class="form-control date-picker">--%>
                            
						</div>
						</div>
						
				        
				         <div class ="form-group row">
                        <label for="input-Default" class="col-sm-2 control-label">Hours<span class="mandatory">*</span></label>
						    	<div class="col-sm-2">
                                   <asp:TextBox ID="txtdays" class="form-control" runat="server" required></asp:TextBox>
                               </div>
                                <div class="col-sm-2">
                                   <asp:RadioButton ID="rdbMorning" runat="server" Text ="AM"  
                                        AutoPostBack="true" oncheckedchanged="rdbMorning_CheckedChanged"/>
                                   <asp:RadioButton ID="rdbEvening" runat="server" Text ="PM"  
                                        AutoPostBack="true" oncheckedchanged="rdbEvening_CheckedChanged"/> 
                                        <span class="mandatory">*</span>
                               </div>
                         <div class="col-md-1"></div>
						
						 <label for="input-Default" class="col-sm-2 control-label">Leave Type<span class="mandatory">*</span></label>
						<div class="col-sm-3">
                            <asp:DropDownList ID="ddlLeaveType" runat="server" class="form-control" 
                                AutoPostBack="True" 
                                onselectedindexchanged="ddlLeaveType_SelectedIndexChanged"> 
                                
                                </asp:DropDownList>
                            
						</div>
						</div>
				        
				        
				        
				        
				        
				        
				        
				        
				        
				                                
                         
                          <div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Leave Desc<span class="mandatory">*</span></label>
						<div class="col-sm-4">
						<asp:TextBox ID="txtLeaveDesc" TextMode="MultiLine" Height="80" class="form-control" runat="server" required></asp:TextBox>
                            
                         
						</div>
						
					
						
						
                         </div>
					
					<!-- Button start -->
						<div class="form-group row">
						</div>
                            <div class="txtcenter">
                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                    onclick="btnSave_Click" />
                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                  
                     <asp:LinkButton ID="btnClear" runat="server" class="btn btn-danger" 
                                    onclick="btnClear_Click">Clear</asp:LinkButton>
                  
                 
                    </div>
                    <!-- Button End -->	
					     
					    <div class="form-group row">
						</div>  
						<div class="panel-body">
                             <div class="table-responsive">
    
                              <asp:Repeater ID="rptrCustomer" runat="server" onitemcommand="Repeater1_ItemCommand">
            <HeaderTemplate>
                
                  <table id="tableCustomer" class="display table" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                                                   <%-- <th>SNo</th>--%>
                                                    <th>EmpNo</th>
                                                    <th>EmpName</th>  
                                                    <th>FromDate</th>
                                                    <th>ToDate</th>
                                                    <th>totalDays</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    
                   <%-- <td>
                        <%# DataBinder.Eval(Container.DataItem, "SNo")%>
                    </td>--%>
                    
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "EmpNo")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "EmpName")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "FromDate")%>
                    </td>
                     <td>
                        <%# DataBinder.Eval(Container.DataItem, "ToDate")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "TotalDays")%>
                    </td>
                    <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("EmpNo")+","+ Eval("Auto_ID") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("EmpNo")+","+ Eval("Auto_ID") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
                        </div>
						
                        
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 <!-- Dashboard start -->
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>
 
  </ContentTemplate>
 </asp:UpdatePanel>
  <%--</div>--%>
</asp:Content>

