﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class LunchTimeReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType; 


    BALDataAccess objdata = new BALDataAccess();
    //string SessionAdmin = "admin";
    //string SessionCcode = "ESM";
    //string SessionLcode = "UNIT I";


    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];

    DataTable mDataSet = new DataTable();
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str = null;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    Int32 time_Check_dbl = 0;
    string Total_Time_get = "";

    DataTable Payroll_DS = new DataTable();
    DataTable AutoDTable = new DataTable();
    DataSet ds = new DataSet();
    string SSQL = "";
    string mIpAddress_IN;
    string mIpAddress_OUT;

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Lunch Time Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("LeaveManagement"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();


            //ModeType = Request.QueryString["ModeType"].ToString();

            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            //ddlShiftType = Request.QueryString["ddlShiftType"].ToString();


            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("Dept");
            AutoDTable.Columns.Add("Type");
            AutoDTable.Columns.Add("Shift");
            AutoDTable.Columns.Add("EmpCode");
            AutoDTable.Columns.Add("ExCode");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("TimeIN");
            AutoDTable.Columns.Add("TimeOUT");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Category");
            AutoDTable.Columns.Add("SubCategory");
            AutoDTable.Columns.Add("TotalMIN");
            AutoDTable.Columns.Add("GrandTOT");

            AutoDTable.Columns.Add("CompanyName");
            AutoDTable.Columns.Add("LocationName");

            SSQL = "";
            SSQL = "Select IPAddress,IPMode from IPAddress_Mst Where Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
            mDataSet = objdata.ReturnMultipleValue(SSQL);



            for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
            {
                if (mDataSet.Rows[iRow]["IPMode"].ToString() == "IN")
                {
                    mIpAddress_IN = mDataSet.Rows[iRow]["IPAddress"].ToString();
                }
                else if (mDataSet.Rows[iRow]["IPMode"].ToString() == "OUT")
                {
                    mIpAddress_OUT = mDataSet.Rows[iRow]["IPAddress"].ToString();
                }
            }

            DataTable mLocalDS = new DataTable();
            //if (ModeType == "IN/OUT")
            //{
            SSQL = "";
            SSQL = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
            SSQL = SSQL + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
            SSQL += " from Shift_Mst Where CompCode='" + SessionCcode + "'";
            SSQL += " And LocCode='" + SessionLcode + "'";
            if (ShiftType1 != "ALL")
            {
                SSQL += " And shiftDesc='" + ShiftType1 + "'";
            }
            SSQL += " And ShiftDesc like '" + ddlShiftType + "%'";
            //Shift%'"
            SSQL += " Order By shiftDesc";

            //}
            //else
            //{
            //    return;
            //}
            string ShiftType = "";
            string ng = string.Format(Date, "dd-MM-yyyy");
            DateTime date1 = Convert.ToDateTime(ng);

            mLocalDS = objdata.ReturnMultipleValue(SSQL);

            int mStartINRow = 0;
            int mStartOUTRow = 0;

            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            for (int iTabRow = 0; iTabRow < mLocalDS.Rows.Count; iTabRow++)
            {
                if (AutoDTable.Rows.Count <= 1)
                {
                    mStartOUTRow = 0;
                }
                else
                {
                    mStartOUTRow = AutoDTable.Rows.Count - 1;
                }
                if (mLocalDS.Rows[iTabRow]["shiftDesc"].ToString() == "GENERAL")
                {
                    ShiftType = "GENERAL";
                }
                else
                {
                    ShiftType = "SHIFT";
                }

                string sIndays_str = mLocalDS.Rows[iTabRow]["StartIN_Days"].ToString();
                double sINdays = Convert.ToDouble(sIndays_str);
                string eIndays_str = mLocalDS.Rows[iTabRow]["EndIN_Days"].ToString();
                double eINdays = Convert.ToDouble(eIndays_str);
                string ss = "";

                if (ShiftType == "GENERAL")
                {
                    SSQL = "";
                    SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID ";
                    SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And LT.IPAddress='" + mIpAddress_IN + "'";

                    SSQL = SSQL + " And LT.TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";

                    SSQL = SSQL + " And LT.TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    SSQL = SSQL + " AND em.ShiftType='" + ShiftType + "' and em.IsActive='Yes' Group By LT.MachineID";
                    SSQL = SSQL + " Order By Max(LT.TimeIN)";

                }
                else if (ShiftType == "SHIFT")
                {
                    SSQL = "";
                    SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID ";
                    SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And LT.IPAddress='" + mIpAddress_IN + "'";

                    SSQL = SSQL + " And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartIN"].ToString() + "'";

                    SSQL = SSQL + " And LT.TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndIN"].ToString() + "' and em.IsActive='Yes' ";
                    SSQL = SSQL + " Group By LT.MachineID";
                    SSQL = SSQL + " Order By Max(LT.TimeIN)";
                }
                else
                {
                    SSQL = "";
                    SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID ";
                    SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And LT.IPAddress='" + mIpAddress_IN + "'";

                    SSQL = SSQL + " And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartIN"].ToString() + "'";

                    SSQL = SSQL + " And LT.TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndIN"].ToString() + "'";
                    SSQL = SSQL + " AND em.ShiftType='" + ShiftType + "' and em.IsActive='Yes' Group By LT.MachineID";
                    SSQL = SSQL + " Order By Max(LT.TimeIN)";
                }

                mDataSet = objdata.ReturnMultipleValue(SSQL);


                if (mDataSet.Rows.Count > 0)
                {
                    //fg.Rows = fg.Rows + mDataSet.Tables(0).Rows.Count

                    string MachineID;

                    for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
                    {

                        Boolean chkduplicate = false;
                        chkduplicate = false;

                        for (int ia = 0; ia < AutoDTable.Rows.Count - 1; ia++)
                        {
                            string id = mDataSet.Rows[iRow]["MachineID"].ToString();

                            if (id == AutoDTable.Rows[ia][9].ToString())
                            {
                                chkduplicate = true;
                            }
                        }
                        if (chkduplicate == false)
                        {
                            AutoDTable.NewRow();
                            AutoDTable.Rows.Add();


                            // AutoDTable.Rows.Add();

                            // AutoDTable.Rows[mStartINRow][0] = iRow + 1;
                            AutoDTable.Rows[mStartINRow][3] = mLocalDS.Rows[iTabRow]["ShiftDesc"].ToString();
                            if (ShiftType == "SHIFT")
                            {
                                string str = mDataSet.Rows[iRow][1].ToString();
                                // string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1])
                                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);
                            }
                            else
                            {
                                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);
                            }


                            MachineID = mDataSet.Rows[iRow]["MachineID"].ToString();
                            AutoDTable.Rows[mStartINRow][9] = MachineID.ToString();
                            mStartINRow += 1;
                        }

                    }
                }


                mDataSet = new DataTable();

                SSQL = "";
                SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT ";

                SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And IPAddress='" + mIpAddress_OUT + "'";

                SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";

                SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                SSQL = SSQL + " Group By MachineID";
                SSQL = SSQL + " Order By min(TimeOUT)";


                mDataSet = objdata.ReturnMultipleValue(SSQL);

                string InMachine_IP = "";
                string str2 = "";



                DataTable mLocalDS_out = new DataTable();

                for (int iRow2 = mStartOUTRow; iRow2 < AutoDTable.Rows.Count; iRow2++)
                {
                    InMachine_IP = AutoDTable.Rows[iRow2][9].ToString();

                    //TimeOUT Get

                    Date_Value_Str = string.Format(Date, "yyyy/MM/dd");
                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN Asc";
                    mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);


                    if (mLocalDS_INTAB.Rows.Count == 0)
                    {

                    }
                    else
                    {
                        //Shift Check the Shift1

                        string Employee_Time = "";
                        DataTable Shift_Ds = new DataTable();
                        Employee_Time = mLocalDS_INTAB.Rows[0]["TimeIN"].ToString();
                        string[] Dates = null;
                        string Dates1 = "";
                        string[] hours = null;
                        string Final_IN = "";
                        TimeSpan ts = new TimeSpan();
                        Dates = Employee_Time.Split(' ');

                        Dates1 = Dates[1];
                        hours = Dates1.Split(':');

                        string dt = hours[0] + ":" + "00";
                        Final_IN = hours[0] + ":" + hours[1];
                        string Shift1 = " ";
                        SSQL = "Select * from Shift_Mst where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and StartIN<='" + dt + "' and EndIN>='" + dt + "'";
                        Shift_Ds = objdata.ReturnMultipleValue(SSQL);
                        if (Shift_Ds.Rows.Count == 0)
                        {
                        }
                        else
                        {
                            Shift1 = Shift_Ds.Rows[0]["ShiftDesc"].ToString();
                        }
                        if (Shift1 == "SHIFT1")
                        {
                            SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00' Order by TimeOUT Asc";


                        }
                        else
                        {
                            SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "' And Compcode='" + SessionCcode + "'";
                            SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IPAddress='" + mIpAddress_OUT + "'";
                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                            SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Asc";

                        }

                    }
                    mLocalDS_out = objdata.ReturnMultipleValue(SSQL);
                    if (mLocalDS_out.Rows.Count <= 0)
                    {
                        //Skip
                    }
                    else
                    {
                        if (AutoDTable.Rows[iRow2][9].ToString() == mLocalDS_out.Rows[0][0].ToString())
                        {
                            AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", (mLocalDS_out.Rows[0][1]));
                            //GoTo 1
                        }
                    }

                    //Grand Total value Display
                    Time_IN_Str = "";
                    Time_Out_Str = "";
                    string Machine_ID_Str = AutoDTable.Rows[iRow2][9].ToString();


                    Date_Value_Str = date1.ToString("yyyy/MM/dd");
                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN Asc";
                    mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);


                    if (mLocalDS_INTAB.Rows.Count == 0)
                    {

                    }
                    else
                    {
                        //Shift Check the Shift1
                        string Employee_Time = "";
                        DataTable Shift_Ds = new DataTable();
                        Employee_Time = mLocalDS_INTAB.Rows[0]["TimeIN"].ToString();
                        string[] Dates = null;
                        string Dates1 = "";
                        string[] hours = null;
                        string Final_IN = "";
                        TimeSpan ts = new TimeSpan();
                        Dates = Employee_Time.Split(' ');

                        Dates1 = Dates[1];
                        hours = Dates1.Split(':');

                        string dt = hours[0] + ":" + "00";
                        Final_IN = hours[0] + ":" + hours[1];
                        string Shift1 = " ";
                        SSQL = "Select * from Shift_Mst where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and StartIN<='" + dt + "' and EndIN>='" + dt + "'";
                        Shift_Ds = objdata.ReturnMultipleValue(SSQL);
                        if (Shift_Ds.Rows.Count == 0)
                        {
                        }
                        else
                        {
                            Shift1 = Shift_Ds.Rows[0]["ShiftDesc"].ToString();
                        }
                        if (Shift1 == "SHIFT1")
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00' Order by TimeOUT Asc";
                            mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);

                        }
                        else
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                            mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);

                        }

                    }

                    string Emp_Total_Work_Time_1 = "00:00";


                    InMachine_IP = AutoDTable.Rows[iRow2][9].ToString();




                    if (mLocalDS_INTAB.Rows.Count >= 1)
                    {

                        for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

                            if (mLocalDS_OUTTAB.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                            }
                            else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_Out_Str = "";
                            }
                            TimeSpan ts4 = new TimeSpan();
                            Emp_Total_Work_Time_1 = "00:00";
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            //ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay()
                            //ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay()
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                            }

                            //Emp_Total_Work_Time
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);

                                TimeSpan ts1 = new TimeSpan();
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = Convert.ToString(ts1.Hours);
                                //x& ":" & Trim(ts.Minutes)
                                //ts4 = ts4.Add(ts1)
                                //OT Time Get
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1")
                                    Emp_Total_Work_Time_1 = "00:00";
                                //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")

                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts1 = date3.Subtract(date4);
                                    ts1 = date3.Subtract(date4);
                                    ts4 = ts4.Add(ts1);

                                    Total_Time_get = Convert.ToString(ts1.Hours);
                                    //& ":" & Trim(ts.Minutes)
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                    //Emp_Total_Work_Time_1 = Trim(ts1.Hours) & ":" & Trim(ts1.Minutes)
                                    Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1")
                                        Emp_Total_Work_Time_1 = "00:00";

                                }
                                else
                                {

                                    ts4 = ts4.Add(ts1);

                                    Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1")
                                        Emp_Total_Work_Time_1 = "00:00";
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                }
                            }
                            AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                            //fg.set_TextMatrix(iRow2, 13, Emp_Total_Work_Time_1);
                        }


                    }
                    else
                    {

                    }
                    AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                    //fg.set_TextMatrix(iRow2, 13, Emp_Total_Work_Time_1);
                    //fg.set_TextMatrix(iRow2, 13, Emp_Total_Work_Time_1)
                    //End


                }


                //Next

            }


            //DataTable mEmployeeDS = new DataTable();
            //SSQL = "";
            //SSQL = "select Distinct isnull(EM.MachineID,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
            //SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
            //SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
            //SSQL = SSQL + ",isnull(EM.CatName,'') as [CatName], isnull(EM.SubCatName,'') as [SubCatName]";
            //SSQL = SSQL + ",DM.DeptCode from Employee_Mst EM inner Join Department_Mst DM on DM.DeptName=EM.DeptName Where EM.Compcode='" + SessionCcode + "'";
            //SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'" + " order by DM.DeptCode Asc";
            //mEmployeeDS = objdata.ReturnMultipleValue(SSQL);
            //if (mEmployeeDS.Rows.Count > 0)
            //{
            //    for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
            //    {
            //        for (int iRow2 = 1; iRow2 <AutoDTable.Rows.Count; iRow2++)
            //        {
            //            if (AutoDTable.Rows[iRow2][9].ToString() == mEmployeeDS.Rows[iRow1]["MachineID"].ToString())
            //            {
            //                AutoDTable.Rows[iRow2]["Dept"] = mEmployeeDS.Rows[iRow1]["DeptName"].ToString();
            //                AutoDTable.Rows[iRow2]["Type"] = mEmployeeDS.Rows[iRow1]["TypeName"].ToString();
            //                AutoDTable.Rows[iRow2]["EmpCode"] = mEmployeeDS.Rows[iRow1]["EmpNo"].ToString();
            //                AutoDTable.Rows[iRow2]["ExCode"] = mEmployeeDS.Rows[iRow1]["ExistingCode"].ToString();
            //                AutoDTable.Rows[iRow2]["Name"] = mEmployeeDS.Rows[iRow1]["FirstName"].ToString();
            //                AutoDTable.Rows[iRow2]["Category"] = mEmployeeDS.Rows[iRow1]["CatName"].ToString();
            //                AutoDTable.Rows[iRow2]["SubCategory"] = mEmployeeDS.Rows[iRow1]["SubCatName"].ToString();
            //            }
            //            else
            //            {
            //            }
            //        }
            //    }
            //}

            DataTable mEmployeeDS = new DataTable();
            for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
            {

                string MID = AutoDTable.Rows[iRow2][9].ToString();

                SSQL = "";
                SSQL = "select Distinct isnull(EM.MachineID_Encrypt,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
                SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
                SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
                SSQL = SSQL + ",isnull(EM.CatName,'') as [CatName], isnull(EM.SubCatName,'') as [SubCatName]";
                SSQL = SSQL + ",DM.DeptCode,isnull(EM.Designation,'')as [Designation] from Employee_Mst EM inner Join Department_Mst DM on DM.DeptName=EM.DeptName Where EM.MachineID_Encrypt='" + MID + "' And EM.Compcode='" + SessionCcode.ToString() + "'";
                SSQL = SSQL + " And EM.LocCode='" + SessionLcode.ToString() + "'  And EM.IsActive='Yes'" + " order by DM.DeptCode Asc";


                mEmployeeDS = objdata.ReturnMultipleValue(SSQL);

                if (mEmployeeDS.Rows.Count > 0)
                {
                    for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                    {
                        string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();

                        AutoDTable.Rows[iRow2]["Dept"] = mEmployeeDS.Rows[iRow1]["DeptName"].ToString();
                        AutoDTable.Rows[iRow2]["Type"] = mEmployeeDS.Rows[iRow1]["TypeName"].ToString();
                        AutoDTable.Rows[iRow2]["EmpCode"] = mEmployeeDS.Rows[iRow1]["EmpNo"].ToString();
                        AutoDTable.Rows[iRow2]["ExCode"] = mEmployeeDS.Rows[iRow1]["ExistingCode"].ToString();
                        AutoDTable.Rows[iRow2]["Name"] = mEmployeeDS.Rows[iRow1]["FirstName"].ToString();
                        AutoDTable.Rows[iRow2]["Category"] = mEmployeeDS.Rows[iRow1]["CatName"].ToString();
                        AutoDTable.Rows[iRow2]["SubCategory"] = mEmployeeDS.Rows[iRow1]["SubCatName"].ToString();

                        AutoDTable.Rows[iRow2]["CompanyName"] = SessionCompanyName.ToString();
                        AutoDTable.Rows[iRow2]["LocationName"] = SessionLocationName.ToString();




                    }
                }
            }

            ds.Tables.Add(AutoDTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Lunch.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            CrystalReportViewer1.ReportSource = report;


        }

    }
    
  }
