﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;

public partial class NonOTEmployeeList : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();

    System.Web.UI.WebControls.DataGrid GridView1 =
                 new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Non OT Emp List";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");
            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();


            AutoDTable.Columns.Add("EmployeeNo");
            AutoDTable.Columns.Add("ExistingCode");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("Designation");
            AutoDTable.Columns.Add("OTEligible");

            SSQL = "";
            SSQL = "select MachineID,ExistingCode,DeptName,isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + ",Designation,OTEligible from Employee_Mst Where Compcode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "' And OTEligible='No' And IsActive='Yes' order by DeptName Asc";

            mEmployeeDT = objdata.ReturnMultipleValue(SSQL);

            if (mEmployeeDT.Rows.Count > 0)
            {
                for (int i = 0; i < mEmployeeDT.Rows.Count; i++)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[i]["EmployeeNo"] = mEmployeeDT.Rows[i]["MachineID"].ToString();
                    AutoDTable.Rows[i]["ExistingCode"] = mEmployeeDT.Rows[i]["ExistingCode"].ToString();
                    AutoDTable.Rows[i]["DeptName"] = mEmployeeDT.Rows[i]["DeptName"].ToString();
                    AutoDTable.Rows[i]["Name"] = mEmployeeDT.Rows[i]["FirstName"].ToString();
                    AutoDTable.Rows[i]["Designation"] = mEmployeeDT.Rows[i]["Designation"].ToString();
                    AutoDTable.Rows[i]["OTEligible"] = mEmployeeDT.Rows[i]["OTEligible"].ToString();

                }
            }


            GridView1.HeaderStyle.Font.Bold = true;
            GridView1.DataSource = AutoDTable;
            GridView1.DataBind();
            string attachment = "attachment;filename=NONEligibleOTEMPLOYEELIST.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";

            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            GridView1.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td font-Bold='true' colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td font-Bold='true' colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\"> NON ELIGIBLE OT EMPLOYEE LIST REPORT &nbsp;&nbsp;&nbsp;</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
    }
}
