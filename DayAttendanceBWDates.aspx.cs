﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;


public partial class DayAttendanceBWDates : System.Web.UI.Page 
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string Date1_str;
    string Date2_str;
    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    DataTable mEmployee = new DataTable();
    DataTable mLogTime = new DataTable();
    int k1 = 0;

    System.Web.UI.WebControls.DataGrid grid =
                             new System.Web.UI.WebControls.DataGrid();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day attendance B/w Date";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();


            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("ExistingNo");
            AutoDTable.Columns.Add("MachineEncry");
            AutoDTable.Columns.Add("EmpName");


            DataCells.Columns.Add("MachineID");
            DataCells.Columns.Add("ExistingCode");

            DataCells.Columns.Add("DeptName");
            DataCells.Columns.Add("FirstName");
            Date1_str = Request.QueryString["FromDate"].ToString();
            Date2_str = Request.QueryString["ToDate"].ToString();

            DayAttendance_BWDates();
        }
    
    }
   public void DayAttendance_BWDates()
    {

        
    DateTime date1;
    date1 = Convert.ToDateTime(Date1_str);
    string dat = Date2_str;
    DateTime Date2 = Convert.ToDateTime(dat);
   
    int daycount = (int)((Date2 - date1).TotalDays);
    int daysAdded = 0;





    SSQL = "select DeptName,MachineID,ExistingCode,MachineID_Encrypt,FirstName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'  order by DeptName Asc";

    mEmployee = objdata.ReturnMultipleValue(SSQL);


   if (mEmployee.Rows.Count < 0)
       return;
   int i1 = 0;

   for (int j1 = 0; j1 < mEmployee.Rows.Count; j1++)
   {

       AutoDTable.NewRow();
       AutoDTable.Rows.Add();
       AutoDTable.Rows[i1][0] = mEmployee.Rows[j1]["DeptName"].ToString();
       AutoDTable.Rows[i1][1] = mEmployee.Rows[j1]["MachineID"].ToString();
       AutoDTable.Rows[i1][2] = mEmployee.Rows[j1]["ExistingCode"].ToString();
       AutoDTable.Rows[i1][3] = mEmployee.Rows[j1]["MachineID_Encrypt"].ToString();
       AutoDTable.Rows[i1][4] = mEmployee.Rows[j1]["FirstName"].ToString();
       i1++;
   }


  while(daycount >= 0)
    {
     DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
     AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString())); 
     DataCells.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

    daycount -= 1;
    daysAdded += 1;
    }
  AutoDTable.Columns.Add("Total_Days_Present");
  DataCells.Columns.Add("Total_Days_Present");
     
        int j=0;     
        for(int i=0;i<AutoDTable.Rows.Count;i++)
        {
          
            string mID=AutoDTable.Rows[i][1].ToString();
            int daycount1 = (int)((Date2 - date1).TotalDays);
            int daysAdded1 = 0;
            int day_col=5;
            int day_col1 = 4;
            int count=0;
         while(daycount1 >= 0)
        {
        DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded1).ToShortDateString());

        SSQL = "select MachineID,ExistingCode,FirstName,DeptName,Present from LogTime_Days where MachineID='" + mID + "'";
        SSQL = SSQL + " And CompCode= '" + SessionCcode + "' And LocCode= '" + SessionLcode + "'";
        SSQL = SSQL + " And Attn_Date='" + dayy.ToString("MM/d/yyyy") + "'";



        DataTable newdb = new DataTable();
        newdb = objdata.ReturnMultipleValue(SSQL);

        mLogTime=objdata.ReturnMultipleValue(SSQL);

        if (mLogTime.Rows.Count > 0)
        {

            if (mLogTime.Rows[0]["Present"].ToString() == "1.0" | mLogTime.Rows[0]["Present"].ToString() == "0.5")
            {
                AutoDTable.Rows[i][day_col] = 'x';
                count += 1;
            }
        }
        daycount1 -= 1;
        daysAdded1 += 1;
        day_col += 1;
    
         }
         if (count != 0)
         {


             int day_col2 = 5;

             DataCells.NewRow();
             DataCells.Rows.Add();
             AutoDTable.Rows[i]["Total_Days_Present"] = count;
             DataCells.Rows[k1]["MachineID"] = AutoDTable.Rows[i]["MachineID"];
             DataCells.Rows[k1]["ExistingCode"] = AutoDTable.Rows[i]["ExistingNo"];
             DataCells.Rows[k1]["DeptName"] = AutoDTable.Rows[i]["DeptName"];
             DataCells.Rows[k1]["FirstName"] = AutoDTable.Rows[i]["EmpName"];
             DataCells.Rows[k1]["Total_Days_Present"] = AutoDTable.Rows[i]["Total_Days_Present"];
           
             for (int intCol23 = 0; intCol23 < daysAdded1 ; intCol23++)
             {
                 string a="A";
                 if (AutoDTable.Rows[i][day_col2].ToString() == "x")
                 {
                     DataCells.Rows[k1][day_col1] = "<span style=color:green>" + AutoDTable.Rows[i][day_col2] + "</span>";
                 }
                 else
                 {
                     DataCells.Rows[k1][day_col1] = "<span style=color:red>" + a + "</span>";
                 }
                              
                 day_col1 += 1;
                 day_col2 += 1;
                
             }
             k1 += 1;
         }
         
    }
         int day_col3 = 4;
        DataCells.NewRow();
        DataCells.Rows.Add();
        DataCells.Rows[DataCells.Rows.Count - 1][day_col3 - 1] = "<b>GrandTotal</b>";
        for (int intcol24 = 0; intcol24 < daysAdded; intcol24++)
        {
            int count1=0;
            
            for (int i = 0; i < DataCells.Rows.Count-1; i++)
            {
                string aa = DataCells.Rows[i][day_col3].ToString();
                //if (aa.ToString() == "x")

                if (aa != "<span style=color:red>A</span>")
                {
                    count1 += 1;
                }
               
            }
            DataCells.Rows[DataCells.Rows.Count-1][day_col3] = count1;
            day_col3 += 1;
        }

        grid.DataSource = DataCells;
        grid.DataBind();
        string attachment = "attachment;filename=DAY ATTENDANCE BETWEEN DATES.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        //dt = objdata.Company_retrive(SessionCcode, SessionLcode);
        ////if (dt.Rows.Count > 0)
        ////{
        ////    CmpName = dt.Rows[0]["Cname"].ToString();
        ////    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        ////}
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);

        Response.Write("<table>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
        Response.Write(" --- ");
        Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">DAY ATTENDANCE BETWEEN DATES</a>");
        Response.Write("  ");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">FROM:" + Date1_str + "</a>");
        Response.Write(" -- ");
        Response.Write("<a style=\"font-weight:bold\">TO:" + Date2_str + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();
}

}
