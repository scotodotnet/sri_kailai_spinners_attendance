﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;



public partial class AbsentReportBetweenDatesReport : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    string SessionUserType;
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Date1_str;
    string Date2_str;
    string Wages;
    string SSQL;
    DataTable mEmployee = new DataTable();
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    DataTable mLogTime = new DataTable();
    DataTable mLocalDS = new DataTable();
    DataTable mLocalDSout = new DataTable();

    int k1 = 0;


    
    System.Web.UI.WebControls.DataGrid GridView1 =
                new System.Web.UI.WebControls.DataGrid();


    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay module | Report-Absent Report Between Dates";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

          
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();
            Wages = Session["Wages"].ToString();


            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("ExistingNo");
            AutoDTable.Columns.Add("MachineEncry");
            AutoDTable.Columns.Add("EmpName");

            DataCells.Columns.Add("EmployeeNo");
            DataCells.Columns.Add("ExistingCode");
            DataCells.Columns.Add("DeptName");
            DataCells.Columns.Add("FirstName");
            Date1_str = Request.QueryString["FromDate"].ToString();
            Date2_str = Request.QueryString["ToDate"].ToString();
           
            AbsentReport();
        }
            }

    public void AbsentReport()
    {
        DateTime date1;
        date1 = Convert.ToDateTime(Date1_str);
        string dat = Date2_str;
        DateTime Date2 = Convert.ToDateTime(dat);
        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;

        SSQL = "select DeptName,MachineID,ExistingCode,MachineID_Encrypt,FirstName from Employee_Mst where CompCode= '" + SessionCcode + "' And LocCode= '" + SessionLcode + "' and  IsActive='Yes' and Wages='" + Wages + "'";
        //if (SessionUserType == "2")
        //{
        //    SSQL = SSQL + " And IsNonAdmin='1'";
        //}
        
        
        mEmployee = objdata.ReturnMultipleValue(SSQL);
        if (mEmployee.Rows.Count < 0)
        {
        }
        else
        {
            int i1 = 0;
            for (int j1 = 0; j1 < mEmployee.Rows.Count; j1++)
            {
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[i1][0] = mEmployee.Rows[j1]["DeptName"].ToString();
                AutoDTable.Rows[i1][1] = mEmployee.Rows[j1]["MachineID"].ToString();
                AutoDTable.Rows[i1][2] = mEmployee.Rows[j1]["ExistingCode"].ToString();
                AutoDTable.Rows[i1][3] = mEmployee.Rows[j1]["MachineID_Encrypt"].ToString();
                AutoDTable.Rows[i1][4] = mEmployee.Rows[j1]["FirstName"].ToString();
                i1++;
            }
            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));
                DataCells.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                daycount -= 1;
                daysAdded += 1;
            }
            AutoDTable.Columns.Add("Total Days");
            DataCells.Columns.Add("Total Days");
            int j = 0;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                string mID = AutoDTable.Rows[i][3].ToString();
                int daycount1 = (int)((Date2 - date1).TotalDays);
                int daysAdded1 = 0;
                int day_col = 5;
                int day_col1 = 4;
                int count = 0;
                while (daycount1 >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded1).ToShortDateString());

                    SSQL = "";
                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + mID + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeIN >='" + dayy.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dayy.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

                    mLocalDS = objdata.ReturnMultipleValue(SSQL);

                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + mID + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeOUT >='" + dayy.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dayy.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                    mLocalDSout = objdata.ReturnMultipleValue(SSQL);

                    if (mLocalDS.Rows.Count == 0)
                    {

                        AutoDTable.Rows[i][day_col] = 'A';
                        count += 1;

                    }
                    daycount1 -= 1;
                    daysAdded1 += 1;
                    day_col += 1;
                }
                if (count != 0)
                {
                    int day_col2 = 5;
                    DataCells.NewRow();
                    DataCells.Rows.Add();
                    AutoDTable.Rows[i]["Total Days"] = count;
                    DataCells.Rows[k1]["EmployeeNo"] = AutoDTable.Rows[i]["MachineID"];
                    DataCells.Rows[k1]["ExistingCode"] = AutoDTable.Rows[i]["ExistingNo"];
                    DataCells.Rows[k1]["FirstName"] = AutoDTable.Rows[i]["EmpName"];
                    DataCells.Rows[k1]["DeptName"] = AutoDTable.Rows[i]["DeptName"];
                    DataCells.Rows[k1]["Total Days"] = AutoDTable.Rows[i]["Total Days"];
                    for (int intCol23 = 0; intCol23 < daysAdded1; intCol23++)
                    {
                        DataCells.Rows[k1][day_col1] = "<span style=color:red>" + AutoDTable.Rows[i][day_col2] + "</span>";
                        day_col1 += 1;
                        day_col2 += 1;
                    }
                    k1 += 1;
                }

            }

            int i2;
            int j2;
            int daycol1 = 4;
            int grand;

            DataCells.NewRow();
            DataCells.Rows.Add();
            DataCells.Rows[DataCells.Rows.Count - 1][daycol1 - 1] = "<b>Grand Total</b>";

            for (i2 = 0; i2 < daysAdded; i2++)
            {
                grand = 0;
                for (j2 = 0; j2 < DataCells.Rows.Count - 1; j2++)
                {
                    if (DataCells.Rows[j2][daycol1].ToString() == "<span style=color:red>A</span>")
                    {
                        grand = grand + 1;
                    }
                }
                DataCells.Rows[DataCells.Rows.Count - 1][daycol1] = grand;
                daycol1 += 1;
            }


            GridView1.HeaderStyle.Font.Bold = true;
            GridView1.DataSource = DataCells;
            GridView1.DataBind();
            string attachment = "attachment;filename=AbsentReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";

            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            GridView1.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td font-Bold='true' colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td font-Bold='true' colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\"> ABSENT REPORT BETWEEN DATES &nbsp;&nbsp;&nbsp;</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
           
            Response.Write("<a style=\"font-weight:bold\"> FROM  -" + Date1_str + "</a>");
            Response.Write("&nbsp;&nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\"> TO -" + Date2_str + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
           
        }
    }
}
