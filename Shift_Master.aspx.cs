﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;

public partial class Shift_Master : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    string SSQL;
    bool ErrFlag = false;
    static int SNo;
    DataTable dtdMstShift = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Shift Master";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                DropDown_TokenNumber();
                DropDown_ShiftType();
            }
        }
    }

  
    protected void ddlTicketNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable dted = new DataTable();
        string s = ddlTicketNo.SelectedItem.Text;
        string[] delimiters = new string[] { "-->" };
        string[] items = s.Split(delimiters, StringSplitOptions.None);
        string ss = items[0];
        string ss1 = items[1];
        txtEmpName.Text = ss1.ToString();
        txtEmpName.Text = items[1];
    
        dted = objdata.Manual_Data(ss.ToString());
        if (dted.Rows.Count > 0)
        {
         txtEmpName.Text = dted.Rows[0]["FirstName"].ToString();

         string empcategory = dted.Rows[0]["CatName"].ToString();

         if (empcategory.ToString() == "STAFF")
         {
             ddlCategory.SelectedIndex = 1;
         }
         else
         {
             ddlCategory.SelectedIndex = 2;
         }
        }
      }

    public void DropDown_TokenNumber()
    {
        DataTable dt = new DataTable();
        dt = objdata.dropdown_TokenNumber(SessionCcode, SessionLcode);
        ddlTicketNo.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["EmpName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlTicketNo.Items.Add(dt.Rows[i]["EmpName"].ToString());
        }
    }


    public void DropDown_ShiftType()
    {
        DataTable dt = new DataTable();
        //dt = objdata.dropdown_TokenNumber(SessionCcode, SessionLcode);
        dt = objdata.Dropdown_ShiftType(SessionCcode, SessionLcode);
        ddlTicketNo.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["ShiftDesc"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
          ddlShiftType.Items.Add(dt.Rows[i]["ShiftDesc"].ToString());
        }
    }







    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ddlTicketNo.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Employee No');", true);
            ErrFlag = true;
        }

        else if (ddlCategory.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Employee Category');", true);
            ErrFlag = true;
        }
        else if (ddlShiftType.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Shift Type');", true);
            ErrFlag = true;
        }
        else
        {
            string ss1 = txtDate.Text;
            DateTime da = Convert.ToDateTime(ss1.ToString());
            string Month = da.ToString("MMMM");
            string Year = da.Year.ToString();
            DataTable dtdupdate = new DataTable();

            SSQL = "select * from MstShifUpload where MachineID='" + ddlTicketNo.SelectedItem.Text + "' and EmpCatgory='" + ddlCategory.SelectedItem.Text + "'";
            SSQL = SSQL + " And Months='" + Month + "' and Year='" + Year + "'";

            dtdupdate = objdata.ReturnMultipleValue(SSQL);
            if (dtdupdate.Rows.Count > 0)
            {
                string fromdate = dtdupdate.Rows[0]["FromDate"].ToString();
                DateTime fdate = Convert.ToDateTime(fromdate.ToString());
                string given_Date = txtDate.Text;
                DateTime tdate = Convert.ToDateTime(given_Date.ToString());
                int fday = fdate.Day;
                int tday = tdate.Day;
                int daycount = tday - fday +1;

                string f_date = "Date" + daycount;
                //string f_date = daycount + days.ToString();

                SSQL = "update  MstShifUpload set " + f_date + "='" + ddlShiftType.Text + "'where MachineID='" + ddlTicketNo.SelectedItem.Text + "'and EmpCatgory='" + ddlCategory.SelectedItem.Text + "' and Months='" + Month + "'";
                dtdMstShift = objdata.ReturnMultipleValue(SSQL);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Manual Shift Updated sucessFully');", true);
                ErrFlag = true;
            }
        }
    }

    protected void BtnClear_Click(object sender, EventArgs e)
    {
        ddlTicketNo.SelectedIndex = 0;
        ddlCategory.SelectedIndex = 0;
        ddlShiftType.SelectedIndex = 0;
        txtEmpName.Text = "";
        txtDate.Text = "";
    }



}
