﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class DayAttendanceChart : System.Web.UI.Page
{
    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;


    BALDataAccess objdata = new BALDataAccess();
    //string SessionAdmin = "admin";
    //string SessionCcode = "ESM";
    //string SessionLcode = "UNIT II";

    DataSet ds = new DataSet();
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    DataTable Abstract_table = new DataTable();
        DataTable mLocalDS_INTAB=new DataTable();
        DataTable mLocalDS_OUTTAB=new DataTable();
        string Date_Value_Str = null;
	    string Time_IN_Str = "";
	    string Time_Out_Str = "";
	    Int32 time_Check_dbl = 0;
	    string Total_Time_get = "";

        DataTable Payroll_DS = new DataTable();

        string Shift_Start_Time = null;
	    string Shift_End_Time = null;
	    string Employee_Time = "";
	    DateTime ShiftdateStartIN = default(DateTime);
	    DateTime ShiftdateEndIN = default(DateTime);
	    DateTime EmpdateIN = default(DateTime);
        string SSQL="";
    DataTable mDataSet=new DataTable();
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string[] Time_Minus_Value_Check;

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Report-Day attendance Chart";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();


            DataCells.Columns.Add("Dept");
            DataCells.Columns.Add("Type");
            DataCells.Columns.Add("Shift");
            DataCells.Columns.Add("EmpCode");
            DataCells.Columns.Add("ExCode");
            DataCells.Columns.Add("Name");
            DataCells.Columns.Add("TimeIN");
            DataCells.Columns.Add("TimeOUT");
            DataCells.Columns.Add("MachineID");
            DataCells.Columns.Add("Category");
            DataCells.Columns.Add("SubCategory");
            DataCells.Columns.Add("TotalMIN");
            DataCells.Columns.Add("GrandTOT");
            DataCells.Columns.Add("ShiftDate");
            DataCells.Columns.Add("OTHour");
            DataCells.Columns.Add("CompanyName");
            DataCells.Columns.Add("LocationName");




            ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            ddlShiftType = Request.QueryString["ddlShiftType"].ToString();


            GetAttdDayWise_DayAttnReport();


            Int32 Get_Actual_Present = 0;
            Int32 Time_In_Present = 0;
            string Wages_Type_Gender_Join = "";
            string Wages_Type_Adolo_Join = "";
            string Adolescent_Check = "0";
            string Get_Time_IN = "";
            string Get_Time_Out = "";
            string Get_Total_Hours = "";
            string Get_ShiftName = "";

            DataTable Emp_DS = new DataTable();
            DataTable table = new DataTable();
            DataTable Abstract_table = new DataTable();

            table.Columns.Add("CompanyName");
            table.Columns.Add("LocationName");
            table.Columns.Add("ShiftDate");
            table.Columns.Add("SNo");
            table.Columns.Add("Dept");
            table.Columns.Add("Type");
            table.Columns.Add("Shift");
            table.Columns.Add("Category");
            table.Columns.Add("SubCategory");
            table.Columns.Add("EmpCode");
            table.Columns.Add("ExCode");
            table.Columns.Add("Name");
            table.Columns.Add("TimeIN");
            table.Columns.Add("TimeOUT");
            table.Columns.Add("MachineID");
            table.Columns.Add("PrepBy");
            table.Columns.Add("PrepDate");
            table.Columns.Add("TotalMIN");
            table.Columns.Add("GrandTOT");

            DataTable Delete_DT = new DataTable();
            SSQL = "Delete from Day_Attn_Record_Det_Saved where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            Delete_DT = objdata.ReturnMultipleValue(SSQL);
            int k = 0;
            for (int iRow = 0; iRow < AutoDTable.Rows.Count; iRow++)
            {


                table.NewRow();
                table.Rows.Add();

                table.Rows[k]["CompanyName"] = SessionCcode;
                table.Rows[k]["LocationName"] = SessionLcode;
                table.Rows[k]["ShiftDate"] = Date;
                table.Rows[k]["Dept"] = AutoDTable.Rows[iRow]["Dept"];
                table.Rows[k]["Type"] = AutoDTable.Rows[iRow]["Type"];

                table.Rows[k]["Shift"] = AutoDTable.Rows[iRow]["Shift"];
                Get_ShiftName = AutoDTable.Rows[iRow]["Shift"].ToString();

                table.Rows[k]["Category"] = AutoDTable.Rows[iRow]["Category"];
                table.Rows[k]["SubCategory"] = AutoDTable.Rows[iRow]["SubCategory"];
                table.Rows[k]["EmpCode"] = AutoDTable.Rows[iRow]["EmpCode"];
                table.Rows[k]["ExCode"] = AutoDTable.Rows[iRow]["ExCode"];
                table.Rows[k]["Name"] = AutoDTable.Rows[iRow]["Name"];
                if (AutoDTable.Rows[iRow][7].ToString() != "")
                {
                    table.Rows[k]["TimeIN"] = String.Format("{0:hh:mm tt}", (AutoDTable.Rows[iRow][7]));
                    Get_Time_IN = String.Format("{0:hh:mm tt}", (AutoDTable.Rows[iRow][7]));
                }
                else
                {
                    table.Rows[k]["TimeIN"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow][7]);
                    Get_Time_IN = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow][7]);
                }

                if (AutoDTable.Rows[iRow][8] != "")
                {
                    table.Rows[k]["TimeOUT"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow][8]);
                    Get_Time_Out = String.Format("{0:hh:mm tt}", (AutoDTable.Rows[iRow][8]));
                }
                else
                {
                    table.Rows[k]["TimeOUT"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow][8]);
                    Get_Time_Out = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow][8]);
                }

                table.Rows[k]["MachineID"] = AutoDTable.Rows[iRow][9];
                table.Rows[k]["PrepBy"] = "User";
                table.Rows[k]["PrepDate"] = Date;

                table.Rows[k]["TotalMIN"] = AutoDTable.Rows[iRow]["TotalMIN"];
                if ((AutoDTable.Rows[iRow][8].ToString() == "") || (AutoDTable.Rows[iRow]["GrandTOT"].ToString() == "00:00"))
                {
                    table.Rows[k]["GrandTOT"] = "00:00";
                    Get_Total_Hours = "00:00";
                    Get_Actual_Present = 0;
                }
                else
                {
                    if ((AutoDTable.Rows[iRow]["GrandTOT"]) != "")
                    {
                        table.Rows[k]["GrandTOT"] = AutoDTable.Rows[iRow]["GrandTOT"].ToString();
                        Get_Total_Hours = AutoDTable.Rows[iRow]["GrandTOT"].ToString();
                        //Time Spilit
                        string[] Time_Split_Str;
                        Int32 Time_4Hrs_Check = 0;
                        Time_Split_Str = AutoDTable.Rows[iRow]["GrandTOT"].ToString().Split(':');
                        Time_4Hrs_Check = Convert.ToInt32(Time_Split_Str[0]);
                        if (Time_4Hrs_Check >= 4)
                        {
                            Get_Actual_Present = 1;
                        }
                        else
                        {
                            Get_Actual_Present = 0;
                        }
                    }
                    else
                    {
                        table.Rows[k]["GrandTOT"] = AutoDTable.Rows[iRow]["GrandTOT"];
                        Get_Total_Hours = AutoDTable.Rows[iRow]["GrandTOT"].ToString();
                        Get_Actual_Present = 0;
                    }
                }

                //Insert Table Reocrd
                Time_In_Present = 1;
                SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID_Encrypt = '" + AutoDTable.Rows[iRow][9].ToString() + "' And ExistingCode='" + AutoDTable.Rows[iRow][5].ToString() + "'";
                Emp_DS = objdata.ReturnMultipleValue(SSQL);
                if (Emp_DS.Rows.Count != 0)
                {
                    if (Emp_DS.Rows[0]["Wages"].ToString() == ("REGULAR") | (Emp_DS.Rows[0]["Wages"].ToString() == ("HOSTEL")))
                    {
                        Wages_Type_Gender_Join = Emp_DS.Rows[0]["Wages"].ToString();
                        if (Emp_DS.Rows[0]["Gender"].ToString() == "Female")
                        {
                            Wages_Type_Gender_Join = Wages_Type_Gender_Join + " - " + Emp_DS.Rows[0]["Gender"].ToString();
                        }
                        else
                        {
                            Wages_Type_Gender_Join = Wages_Type_Gender_Join + " - " + "Male";
                        }
                    }
                    else
                    {
                        Wages_Type_Gender_Join = Emp_DS.Rows[0]["Wages"].ToString();
                    }
                    //if (Emp_DS.Rows[0]["Adolescent"].ToString() == "") {
                    //    Adolescent_Check = "0";
                    //    Wages_Type_Adolo_Join = Emp_DS.Rows[0]["Wages"].ToString();
                    //} else if (Emp_DS.Rows[0]["Adolescent"] == "1") {
                    //    Adolescent_Check = "1";
                    //    if ((Emp_DS.Rows[0]["Wages"].ToString()) == ("REGULAR") | (Emp_DS.Rows[0]["Wages"].ToString()) == ("HOSTEL"))
                    //    {
                    //        Wages_Type_Adolo_Join = Emp_DS.Rows[0]["Wages"].ToString() + "_Adolo";
                    //    } else {
                    //        Wages_Type_Adolo_Join = Emp_DS.Rows[0]["Wages"].ToString();
                    //    }

                    //} else {
                    //    Adolescent_Check = "0";
                    //    Wages_Type_Adolo_Join = Emp_DS.Rows[0]["Wages"].ToString();
                    //}

                    //Insert Record
                    DataTable Insert_DT = new DataTable();
                    SSQL = "Insert Into Day_Attn_Record_Det_Saved(CompCode,LocCode,MachineID,ExistingCode,FirstName,LastName,Department,Wages_Type,";
                    SSQL = SSQL + "Wages_Gender_Join,Gender,Actual_Present,TimeINPresent,TimeIN,TimeOut,TotalHrs,Shift,Wages_Adolo_Join) Values('" + SessionCcode + "'";
                    SSQL = SSQL + ",'" + SessionLcode + "','" + Emp_DS.Rows[0]["MachineID"] + "','" + Emp_DS.Rows[0]["ExistingCode"] + "','" + Emp_DS.Rows[0]["FirstName"] + "'";
                    SSQL = SSQL + ",'" + Emp_DS.Rows[0]["LastName"] + "','" + Emp_DS.Rows[0]["DeptName"] + "','" + Emp_DS.Rows[0]["Wages"] + "','" + Wages_Type_Gender_Join + "'";
                    SSQL = SSQL + ",'" + Emp_DS.Rows[0]["Gender"] + "','" + Get_Actual_Present + "','" + Time_In_Present + "'";
                    SSQL = SSQL + ",'" + Get_Time_IN + "','" + Get_Time_Out + "','" + Get_Total_Hours + "','" + Get_ShiftName + "','" + Wages_Type_Adolo_Join + "')";

                    Insert_DT = objdata.ReturnMultipleValue(SSQL);

                }

                k = k + 1;
            }

            //Get Department Abstract
            Abstract_table.Columns.Add("CompanyName", typeof(string));
            Abstract_table.Columns.Add("LocationName", typeof(string));
            Abstract_table.Columns.Add("ShiftDate", typeof(string));
            Abstract_table.Columns.Add("Dept", typeof(string));
            Abstract_table.Columns.Add("Actual_Present_Count", typeof(decimal));
            Abstract_table.Columns.Add("TimeIn_Present_Count", typeof(decimal));
            Abstract_table.Columns.Add("Dept_Onroll_Count", typeof(decimal));
            Abstract_table.Columns.Add("Actual_Absent_Count", typeof(decimal));
            Abstract_table.Columns.Add("TimeIn_Absent_Count", typeof(decimal));

            SSQL = "Select Department,isnull(sum(Actual_Present),0) as ActualCount,isnull(sum(TimeINPresent),0) as TimeINCount from Day_Attn_Record_Det_Saved where";
            SSQL = SSQL + " CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " Group by Department Order by Department Asc";
            mDataSet = objdata.ReturnMultipleValue(SSQL);
            int j = 0;
            if (mDataSet.Rows.Count != 0)
            {
                for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
                {
                    Abstract_table.NewRow();
                    Abstract_table.Rows.Add();
                    Abstract_table.Rows[j]["CompanyName"] = SessionCcode;
                    Abstract_table.Rows[j]["LocationName"] = SessionLcode;
                    Abstract_table.Rows[j]["ShiftDate"] = Date;
                    Abstract_table.Rows[j]["Dept"] = mDataSet.Rows[iRow]["Department"].ToString();
                    Abstract_table.Rows[j]["Actual_Present_Count"] = mDataSet.Rows[iRow]["ActualCount"];
                    Abstract_table.Rows[j]["TimeIn_Present_Count"] = mDataSet.Rows[iRow]["TimeINCount"];

                    Int32 Dept_Onroll_Count = 0;
                    Int32 Actual_Present_Count = Convert.ToInt32(mDataSet.Rows[iRow]["ActualCount"]);
                    Int32 TimeIn_Present_Count = Convert.ToInt32(mDataSet.Rows[iRow]["TimeINCount"]);
                    Int32 Actual_Absent_Count = 0;
                    Int32 TimeIn_Absent_Count = 0;
                    DataTable Onroll_DS = new DataTable();

                    //Get onroll count
                    SSQL = "Select isnull(count(EmpNo),0) as Onroll_Count from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And isActive='Yes' And DeptName='" + mDataSet.Rows[iRow]["Department"].ToString() + "'";
                    Onroll_DS = objdata.ReturnMultipleValue(SSQL);
                    if (Onroll_DS.Rows.Count != 0)
                    {
                        Dept_Onroll_Count = Convert.ToInt32(Onroll_DS.Rows[0]["Onroll_Count"]);
                        if (Dept_Onroll_Count != 0)
                        {
                            Actual_Absent_Count = Dept_Onroll_Count - Actual_Present_Count;
                            TimeIn_Absent_Count = Dept_Onroll_Count - TimeIn_Present_Count;
                            Abstract_table.Rows[j]["Dept_Onroll_Count"] = Dept_Onroll_Count;
                            Abstract_table.Rows[j]["Actual_Absent_Count"] = Actual_Absent_Count;
                            Abstract_table.Rows[j]["TimeIn_Absent_Count"] = TimeIn_Absent_Count;
                        }
                        else
                        {
                            Abstract_table.Rows[j]["Dept_Onroll_Count"] = 0;
                            Abstract_table.Rows[j]["Actual_Absent_Count"] = 0;
                            Abstract_table.Rows[j]["TimeIn_Absent_Count"] = 0;
                        }
                    }
                    else
                    {
                        Abstract_table.Rows[j]["Dept_Onroll_Count"] = 0;
                        Abstract_table.Rows[j]["Actual_Absent_Count"] = 0;
                        Abstract_table.Rows[j]["TimeIn_Absent_Count"] = 0;
                    }

                    j = j + 1;
                }
            }

            ds.Tables.Add(Abstract_table);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Department_Abstract_Chart.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            CrystalReportViewer1.ReportSource = report;

        }

    }
    public void GetAttdDayWise_DayAttnReport()
    {
  
        DataColumn auto = new DataColumn();
        auto.AutoIncrement = true;
        auto.AutoIncrementSeed = 1;

        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Type");
        AutoDTable.Columns.Add("Shift");
        AutoDTable.Columns.Add("EmpCode");
        AutoDTable.Columns.Add("ExCode");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("TimeIN");
        AutoDTable.Columns.Add("TimeOUT");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("Category");
        AutoDTable.Columns.Add("SubCategory");
        AutoDTable.Columns.Add("TotalMIN");
        AutoDTable.Columns.Add("GrandTOT");
        AutoDTable.Columns.Add("ShiftDate");
        AutoDTable.Columns.Add("OTHour");
        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");


        SSQL = "";
	    SSQL = "Select IPAddress,IPMode from IPAddress_Mst Where Compcode='" + SessionCcode + "'";
	    SSQL = SSQL + " And LocCode='" + SessionLcode + "'";

        mDataSet=objdata.ReturnMultipleValue(SSQL);

	if ( mDataSet.Rows.Count > 0) {
		for (int iRow = 0; iRow <  mDataSet.Rows.Count; iRow++) {
			if (( mDataSet.Rows[iRow]["IPMode"].ToString()) == "IN") 
            {
				mIpAddress_IN =  mDataSet.Rows[iRow]["IPAddress"].ToString();
			} else if (( mDataSet.Rows[iRow]["IPMode"].ToString()) == "OUT") 
            {
				mIpAddress_OUT =  mDataSet.Rows[iRow]["IPAddress"].ToString();
			}
		}
	}
DataTable mLocalDS = new DataTable();

	if (ModeType == "IN/OUT") {
		SSQL = "";
		SSQL = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
		SSQL = SSQL + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
		SSQL += " from Shift_Mst Where CompCode='" + SessionCcode + "'";
		SSQL += " And LocCode='" + SessionLcode + "'";
		if (ShiftType1 != "ALL") {
			SSQL += " And shiftDesc='" + ShiftType1 + "'";
		}
		SSQL += " And ShiftDesc like '" + ddlShiftType + "%'";
		//Shift%'"
		SSQL += " Order By shiftDesc";

	} else {
		return;
	}
	string ShiftType = "";

	mLocalDS = objdata.ReturnMultipleValue(SSQL);

        if (SessionLcode == "UNIT I") 
        {
		for (int i = 0; i < mLocalDS.Rows.Count; i++) 
        {
			if (mLocalDS.Rows[i]["shiftDesc"].ToString() == "SHIFT10") 
            {
				mLocalDS.Rows[i]["StartIN_Days"] = -1;
				mLocalDS.Rows[i]["EndIN_Days"] = 0;
			}
		}
	}
    string ng = string.Format(Date, "dd-MM-yyyy");
            DateTime date1 = Convert.ToDateTime(ng);
            DateTime date2 = date1.AddDays(1);
            string Start_IN = "";
            string End_In;

            string End_In_Days = "";
            string Start_In_Days = "";
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();


            int mStartINRow = 0;
            int mStartOUTRow = 0;

        for (int iTabRow = 0; iTabRow < mLocalDS.Rows.Count; iTabRow++) {


             if (AutoDTable.Rows.Count <= 1)
                {
                    mStartOUTRow = 0;
                }
                else
                {
                    mStartOUTRow = AutoDTable.Rows.Count - 1;
                }

                if (mLocalDS.Rows[iTabRow]["shiftDesc"].ToString() == "GENERAL")
                {
                    ShiftType = "GENERAL";
                }
                else
                {
                    ShiftType = "SHIFT";
                }


                string sIndays_str = mLocalDS.Rows[iTabRow]["StartIN_Days"].ToString();
                double sINdays = Convert.ToDouble(sIndays_str);
                string eIndays_str = mLocalDS.Rows[iTabRow]["EndIN_Days"].ToString();
                double eINdays = Convert.ToDouble(eIndays_str);
                string ss = "";
	
	if (ShiftType == "GENERAL")
                {
                    ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                    ss = ss + " Where LT.Compcode='" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + "And LT.TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    ss = ss + "And LT.TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    ss = ss + "AND em.ShiftType='" + ddlShiftType + "' And EM.Compcode='" + SessionCcode.ToString() + "'";
                    ss = ss + "And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsActive='yes' Group By LT.MachineID Order By Min(LT.TimeIN)";
                }


                else if (ShiftType == "SHIFT")
                {
                    ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                    ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And EM.IsActive='yes' And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartIN"].ToString() + "' ";
                    ss = ss + " And LT.TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndIN"].ToString() + "'";
                    ss = ss + " Group By LT.MachineID Order By Min(LT.TimeIN)";
                }
                else
                {
                    ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                    ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And EM.IsActive='yes' And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartIN"].ToString() + "' ";
                    ss = ss + " And LT.TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndIN"].ToString() + "' ";
                    ss = ss + " AND EM.ShiftType='" + ddlShiftType + "' ";
                    ss = ss + " Group By LT.MachineID Order By Min(LT.TimeIN)";
                }


                mDataSet = objdata.ReturnMultipleValue(ss);

	 if (mDataSet.Rows.Count > 0)
                {
                    string MachineID;

                    for (int iRow = 0; iRow < mDataSet.Rows.Count - 1; iRow++)
                    {
                        Boolean chkduplicate = false;
                        chkduplicate = false;

                        for (int ia = 0; ia < AutoDTable.Rows.Count - 1; ia++)
                        {
                            string id = mDataSet.Rows[iRow]["MachineID"].ToString();

                            if (id == AutoDTable.Rows[ia][9].ToString())
                            {
                                chkduplicate = true;
                            }
                        }
                        if (chkduplicate == false)
                        {
                            AutoDTable.NewRow();
                            AutoDTable.Rows.Add();


                            // AutoDTable.Rows.Add();

                            // AutoDTable.Rows[mStartINRow][0] = iRow + 1;
                            AutoDTable.Rows[mStartINRow][3] = mLocalDS.Rows[iTabRow]["ShiftDesc"].ToString();
                            if (ShiftType == "SHIFT")
                            {
                                string str = mDataSet.Rows[iRow][1].ToString();
                                // string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1])
                                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);
                            }
                            else
                            {
                                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);
                            }


                            MachineID = mDataSet.Rows[iRow]["MachineID"].ToString();
                            AutoDTable.Rows[mStartINRow][9] = MachineID.ToString();
                            mStartINRow += 1;
                        }
                    }
                }
            
		mDataSet = new DataTable();

	
		SSQL = "";
		SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT ";
		SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
		SSQL = SSQL + " And LocCode='" + SessionLcode + "'";


		if (mLocalDS.Rows[iTabRow]["shiftDesc"] == "SHIFT1") {
			SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
			SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
		} else {
			SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
			SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
		}

		SSQL = SSQL + " Group By MachineID";
		SSQL = SSQL + " Order By Max(TimeOUT)";


		mDataSet = objdata.ReturnMultipleValue(SSQL);

        string InMachine_IP = "";
		DataTable mLocalDS_out = new DataTable();
	
	
		for (int iRow2 = mStartOUTRow; iRow2 <AutoDTable.Rows.Count; iRow2++) {
			InMachine_IP = AutoDTable.Rows[iRow2][9].ToString();
			//TimeOUT Get
		
			SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
			SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

			//Day Atten. Time Order by Change (Eveready MILL)
			if (AutoDTable.Rows[iRow2][3].ToString() == "SHIFT1") 
            {
				SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "08:00" + "'";
				SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:45" + "' Order by TimeOUT Asc";
			} else {
				SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
				SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Asc";
			}

			mLocalDS_out = objdata.ReturnMultipleValue(SSQL);
			if (mLocalDS_out.Rows.Count <= 0) {
				//Skip
			} else {
				if (AutoDTable.Rows[iRow2][9].ToString() == mLocalDS_out.Rows[0][0].ToString()) {
					AutoDTable.Rows[iRow2][8]= string.Format("{0:hh:mm tt}", (mLocalDS_out.Rows[0][1].ToString()));
					//GoTo 1
				}
			}
		

			//Grand Total value Display
			Time_IN_Str = "";
			Time_Out_Str = "";
			
			Date_Value_Str = date1.ToString("yyyy/MM/dd");
			if (SessionLcode == "UNIT I" & mLocalDS.Rows[iTabRow]["shiftDesc"].ToString() == "SHIFT10") {
				SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
				SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
				SSQL = SSQL + " And TimeIN >='" + date1.AddDays(-1).ToString("yyyy/MM/dd") + " " + "23:00' And TimeIN <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:59' Order by TimeIN ASC";
			} else {
				SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
				SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
				SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
			}

			mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);
			DateTime InTime_Check = default(DateTime);
			DateTime InToTime_Check = default(DateTime);
			TimeSpan InTime_TimeSpan = default(TimeSpan);
			string From_Time_Str = "";
			string To_Time_Str = "";
			DataTable DS_Time = default(DataTable);
			DataTable DS_InTime = default(DataTable);
			string Final_InTime = "";
			string Final_OutTime = "";
			string Final_Shift = "";
			DataTable Shift_DS = default(DataTable);
			Int32 K = 0;
			bool Shift_Check_blb = false;
			if ((AutoDTable.Rows[iRow2][3].ToString() == "SHIFT1") | (AutoDTable.Rows[iRow2][3].ToString() == "SHIFT2"))
            {
				 InTime_Check = Convert.ToDateTime(AutoDTable.Rows[iRow2][7].ToString());
                        InToTime_Check = InTime_Check.AddHours(2);

                        string s1 = InTime_Check.ToString("HH:mm:ss");
                        string s2 = InToTime_Check.ToString("HH:mm:ss");

                        string InTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InTime_Check);
                        string InToTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InToTime_Check);
                        InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
                        From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                        InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
                        To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;

				//Two Hours OutTime Check
				SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
				SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
				SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + From_Time_Str + "' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";
				DS_Time = objdata.ReturnMultipleValue(SSQL);
				if (DS_Time.Rows.Count != 0) {
					SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
					SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
					SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
					DS_InTime = objdata.ReturnMultipleValue(SSQL);
					if (DS_InTime.Rows.Count != 0) 
                    {
					            Final_InTime = DS_InTime.Rows[0][0].ToString();
                                SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And ShiftDesc like '%SHIFT%'";
                                Shift_DS = objdata.ReturnMultipleValue(SSQL);
                                Shift_Check_blb = false;


						  for (int k = 0; k < Shift_DS.Rows.Count; k++)
                                {
                                    string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                    int b = Convert.ToInt16(a.ToString());
                                    Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                    string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                    int b1 = Convert.ToInt16(a1.ToString());
                                    Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                    ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                                    ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                                    EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());
                                    if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                    {
                                        Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                    }
                                }
						 if (Shift_Check_blb == true)
                                {
                                    AutoDTable.Rows[iRow2][3] = Final_Shift.ToString();
                                    AutoDTable.Rows[iRow2][7] = String.Format("HH:mm:ss", Final_InTime);

							//IN Time Query Update
							SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
							SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
							SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
							mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);
							//Out Time Query Update
							if (Final_Shift == "SHIFT2") {
								SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
								SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
								SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
							} else {
								SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
								SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
								SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "17:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
							}
							DS_Time = objdata.ReturnMultipleValue(SSQL);
							//Out Time Update
							if (DS_Time.Rows.Count != 0) {
								AutoDTable.Rows[iRow2][8]= string.Format("{0:hh:mm tt}", (DS_Time.Rows[0][0]));
							}
						} else {
							if (AutoDTable.Rows[iRow2][3].ToString() == "SHIFT1") {
								SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
								SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
								SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' Order by TimeOUT Asc";
							} else {
								SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
								SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
								SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
							}

							DS_Time = objdata.ReturnMultipleValue(SSQL);
							//Out Time Update
							if (DS_Time.Rows.Count != 0) {
								AutoDTable.Rows[iRow2][8]= string.Format("{0:hh:mm tt}", (DS_Time.Rows[0][0]));
							}
						}
					} else {
						if (AutoDTable.Rows[iRow2][3].ToString() == "SHIFT1") {
							SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
							SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
							SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "14:00' Order by TimeOUT Asc";
						} else {
							SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
							SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
							SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
						}

						DS_Time = objdata.ReturnMultipleValue(SSQL);
						//Out Time Update
						if (DS_Time.Rows.Count != 0) {
							AutoDTable.Rows[iRow2][8]= string.Format("{0:hh:mm tt}", (DS_Time.Rows[0][0]));
						}
					}
				} else {
					SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
					SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
					SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
				}
				//New Code End
			} else {
				SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
				SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
				SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
			}

			mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);
			string Emp_Total_Work_Time_1 = "00:00";
			if (mLocalDS_INTAB.Rows.Count > 1) {

				for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++) {
					Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

					if (mLocalDS_OUTTAB.Rows.Count > tin) {
						Time_Out_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
					} else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count) {
						Time_Out_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count][0].ToString();
					} else {
						Time_Out_Str = "";
					}
					TimeSpan ts4 = new TimeSpan();
					ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
					
					if (mLocalDS.Rows.Count <= 0) {
						Time_IN_Str = "";
					} else {
						Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
					}
					

					//Emp_Total_Work_Time
					if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str)) {
						time_Check_dbl = time_Check_dbl;
					} else {
						DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
						DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
						//If date3 > date4 Then

						//    date4 = System.Convert.ToDateTime(mLocalDS_OUTTAB.Tables(0).Rows(tin + 1)(0))
						//End If
						TimeSpan ts1 = new TimeSpan();
						ts1 = date4.Subtract(date3);
						ts1 = date4.Subtract(date3);
						Total_Time_get = ts1.Hours.ToString();
						//& ":" & Trim(ts.Minutes)
						//ts4 = ts4.Add(ts1)
						//OT Time Get
						Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
						Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
						if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
							Emp_Total_Work_Time_1 = "00:00";
						if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
							Emp_Total_Work_Time_1 = "00:00";
						//MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")

						if (Left_Val(Total_Time_get, 1) == "-") {
							date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
							ts1 = date4.Subtract(date3);
							ts1 = date4.Subtract(date3);
							ts4 = ts4.Add(ts1);
                            Total_Time_get = ts1.Hours.ToString();
                            //& ":" & Trim(ts.Minutes)
							time_Check_dbl = Convert.ToInt32(Total_Time_get);
							//Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
							//Emp_Total_Work_Time_1 = Trim(ts1.Hours) & ":" & Trim(ts1.Minutes)
							Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
							Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
							if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
								Emp_Total_Work_Time_1 = "00:00";
							if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
								Emp_Total_Work_Time_1 = "00:00";
						} else {
							ts4 = ts4.Add(ts1);
							Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
							Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
							if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
								Emp_Total_Work_Time_1 = "00:00";
							if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
								Emp_Total_Work_Time_1 = "00:00";
							time_Check_dbl = Convert.ToInt32(Total_Time_get);
						}
					}
					AutoDTable.Rows[iRow2][13]= Emp_Total_Work_Time_1;

					
				}

			} else {
				TimeSpan ts4 = new TimeSpan();
				ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
				if (mLocalDS_INTAB.Rows.Count <= 0) {
					Time_IN_Str = "";
				} else {
					Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
				}
				for (int tout = 0; tout < mLocalDS_OUTTAB.Rows.Count; tout++) {
					if (mLocalDS_OUTTAB.Rows.Count <= 0) {
						Time_Out_Str = "";
					} else {
						Time_Out_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
					}

				}
				//Emp_Total_Work_Time
				if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str)) {
					time_Check_dbl = 0;
				} else {
					DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
					DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
					TimeSpan ts1 = new TimeSpan();
					ts1 = date4.Subtract(date3);
					ts1 = date4.Subtract(date3);
					Total_Time_get = ts1.Hours.ToString();
				
					if (Left_Val(Total_Time_get, 1) == "-") {
						date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
						ts1 = date4.Subtract(date3);
						ts1 = date4.Subtract(date3);
						ts4 = ts4.Add(ts1);
						Total_Time_get = ts1.Hours.ToString();
						//& ":" & Trim(ts.Minutes)
						time_Check_dbl = Convert.ToInt32(Total_Time_get);
						//Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
						//Emp_Total_Work_Time_1 = Trim(ts1.Hours) & ":" & Trim(ts1.Minutes)
						Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
						Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
						if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
							Emp_Total_Work_Time_1 = "00:00";
						if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
							Emp_Total_Work_Time_1 = "00:00";
					} else {
						ts4 = ts4.Add(ts1);
						Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
						Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
						if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
							Emp_Total_Work_Time_1 = "00:00";
						if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
							Emp_Total_Work_Time_1 = "00:00";
						time_Check_dbl = Convert.ToInt32(Total_Time_get);
					}
					AutoDTable.Rows[iRow2][12]= Emp_Total_Work_Time_1;
				}
			}
			AutoDTable.Rows[iRow2][13]= Emp_Total_Work_Time_1;
		
		
		}
		
		
	}

DataTable mEmployeeDS = new DataTable();

	SSQL = "";
	SSQL = "select Distinct isnull(EM.MachineID_Encrypt,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
	SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
	SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
	SSQL = SSQL + ",isnull(EM.CatName,'') as [CatName], isnull(EM.SubCatName,'') as [SubCatName]";
	SSQL = SSQL + ",DM.DeptCode from Employee_Mst EM inner Join Department_Mst DM on DM.DeptName=EM.DeptName Where EM.Compcode='" + SessionCcode + "'";
	SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'" + " order by DM.DeptCode Asc";

	mEmployeeDS = objdata.ReturnMultipleValue(SSQL);



	if (mEmployeeDS.Rows.Count > 0) {
		
		for (int iRow1 = 0; iRow1 <mEmployeeDS.Rows.Count; iRow1++) {
			for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++) {
				if (AutoDTable.Rows[iRow2][9].ToString() == mEmployeeDS.Rows[iRow1]["MachineID"].ToString()) {
					AutoDTable.Rows[iRow2]["Dept"]= mEmployeeDS.Rows[iRow1]["DeptName"].ToString();
					AutoDTable.Rows[iRow2]["Type"]=mEmployeeDS.Rows[iRow1]["TypeName"].ToString();
					AutoDTable.Rows[iRow2]["EmpCode"]=mEmployeeDS.Rows[iRow1]["EmpNo"].ToString();
					AutoDTable.Rows[iRow2]["ExCode"]=mEmployeeDS.Rows[iRow1]["ExistingCode"].ToString();
					AutoDTable.Rows[iRow2]["Name"]=mEmployeeDS.Rows[iRow1]["FirstName"].ToString();
					AutoDTable.Rows[iRow2]["Category"]=mEmployeeDS.Rows[iRow1]["CatName"].ToString();
					AutoDTable.Rows[iRow2]["SubCategory"]=mEmployeeDS.Rows[iRow1]["SubCatName"].ToString();


				} else {
				}
			}
		}
		

	}



    }



   
}

