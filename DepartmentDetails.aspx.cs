﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;



public partial class DepartmentDetails : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    UserCreationClass objUsercreation = new UserCreationClass();
    DataTable dtdDisplay = new DataTable();
    string SessionSpay;
    string SessionEpay;
    static int ss;
    bool ErrFlag = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionSpay = Session["SessionSpay"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Department Details";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");

              
            }
            Load_Data();
        }
    }
    protected void BtnClear_Click(object Sender, EventArgs e)
    {
        txtDepartmentName.Text = "";
    }
    protected void btnSaveDept_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string Dept = DeptNameHide.Value;
        
        string Query = "";
        
        Query = "Select * from  ["+SessionSpay+"]..Department_Mst where DeptName='" + Dept + "' ";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            Query = "delete  from  ["+SessionSpay+"]..Department_Mst where DeptName='" + Dept + "' ";
            dt = objdata.RptEmployeeMultipleDetails(Query);

            Query = "delete  from  ["+SessionEpay+"]..MstDepartment where DepartmentNm='" + Dept + "' ";
            dt = objdata.RptEmployeeMultipleDetails(Query);
        }

        Query = "Insert into ["+SessionSpay+"]..Department_Mst(DeptName) Values('" + txtDepartmentName.Text + "') ";
        dt = objdata.RptEmployeeMultipleDetails(Query);


        Query = "Insert into ["+SessionEpay+"]..MstDepartment(DepartmentNm) Values('" + txtDepartmentName.Text + "') ";
        dt = objdata.RptEmployeeMultipleDetails(Query);



       
    }
    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select DeptName from ["+SessionSpay+"]..Department_Mst ";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;

        Repeater1.DataBind();
    }
    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        string Query = "";
        mp1.Show();

        Query = "select DeptName from ["+SessionSpay+"]..Department_Mst where DeptName='" + e.CommandName.ToString() + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtDepartmentName.Text = dt.Rows[0]["DeptName"].ToString();
            DeptNameHide.Value = dt.Rows[0]["DeptName"].ToString();
        }


    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        string Query = "";

        Query = "select DeptName from ["+SessionSpay+"]..Department_Mst where DeptName='" + e.CommandName.ToString() + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            Query = "delete  from  ["+SessionSpay+"]..Department_Mst where DeptName='" + e.CommandName.ToString() + "' ";
            dt = objdata.RptEmployeeMultipleDetails(Query);

            Query = "delete  from  ["+SessionEpay+"]..MstDepartment where DepartmentNm='" + e.CommandName.ToString() + "' ";
            dt = objdata.RptEmployeeMultipleDetails(Query);
        }
        Load_Data();
    }

    
}
