﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default7.aspx.cs" Inherits="Default7" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


 <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">New Employee</li></h4> 
                    </ol>
          </div>
  <div class="panel panel-primary">
            <div class="panel-heading clearfix">
                <h3 class="panel-title">New Employee</h3>
            </div>
            </div>
            
            
            <div class="panel-body">
               <div id="rootwizard">
                <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Department</a></li>
                        <li role="presentation"><a href="#tab2" data-toggle="tab"><i class="fa fa-building m-r-xs"></i>Designation</a></li>
                        
                    </ul>
                    
                    
                    <form id="wizardForm">
               
                            <div class="tab-content">
                            
                            <div class="tab-pane active fade in" id="tab1">
                                 
                                                  
                                                  <div class="col-md-12">
                                            <div class="row">
											<div class="form-group col-md-4">
                                               
                                                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                      <ContentTemplate>
   <asp:Button ID="BtnAdd" runat="server" Text="Add" class="btn btn-success"   data-toggle="modal" href="#myModal1" AutoPostBack="false" ></asp:Button>
     </ContentTemplate>
      </asp:UpdatePanel>
      
      <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                              <h4 class="modal-title">Department</h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                            <div class="col-lg-12">
                                             <div class="col-lg-6">
                                              <div class="form-group">
                                                  <asp:Label ID="Label1" runat="server" Text="Department"></asp:Label>     
                                                  <asp:TextBox ID="txtDept" runat="server"></asp:TextBox>
                                                                         
                                  </div>
                                 </div>                     
                                                                           
                                           </div></div></div>

								 

                                          
                                          
                                      </div>
                                  </div>
                              </div>
                                                    
					                          </div>
                                              
											</div>
										</div>
             </div>
             
             
             
             
             <div class="tab-pane fade" id="tab2">
             
             <div class="col-md-12">
                                            <div class="row">
											<div class="form-group col-md-4">
											
											<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                      <ContentTemplate>
   <asp:Button ID="BtnAdd2" runat="server" Text="Add" class="btn btn-success"   data-toggle="modal" href="#myModal2" AutoPostBack="false" ></asp:Button>
     </ContentTemplate>
      </asp:UpdatePanel>
      <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      
			<div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                              <h4 class="modal-title">Designation</h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                            <div class="col-lg-12">
                                             <div class="col-lg-6">
                                              <div class="form-group">
                                                  <asp:Label ID="Label2" runat="server" Text="Designation"></asp:Label>     
                                                  <asp:TextBox ID="txtDesgn" runat="server"></asp:TextBox>
                                                                         
                                  </div>
                                 </div>                     
                                                                           
                                           </div></div></div>

								 

                                          
                                          
                                      </div>
                                  </div>						
									
									
																		
											
                                              
                                                </div>
                                                </div>
             
             </div>
             
             
             
             </div>
             
               
               </div>
              </div>
               </form>
               </div>               
                
               </div> 




</asp:Content>

