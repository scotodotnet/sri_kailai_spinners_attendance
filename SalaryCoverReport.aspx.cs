﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class SalaryCoverReport : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string SSQL;
    DataTable mEmployeeDS = new DataTable();
    DataTable AutoDataTable = new DataTable();
    DataTable mDataTable = new DataTable();
    DataSet ds = new DataSet();
    
  
    string Date;
    string Date2;
    DataTable mLocalDS_INTAB = new DataTable();
	DataTable mLocalDS_OUTTAB = new DataTable();
	string Time_IN_Str = "";
	string Time_Out_Str = "";
	Int32 time_Check_dbl = 0;
	string Total_Time_get = "";

	DataTable Payroll_DS = new DataTable();
    DataTable DataCells=new DataTable();
    DataTable mDataSet = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Salary Cover Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            string SessionCcode = Session["Ccode"].ToString();
            string SessionLcode = Session["Lcode"].ToString();

            AutoDataTable.Columns.Add("SNo");
            AutoDataTable.Columns.Add("Dept");
            AutoDataTable.Columns.Add("Type");
            AutoDataTable.Columns.Add("Shift");
            AutoDataTable.Columns.Add("EmpCode");
            AutoDataTable.Columns.Add("Ex.Code");
            AutoDataTable.Columns.Add("Name");
            AutoDataTable.Columns.Add("TimeIN");
            AutoDataTable.Columns.Add("MachineID");
            AutoDataTable.Columns.Add("Category");
            AutoDataTable.Columns.Add("SubCategory");
            AutoDataTable.Columns.Add("CoverDate");



            DataCells.Columns.Add("CompanyName");
            DataCells.Columns.Add("LocationName");
            DataCells.Columns.Add("ShiftDate");
            DataCells.Columns.Add("SNo");
            DataCells.Columns.Add("Dept");
            DataCells.Columns.Add("Type");
            DataCells.Columns.Add("Shift");
            DataCells.Columns.Add("Category");
            DataCells.Columns.Add("SubCategory");
            DataCells.Columns.Add("EmpCode");
            DataCells.Columns.Add("ExCode");
            DataCells.Columns.Add("Name");
            DataCells.Columns.Add("TimeIN");
            DataCells.Columns.Add("MachineID");
            DataCells.Columns.Add("PrepBy");
            DataCells.Columns.Add("PrepDate");
            DataCells.Columns.Add("CoverDate");
            DataCells.Columns.Add("ReportDate");


            Date = Request.QueryString["FromDate"].ToString();
            Date2 = Request.QueryString["ToDate"].ToString();


            DataTable mLocalDS = new DataTable();
            int mStartINRow = 0;
            int mStartOUTRow = 0;
            string ShiftType = "";
            string ng = string.Format(Date, "dd-MM-yyyy");
            DateTime date1 = Convert.ToDateTime(ng);
            string nc = string.Format(Date2, "dd-MM-yyyy");
            DateTime date2 = Convert.ToDateTime(nc);

            for (int iTabRow = 0; iTabRow < 1; iTabRow++)
            {
                if (AutoDataTable.Rows.Count <= 1)
                {
                    mStartOUTRow = 1;
                }
                else
                {
                    mStartOUTRow = AutoDataTable.Rows.Count;
                }
                SSQL = "";
                SSQL = "Select LT.MachineID,Min(LT.Payout) as [TimeIN] from LogTime_SALARY LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID ";
                SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And LT.Payout >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01" + "'";
                SSQL = SSQL + " And LT.Payout <='" + date2.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59" + "'";
                SSQL = SSQL + " And EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes' Group By LT.MachineID";
                SSQL = SSQL + " Order By Min(LT.Payout)";

                mDataSet = objdata.ReturnMultipleValue(SSQL);

                if (mDataSet.Rows.Count > 0)
                {
                    string MachineID;

                    for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
                    {
                        bool chkduplicate = false;
                        chkduplicate = false;
                        //Check Duplicate Entry
                        for (int ia = 0; ia < AutoDataTable.Rows.Count - 1; ia++)
                        {
                            string id = mDataSet.Rows[iRow]["MachineID"].ToString();

                            if (id == AutoDataTable.Rows[ia][9].ToString())
                            {
                                chkduplicate = true;
                                break; // TODO: might not be correct. Was : Exit For
                            }
                        }

                        if (chkduplicate == false)
                        {
                            AutoDataTable.NewRow();
                            AutoDataTable.Rows.Add();

                            AutoDataTable.Rows[mStartINRow][3] = "Salary Cover";
                            AutoDataTable.Rows[mStartINRow][7] = mDataSet.Rows[iRow]["TimeIN"].ToString();
                            AutoDataTable.Rows[mStartINRow][11] = mDataSet.Rows[iRow]["TimeIN"].ToString();
                            MachineID = mDataSet.Rows[iRow]["MachineID"].ToString();
                            AutoDataTable.Rows[mStartINRow][9] = MachineID;
                            mStartINRow += 1;
                        }
                    }
                }
            }
        


    DataTable mEmployeeDS = new DataTable();

	SSQL = "";
	SSQL = "select Distinct isnull(EM.MachineID_Encrypt,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
	SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
	SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
	SSQL = SSQL + ",isnull(EM.CatName,'') as [CatName], isnull(EM.SubCatName,'') as [SubCatName]";
	SSQL = SSQL + ",DM.DeptCode from Employee_Mst EM inner Join Department_Mst DM on DM.DeptName=EM.DeptName Where EM.Compcode='" + SessionCcode + "'";
	SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'" + " order by DM.DeptCode Asc";

	mEmployeeDS = objdata.ReturnMultipleValue(SSQL); 

if (mEmployeeDS.Rows.Count > 0) 
{
		
		for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++) 
        {
			for (int iRow2 = 0; iRow2 <AutoDataTable.Rows.Count; iRow2++) 
            {
				if (AutoDataTable.Rows[iRow2][9].ToString() == mEmployeeDS.Rows[iRow1]["MachineID"].ToString())
                {

                    AutoDataTable.Rows[iRow2]["Dept"] = mEmployeeDS.Rows[iRow1]["DeptName"].ToString();
                    AutoDataTable.Rows[iRow2]["Type"] = mEmployeeDS.Rows[iRow1]["TypeName"].ToString();
                    AutoDataTable.Rows[iRow2]["EmpCode"] = mEmployeeDS.Rows[iRow1]["EmpNo"].ToString();
                    AutoDataTable.Rows[iRow2]["Ex.Code"] = mEmployeeDS.Rows[iRow1]["ExistingCode"].ToString();
                    AutoDataTable.Rows[iRow2]["Name"] = mEmployeeDS.Rows[iRow1]["FirstName"].ToString();
                    AutoDataTable.Rows[iRow2]["Category"] = mEmployeeDS.Rows[iRow1]["CatName"].ToString();
                    AutoDataTable.Rows[iRow2]["SubCategory"] = mEmployeeDS.Rows[iRow1]["SubCatName"].ToString();
				}
                else 
                {
					//Skip
				}
			}
		}
	}
        int i=0;
        for (int iRow = 0; iRow < AutoDataTable.Rows.Count; iRow++)
        {
            	DataCells.NewRow();
                DataCells.Rows.Add();

           DataCells.Rows[i]["CompanyName"] = SessionCcode;
           DataCells.Rows[i]["LocationName"] = SessionLcode;
           DataCells.Rows[i]["ShiftDate"]=Date;
           DataCells.Rows[i]["Dept"] = AutoDataTable.Rows[iRow]["Dept"];
           DataCells.Rows[i]["Type"] = AutoDataTable.Rows[iRow]["Type"];
           DataCells.Rows[i]["Shift"] = AutoDataTable.Rows[iRow]["Shift"];
           DataCells.Rows[i]["Category"] = AutoDataTable.Rows[iRow]["Category"];
           DataCells.Rows[i]["SubCategory"] = AutoDataTable.Rows[iRow]["SubCategory"];
           DataCells.Rows[i]["EmpCode"] = AutoDataTable.Rows[iRow]["EmpCode"];
           DataCells.Rows[i]["ExCode"] = AutoDataTable.Rows[iRow]["Ex.Code"];
           DataCells.Rows[i]["Name"] = AutoDataTable.Rows[iRow]["Name"];
           string InTime_str = AutoDataTable.Rows[iRow][7].ToString();
           DateTime InTime_DT = Convert.ToDateTime(InTime_str);
           DataCells.Rows[i]["TimeIN"] = String.Format("{0:hh:mm tt}", InTime_DT);
           DataCells.Rows[i]["MachineID"] = AutoDataTable.Rows[iRow]["MachineID"].ToString();
           DataCells.Rows[i]["PrepBy"] = "User";
           DataCells.Rows[i]["PrepDate"] = Date;
           DataCells.Rows[i]["ReportDate"] = "SALARY COVER REPORT FROM : " + Date + " TO : " + Date2 + "" ;
           string CoverDate_Str = AutoDataTable.Rows[iRow][11].ToString();
           DateTime CoverDate_new = Convert.ToDateTime(CoverDate_Str);
           DataCells.Rows[i]["CoverDate"] = CoverDate_new.ToString("dd/MM/yyyy");
          i=i+1;
        }
   
            ds.Tables.Add(DataCells);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Salary_Cover_Report.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            CrystalReportViewer1.ReportSource = report;


        }
    }
}



