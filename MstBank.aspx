﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstBank.aspx.cs" Inherits="MstBank" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>



<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>


<script src='<%= ResolveUrl("assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

    <script>
        $(document).ready(function() {
            $('#tableCustomer').dataTable();
        });
    </script>

      

                               <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate>
                                        
            <div class="page-breadcrumb">
                            
                    <ol class="breadcrumb container">
                       <h4><li class="active">Bank Details</li>
                           <h4>
                           </h4>
                        </h4> 
                    </ol>
                </div>

            <div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
    <div class="page-inner">
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Bank</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
			
                           
				    	<div class="form-group row">
					      <label for="input-Default" class="col-sm-2 control-label">Bank Code</label>
							<div class="col-sm-4">
                                <asp:TextBox ID="txtBankCode" runat="server" class="form-control" required></asp:TextBox>
							</div>
					
					    
					      <label for="input-Default" class="col-sm-2 control-label">Bank Name</label>
						   <div class="col-sm-4">
                              <asp:TextBox ID="txtBankName" class="form-control" runat="server" required></asp:TextBox>
                          </div>
                       </div>
                       
                        <div class="form-group row">
							<label for="input-Default" class="col-sm-2 control-label">Bank IFCode</label>
							<div class="col-sm-4">
                                 <asp:TextBox ID="txtIFC" class="form-control" runat="server"  required></asp:TextBox>
							</div>
						
				        
					    
				       </div>
					    
					      
                         
                        
					<!-- Button start -->
						<div class="form-group row">
						</div>
                         <div class="txtcenter">
                               <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                   onclick="btnSave_Click"  />
                                   
                               <asp:LinkButton ID="BtnClear" runat="server" class="btn btn-danger" 
                                   onclick="BtnClear_Click">Clear</asp:LinkButton>   
                         </div>
                    <!-- Button End -->
					
					       <div class="form-group row">
						  </div>


                 

                        <%-- <div class="panel-body">--%>
                             <div class="table-responsive">
                               <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
		                                        <HeaderTemplate>
                                                    <table id="example" class="display table">
                                                        <thead>
                                                            <tr>
                                                                
                                                                <th>Bank Code</th>
                                                                <th>Bank Name</th>
                                                                <th>IFC Code</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        
                                                        <td><%# Eval("BankCode")%></td>
                                                        <td><%# Eval("BankName")%></td>
                                                        <td><%# Eval("IFCcode")%></td>
                                                       
                                                        <td>
                                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("BankCode")%>'>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("BankCode")%>' 
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this new user details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
		                                    </asp:Repeater>
                              
                            </div>
                        <%--</div>--%>
                       
						  
						 </div>
 
	             </form>
			
			
			
                                                        
			
			
			
			
			
			
			
			
			</div><!-- panel white end -->
		    </div>
		    <div class="col-md-2"></div>
		    
            
      
        <!-- Dashboard start -->
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- Dashboard End -->   
                        
  </div>
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
  
 </div>
                                       
                                       </ContentTemplate>
                                </asp:UpdatePanel>
</asp:Content>

