﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class new_Employee_ADD : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    UserCreationClass objUsercreation = new UserCreationClass();
    DataTable dtdDisplay = new DataTable();
    string SSQL;
    string MachineID;
    string SessionSpay;
    string SessionEpay;
    string SessionUserType;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionSpay = Session["SessionSpay"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["UserType"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Employee Details";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");

                CreateEmployeeDisplay();


            }
        }
    }

    protected void btnNEWEMPentry_Click(object sender, EventArgs e)
    {
        string id = "";
        Session["MachineID"] = id;
        Response.Redirect("new_employee.aspx");
        CreateEmployeeDisplay();
    }
    public void CreateEmployeeDisplay()
    {
        DataTable dtDisplay = new DataTable();
        string Query = "select MachineID,FirstName,CatName,Gender,DeptName,Designation,CONVERT(varchar(10),BirthDate,103) as DOB from Employee_Mst where ";
        
        //if (SessionUserType=="2")
        //{
        //    Query = Query + " IsNonAdmin='1'and ";
        //}
        Query = Query + "CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' order by MachineID asc"; 

        dtDisplay = objdata.RptEmployeeMultipleDetails(Query);

        //dtdDisplay = objdata.EmployeeDisplay(SessionCcode,SessionLcode);
        if (dtDisplay.Rows.Count > 0)
        {
            rptrEmployee.DataSource = dtDisplay;
            rptrEmployee.DataBind();
        }
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(id);
                break;
            case ("Edit"):
                EditRepeaterData(id);
                break;
        }
    }

    private void DeleteRepeaterData(string id)
    {
        DataTable Dtd = new DataTable();
        SSQL = "update Employee_Mst set IsActive='No' where MachineID = '" + id + "'";
        Dtd = objdata.ReturnMultipleValue(SSQL);

        SSQL = "Update ["+SessionEpay+"]..EmployeeDetails set ActivateMode='N' where BiometricID='" + id + "'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
    }

    private void EditRepeaterData(string id)
    {
        Session["MachineID"] = id;
        Response.Redirect("new_employee.aspx");
        //ResponseHelper.Redirect("new_employee.aspx?MachineID=" + id, "_blank", ""); 
    }

  
}
