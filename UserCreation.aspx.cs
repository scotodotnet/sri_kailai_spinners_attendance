﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;


public partial class UserCreation : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    UserCreationClass objUsercreation = new UserCreationClass();
    DataTable dtdDisplay = new DataTable();

    static int ss;
 
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "ERP Stores Module :: Spay Module | User Creation";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");
                
                CreateUserDisplay();
                //Dropdown_Company();
                //Dropdown_Location();
            }
        }
    }

    //public void Dropdown_Company()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.Dropdown_Company();
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        ddlCompany.Items.Add(dt.Rows[i]["Cname"].ToString());
    //    }
    //}

    //public void Dropdown_Location()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.dropdown_ParticularLocation(SessionCcode, SessionLcode);
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        ddlLocation.Items.Add(dt.Rows[i]["LocName"].ToString());
    //    }
    //}



    public void CreateUserDisplay()
    {
        dtdDisplay = objdata.UserCreationDisplay(SessionCcode, SessionLcode);
        if (dtdDisplay.Rows.Count > 0)
        {
            rptrCustomer.DataSource = dtdDisplay;
            rptrCustomer.DataBind();
        }

    }



    protected void btnSave_Click(object sender, EventArgs e)
    {
        //bool ErrFlag = false;
        //if (ddlUserType.SelectedItem.Text == "- select -")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select UserType');", true);
        //    ErrFlag = true;
        //}
        //else
        //{
        //    DataTable dtd = new DataTable();
        //    dtd = objdata.CheckUser_Login(SessionCcode, SessionLcode, txtUserName.Text, ss);
        //    if (dtd.Rows.Count > 0)
        //    {
        //        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Already Exist');", true);
        //        //ErrFlag = true;
        //        DataTable Dtdedit = new DataTable();
        //        objUsercreation.Ccode = SessionCcode.ToString();
        //        objUsercreation.Lcode = SessionLcode.ToString();
        //        objUsercreation.UserCode = ddlUserType.SelectedItem.Text;
        //        objUsercreation.UserName = txtUserName.Text;
        //        objUsercreation.Password = s_hex_md5(txtPassword.Text.Trim());
        //        string pwd = s_hex_md5(txtPassword.Text);
        //        objUsercreation.UserID = ss;
        //        Dtdedit = objdata.EditUserCreation(objUsercreation);
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Update sucessFully');", true);
        //        CreateUserDisplay();
        //    }
        //    else
        //    {
        //        objUsercreation.Ccode = SessionCcode.ToString();
        //        objUsercreation.Lcode = SessionLcode.ToString();
        //        objUsercreation.UserCode = ddlUserType.SelectedItem.Text;
        //        objUsercreation.UserName = txtUserName.Text;
        //        objUsercreation.Password = s_hex_md5(txtPassword.Text.Trim());
        //        string pwd = s_hex_md5(txtPassword.Text);
        //        objdata.UserCreationRegistration(objUsercreation);
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Save sucessFully');", true);
        //        clear();
        //        CreateUserDisplay();
        //    }
        //}
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(id);
                break;
            case ("Edit"):
                EditRepeaterData(id);
                break;
        }
    }

    private void EditRepeaterData(string id)
    {
    //    ss = Convert.ToInt16(id);
    //    DataTable Dtd = new DataTable();
    //    Dtd = objdata.EditUserCreation(ss);
    //    if (Dtd.Rows.Count > 0)
    //    {
    //        String Utype = Dtd.Rows[0]["UserType"].ToString();
    //        if (Utype == "Admin")
    //        {
    //            ddlUserType.SelectedIndex = 1;
    //        }
    //        else if (Utype == "Non-Admin")
    //        {
    //            ddlUserType.SelectedIndex = 2;
    //        }
    //        txtUserName.Text = Dtd.Rows[0]["UserName"].ToString();
    //        txtPassword.Text = Dtd.Rows[0]["Password"].ToString();

    //        CreateUserDisplay();
    //    }
    }




    private void DeleteRepeaterData(string id)
    {
        //int ss = Convert.ToInt16(id);
        //DataTable Dtd = new DataTable();
        //Dtd = objdata.DeleteUserCreation(ss);
        //CreateUserDisplay();
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
        //string str = "delete from students where id=" + id;
        //try
        //{
        //    cmd = new SqlCommand(str, con);
        //    con.Open();
        //    cmd.ExecuteNonQuery();
        //    lblresult.Text = "Record Deleted Successfully";
        //    lblresult.ForeColor = System.Drawing.Color.Green;
        //}
        //catch (Exception ex)
        //{
        //    lblresult.Text = ex.Message.ToString();
        //    lblresult.ForeColor = System.Drawing.Color.Red;
        //}
        //finally
        //{
        //    Bind();
        //    con.Close();
        //}

    }


    protected void LinkButton1_Command(object sender, CommandEventArgs e)
    {
    //    if (e.CommandName == "Delete")
    //    {
    //        string id = Convert.ToString(e.CommandArgument);
    //        int id1 = Convert.ToInt16(e.CommandArgument);
    //        DataTable Dtd = new DataTable();
    //        Dtd = objdata.DeleteUserCreation(id1);
    //        //e.CommandArgument --> contain the erid value
    //        //Do something
    //    }
    }


    //public static String s_hex_md5(String originalPassword)
    //{
    //    UTF8Encoding encoder = new UTF8Encoding();
    //    MD5 md5 = new MD5CryptoServiceProvider();

    //    Byte[] hashedbytes = md5.ComputeHash(encoder.GetBytes(originalPassword));
    //    return BitConverter.ToString(hashedbytes).Replace("-", "").ToLower();
    //}


    protected void BtnClear_Click(object sender, EventArgs e)
    {
        //clear();
    }

    //public void clear()
    //{
    //    ddlUserType.SelectedIndex = 1;
    //    txtUserName.Text = "";
    //    txtPassword.Text = "";
    //}

}
