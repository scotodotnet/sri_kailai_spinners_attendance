﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class DayAttendanceShiftWise : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
   
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string SSQL;
    DataTable AutoDataTable = new DataTable();
    DataTable Datacells = new DataTable();
    string ModeType;
    string ShiftType;
    string Date;
    DateTime date1;
    string ShiftType1;
    string ddlShiftType;
    string SessionCcode;
    string SessionLcode;
    DataTable mLocalDS = new DataTable();
    string SessionUserType;


    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();


    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Shift Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionUserType = Session["UserType"].ToString();

            Datacells.Columns.Add("EmployeeNo");
            Datacells.Columns.Add("ExistingCode");
            Datacells.Columns.Add("Dept");
            Datacells.Columns.Add("Name");
            //Datacells.Columns.Add("Type");
            Datacells.Columns.Add("Shift");
            Datacells.Columns.Add("TimeIN");
            Datacells.Columns.Add("TimeOUT");
            //Datacells.Columns.Add("Category");
            //Datacells.Columns.Add("SubCategory");

            AutoDataTable.Columns.Add("Dept");
            AutoDataTable.Columns.Add("Type");
            AutoDataTable.Columns.Add("Shift");
            AutoDataTable.Columns.Add("EmpCode");
            AutoDataTable.Columns.Add("ExCode");
            AutoDataTable.Columns.Add("Name");
            AutoDataTable.Columns.Add("MachineID");
            AutoDataTable.Columns.Add("TimeIN");
            AutoDataTable.Columns.Add("TimeOUT");
            AutoDataTable.Columns.Add("Category");
            AutoDataTable.Columns.Add("SubCategory");


            //ModeType = Request.QueryString["ModeType"].ToString();

            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            //ddlShiftType = Request.QueryString["ddlShiftType"].ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();

            date1 = Convert.ToDateTime(Date.ToString());

            if (ShiftType1 == "SHIFT1" || ShiftType1 == "SHIFT2" || ShiftType1 == "SHIFT3" || ShiftType1 == "GENERAL")
            {
                SSQL = "select StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
                SSQL = SSQL + "EndIN_Days,StartOUT_Days,EndOUT_Days,shiftDesc from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + "And shiftDesc='" + ShiftType1 + "'";
                mLocalDS = objdata.ReturnMultipleValue(SSQL);

            }



            if (mLocalDS.Rows.Count < 0)
                return;

            string StartIN = mLocalDS.Rows[0]["StartIN"].ToString();
            string EndIN = mLocalDS.Rows[0]["EndIN"].ToString();
            string StartIN_Days = mLocalDS.Rows[0]["StartIN_Days"].ToString();
            string EndIN_Days = mLocalDS.Rows[0]["EndIN_Days"].ToString();
            string StartOUT = mLocalDS.Rows[0]["StartOUT"].ToString();
            string EndOUT = mLocalDS.Rows[0]["EndOUT"].ToString();
            string StartOUT_Days = mLocalDS.Rows[0]["StartOUT_Days"].ToString();
            string EndOUT_Days = mLocalDS.Rows[0]["EndOUT_Days"].ToString();
            string shiftDesc = mLocalDS.Rows[0]["shiftDesc"].ToString();

            Fill_Shift_IN_Details(StartIN, EndIN, StartIN_Days, EndIN_Days, shiftDesc);
            Fill_Shift_OUT_Details(StartOUT, EndOUT, StartOUT_Days, EndOUT_Days, shiftDesc);

            DataTable mEmployeeDS = new DataTable();

            for (int iTabRow = 0; iTabRow < AutoDataTable.Rows.Count; iTabRow++)
            {
                string MID = AutoDataTable.Rows[iTabRow]["MachineID"].ToString();
                SSQL = "";
                SSQL = "select Distinct MachineID,MachineID_Encrypt,isnull(DeptName,'') as [DeptName]";
                SSQL = SSQL + ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
                SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
               
                SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName]";
               
                SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode + "'";
                //if (SessionUserType == "2")
                //{
                //    SSQL = SSQL + " And IsNonAdmin='1'";
                //}
              

                SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes' And MachineID_Encrypt = '" + MID + "'";

                mEmployeeDS = objdata.ReturnMultipleValue(SSQL);
                if (mEmployeeDS.Rows.Count > 0)
                {
                    for (int iRow3 = 0; iRow3 < mEmployeeDS.Rows.Count; iRow3++)
                    {

                        string MID1 = mEmployeeDS.Rows[iRow3]["MachineID"].ToString();

                        AutoDataTable.Rows[iTabRow][0] = mEmployeeDS.Rows[iRow3]["DeptName"];
                        AutoDataTable.Rows[iTabRow][1] = mEmployeeDS.Rows[iRow3]["TypeName"];
                        AutoDataTable.Rows[iTabRow][3] = mEmployeeDS.Rows[iRow3]["EmpNo"];
                        AutoDataTable.Rows[iTabRow][4] = mEmployeeDS.Rows[iRow3]["ExistingCode"];
                        AutoDataTable.Rows[iTabRow][5] = mEmployeeDS.Rows[iRow3]["FirstName"];
                        AutoDataTable.Rows[iTabRow][9] = mEmployeeDS.Rows[iRow3]["CatName"];
                        AutoDataTable.Rows[iTabRow][10] = mEmployeeDS.Rows[iRow3]["SubCatName"];


                        Datacells.Rows[iTabRow][0] = AutoDataTable.Rows[iTabRow][3].ToString();
                        Datacells.Rows[iTabRow][1] = AutoDataTable.Rows[iTabRow][4].ToString();
                        Datacells.Rows[iTabRow][2] = AutoDataTable.Rows[iTabRow][0].ToString();
                        Datacells.Rows[iTabRow][3] = AutoDataTable.Rows[iTabRow][5].ToString();
                        Datacells.Rows[iTabRow][4] = AutoDataTable.Rows[iTabRow][2].ToString();
                        //Datacells.Rows[iTabRow][8] = AutoDataTable.Rows[iTabRow][9].ToString();
                        //Datacells.Rows[iTabRow][9] = AutoDataTable.Rows[iTabRow][10].ToString();

                    }
                }
            }


            grid.DataSource = Datacells;
            grid.DataBind();
            string attachment = "attachment;filename=DAYATTSHIFTWISE.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">Day Attendance ShiftWise Report &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\"> Date: -" + Date + "</a>");
            Response.Write("&nbsp;&nbsp;&nbsp;");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();


        }
      

    }

    public void Fill_Shift_IN_Details(string StartIN, string EndIN, string StartIN_Days, string EndIN_Days, string shiftDesc)
    {

        string ShiftType_str;
        string shiftDesc_str = shiftDesc;
        int StartIN_Days1 = Convert.ToInt32(StartIN_Days);
        int EndIN_Days1 = Convert.ToInt32(EndIN_Days);
        string StartIN_str = StartIN;
        string EndIN_str = EndIN;

        if (shiftDesc_str == "GENERAL")
        {
            ShiftType_str = "GENERAL";
        }
        else
        {
            ShiftType_str = "SHIFT";
        }

        if (ShiftType_str == "GENERAL")
        {
            SSQL="Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID Where LT.Compcode='" + SessionCcode + "'";
            SSQL= SSQL + " And LT.LocCode='" + SessionLcode + "'";
            SSQL=SSQL + " And LT.TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
            //if (SessionUserType == "2")
            //{
            //    SSQL = SSQL + " And EM.IsNonAdmin='1'";
            //}
            SSQL = SSQL + " And LT.TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
            
            
            SSQL = SSQL + "And EM.ShiftType='" + ShiftType_str + "' And EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes' Group By LT.MachineID";
           
            SSQL = SSQL + " Order By Min(LT.TimeIN)";


        }
        else
        {
            SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID Where LT.Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And LT.TimeIN >='" + date1.AddDays(StartIN_Days1).ToString("yyyy/MM/dd") + " " + StartIN_str + "'";
            //if (SessionUserType == "2")
            //{
            //    SSQL = SSQL + " And EM.IsNonAdmin='1'";
            //}
            SSQL = SSQL + " And LT.TimeIN <='" + date1.AddDays(EndIN_Days1).ToString("yyyy/MM/dd") + " " + EndIN_str + "'";
            
            SSQL = SSQL + " And EM.ShiftType='" + ShiftType_str + "' And EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes' Group By LT.MachineID";
            
            
            SSQL = SSQL + " Order By Min(LT.TimeIN)";
        }

        DataTable mLocalIN = new DataTable();
        mLocalIN = objdata.ReturnMultipleValue(SSQL);
        int jRow=0;
        if(mLocalIN.Rows.Count <= 0)
        {
            return;
        }
        DataTable mEmployeeIN=new DataTable();
        for(int iRow=0;iRow<mLocalIN.Rows.Count;iRow++)
        {
            if(ShiftType_str=="GENERAL")
            {
                SSQL="Select * from Employee_Mst where MachineID_Encrypt='" + mLocalIN.Rows[iRow]["MachineID"].ToString() + "' And IsActive='Yes'";
                
                
                mEmployeeIN=objdata.ReturnMultipleValue(SSQL);

                if(mEmployeeIN.Rows.Count<=0)
                {
                    return;
                }
                else
                {
                    if(mEmployeeIN.Rows[0]["ShiftType"].ToString()=="GENERAL")
                    {
                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();

                        Datacells.NewRow();
                        Datacells.Rows.Add();

                        AutoDataTable.Rows[jRow]["Shift"] = shiftDesc_str;
                        AutoDataTable.Rows[jRow]["TimeIN"] = String.Format("{0:hh:mm tt}", (mLocalIN.Rows[iRow]["TimeIN"]));
                        AutoDataTable.Rows[jRow]["MachineID"] = mLocalIN.Rows[iRow]["MachineID"];
                        Datacells.Rows[jRow]["Shift"] = AutoDataTable.Rows[jRow]["Shift"].ToString();
                        Datacells.Rows[jRow]["TimeIN"] = AutoDataTable.Rows[jRow]["TimeIN"].ToString();
                        jRow = jRow + 1;
                    }
                }
            }
            else
            {
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();

                Datacells.NewRow();
                Datacells.Rows.Add();

                AutoDataTable.Rows[iRow]["Shift"] = shiftDesc_str;
                AutoDataTable.Rows[iRow]["TimeIN"] = String.Format("{0:hh:mm tt}", (mLocalIN.Rows[iRow]["TimeIN"]));
                AutoDataTable.Rows[iRow]["MachineID"] = mLocalIN.Rows[iRow]["MachineID"];
                Datacells.Rows[iRow]["Shift"] = AutoDataTable.Rows[iRow]["Shift"].ToString();
                Datacells.Rows[iRow]["TimeIN"] = AutoDataTable.Rows[iRow]["TimeIN"].ToString();
            }
        }
        
    }

    public void Fill_Shift_OUT_Details(string StartOUT, string EndOUT, string StartOUT_Days, string EndOUT_Days, string shiftDesc)
    {

       
        string shiftDesc_str = shiftDesc;
        int StartOUT_Days1 = Convert.ToInt32(StartOUT_Days);
        int EndOUT_Days1 = Convert.ToInt32(EndOUT_Days);
        string StartOUT_str = StartOUT;
        string EndOUT_str = EndOUT;

        DataTable mLocalOUT=new DataTable();

        SSQL="Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT Where Compcode='" + SessionCcode + "'";
        SSQL=SSQL+" And LocCode='" + SessionLcode + "'";
        SSQL = SSQL+ " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
        SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
        SSQL = SSQL + " Group By MachineID";
        SSQL = SSQL + " Order By Max(TimeOUT)";

        mLocalOUT=objdata.ReturnMultipleValue(SSQL);

        for(int iRow1=0;iRow1<mLocalOUT.Rows.Count;iRow1++)
        {
            string mOUT_ID=mLocalOUT.Rows[iRow1]["MachineID"].ToString();

            for(int iRow2=0;iRow2<AutoDataTable.Rows.Count;iRow2++)
            {
                string AutoID = AutoDataTable.Rows[iRow2]["MachineID"].ToString();
                if (mOUT_ID == AutoID)
                {
                    AutoDataTable.Rows[iRow2]["TimeOUT"]=String.Format("{0:hh:mm tt}", (mLocalOUT.Rows[iRow1]["TimeOUT"]));
                    Datacells.Rows[iRow2]["TimeOUT"] = AutoDataTable.Rows[iRow2]["TimeOUT"].ToString();
                }
            }
        }
    }
 



}
