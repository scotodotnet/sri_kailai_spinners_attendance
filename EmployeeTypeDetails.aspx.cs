﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;



public partial class EmployeeTypeDetails : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    UserCreationClass objUsercreation = new UserCreationClass();
    DataTable dtdDisplay = new DataTable();

    static int ss;
    bool ErrFlag = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString(); 
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Employee Type Details";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");
                CreateEmployeeType();
            }
        }

    }

    public void CreateEmployeeType()
    {
        DataTable dtEmployeeTypeDisplay = new DataTable();
        dtEmployeeTypeDisplay = objdata.EmployeeTypeDisplay();

        if (dtEmployeeTypeDisplay.Rows.Count > 0)
        {
            rptrEmplyeeType.DataSource = dtEmployeeTypeDisplay;
            rptrEmplyeeType.DataBind();
        }


    }



    protected void btnSaveEmployeeType_Click(object sender, EventArgs e)
    {
        if (txtEmployeeType.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Emplyee Type');", true);
            ErrFlag = true;
        }
        else
        {


            DataTable dt = new DataTable();
            DataTable dtt = new DataTable();
            if (btnSaveEmployeeType.Text == "Update")
            {
                dt = objdata.EmployeeTypeUpdate(txtEmployeeType.Text, ss);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Sussessfully');", true);
            }
            else
            {
                dtt = objdata.CheckEmployeeType_AlreadyExist(txtEmployeeType.Text);
                if (dtt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Type Already Exist');", true);
                    //txtDesginationName.Text = "";

                    txtEmployeeType.Text = "";
                }

                else
                {
                    dt = objdata.EmployeeTypeRegister(txtEmployeeType.Text);
                    txtEmployeeType.Text = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Save Successfully');", true);
                }
            }
            CreateEmployeeType();
            btnSaveEmployeeType.Text = "Save";
            ClearEmployeeType();
        }
    }
    protected void BtnClearEmployeeType_Click(object sender, EventArgs e)
    {
        ClearEmployeeType();
        btnSaveEmployeeType.Text = "Save";

    }

    public void ClearEmployeeType()
    {
        txtEmployeeType.Text = "";
    }

    protected void rptrEmplyeeType_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterDataEmployeeType(id);
                break;
            case ("Edit"):
                MPE2.Show();
                EditRepeaterDataEmployeeType(id);
                break;
        }
    }

    private void EditRepeaterDataEmployeeType(string TypeID)
    {
        ss = Convert.ToInt16(TypeID);
        DataTable Dtd = new DataTable();
        Dtd = objdata.EditEmployeeType(ss);
        if (Dtd.Rows.Count > 0)
        {
            txtEmployeeType.Text = Dtd.Rows[0]["TypeName"].ToString();
        }
        btnSaveEmployeeType.Text = "Update";
    }

    private void DeleteRepeaterDataEmployeeType(string TypeID)
    {
        int ss = Convert.ToInt16(TypeID);

        DataTable DtdCheck = new DataTable();
        DtdCheck = objdata.EditEmployeeType(ss);
        if (DtdCheck.Rows.Count > 0)
        {
            string TypeName = DtdCheck.Rows[0]["TypeName"].ToString();
            DataTable CheckInEmployee = new DataTable();
            CheckInEmployee = objdata.CheckingEmployeeType(TypeName);
            if (CheckInEmployee.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' Employee Type Allocated to Employee');", true);
            }
            else
            {
                DataTable Dtd = new DataTable();
                Dtd = objdata.DeleteEmployeeeType(ss);
                CreateEmployeeType();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
            }
        }
        CreateEmployeeType();

    }



}
