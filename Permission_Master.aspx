﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Permission_Master.aspx.cs" Inherits="Permission_Master" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tableCustomer').dataTable();               
            }
        }); 
    };
    </script>
      
             
     <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableCustomer').dataTable();
        });
    </script>


<asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate>
                                        
            <div class="page-breadcrumb">
                            
                    <ol class="breadcrumb container">
                       <h4><li class="active">Permission Detail Master</li>
                           <h4>
                           </h4>
                           <h4>
                           </h4>
                        </h4> 
                    </ol>
                </div>

            <div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
    <div class="page-inner">
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Permission Master</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
				    	<%--<div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Company<span class="mandatory">*</span></label>
						<div class="col-sm-4">
                            <asp:DropDownList ID="ddlCompanyCode" runat="server" class="form-control">
                            </asp:DropDownList>
						</div>
						
						<label for="input-Default" class="col-sm-2 control-label">Location<span class="mandatory">*</span></label>
							<div class="col-sm-4">
                               <asp:DropDownList ID="ddlLocationcode" runat="server" class="form-control">
                               </asp:DropDownList>
							</div>
                   </div>--%> 
                           
				    	<%--<div class="form-group row">
					      <label for="input-Default" class="col-sm-2 control-label">Emp No</label>
							<div class="col-sm-4">
                                 <asp:DropDownList ID="ddlTicketNo" runat="server" class="form-control" AutoPostBack="true"
                                         onselectedindexchanged="ddlTicketNo_SelectedIndexChanged">
                                 </asp:DropDownList>  
							</div>
					
					    
					      <label for="input-Default" class="col-sm-2 control-label">Emp Name</label>
						   <div class="col-sm-4">
                              <asp:TextBox ID="txtEmpName" class="form-control" runat="server" required></asp:TextBox>
                          </div>
                       </div>--%>
                       
                        <div class="form-group row">
							<label for="input-Default" class="col-sm-2 control-label">Department<span class="mandatory">*</span></label>
							<div class="col-sm-4">
                                 <asp:DropDownList ID="ddlDepartment" runat="server" class="form-control col-md-6">
                                 </asp:DropDownList>
							</div>
						
				          <%--<div class="col-sm-1"></div>  --%>
			                    
				        
						 <label for="input-Default" class="col-sm-2 control-label">Permission Type<span class="mandatory">*</span></label>
						   <div class="col-sm-4">
                              <asp:TextBox ID="txtPermissionType" class="form-control" runat="server" required></asp:TextBox>
                            
						  </div>
							
							</div>
							
							
					    <div class="form-group row">
                                      <label for="input-Default" class="col-sm-2 control-label">Month<span class="mandatory">*</span></label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlMonth" runat="server" class="form-control">
                                                <asp:ListItem Value="0"> Select </asp:ListItem>     
                                                 <asp:ListItem Value="1">January</asp:ListItem>
                                                 <asp:ListItem Value="2">February</asp:ListItem>
                                                 <asp:ListItem Value="3">March</asp:ListItem>
                                                 <asp:ListItem Value="4">April</asp:ListItem>
                                                 <asp:ListItem Value="5">May</asp:ListItem>
                                                 <asp:ListItem Value="6">June</asp:ListItem>
                                                 <asp:ListItem Value="7">July</asp:ListItem>
                                                 <asp:ListItem Value="8">August</asp:ListItem>
                                                 <asp:ListItem Value="9">September</asp:ListItem>
                                                 <asp:ListItem Value="10">October</asp:ListItem>  
                                                 <asp:ListItem Value="11">November</asp:ListItem>
                                                 <asp:ListItem Value="12">December</asp:ListItem>    
                                               </asp:DropDownList>
                                            
                                          
                                        </div>
						     <label for="input-Default" class="col-sm-2 control-label">Number of Permission<span class="mandatory">*</span></label>
							     <div class="col-sm-4">
                                  <asp:TextBox ID="txtCountofMonth" class="form-control" runat="server"></asp:TextBox>
						 	</div>
				     	                    
				            
							
							</div>
							
							
							
					     <div class="form-group row">
					          <label for="input-Default" class="col-sm-2 control-label">Financial Year<span class="mandatory">*</span></label>
							     <div class="col-sm-4">
                                  <asp:TextBox ID="txtFinancialYear" class="form-control" runat="server" ReadOnly="true"></asp:TextBox>
						 	  </div>
					         
							 <label for="input-Default" class="col-sm-2 control-label">Hours<span class="mandatory">*</span></label>
						    	<div class="col-sm-2">
                                   <asp:TextBox ID="txtdays" class="form-control" runat="server" required></asp:TextBox>
                               </div>
                                <div class="col-sm-2">
                                   <asp:RadioButton ID="rdbMorning" runat="server" Text ="AM"  
                                        AutoPostBack="true" oncheckedchanged="rdbMorning_CheckedChanged"/>
                                   <asp:RadioButton ID="rdbEvening" runat="server" Text ="PM"  
                                        AutoPostBack="true" oncheckedchanged="rdbEvening_CheckedChanged"/> 
                                        <span class="mandatory">*</span>
                               </div>
                        
				       </div>
	
					<!-- Button start -->
						<div class="form-group row">
						</div>
                             <div class="txtcenter">
                               <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                   onclick="btnSave_Click"/> 
                                    
                               <asp:LinkButton ID="BtnClear" runat="server" class="btn btn-danger" 
                                   onclick="BtnClear_Click">Clear</asp:LinkButton>   
                         </div>
                    <!-- Button End -->
					
					       <div class="form-group row">
						  </div>
						  
						  
						  <div class="table-responsive">
    
                              <asp:Repeater ID="rptrCustomer" runat="server" onitemcommand="Repeater1_ItemCommand">
            <HeaderTemplate>
                
                  <table id="tableCustomer" class="display table" style="width: 100%;">
                    <thead>
                        <tr>
                                                  
                                                    <th>Dept</th>
                                                    <th>PermissionType</th>
                                                    <th>Permission Granted</th> 
                                                    <th>Month</th> 
                                                    <th>Year</th>
                                                    <th>Edit</th>  
                                                    <th>Delete</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
        
                 
                    
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "CatName")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "PermissionType")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "NoofCount")%>
                    </td>
                     <td>
                        <%# DataBinder.Eval(Container.DataItem, "Month")%>
                    </td>
                     <td>
                        <%# DataBinder.Eval(Container.DataItem, "FinancialYear")%>
                    </td>
                    <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("CatName")+","+ Eval("SNo") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                     </td>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("CatName")+","+ Eval("SNo") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
						  
						  
						  
						  
						  
				     </div>
 
	             </form>
				
			</div><!-- panel white end -->
		    </div>
		    <div class="col-md-2"></div>
	
                       <!-- Dashboard start -->
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- Dashboard End -->   
                        
  </div>
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
  
 </div>
                                       
                                       </ContentTemplate>
                                </asp:UpdatePanel>
</asp:Content>

