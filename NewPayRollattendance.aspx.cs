﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;

public partial class NewPayRollattendance : System.Web.UI.Page
{
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    DataTable mLocalDS = new DataTable();
    string SSQL;
    string  Wages_Type;
    string Date1_str;
    string Date2_str;
    DataTable Log_DS = new DataTable();
    DateTime Date1;
    DateTime Date2;
    DataTable mDataTable = new DataTable();

                decimal NFH_Days_Count = 0;
                decimal NFH_Days_Present_Count = 0;
                decimal WH_Count = 0;
                decimal WH_Present_Count = 0;
                decimal Present_Days_Count; 
                Int32 Fixed_Work_Days = 0;
                decimal Spinning_Incentive_Days = 0;


                //NFH New Rule 29/10/2015
                decimal NFH_Double_Wages_Checklist = 0;
                decimal NFH_Double_Wages_Statutory = 0;
                decimal NFH_Double_Wages_Manual = 0;
                decimal NFH_WH_Days_Mins = 0;
                decimal NFH_Single_Wages = 0;

                DataTable NFH_Type_Ds = new DataTable();
                string NFH_Type_Str = "";
                string NFH_Dbl_Wages_Statutory_Check = "";

                string Emp_WH_Day = "";
                string DOJ_Date_Str = "";
               

                //NFH Check Start
                DataTable NFH_DS = new DataTable();
                System.DateTime NFH_Date = default(System.DateTime);
                System.DateTime DOJ_Date_Format = default(System.DateTime);
                string qry_nfh = "";
                double NFH_Present_Check = 0;
                static int In = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-PayRoll Attendance";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            Date1_str = Request.QueryString["FromDate"].ToString();
            Date2_str = Request.QueryString["ToDate"].ToString();
            Wages_Type = Request.QueryString["Wages"].ToString();
            SessionUserType = Session["UserType"].ToString();

            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("ExistingCode");
            AutoDTable.Columns.Add("MachineEncry");
            AutoDTable.Columns.Add("EmpName");

            DataCells.Columns.Add("MachineID");
            DataCells.Columns.Add("ExistingCode");
            DataCells.Columns.Add("FirstName");
            DataCells.Columns.Add("Days");
            DataCells.Columns.Add("H.Allowed");
            DataCells.Columns.Add("N/FH");
            DataCells.Columns.Add("OT Days");
            DataCells.Columns.Add("SPG Allow");
            DataCells.Columns.Add("Canteen Days Minus");
            DataCells.Columns.Add("OT Hours");
            DataCells.Columns.Add("W.H");
            DataCells.Columns.Add("Fixed W.Days");
            DataCells.Columns.Add("NFH W.Days");
            DataCells.Columns.Add("Total Month Days");
            DataCells.Columns.Add("LOP Days");
            DataCells.Columns.Add("NFH Worked Days");
            DataCells.Columns.Add("NFH D.W Statutory");

            Write_payroll_attenance_New();


            grid.DataSource = DataCells;
            grid.DataBind();
            string attachment = "attachment;filename=PAYROLL ATTENDANCE.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
          
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
            Response.Write("  ");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">MONTHLY ATTENDANCE REPORT</a>");
            Response.Write("  ");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">FROM DATE:" + Date1_str + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">TO DATE:" + Date2_str + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    public void Write_payroll_attenance_New()
    {
        try
        {
           Date1 = Convert.ToDateTime(Date1_str);
           Date2 = Convert.ToDateTime(Date2_str); 
            


            //var _with1 = xls;

            ////.cells(1, 1).value = xlTitle
            ////.cells(1, 4).EntireRow.Font.Bold = True


            //int intI = 1;
            //int intK = 1;
            //int intCol = 0;

            //_with1.cells(1, 1).value = "Machine ID";
            //_with1.cells(1, 2).value = "Token No";
            //_with1.cells(1, 3).value = "EmpName";
            //_with1.cells(1, 4).value = "Days";
            //_with1.cells(1, 5).value = "H.Allowed";
            //_with1.cells(1, 6).value = "N/FH";
            //_with1.cells(1, 7).value = "OT Days";
            //_with1.cells(1, 8).value = "SPG Allow";
            //_with1.cells(1, 9).value = "Canteen Days Minus";
            //_with1.cells(1, 10).value = "OT Hours";
            //_with1.cells(1, 11).value = "W.H";
            //_with1.cells(1, 12).value = "Fixed W.Days";
            //_with1.cells(1, 13).value = "NFH W.Days";
            //_with1.cells(1, 14).value = "Total Month Days";
            ////.cells(1, 15).value = "LOP Days"
            //_with1.cells(1, 15).value = "NFH Worked Days";
            //_with1.cells(1, 16).value = "NFH D.W Statutory";

            DataTable Log_DS = new DataTable();

            SSQL = "Select LTD.MachineID,LTD.ExistingCode,EM.FirstName,sum(LTD.Present) as Days,";
            SSQL = SSQL + " EM.DOJ,EM.Weekoff from LogTime_Days LTD inner join Employee_Mst EM on";
            SSQL = SSQL + " EM.CompCode=LTD.CompCode And EM.LocCode=LTD.LocCode And LTD.MachineID=EM.MachineID";
            SSQL = SSQL + " where LTD.CompCode='" + SessionCcode  + "' And LTD.LocCode='" + SessionLcode  + "'";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.Wages='" + Wages_Type + "' And EM.IsActive='Yes'";
            SSQL = SSQL + " And LTD.Attn_Date >= '" + Date1.ToString("yyyy/MM/dd") +"'";
            SSQL = SSQL + " And LTD.Attn_Date <= '" + Date2.ToString("yyyy/MM/dd") + "'";
            SSQL = SSQL + " Group by LTD.MachineID,LTD.ExistingCode,EM.FirstName,EM.DOJ,EM.Weekoff";
            SSQL = SSQL + " Order by LTD.ExistingCode Asc";

            Log_DS = objdata.ReturnMultipleValue(SSQL);

            

            for (int i = 0; i < Log_DS.Rows.Count; i++)
            {
                Present_Days_Count = Convert.ToDecimal(Log_DS.Rows[i]["Days"].ToString());

                if (Log_DS.Rows[i]["DOJ"] == "")
                {
                    DOJ_Date_Str = "";
                }
                else
                {
                    DOJ_Date_Str = Log_DS.Rows[i]["DOJ"].ToString();
                }

                Emp_WH_Day = Log_DS.Rows[i]["WeekOff"].ToString();

               
                //Final Output Variable Declaration
              
                qry_nfh = "Select NFHDate from NFH_Mst where NFHDate >= '" + Date1.ToString("yyyy/MM/dd") +"'";
                qry_nfh = qry_nfh + " And NFHDate <= '" + Date2.ToString("yyyy/MM/dd") + "'";
                NFH_DS = objdata.ReturnMultipleValue(qry_nfh);
                if (NFH_DS.Rows.Count > 0)
                {
                    //Date OF Joining Check Start
                    for (int K = 0; K < NFH_DS.Rows.Count; K++)
                    {
                  
                        NFH_Date = Convert.ToDateTime(NFH_DS.Rows[K]["NFHDate"].ToString());
                        //NFH Day Present Check
                        string NFH_Date_P_Date = (Convert.ToDateTime(NFH_Date).AddDays(0).ToString("yyyy/MM/dd"));
                        SSQL = "Select * from LogTime_Days where Attn_Date='" + NFH_Date_P_Date + "' And MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode  + "' And LocCode='" + SessionLcode  + "'";
                        mDataTable = objdata.ReturnMultipleValue(SSQL);
                        if (mDataTable.Rows.Count != 0)
                        {
                            NFH_Present_Check = Convert.ToDouble(mDataTable.Rows[0]["Present"].ToString());
                        }
                        else
                        {
                            NFH_Present_Check = 0;
                        }

                        //Get NFh Type
                        SSQL = "Select * from NFH_Mst where NFHDate=convert(varchar,'" + NFH_Date_P_Date + "',103)";
                        NFH_Type_Ds = objdata.ReturnMultipleValue(SSQL);
                        if (NFH_Type_Ds.Rows.Count != 0)
                        {
                            NFH_Type_Str = NFH_Type_Ds.Rows[0]["NFH_Type"].ToString();
                            NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                        }
                        else
                        {
                            NFH_Type_Str = "";
                            NFH_Dbl_Wages_Statutory_Check = "";
                        }


                        if (!string.IsNullOrEmpty(DOJ_Date_Str))
                        {
                            DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str.ToString());
                            if (DOJ_Date_Format.Date < NFH_Date.Date)
                            {
                                if (NFH_Type_Str.ToString() == "WH Minus")
                                {
                                    NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;
                                }
                                else
                                {
                                    NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Type_Str == "Double Wages Checklist")
                                    {
                                        NFH_Double_Wages_Checklist = Convert.ToDecimal(NFH_Double_Wages_Checklist) + Convert.ToDecimal(NFH_Present_Check);
                                        if (NFH_Dbl_Wages_Statutory_Check.ToString() == "Yes")
                                        {
                                            NFH_Double_Wages_Statutory = Convert.ToDecimal(NFH_Double_Wages_Statutory) + Convert.ToDecimal(NFH_Present_Check);
                                        }
                                    }

                                    if(NFH_Type_Str == "Double Wages Manual")
                                        NFH_Double_Wages_Manual = Convert.ToDecimal(NFH_Double_Wages_Manual) + Convert.ToDecimal(NFH_Present_Check);
                                }
                                NFH_Days_Count = NFH_Days_Count + 1;
                            }
                            else
                            {
                                //Skip
                            }
                        }
                        else
                        {
                            NFH_Days_Count = NFH_Days_Count + 1;
                            if (NFH_Type_Str == "WH Minus")
                            {
                                NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;
                            }
                            else
                            {
                                NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(NFH_Present_Check);
                                if (NFH_Type_Str == "Double Wages Checklist")
                                {
                                    NFH_Double_Wages_Checklist = Convert.ToDecimal(NFH_Double_Wages_Checklist) + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Dbl_Wages_Statutory_Check == "Yes")
                                    {
                                        NFH_Double_Wages_Statutory = Convert.ToDecimal(NFH_Double_Wages_Statutory) + Convert.ToDecimal(NFH_Present_Check);
                                    }
                                }
                                if (NFH_Type_Str == "Double Wages Manual")
                                    NFH_Double_Wages_Manual = Convert.ToDecimal(NFH_Double_Wages_Manual) + Convert.ToDecimal(NFH_Present_Check);
                            }
                        }
                    }
                    //Date OF Joining Check End
                }
                else
                {
                    //Skip
                }
                //NFH Check END

                //Week of Check
                DataTable WH_DS = new DataTable();
                System.DateTime Week_Off_Date = default(System.DateTime);
                System.DateTime WH_DOJ_Date_Format = default(System.DateTime);
                bool Check_Week_Off_DOJ = false; 
                
                SSQL = "Select Present,Attn_Date from LogTime_Days where MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionCcode + "'";
                SSQL = SSQL + " And upper(DATENAME(weekday,Attn_Date))=upper('" + Emp_WH_Day + "')";
                SSQL = SSQL + " And Attn_Date >= '" + Date1.ToString("yyyy/MM/dd") + "'";
                SSQL = SSQL + " And Attn_Date <= '" + Date2.ToString("yyyy/MM/dd") + "'";
                WH_DS = objdata.ReturnMultipleValue(SSQL);
                for (int j = 0; j < WH_DS.Rows.Count; j++)
                {

                    if (WH_DS.Rows[j]["Attn_Date"].ToString() == "")
                    {
                       
                    }
                    else
                    {
                        Week_Off_Date = Convert.ToDateTime(WH_DS.Rows[j]["Attn_Date"].ToString());
                    }



                    if (Wages_Type.ToString() == "MONTHLY-MONTHLY-STAFF" | Wages_Type.ToString() == "Watch & Ward")
                    {
                        if (!string.IsNullOrEmpty(DOJ_Date_Str))
                        {
                            WH_DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (WH_DOJ_Date_Format.Date <= Week_Off_Date.Date)
                            {
                                Check_Week_Off_DOJ = true;
                            }
                            else
                            {
                                Check_Week_Off_DOJ = false;
                            }
                        }
                        else
                        {
                            Check_Week_Off_DOJ = true;
                        }
                    }
                    else
                    {
                        Check_Week_Off_DOJ = true;
                    }

                    if (Check_Week_Off_DOJ == true)
                    {
                        WH_Count = WH_Count + 1;
                        //NFH Day Check
                        string NFH_Date_WH_Date = Convert.ToDateTime(Week_Off_Date.ToString()).ToString("yyyy/MM/dd");
                        SSQL = "Select * from NFH_Mst where NFHDate=convert(varchar,'" + NFH_Date_WH_Date + "',103)";
                        mDataTable = objdata.ReturnMultipleValue(SSQL);
                        if (mDataTable.Rows.Count > 0)
                        {
                            //Skip
                            if (mDataTable.Rows.Count != 0)
                            {
                                NFH_Type_Str = mDataTable.Rows[0]["NFH_Type"].ToString();
                                NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            }
                            else
                            {
                                NFH_Type_Str = "";
                                NFH_Dbl_Wages_Statutory_Check = "";
                            }
                            if (NFH_Type_Str == "WH Minus")
                            {
                                WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"]);
                            }
                            else
                            {
                                //Skip
                            }
                        }
                        else
                        {
                            WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"]);
                        }
                    }
                }

                //Spinning Incentive Check
                if (Wages_Type.ToString() == "REGULAR" | Wages_Type.ToString() == "HOSTEL")
                {
                    //Check Spinning Days Start
                    bool Check_Spinning_Eligible = false;
                    DataTable DS_Spinning_Incv = new DataTable();
                    //Spinning Wages Check
                    Int32 Month_Int = 1;
                    string Months_Full_Str = "";
                    string Spin_Wages = Wages_Type.ToString();
                    string Spin_Machine_ID_Str = Log_DS.Rows[i]["MachineID"].ToString();
                    string Fin_Year = "";

                    Month_Int = Date1.Month;

                    //Month_Int = Month(dtpFromDate.Value.Date);
                    if (Month_Int >= 4)
                    {
                        Fin_Year = Date1.Year + "-" + Date1.Year + 1;
                    }
                    else
                    {
                        Fin_Year = Date1.Year - 1 + "-" + Date1.Year;
                    }
                    Months_Full_Str = Date1.ToString("MMMM");
                    Months_Full_Str = Left_Val(Months_Full_Str, 3);
                    SSQL = "Select * from SpinIncentiveDetPart where CompCode='" + SessionCcode + "' And LocCode='" + SessionCcode + "' And Months='" + Months_Full_Str + "' And FinYear='" + Fin_Year + "' And Wages='" + Spin_Wages + "' And MachineID='" + Spin_Machine_ID_Str + "'";
                    DS_Spinning_Incv = objdata.ReturnMultipleValue(SSQL);
                    if (DS_Spinning_Incv.Rows.Count != 0)
                    {
                        Check_Spinning_Eligible = true;
                    }
                    else
                    {
                        Check_Spinning_Eligible = false;
                    }
                    if (Check_Spinning_Eligible == true)
                    {
                        if (Wages_Type.ToString() == "HOSTEL")
                        {
                            Spinning_Incentive_Days = Convert.ToDecimal(Log_DS.Rows[i]["Days"].ToString());
                        }
                        else
                        {
                            if (Wages_Type.ToString() == "REGULAR")
                            {
                                SSQL = "Select isnull(sum(Present),0) as Days from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionCcode + "'";
                                SSQL = SSQL + " And MachineID='" + Log_DS.Rows[i]["MachineID"] + "'";
                                SSQL = SSQL + " And Attn_Date >= '" + Date1.ToString("yyyy/MM/dd") + "'";
                                SSQL = SSQL + " And Attn_Date <= '" + Date2.ToString("yyyy/MM/dd") + "'";
                                SSQL = SSQL + " And (Shift='SHIFT2' or Shift='SHIFT3')";
                                mDataTable = objdata.ReturnMultipleValue(SSQL);
                                if (mDataTable.Rows.Count != 0)
                                {
                                    Spinning_Incentive_Days = Convert.ToDecimal(mDataTable.Rows[0]["Days"].ToString());
                                }
                                else
                                {
                                    Spinning_Incentive_Days = 0;
                                }
                            }
                            else
                            {
                                Spinning_Incentive_Days = 0;
                            }
                        }
                    }
                }
                else
                {
                    Spinning_Incentive_Days = 0;
                }

                //Total Days Get

                 int Total_Days_Count = (int)((Date2 - Date1).TotalDays);

                //int Total_Days_Count = DateDiff(DateInterval.Day, dtpFromDate.Value.Date, dtpToDate.Value.Date);
                Total_Days_Count = Total_Days_Count + 1;
                Int32 Month_Mid_Total_Days_Count = default(Int32);
                //Check DOJ Date to Report Date
                System.DateTime Report_Date = default(System.DateTime);
                System.DateTime DOJ_Date_Format_Check = default(System.DateTime);
                if (Wages_Type.ToString() == "MONTHLY-STAFF" | Wages_Type.ToString() == "Watch & Ward")
                {
                    if (!string.IsNullOrEmpty(DOJ_Date_Str))
                    {
                        Report_Date = Date1;
                        //Report Date
                        DOJ_Date_Format_Check = Convert.ToDateTime(DOJ_Date_Str);
                        if (DOJ_Date_Format_Check.Date <= Report_Date.Date)
                        {
                            Month_Mid_Total_Days_Count = Total_Days_Count;
                        }
                        else
                        {
                            Month_Mid_Total_Days_Count = (int)((Date2 - DOJ_Date_Format_Check).TotalDays);

                            //Month_Mid_Total_Days_Count = DateDiff(DateInterval.Day, .Date, .Value.Date);
                            Month_Mid_Total_Days_Count = Month_Mid_Total_Days_Count + 1;
                        }
                    }
                    else
                    {
                        Month_Mid_Total_Days_Count = Total_Days_Count;
                    }
                }
                else
                {
                    //Skip
                }

                if (Wages_Type.ToString() == "MONTHLY-STAFF" | Wages_Type.ToString() == "Watch & Ward")
                {
                    Fixed_Work_Days = Convert.ToInt32(Month_Mid_Total_Days_Count) - Convert.ToInt32(WH_Count);
                    Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count + NFH_Days_Present_Count;
                }
                else
                {
                    Fixed_Work_Days = 0;
                    Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    //WH_Present_Count = WH_Present_Count;
                }



                if (Present_Days_Count != 0 & NFH_Days_Count != 0 & NFH_WH_Days_Mins != 0)
                {
                    double NFH_Week_Off_Minus_Days = Convert.ToDouble(NFH_WH_Days_Mins);
                    double NFH_Final_Disp_Days = 0;
                    if (Convert.ToDecimal(NFH_Week_Off_Minus_Days) <= WH_Present_Count)
                    {
                        WH_Present_Count = WH_Present_Count - Convert.ToDecimal(NFH_Week_Off_Minus_Days);
                    }
                    else
                    {
                        //NFH Days Minus for Working Days
                        double Balance_NFH_Days_Minus_IN_WorkDays = 0;
                        Balance_NFH_Days_Minus_IN_WorkDays = Convert.ToDouble(NFH_Week_Off_Minus_Days) - Convert.ToDouble(WH_Present_Count);
                        Present_Days_Count = Present_Days_Count - Convert.ToDecimal(Balance_NFH_Days_Minus_IN_WorkDays);
                        WH_Present_Count = 0;
                    }
                }
                //'Get NFH Rule End

                Decimal NFH_Doubale_Wages_Attn_Inct = (Convert.ToDecimal(NFH_Double_Wages_Checklist) + NFH_Double_Wages_Manual + NFH_WH_Days_Mins);

                if (Present_Days_Count != 0)
                {

                    DataCells.NewRow();
                    DataCells.Rows.Add();
                    DataCells.Rows[i]["MachineID"] = Log_DS.Rows[In]["MachineID"];
                    DataCells.Rows[i]["ExistingCode"] = Log_DS.Rows[In]["ExistingCode"];
                    DataCells.Rows[i]["FirstName"] = Log_DS.Rows[In]["FirstName"];
                    DataCells.Rows[i]["Days"] = Present_Days_Count;
                    DataCells.Rows[i]["H.Allowed"] = "0";
                    DataCells.Rows[i]["N/FH"] = NFH_Days_Count;       //NFH_Days_Count


                    if (Wages_Type.ToString() == "MONTHLY-STAFF" | Wages_Type.ToString() == "Watch & Ward")
                    {
                        DataCells.Rows[i]["OT Days"] = "0";
                        DataCells.Rows[i]["SPG Allow"] = "0";
                        DataCells.Rows[i]["Canteen Days Minus"] = "0";
                        DataCells.Rows[i]["OT Hours"] = "0";

                        DataCells.Rows[i]["W.H"] = WH_Present_Count;
                        DataCells.Rows[i]["Fixed W.Days"] = Fixed_Work_Days;

                    }
                    else
                    {
                        DataCells.Rows[i]["OT Days"] = WH_Present_Count;
                        DataCells.Rows[i]["SPG Allow"] = Spinning_Incentive_Days;
                        DataCells.Rows[i]["Canteen Days Minus"] = "0";
                        DataCells.Rows[i]["OT Hours"] = "0";

                        DataCells.Rows[i]["W.H"] = "0";
                        DataCells.Rows[i]["Fixed W.Days"] = "0";
                    }

                    DataCells.Rows[i]["NFH W.Days"] = NFH_Double_Wages_Checklist;
                    DataCells.Rows[i]["Total Month Days"] = Total_Days_Count;
                    DataCells.Rows[i]["LOP Days"] = NFH_Doubale_Wages_Attn_Inct;
                    DataCells.Rows[i]["NFH Worked Days"] = NFH_Double_Wages_Statutory;

                   

                    //In = In + 1;

                }
                NFH_Days_Count = 0;
                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;

            }
        }

         catch (Exception ex)
        {
            
        }
    }
}
                 
