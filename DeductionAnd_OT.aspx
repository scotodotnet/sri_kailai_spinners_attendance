<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DeductionAnd_OT.aspx.cs" Inherits="DeductionAnd_OT" Title="Untitled Page" EnableViewState="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Deduction & OT</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
    <div class="col-md-9">
        <div class="panel panel-white">
        <div class="panel panel-primary">
            <div class="panel-heading clearfix">
                <h3 class="panel-title">Deduction & OT</h3>
            </div>
            </div>
           
             <form class="form-horizontal">
						
						
				           <%-- <form class="form-horizontal">--%>
				            <!-- panel body end -->
					
				       <%-- </form>--%>
			            
		               
                       	<div class="col-md-1"></div>
						</form>
						
						
            <div class="panel-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Allowances & Deduction</a></li>

                        </ul>
                  	
                     <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="tab1">
                        
                       <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                       
                        
                       <%-- <form class="form-horizontal">--%>
						<div class="col-md-1"></div>
						<div class="col-md-12">
			            <div class="panel panel-white">
				            
				          <%--  <form class="form-horizontal">--%>
				            <div class="panel-body">
					  
						<div class="form-group row">
   <!-- col-12 start --><div class="col-sm-12">
				               
						    <div class="form-group row">
						    <div class="col-sm-12">
						    <div class="col-sm-6">
						    
						     <div class="form-group row">
						    <label for="input-Default" class="col-sm-3 control-label">Month</label>
							<div class="col-sm-6">
						     <asp:DropDownList ID="ddlMonth" runat="server" class="form-control">
                               <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                 <asp:ListItem>January</asp:ListItem>
                                                 <asp:ListItem>February</asp:ListItem>
                                                 <asp:ListItem>March</asp:ListItem>
                                                 <asp:ListItem>April</asp:ListItem>
                                                 <asp:ListItem>May</asp:ListItem>
                                                 <asp:ListItem>June</asp:ListItem>
                                                 <asp:ListItem>July</asp:ListItem>
                                                 <asp:ListItem>August</asp:ListItem>
                                                 <asp:ListItem>September</asp:ListItem>
                                                 <asp:ListItem>October</asp:ListItem>  
                                                 <asp:ListItem>November</asp:ListItem>
                                                 <asp:ListItem>December</asp:ListItem>   
                                  </asp:DropDownList>       
						   </div>
						   </div>
						  <%-- <asp:UpdatePanel ID="Wages" runat="server" >
						  
						   <ContentTemplate>--%>
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-3 control-label">Wages</label>
							<div class="col-sm-6">
						  
						     <asp:DropDownList ID="ddlWages" runat="server" class="form-control" 
                                    onselectedindexchanged="ddlWages_SelectedIndexChanged1"  AutoPostBack="true"  >
                                   
                                
                                </asp:DropDownList>       
						   </div>
						   </div>
						   
						     <div class="form-group row">
						    <label for="input-Default" class="col-sm-3 control-label">Name</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtName" class="form-control" runat="server" required></asp:TextBox>       
						   </div>
						   </div>
						  
						 <%-- </ContentTemplate>
						    <Triggers>
						   <asp:PostBackTrigger ControlID="ddlMID" />
						   	<asp:AsyncPostBackTrigger ControlID ="ddlMID" />
						   	</Triggers>
						   </asp:UpdatePanel> --%>
						   
						   
						   
						   
						   
						   
						   
						    
						   
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-3 control-label">From Date</label>
							<div class="col-sm-6">
							    <asp:TextBox ID="txtFromDate" name="ctl00$ContentPlaceHolder1$tin_date" type="text" class="form-control date-picker" runat="server" required></asp:TextBox>   
                                <%--    <input name="ctl00$ContentPlaceHolder1$tin_date" type="text" id="Text4" class="form-control date-picker">  --%>			
                                               <cc1:CalendarExtender ID="CalendarExtender2" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtFromDate">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtFromDate" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
                                
                                
                                			   </div>
						   </div>
						   
						  
						   
						   
						   
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-3 control-label">OT Hours</label>
							<div class="col-sm-6">
						     <asp:TextBox ID="txtOT_hours" class="form-control" runat="server" required></asp:TextBox>       
						   </div>
						   </div>
						   
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-3 control-label">LWF</label>
							<div class="col-sm-6">
						     <asp:TextBox ID="txtLabourWellFund" class="form-control" runat="server" required></asp:TextBox>       
						   </div>
						   </div>
						   
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">Allowance1</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtAllowance1" class="form-control" runat="server" required></asp:TextBox>       
						   </div> 
						   </div>
						   
						    
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">Allowance2</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtAllowance2" class="form-control" runat="server" required></asp:TextBox>       
						   </div> 
						   </div>
						   
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">Allowance3</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtAllowance3" class="form-control" runat="server" required></asp:TextBox>       
						   </div> 
						   </div>
						   
						   
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">H.Allowed</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtH_allowed" class="form-control" runat="server" required></asp:TextBox>       
						   </div> 
						   </div>
						   
						   
						   </div><!-- col-6 end -->
						   
						   
						   <div class="col-sm-6">
						   
						     <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">Financial Year</label>
							<div class="col-sm-6">
						     <asp:DropDownList ID="ddlFinancialYear" runat="server" class="form-control">
                               
                             </asp:DropDownList>       
						   </div> 
						   </div>
						   
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-3 control-label">MachineID</label>
						   <div class="col-sm-6">
                               <asp:DropDownList ID="ddlMID" runat="server"  class="form-control"
                                   onselectedindexchanged="ddlMID_SelectedIndexChanged" AutoPostBack="true">
                               </asp:DropDownList>
						   </div>
						   </div> 
						   
						    
						   
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">Ticket No</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtTicketNumberTab1" class="form-control" AutoPostBack="true"  runat="server" required ontextchanged="txtTicketNumberTab1_TextChanged" 
                                    ></asp:TextBox>       
						   </div> 
						   </div>
						   
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">To Date</label>
							<div class="col-sm-6">
							<asp:TextBox ID="txtToDate" name="ctl00$ContentPlaceHolder1$tin_date" type="text" class="form-control date-picker" runat="server" required></asp:TextBox>
                              
                             <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                               CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                               TargetControlID="txtToDate">
                            </cc1:CalendarExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" 
                              FilterMode="ValidChars" 
                              FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                              TargetControlID="txtToDate" ValidChars="0123456789-/">
                            </cc1:FilteredTextBoxExtender>
          
						   </div> 
						   </div>
						   
						  
						   
						  
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">Advance Allowance</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtAdvanceAllowance" class="form-control" runat="server" required></asp:TextBox>       
						   </div> 
						   </div>
						   
						  <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">P.Tax Deduction</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtPtax_Deduction" class="form-control" runat="server" required></asp:TextBox>       
						   </div> 
						   </div>
						   
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">LOP Days</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtLossOfPaydays" class="form-control" runat="server" required></asp:TextBox>       
						   </div> 
						   </div>
						  
						   
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">Deduction1</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtDeduction1" class="form-control" runat="server" required></asp:TextBox>       
						   </div> 
						   </div>
						   
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">Deduction2</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtDeduction2" class="form-control" runat="server" required></asp:TextBox>       
						   </div> 
						   </div>
						   
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">Deduction3</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtDeduction3" class="form-control" runat="server" required></asp:TextBox>       
						   </div> 
						   </div>
						   
						   </div><!-- col-6 end -->
				            
				            </div>
				            </div><!-- col-12 end -->
				                        
				  </div><!-- col-12 over all end -->
				  </div>
						<!-- Button start -->
						<div class="form-group row">
						</div>
                            <div class="txtcenter">
                                   <asp:Button ID="btnDeductionAndAllowance" class="btn btn-success"  runat="server" 
                                    Text="Save" onclick="btnDeductionAndAllowance_Click"/> 
                           </div>
                    <!-- Button End -->
                    
                    <!-- Table Start --> <div class="form-group row">
						</div>
						
						<div class="panel-body">
                             <div class="table-responsive">
    
                              <asp:Repeater ID="rptrCustomer" runat="server">
            <HeaderTemplate>
                
                  <table id="tableCustomer" class="display table" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                                                   <%-- <th>SNo</th>--%>
                                                    <th>Location Name</th>
                                                    <th>User Name</th>  
                                                    <th>User Type</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    
                   <%-- <td>
                        <%# DataBinder.Eval(Container.DataItem, "SNo")%>
                    </td>--%>
                    
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "LocCode")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "UserName")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "UserType")%>
                    </td>
                    <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("SNo") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("SNo") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
                        </div>
						
						
						
						
						
						<!-- table end -->
                 
						
						</div><!-- panel body end -->
					
				        <%--</form>--%>
			            </div><!-- panel white end -->
		                </div><!-- col-12 end -->
		                
                       	<div class="col-md-1"></div>
						<%--</form>--%>
						
						</ContentTemplate>
                                </asp:UpdatePanel> 
                        </div><!-- tab1 end -->
                        
                         <!-- tab2 start -->
                        <div role="tabpane2" class="tab-pane" id="tab2">
                        <form class="form-horizontal">
						<div class="col-md-1"></div>
						<div class="col-md-12">
			            <div class="panel panel-white">
                            <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
				            <form class="form-horizontal">
				            <div class="panel-body">
				            
            				<div class="form-group row">
						    <div class="col-sm-12">
						    <div class="col-sm-6">
						    
						   <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>						     <%-- <div class="panel-heading clearfix">
					            <h4 class="panel-title">Other</h4>
				            </div>--%>						        <%--<input name="ctl00$ContentPlaceHolder1$tin_date" type="text" id="Text3" class="form-control date-picker">--%>
						   
						    
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">Ticket No</label>
							<div class="col-sm-6">
						     <asp:TextBox ID="txtTicketNumbertab2" class="form-control" runat="server"></asp:TextBox>       
						   </div>
						   </div>
						   
						    
						     <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">Name</label>
							<div class="col-sm-6">
						     <asp:TextBox ID="txtNametab2" class="form-control" runat="server"></asp:TextBox>       
						   </div>
						   </div>
						   
						    <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">Department</label>
							<div class="col-sm-6">
						     <asp:TextBox ID="txtdepartment" class="form-control" runat="server"></asp:TextBox>       
						   </div>
						   </div>
						   
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">OT Hours</label>
							<div class="col-sm-6">
						         <asp:TextBox ID="txtOThours" class="form-control" runat="server"></asp:TextBox>   
						   </div>
						   </div>
						   
						   
						 
						
						   </div><!-- col-6 end -->
						   
						   
						   <div class="col-sm-6">
						   
						    <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">Existing No</label>
							<div class="col-sm-6">
						     <asp:TextBox ID="txtExistingNumber" class="form-control" runat="server"></asp:TextBox>       
						   </div>
						   </div>
						   
						   
						   
						   
						   
						     <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">Wages</label>
							<div class="col-sm-6">
						    <asp:DropDownList ID="ddlWagestab2" runat="server" class="form-control">
                               
                               </asp:DropDownList>       
						   </div>
						   </div>
						   
						
						    <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">Designation</label>
							<div class="col-sm-6">
						     <asp:TextBox ID="txtDesignation" class="form-control" runat="server"></asp:TextBox>      
						   </div>
						   </div>
						   
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">OT Date</label>
							<div class="col-sm-6">
							   <asp:TextBox ID="txtOTdays" name="ctl00$ContentPlaceHolder1$tin_date" type="text" class="form-control date-picker" runat="server"></asp:TextBox>     
                                <%--<input name="ctl00$ContentPlaceHolder1$tin_date" type="text" id="Text9" class="form-control date-picker">--%>   
						   </div>
						   </div>
						   
						   
						   <div class="form-group row">
						   <div class="col-sm-4"></div>
						   <div class="col-sm-6">
						   <asp:CheckBox ID="CheckBox2" runat="server" />Eligible for Incentive
						   </div>
						   </div>
						      
				           </div><!-- col-6 end -->
				            
				            </div><!-- col-12 end -->
				            </div>
				            
					           		<!-- Button start -->
						<div class="form-group row">
						</div>
                            <div class="txtcenter">
                    <asp:Button ID="btnSavetab2" class="btn btn-success"  runat="server" Text="save" 
                                    onclick="btnSavetab2_Click" />
                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                    <asp:Button ID="btnCleartab2" class="btn btn-danger" runat="server" Text="Clear" />
                    </div>
                    <!-- Button End -->
                    
                    <!-- Table Start --> <div class="form-group row">
						</div>
						<div class="table-responsive scroll-table">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>S.No</th>
                                                    <th>Ticket No</th>
                                                    <th>Emp Name</th>
                                                    <th>OT Hourss</th>
                                                    <th>SPG Inctv</th>
                                                    <th>Department</th>
                                               </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><asp:CheckBox ID="CheckBox10" runat="server" /></td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                <tr>
                                                    <td><asp:CheckBox ID="CheckBox11" runat="server" /></td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td><asp:CheckBox ID="CheckBox12" runat="server" /></td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td><asp:CheckBox ID="CheckBox13" runat="server" /></td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td><asp:CheckBox ID="CheckBox14" runat="server" /></td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td><asp:CheckBox ID="CheckBox15" runat="server" /></td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td><asp:CheckBox ID="CheckBox16" runat="server" /></td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td><asp:CheckBox ID="CheckBox17" runat="server" /></td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div><!-- table end -->
                                    
            				<!-- Button start -->
						<div class="form-group row">
						</div>
                            <div class="txtcenter">
                     <div class="form-group row">  
                    <asp:Button ID="btnView2" class="btn btn-info"  runat="server" Text="View" />
                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                    <asp:Button ID="btnDelete2" class="btn btn-danger" runat="server" Text="Delete" />
                    </div>
                    </div> 
                    
                    <!-- Button End -->
                    
				            </div><!-- panel body end -->
				            </form>
			            </div><!-- panel white end -->
		                </div><!-- col-12 end -->
		            <div class="col-md-1"></div>
						</form> 
						</div> <!-- tab2 end -->
						
						
						<!-- tab3 start -->
                        <div role="tabpane3" class="tab-pane" id="tab3">
                        <form class="form-horizontal">
						<div class="col-md-1"></div>
						<div class="col-md-12">
			            <div class="panel panel-white">
				           <%-- <div class="panel-heading clearfix">
					            <h4 class="panel-title">Other</h4>
				            </div>--%>
				            <form class="form-horizontal">
				            <div class="panel-body">
				            
            				<div class="form-group row">
						    <div class="col-sm-12">
						    <div class="col-sm-6">
						    
						     <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">Name</label>
							<div class="col-sm-6">
						      <asp:TextBox ID="txtNametab3" class="form-control" runat="server"></asp:TextBox>      
						   </div>
						   </div>
						   
						    <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">From Date</label>
							<div class="col-sm-6">
							 <asp:TextBox ID="txtFromDatetab3" name="ctl00$ContentPlaceHolder1$tin_date" type="text" class="form-control date-picker" runat="server"></asp:TextBox> 
							<%--<input name="ctl00$ContentPlaceHolder1$tin_date" type="text" id="Text3" class="form-control date-picker">--%>
						    </div>
						   </div>
						   
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">Month</label>
							<div class="col-sm-6">
						       <asp:DropDownList ID="ddlMonth3" runat="server" class="form-control">
                                <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                 <asp:ListItem>January</asp:ListItem>
                                                 <asp:ListItem>February</asp:ListItem>
                                                 <asp:ListItem>March</asp:ListItem>
                                                 <asp:ListItem>April</asp:ListItem>
                                                 <asp:ListItem>May</asp:ListItem>
                                                 <asp:ListItem>June</asp:ListItem>
                                                 <asp:ListItem>July</asp:ListItem>
                                                 <asp:ListItem>August</asp:ListItem>
                                                 <asp:ListItem>September</asp:ListItem>
                                                 <asp:ListItem>October</asp:ListItem>  
                                                 <asp:ListItem>November</asp:ListItem>
                                                 <asp:ListItem>December</asp:ListItem>   
                               </asp:DropDownList>    
						   </div>
						   </div>
						   
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">Wages</label>
							<div class="col-sm-6">
						      <asp:DropDownList ID="ddlwagestab3" runat="server" class="form-control">
                             
                               </asp:DropDownList>        
						   </div>
						   </div>
						 
						   </div><!-- col-6 end -->
						   
						   
						   <div class="col-sm-6">
						   
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">Ticket No</label>
							<div class="col-sm-6">
						     <asp:TextBox ID="txtTicketNumbertab3" class="form-control" runat="server"></asp:TextBox>       
						   </div>
						   </div>
						   
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">To Date</label>
							<div class="col-sm-6">
							<asp:TextBox ID="txtToDatetab3" class="form-control date-picker" name="ctl00$ContentPlaceHolder1$tin_date" type="text" runat="server"></asp:TextBox> 
						    <%--<input name="ctl00$ContentPlaceHolder1$tin_date" type="text" id="Text9" class="form-control date-picker">--%>   
						   </div>
						   </div>
						   
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">Financial Year</label>
							<div class="col-sm-6">
						       <asp:DropDownList ID="ddlFinancialYeartab3" runat="server" class="form-control">
                                <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                               </asp:DropDownList> 
						   </div>
						   </div>
						   
						    <div class="form-group row">
						    <label for="input-Default" class="col-sm-4 control-label">Pay Period</label>
							<div class="col-sm-6">
						     <asp:DropDownList ID="ddlPayPeriod" runat="server" class="form-control">
                              </asp:DropDownList>      
						   </div>
						   </div>
						        
				           </div><!-- col-6 end -->
				            
				            </div>
				            </div>
					           
            							 
					<!-- Button start -->
						<div class="form-group row">
						</div>
                            <div class="txtcenter">
                    <asp:Button ID="btnEPayTransfer" class="btn btn-success"  runat="server" Text="E-pay Transfer" />
                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                    <asp:Button ID="btnIncentivedays" class="btn btn-info" runat="server" Text="Incentive days" />
                    </div>
                    <!-- Button End --> 

     
						
				            </div><!-- panel body end -->
				            </form>
			            </div><!-- panel white end -->
		                </div><!-- col-12 end -->
		            <div class="col-md-1"></div>
						</form> 
						</div> <!-- tab3 end -->
                        
                        
                    
					 </div><!-- tab -content end -->
                </div><!-- tab -panel end -->
            </div><!-- panel body end -->
        </div> <!-- panel  end -->
        </div><!-- col 9 end -->
        
         <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
</div><!-- col 12 end -->
</div><!-- row end -->

</div><!-- main-wrapper end -->
</asp:Content>

