﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="report1.aspx.cs" Inherits="report1" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

                <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Report</li></h4> 
                    </ol>
                </div>

        <div id="main-wrapper" class="container">
          <div class="row">
                 <div class="col-md-12">
                  <div class="col-sm-6">
                         <div class="panel panel-white">
                                <div class="panel panel-primary">
                                <div class="panel-heading clearfix">
                                    <h3 class="panel-title">Report</h3>
                                </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-6">
                                            <div class="dd" id="nestable">
                                                <ol class="dd-list">
                                                    <li class="dd-item" data-id="1">
                                                        <div class="dd-handle">Item 1</div>
                                                    </li>
                                                    <li class="dd-item" data-id="2">
                                                        <div class="dd-handle">Item 2</div>
                                                        <ol class="dd-list">
                                                            <li class="dd-item" data-id="3"><div class="dd-handle">Item 3</div></li>
                                                            <li class="dd-item" data-id="4"><div class="dd-handle">Item 4</div></li>
                                                            <li class="dd-item" data-id="5">
                                                                <div class="dd-handle">Item 5</div>
                                                                <ol class="dd-list">
                                                                    <li class="dd-item" data-id="6"><div class="dd-handle">Item 6</div></li>
                                                                    <li class="dd-item" data-id="7"><div class="dd-handle">Item 7</div></li>
                                                                    <li class="dd-item" data-id="8"><div class="dd-handle">Item 8</div></li>
                                                                </ol>
                                                            </li>
                                                            <li class="dd-item" data-id="9"><div class="dd-handle">Item 9</div></li>
                                                            <li class="dd-item" data-id="10"><div class="dd-handle">Item 10</div></li>
                                                        </ol>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                </div>
                            </div>
                         </div> <!-- col-sm-6 -->  
                         
                                <div class="col-sm-6">
                                <div class="panel panel-white">
                                <div class="panel panel-primary">
                                <div class="panel-heading clearfix">
                                    <h3 class="panel-title">Report</h3>
                                </div>
                                </div>
                                <div class="panel-body">
                                <div class="col-md-12">
                                            <div class="row">
											<div class="form-group col-md-6">
                                                <label for="exampleInputName">Company Code</label>
                                                        <asp:DropDownList ID="DropDownList34" runat="server" class="form-control col-md-6">
                                                        <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="1"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="2"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="3"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="4"></asp:ListItem>
                                                        </asp:DropDownList> 
                                                              
					                          </div>
                                              <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Location Code</label>
                                                            <asp:DropDownList ID="DropDownList1" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="1"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="2"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="3"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Department</label>
                                                            <asp:DropDownList ID="DropDownList2" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="1"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="2"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="3"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Wages Type</label>
                                                            <asp:DropDownList ID="DropDownList3" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="1"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="2"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="3"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Employee Code</label>
                                                            <asp:DropDownList ID="DropDownList4" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="1"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="2"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="3"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Token No</label>
                                                            <asp:DropDownList ID="DropDownList5" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="1"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="2"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="3"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Shift</label>
                                                            <asp:DropDownList ID="DropDownList6" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="1"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="2"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="3"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Mode</label>
                                                            <asp:DropDownList ID="DropDownList7" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="1"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="2"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="3"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Shift Type</label>
                                                            <asp:DropDownList ID="DropDownList8" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="1"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="2"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="3"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Year</label>
                                                            <asp:DropDownList ID="DropDownList9" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="1"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="2"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="3"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Type Of Certificate</label>
                                                    <asp:TextBox ID="TextBox3" class="form-control col-md-6" runat="server"></asp:TextBox>
												</div>
												<div class="form-group col-md-6">
                                                    <label for="exampleInputName">Leave Days</label>
                                                    <asp:TextBox ID="TextBox1" class="form-control col-md-6" runat="server"></asp:TextBox>
												</div>
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName" >From Date</label>
                                                    <input type="text" class="form-control date-picker" />
												</div>
												<div class="form-group col-md-6">
                                                    <label for="exampleInputName" >To Date</label>
                                                    <input type="text" class="form-control date-picker" />
												</div>
                                               
											</div>
										</div>
										
										<!-- Button start -->
						<div class="form-group row">
						<div class="txtcenter">
			        <asp:Button ID="Button1" class="btn btn-info"  runat="server" Text="30 days" />
                    <asp:Button ID="Button12" class="btn btn-info" runat="server" Text="60 days" />
                    <asp:Button ID="Button13" class="btn btn-info" runat="server" Text="Excel" />
                    </div>
                    </div>
                    <div class="form-group row">
					<div class="txtcenter">
						<asp:Button ID="Button2" class="btn btn-success" runat="server" Text="Report" />
                        <asp:Button ID="Button3" class="btn btn-danger" runat="server" Text="Exit" />
                    </div>
                    </div>
                    <!-- Button End -->
                    
                                </div>
                                
                                </div>
                                </div>       
                    
                       
                    </div>   
                 </div>   
              </div>      
                                     
       
</asp:Content>

