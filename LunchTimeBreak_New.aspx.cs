﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
public partial class LunchTimeBreak_New : System.Web.UI.Page
{
    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();

    DateTime ShiftdateStartIN;
    DateTime ShiftdateEndIN;
    DateTime InTime_Check;
    DateTime InToTime_Check;
    DateTime EmpdateIN;

    DataTable mEmployeeDS = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable DS_InTime = new DataTable();
    DataTable DS_Time = new DataTable();
    DataTable AutoDTable1 = new DataTable();
    string SSQL = "";
    string Final_InTime;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string Time_IN_Str;
    string Time_OUT_Str;
    string Date_Value_Str;
    string Shift_Start_Time;
    string Shift_End_Time;
    string Final_Shift;
    string Total_Time_get;
    string[] Time_Minus_Value_Check;
    string[] OThour_Str;
    DataSet ds = new DataSet();
    int time_Check_dbl;
    DataTable mdatatable = new DataTable();
    DataTable LocalDT = new DataTable();
    DataTable Shift_DS = new DataTable();
    TimeSpan InTime_TimeSpan;
    string From_Time_Str = "";
    string To_Time_Str = "";
    int OT_Hour;
    int OT_Min;
    int OT_Time;
    string OT_Hour1 = "";
    string mUser = "";
    string Datestr = "";
    string Datestr1 = "";

    Boolean Shift_Check_blb = false;
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString().Trim();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();

           
            //ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
           
            if (SessionUserType == "3")
            {
                NonAdminGetAttdDayWise_Change();
            }
            else
            {
                DataSet ds = new DataSet();
                DataTable AutoDTable = new DataTable();


                AutoDTable.Columns.Add("SNo");
                AutoDTable.Columns.Add("Dept");
                AutoDTable.Columns.Add("Type");
                AutoDTable.Columns.Add("Shift");
                AutoDTable.Columns.Add("EmpCode");
                AutoDTable.Columns.Add("ExCode");
                AutoDTable.Columns.Add("Name");
                AutoDTable.Columns.Add("TimeIN");
                AutoDTable.Columns.Add("TimeOUT");
                AutoDTable.Columns.Add("MachineID");
                AutoDTable.Columns.Add("Category");
                AutoDTable.Columns.Add("SubCategory");
                AutoDTable.Columns.Add("TotalMIN");
                AutoDTable.Columns.Add("GrandTOT");
                AutoDTable.Columns.Add("ShiftDate");
                AutoDTable.Columns.Add("OTHour");
                AutoDTable.Columns.Add("CompanyName");
                AutoDTable.Columns.Add("LocationName");
                AutoDTable.Columns.Add("Desgination");
                AutoDTable.Columns.Add("MachineIDNew");



                AutoDTable1.Columns.Add("SNo");
                AutoDTable1.Columns.Add("Dept");
                AutoDTable1.Columns.Add("Type");
                AutoDTable1.Columns.Add("Shift");
                AutoDTable1.Columns.Add("EmpCode");
                AutoDTable1.Columns.Add("ExCode");
                AutoDTable1.Columns.Add("Name");
                AutoDTable1.Columns.Add("TimeIN");
                AutoDTable1.Columns.Add("TimeOUT");
                AutoDTable1.Columns.Add("MachineID");
                AutoDTable1.Columns.Add("Category");
                AutoDTable1.Columns.Add("SubCategory");
                AutoDTable1.Columns.Add("TotalMIN");
                AutoDTable1.Columns.Add("GrandTOT");
                AutoDTable1.Columns.Add("ShiftDate");
                AutoDTable1.Columns.Add("OTHour");
                AutoDTable1.Columns.Add("CompanyName");
                AutoDTable1.Columns.Add("LocationName");
                AutoDTable1.Columns.Add("Desgination");
                AutoDTable1.Columns.Add("MachineIDNew");


               
                string ng = string.Format(Date, "MM-dd-yyyy");
                Datestr = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
                Datestr1 = Convert.ToDateTime(Date).AddDays(1).ToShortDateString();
                DateTime date1 = Convert.ToDateTime(ng);
                DateTime date2 = date1.AddDays(1);
                

                int mStartINRow = 0;
              
             
                string ss = "";

                ss = "select Distinct EM.MachineID_Encrypt as MachineID from Employee_Mst EM ";
                ss = ss + " Where EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                ss = ss + " And EM.IsActive='yes'";

                mdatatable = objdata.ReturnMultipleValue(ss);

                if (mdatatable.Rows.Count > 0)
                {
                    string MachineID;

                    for (int iRow = 0; iRow < mdatatable.Rows.Count; iRow++)
                    {
                        Boolean chkduplicate = false;
                        chkduplicate = false;

                        for (int ia = 0; ia < AutoDTable.Rows.Count; ia++)
                        {
                            string id = mdatatable.Rows[iRow]["MachineID"].ToString();

                            if (id == AutoDTable.Rows[ia][19].ToString())
                            {
                                chkduplicate = true;
                            }
                        }
                        if (chkduplicate == false)
                        {
                            AutoDTable.NewRow();
                            AutoDTable.Rows.Add();
   
                            AutoDTable.Rows[mStartINRow][16] = SessionCompanyName.ToString();
                            AutoDTable.Rows[mStartINRow][17] = SessionLocationName.ToString();
                            AutoDTable.Rows[mStartINRow][14] = date1.ToString("dd/MM/yyyy");
                       
                            MachineID = mdatatable.Rows[iRow]["MachineID"].ToString();

                            ss = UTF8Decryption(MachineID.ToString());
   
                            AutoDTable.Rows[mStartINRow][9] = ss.ToString();

                            AutoDTable.Rows[mStartINRow][19] = MachineID.ToString();
                            mStartINRow += 1;
                        }
                    }
                }

                DataTable nnDatatable = new DataTable();



                string InMachine_IP;
                DataTable mLocalDS_out = new DataTable();

                for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
                {
                    string mach = AutoDTable.Rows[iRow2][19].ToString();

                    InMachine_IP = mach.ToString();



                    Time_IN_Str = "";
                    Time_OUT_Str = "";
                    Date_Value_Str = string.Format(Date, "yyyy/MM/dd");


                    SSQL = "Select TimeIN from LogTime_Lunch where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeIN ASC";
                    mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);



                    String Emp_Total_Work_Time_1 = "00:00";
                    if (mLocalDS_INTAB.Rows.Count >= 2)
                    {
                        //for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                        //{
                            Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                            Time_OUT_Str = mLocalDS_INTAB.Rows[1][0].ToString();

                            AutoDTable.Rows[iRow2][7] = String.Format("{0:hh:mm tt}", mLocalDS_INTAB.Rows[0][0]);
                            AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", mLocalDS_INTAB.Rows[1][0]);
                          


                            TimeSpan ts4;
                            ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;


                            if (Time_IN_Str == "" || Time_OUT_Str == "")
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date3 = Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = Convert.ToDateTime(Time_OUT_Str);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }

                                }
                                else
                                {
                                    ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                            }

                            AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;
                            AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                            OThour_Str = Emp_Total_Work_Time_1.Split(':');
                            OT_Hour = Convert.ToInt32(OThour_Str[0]);
                            OT_Min = Convert.ToInt32(OThour_Str[1]);
                            if (OT_Hour >= 9)
                            {
                                OT_Time = OT_Hour - 8;
                                OT_Hour1 = Convert.ToString(OT_Time) + " : " + Convert.ToString(OT_Min);
                                AutoDTable.Rows[iRow2][15] = OT_Hour1;
                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][15] = "00:00";
                            }

                        //}
                    }
                    else
                    {
                        TimeSpan ts4;
                        ts4 = Convert.ToDateTime(String.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS_INTAB.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                            Time_OUT_Str = "";

                            AutoDTable.Rows[iRow2][7] = "00:00";
                            AutoDTable.Rows[iRow2][8] = "00:00";
                          

                        }
                        else if (mLocalDS_INTAB.Rows.Count >= 1)
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                            Time_OUT_Str = "";

                            AutoDTable.Rows[iRow2][7] = string.Format("{0:hh:mm tt}", mLocalDS_INTAB.Rows[0][0]);
                            AutoDTable.Rows[iRow2][8] = "00:00";

                        }

                        else
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();


                            Time_OUT_Str = mLocalDS_INTAB.Rows[1][0].ToString();

                            AutoDTable.Rows[iRow2][7] = String.Format("{0:hh:mm tt}", mLocalDS_INTAB.Rows[0][0]);
                            AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", mLocalDS_INTAB.Rows[1][0]);
                          

                        }


                        if (Time_IN_Str == "" || Time_OUT_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4 = System.Convert.ToDateTime(Time_OUT_Str);
                            TimeSpan ts1;
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                            }
                            else
                            {
                                ts4 = ts4.Add(ts1);
                                string s1 = ts4.Minutes.ToString();
                                if (s1.Length == 1)
                                {
                                    string ts_str = "0" + ts4.Minutes;
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts_str;
                                }
                                else
                                {
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                }
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }

                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                            }
                            AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;

                            AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                            OThour_Str = Emp_Total_Work_Time_1.Split(':');
                            OT_Hour = Convert.ToInt32(OThour_Str[0]);
                            OT_Min = Convert.ToInt32(OThour_Str[1]);
                            if (OT_Hour >= 9)
                            {
                                OT_Time = OT_Hour - 8;
                                OT_Hour1 = Convert.ToString(OT_Time) + " : " + Convert.ToString(OT_Min);
                                AutoDTable.Rows[iRow2][15] = OT_Hour1;
                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][15] = "00:00";
                            }


                        }

                    }
                    // Bellow codings are correct:
                    
                }
                for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
                {
                    string MID = AutoDTable.Rows[iRow2][19].ToString();

                 
                    SSQL = "";
                    SSQL = "select Distinct isnull(MachineID_Encrypt,'') as [MachineID],isnull(DeptName,'') as [DeptName]";
                    SSQL = SSQL + ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
                    SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
                    SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName],isnull(Designation,'') as Desgination";
                    SSQL = SSQL + " from Employee_Mst  Where MachineID_Encrypt='" + MID + "' And Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'" + " order by ExistingCode Asc";

                    mEmployeeDS = objdata.ReturnMultipleValue(SSQL);

                    if (mEmployeeDS.Rows.Count > 0)
                    {
                        for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                        {

                            ss = UTF8Decryption(MID.ToString());
                            AutoDTable.Rows[iRow2][19] = ss.ToString();
                            string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();



                            AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                            AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
                            AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                            AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                            AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                            AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                            AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                            AutoDTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");
                            AutoDTable.Rows[iRow2][16] = SessionCompanyName.ToString();
                            AutoDTable.Rows[iRow2][17] = SessionLocationName.ToString();
                            AutoDTable.Rows[iRow2][18] = mEmployeeDS.Rows[iRow1]["Desgination"];


                        }
                    }
                }


             
                for (int i = 0; i < AutoDTable.Rows.Count; i++)
                {
                    if (AutoDTable.Rows[i]["TimeIN"].ToString() != "00:00" && AutoDTable.Rows[i]["TimeOUT"].ToString() != "00:00")
                    {
                        AutoDTable1.NewRow();
                        AutoDTable1.Rows.Add();

                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["Dept"] = AutoDTable.Rows[i]["Dept"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["Type"] = AutoDTable.Rows[i]["Type"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["EmpCode"] = AutoDTable.Rows[i]["EmpCode"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["ExCode"] = AutoDTable.Rows[i]["ExCode"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["Name"] = AutoDTable.Rows[i]["Name"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["Category"] = AutoDTable.Rows[i]["Category"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["SubCategory"] = AutoDTable.Rows[i]["SubCategory"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["TotalMIN"] = AutoDTable.Rows[i]["TotalMIN"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["GrandTOT"] = AutoDTable.Rows[i]["GrandTOT"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["ShiftDate"] = AutoDTable.Rows[i]["ShiftDate"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["OTHour"] = AutoDTable.Rows[i]["OTHour"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["CompanyName"] = AutoDTable.Rows[i]["CompanyName"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["LocationName"] = AutoDTable.Rows[i]["LocationName"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["Desgination"] = AutoDTable.Rows[i]["Desgination"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["MachineIDNew"] = AutoDTable.Rows[i]["MachineIDNew"].ToString();


                    }


   
                }



                ds.Tables.Add(AutoDTable1);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/BreakTimeReport.rpt"));
                report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                CrystalReportViewer1.ReportSource = report;
            }
        }
    }

    public void NonAdminGetAttdDayWise_Change()
    {
        DataSet ds = new DataSet();
        DataTable AutoDTable = new DataTable();


        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Type");
        AutoDTable.Columns.Add("Shift");
        AutoDTable.Columns.Add("EmpCode");
        AutoDTable.Columns.Add("ExCode");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("TimeIN");
        AutoDTable.Columns.Add("TimeOUT");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("Category");
        AutoDTable.Columns.Add("SubCategory");
        AutoDTable.Columns.Add("TotalMIN");
        AutoDTable.Columns.Add("GrandTOT");
        AutoDTable.Columns.Add("ShiftDate");
        AutoDTable.Columns.Add("OTHour");
        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
        AutoDTable.Columns.Add("Desgination");
        AutoDTable.Columns.Add("MachineIDNew");



        AutoDTable1.Columns.Add("SNo");
        AutoDTable1.Columns.Add("Dept");
        AutoDTable1.Columns.Add("Type");
        AutoDTable1.Columns.Add("Shift");
        AutoDTable1.Columns.Add("EmpCode");
        AutoDTable1.Columns.Add("ExCode");
        AutoDTable1.Columns.Add("Name");
        AutoDTable1.Columns.Add("TimeIN");
        AutoDTable1.Columns.Add("TimeOUT");
        AutoDTable1.Columns.Add("MachineID");
        AutoDTable1.Columns.Add("Category");
        AutoDTable1.Columns.Add("SubCategory");
        AutoDTable1.Columns.Add("TotalMIN");
        AutoDTable1.Columns.Add("GrandTOT");
        AutoDTable1.Columns.Add("ShiftDate");
        AutoDTable1.Columns.Add("OTHour");
        AutoDTable1.Columns.Add("CompanyName");
        AutoDTable1.Columns.Add("LocationName");
        AutoDTable1.Columns.Add("Desgination");
        AutoDTable1.Columns.Add("MachineIDNew");
        
            string ng = string.Format(Date, "MM-dd-yyyy");
            Datestr = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
            Datestr1 = Convert.ToDateTime(Date).AddDays(1).ToShortDateString();
            DateTime date1 = Convert.ToDateTime(ng);
            DateTime date2 = date1.AddDays(1);

            int mStartINRow = 0;
          
            string ss = "";

                ss = "select Distinct EM.MachineID_Encrypt as MachineID Employee_Mst EM ";
                ss = ss + " Where EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                ss = ss + " And EM.IsActive='yes' and EM.IsNonAdmin='1'";

                mdatatable = objdata.ReturnMultipleValue(ss);

                if (mdatatable.Rows.Count > 0)
                {
                    string MachineID;

                    for (int iRow = 0; iRow < mdatatable.Rows.Count; iRow++)
                    {
                        Boolean chkduplicate = false;
                        chkduplicate = false;

                        for (int ia = 0; ia < AutoDTable.Rows.Count; ia++)
                        {
                            string id = mdatatable.Rows[iRow]["MachineID"].ToString();

                            if (id == AutoDTable.Rows[ia][19].ToString())
                            {
                                chkduplicate = true;
                            }
                        }
                        if (chkduplicate == false)
                        {
                            AutoDTable.NewRow();
                            AutoDTable.Rows.Add();
                          
                            AutoDTable.Rows[mStartINRow][16] = SessionCompanyName.ToString();
                            AutoDTable.Rows[mStartINRow][17] = SessionLocationName.ToString();
                            AutoDTable.Rows[mStartINRow][14] = date1.ToString("dd/MM/yyyy");

                            MachineID = mdatatable.Rows[iRow]["MachineID"].ToString();

                            ss = UTF8Decryption(MachineID.ToString());

                            AutoDTable.Rows[mStartINRow][9] = ss.ToString();

                            AutoDTable.Rows[mStartINRow][19] = MachineID.ToString();
                            mStartINRow += 1;
                        }
                    }
                }

                DataTable nnDatatable = new DataTable();

                string InMachine_IP;
                DataTable mLocalDS_out = new DataTable();

                for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
                {
                    string mach = AutoDTable.Rows[iRow2][19].ToString();

                    InMachine_IP = mach.ToString();
                  
                    Time_IN_Str = "";
                    Time_OUT_Str = "";
                    Date_Value_Str = string.Format(Date, "yyyy/MM/dd");


                    SSQL = "Select TimeIN from LogTime_Lunch where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeIN ASC";
                    mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);
                   

                   
                    String Emp_Total_Work_Time_1 = "00:00";
                    if (mLocalDS_INTAB.Rows.Count >= 2)
                    {
                        //for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                        //{
                            Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();


                            Time_OUT_Str = mLocalDS_INTAB.Rows[1][0].ToString();

                            AutoDTable.Rows[iRow2][7] = String.Format("{0:hh:mm tt}", mLocalDS_INTAB.Rows[0][0]);
                            AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", mLocalDS_INTAB.Rows[1][0]);
                          

                            TimeSpan ts4;
                            ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;
                            

                            if (Time_IN_Str == "" || Time_OUT_Str == "")
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date3 = Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = Convert.ToDateTime(Time_OUT_Str);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }

                                }
                                else
                                {
                                    ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                            }

                            AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;
                            AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                            OThour_Str = Emp_Total_Work_Time_1.Split(':');
                            OT_Hour = Convert.ToInt32(OThour_Str[0]);
                            OT_Min = Convert.ToInt32(OThour_Str[1]);
                            if (OT_Hour >= 9)
                            {
                                OT_Time = OT_Hour - 8;
                                OT_Hour1 = Convert.ToString(OT_Time) + " : " + Convert.ToString(OT_Min);
                                AutoDTable.Rows[iRow2][15] = OT_Hour1;
                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][15] = "00:00";
                            }

                        //}
                    }
                    else
                    {
                        TimeSpan ts4;
                        ts4 = Convert.ToDateTime(String.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS_INTAB.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                            Time_OUT_Str = "";
                            AutoDTable.Rows[iRow2][7] = "00:00";
                            AutoDTable.Rows[iRow2][8] = "00:00";
                          
                        }
                        else if (mLocalDS_INTAB.Rows.Count >= 1)
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                            Time_OUT_Str = "";

                            AutoDTable.Rows[iRow2][7] = String.Format("{0:hh:mm tt}", mLocalDS_INTAB.Rows[0][0]);
                            AutoDTable.Rows[iRow2][8] = "00:00";
                          
                        }
                        
                            else
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                                Time_OUT_Str = mLocalDS_INTAB.Rows[1][0].ToString();

                                AutoDTable.Rows[iRow2][7] = String.Format("{0:hh:mm tt}", mLocalDS_INTAB.Rows[0][0]);
                                AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", mLocalDS_INTAB.Rows[1][0]);
                          
                            }

                       
                        if (Time_IN_Str == "" || Time_OUT_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4 = System.Convert.ToDateTime(Time_OUT_Str);
                            TimeSpan ts1;
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                            }
                            else
                            {
                                ts4 = ts4.Add(ts1);
                                string s1 = ts4.Minutes.ToString();
                                if (s1.Length == 1)
                                {
                                    string ts_str = "0" + ts4.Minutes;
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts_str;
                                }
                                else
                                {
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                }
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }

                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                            }
                            AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;

                            AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                            OThour_Str = Emp_Total_Work_Time_1.Split(':');
                            OT_Hour = Convert.ToInt32(OThour_Str[0]);
                            OT_Min = Convert.ToInt32(OThour_Str[1]);
                            if (OT_Hour >= 9)
                            {
                                OT_Time = OT_Hour - 8;
                                OT_Hour1 = Convert.ToString(OT_Time) + " : " + Convert.ToString(OT_Min);
                                AutoDTable.Rows[iRow2][15] = OT_Hour1;
                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][15] = "00:00";
                            }


                        }

                    }
                    // Bellow codings are correct:
//
                }
                for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
                {
                    string MID = AutoDTable.Rows[iRow2][19].ToString();

                  
                    SSQL = "";
                    SSQL = "select Distinct isnull(MachineID_Encrypt,'') as [MachineID],isnull(DeptName,'') as [DeptName]";
                    SSQL = SSQL + ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
                    SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
                    SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName],isnull(Designation,'') as Desgination";
                    SSQL = SSQL + " from Employee_Mst  Where MachineID_Encrypt='" + MID + "' And Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'" + " order by ExistingCode Asc";

                    mEmployeeDS = objdata.ReturnMultipleValue(SSQL);

                    if (mEmployeeDS.Rows.Count > 0)
                    {
                        for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                        {

                            ss = UTF8Decryption(MID.ToString());
                            AutoDTable.Rows[iRow2][19] = ss.ToString();
                            string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();

                           
                            AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                            AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
                            AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                            AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                            AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                            AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                            AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                            AutoDTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");
                            AutoDTable.Rows[iRow2][16] = SessionCompanyName.ToString();
                            AutoDTable.Rows[iRow2][17] = SessionLocationName.ToString();
                            AutoDTable.Rows[iRow2][18] = mEmployeeDS.Rows[iRow1]["Desgination"];

                          
                        }
                    }
                }


                for (int i = 0; i < AutoDTable.Rows.Count; i++)
                {
                    if (AutoDTable.Rows[i]["TimeIN"].ToString() != "00:00" && AutoDTable.Rows[i]["TimeOUT"].ToString() != "00:00")
                    {
                        AutoDTable1.NewRow();
                        AutoDTable1.Rows.Add();

                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["Dept"] = AutoDTable.Rows[i]["Dept"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["Type"] = AutoDTable.Rows[i]["Type"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["EmpCode"] = AutoDTable.Rows[i]["EmpCode"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["ExCode"] = AutoDTable.Rows[i]["ExCode"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["Name"] = AutoDTable.Rows[i]["Name"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["Category"] = AutoDTable.Rows[i]["Category"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["SubCategory"] = AutoDTable.Rows[i]["SubCategory"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["TotalMIN"] = AutoDTable.Rows[i]["TotalMIN"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["GrandTOT"] = AutoDTable.Rows[i]["GrandTOT"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["ShiftDate"] = AutoDTable.Rows[i]["ShiftDate"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["OTHour"] = AutoDTable.Rows[i]["OTHour"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["CompanyName"] = AutoDTable.Rows[i]["CompanyName"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["LocationName"] = AutoDTable.Rows[i]["LocationName"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["Desgination"] = AutoDTable.Rows[i]["Desgination"].ToString();
                        AutoDTable1.Rows[AutoDTable1.Rows.Count - 1]["MachineIDNew"] = AutoDTable.Rows[i]["MachineIDNew"].ToString();


                    }
                }
                
       
        ds.Tables.Add(AutoDTable1);
        ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("crystal/BreakTimeReport.rpt"));
        report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
        report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        CrystalReportViewer1.ReportSource = report;


    }


    public static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    public static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }
}
