﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class LateINNew : System.Web.UI.Page
{

    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();

    DateTime ShiftdateStartIN;
    DateTime ShiftdateEndIN;
    DateTime InTime_Check;
    DateTime InToTime_Check;
    DateTime EmpdateIN;

    DataTable mEmployeeDS = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable DS_InTime = new DataTable();
    DataTable DS_Time = new DataTable();
    string SSQL = "";
    string Final_InTime;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string Time_IN_Str;
    string Time_OUT_Str;
    string Date_Value_Str;
    string Shift_Start_Time;
    string Shift_End_Time;
    string Final_Shift;
    string Total_Time_get;
    string[] Time_Minus_Value_Check;
    string[] OThour_Str;
    DataSet ds = new DataSet();
    int time_Check_dbl;
    DataTable mdatatable = new DataTable();
    DataTable LocalDT = new DataTable();
    DataTable Shift_DS = new DataTable();
    TimeSpan InTime_TimeSpan;
    string From_Time_Str = "";
    string To_Time_Str = "";
    int OT_Hour;
    int OT_Min;
    int OT_Time;
    string OT_Hour1 = "";
    string mUser = "";

    Boolean Shift_Check_blb = false;
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    public string Right_Val(string Value, int Length)
    {

        int i;
        i = 0;
        if (Value.Length >= Length)
        {
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }


    DateTime date1 = new DateTime();
    DataTable table = new DataTable();
    DataTable AutoDTable = new DataTable();
    DataTable mDataSet = new DataTable();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "ERP Stores Module :: Spay Module | Report-LateIn";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();


            //ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            ddlShiftType = Request.QueryString["ShiftType1"].ToString();

            date1 = Convert.ToDateTime(Date);


            GetAttdTable_MisMatchShift();

            ds.Tables.Add(table);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/latein.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            CrystalReportViewer1.ReportSource = report;

        }

    }




    public void GetAttdTable_MisMatchShift()
    {

        GetAttdDayWise_MisMatchShiftReport();

       

        table.Columns.Add("CompanyName");
        table.Columns.Add("LocationName");
        table.Columns.Add("ShiftDate");
        table.Columns.Add("SNo");
        table.Columns.Add("Dept");
        table.Columns.Add("Type");
        table.Columns.Add("Shift");
        table.Columns.Add("Category");
        table.Columns.Add("SubCategory");
        table.Columns.Add("EmpCode");
        table.Columns.Add("ExCode");
        table.Columns.Add("Name");
        table.Columns.Add("TimeIN");
        table.Columns.Add("TimeOUT");
        table.Columns.Add("MachineID");
        table.Columns.Add("PrepBy");
        table.Columns.Add("PrepDate");
        table.Columns.Add("TotalMIN");
        table.Columns.Add("GrandTOT");



        DataRow dtRow = null;

        for (int iRow = 0; iRow <AutoDTable.Rows.Count; iRow++)
        {

            table.NewRow();
            table.Rows.Add();


            table.Rows[iRow]["CompanyName"]= SessionCcode;
            table.Rows[iRow]["LocationName"] = SessionLcode;
            table.Rows[iRow]["ShiftDate"] = Date;
           
            table.Rows[iRow]["Dept"] = AutoDTable.Rows[iRow][1];
            table.Rows[iRow]["Type"] = AutoDTable.Rows[iRow][2];
            table.Rows[iRow]["Shift"] = AutoDTable.Rows[iRow][3];
            table.Rows[iRow]["Category"] = AutoDTable.Rows[iRow][10];
            table.Rows[iRow]["SubCategory"] = AutoDTable.Rows[iRow][11];
            table.Rows[iRow]["EmpCode"] = AutoDTable.Rows[iRow][4];
            table.Rows[iRow]["ExCode"] = AutoDTable.Rows[iRow][5];
            table.Rows[iRow]["Name"] = AutoDTable.Rows[iRow][6];
            if (!string.IsNullOrEmpty(AutoDTable.Rows[iRow][7].ToString()))
            {
                table.Rows[iRow]["TimeIN"] = string.Format("{0:hh:mm tt}", Convert.ToDateTime(AutoDTable.Rows[iRow][ 7]));
            }
            else
            {
                table.Rows[iRow]["TimeIN"] = string.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow][7]);
            }
            if (!string.IsNullOrEmpty(AutoDTable.Rows[iRow][8].ToString()))
            {
                table.Rows[iRow]["TimeOUT"] = string.Format("{0:hh:mm tt}", Convert.ToDateTime(AutoDTable.Rows[iRow][8]));
            }
            else
            {
                table.Rows[iRow]["TimeOUT"] = string.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow][8]);
            }

            table.Rows[iRow]["MachineID"] = AutoDTable.Rows[iRow][9];
            table.Rows[iRow]["PrepBy"] = "User";
            table.Rows[iRow]["PrepDate"] = Date;

            table.Rows[iRow]["TotalMIN"] = AutoDTable.Rows[iRow][12];
            table.Rows[iRow]["GrandTOT"] = AutoDTable.Rows[iRow][12];

          
        }

        
    }

   


    public void GetAttdDayWise_MisMatchShiftReport()
    {
        DataTable mLocalDS_INTAB = new DataTable();
        DataTable mLocalDS_OUTTAB = new DataTable();
        string Time_IN_Str = "";
        string Time_Out_Str = "";
        Int32 time_Check_dbl = 0;
        string Total_Time_get = "";

        DataTable Payroll_DS = new DataTable();


        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Type");
        AutoDTable.Columns.Add("Shift");
        AutoDTable.Columns.Add("EmpCode");
        AutoDTable.Columns.Add("Ex.Code");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("TimeIN");
        AutoDTable.Columns.Add("TimeOUT");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("Category");
        AutoDTable.Columns.Add("SubCategory");
        AutoDTable.Columns.Add("TotalMIN");
        AutoDTable.Columns.Add("GrandTOT");




        SSQL = "";
        SSQL = "Select IPAddress,IPMode from IPAddress_Mst Where Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";

        mDataSet = objdata.ReturnMultipleValue(SSQL);


        if (mDataSet.Rows.Count == 0)
        {
            return;
        }


        if (mDataSet.Rows.Count > 0)
        {
            for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
            {
                if ((mDataSet.Rows[iRow]["IPMode"].ToString()) == "IN")
                {
                    mIpAddress_IN = (mDataSet.Rows[iRow]["IPAddress"].ToString());
                }
                else if ((mDataSet.Rows[iRow]["IPMode"].ToString()) == "OUT")
                {
                    mIpAddress_OUT = (mDataSet.Rows[iRow]["IPAddress"].ToString());
                }
            }
        }

          DataTable mLocalDS = new DataTable();

       
            SSQL = "";
            SSQL = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
            SSQL = SSQL + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
            SSQL += " from Shift_Mst Where CompCode='" + SessionCcode + "'";
            SSQL += " And LocCode='" + SessionLcode + "'";
            if (ShiftType1 != "ALL")
            {
                SSQL += " And shiftDesc='" + ShiftType1 + "'";
            }
            else
            {
                SSQL += " And ShiftDesc like '" + ddlShiftType + "%'";
                //Shift%'"
            }
            SSQL += " Order By shiftDesc";

      
        string ShiftType = "";

        mLocalDS = objdata.ReturnMultipleValue(SSQL);

        if (SessionLcode == "UNIT I")
        {
            for (int i = 0; i < mLocalDS.Rows.Count; i++)
            {
                if (mLocalDS.Rows[i]["shiftDesc"].ToString() == "SHIFT10")
                {
                    mLocalDS.Rows[i]["StartIN_Days"] = -1;
                    mLocalDS.Rows[i]["EndIN_Days"] = 0;
                }
            }
        }

        if (mLocalDS.Rows.Count < 0)
            return;


        int mStartINRow = 0;
        int mStartOUTRow = 0;

        //'No Shift (Shift Timing Not Match Employee Add
        NoShift_Add_All_Shift_Wise_GetAttdDayWise();

        DataTable mEmployeeDS = new DataTable();



        SSQL = "";
        SSQL = "select Distinct isnull(EM.MachineID_Encrypt,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
        SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
        SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
        SSQL = SSQL + ",isnull(EM.CatName,'') as [CatName], isnull(EM.SubCatName,'') as [SubCatName]";
        SSQL = SSQL + ",DM.DeptCode from Employee_Mst EM inner Join Department_Mst DM on DM.DeptName=EM.DeptName Where EM.Compcode='" + SessionCcode + "'";
        //if (SessionUserType == "2")
        //{
        //    SSQL = SSQL + " And EM.IsNonAdmin='1'";
        //}
        
        SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'" + " order by DM.DeptCode Asc";

        mEmployeeDS = objdata.ReturnMultipleValue(SSQL);





        if (mEmployeeDS.Rows.Count > 0)
        {

            for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
            {
                for (int iRow2 = 1; iRow2 < AutoDTable.Rows.Count; iRow2++)
                {
                    if (AutoDTable.Rows[iRow2][9].ToString() == mEmployeeDS.Rows[iRow1]["MachineID"].ToString())
                    {
                        AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"].ToString();
                        AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"].ToString();
                        AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"].ToString();
                        AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"].ToString();
                        AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"].ToString();
                        AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"].ToString();
                        AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"].ToString();

                    }
                    else
                    {
                    }
                }
            }

        }



    }

  


    public void NoShift_Add_All_Shift_Wise_GetAttdDayWise()
    {
        DataTable mLocalDS_INTAB = new DataTable();
        DataTable mLocalDS_OUTTAB = new DataTable();
        string Date_Value_Str = null;
        string Time_IN_Str = "";
        string Time_Out_Str = "";
        Int32 time_Check_dbl = 0;
        string Total_Time_get = "";

        SSQL = "";
        SSQL = "Select IPAddress,IPMode from IPAddress_Mst Where Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";

        mDataSet = objdata.ReturnMultipleValue(SSQL);

        if (mDataSet.Rows.Count == 0)
        {
            //MessageBox.Show("No Data Please Contact Your Administrator.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            return;
        }


        if (mDataSet.Rows.Count > 0)
        {
            for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
            {
                if (mDataSet.Rows[iRow]["IPMode"].ToString() == "IN")
                {
                    mIpAddress_IN = mDataSet.Rows[iRow]["IPAddress"].ToString();
                }
                else if ((mDataSet.Rows[iRow]["IPMode"].ToString()) == "OUT")
                {
                    mIpAddress_OUT = mDataSet.Rows[iRow]["IPAddress"].ToString();
                }
            }
        }



        DataTable mLocalDS = new DataTable();

        SSQL = "";
        SSQL = "Select Distinct A.MachineID from LogTime_IN A,Employee_Mst B where a.Compcode='" + SessionCcode + "' and b.Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " and a.LocCode='" + SessionLcode + "' and b.Loccode='" + SessionLcode + "' and a.MachineID=b.MachineID_Encrypt";
        //if (SessionUserType == "2")
        //{
        //    SSQL = SSQL + " And b.IsNonAdmin='1'";
        //}

        mLocalDS = objdata.ReturnMultipleValue(SSQL);

        if (mLocalDS.Rows.Count < 0)
            return;

        //fg.Rows = 1

        int mStartINRow = 0;
        int mStartOUTRow = 0;

        string Time_IN_Check_Shift = "";
        string Machine_ID_Check = "";
        DataTable Shift_DataSet = new DataTable();
        DataTable mLocalDS_out = new DataTable();

        for (int iTabRow = 0; iTabRow < mLocalDS.Rows.Count; iTabRow++)
        {
            Machine_ID_Check = mLocalDS.Rows[iTabRow]["MachineID"].ToString();

            if (Machine_ID_Check == "EmlD5+KFvYcPQ+mv4hR32w==")
            {
                string sdds = "";
            }
            SSQL = "Select MachineID,TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IPAddress='" + mIpAddress_IN + "'";
            SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
            SSQL = SSQL + " And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "' Order by TimeIN asc";
            mDataSet = objdata.ReturnMultipleValue(SSQL);

            if (mDataSet.Rows.Count <= 0)
            {
                //Skip
            }
            else
            {
                string Time_IN_str = string.Format("{0:hh:mm tt}", mDataSet.Rows[0]["TimeIN"].ToString());
                DateTime Time_IN = Convert.ToDateTime(Time_IN_str);

                Time_IN_Check_Shift = Time_IN.ToString("HH:mm:ss");
                //TimeIN Check With Shift Master Table
                SSQL = "";
                SSQL = "Select * from Shift_Mst where StartIN < '" + Time_IN_Check_Shift + "' And EndIN > '";
                SSQL = SSQL + Time_IN_Check_Shift + "'";

                Shift_DataSet = objdata.ReturnMultipleValue(SSQL);

              
                if (Shift_DataSet.Rows.Count <= 0)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    //Time IN Add
                    AutoDTable.Rows[AutoDTable.Rows.Count-1][3]="LATE-IN";
                    //fg.set_TextMatrix(mStartINRow, 7, String.Format("{0:hh:mm tt}", (.Rows(iRow)("TimeIN"))))
                     AutoDTable.Rows[AutoDTable.Rows.Count-1][7]= Time_IN_Check_Shift;
                    AutoDTable.Rows[AutoDTable.Rows.Count-1][9]=Machine_ID_Check;

                    //Time OUT Add
                    SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IPAddress='" + mIpAddress_OUT + "'";
                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                    SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Desc";
                    mLocalDS_out = objdata.ReturnMultipleValue(SSQL);
                    if (mLocalDS_out.Rows.Count <= 0)
                    {
                        //Skip
                    }
                    else
                    {
                        if (AutoDTable.Rows[AutoDTable.Rows.Count - 1][9] == mLocalDS_out.Rows[0][0])
                        {
                            AutoDTable.Rows[AutoDTable.Rows.Count - 1][8]= string.Format("{0:hh:mm tt}", (mLocalDS_out.Rows[0][1]));
                            //GoTo 1
                        }
                    }
                    //Grand Total value Display
                    Time_IN_Str = "";
                    Time_Out_Str = "";
                    //If fg.get_TextMatrix(iRow2, 9) = "257" Then
                    //    MessageBox.Show("" & fg.get_TextMatrix(iRow2, 9))
                    //End If

                    Date_Value_Str = date1.ToString("yyyy/MM/dd");
                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "'";
                    SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                    mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Check + "'";
                    SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Desc";
                    mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);

                    string Emp_Total_Work_Time_1 = "00:00";
                    if (mLocalDS_INTAB.Rows.Count > 1)
                    {
                        for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

                            if (mLocalDS_OUTTAB.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_Out_Str = "";
                            }
                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                            }
                            //Emp_Total_Work_Time
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);

                                TimeSpan ts1 = new TimeSpan();
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)

                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get =ts1.Hours.ToString();
                                    //& ":" & Trim(ts.Minutes)
                                    time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1")
                                        Emp_Total_Work_Time_1 = "00:00";
                                }
                                else
                                {
                                    ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1")
                                        Emp_Total_Work_Time_1 = "00:00";
                                    time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                }
                            }
                            AutoDTable.Rows[tin][12]= Emp_Total_Work_Time_1;
                        }
                    }
                    else
                    {
                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS_INTAB.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout <= mLocalDS_OUTTAB.Rows.Count; tout++)
                        {
                            if (mLocalDS_OUTTAB.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";
                            }
                            else
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                            }

                        }
                        //Emp_Total_Work_Time
                        if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts1 = new TimeSpan();
                            ts1 = date4.Subtract(date3);
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();
                            //& ":" & Trim(ts.Minutes)
                            //ts4 = ts4.Add(ts1)
                            //OT Time Get
                            //Emp_Total_Work_Time_1 = Trim(ts4.Hours) & ":" & Trim(ts4.Minutes)
                            //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                //Emp_Total_Work_Time_1 = Trim(ts1.Hours) & ":" & Trim(ts1.Minutes)
                                Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1")
                                    Emp_Total_Work_Time_1 = "00:00";
                            }
                            else
                            {
                                ts4 = ts4.Add(ts1);
                                Emp_Total_Work_Time_1 = ts4.Hours.ToString() + ":" + ts4.Minutes.ToString();
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1")
                                    Emp_Total_Work_Time_1 = "00:00";
                                time_Check_dbl = Convert.ToInt32(Total_Time_get);
                            }
                            AutoDTable.Rows[AutoDTable.Rows.Count-1][12]= Emp_Total_Work_Time_1;
                        }
                    }

                }
                else
                {
                    //Skip
                }
            }
        }
    }

  
}