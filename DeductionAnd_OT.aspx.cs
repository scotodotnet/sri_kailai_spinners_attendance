﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
public partial class DeductionAnd_OT : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    ManualAttendanceClass objManual = new ManualAttendanceClass();
    bool ErrFlag = false;
    string SessionUserType;
    string SSQL;
    DateTime fromdate;
    DateTime todate;
    Decimal Allow1;
    Decimal Allow2;
    Decimal Allow3;
    Decimal Deduc1;
    Decimal Deduc2;
    Decimal Deduc3;

    Decimal OTHours;
    Decimal Hallowed;
    Decimal Advance;
    Decimal LOP;
    Decimal PTaxDedu;
    Decimal LWF;
    DataTable mDataSet = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionUserType = Session["UserType"].ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Deduction And OT ";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                li.Attributes.Add("class", "droplink active open");
                DropDown_WagesType();
                Financial_Year();
                Dropdown_PayPeriod();
                //DropDown_TokenNumber
            }
        }
    }
    public void DropDown_WagesType()
    {
        DataTable dt = new DataTable();
        dt = objdata.DropDown_WagesType();
        DataRow dr = dt.NewRow();
        dr["WagesTypeName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlWages.Items.Add(dt.Rows[i]["WagesTypeName"].ToString());
            ddlWagestab2.Items.Add(dt.Rows[i]["WagesTypeName"].ToString());
            ddlwagestab3.Items.Add(dt.Rows[i]["WagesTypeName"].ToString());
        }
    }

    public void Financial_Year()
    {
        String CurrentYear1;
        int CurrentYear;
        CurrentYear1 = DateTime.Now.Year.ToString();
        CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());
        if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
        {
            CurrentYear = CurrentYear - 1;
        }
        for (int i = 0; i < 11; i++)
        {
            string tt = (CurrentYear1 + "-" + (CurrentYear - 1));
            ddlFinancialYear.Items.Add(tt.ToString());
            ////ddlFinancialYeartab3.Items.Add(tt.ToString());
            CurrentYear = CurrentYear - 1;
            string cy = Convert.ToString(CurrentYear);
            CurrentYear1 = cy;
        }
    }

   

    public void Dropdown_PayPeriod()
    {
        //DataTable dt = new DataTable();
        //dt = objdata.Dropdown_PayPeriod();
        //DataRow dr = dt.NewRow();
        //dr["PayPeriod_Desc"] = "- select -";
        //dt.Rows.InsertAt(dr, 0);
        //for (int i = 0; i < dt.Rows.Count; i++)
        //{
        //    ddlPayPeriod.Items.Add(dt.Rows[i]["PayPeriod_Desc"].ToString());
        //}
    }

    //public void DropDown_Department()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.Dropdown_Department();
    //    DataRow dr = dt.NewRow();
    //    dr["DeptName"] = "- select -";
    //    dt.Rows.InsertAt(dr, 0);
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        ddlDepartment.Items.Add(dt.Rows[i]["DeptName"].ToString());

           
    //    }
    //}

    //public void Dropdown_Desgination()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.Dropdown_Desgination();
    //    DataRow dr = dt.NewRow();
    //    dr["DesignName"] = "- select -";
    //    dt.Rows.InsertAt(dr, 0);
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        ddlDesignation.Items.Add(dt.Rows[i]["DesignName"].ToString());

    //    }
    //}



    public void DropDown_TokenNumber()
    {
        bool ErrFlag = false;
        DataTable dt = new DataTable();
        string Wages=ddlWages.SelectedItem.Text ;
        dt = objdata.dropdown_TokenNumber_New_SP(SessionCcode, SessionLcode, Wages);
        if (dt.Rows.Count > 0)
        {


            ddlMID.DataSource = dt;
            DataRow dr = dt.NewRow();
            dr["EmpName"] = "- select -";
            dt.Rows.InsertAt(dr, 0);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlMID.Items.Add(dt.Rows[i]["EmpName"].ToString());
            }
        }
    }

    //protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    //{
    //    string id = Convert.ToString(e.CommandArgument);
    //    switch (e.CommandName)
    //    {
    //        case ("Delete"):
    //            DeleteRepeaterData(id);
    //            break;
    //        case ("Edit"):
    //            EditRepeaterData(id);
    //            break;
    //    }
    //}


    //private void EditRepeaterData(string id)
    //{
    //    ss = Convert.ToInt16(id);
    //    DataTable Dtd = new DataTable();
    //    Dtd = objdata.EditUserCreation(ss);
    //    if (Dtd.Rows.Count > 0)
    //    {
    //        String Utype = Dtd.Rows[0]["UserType"].ToString();
    //        if (Utype == "Admin")
    //        {
    //            ddlUserType.SelectedIndex = 1;
    //        }
    //        else if (Utype == "Non-Admin")
    //        {
    //            ddlUserType.SelectedIndex = 2;
    //        }
    //        txtUserName.Text = Dtd.Rows[0]["UserName"].ToString();
    //        txtPassword.Text = Dtd.Rows[0]["Password"].ToString();

    //        CreateUserDisplay();
    //        btnSave.Text = "Update";


    //    }
    //}

  
    protected void btnDeductionAndAllowance_Click(object sender, EventArgs e)
    {

        fromdate = Convert.ToDateTime(txtFromDate.Text);
        todate = Convert.ToDateTime(txtToDate.Text);

        bool ErrFlag = false;

        if (ddlFinancialYear.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Financial Year');", true);
            ErrFlag = true;
        }
        else if (ddlWages.SelectedItem.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Wages');", true);
            ErrFlag = true;
        }
        else
        {

            if (txtAllowance1.Text == "")
            {
                Allow1 = 0;
            }
            else
            {
                Allow1 = Convert.ToDecimal(txtAllowance1.Text);
            }
            if (txtAllowance2.Text == "")
            {
                Allow2 = 0;
            }
            else
            {
                Allow2 = Convert.ToDecimal(txtAllowance2.Text);
            }
            if (txtAllowance3.Text == "")
            {
                Allow3 = 0;
            }
            else
            {
                Allow3 = Convert.ToDecimal(txtAllowance3.Text);
            }
            if (txtDeduction1.Text == "")
            {
                Deduc1 = 0;
            }
            else
            {
                Deduc1 = Convert.ToDecimal(txtDeduction1.Text);
            }
            if (txtDeduction2.Text == "")
            {
                Deduc2 = 0;
            }
            else
            {
                Deduc2 = Convert.ToDecimal(txtDeduction2.Text);
            }
            if (txtDeduction3.Text == "")
            {
                Deduc1 = 0;
            }
            else
            {
                Deduc3 = Convert.ToDecimal(txtDeduction3.Text);
            }
            if (txtH_allowed.Text == "")
            {
                Hallowed = 0;
            }
            else
            {
                Hallowed = Convert.ToDecimal(txtH_allowed.Text);
            }
            if (txtAdvanceAllowance.Text == "")
            {
                Advance = 0;
            }
            else
            {
                Advance = Convert.ToDecimal(txtAdvanceAllowance.Text);
            }
            if (txtLossOfPaydays.Text == "")
            {
                LOP = 0;
            }
            else
            {
                LOP = Convert.ToDecimal(txtLossOfPaydays.Text);
            }
            if (txtPtax_Deduction.Text == "")
            {
                PTaxDedu = 0;
            }
            else
            {
                PTaxDedu = Convert.ToDecimal(txtPtax_Deduction.Text);
            }
            if (txtLabourWellFund.Text == "")
            {
                LWF = 0;
            }
            else
            {
                LWF = Convert.ToDecimal(txtLabourWellFund.Text);
            }

            DataTable dtdEpay = new DataTable();

            SSQL = "Insert Into [ESM-Epay]..eAlert_Deduction_Det(Ccode,Lcode,Month,FinancialYear,Wages,MachineID,ExisistingCode,EmpName,allowances3,";
            SSQL = SSQL + "allowances4,allowances5,Deduction3,Deduction4,DedOthers2,HAllowed,OTHours,Advance,";
            SSQL = SSQL + "FromDate_Str,FromDate,ToDate_Str,ToDate,LOPDays,Advance_Check) Values('" + SessionCcode + "','" + SessionLcode + "','" + ddlMonth.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + ddlFinancialYear.SelectedItem.Text + "','" + ddlWages.SelectedItem.Text + "','" + txtTicketNumberTab1.Text + "','" + txtTicketNumberTab1.Text + "',";
            SSQL = SSQL + "'" + txtName.Text + "','" + Allow1 + "','" + Allow2 + "','" + Allow3 + "','" + Deduc1 + "','" + Deduc2 + "',";
            SSQL = SSQL + "'" + Deduc3 + "','" + Hallowed + "','" + OTHours + "',";
            SSQL = SSQL + "'" + Advance + "','" + txtFromDate.Text + "',";
            SSQL = SSQL + "'" + fromdate.ToString("yyyy/MM/dd") + "','" + txtToDate.Text + "',";
            SSQL = SSQL + "'" + todate.ToString("yyyy/MM/dd") + "','" + LOP + "','" + Advance + "')";

            dtdEpay = objdata.ReturnMultipleValue(SSQL);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deduction Details Saved Successfully in Payroll');", true);
            ErrFlag = true;

            if (OTHours != 0 || Hallowed != 0)
            {
                //Get Payroll EmpNo
                string Epay_EmpNo_Get = "";
                SSQL = "Select * from [ESM_Epay]..EmployeeDetails where BiometricID='" + txtTicketNumberTab1.Text + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                mDataSet = objdata.ReturnMultipleValue(SSQL);
                if (mDataSet.Rows.Count != 0)
                {
                    Epay_EmpNo_Get = Convert.ToString(mDataSet.Rows[0]["EmpNo"]);
                    //Attn Update
                    SSQL = "Select * from [ESM_Epay]..AttenanceDetails where EmpNo='" + Epay_EmpNo_Get + "' And Months='" + ddlMonth.SelectedItem.Text + "'";
                    SSQL = SSQL + " And FinancialYear='" + ddlFinancialYear.SelectedItem.Text + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FromDate = '" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + "'";
                    SSQL = SSQL + " And ToDate = '" + todate.AddDays(0).ToString("yyyy/MM/dd") + "'";
                    mDataSet = objdata.ReturnMultipleValue(SSQL);
                    if (mDataSet.Rows.Count != 0)
                    {
                        SSQL = "Update [ESM_Epay]..AttenanceDetails set CL='" + Hallowed + "',OTHoursNew='" + OTHours + "'";
                        SSQL = SSQL + " Where EmpNo='" + Epay_EmpNo_Get + "' And Months='" + ddlMonth.SelectedItem.Text + "'";
                        SSQL = SSQL + " And FinancialYear='" + ddlFinancialYear.SelectedItem.Text + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        SSQL = SSQL + " And FromDate = '" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + "'";
                        SSQL = SSQL + " And ToDate = '" + todate.AddDays(0).ToString("yyyy/MM/dd") + "'";
                        mDataSet = objdata.ReturnMultipleValue(SSQL);

                    }
                  
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Biometric Id Not Available In Payroll');", true);
                ErrFlag = true;

            }

        }    

    }

    protected void btnSavetAllowancedDeduction_Click(object sender, EventArgs e)
    {
    }

    protected void btnSavetab1_Click(object sender, EventArgs e)
    {

       
    }
    protected void btnCleartab1_Click(object sender, EventArgs e)
    {
        txtName.Text = "";
        txtTicketNumberTab1.Text = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        ddlMonth.SelectedIndex = 1;
        ddlFinancialYear.SelectedIndex = 1;
        ddlWages.SelectedIndex = 1;
        txtAdvanceAllowance.Text = "";
        txtOT_hours.Text = "";
        txtPtax_Deduction.Text = "";
        txtLossOfPaydays.Text = "";
        txtLabourWellFund.Text = "";
        txtH_allowed.Text = "";
        txtAllowance1.Text = "";
        txtAllowance2.Text = "";
        txtAllowance3.Text = "";
        txtDeduction1.Text = "";
        txtDeduction2.Text = "";
        txtDeduction3.Text = "";
  
    }
    protected void txtToDate_TextChanged(object sender, EventArgs e)
    {
        fromdate = Convert.ToDateTime(txtFromDate.Text);
        todate = Convert.ToDateTime(txtToDate.Text);
        int dayCount = (int)((todate - fromdate).TotalDays);
        string counting = Convert.ToString(dayCount.ToString());
        if (dayCount < 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Give Proper Date');", true);
            ErrFlag = true;
        }

    }

    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }


    protected void btnSavetab2_Click(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void ddlMID_SelectedIndexChanged(object sender, EventArgs e)
    {
        token_NO();
    }


    public void token_NO()
    {
        DataTable dt = new DataTable();
        string s = ddlMID.SelectedItem.Text;
        string[] delimiters = new string[] { "-->" };
        string[] items = s.Split(delimiters, StringSplitOptions.None);
        string ss = items[0];
        string ss1 = items[1];
        //TxtEmpName.Text = ss1.ToString();
        //TxtEmpName.Text = items[1];

        DataTable dted = new DataTable();
        dted = objdata.Manual_Data(ss.ToString());
        if (dted.Rows.Count > 0)
        {
           // txtwagesName.Text = dted.Rows[0]["Wages"].ToString();
            txtTicketNumberTab1.Text = dted.Rows[0]["ExistingCode"].ToString();
            txtName.Text = dted.Rows[0]["FirstName"].ToString();
            txtTicketNumberTab1.Enabled = false;
            txtName.Enabled = false;
 

            //txtMachineNo.Text = dted.Rows[0][""].ToString();
            //TxtEmpName.Text = dted.Rows[0]["FirstName"].ToString();
        }
    }

       
   
    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable Da = new DataTable();
        string SSQL = "";
        SSQL = "Select MachineID,ExistingCode,FirstName from Employee_Mst where Wages='" + ddlWages.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
        Da = objdata.ReturnMultipleValue(SSQL);
    }
    protected void txtTicketNumberTab1_TextChanged(object sender, EventArgs e)
    {
        fromdate = Convert.ToDateTime(txtFromDate.Text);

    }
    protected void ddlWages_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if (ddlWages.SelectedValue == "")
        {

        }
        else
        {
            DropDown_TokenNumber();
        }
        
          

    }
}
