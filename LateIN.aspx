﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LateIN.aspx.cs" Inherits="LateIN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<%@ Register assembly="CrystalDecisions.Web, Version=12.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">



<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        .CalendarCSS
           {
             background-color:White;
            color:Black;
            }
        </style>
</head>
<form id="form1" runat="server">
    
    
     <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                                         ID="ScriptManager1" EnablePartialRendering="true">
                                    </cc1:ToolkitScriptManager>
    <div align="center">
     <asp:Label ID="mReportName" runat="server" Text="LATE IN"></asp:Label>
   
    </div>
    <div>
        <asp:Label ID="EmployeeCode" runat="server" Text="Employee Code"></asp:Label>
        
        <asp:DropDownList ID="ddlEmployeeCode" runat="server">
            <asp:ListItem>ALL</asp:ListItem>
        </asp:DropDownList>
       
       </div>
      
      
      <div>
        <asp:Label ID="Label7" runat="server" Text="Shift"></asp:Label>
         <asp:DropDownList ID="ddlShift" runat="server">
             <asp:ListItem>ALL</asp:ListItem>
             <asp:ListItem>GENERAL</asp:ListItem>
             <asp:ListItem>SHIFT1</asp:ListItem>
             <asp:ListItem>SHIFT2</asp:ListItem>
             <asp:ListItem>SHIFT3</asp:ListItem>
             <asp:ListItem>NO SHIFT</asp:ListItem>
        </asp:DropDownList>
        </div>
      
      
      
      
       <div> 
        <asp:Label ID="Label2" runat="server" Text="Mode"></asp:Label>
        
        <asp:DropDownList ID="ddlMode" runat="server">
             <asp:ListItem>NONE</asp:ListItem>
             <asp:ListItem>IN</asp:ListItem>
             <asp:ListItem>OUT</asp:ListItem>
              <asp:ListItem>IN/OUT</asp:ListItem>
        </asp:DropDownList>
        </div>
        <div>
        <asp:Label ID="Label3" runat="server" Text="ShiftType"></asp:Label>
         <asp:DropDownList ID="ddlshifttype" runat="server">
             <asp:ListItem>SHIFT</asp:ListItem>
             <asp:ListItem>GENERAL</asp:ListItem>
           
             <asp:ListItem>SHIFT1</asp:ListItem>
           
        </asp:DropDownList>
        </div>
        
        
         <div>
        <asp:Label ID="Label13" runat="server" Text="Employee Code"></asp:Label>
        
        <asp:TextBox ID="txtEmpcode" runat="server"></asp:TextBox>
       <%--  <asp:DropDownList ID="ddlEmpCode" runat="server">
             <asp:ListItem>SHIFT</asp:ListItem>
             <asp:ListItem>GENERAL</asp:ListItem>
           
             <asp:ListItem>SHIFT1</asp:ListItem>
           
        </asp:DropDownList>--%>
        </div>
        
        
        
        
        
        
        <div>
        
        <asp:Label ID="Label4" runat="server" Text="Year"></asp:Label>
          <asp:DropDownList ID="ddlYear" runat="server">
             <asp:ListItem>2014</asp:ListItem>
             <asp:ListItem>2015</asp:ListItem>
           
        </asp:DropDownList>
        
        
        </div>
        
        <div>
        <asp:Label ID="Label5" runat="server" Text="Type of certificate"></asp:Label>
         <asp:DropDownList ID="ddlTypeOfCertificate" runat="server">
             <asp:ListItem>Adolescent</asp:ListItem>
             <asp:ListItem>Noise Testing</asp:ListItem>
             <asp:ListItem>Canteen</asp:ListItem>
        </asp:DropDownList>
        </div>
        
        <div>
        <asp:Label ID="Label9" runat="server" Text="Wages Type"></asp:Label>
         <asp:DropDownList ID="ddlwagesType" runat="server">
             <asp:ListItem>STAFF</asp:ListItem>
             <asp:ListItem>SUB-STAFF</asp:ListItem>
             <asp:ListItem>Watch & Ward</asp:ListItem>
             <asp:ListItem>REGULAR</asp:ListItem>
             <asp:ListItem>HOSTEL</asp:ListItem>
             <asp:ListItem>CIVIL</asp:ListItem>
             <asp:ListItem>OTHERS</asp:ListItem>
               
        </asp:DropDownList>
        </div>
        
        
        
          <div>
        <asp:Label ID="Label8" runat="server" Text="Type Of Certificates"></asp:Label>
         <asp:DropDownList ID="cmbWages" runat="server">
             <asp:ListItem>Adolescent</asp:ListItem>
             <asp:ListItem>Noise Testing</asp:ListItem>
             <asp:ListItem>Canteen</asp:ListItem>
        </asp:DropDownList>
        </div>
        <div>
          <asp:Label ID="Label20" runat="server" Text="LeaveDays"></asp:Label>
            <asp:textbox id="txtLeavedays" runat="server"></asp:textbox>
          
        </div>
        
        
        <div>
        
        <asp:Label ID="Label6" runat="server" Text="From Date"></asp:Label>
        <asp:TextBox ID="Txtfrom" runat="server"></asp:TextBox>
        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="Txtfrom" Format ="dd/MM/yyyy"  CssClass ="CalendarCSS" Enabled="true"></cc1:CalendarExtender>
       <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
        TargetControlID="Txtfrom" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" ValidChars="0123456789-/">
        </cc1:FilteredTextBoxExtender>
        
        </div>
        <div>
        <asp:Label ID="Label1" runat="server" Text="To Date"></asp:Label>
        <asp:TextBox ID="TxtTo" runat="server"></asp:TextBox>
        
          <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtTo" Format ="dd/MM/yyyy"  CssClass ="CalendarCSS" Enabled="true"></cc1:CalendarExtender>
       <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        TargetControlID="TxtTo" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" ValidChars="0123456789-/">
        </cc1:FilteredTextBoxExtender>
        
       </div>
       
       <div>
           <asp:Button ID="btnNext" runat="server" Text="Report" onclick="btnNext_Click" />
       </div>
        
         <div>
           <asp:Button ID="btnNext1" runat="server" Text="Day Attendance Summary" 
                 onclick="btnNext1_Click" />
       </div>
        
        <div>
           <asp:Button ID="btnNext2" runat="server" Text="EMPLOYEE MASTER" 
                onclick="btnNext2_Click"/>
       </div>
        
         <div>
           <asp:Button ID="btnNext3" runat="server" Text="EMPLOYEE PROFILE" 
                onclick="btnNext3_Click"/>
       </div>
        
        
         <div>
           <asp:Button ID="btnNext4" runat="server" Text="EMPLOYEE FULL PROFILE" 
                onclick="btnNext4_Click"/>
       </div>
        
        <div>
        <asp:button ID="btnEarlyOUT" runat="server" text="EarlyOUT" onclick="Unnamed1_Click" />
        
        </div>
        
        <div>
        
<asp:button ID="btnDayShiftWise" runat="server" text="DayAttendanceShiftWise" 
                onclick="btnDayShiftWise_Click" />
</div>
        <div>
            <asp:button id="btnLongAbsent" runat="server" text="LONGABSENT" onclick="btnLongAbsent_Click" />
         </div>
       <div>
           <asp:GridView ID="fg" runat="server">
           </asp:GridView>
           
       </div>
   
   
     <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
         AutoDataBind="true" />
   
    </form></html>
