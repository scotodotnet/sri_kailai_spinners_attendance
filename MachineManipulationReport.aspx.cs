﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;

public partial class MachineManipulationReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();

    System.Web.UI.WebControls.DataGrid GridView1 =
                 new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Machine Manipulation Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("LeaveManagement"));
                //li.Attributes.Add("class", "droplink active open");
            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();


            AutoDTable.Columns.Add("LocCode");
            AutoDTable.Columns.Add("IPAddress");
            AutoDTable.Columns.Add("GEnlNo");
            AutoDTable.Columns.Add("Manipulation");
            AutoDTable.Columns.Add("FpNo");
            AutoDTable.Columns.Add("HDateTime");


            SSQL = "";
            SSQL = "Select LocCode,IPAddress,GEnlNo,Manipulation,FpNo,HDateTime From LogHistory_Details";
            SSQL = SSQL + " Where LocCode='" + SessionLcode + "'";
            mEmployeeDT = objdata.ReturnMultipleValue(SSQL);


            for (int iRow = 0; iRow < mEmployeeDT.Rows.Count; iRow++)
            {

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[iRow][0] = mEmployeeDT.Rows[iRow]["LocCode"].ToString();
                AutoDTable.Rows[iRow][1] = mEmployeeDT.Rows[iRow]["IPAddress"].ToString();
                AutoDTable.Rows[iRow][2] = mEmployeeDT.Rows[iRow]["GEnlNo"].ToString();
                AutoDTable.Rows[iRow][3] = mEmployeeDT.Rows[iRow]["Manipulation"].ToString();
                AutoDTable.Rows[iRow][4] = mEmployeeDT.Rows[iRow]["FpNo"].ToString();
                AutoDTable.Rows[iRow][5] = mEmployeeDT.Rows[iRow]["HDateTime"].ToString();

            }


            GridView1.HeaderStyle.Font.Bold = true;
            GridView1.DataSource = AutoDTable;
            GridView1.DataBind();
            string attachment = "attachment;filename=MachineManipulationReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";

            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            GridView1.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td font-Bold='true' colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td font-Bold='true' colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\"> MACHINE MANIPULATION REPORT &nbsp;&nbsp;&nbsp;</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
    }
}
