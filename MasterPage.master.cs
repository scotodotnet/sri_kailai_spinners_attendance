﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using System.Security.Cryptography;
using System.IO;
using System.Text;



public partial class MasterPage : System.Web.UI.MasterPage
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Data;
    string SSQL;
    string SessionUserType;
    BALDataAccess objdata = new BALDataAccess();
    DataTable dtd = new DataTable();

    string DashboardM;
    string masterpageM;
    string UserCreationM;
    string AdministrationRightM;
    string UserRightsM;
    string DepartmentDetailsM;
    string DesginationDetailsM;
    string EmployeeTypeDetailsM;
    string WagesTypeDetailsM;
    string BankDetailsM;
    string EmployeeDetailsM;
    string LeaveMasterM;

    string PermissionMasterM;
    string GraceTimeM;
    string EarlyOutMasterM;
    string LateINMasterM;
    string EmployeeM;
    string EmployeeApprovalM;
    string EmployeeStatusM;
    string LeaveManagementM;
    string TimeDeleteM;
    string LeaveDetailsM;

    string PermissionDetailM;
    string ManualAttendanceM;
    string ManualShiftM;
    string ManualShiftDetailsM;
    string UploadDownloadM;
    string DeductionM;
    string IncentiveM;
    string ReportM;
    string DownloadClearM;
    string CompanyMasterM = "0";
    string SuperAdminCreationM = "0";
    string SalaryProcessM;
    string SessionSpay;
    string SessionEpay;
         

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        lblUserNameDisp.Text = SessionCcode.ToString() + "-" + SessionLcode.ToString();
        lbladmin.Text = SessionAdmin.ToString();
        SessionCompanyName = Session["CompanyName"].ToString();
        lblCompanyName.Text = SessionCompanyName.ToString();
        SessionLocationName = Session["LocationName"].ToString();
        Data = Session["Data"].ToString();
        SessionUserType = Session["UserType"].ToString();
        //HiddenUserType.Value = SessionUserType.ToString();
        if (!IsPostBack)
        {
            //AdminRightMaster();
            if (SessionAdmin.ToUpper().ToString() == "Scoto".ToUpper().ToString())
            {
                //Skip
                btnPayroll_Link.Visible = true;
            }
            else
            {                
                Payroll_ModuleLink_Check();
                if (SessionUserType == "1")
                {
                     Admin_User_Rights_Check();
                }
                else
                {
                   NonAdmin_User_Rights_Check();
                    //Admin_User_Rights_Check();
                }
                //AdminRightMaster();
            }

        }
     }

    private void Payroll_ModuleLink_Check()
    {
        string ModuleID = Encrypt("2").ToString();
        DataTable DT_Head = new DataTable();
        //Check with Module Rights
        string query = "Select * from [HR_Rights]..Company_Module_Rights where ModuleID='" + ModuleID + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT_Head = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Head.Rows.Count != 0)
        {
            btnPayroll_Link.Visible = true;
        }
        else
        {
            btnPayroll_Link.Visible = false;
        }
    }
    private void Admin_User_Rights_Check()
    {
        ALL_Menu_Header_Disable();
        ALL_Menu_Header_Forms_Disable();
        string ModuleID = Encrypt("1").ToString();
        string MenuID = "";
        DataTable DT_Head = new DataTable();
        DataTable DT_Form = new DataTable();
        //Check with Header Menu
        string query = "Select Distinct Menu_LI_ID,MenuID from [HR_Rights]..Company_Module_MenuHead_Rights where ModuleID='" + ModuleID + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT_Head = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;

            MenuID = DT_Head.Rows[i]["MenuID"].ToString();
            query = "Select Distinct Form_LI_ID from [HR_Rights]..Company_Module_Menu_Form_Rights where ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT_Form = objdata.RptEmployeeMultipleDetails(query);
            for (int J = 0; J < DT_Form.Rows.Count; J++)
            {
                this.FindControl(DT_Form.Rows[J]["Form_LI_ID"].ToString()).Visible = true;
            }
        }

    }

    private void NonAdmin_User_Rights_Check()
    {
        ALL_Menu_Header_Disable();
        ALL_Menu_Header_Forms_Disable();
        string ModuleID = Encrypt("1").ToString();
        string MenuID = "";
        DataTable DT_Head = new DataTable();
        DataTable DT_Form = new DataTable();
        //Check with Header Menu
        string query = "Select Distinct Menu_LI_ID,MenuID from [HR_Rights]..Company_Module_User_Rights where ModuleID='" + ModuleID + "' And UserName='" + SessionAdmin + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT_Head = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;

            MenuID = DT_Head.Rows[i]["MenuID"].ToString();
            query = "Select Distinct Form_LI_ID from [HR_Rights]..Company_Module_User_Rights where ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "' And UserName='" + SessionAdmin + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT_Form = objdata.RptEmployeeMultipleDetails(query);
            for (int J = 0; J < DT_Form.Rows.Count; J++)
            {
                this.FindControl(DT_Form.Rows[J]["Form_LI_ID"].ToString()).Visible = true;
            }
        }

    }
    private void ALL_Menu_Header_Disable()
    {
        this.FindControl("Dashboard").Visible = false;
        this.FindControl("masterpage").Visible = false;
        this.FindControl("Employee").Visible = false;
        this.FindControl("LeaveManagement").Visible = false;
        this.FindControl("ManualEntry").Visible = false;
        this.FindControl("SalaryProcess").Visible = false;
        this.FindControl("Report").Visible = false;
        this.FindControl("DownloadClear").Visible = false;
        this.FindControl("SuperAdminCreation").Visible = false;
        this.FindControl("CompanyMaster").Visible = false;
    }
    private void ALL_Menu_Header_Forms_Disable()
    {
        this.FindControl("Dashboard").Visible = false;
        //Master
        this.FindControl("UserCreation").Visible = false;
        this.FindControl("AdministrationRights").Visible = false;
        this.FindControl("UserRights").Visible = false;
        this.FindControl("Company_Module_Rights").Visible = false;
        this.FindControl("Company_Module_MenuHead_Rights").Visible = false;
        this.FindControl("Module_Form_Rights").Visible = false;
        this.FindControl("DepartmentDetails").Visible = false;
        this.FindControl("DesginationDetails").Visible = false;
        this.FindControl("EmployeeTypeDetails").Visible = false;
        this.FindControl("WagesTypeDetails").Visible = false;
        this.FindControl("EmployeeDetails").Visible = false;
        this.FindControl("BankDetails").Visible = false;
        this.FindControl("LeaveMaster").Visible = false;
        this.FindControl("GraceTime").Visible = false;
        this.FindControl("EarlyOutMaster").Visible = false;
        this.FindControl("LateINMaster").Visible = false;
        this.FindControl("Incentive").Visible = false;
        this.FindControl("PermissionMaster").Visible = false; 

        //Employee
        this.FindControl("EmployeeApproval").Visible = false;
        this.FindControl("EmployeeStatus").Visible = false;
        //LeaveManagement
        this.FindControl("TimeDelete").Visible = false;
        this.FindControl("LeaveDetails").Visible = false;
        //ManualShift
        this.FindControl("ManualShiftDetails").Visible = false;
        this.FindControl("ManualAttendance").Visible = false;
        this.FindControl("UploadDownload").Visible = false;
    }

    public void AdminRightMaster()
    {
        if (SessionAdmin.ToString() == "Altius")
        {
            this.FindControl("SuperAdminCreation").Visible = true;
            this.FindControl("CompanyMaster").Visible = true;
            this.FindControl("Dashboard").Visible = true;

            this.FindControl("masterpage").Visible = false;
            this.FindControl("Employee").Visible = false;
            this.FindControl("LeaveManagement").Visible = false;
            this.FindControl("ManualAttendance").Visible = false;
            this.FindControl("ManualShift").Visible = false;
            this.FindControl("Deduction").Visible = false;
            this.FindControl("Incentive").Visible = false;
            this.FindControl("Report").Visible = false;
            this.FindControl("DownloadClear").Visible = false;
        }
        else
        {

            SSQL = "select * from Rights where Username='" + SessionAdmin + "' and CompCode = '" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dtd = objdata.ReturnMultipleValue(SSQL);
            if (dtd.Rows.Count > 0)
            {
                DashboardM = dtd.Rows[0]["DashBoard"].ToString();
                if (DashboardM == "1")
                {
                    this.FindControl("Dashboard").Visible = true;
                }
                else
                {
                    this.FindControl("Dashboard").Visible = false;
                }

                masterpageM = dtd.Rows[0]["masterpage"].ToString();
                UserCreationM = dtd.Rows[0]["UserCreation"].ToString();
                AdministrationRightM = dtd.Rows[0]["AdministrationRights"].ToString();
                DepartmentDetailsM = dtd.Rows[0]["DepartmentDetails"].ToString();
                DesginationDetailsM = dtd.Rows[0]["DesginationDetails"].ToString();
                EmployeeTypeDetailsM = dtd.Rows[0]["EmployeeTypeDetails"].ToString();
                WagesTypeDetailsM = dtd.Rows[0]["WagesTypeDetails"].ToString();
                EmployeeDetailsM = dtd.Rows[0]["EmployeeDetails"].ToString();

                BankDetailsM = dtd.Rows[0]["BankDetails"].ToString();

                LeaveMasterM = dtd.Rows[0]["LeaveMaster"].ToString();
                PermissionMasterM = dtd.Rows[0]["PermissionMaster"].ToString();
                GraceTimeM = dtd.Rows[0]["GraceTime"].ToString();
                EarlyOutMasterM = dtd.Rows[0]["EarlyOutMaster"].ToString();
                LateINMasterM = dtd.Rows[0]["LateINMaster"].ToString();

                if (masterpageM != "1" && UserCreationM != "1" && AdministrationRightM != "1" && DepartmentDetailsM != "1" 
                    && DesginationDetailsM != "1" && EmployeeTypeDetailsM != "1" && WagesTypeDetailsM != "1" && BankDetailsM !="1"
                    && EmployeeDetailsM != "1" && LeaveMasterM != "1" && PermissionMasterM != "1" && GraceTimeM != "1"
                    && EarlyOutMasterM != "1" && LateINMasterM != "1")
                {

                    this.FindControl("masterpage").Visible = false;
                }
                else
                {
                    this.FindControl("masterpage").Visible = true;

                }

                if (UserCreationM == "1")
                {
                    this.FindControl("UserCreation").Visible = true;
                }
                else
                {
                    this.FindControl("UserCreation").Visible = false;
                }



                if (UserRightsM == "1")
                {
                    this.FindControl("UserRights").Visible = true;
                }
                else
                {
                    this.FindControl("UserRights").Visible = false;
                }



                if (AdministrationRightM == "1")
                {
                    this.FindControl("AdministrationRights").Visible = true;
                }
                else
                {
                    this.FindControl("AdministrationRights").Visible = false;
                }


                if (DepartmentDetailsM == "1")
                {
                    this.FindControl("DepartmentDetails").Visible = true;
                }
                else
                {
                    this.FindControl("DepartmentDetails").Visible = false;
                }

                if (DesginationDetailsM == "1")
                {
                    this.FindControl("DesginationDetails").Visible = true;
                }
                else
                {
                    this.FindControl("DesginationDetails").Visible = false;
                }

                if (EmployeeTypeDetailsM == "1")
                {
                    this.FindControl("EmployeeTypeDetails").Visible = true;
                }
                else
                {
                    this.FindControl("EmployeeTypeDetails").Visible = false;
                }

                if (WagesTypeDetailsM == "1")
                {
                    this.FindControl("WagesTypeDetails").Visible = true;
                }
                else
                {
                    this.FindControl("WagesTypeDetails").Visible = false;
                }

                if (BankDetailsM == "1")
                {
                    this.FindControl("BankDetails").Visible = true;
                }
                else
                {
                    this.FindControl("BankDetails").Visible = false;
                }

                if (EmployeeDetailsM == "1")
                {
                    this.FindControl("EmployeeDetails").Visible = true;
                }
                else
                {
                    this.FindControl("EmployeeDetails").Visible = false;
                }


                if (LeaveMasterM == "1")
                {
                    this.FindControl("LeaveMaster").Visible = true;
                }
                else
                {
                    this.FindControl("LeaveMaster").Visible = false;
                }

                if (PermissionMasterM == "1")
                {
                    this.FindControl("PermissionMaster").Visible = true;
                }
                else
                {
                    this.FindControl("PermissionMaster").Visible = false;
                }

                if (GraceTimeM == "1")
                {
                    this.FindControl("GraceTime").Visible = true;
                }
                else
                {
                    this.FindControl("GraceTime").Visible = false;
                }

                if (EarlyOutMasterM == "1")
                {
                    this.FindControl("EarlyOutMaster").Visible = true;
                }
                else
                {
                    this.FindControl("EarlyOutMaster").Visible = false;
                }

                if (LateINMasterM == "1")
                {
                    this.FindControl("LateINMaster").Visible = true;
                }
                else
                {
                    this.FindControl("LateINMaster").Visible = false;
                }


                EmployeeM = dtd.Rows[0]["Employee"].ToString();
                EmployeeApprovalM = dtd.Rows[0]["EmployeeApproval"].ToString();
                EmployeeStatusM = dtd.Rows[0]["EmployeeStatus"].ToString();

                if (EmployeeM != "1" && EmployeeApprovalM != "1" && EmployeeStatusM != "1")
                {
                    this.FindControl("Employee").Visible = false;
                }
                else
                {
                    this.FindControl("Employee").Visible = true;
                }

                if (EmployeeApprovalM == "1")
                {
                    this.FindControl("EmployeeApproval").Visible = true;
                }
                else
                {
                    this.FindControl("EmployeeApproval").Visible = false;
                }

                if (EmployeeStatusM == "1")
                {
                    this.FindControl("EmployeeStatus").Visible = true;
                }
                else
                {
                    this.FindControl("EmployeeStatus").Visible = false;
                }

                LeaveManagementM = dtd.Rows[0]["LeaveManagement"].ToString();
                TimeDeleteM = dtd.Rows[0]["TimeDelete"].ToString();
                LeaveDetailsM = dtd.Rows[0]["LeaveDetails"].ToString();
                PermissionDetailM = dtd.Rows[0]["PermissionDetail"].ToString();

                if (LeaveManagementM != "1" && TimeDeleteM != "1" && LeaveDetailsM != "1" && PermissionDetailM != "1")
                {
                    this.FindControl("LeaveManagement").Visible = false;
                }
                else
                {
                    this.FindControl("LeaveManagement").Visible = true;
                }
                if (TimeDeleteM == "1")
                {
                    this.FindControl("TimeDelete").Visible = true;
                }
                else
                {
                    this.FindControl("TimeDelete").Visible = false;
                }

                if (LeaveDetailsM == "1")
                {
                    this.FindControl("LeaveDetails").Visible = true;
                }
                else
                {
                    this.FindControl("LeaveDetails").Visible = false;
                }

                //if (PermissionDetailM == "1")
                //{
                //    this.FindControl("PermissionDetail").Visible = true;
                //}
                //else
                //{
                //    this.FindControl("PermissionMaster").Visible = false;
                //}

               

               

                ManualShiftM = dtd.Rows[0]["ManualShift"].ToString();
                ManualShiftDetailsM = dtd.Rows[0]["ManualShiftDetails"].ToString();
                UploadDownloadM = dtd.Rows[0]["UploadDownload"].ToString();
                ManualAttendanceM = dtd.Rows[0]["ManualAttendance"].ToString();

                if (ManualShiftM != "1" && ManualShiftDetailsM != "1" && UploadDownloadM != "1" && ManualAttendanceM != "1")
                {
                    this.FindControl("ManualEntry").Visible = false;
                }
                else
                {
                    this.FindControl("ManualEntry").Visible = true;
                }

                if (ManualShiftDetailsM == "1")
                {
                    this.FindControl("ManualShiftDetails").Visible = true;
                }
                else
                {
                    this.FindControl("ManualShiftDetails").Visible = false;
                }

                if (UploadDownloadM == "1")
                {
                    this.FindControl("UploadDownload").Visible = true;
                }
                else
                {
                    this.FindControl("UploadDownload").Visible = false;
                }

                if (ManualAttendanceM == "1")
                {
                    this.FindControl("ManualAttendance").Visible = true;
                }
                else
                {
                    this.FindControl("ManualAttendance").Visible = false;
                }

                SalaryProcessM = dtd.Rows[0]["SalaryProcess"].ToString();
                DeductionM = dtd.Rows[0]["Deduction"].ToString();
                IncentiveM = dtd.Rows[0]["Incentive"].ToString();

                if (SalaryProcessM != "1" && DeductionM != "1" && IncentiveM != "1")
                {
                    this.FindControl("SalaryProcess").Visible = false;
                }
                else
                {
                    this.FindControl("SalaryProcess").Visible = true;
                }
               
                if (DeductionM == "1")
                {
                    this.FindControl("Deduction").Visible = true;
                }
                else
                {
                    this.FindControl("Deduction").Visible = false;
                }

                if (IncentiveM == "1")
                {
                    this.FindControl("Incentive").Visible = true;
                }
                else
                {
                    this.FindControl("Incentive").Visible = false;
                }

                ReportM = dtd.Rows[0]["Report"].ToString();

                if (ReportM == "1")
                {
                    this.FindControl("Report").Visible = true;
                }
                else
                {
                    this.FindControl("Report").Visible = false;
                }

                DownloadClearM = dtd.Rows[0]["DownloadClear"].ToString();

                if (DownloadClearM == "1")
                {
                    this.FindControl("DownloadClear").Visible = true;
                }
                else
                {
                    this.FindControl("DownloadClear").Visible = false;
                }
                

                if (SuperAdminCreationM == "1")
                {
                    this.FindControl("SuperAdminCreation").Visible = true;
                }
                else
                {
                    this.FindControl("SuperAdminCreation").Visible = false;
                }

                

                if (CompanyMasterM == "1")
                {
                    this.FindControl("CompanyMaster").Visible = true;
                }
                else
                {
                    this.FindControl("CompanyMaster").Visible = false;
                }


            }
        }
       
    }





    //public void AdminRights()
    //{
    //  if (SessionAdmin.ToString() == "Altius")
    //    {
    //        var ct = this.FindControl("SuperAdminCreation");
    //        if (ct != null)
    //        {
    //            this.FindControl("SuperAdminCreation").Visible = true;
    //        }
    //        var ctt = this.FindControl("CompanyMaster");
    //        if (ctt != null)
    //        {
    //            this.FindControl("CompanyMaster").Visible = true;
    //        }
           
    //    }
    //    else
    //    {
    //        var ct = this.FindControl("SuperAdminCreation");
    //        if (ct != null)
    //        {
    //            this.FindControl("SuperAdminCreation").Visible = false;
    //        }
    //        var ctt = this.FindControl("CompanyMaster");
    //        if (ctt != null)
    //        {
    //            this.FindControl("CompanyMaster").Visible = false;
    //        }

    //    }
      
    //    string[] arr = Data.Split(',');
       

    //    if (arr[1].ToString() == "0" && arr[2].ToString() == "0" && arr[3].ToString() == "0" && arr[0].ToString() == "0")
    //    {
    //        var ct1 = this.FindControl("masterpag");
    //        if (ct1 != null)
    //        {
    //            this.FindControl("masterpag").Visible = false;
    //         }
    //    }
       
        
    //    if (arr[0].ToString() == "0")
    //    {
    //        var ct2 = this.FindControl("AdministrationRights");
    //        if (ct2 != null)
    //        {
    //            this.FindControl("AdministrationRights").Visible = false;
    //        }

    //       // Li11.Visible = false;
    //    }
       
    //    if (arr[1].ToString() == "0")
    //    {
    //        var ct3 = this.FindControl("UserCreation");
    //        if (ct3 != null)
    //        {
    //            this.FindControl("UserCreation").Visible = false;
    //        }
  
    //        //Li12.Visible = false;
    //    }
    //    if (arr[2].ToString() == "0")
    //    {
    //        var ct4 = this.FindControl("NewEmployee");
    //        if (ct4 != null)
    //        {
    //            this.FindControl("NewEmployee").Visible = false;
    //        }
            
    //        //NewEmployee.Visible = false;
    //    }
    //    if (arr[3].ToString() == "0")
    //    {
    //        var ct5 = this.FindControl("EmpDetails");
    //        if (ct5 != null)
    //        {
    //            this.FindControl("EmpDetails").Visible = false;
    //        }
            
            
    //        //EmpDetails.Visible = false;
    //    }
    //    if (arr[4].ToString() == "0")
    //    {

    //        var ct6 = this.FindControl("DownloadClear");
    //        if (ct6 != null)
    //        {
    //            this.FindControl("DownloadClear").Visible = false;
    //        }
            
    //        // Li2.Visible = false;
    //    }
    //    if (arr[5].ToString() == "0" && arr[6].ToString() == "0")
    //    {
    //        var ct7 = this.FindControl("Employee");
    //        if (ct7 != null)
    //        {
    //            this.FindControl("Employee").Visible = false;
    //        }
            
            
    //        //Li3.Visible = false;
    //    }
    //    if (arr[5].ToString() == "0")
    //    {
    //        var ct8 = this.FindControl("EmployeeApproval");
    //        if (ct8 != null)
    //        {
    //            this.FindControl("EmployeeApproval").Visible = false;
    //        }
            
    //        //Li31.Visible = false;
    //    }
    //    if (arr[6].ToString() == "0")
    //    {
    //        var ct9 = this.FindControl("EmployeeStatus");
    //        if (ct9 != null)
    //        {
    //            this.FindControl("EmployeeStatus").Visible = false;
    //        }


    //        //Li32.Visible = false;
    //    }
    //    if (arr[7].ToString() == "0")
    //    {

    //        var ct10 = this.FindControl("ManualAttendance");
    //        if (ct10 != null)
    //        {
    //            this.FindControl("ManualAttendance").Visible = false;
    //        }
            
            
    //        // ManualAttendance.Visible = false;
    //    }
    //    if (arr[8].ToString() == "0" && arr[9].ToString() == "0")
    //    {
    //        var ct11 = this.FindControl("SalaryCalculation");
    //        if (ct11 != null)
    //        {
    //            this.FindControl("SalaryCalculation").Visible = false;
    //        }
            
            
    //        //SalaryCalculation.Visible = false;
    //    }
    //    if (arr[8].ToString() == "0")
    //    {
    //        var ct12 = this.FindControl("DeductionandOT");
    //        if (ct12 != null)
    //        {
    //            this.FindControl("DeductionandOT").Visible = false;
    //        }
            
    //        //DeductionandOT.Visible = false;
    //    }
    //    if (arr[9].ToString() == "0")
    //    {
    //        var ct13 = this.FindControl("DepartmentIncentive");
    //        if (ct13 != null)
    //        {
    //            this.FindControl("DepartmentIncentive").Visible = false;
    //        }
            
    //        //DepartmentIncentive.Visible = false;
    //    }
    //    if (arr[10].ToString() == "0" && arr[11].ToString() == "0")
    //    {
    //        var ct14 = this.FindControl("SalaryCover");
    //        if (ct14 != null)
    //        {
    //            this.FindControl("SalaryCover").Visible = false;
    //        }
            
    //        // DepartmentIncentive.Visible = false;
    //    }
    //    if (arr[10].ToString() == "0")
    //    {
    //        var ct15 = this.FindControl("SalaryCoverPhoto");
    //        if (ct15 != null)
    //        {
    //            this.FindControl("SalaryCoverPhoto").Visible = false;
    //        }
            
    //        // SalaryCoverPhoto.Visible = false;
    //    }
    //    if (arr[11].ToString() == "0")
    //    {
    //        var ct16 = this.FindControl("SalaryDisbursement");
    //        if (ct16 != null)
    //        {
    //            this.FindControl("SalaryDisbursement").Visible = false;
    //        }
            
    //        // SalaryDisbursement.Visible = false;
    //    }
    //    if (arr[12].ToString() == "0" && arr[13].ToString() == "0")
    //    {
    //        var ct17 = this.FindControl("Others");
    //        if (ct17 != null)
    //        {
    //            this.FindControl("Others").Visible = false;
    //        }

    //        //SalaryCover.Visible = false;
    //    }
    //    if (arr[12].ToString() == "0")
    //    {
    //        var ct18 = this.FindControl("TimeDelete");
    //        if (ct18 != null)
    //        {
    //            this.FindControl("TimeDelete").Visible = false;
    //        }
            
            
    //        //TimeDelete.Visible = false;
    //    }
    //    if (arr[13].ToString() == "0")
    //    {
    //        var ct19 = this.FindControl("LeaveDetails");
    //        if (ct19 != null)
    //        {
    //            this.FindControl("LeaveDetails").Visible = false;
    //        }

    //        // LeaveDetails.Visible = false;
    //    }
    //    if (arr[14].ToString() == "0")
    //    {
    //        var ct20 = this.FindControl("Report");
    //        if (ct20 != null)
    //        {
    //            this.FindControl("Report").Visible = false;
    //        }

    //        //Li8.Visible = false;
    //    }
    //}

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    private string Decrypt(string cipherText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    protected void btnPayroll_Link_Click(object sender, EventArgs e)
    {
        //Get Payroll Link
        DataTable DT_Link = new DataTable();
        string query = "Select * from [HR_Rights]..Module_List where ModuleID='2'";
        DT_Link = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Link.Rows.Count != 0)
        {
            string Link_Open = DT_Link.Rows[0]["ModuleLink"].ToString();
            query = "Delete from [HR_Rights]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);
            //Get user Password
            query = "Select * from [HR_Rights]..MstUsers where UserCode='" + SessionAdmin + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
            DT_Link = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Link.Rows.Count != 0)
            {
                string Password_Str = DT_Link.Rows[0]["Password"].ToString();
                //Insert User Details
                query = "Insert Into [HR_Rights]..Module_Open_User(CompCode,LocCode,UserName,Password) Values";
                query = query + " ('" + SessionCcode + "','" + SessionLcode + "','" + SessionAdmin + "','" + Password_Str + "')";
                objdata.RptEmployeeMultipleDetails(query);
                Response.Redirect(Link_Open);
            }

        }
    }
}
