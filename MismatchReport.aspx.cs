﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class MismatchReport : System.Web.UI.Page
{
    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    BALDataAccess objdata = new BALDataAccess();
    
  //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();

    DateTime ShiftdateStartIN;
    DateTime ShiftdateEndIN;
    DateTime InTime_Check;
    DateTime InToTime_Check;
    DateTime EmpdateIN;
    DateTime date1;
    DataTable mEmployeeDS = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable DS_InTime = new DataTable();
    DataTable DS_Time = new DataTable();
    string Final_InTime;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string Time_IN_Str;
    string Time_OUT_Str;
    string Date_Value_Str;
    string Shift_Start_Time;
    string Shift_End_Time;
    string Final_Shift;
    string Total_Time_get;
    string[] Time_Minus_Value_Check;
    string[] OThour_Str;
    DataSet ds = new DataSet();
    int time_Check_dbl;
    DataTable mdatatable = new DataTable();
    DataTable LocalDT = new DataTable();
    DataTable Shift_DS = new DataTable();
    TimeSpan InTime_TimeSpan;
    string From_Time_Str = "";
    string To_Time_Str = "";
    DataTable mLocalDS=new DataTable();
    DataTable mLocalDT=new DataTable();
    DataTable dtIPaddress = new DataTable();
    DataTable AutoDTable = new DataTable();
	string Time_Out_Str = "";
    string SSQL = "";
    int itab = 0;
    string SessionUserType;

    Boolean Shift_Check_blb = false;
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-MisMatch Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();

            DataSet ds = new DataSet();

            DataColumn auto = new DataColumn();
            auto.AutoIncrement = true;
            auto.AutoIncrementSeed = 1;

            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("Dept");
            AutoDTable.Columns.Add("Type");
            AutoDTable.Columns.Add("Shift");
            AutoDTable.Columns.Add("EmpCode");
            AutoDTable.Columns.Add("ExCode");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("TimeIN");
            AutoDTable.Columns.Add("TimeOUT");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Category");
            AutoDTable.Columns.Add("SubCategory");
            AutoDTable.Columns.Add("TotalMIN");
            AutoDTable.Columns.Add("GrandTOT");
            AutoDTable.Columns.Add("ShiftDate");
            AutoDTable.Columns.Add("CompanyName");
            AutoDTable.Columns.Add("LocationName");
            AutoDTable.Columns.Add("Desgination");

            //ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            //ddlShiftType = Request.QueryString["ddlShiftType"].ToString();

            date1 = Convert.ToDateTime(Date);


            dtIPaddress = objdata.IPAddressForAll(SessionCcode.ToString(), SessionLcode.ToString());



            if (dtIPaddress.Rows.Count > 0)
            {
                for (int i = 0; i < dtIPaddress.Rows.Count; i++)
                {
                    if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                    {
                        mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                    else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                    {
                        mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                }
            }

            //if (ModeType == "IN/OUT")
            //{
            SSQL = "";
            SSQL = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
            SSQL = SSQL + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
            SSQL = SSQL + " from Shift_Mst Where CompCode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
            if (ShiftType1 != "ALL")
            {
                SSQL = SSQL + " And shiftDesc='" + ShiftType1 + "'";

            }
            //else
            //{
            //    SSQL = SSQL + " And ShiftDesc like '" + ddlShiftType + "%'";

            //}
            SSQL = SSQL + " Order By shiftDesc";
            LocalDT = objdata.ReturnMultipleValue(SSQL);
            //}
            //else
            //{
            //    return;
            //}
            string ShiftType = "";

            if (SessionLcode.ToString() == "UNIT I")
            {
                for (int i = 0; i <= LocalDT.Rows.Count - 1; i++)
                {
                    if (LocalDT.Rows[i]["shiftDesc"] == "SHIFT10")
                    {
                        LocalDT.Rows[i]["StartIN_Days"] = -1;
                        LocalDT.Rows[i]["EndIN_Days"] = 0;
                    }
                }
            }

            if (LocalDT.Rows.Count < 0)
                return;

            string ng = string.Format(Date, "dd-MM-yyyy");
            date1 = Convert.ToDateTime(ng);
            DateTime date2 = date1.AddDays(1);
            string Start_IN = "";
            string End_In;

            string End_In_Days = "";
            string Start_In_Days = "";


            int mStartINRow = 0;
            int mStartOUTRow = 0;

            NoShift_Add_All_Shift_Wise_GetAttdDayWise();


            for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
            {
                string MID = AutoDTable.Rows[iRow2][9].ToString();

                SSQL = "";
                SSQL = "select Distinct isnull(EM.MachineID_Encrypt,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
                SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
                SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
                SSQL = SSQL + ",isnull(EM.CatName,'') as [CatName], isnull(EM.SubCatName,'') as [SubCatName],isnull(EM.Designation,'') as Desgination";
                SSQL = SSQL + ",DM.DeptCode from Employee_Mst EM inner Join Department_Mst DM on DM.DeptName=EM.DeptName Where EM.MachineID_Encrypt='" + MID + "' And EM.Compcode='" + SessionCcode.ToString() + "'";
               
                if (SessionUserType == "3")
                {
                    SSQL = SSQL + " And EM.IsNonAdmin='1'";
                }
                
                SSQL = SSQL + " And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsActive='Yes'" + " order by DM.DeptCode Asc";
                mEmployeeDS = objdata.ReturnMultipleValue(SSQL);

                if (mEmployeeDS.Rows.Count > 0)
                {
                    for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                    {
                        string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();

                        //if (AutoDTable.Rows[iRow2][9] == mEmployeeDS.Rows[iRow1]["MachineID"])
                        //{

                        AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                        AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
                        AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                        AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                        AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                        AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                        AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                        AutoDTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");
                        AutoDTable.Rows[iRow2][15] = SessionCompanyName.ToString();
                        AutoDTable.Rows[iRow2][16] = SessionLocationName.ToString();
                        AutoDTable.Rows[iRow2][17] = mEmployeeDS.Rows[iRow1]["Desgination"];

                        //}
                    }
                }
            }



            ds.Tables.Add(AutoDTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Mismatch.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            CrystalReportViewer1.ReportSource = report;



        }

    }





    public void NoShift_Add_All_Shift_Wise_GetAttdDayWise()
    {
    
          dtIPaddress = objdata.IPAddressForAll(SessionCcode.ToString(), SessionLcode.ToString());

            if (dtIPaddress.Rows.Count > 0)
            {
                for (int i = 0; i < dtIPaddress.Rows.Count; i++)
                {
                    if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                    {
                        mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                    else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                    {
                        mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                }
            }

       

        string SSQL="";
        SSQL="Select Distinct MachineID_Encrypt as MachineID,ShiftType from Employee_Mst where Compcode='" + SessionCcode.ToString() + "'";
        SSQL=SSQL+"and Loccode='" + SessionLcode.ToString() + "' And IsActive='Yes'";

        if (SessionUserType == "3")
        {
            SSQL = SSQL + " And IsNonAdmin='1'";
        }


        mLocalDS=objdata.ReturnMultipleValue(SSQL);

        if(mLocalDS.Rows.Count<0)
            return;


       

    string Time_IN_Check_Shift = "";
	string Machine_ID_Check = "";
	string Emp_ShiftType = "";
	DataTable Shift_DataSet = new DataTable();
	DataTable mLocalDS_out = new DataTable();


	for (int iTabRow = 0; iTabRow < mLocalDS.Rows.Count; iTabRow++) 
{
    Machine_ID_Check = mLocalDS.Rows[iTabRow]["MachineID"].ToString();
    if (Machine_ID_Check == "MjA=")
    {
        Machine_ID_Check = "MjA=";
    }
    Emp_ShiftType = mLocalDS.Rows[iTabRow]["ShiftType"].ToString();

        SSQL="Select MachineID,TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode.ToString() + "'";
        SSQL=SSQL+" And LocCode='" + SessionLcode.ToString() + "'";
        SSQL=SSQL+" And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "05:00" + "'";
		SSQL = SSQL + " And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "' Order by TimeIN ASC";

        mLocalDT=objdata.ReturnMultipleValue(SSQL);


       if (mLocalDT.Rows.Count <= 0) 
       {
			//Skip
		} 
       else 
       {
           Time_IN_Check_Shift = mLocalDT.Rows[0]["TimeIN"].ToString();
             //Shift Check
             //2nd Shift Check
              string Shift_Start_Time = "";
			string Shift_End_Time = "";
			string Employee_Time = "";
           DataTable Shift_Ds=new DataTable();
           bool Shift_Add_or_Check = false;

           DateTime ShiftdateStartIN;
           DateTime ShiftdateEndIN;
           DateTime EmpdateIN;
           
           SSQL = "Select * from Shift_Mst where Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And ShiftDesc like '%%'";
           Shift_Ds = objdata.ReturnMultipleValue(SSQL);

            if (Shift_Ds.Rows.Count != 0)
            {
                Shift_Add_or_Check = false;
                for(int Z = 0; Z <= Shift_Ds.Rows.Count - 1; Z++) 
                {
                    if(Emp_ShiftType == "GENERAL" & Shift_Ds.Rows[Z]["ShiftDesc"].ToString() == "GENERAL")
                    {
                        Shift_Start_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["StartIN"].ToString();
                        Shift_End_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["EndIN"].ToString();

                        Employee_Time=mLocalDT.Rows[0]["TimeIN"].ToString();

                        ShiftdateStartIN=Convert.ToDateTime(Shift_Start_Time);
                        ShiftdateEndIN=Convert.ToDateTime(Shift_End_Time);
                        EmpdateIN=Convert.ToDateTime(Employee_Time);

                        if(EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                        {
                            Shift_Add_or_Check = true;
                            break;
                        }
                    }
                    else
                    {
                        if(Shift_Ds.Rows[Z]["ShiftDesc"].ToString() == "SHIFT3")
                        {
                            Shift_Start_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["StartIN"].ToString();
                            Shift_End_Time = date1.AddDays(0).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["EndIN"].ToString();
                        }
                        else
                        {
                            if(Shift_Ds.Rows[Z]["ShiftDesc"].ToString() == "GENERAL")
                            {
                            }
                            else
                            {
                                Shift_Start_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["StartIN"].ToString();
                                Shift_End_Time = date1.ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[Z]["EndIN"].ToString();
                            }
                        }
                        Employee_Time=mLocalDT.Rows[0]["TimeIN"].ToString();
                        if(Shift_Start_Time != "" & Shift_End_Time != "")
                        {
                            ShiftdateStartIN=Convert.ToDateTime(Shift_Start_Time);
                            ShiftdateEndIN=Convert.ToDateTime(Shift_End_Time);
                            EmpdateIN=Convert.ToDateTime(Employee_Time);

                            if(EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                            {
                                Shift_Add_or_Check = true;
								break;
                            }
                        }
                    }
                }
            }
if (Shift_Add_or_Check == false & Shift_Ds.Rows.Count != 0)
{
    AutoDTable.NewRow();
    AutoDTable.Rows.Add();

    AutoDTable.Rows[itab][3] = "SHIFT-MISMATCH";
    DateTime TimeIN_Str = Convert.ToDateTime(Time_IN_Check_Shift);
    AutoDTable.Rows[itab][7] = String.Format("{0:hh:mm tt}", TimeIN_Str);
     
    AutoDTable.Rows[itab][9] = Machine_ID_Check;
   
    //Time OUT Add
				SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode.ToString() + "'";
				SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "'";
				SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
				SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Desc";
				mLocalDS_out = objdata.ReturnMultipleValue(SSQL);

    if (mLocalDS_out.Rows.Count <= 0) 
    {
        //Skip
	}
    else 
    {
        string machine_outSTR = mLocalDS_out.Rows[0][0].ToString();
        if (AutoDTable.Rows[itab][9].ToString() == machine_outSTR)
        {
            string aa = mLocalDS_out.Rows[0][1].ToString();
          
            AutoDTable.Rows[itab][8] = String.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0]["TimeOUT"]);
            //AutoDTable.Rows[iRow2][8] = String.Format("HH:mm:ss", DS_Time.Rows[0][0]);			
		}
	}


    //Grand Total value Display
				Time_IN_Str = "";
				Time_Out_Str = "";

     Date_Value_Str = String.Format("yyyy/MM/dd",date1);

    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "'";
				SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
				SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
				mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

    	SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Check + "'";
				SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
				SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Desc";
				mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);


    string Emp_Total_Work_Time_1 = "00:00";
    if (mLocalDS_INTAB.Rows.Count > 1) 
    {
	for (int tin = 0; tin <= mLocalDS_INTAB.Rows.Count - 1; tin++) 
    {
		Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
	if (mLocalDS_OUTTAB.Rows.Count > tin) 
    {
		Time_Out_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
	} 
    else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
    {
			Time_Out_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
	} 
    else 
    {
		Time_Out_Str = "";
	}
        TimeSpan ts4 = new TimeSpan();
						ts4 = Convert.ToDateTime(string.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
						if (mLocalDS.Rows.Count <= 0) 
                        {
							Time_IN_Str = "";
						} 
                        else 
                        {
							Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
						}
        //Emp_Total_Work_Time
						if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                        {
							time_Check_dbl = time_Check_dbl;
						} 
                        else 
                        {
							DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
							DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);

							TimeSpan ts1 = new TimeSpan();
							ts1 = date4.Subtract(date3);
							ts1 = date4.Subtract(date3);
							Total_Time_get = ts1.Hours.ToString();
							//& ":" & Trim(ts.Minutes)
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                time_Check_dbl = Convert.ToInt32(Total_Time_get);
				Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
				Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
				if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
				    Emp_Total_Work_Time_1 = "00:00";
				if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
				    Emp_Total_Work_Time_1 = "00:00";
							
                   }
        else 
        {
            ts4 = ts4.Add(ts1);
								Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
								Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
								if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
									Emp_Total_Work_Time_1 = "00:00";
								if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
									Emp_Total_Work_Time_1 = "00:00";
								time_Check_dbl =Convert.ToInt32(Total_Time_get);
							}
						}
                        AutoDTable.Rows[itab][12] = Emp_Total_Work_Time_1;

						
						//fg.set_TextMatrix(fg.Rows - 1, fg.Cols - 1, Emp_Total_Work_Time_1);
					}
				}
    else 
{

TimeSpan ts4 = new TimeSpan();
					ts4 = Convert.ToDateTime(string.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
					if (mLocalDS_INTAB.Rows.Count <= 0) 
                    {
						Time_IN_Str = "";
					} else {
						Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
					}

    for (int tout = 0; tout <= mLocalDS_OUTTAB.Rows.Count - 1; tout++) 
    {
						if (mLocalDS_OUTTAB.Rows.Count <= 0) {
							Time_Out_Str = "";
						} else {
							Time_Out_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
						}

					}
            //Emp_Total_Work_Time
					if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str)) 
                    {
						time_Check_dbl = 0;
					} 
                    else 
                    {
						DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
						DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
						TimeSpan ts1 = new TimeSpan();
						ts1 = date4.Subtract(date3);
						ts1 = date4.Subtract(date3);
						Total_Time_get = ts1.Hours.ToString();

	if (Left_Val(Total_Time_get, 1) == "-") {
							date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
							ts1 = date4.Subtract(date3);
							ts1 = date4.Subtract(date3);
							ts4 = ts4.Add(ts1);
							Total_Time_get = ts1.Hours.ToString();
							//& ":" & Trim(ts.Minutes)
							time_Check_dbl =Convert.ToInt32(Total_Time_get);
							//Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
							//Emp_Total_Work_Time_1 = Trim(ts1.Hours) & ":" & Trim(ts1.Minutes)
							Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
							Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
							if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
								Emp_Total_Work_Time_1 = "00:00";
							if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
								Emp_Total_Work_Time_1 = "00:00";
						} else {
							ts4 = ts4.Add(ts1);
							Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
							Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
							if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
								Emp_Total_Work_Time_1 = "00:00";
							if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
								Emp_Total_Work_Time_1 = "00:00";
							time_Check_dbl = Convert.ToInt32(Total_Time_get);
						}
    AutoDTable.Rows[itab][12] = Emp_Total_Work_Time_1;
						//fg.set_TextMatrix(fg.Rows - 1, fg.Cols - 1, Emp_Total_Work_Time_1);
					}
				}
    itab = itab + 1;
			} else {
				//Skip
			}
		}
}
	





    

   
















    }

}


