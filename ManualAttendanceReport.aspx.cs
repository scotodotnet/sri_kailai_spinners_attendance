﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Web.UI;

public partial class ManualAttendanceReport : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string Date1_str;
    string Date2_str;

 

    string SSQL = "";
    
    DataTable mDataSet = new DataTable();
 
    DataTable AutoDataTable = new DataTable();
    int shiftCount;
    DateTime fromdate;
    DateTime todate;
    int dayCount;
    System.DateTime iDate;
    int daysAdded = 0;
    DataTable Datacells = new DataTable();
    Boolean isPresent;
    string SessionUserType;
    int grand;
    DataTable mLocalDS = new DataTable();

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Manual Attendance";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();

            Date1_str = Request.QueryString["FromDate"].ToString();
            Date2_str = Request.QueryString["ToDate"].ToString();

            Fill_Manual_Day_Attd_Between_Dates();

            Manual_Attendance_writeAttend();

        }

    }

    public void Fill_Manual_Day_Attd_Between_Dates()
    {
        try
        {
            fromdate = Convert.ToDateTime(Date1_str);
            todate = Convert.ToDateTime(Date2_str);
            int dayCount = (int)((todate - fromdate).TotalDays);
            if (dayCount > 0)
            {
                //if (category == "STAFF")
                //{
                    SSQL = "";
                    SSQL = "Select Distinct MA.EmpNo,EM.DeptName,EM.MachineID,EM.ExistingCode,";
                    SSQL = SSQL + " (EM.FirstName + '.'+ EM.MiddleInitial) as [FirstName] from Employee_Mst EM,ManAttn_Details MA where";
                    SSQL = SSQL + " EM.Compcode=MA.CompCode And EM.LocCode=MA.LocCode And EM.EmpNo=MA.EmpNo";
                    SSQL = SSQL + " And EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";
                    SSQL = SSQL + " And MA.Compcode='" + SessionCcode + "' And MA.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) >= CONVERT(VARCHAR(10), '" + Date1_str + "', 105)";
                    SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) <= CONVERT(VARCHAR(10), '" + Date2_str + "', 105)";
                    //SSQL = SSQL + " And EM.CatName = '" + category + "'";
                    if (SessionUserType == "3")
                    {
                        SSQL = SSQL + " And EM.IsNonAdmin='1'";
                    }
                    SSQL = SSQL + " Order By EM.DeptName, EM.MachineID";
                //}
                //else if (category == "LABOUR")
                //{
                //    SSQL = "";
                //    SSQL = "Select Distinct MA.EmpNo,EM.DeptName,EM.MachineID,EM.ExistingCode,";
                //    SSQL = SSQL + " (EM.FirstName + '.'+ EM.MiddleInitial) as [FirstName] from Employee_Mst EM,ManAttn_Details MA where";
                //    SSQL = SSQL + " EM.Compcode=MA.CompCode And EM.LocCode=MA.LocCode And EM.EmpNo=MA.EmpNo";
                //    SSQL = SSQL + " And EM.Compcode='" + CompanyCode.ToString() + "' And EM.LocCode='" + LocationCode.ToString() + "' And EM.IsActive='" + active + "'";
                //    SSQL = SSQL + " And MA.Compcode='" + CompanyCode.ToString() + "' And MA.LocCode='" + LocationCode.ToString() + "'";
                //    SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) >= CONVERT(VARCHAR(10), '" + Date + "', 105)";
                //    SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) <= CONVERT(VARCHAR(10), '" + Date2 + "', 105)";
                //    SSQL = SSQL + " Order By EM.DeptName, EM.MachineID";
                //}
              
     
                DataTable dsEmployee = objdata.ReturnMultipleValue(SSQL);

                if (dsEmployee.Rows.Count > 0)
                {
                    AutoDataTable.Columns.Add("EmpNo");
                    AutoDataTable.Columns.Add("DeptName");
                    AutoDataTable.Columns.Add("MachineID");
                    AutoDataTable.Columns.Add("ExistingCode");
                    AutoDataTable.Columns.Add("FirstName");
                    
                  for (int i = 0; i < dsEmployee.Rows.Count; i++)
                    {
                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();

                        AutoDataTable.Rows[i]["EmpNo"] = dsEmployee.Rows[i]["EmpNo"];
                        AutoDataTable.Rows[i]["DeptName"] = dsEmployee.Rows[i]["DeptName"];
                        AutoDataTable.Rows[i]["MachineID"] = dsEmployee.Rows[i]["MachineID"];
                        AutoDataTable.Rows[i]["ExistingCode"] = dsEmployee.Rows[i]["ExistingCode"];
                        AutoDataTable.Rows[i]["FirstName"] = dsEmployee.Rows[i]["FirstName"];

                    }


            SSQL = "";
            SSQL = "select ShiftDesc,StartIN,StartIN_Days,EndIN,EndIn_Days,StartOut,StartOut_Days,EndOut,EndOut_Days";
            SSQL = SSQL + " from Shift_Mst Where CompCode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And (ShiftDesc Like '%Shift%' Or ShiftDesc Like '%GENERAL%')";

            mDataSet = null;
            mDataSet = objdata.ReturnMultipleValue(SSQL);

            if(mDataSet.Rows.Count > 0)
            {

                shiftCount = mDataSet.Rows.Count * 2;
                long iColVal = 0;

                fromdate = Convert.ToDateTime(Date1_str);
                todate = Convert.ToDateTime(Date2_str);
                dayCount = (int)((todate - fromdate).TotalDays);
                iColVal = dayCount + 1;
                iDate = Convert.ToDateTime(Date1_str.ToString());

               
                Datacells.Columns.Add("EmpNo");
               //Datacells.Columns.Add("MachineID");
                Datacells.Columns.Add("ExistingCode");
                Datacells.Columns.Add("DeptName");
                Datacells.Columns.Add("FirstName");
                do
                {
                    DateTime dayy = Convert.ToDateTime(fromdate.AddDays(daysAdded).ToShortDateString());
                    AutoDataTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));
                    Datacells.Columns.Add(Convert.ToString(dayy.ToShortDateString()));
                    dayCount -= 1;
                    daysAdded += 1;
                    iDate = iDate.AddDays(1);
                    while (iDate > Convert.ToDateTime(Date2_str))
                    {
                        return;
                    }
                }
               
                while (true);
               }
             }
           }


        }

        catch (Exception ex)
        {
           
        }
    }


    public void Manual_Attendance_writeAttend()
    {
        try
        {
            string Date_value_str1 = "";
            int intI = 1;
            int intK = 1;
            int intCol = 0;
            
            Datacells.Columns.Add("Total Days");

            for (int i = 0; i < AutoDataTable.Rows.Count; i++)
            {
                string Machine_ID_Str;
                string OT_Week_OFF_Machine_No;
                string Date_Value_Str;

                string Attn_Status;
                isPresent = false;

                Datacells.NewRow();
                Datacells.Rows.Add();
                Datacells.Rows[i]["EmpNo"] = AutoDataTable.Rows[i]["EmpNo"];
                Datacells.Rows[i]["DeptName"] = AutoDataTable.Rows[i]["DeptName"];
                //Datacells.Rows[i]["MachineID"] = AutoDataTable.Rows[i]["MachineID"];
                Datacells.Rows[i]["ExistingCode"] = AutoDataTable.Rows[i]["ExistingCode"];
                Datacells.Rows[i]["FirstName"] = AutoDataTable.Rows[i]["FirstName"];




                OT_Week_OFF_Machine_No = AutoDataTable.Rows[i]["MachineID"].ToString();
                //Date_Value_Str = Datacells.Rows[3][intK].ToString();


                fromdate = Convert.ToDateTime(Date1_str);
                todate = Convert.ToDateTime(Date2_str);
                int dayCount3 = (int)((todate - fromdate).TotalDays);
                int daycol = 4;
                int counting = 0;
                int daysAdded = 0;

                while (dayCount3 >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(fromdate.AddDays(daysAdded).ToShortDateString());

                    SSQL = "Select * from ManAttn_Details where Machine_No='" + OT_Week_OFF_Machine_No + "' And AttnDate ='" + dayy.ToShortDateString() + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                    mLocalDS = objdata.ReturnMultipleValue(SSQL);

                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Attn_Status = "";
                        isPresent = true;
                    }
                    else
                    {

                        Attn_Status = mLocalDS.Rows[0]["AttnStatus"].ToString();
                        isPresent = true;
                    }

                    if (string.IsNullOrEmpty(Attn_Status))
                    {
                        Datacells.Rows[i][daycol] = "";
                    }
                    else if (Attn_Status == "H")
                    {


                        Datacells.Rows[i][daycol] = "<span style=color:Green><b>H</b></span>";

                        
                    }
                    else
                    {
                      //  Datacells.Rows[i][daycol] = "<b><span style=color:red>" + Attn_Status + "</span></b>";

                        Datacells.Rows[i][daycol] = "<b>" + Attn_Status + "</b>";
                        counting += 1;
                    }

                    dayCount3 -= 1;
                    daysAdded += 1;
                    daycol += 1;
                }
                Datacells.Rows[i]["Total Days"] = counting;
            }

            int i1;
            int j;
            int daycol1 = 4;

            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1][daycol1 - 1] = "<b>Grand Total</b>";
         
            for (i1 = 0; i1 < daysAdded; i1++)
            {
                grand = 0;
                for (j = 0; j < Datacells.Rows.Count-1; j++)
                {
                    if (Datacells.Rows[j][daycol1] != "")
                    {
                        grand = grand + 1;
                    }
                }
                Datacells.Rows[Datacells.Rows.Count - 1][daycol1] = grand;
                daycol1 += 1;
            }





        }



        catch (Exception ex)
        {

        }





        grid.DataSource = Datacells;
        grid.DataBind();
        string attachment = "attachment;filename=MANUAL ATTENDANCE BETWEEN DATES.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);

        Response.Write("<table>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
        Response.Write("--");
        Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">MANUAL ATTENDANCE BETWEEN DATES REPORT &nbsp;&nbsp;&nbsp;</a>");
      
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\"> FROM -" + Date1_str + "</a>");
        Response.Write("&nbsp;&nbsp;&nbsp;");
        Response.Write("<a style=\"font-weight:bold\"> TO -" + Date2_str + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();



















    }

}

 



//private void Manual_Attendance_writeAttend(object xls)
//{
//    try {
//        var _with1 = xls;

//        _with1.cells(1, 1).value = xlTitle;
//        _with1.cells(1, 4).EntireRow.Font.Bold = true;

//        string Date_value_str1 = "";
//        int intI = 1;
//        int intK = 1;
//        int intCol = 0;
//        for (intCol = 0; intCol <= 5; intCol++) {
//            _with1.cells(3, intCol + 1).value = fg.get_TextMatrix(0, intCol);
//        }

//        intCol = 6;
//        int dayCount = DateDiff(DateInterval.Day, dtpFromDate.Value.Date, dtpToDate.Value.Date);
//        int daysAdded = 0;
//        while (dayCount >= 0) {
//            _with1.cells(3, intCol).Value = Strings.Format(dtpFromDate.Value.Date.AddDays(daysAdded), "dd/MM/yyyy");
//            int Daysadded1 = daysAdded + 1;
//            //Date_value_str1 = Format(dtpFromDate.Value.Date.AddDays(Daysadded1), "dd/MM/yyyy")
//            intCol += 1;
//            dayCount -= 1;
//            daysAdded += 1;
//        }
//        _with1.cells(3, intCol).EntireRow.Font.Bold = true;

//        intI = 4;
//        intK = 1;

//        for (intRow = 3; intRow <= fg.Rows - 1; intRow++) {
//            intK = 1;
//            //Master Data(Eno,Ename,...)
//            for (intCol = 0; intCol <= 4; intCol++) {
//                _with1.Cells(intI, intK).value = fg.get_TextMatrix(intRow, intCol);
//                intK += 1;
//            }
//            int colIndex = intK;
//            bool isPresent = false;

//            for (intCol = 0; intCol <= daysAdded - 1; intCol++) {
//                string Machine_ID_Str = "";
//                string OT_Week_OFF_Machine_No = "";
//                string Date_Value_Str = "";
//                DataSet mLocalDS = new DataSet();
//                string Attn_Status = "";
//                isPresent = false;
//                //For i = 0 To shiftCount - 1 Step 2 'take account only IN Column(leave OUT Column)
//                //MsgBox(.Cells(intI, 2).value)
//                //Machine_ID_Str = Encryption(.Cells(intI, 1).value)
//                OT_Week_OFF_Machine_No = _with1.Cells(intI, 1).value;
//                Date_Value_Str = _with1.Cells(3, intK).value;

//                //Attendance Status Get
//                SSQL = "Select * from ManAttn_Details where Machine_No='" + OT_Week_OFF_Machine_No + "' And AttnDate ='" + Date_Value_Str + "' And CompCode='" + iStr1(0) + "' And LocCode='" + iStr2(0) + "'";
//                mLocalDS = ReturnMultipleValue(SSQL);
//                if (mLocalDS.Tables(0).Rows.Count <= 0) {
//                    Attn_Status = "";
//                    isPresent = true;
//                } else {
//                    Attn_Status = mLocalDS.Tables(0).Rows(0)("AttnStatus");
//                    isPresent = true;
//                }

//                //WRITE ATTENDANCE STATUS

//                //.cells(intI, intK).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green)
//                //.Cells(intI, intK).Font.Size = 10
//                if (string.IsNullOrEmpty(Attn_Status)) {
//                    _with1.Cells(intI, intK).value = "";
//                } else if (Attn_Status == "H") {
//                    _with1.Cells(intI, intK).value = "H";
//                    //.Cells(intI, intK).Font.Name = "Webdings"
//                    _with1.Cells(intI, intK).Font.Bold = true;
//                    _with1.cells(intI, intK).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green);
//                    _with1.Cells(intI, intK).Font.Size = 12;
//                } else {
//                    _with1.Cells(intI, intK).value = Attn_Status;
//                    //.Cells(intI, intK).Font.Name = "Webdings"
//                    _with1.Cells(intI, intK).Font.Bold = true;
//                    //.cells(intI, intK).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green)
//                    _with1.Cells(intI, intK).Font.Size = 12;
//                }
//                colIndex += shiftCount;
//                intK += 1;

//            }
//            intI += 1;
//        }

//    } catch (Exception ex) {
//        Show_Message(ex.Message);
//    }

//}
























    //public void Fill_Manual_Day_Attd_Between_Dates()
    //{

    //    try
    //    {
    //        if (dtpFromDate.Value > dtpToDate.Value)
    //            return;

    //        SSQL = "";
    //        SSQL = "Select Distinct MA.EmpNo,EM.DeptName,EM.MachineID,EM.ExistingCode,";
    //        SSQL = SSQL + " (EM.FirstName + '.'+ EM.MiddleInitial) as [FirstName] from Employee_Mst EM,ManAttn_Details MA where";
    //        SSQL = SSQL + " EM.Compcode=MA.CompCode And EM.LocCode=MA.LocCode And EM.EmpNo=MA.EmpNo";
    //        SSQL = SSQL + " And EM.Compcode='" + iStr1(0) + "' And EM.LocCode='" + iStr2(0) + "' And EM.IsActive='Yes'";
    //        SSQL = SSQL + " And MA.Compcode='" + iStr1(0) + "' And MA.LocCode='" + iStr2(0) + "'";
    //        SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) >= CONVERT(VARCHAR(10), '" + dtpFromDate.Value + "', 105)";
    //        SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) <= CONVERT(VARCHAR(10), '" + dtpToDate.Value + "', 105)";
    //        SSQL = SSQL + " Order By EM.DeptName, EM.MachineID";

    //        DataSet dsEmployee = ReturnMultipleValue(SSQL);

    //        if (dsEmployee.Tables(0).Rows.Count <= 0)
    //            return;

    //        fg.Rows = 0;
    //        fg.Cols = 0;

    //        fg.Rows = dsEmployee.Tables(0).Rows.Count + 3;
           
    //        fg.Cols = dsEmployee.Tables(0).Columns.Count + 1;

    //        for (int j = 0; j <= dsEmployee.Tables(0).Columns.Count - 1; j++)
    //        {
    //            fg.set_TextMatrix(0, j, dsEmployee.Tables(0).Columns(j).ColumnName);
    //        }

    //        for (int i = 0; i <= dsEmployee.Tables(0).Rows.Count - 1; i++)
    //        {
    //            for (int j = 0; j <= dsEmployee.Tables(0).Columns.Count - 1; j++)
    //            {
    //             fg.set_TextMatrix(i + 3, j, dsEmployee.Tables(0).Rows(i)(j));
    //            }
    //        }

    //        SSQL = "";
    //        SSQL = "select ShiftDesc,StartIN,StartIN_Days,EndIN,EndIn_Days,StartOut,StartOut_Days,EndOut,EndOut_Days";
    //        SSQL = SSQL + " from Shift_Mst Where CompCode='" + iStr1(0) + "'";
    //        SSQL = SSQL + " And LocCode='" + iStr2(0) + "'";
    //        SSQL = SSQL + " And (ShiftDesc Like '%Shift%' Or ShiftDesc Like '%GENERAL%')";

    //        mDataSet = null;
    //        mDataSet = ReturnMultipleValue(SSQL);

    //        if (mDataSet.Tables(0).Rows.Count <= 0)
    //            return;
    //        shiftCount = mDataSet.Tables(0).Rows.Count * 2;
          
    //        long iColVal = 0;

    //        iColVal = DateDiff(DateInterval.Day, dtpFromDate.Value, dtpToDate.Value) + 1;

    //        System.DateTime iDate = dtpFromDate.Value;

    //        string mStartIN = "";
    //        string mEndIN = "";
    //        double mStartIN_Days = 0;
    //        double mEndIN_Days = 0;

    //        string mStartOUT = "";
    //        string mEndOUT = "";
    //        double mStartOUT_Days = 0;
    //        double mEndOUT_Days = 0;

    //        DataSet mLocalDS = new DataSet();
    //        mLocalDS = null;

    //        do
    //        {
    //            fg.set_TextMatrix(0, fg.Cols - 1, iDate.ToString("dd/MM/yyyy"));
               
    //            fg.Cols = fg.Cols + 1;
            
    //            iDate = iDate.AddDays(1);

    //            while (iDate > dtpToDate.Value)
    //            {
    //                return;
    //            }
    //        }
    //        while (true);
    //    }
    //    catch (Exception ex)
    //    {
    //        MessageBox.Show(ex.Message, "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information);
    //        return;
    //    }
//    }



//}
