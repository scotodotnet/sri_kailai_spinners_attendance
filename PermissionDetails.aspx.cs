﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;

public partial class PermissionDetails : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    ManualAttendanceClass objManual = new ManualAttendanceClass();
    bool ErrFlag = false;
    string SSQL;
    string MonthDays_Get;
    DateTime fromdate;
    DateTime todate;
    DataTable dtdDisplay = new DataTable();
    string deptname;
    int Permission_Mst;
    DataTable mDataSet = new DataTable();
    int Permission = 1;
    static string tt;   
    int Remaining;
    DataTable dtbPermission = new DataTable();
    DataTable dtdata = new DataTable();
    string remaincount;
    static string Auto_ID;
    static string id;
    int rcount;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
                SessionCcode = Session["Ccode"].ToString();
                SessionLcode = Session["Lcode"].ToString();
                SessionAdmin = Session["Isadmin"].ToString();
                SessionCompanyName = Session["CompanyName"].ToString();
                SessionLocationName = Session["LocationName"].ToString();
                if (!IsPostBack)
                {
                    Page.Title = "Spay Module | Permission Details";
                    //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                    //li.Attributes.Add("class", "droplink active open");

                    Financial_Year();
                    DropDown_TokenNumber();
                    DropDown_PermissionTypeMst();
                    DisplayLeaveReg();
                }
        
        }
    }

    public void DisplayLeaveReg()
    {
        DataTable dtDisplay = new DataTable();
        SSQL = "select * from Permi_Register_Mst";
        dtdDisplay = objdata.ReturnMultipleValue(SSQL);
        rptrCustomer.DataSource = dtdDisplay;
        rptrCustomer.DataBind();
    }

   

    public void DropDown_PermissionTypeMst()
    {
        DataTable dtdPermi = new DataTable();
        SSQL = "select Distinct PermissionType from PermissionType_Mst";
        dtdPermi = objdata.ReturnMultipleValue(SSQL);
        ddlpermissionType.DataSource = dtdPermi; ;
        DataRow dr = dtdPermi.NewRow();
        dr["PermissionType"] = "- select -";
        dtdPermi.Rows.InsertAt(dr, 0);
        for (int i = 0; i < dtdPermi.Rows.Count; i++)
         {
             ddlpermissionType.Items.Add(dtdPermi.Rows[i]["PermissionType"].ToString());
         }

    }


   
    

    protected void ddlTicketno_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dted = new DataTable();
        dted = objdata.Manual_Data(ddlTicketno.SelectedItem.Text);
        if (dted.Rows.Count > 0)
        {
            txtExistingCode.Text = dted.Rows[0]["ExistingCode"].ToString();
            txtEmpName.Text = dted.Rows[0]["FirstName"].ToString();
        }
    }





    public void Financial_Year()
    {
        String CurrentYear1;
        int CurrentYear;
        CurrentYear1 = DateTime.Now.Year.ToString();
        CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());
        if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
        {
            CurrentYear = CurrentYear - 1;
        }
        for (int i = 0; i < 1; i++)
        {
            tt = CurrentYear1;
            CurrentYear = CurrentYear - 1;
            string cy = Convert.ToString(CurrentYear);
            CurrentYear1 = cy;

        }
    }
   


    public void DropDown_TokenNumber()
    {
        DataTable dt = new DataTable();
        dt = objdata.dropdown_TokenNumber(SessionCcode, SessionLcode);
        ddlTicketno.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["EmpNo"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlTicketno.Items.Add(dt.Rows[i]["EmpNo"].ToString());
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string ss1 = txtDate.Text;

        DateTime da = Convert.ToDateTime(ss1.ToString());
        string Monthname = da.ToString("MMMM"); 

       string Encrpt_ID = UTF8Encryption(ddlTicketno.SelectedItem.Text);
       DataTable dtbLeave = new DataTable();
        
        if (ddlTicketno.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Your TicketNo');", true);
            ErrFlag = true;
        }
        else if (ddlpermissionType.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Your Permission Type');", true);
            ErrFlag = true;
        }
        else
        {
            if (btnSave.Text == "Update")
            {
                DataTable dtdUpdate = new DataTable();
                SSQL = "select*from Permi_Register_Mst where Auto_ID='" + Auto_ID + "';";
                dtdUpdate = objdata.ReturnMultipleValue(SSQL);
                string rr = dtdUpdate.Rows[0]["PermAllowedCount"].ToString();
                string dd = dtdUpdate.Rows[0]["PermRemainCount"].ToString();


                DataTable dtdatatable = new DataTable();
                SSQL = "update Permi_Register_Mst set EmpNo = '" + ddlTicketno.SelectedItem.Text + "'";
                SSQL = SSQL + ",ExistingCode = '" + txtExistingCode.Text + "',Machine_No = '" + ddlTicketno.SelectedItem.Text + "'";
                SSQL = SSQL + ",Machine_Encrypt = '" + Encrpt_ID + "',EmpName = '" + txtEmpName.Text + "'";
                SSQL = SSQL + ",Date= '" + txtDate.Text + "',Month='"+ Monthname +"'";
                SSQL = SSQL + ",PermissionType = '" + ddlpermissionType.SelectedItem.Text + "',PermissionDesc = '" + txtPermissionDesc.Text + "'";
                SSQL = SSQL + ",PermissionStatus = 'N',Date_Dt='" + txtDate.Text + "',PermAllowedCount = '" + rr + "',PermRemainCount = '" + dd + "',FinancialYear='" + tt + "' where EmpNo='" + ddlTicketno.SelectedItem.Text + "' and Auto_ID='"+ Auto_ID+"'";
                dtdatatable = objdata.ReturnMultipleValue(SSQL);

                Clear();
                DisplayLeaveReg();
                btnSave.Text = "Save";
                ddlTicketno.Enabled = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Updated Successfully');", true);
                ErrFlag = true;
            }
            else
            {

                DataTable dtdDeptName = new DataTable();
                SSQL = "select DeptName from Employee_Mst where EmpNo='" + ddlTicketno.SelectedItem.Text + "'";
                SSQL = SSQL + " and CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'";
                dtdDeptName = objdata.ReturnMultipleValue(SSQL);
                if (dtdDeptName.Rows.Count > 0)
                {
                    deptname = dtdDeptName.Rows[0]["DeptName"].ToString();
                }


                DataTable dtdCount = new DataTable();
                string PermType = ddlpermissionType.SelectedItem.Text;
                dtdCount = objdata.ChechDepartmentPermisssion(PermType, deptname, tt, SessionCcode, SessionLcode, Monthname);

                if (dtdCount.Rows.Count > 0)
                {
                    string allowedCount = dtdCount.Rows[0]["NoofCount"].ToString();
                    Permission_Mst = Convert.ToInt32(allowedCount.ToString());
                    if (Permission_Mst < Permission)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Sorry You Have Only Limited Permission');", true);
                        ErrFlag = true;
                        Clear();
                    }
                }

                string ss = "select Min(PermRemainCount) As Permission from Permi_Register_Mst where EmpNo='" + ddlTicketno.SelectedItem.Text + "' and CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "' and FinancialYear= '" + tt.ToString() + "' and Month='" + Monthname + "'";
                DataTable dtdPerm = new DataTable();
                dtdPerm = objdata.ReturnMultipleValue(ss);
                if (dtdPerm.Rows.Count > 0)
                {
                    remaincount = dtdPerm.Rows[0]["Permission"].ToString();
                    if (remaincount != "")
                    {
                        rcount = Convert.ToInt16(remaincount.ToString());
                        if (rcount > 0)
                        {
                            rcount = Convert.ToInt16(remaincount.ToString());
                            Remaining = rcount - Permission;

                            SSQL = "select Machine_No from Permi_Register_Mst where Machine_No = '" + ddlTicketno.SelectedItem.Text + "'and Date = '" + txtDate.Text + "'and FinancialYear='" + tt + "'and Month='" + Monthname + "'";
                            dtdata = objdata.ReturnMultipleValue(SSQL);
                            if (dtdata.Rows.Count <= 0)
                            {
                                SSQL = "Insert Into Permi_Register_Mst(CompCode,LocCode,EmpNo,ExistingCode,Machine_No,Machine_Encrypt,EmpName";
                                SSQL = SSQL + ",Date,PermissionType,PermissionDesc,PermissionStatus,Date_Dt,PermAllowedCount,PermRemainCount,FinancialYear,Month) Values('" + SessionCcode + "'";
                                SSQL = SSQL + ",'" + SessionLcode + "','" + ddlTicketno.SelectedItem.Text + "','" + txtExistingCode.Text + "','" + ddlTicketno.SelectedItem.Text + "'";
                                SSQL = SSQL + ",'" + UTF8Encryption(ddlTicketno.SelectedItem.Text) + "','" + txtEmpName.Text + "','" + txtDate.Text + "'";
                                SSQL = SSQL + ",'" + ddlpermissionType.SelectedItem.Text + "','" + txtPermissionDesc.Text + "','N','" + (Convert.ToDateTime(txtDate.Text).AddDays(0).ToString("dd-MMM-yyyy")) + "'";
                                SSQL = SSQL + ",'" + rcount + "','" + Remaining + "','" + tt.ToString() + "','" + Monthname + "')";
                                dtbPermission = objdata.ReturnMultipleValue(SSQL);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Save Successfully');", true);
                                ErrFlag = true;
                                Clear();
                                DisplayLeaveReg();
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Sorry Your Permission Completed');", true);
                            ErrFlag = true;
                            Clear();
                        }

                    }
                    else
                    {
                        DataTable dtdFirst = new DataTable();
                        SSQL = "select NoofCount from PermissionType_Mst where PermissionType='" + ddlpermissionType.SelectedItem.Text + "' and Dept='" + deptname + "'";
                        SSQL = SSQL + "and FinancialYear='" + tt + "'and Month='" + Monthname + "'";
                        SSQL = SSQL + "and CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'";
                        dtdFirst = objdata.ReturnMultipleValue(SSQL);
                        if(dtdFirst.Rows.Count >0)
                        {
                        string allowedCount = dtdFirst.Rows[0]["NoofCount"].ToString();
                        Permission_Mst = Convert.ToInt32(allowedCount.ToString());
                        if (Permission_Mst > Permission)
                        {
                            remaincount = "0";
                            int count = Convert.ToInt16(remaincount.ToString());
                            Remaining = Permission_Mst - Permission;


                            SSQL = "select Machine_No from Permi_Register_Mst where Machine_No = '" + ddlTicketno.SelectedItem.Text + "'and Date = '" + txtDate.Text + "'and FinancialYear='" + tt + "'";
                            dtdata = objdata.ReturnMultipleValue(SSQL);
                            if (dtdata.Rows.Count <= 0)
                            {
                                SSQL = "Insert Into Permi_Register_Mst(CompCode,LocCode,EmpNo,ExistingCode,Machine_No,Machine_Encrypt,EmpName";
                                SSQL = SSQL + ",Date,PermissionType,PermissionDesc,PermissionStatus,Date_Dt,PermAllowedCount,PermRemainCount,FinancialYear,Month) Values('" + SessionCcode + "'";
                                SSQL = SSQL + ",'" + SessionLcode + "','" + ddlTicketno.SelectedItem.Text + "','" + txtExistingCode.Text + "','" + ddlTicketno.SelectedItem.Text + "'";
                                SSQL = SSQL + ",'" + UTF8Encryption(ddlTicketno.SelectedItem.Text) + "','" + txtEmpName.Text + "','" + txtDate.Text + "'";
                                SSQL = SSQL + ",'" + ddlpermissionType.SelectedItem.Text + "','" + txtPermissionDesc.Text + "','N','" + (Convert.ToDateTime(txtDate.Text).AddDays(0).ToString("dd-MMM-yyyy")) + "'";
                                SSQL = SSQL + ",'" + Permission_Mst + "','" + Remaining + "','" + tt.ToString() + "','" + Monthname + "')";
                                dtbPermission = objdata.ReturnMultipleValue(SSQL);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Save Successfully');", true);
                                ErrFlag = true;
                                Clear();
                                DisplayLeaveReg();
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Already Exist');", true);
                                ErrFlag = true;
                                Clear();
                            }
                        }
                       
                      }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Register Data in Permission Master');", true);
                            ErrFlag = true;
                            Clear();
                        }
                    }
                }
            }
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    public void Clear()
    {
        ddlTicketno.Enabled = true;

        ddlTicketno.Items.Clear();
        ddlpermissionType.Items.Clear();
        
        
        
        //ddlTicketno.SelectedIndex = 0;
        //ddlpermissionType.SelectedIndex = 0;
        txtExistingCode.Text = "";
        txtDate.Text = "";
        txtEmpName.Text = "";
        txtPermissionDesc.Text = "";
        btnSave.Text = "Save";

        DropDown_TokenNumber();
        DropDown_PermissionTypeMst();

   }

   


    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

        id = commandArgs[0];
        Auto_ID = commandArgs[1];


        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(id, Auto_ID);
                break;
            case ("Edit"):
                EditRepeaterData(id, Auto_ID);
                break;
        }
    }

    private void EditRepeaterData(string id, string Auto_ID)
    {
        DataTable Dtd = new DataTable();
        SSQL = "select * from Permi_Register_Mst where EmpNo='" + id + "' and Auto_ID='" + Auto_ID + "'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        if (Dtd.Rows.Count > 0)
        {
            ddlTicketno.SelectedItem.Text = Dtd.Rows[0]["EmpNo"].ToString();
            txtExistingCode.Text = Dtd.Rows[0]["ExistingCode"].ToString();
            txtEmpName.Text = Dtd.Rows[0]["EmpName"].ToString();
            txtDate.Text = Dtd.Rows[0]["Date"].ToString();
            ddlpermissionType.SelectedItem.Text = Dtd.Rows[0]["PermissionType"].ToString();
            txtPermissionDesc.Text = Dtd.Rows[0]["PermissionDesc"].ToString();
            DisplayLeaveReg();
            btnSave.Text = "Update";
            ddlTicketno.Enabled = false;
        }

    }
    private void DeleteRepeaterData(string id, string Auto_ID)
    {

        DataTable Dtd = new DataTable();
        SSQL = "Delete Permi_Register_Mst where EmpNo='" + id + "' and Auto_ID = '" + Auto_ID + "'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        DisplayLeaveReg();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
    }

   
    protected void txtToDate_TextChanged(object sender, EventArgs e)
    {
    //    fromdate = Convert.ToDateTime(txtFromDate.Text);
    //    todate = Convert.ToDateTime(txtToDate.Text);
    //    int dayCount = (int)((todate - fromdate).TotalDays);
    //    txtTotalDays.Text = Convert.ToString(dayCount.ToString());
    }


    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
}
