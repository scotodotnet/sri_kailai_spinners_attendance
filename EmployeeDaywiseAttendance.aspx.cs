﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class EmployeeDaywiseAttendance : System.Web.UI.Page
{
    string EmpCode1 = "";
    string ShiftType1 = "";
    string Date1 = "";
    string Date2 = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionUserType;

    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string SSQL = "";
    DateTime date1;
    DateTime date2;
    string[] Time_Minus_Value_Check;
    string EmpName = "";
    DataTable AutoDTable = new DataTable();
    DataTable mLocalDS = new DataTable();
    System.Web.UI.WebControls.DataGrid grid =
                             new System.Web.UI.WebControls.DataGrid();


    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Employee DayWise Report";
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();

            EmpCode1 = Request.QueryString["EmpCode"].ToString();
            Date1 = Request.QueryString["Date1"].ToString();
            Date2 = Request.QueryString["Date2"].ToString();

            DataSet ds = new DataSet();

            DataColumn auto = new DataColumn();
            auto.AutoIncrement = true;
            auto.AutoIncrementSeed = 1;

            AutoDTable.Columns.Add("Date");
            AutoDTable.Columns.Add("1st IN");
            AutoDTable.Columns.Add("1st OUT");
            AutoDTable.Columns.Add("1st Total Hours");
            AutoDTable.Columns.Add("2nd IN");
            AutoDTable.Columns.Add("2nd OUT");
            AutoDTable.Columns.Add("2nd Total Hours");
            AutoDTable.Columns.Add("3nd IN");
            AutoDTable.Columns.Add("3nd OUT");
            AutoDTable.Columns.Add("3nd Total Hours");
            AutoDTable.Columns.Add("Final Total Hours");





            date1 = Convert.ToDateTime(Date1);
            date2 = Convert.ToDateTime(Date2);
            Fill_Emp_Day_Attd_Between_Dates();
            // UploadDataTableToExcel(AutoDTable);



            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=EmployeeWise.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
            Response.Write(" &nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">EMPLOYEE WISE DAY ATTENDANCE BETWEEN DATES</a>");
            Response.Write(" &nbsp;&nbsp;&nbsp; ");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">From:" + Date1 + "</a>");
            Response.Write(" &nbsp;-- &nbsp;");
            Response.Write("<a style=\"font-weight:bold\">To:" + Date2 + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write(" <a style=\"font-weight:bold\">EMPCODE: ");
            Response.Write("<a style=\"font-weight:bold\">" + EmpCode1 + "</a>");
            Response.Write(" &nbsp; &nbsp; &nbsp; ");
            Response.Write("<a style=\"font-weight:bold\">EMPNAME: ");
            Response.Write("<a style=\"font-weight:bold\">" + EmpName + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            //Response.Write("<tr align='Center'>");
            // Response.Write("<td colspan='10'>");
            //Response.Write("" + Cmpaddress + "");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            //Response.Write("<tr>");
            //Response.Write("<td colspan='10'>");
            //Response.Write("Salary Month of " + ddlMonths.SelectedValue + " - " + YR);
            //Response.Write("</td>");
            //Response.Write("</tr>");
            Response.Write("</table>");

            ////Response.Write("Contract Details");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
        }
    }


    //protected void UploadDataTableToExcel(DataTable dtRecords)
    //{
    //    string XlsPath = Server.MapPath(@"~/Add_data/test.xls");
    //    string attachment = string.Empty;
    //    if (XlsPath.IndexOf("\\") != -1)
    //    {
    //        string[] strFileName = XlsPath.Split(new char[] { '\\' });
    //        attachment = "attachment; filename=" + strFileName[strFileName.Length - 1];
    //    }
    //    else
    //        attachment = "attachment; filename=" + XlsPath;
    //    try
    //    {
    //        Response.ClearContent();
    //        Response.AddHeader("content-disposition", attachment);
    //        Response.ContentType = "application/vnd.ms-excel";
    //        string tab = string.Empty;

    //        foreach (DataColumn datacol in dtRecords.Columns)
    //        {
    //            Response.Write(tab + datacol.ColumnName);
    //            tab = "\t";
    //        }
    //        Response.Write("\n");

    //        foreach (DataRow dr in dtRecords.Rows)
    //        {
    //            tab = "";
    //            for (int j = 0; j < dtRecords.Columns.Count; j++)
    //            {
    //                Response.Write(tab + Convert.ToString(dr[j]));
    //                tab = "\t";
    //            }

    //            Response.Write("\n");
    //        }
    //        Response.End();
    //    }
    //    catch (Exception ex)
    //    {
    //        //Response.Write(ex.Message);
    //    }
    //}


    public void Fill_Emp_Day_Attd_Between_Dates()
    {

        try
        {
            if (date1 > date2)
                return;

            string iEmpDet = null;

            iEmpDet = EmpCode1;

            long iRowVal = 0;

            int daysAdded = 0;
            int daycount = (int)((date2 - date1).TotalDays);

            iRowVal = daycount;

            int i = 0;
            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded));
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[i]["Date"] = Convert.ToString(dayy.ToShortDateString());
                daycount -= 1;
                daysAdded += 1;
                i++;
            }

            DataTable mLocalShift = new DataTable();
            DataTable mEmployeeDT = new DataTable();
            string Machine_ID_Str = null;

            SSQL = "";
            SSQL = "select ShiftDesc,StartIN,StartIN_Days,EndIN,EndIn_Days,StartOut,StartOut_Days,EndOut,EndOut_Days";
            SSQL = SSQL + " from Shift_Mst Where ShiftDesc Like '%Shift%'";
            SSQL = SSQL + " And CompCode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "'";

            mLocalShift = objdata.ReturnMultipleValue(SSQL);

            SSQL = "Select MachineID_Encrypt,FirstName from Employee_Mst where IsActive='Yes' And EmpNo='" + iEmpDet + "'";
            SSQL = SSQL + " And CompCode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "'";
            



            mEmployeeDT = objdata.ReturnMultipleValue(SSQL);

            Machine_ID_Str = mEmployeeDT.Rows[0][0].ToString();
            EmpName = mEmployeeDT.Rows[0][1].ToString();


            string Date_Value_Str = null;
            string Date_Value_Str1 = null;

            string OT_Week_OFF_Machine_No = null;
            string Time_IN_Str = null;
            string Time_Out_Str = null;

            string Total_Time_get = "";
            string Total_Time_get_1 = "";
            Int32 j = 0;
            string time_Check_dbl = "";
            Int32 time_Check_dbl_1 = 0;
            for (int mGrdRow = 0; mGrdRow < AutoDTable.Rows.Count; mGrdRow++)
            {
                DateTime fromdate;
                OT_Week_OFF_Machine_No = Machine_ID_Str;
                string aa = AutoDTable.Rows[mGrdRow]["Date"].ToString();
                DateTime Date_Value = Convert.ToDateTime(aa);
                DateTime Date_Value1 = Date_Value.AddDays(1);
                Date_Value_Str = string.Format(Date_Value.ToString("yyyy/MM/dd"));
                Date_Value_Str1 = string.Format(Date_Value1.ToString("yyyy/MM/dd"));

                string Emp_Total_Work_Time_1 = "00:00";
                string Final_OT_Work_Time_1 = "00:00";

                //Find Week OFF
                string Employee_Week_Name = "";
                string Assign_Week_Name = "";
                DataTable NFHDS = new DataTable();
                string qry_nfh = "";

                //Time In Query
                DataTable mLocalDS1 = new DataTable();
                time_Check_dbl = "";
                time_Check_dbl_1 = 0;
                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
                mLocalDS = objdata.ReturnMultipleValue(SSQL);
                if (mLocalDS.Rows.Count <= 0)
                {
                    Time_IN_Str = "";
                }
                else
                {
                    //Time_IN_Str = mLocalDS.Tables(0).Rows(0)(0)
                }

                //Find Shift Name Start                
                string From_Time_Str = "";
                string To_Time_Str = "";
                string Final_InTime = "";
                string Final_OutTime = "";
                string Final_Shift = "";
                DataTable Shift_DS = new DataTable();
                Int32 K = 0;
                bool Shift_Check_blb = false;
                string Shift_Start_Time = null;
                string Shift_End_Time = null;
                string Employee_Time = "";
                DateTime ShiftdateStartIN = default(DateTime);
                DateTime ShiftdateEndIN = default(DateTime);
                DateTime EmpdateIN = default(DateTime);

                //Check With IN Time Shift
                if (mLocalDS.Rows.Count != 0)
                {
                    SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And ShiftDesc like '%SHIFT%'";
                    Shift_DS = objdata.ReturnMultipleValue(SSQL);
                    Shift_Check_blb = false;
                    Final_InTime = mLocalDS.Rows[0]["TimeIN"].ToString();
                    for (K = 0; K < Shift_DS.Rows.Count; K++)
                    {
                        string a = Shift_DS.Rows[K]["StartIN_Days"].ToString();
                        int b = Convert.ToInt16(a.ToString());
                        Shift_Start_Time = Convert.ToDateTime(Date_Value_Str).AddDays(b).ToShortDateString() + " " + Shift_DS.Rows[K]["StartIN"].ToString();
                        string a1 = Shift_DS.Rows[K]["EndIN_Days"].ToString();
                        int b1 = Convert.ToInt16(a1.ToString());
                        Shift_End_Time = Convert.ToDateTime(Date_Value_Str).AddDays(b1).ToShortDateString() + " " + Shift_DS.Rows[K]["EndIN"].ToString();

                        ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                        ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                        EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());

                        if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                        {
                            Final_Shift = Shift_DS.Rows[K]["ShiftDesc"].ToString();
                            Shift_Check_blb = true;
                            break; // TODO: might not be correct. Was : Exit For
                        }
                    }
                    //    if (Shift_Check_blb == false)
                    //    {
                    //        //TimeOUT Get
                    //        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    //        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    //        SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";
                    //    }
                    //    else
                    //    {
                    //        if (Final_Shift == "SHIFT1")
                    //        {
                    //            //TimeOUT Get
                    //            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    //            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    //            SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "06:30' And TimeOUT <='" + Date_Value_Str1 + " " + "03:00' Order by TimeOUT Asc";
                    //        }
                    //        else
                    //        {
                    //            //TimeOUT Get
                    //            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    //            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    //            SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    //TimeOUT Get
                    //    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    //    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    //    SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";
                    //}
                }

                // DataTable mLocalDS1 = new DataTable();
                //DayOfWeek day = date1.DayOfWeek;
                //string WeekofDay = day.ToString();
                fromdate = Convert.ToDateTime(Date_Value_Str);

                
                DayOfWeek day = fromdate.DayOfWeek;
                string WeekofDay = day.ToString();


                if (Final_Shift == "SHIFT1" && WeekofDay == "Saturday")
                {
                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                }
                else if (Final_Shift == "SHIFT1")
                {
                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "00:00' And TimeIN <='" + Date_Value_Str + " " + "23:00' Order by TimeIN ASC";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                }
                else
                {
                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                }

                if (Final_Shift == "SHIFT1" && WeekofDay == "Saturday")
                {
                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";
                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                }
                else if (Final_Shift == "SHIFT1" && WeekofDay == "Sunday")
                {
                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "01:00' And TimeOUT <='" + Date_Value_Str + " " + "23:00' Order by TimeOUT Asc";
                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);


                   
                }
                else if (Final_Shift == "SHIFT1")
                {
                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "00:00' And TimeOUT <='" + Date_Value_Str + " " + "23:00' Order by TimeOUT Asc";
                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                }
               
                else
                {
                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";
                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                }












                //Find Shift Name End
                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                if (mLocalDS1.Rows.Count <= 0)
                {
                    Time_Out_Str = "";
                }
                else
                {
                    //Time_Out_Str = mLocalDS.Tables(0).Rows(0)(0)
                }
                if (mLocalDS.Rows.Count > 1)
                {
                    Final_OT_Work_Time_1 = "00:00";
                    for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                    {
                        Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                        if (tin == 3)
                            break; // TODO: might not be correct. Was : Exit For
                        if (mLocalDS1.Rows.Count > tin)
                        {
                            Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                        }
                        else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                        {
                            Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                        }
                        else
                        {
                            Time_Out_Str = "";
                        }
                        if (tin == 0)
                        {
                            //First Time In And Time Out Update
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                                AutoDTable.Rows[mGrdRow][1] = Time_IN_Str;
                                //fg.set_TextMatrix(mGrdRow, 1, Time_IN_Str);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[tin][0]);

                                //  fg.set_TextMatrix(mGrdRow, 1, string.Format("{0:hh:mm tt}", mLocalDS.Tables(0).Rows(tin)(0)));
                            }
                            if (string.IsNullOrEmpty(Time_Out_Str))
                            {
                                AutoDTable.Rows[mGrdRow][2] = Time_Out_Str;
                                //fg.set_TextMatrix(mGrdRow, 2, Time_Out_Str);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][2] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[tin][0]);
                                // fg.set_TextMatrix(mGrdRow, 2, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(tin)(0)));
                            }
                        }
                        else if (tin == 1)
                        {
                            //First Time In And Time Out Update
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                AutoDTable.Rows[mGrdRow][4] = Time_IN_Str;
                                //fg.set_TextMatrix(mGrdRow, 4, Time_IN_Str);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][4] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[tin][0]);

                                // fg.set_TextMatrix(mGrdRow, 4, string.Format("{0:hh:mm tt}", mLocalDS.Tables(0).Rows(tin)(0)));
                            }
                            if (string.IsNullOrEmpty(Time_Out_Str))
                            {
                                AutoDTable.Rows[mGrdRow][5] = Time_Out_Str;
                                // fg.set_TextMatrix(mGrdRow, 5, Time_Out_Str);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][5] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[tin][0]);

                                //fg.set_TextMatrix(mGrdRow, 5, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(mLocalDS1.Tables(0).Rows.Count - 1)(0)));
                            }
                        }

                        else if (tin == 2)
                        {
                            //First Time In And Time Out Update
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                AutoDTable.Rows[mGrdRow][7] = Time_IN_Str;
                                //fg.set_TextMatrix(mGrdRow, 4, Time_IN_Str);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][7] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[tin][0]);

                                // fg.set_TextMatrix(mGrdRow, 4, string.Format("{0:hh:mm tt}", mLocalDS.Tables(0).Rows(tin)(0)));
                            }
                            if (string.IsNullOrEmpty(Time_Out_Str))
                            {
                                AutoDTable.Rows[mGrdRow][8] = Time_Out_Str;
                                // fg.set_TextMatrix(mGrdRow, 5, Time_Out_Str);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][8] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[tin][0]);

                                //fg.set_TextMatrix(mGrdRow, 5, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(mLocalDS1.Tables(0).Rows.Count - 1)(0)));
                            }
                        }



                        //First Total Hours
                        if (tin == 0)
                        {
                            //Time Duration Get
                            time_Check_dbl = "";
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = "";
                            }
                            else
                            {
                                DateTime date5 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date6 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();

                                ts = date6.Subtract(date5);
                                ts = date6.Subtract(date5);
                                Total_Time_get = ts.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);

                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = ts.Hours.ToString();

                                    //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                    time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                                }
                                else
                                {
                                    time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                                }
                            }

                            //Find Week OFF
                            NFHDS = null;
                            qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                            NFHDS = objdata.ReturnMultipleValue(qry_nfh);
                            if (NFHDS.Rows.Count > 0)
                            {
                                AutoDTable.Rows[mGrdRow][3] = "NH / " + time_Check_dbl;
                                //  fg.set_TextMatrix(mGrdRow, 3, "NH / " + time_Check_dbl);
                            }
                            else
                            {
                                Employee_Week_Name = (Convert.ToDateTime(Date_Value_Str).DayOfWeek).ToString();
                                //Get Employee Week OF DAY
                                SSQL = "Select WeekOff from Employee_MST where MachineID_Encrypt='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                NFHDS = objdata.ReturnMultipleValue(SSQL);
                                if (NFHDS.Rows.Count <= 0)
                                {
                                    Assign_Week_Name = "";
                                }
                                else
                                {
                                    Assign_Week_Name = NFHDS.Rows[0][0].ToString();
                                }
                                if (Employee_Week_Name == Assign_Week_Name)
                                {
                                    AutoDTable.Rows[mGrdRow][3] = "WH / " + time_Check_dbl;
                                    //  fg.set_TextMatrix(mGrdRow, 3, "WH / " + time_Check_dbl);
                                }
                                else
                                {
                                    AutoDTable.Rows[mGrdRow][3] = time_Check_dbl;
                                    //fg.set_TextMatrix(mGrdRow, 3, time_Check_dbl);
                                }
                            }
                            // Second Total Hours
                        }
                        else if (tin == 1)
                        {
                            //Time Duration Get
                            time_Check_dbl = "";
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = "";
                            }
                            else
                            {
                                DateTime date7 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date8 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();

                                ts = date8.Subtract(date7);
                                ts = date8.Subtract(date7);
                                Total_Time_get = ts.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date8 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);

                                    ts = date8.Subtract(date7);
                                    ts = date8.Subtract(date7);
                                    Total_Time_get = ts.Hours.ToString();

                                    //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                    time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                                }
                                else
                                {
                                    time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                                }
                            }
                            //Find Week OFF
                            NFHDS = null;
                            qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value_Str + "'";
                            NFHDS = objdata.ReturnMultipleValue(qry_nfh);
                            if (NFHDS.Rows.Count > 0)
                            {
                                AutoDTable.Rows[mGrdRow][6] = "NH / " + time_Check_dbl;
                                //fg.set_TextMatrix(mGrdRow, 6, "NH / " + time_Check_dbl);
                            }
                            else
                            {
                                Employee_Week_Name = (Convert.ToDateTime(Date_Value_Str).DayOfWeek).ToString();
                                SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + iEmpDet + "'";     //OT_Week_OFF_Machine_No
                                NFHDS = objdata.ReturnMultipleValue(SSQL);
                                if (NFHDS.Rows.Count <= 0)
                                {
                                    Assign_Week_Name = "";
                                }
                                else
                                {
                                    Assign_Week_Name = NFHDS.Rows[0][0].ToString();
                                }
                                if (Employee_Week_Name == Assign_Week_Name)
                                {
                                    AutoDTable.Rows[mGrdRow][6] = "NH / " + time_Check_dbl;
                                    // fg.set_TextMatrix(mGrdRow, 6, "WH / " + time_Check_dbl);
                                }
                                else
                                {
                                    AutoDTable.Rows[mGrdRow][6] = time_Check_dbl;
                                    // fg.set_TextMatrix(mGrdRow, 6, time_Check_dbl);
                                }
                            }
                        }
                        else if (tin == 2)
                        {
                            //Time Duration Get
                            time_Check_dbl = "";
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = "";
                            }
                            else
                            {
                                DateTime date7 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date8 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();

                                ts = date8.Subtract(date7);
                                ts = date8.Subtract(date7);
                                Total_Time_get = ts.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date8 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);

                                    ts = date8.Subtract(date7);
                                    ts = date8.Subtract(date7);
                                    Total_Time_get = ts.Hours.ToString();

                                    //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                    time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                                }
                                else
                                {
                                    time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                                }
                            }
                            //Find Week OFF
                            NFHDS = null;
                            qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value_Str + "'";
                            NFHDS = objdata.ReturnMultipleValue(qry_nfh);
                            if (NFHDS.Rows.Count > 0)
                            {
                                AutoDTable.Rows[mGrdRow][9] = "NH / " + time_Check_dbl;
                                //fg.set_TextMatrix(mGrdRow, 6, "NH / " + time_Check_dbl);
                            }
                            else
                            {
                                Employee_Week_Name = (Convert.ToDateTime(Date_Value_Str).DayOfWeek).ToString();
                                SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + iEmpDet + "'";     //OT_Week_OFF_Machine_No
                                NFHDS = objdata.ReturnMultipleValue(SSQL);
                                if (NFHDS.Rows.Count <= 0)
                                {
                                    Assign_Week_Name = "";
                                }
                                else
                                {
                                    Assign_Week_Name = NFHDS.Rows[0][0].ToString();
                                }
                                if (Employee_Week_Name == Assign_Week_Name)
                                {
                                    AutoDTable.Rows[mGrdRow][9] = "NH / " + time_Check_dbl;
                                    // fg.set_TextMatrix(mGrdRow, 6, "WH / " + time_Check_dbl);
                                }
                                else
                                {
                                    AutoDTable.Rows[mGrdRow][9] = time_Check_dbl;
                                    // fg.set_TextMatrix(mGrdRow, 6, time_Check_dbl);
                                }
                            }
                        }




                        //Final Total Time Get Start
                        Time_IN_Str = mLocalDS.Rows[tin][0].ToString();

                        if (mLocalDS1.Rows.Count > tin)
                        {
                            Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                        }
                        else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                        {
                            Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                        }
                        else
                        {
                            Time_Out_Str = "";
                        }
                        TimeSpan ts4Final = new TimeSpan();
                        ts4Final = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Final_OT_Work_Time_1)).TimeOfDay;
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                        }
                        //Emp_Total_Work_Time
                        if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                        {
                            time_Check_dbl = time_Check_dbl;
                        }
                        else
                        {
                            DateTime date3Final = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4Final = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts1Final = new TimeSpan();
                            ts1Final = date4Final.Subtract(date3Final);
                            ts1Final = date4Final.Subtract(date3Final);
                            Total_Time_get = ts1Final.Hours.ToString();
                            //& ":" & Trim(ts.Minutes)
                            //ts4 = ts4.Add(ts1)
                            //OT Time Get
                            Final_OT_Work_Time_1 = ts4Final.Hours + ":" + ts4Final.Minutes;
                            Time_Minus_Value_Check = Final_OT_Work_Time_1.Split(':');
                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                Final_OT_Work_Time_1 = "00:00";
                            if (Left_Val(Final_OT_Work_Time_1, 1) == "0:" | Final_OT_Work_Time_1 == "0:-1" | Final_OT_Work_Time_1 == "0:-3")
                                Final_OT_Work_Time_1 = "00:00";
                            //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4Final = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts1Final = date4Final.Subtract(date3Final);
                                ts1Final = date4Final.Subtract(date3Final);
                                ts4Final = ts4Final.Add(ts1Final);
                                Total_Time_get = ts1Final.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                time_Check_dbl = Total_Time_get;
                                Final_OT_Work_Time_1 = ts4Final.Hours + ":" + ts4Final.Minutes;
                                Time_Minus_Value_Check = Final_OT_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Final_OT_Work_Time_1 = "00:00";
                                if (Left_Val(Final_OT_Work_Time_1, 1) == "0:" | Final_OT_Work_Time_1 == "0:-1" | Final_OT_Work_Time_1 == "0:-3")
                                    Final_OT_Work_Time_1 = "00:00";
                            }
                            else
                            {
                                ts4Final = ts4Final.Add(ts1Final);
                                Final_OT_Work_Time_1 = ts4Final.Hours + ":" + ts4Final.Minutes;
                                Time_Minus_Value_Check = Final_OT_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Final_OT_Work_Time_1 = "00:00";
                                if (Left_Val(Final_OT_Work_Time_1, 1) == "0:" | Final_OT_Work_Time_1 == "0:-1" | Final_OT_Work_Time_1 == "0:-3")
                                    Final_OT_Work_Time_1 = "00:00";
                                time_Check_dbl = Total_Time_get;
                            }
                        }
                        //Final Total Time Get End
                    }
                    //Update Final Work Total Time
                    if (Final_OT_Work_Time_1 != "00:00")
                    {
                        AutoDTable.Rows[mGrdRow][10] = Final_OT_Work_Time_1;
                    }
                    else
                    {
                        AutoDTable.Rows[mGrdRow][10] = "";
                    }

                    //fg.set_TextMatrix(mGrdRow, 7, (Final_OT_Work_Time_1 != "00:00" ? Final_OT_Work_Time_1 : ""));
                }
                else
                {
                    time_Check_dbl = "";

                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Time_IN_Str = "";
                        AutoDTable.Rows[mGrdRow][1] = Time_IN_Str;
                        //fg.set_TextMatrix(mGrdRow, 1, Time_IN_Str);
                    }
                    else
                    {
                        Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                        AutoDTable.Rows[mGrdRow][1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0]);
                        //fg.set_TextMatrix(mGrdRow, 1, string.Format("{0:hh:mm tt}", mLocalDS.Tables[0].Rows[0][0]));
                    }
                    //Time Out Get
                    //If Machine_ID_Str = "iNMfGxLH3zBn/Sxh0+p9RQ==" Then
                    //    MessageBox.Show("" & "")
                    //End If
                    //SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" & Machine_ID_Str & "'"
                    //SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                    //SSQL = SSQL & " And TimeOUT >='" & Date_Value_Str & " " & "10:00' And TimeOUT <='" & Date_Value_Str1 & " " & "10:00' Order by TimeOUT Asc"
                    //mLocalDS = ReturnMultipleValue(SSQL)
                    if (mLocalDS1.Rows.Count <= 0)
                    {
                        Time_Out_Str = "";
                        AutoDTable.Rows[mGrdRow][2] = Time_Out_Str;
                        //fg.set_TextMatrix(mGrdRow, 2, Time_Out_Str);
                    }
                    else
                    {
                        Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                        AutoDTable.Rows[mGrdRow][2] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0]);
                        //fg.set_TextMatrix(mGrdRow, 2, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(0)(0)));
                    }
                    for (int Z = 0; Z <= mLocalDS1.Rows.Count - 1; Z++)
                    {
                        if (Z == 0)
                        {
                            AutoDTable.Rows[mGrdRow][2] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[Z][0]);
                            //fg.set_TextMatrix(mGrdRow, 2, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(Z)(0)));
                        }
                        else if (Z == 1)
                        {
                            AutoDTable.Rows[mGrdRow][5] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[Z][0]);
                            //fg.set_TextMatrix(mGrdRow, 5, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(Z)(0)));
                        }
                        else if (Z == 2)
                        {
                            AutoDTable.Rows[mGrdRow][8] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[Z][0]);
                            //fg.set_TextMatrix(mGrdRow, 5, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(Z)(0)));
                        }
                    }
                    //Time Duration Get
                    if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                    {
                        time_Check_dbl = "";
                    }
                    else
                    {
                        date1 = System.Convert.ToDateTime(Time_IN_Str);
                        date2 = System.Convert.ToDateTime(Time_Out_Str);
                        TimeSpan ts = new TimeSpan();

                        ts = date2.Subtract(date1);
                        ts = date2.Subtract(date1);
                        Total_Time_get = ts.Hours.ToString();
                        //& ":" & Trim(ts.Minutes)
                        if (Left_Val(Total_Time_get, 1) == "-")
                        {
                            date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);

                            ts = date2.Subtract(date1);
                            ts = date2.Subtract(date1);
                            Total_Time_get = ts.Hours.ToString();

                            //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                            time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                        }
                        else
                        {
                            time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                        }
                    }

                    //Find Week OFF
                    NFHDS = null;
                    qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value_Str + "'";
                    NFHDS = objdata.ReturnMultipleValue(qry_nfh);
                    if (NFHDS.Rows.Count > 0)
                    {
                        AutoDTable.Rows[mGrdRow][3] = "NH / " + time_Check_dbl;
                        AutoDTable.Rows[mGrdRow][7] = "NH / " + time_Check_dbl;
                        AutoDTable.Rows[mGrdRow][11] = "NH / " + time_Check_dbl;
                        //fg.set_TextMatrix(mGrdRow, 3, "NH / " + time_Check_dbl);
                        //fg.set_TextMatrix(mGrdRow, 7, "NH / " + time_Check_dbl);
                    }
                    else
                    {
                        Employee_Week_Name = (Convert.ToDateTime(Date_Value_Str).DayOfWeek).ToString();
                        SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "'";
                        NFHDS = objdata.ReturnMultipleValue(SSQL);
                        if (NFHDS.Rows.Count <= 0)
                        {
                            Assign_Week_Name = "";
                        }
                        else
                        {
                            Assign_Week_Name = NFHDS.Rows[0][0].ToString();
                        }
                        if (Employee_Week_Name == Assign_Week_Name)
                        {
                            AutoDTable.Rows[mGrdRow][3] = "WH / " + time_Check_dbl;
                            AutoDTable.Rows[mGrdRow][7] = "WH / " + time_Check_dbl;
                            AutoDTable.Rows[mGrdRow][10] = "WH / " + time_Check_dbl;
                            //fg.set_TextMatrix(mGrdRow, 3, "WH / " + time_Check_dbl);
                            //fg.set_TextMatrix(mGrdRow, 7, "WH / " + time_Check_dbl);
                        }
                        else
                        {
                            AutoDTable.Rows[mGrdRow][3] = time_Check_dbl;
                            AutoDTable.Rows[mGrdRow][7] = time_Check_dbl;
                            AutoDTable.Rows[mGrdRow][10] = time_Check_dbl;


                            //  fg.set_TextMatrix(mGrdRow, 3, time_Check_dbl);
                            // fg.set_TextMatrix(mGrdRow, 7, time_Check_dbl);
                        }
                        //fg.set_TextMatrix(mGrdRow, 3, time_Check_dbl)
                    }
                }



            }

        }
        catch (Exception ex)
        {

            return;
        }





    }


}






