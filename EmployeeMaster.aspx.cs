﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class EmployeeMaster : System.Web.UI.Page
{
    string category;
    string ss;
    string active;
    string SSQL;
    string SessionUserType;
    DataTable mDataSet = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    System.Web.UI.WebControls.DataGrid GridView1 =
                  new System.Web.UI.WebControls.DataGrid();


    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Employee Master";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }
            
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();

            active = Request.QueryString["IsActive"].ToString();
            category = Request.QueryString["Category"].ToString();

            OtherReports();

           

            GridView1.HeaderStyle.Font.Bold = true;
            GridView1.DataSource = AutoDataTable;
            GridView1.DataBind();
            string attachment = "attachment;filename=EMPLOYEE MASTER.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";

            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            GridView1.RenderControl(htextw);
            Response.Write("<table>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td font-Bold='true' colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td font-Bold='true' colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">EMPLOYEE MASTER REPORT &nbsp;&nbsp;&nbsp;</a>");
            Response.Write("<a style=\"font-weight:bold\">" + ss + "</a>");
            Response.Write("<a style=\"font-weight:bold\">--" + category + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            //Response.Write("<tr Font-Bold='true' align='center'>");
            //Response.Write("<td colspan='10'>");
            //Response.Write("<a style=\"font-weight:bold\"> FROM DATE -" + Txtfrom.Text + "</a>");
            //Response.Write("&nbsp;&nbsp;&nbsp;");
            //Response.Write("<a style=\"font-weight:bold\"> TO DATE -" + TxtToDate.Text + "</a>");
            //Response.Write("</td>");
            //Response.Write("</tr>");


            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }

    }


    public void OtherReports()
    {
        //if (category == "STAFF")
        //{
            SSQL = "";
            SSQL = "select EmpNo,ExistingCode,DeptName,DOJ,FirstName,LastName,Designation,ShiftType,Wages,BaseSalary,ESINo,";
            SSQL = SSQL + "PFNo,OTEligible,SubCatName";
            SSQL = SSQL + " From Employee_Mst";
            SSQL = SSQL + " Where LocCode='" + SessionLcode + "'";
            if (active == "Yes")
            {
                SSQL = SSQL + "and IsActive='Yes' ";
            }
            else if (active == "No")
            {
                SSQL = SSQL + "and IsActive='No' ";
            }
            if (category == "STAFF")
            {
                SSQL = SSQL + "and CatName='STAFF' ";
            }
            else if (category == "LABOUR")
            {
                SSQL = SSQL + "and CatName='LABOUR' ";
            }


            //SSQL = SSQL + " And CatName = '" + category + "'";

            //if (SessionUserType == "2")
            //{
            //    SSQL = SSQL + " And IsNonAdmin='1'";
            //}
        
            //if (SessionUserType == "1")
            //{
            //    SSQL = SSQL + " And PFNo <> '' And PFNo IS Not Null And PFNo <> '0' And PFNo <> 'o' And PFNo <> 'A'";
            //    SSQL = SSQL + " And PFNo <> 'NULL'";
            //}

            //SSQL = SSQL + " And IsActive = '" + active + "'  Order by DeptName ASC";
        //}
        //else
        //{
        //    SSQL = "";
        //    SSQL = "select EmpNo,ExistingCode,DeptName,FirstName,LastName,Designation,ShiftType,Wages,BaseSalary,ESINo,";
        //    SSQL = SSQL + "PFNo,OTEligible,SubCatName";
        //    SSQL = SSQL + " From Employee_Mst";
        //    SSQL = SSQL + " Where LocCode='" + SessionLcode + "'";
        //    SSQL = SSQL + " And CatName = '" + category + "'";

        //    if (SessionUserType == "3")
        //    {
        //        SSQL = SSQL + " And IsNonAdmin='1'";
        //    }
        //    //if (SessionUserType == "1")
        //    //{
        //    //    SSQL = SSQL + " And PFNo <> '' And PFNo IS Not Null And PFNo <> '0' And PFNo <> 'o' And PFNo <> 'A'";
        //    //    SSQL = SSQL + " And PFNo <> 'NULL'";
        //    //}
        //    SSQL = SSQL + " And IsActive = '" + active + "'  Order by DeptName ASC";

        //}

          
            mDataSet = objdata.ReturnMultipleValue(SSQL);

            AutoDataTable.Columns.Add("EmpNo");
            AutoDataTable.Columns.Add("ExistingCode");
            AutoDataTable.Columns.Add("DeptName");
            AutoDataTable.Columns.Add("DOJ");
        
            AutoDataTable.Columns.Add("FirstName");
            AutoDataTable.Columns.Add("LastName");
            AutoDataTable.Columns.Add("Designation");
            AutoDataTable.Columns.Add("Shift Type");
            AutoDataTable.Columns.Add("Wages");
            AutoDataTable.Columns.Add("BaseSalary");
            AutoDataTable.Columns.Add("Eligible ESI");
            AutoDataTable.Columns.Add("ESINo");
            AutoDataTable.Columns.Add("Eligible PF");
            AutoDataTable.Columns.Add("PFNo");
            AutoDataTable.Columns.Add("OTEligible");
            AutoDataTable.Columns.Add("Subcategory");


            if (mDataSet.Rows.Count <= 0)
            {
                return;
            }
            else
            {
                for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[iRow][0] = mDataSet.Rows[iRow][0];
                    AutoDataTable.Rows[iRow][1] = mDataSet.Rows[iRow][1];
                    AutoDataTable.Rows[iRow][2] = mDataSet.Rows[iRow][2];
                    AutoDataTable.Rows[iRow][3] = mDataSet.Rows[iRow][3];
                    AutoDataTable.Rows[iRow][4] = mDataSet.Rows[iRow][4];
                    AutoDataTable.Rows[iRow][5] = mDataSet.Rows[iRow][5];
                    AutoDataTable.Rows[iRow][6] = mDataSet.Rows[iRow][6];
                    AutoDataTable.Rows[iRow][7] = mDataSet.Rows[iRow][7];
                    AutoDataTable.Rows[iRow][8] = mDataSet.Rows[iRow][8];
                    AutoDataTable.Rows[iRow][9] = mDataSet.Rows[iRow][9];
                   

                    if (mDataSet.Rows[iRow][10].ToString() == "" || mDataSet.Rows[iRow][10].ToString() == "NULL")
                    {
                        AutoDataTable.Rows[iRow][10] = "NO";
                    }
                    else
                    {
                        AutoDataTable.Rows[iRow][10] = "YES";

                    }

                 

                    if (mDataSet.Rows[iRow][11].ToString() == "" || mDataSet.Rows[iRow][11].ToString() == "NULL")
                    {
                        AutoDataTable.Rows[iRow][11] = "NO";
                    }
                    else
                    {
                        AutoDataTable.Rows[iRow][11] = "YES";
                    }

                    AutoDataTable.Rows[iRow][12] = mDataSet.Rows[iRow][10];
                    AutoDataTable.Rows[iRow][13] = mDataSet.Rows[iRow][11];
                    
                    AutoDataTable.Rows[iRow][14] = mDataSet.Rows[iRow][12];
                    AutoDataTable.Rows[iRow][15] = mDataSet.Rows[iRow][13];
                }
            }
        }
    


    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }


    //protected void UploadDataTableToExcel(DataTable dtRecords)
    //{

    //    grid.DataSource = dtRecords;
    //    grid.DataBind();
    //    string attachment = "attachment;filename=Payslip.xls";
    //    Response.ClearContent();
    //    Response.AddHeader("content-disposition", attachment);
    //    Response.ContentType = "application/ms-excel";

    //    System.IO.StringWriter stw = new System.IO.StringWriter();
    //    HtmlTextWriter htextw = new HtmlTextWriter(stw);
    //    grid.RenderControl(htextw);

    //    Response.Write("<table>");

    //    Response.Write("<tr align='Center'>");
    //    Response.Write("<td colspan='10'>");
    //    Response.Write("" + CompanyCode + "");
    //    Response.Write("" + LocationCode + "");
    //    Response.Write("</td>");
    //    Response.Write("</tr>");

    //    Response.Write("<tr align='Center'>");
    //    Response.Write("<td colspan='10'>");
    //    Response.Write("EMPLOYEE MASTER");
    //    Response.Write("</td>");
    //    Response.Write("</tr>");

        

    //    Response.Write("<tr align='Center'>");
    //    Response.Write("<td colspan='10'>");
    //    Response.Write("" + Date + "");
    //    Response.Write("--");
    //    Response.Write("" + Date2 + "");
    //    Response.Write("</td>");
    //    Response.Write("</tr>");

    //    Response.Write("</table>");

    //    Response.Write(stw.ToString());
    //    Response.End();
    //    Response.Clear();
    //}

}
