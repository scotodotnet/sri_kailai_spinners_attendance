﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class PayRollOTHours : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    //SqlConnection con;


    BALDataAccess objdata = new BALDataAccess();
    string cc = "EVEREADY SPINING MILL";
    //string SessionAdmin = "admin";
    //string SessionCcode = "ESM";
    //string SessionLcode = "UNIT II";
    

    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();

    string SSQL = "";
    DataTable dsEmployee = new DataTable();
    DataTable mDataSet = new DataTable();

    string[] Time_Minus_Value_Check;
    //string Date1 = "";
    //string Date2 = "";
    //string ModeType = "";
    //string ShiftType1 = "";
    //string ddlShiftType = "";
    int shiftCount;
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string SessionCcode;
    string SessionLcode;
    string fromdate;
    string todate;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | PayRoll OT Hours";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }
            fromdate = Request.QueryString["FromDate"].ToString();
            todate = Request.QueryString["ToDate"].ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();

            //ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            //Date1 = Request.QueryString["dtpFromDate"].ToString();
            //Date2 = Request.QueryString["dtpToDate"].ToString();
            //ddlShiftType = Request.QueryString["ddlShiftType"].ToString();
            System.Web.UI.WebControls.DataGrid grid =
                                      new System.Web.UI.WebControls.DataGrid();

            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("MachineID_Enc");
            AutoDTable.Columns.Add("EmpNo");
            AutoDTable.Columns.Add("ExistingCode");
            AutoDTable.Columns.Add("FirstName");


            DataCells.Columns.Add("EmployeeNo");
            DataCells.Columns.Add("ExistingCode");
            DataCells.Columns.Add("DeptName");
            DataCells.Columns.Add("FirstName");




            Fill_Day_Attd_Between_Dates_OT();







            grid.DataSource = DataCells;
            grid.DataBind();
            string attachment = "attachment;filename=ParRollOTHoursReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + cc + "</a>");
            Response.Write("  ");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">MONTHLY CONSOLIDATED OT REPORT</a>");
            Response.Write("  ");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">FROM:" + fromdate.ToString() + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">TO:" + todate.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
    }
    public void Fill_Day_Attd_Between_Dates_OT()
    {



        string SSQL = "";

        SSQL = "";
        SSQL = "select isnull(DeptName,'') as [DeptName], Cast(MachineID As int) As MachineID,MachineID_Encrypt As MachineID_Enc";
        SSQL = SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
        SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
        //SSQL = SSQL & ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName]"
        SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode.ToString() + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "' And IsActive='Yes' And OTEligible='Yes'";
        SSQL = SSQL + " Order By DeptName, MachineID";

        dsEmployee = objdata.ReturnMultipleValue(SSQL);
        if (dsEmployee.Rows.Count <= 0)
            return;
        int i1 = 0;

        for (int j = 0; j < dsEmployee.Rows.Count; j++)
        {
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();
            AutoDTable.Rows[i1][0] = dsEmployee.Rows[j]["DeptName"].ToString();
            AutoDTable.Rows[i1][1] = dsEmployee.Rows[j]["MachineID"].ToString();
            AutoDTable.Rows[i1][2] = dsEmployee.Rows[j]["MachineID_Enc"].ToString();
            AutoDTable.Rows[i1][3] = dsEmployee.Rows[j]["EmpNo"].ToString();
            AutoDTable.Rows[i1][4] = dsEmployee.Rows[j]["ExistingCode"].ToString();
            AutoDTable.Rows[i1][5] = dsEmployee.Rows[j]["FirstName"].ToString();
            i1++;
        }


        writeAttend_OT_Count();

    }

    public void writeAttend_OT_Count()
    {
        int intI = 1;
        int intK = 1;
        int intCol = 0;




        date1 = Convert.ToDateTime(fromdate.ToString());
        string dat = todate.ToString();
        Date2 = Convert.ToDateTime(dat);

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;



        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

            //DataCells.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

            daycount -= 1;
            daysAdded += 1;
        }

        intI = 4;
        intK = 1;

        int DaysCountTotal1 = (int)((Date2 - date1).TotalDays);
        int EndCount1 = 6 + DaysCountTotal1 + 1;

        AutoDTable.Columns.Add("Total Hours");
        DataCells.Columns.Add("Total Hours");
        int k = 0;
        for (int intRow = 0; intRow < AutoDTable.Rows.Count; intRow++)
        {

            intK = 1;
            int colIndex = intK;
            bool isPresent = false;
            string Total_TIme_work = "00:00";
            DateTime TempDateTime;
            TimeSpan TempTimeSpan;
            Int32 Total_Calculate = 0;
            ArrayList OT_Array_Value = new ArrayList();
            OT_Array_Value.Clear();
            intK = 6;
            for (intCol = 0; intCol <= daysAdded - 1; intCol++)
            {
                isPresent = false;
                string Machine_ID_Str = "";
                string OT_Week_OFF_Machine_No = null;
                string Date_Value_Str = "";
                string Date_Value_Str1 = "";
                DataTable mLocalDS = new DataTable();
                string Time_IN_Str = "";
                string Time_Out_Str = "";
                string Total_Time_get = "";
                string Emp_Total_Work_Time_1 = "00:00";
                string Final_OT_Work_Time_1 = "00:00";
                TimeSpan ts_get;
                Int32 time_Check_dbl = 0;

                isPresent = false;
                Machine_ID_Str = AutoDTable.Rows[intRow][2].ToString();
                OT_Week_OFF_Machine_No = AutoDTable.Rows[intRow][1].ToString();

                DateTime dtime = date1.AddDays(intCol);
                DateTime dtime1 = dtime.AddDays(1);

                time_Check_dbl = 0;
                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                mLocalDS = objdata.ReturnMultipleValue(SSQL);
                if (mLocalDS.Rows.Count <= 0)
                {
                    Time_IN_Str = "";
                }
                else
                {
                    Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                }
                DataTable mLocalDS1 = new DataTable();
                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT ASC";
                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);

                if (mLocalDS1.Rows.Count <= 0)
                {
                    Time_Out_Str = "";
                }
                else
                {
                    Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                }
                if (mLocalDS.Rows.Count > 1)
                {
                    for (int tin = 0; tin <= mLocalDS.Rows.Count - 1; tin++)
                    {
                        Time_IN_Str = mLocalDS.Rows[tin][0].ToString();

                        if (mLocalDS1.Rows.Count > tin)
                        {
                            Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                        }
                        else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                        {
                            Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                        }
                        else
                        {
                            Time_Out_Str = "";
                        }
                        TimeSpan ts4;
                        ts4 = Convert.ToDateTime(String.Format("{0:HH\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;

                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                        }

                        if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                        {
                            time_Check_dbl = time_Check_dbl;
                        }
                        else
                        {
                            DateTime d1 = Convert.ToDateTime(Time_IN_Str);
                            DateTime d2 = Convert.ToDateTime(Time_Out_Str);

                            TimeSpan ts;
                            ts = d2.Subtract(d1);
                            Total_Time_get = Convert.ToString(ts.Hours);
                            ts4 = ts4.Add(ts);
                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                Emp_Total_Work_Time_1 = "00:00";
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                Emp_Total_Work_Time_1 = "00:00";

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                d2 = Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = d2.Subtract(d1);
                                Total_Time_get = Convert.ToString(ts.Hours);
                                time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Emp_Total_Work_Time_1 = "00:00";
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    Emp_Total_Work_Time_1 = "00:00";
                            }
                            else
                            {
                                time_Check_dbl = Convert.ToInt32(time_Check_dbl + Total_Time_get);
                            }
                        }
                    }

                }
                else
                {
                    TimeSpan ts4;
                    ts4 = Convert.ToDateTime(String.Format("{0:HH\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Time_IN_Str = "";
                    }
                    else
                    {
                        Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                    }
                    for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
                    {
                        if (mLocalDS1.Rows.Count <= 0)
                        {
                            Time_Out_Str = "";
                        }
                        else
                        {
                            Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                        }

                    }
                    if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                    {
                        time_Check_dbl = 0;
                        //AutoDTable.Rows[intRow][day_col] = time_Check_dbl;
                    }
                    else
                    {
                        DateTime d1 = Convert.ToDateTime(Time_IN_Str);
                        DateTime d2 = Convert.ToDateTime(Time_Out_Str);
                        TimeSpan ts;
                        ts = d2.Subtract(d1);
                        ts = d2.Subtract(d1);
                        Total_Time_get = Convert.ToString(ts.Hours);
                        ts4 = ts4.Add(ts);
                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            Emp_Total_Work_Time_1 = "00:00";
                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                            Emp_Total_Work_Time_1 = "00:00";
                        //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                        if (Left_Val(Total_Time_get, 1) == "-")
                        {
                            d2 = Convert.ToDateTime(Time_Out_Str).AddDays(1);
                            ts = d2.Subtract(d1);
                            ts = d2.Subtract(d1);
                            Total_Time_get = Convert.ToString(ts.Hours);
                            //& ":" & Trim(ts.Minutes)
                            time_Check_dbl = Convert.ToInt32(Total_Time_get);
                            //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                            Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                Emp_Total_Work_Time_1 = "00:00";
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                Emp_Total_Work_Time_1 = "00:00";
                        }
                        else
                        {
                            time_Check_dbl = Convert.ToInt32(Total_Time_get);
                        }
                    }
                }

                string Emp_Total_Work_Time = "";
                string Final_OT_Work_Time = "00:00";
                string Final_OT_Work_Time_Val = "00:00";
                Emp_Total_Work_Time = Emp_Total_Work_Time_1;
                string Date_Value = dtime.ToString("yyyy-MM-dd");
                string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value + "',103)";
                //  string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + dtime.ToString("dd/MM/yyyy") + "'";
                mLocalDS = objdata.ReturnMultipleValue(qry_nfh);

                if (mLocalDS.Rows.Count > 0)
                {
                    if (time_Check_dbl == 0)
                    {
                        //Skip

                    }
                    else
                    {
                        Final_OT_Work_Time = "NH / " + Emp_Total_Work_Time;
                        Final_OT_Work_Time_Val = Emp_Total_Work_Time;
                        isPresent = true;
                    }
                }
                else
                {


                    double Work_Time = 0;
                    double OT_Time = 0;
                    Int32 Final_OT_Time_Check = 0;
                    SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and Working_Hours <> '' ANd CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "' And IsActive='Yes'";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);

                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Final_OT_Time_Check = 9;
                    }
                    else
                    {

                        if (mLocalDS.Rows[0]["Working_Hours"].ToString() == "")
                        {
                            Work_Time = 0;
                        }
                        else
                        {
                            Work_Time = Convert.ToInt16(mLocalDS.Rows[0]["Working_Hours"].ToString());
                        }
                        if (mLocalDS.Rows[0]["OT_Hours"].ToString() == "")
                        {
                            OT_Time = 0;
                        }
                        else
                        {
                            OT_Time = Convert.ToInt16(mLocalDS.Rows[0]["OT_Hours"].ToString());
                        }


                        //Work_Time = (!string.IsNullOrEmpty(mLocalDS.Rows[0]["Working_Hours"]) ? mLocalDS.Rows[0]["Working_Hours"] : 0);
                        //OT_Time = (!string.IsNullOrEmpty(mLocalDS.Rows[0]["OT_Hours"]) ? mLocalDS.Rows[0]["OT_Hours"] : 0);
                        if (Work_Time == 0)
                        {
                            Final_OT_Time_Check = 9;
                        }
                        else
                        {
                            Final_OT_Time_Check = Convert.ToInt32(Work_Time + OT_Time);
                        }
                    }
                    Final_OT_Time_Check = 9;
                    if (time_Check_dbl >= Final_OT_Time_Check)
                    {
                        isPresent = true;
                        string Time_Minus = "08:00";
                        if (Work_Time == 0)
                        {
                            Time_Minus = "08:00";
                        }
                        else
                        {
                            Time_Minus = Work_Time + ":00";
                        }
                        DateTime date_TotalTime = Convert.ToDateTime(Emp_Total_Work_Time);
                        DateTime date_TotalTimeMinus = Convert.ToDateTime(Time_Minus);
                        TimeSpan TSminus;
                        TSminus = date_TotalTimeMinus.Subtract(date_TotalTime);
                        TSminus = date_TotalTimeMinus.Subtract(date_TotalTime);
                        if (Left_Val(Convert.ToString(TSminus.Hours), 1) == "-")
                        {
                            string TSminus_str1 = Convert.ToString(TSminus.Hours);
                            int l1 = TSminus_str1.Length;
                            Final_OT_Work_Time = Right_Val(Convert.ToString(TSminus.Hours), l1 - 1);
                            Final_OT_Work_Time_Val = Final_OT_Work_Time;
                        }
                        else
                        {
                            Final_OT_Work_Time = Convert.ToString(TSminus.Hours);
                            Final_OT_Work_Time_Val = Final_OT_Work_Time;
                        }
                        if (Left_Val(Convert.ToString(TSminus.Minutes), 1) == "-")
                        {
                            string TSminus_str2 = Convert.ToString(TSminus.Minutes);
                            int l2 = TSminus_str2.Length;
                            Final_OT_Work_Time = Final_OT_Work_Time + ":" + Right_Val(Convert.ToString(TSminus.Minutes), l2 - 1);
                            Final_OT_Work_Time_Val = Final_OT_Work_Time;
                        }
                        else
                        {
                            Final_OT_Work_Time = Final_OT_Work_Time + ":" + TSminus.Minutes;
                            Final_OT_Work_Time_Val = Final_OT_Work_Time;
                        }


                    }


                    // }
                    ts_get = Convert.ToDateTime(String.Format("{0:HH\\:mm}", Final_OT_Work_Time_Val)).TimeOfDay;

                    TimeSpan tt = ts_get;

                    TempTimeSpan = tt;
                    Total_TIme_work = tt.Hours + ":" + tt.Minutes;
                }

                if (isPresent == true)
                {
                    OT_Array_Value.Add(Final_OT_Work_Time);
                    Total_Calculate = Total_Calculate + 1;
                }
                else
                {
                    OT_Array_Value.Add("0");
                }
                colIndex += shiftCount;
                intK += 1;
            }
            if (Total_Calculate == 0)
            {
                //Skip
            }
            else
            {
                intK = 1;

                //Add OT Time
                int day_col = 6;
                int day_col1 = 4;

                TimeSpan Total_Hr = new TimeSpan();
                DataCells.NewRow();
                DataCells.Rows.Add();
                for (int intCol23 = 0; intCol23 <= daysAdded - 1; intCol23++)
                {
                    if (OT_Array_Value[intCol23] == "0")
                    {
                        AutoDTable.Rows[intRow][day_col] = "00:00";
                        // DataCells.Rows[k][day_col1] = "00:00";

                    }
                    else if (Left_Val(Convert.ToString(OT_Array_Value[intCol23]), 2) == "WH")
                    {
                        AutoDTable.Rows[intRow][day_col] = OT_Array_Value[intCol23];

                        //DataCells.Rows[k][day_col1] = OT_Array_Value[intCol23];

                    }
                    else if (Left_Val(Convert.ToString(OT_Array_Value[intCol23]), 2) == "NH")
                    {
                        AutoDTable.Rows[intRow][day_col] = OT_Array_Value[intCol23];
                        //DataCells.Rows[k][day_col1] = OT_Array_Value[intCol23];

                    }
                    else
                    {
                        AutoDTable.Rows[intRow][day_col] = OT_Array_Value[intCol23];

                        // DataCells.Rows[k][day_col1] = OT_Array_Value[intCol23];
                    }

                    string tot_hr = AutoDTable.Rows[intRow][day_col].ToString();

                    if (tot_hr != "")
                    {

                        TimeSpan temp1 = Convert.ToDateTime(String.Format("{0:HH\\:mm}", tot_hr)).TimeOfDay;

                        Total_Hr = Total_Hr.Add(temp1);


                    }


                    intK += 1;
                    day_col += 1;
                    day_col1 += 1;

                }

                //DateTime d = new DateTime(1, 1, 1, 23, 12, 0);
                //var res = d.ToString("hh:mm tt");   // this show  11:12 Pm
                //var res2 = d.ToString("HH:mm");  // this show  23:12

                string totHour_str;
                string totMinutes_str;

                int totalHours = (int)Total_Hr.TotalHours;
                string totalHour_str = Convert.ToString(totalHours);
                if (totalHour_str.Length == 1)
                {
                    totHour_str = "0" + totalHour_str;
                }
                else
                {
                    totHour_str = totalHour_str;
                }
                int totalMinutes = (int)Total_Hr.Minutes;
                string totalMin_str = Convert.ToString(totalMinutes);
                if (totalMin_str.Length == 1)
                {
                    totMinutes_str = "0" + totalMin_str;
                }
                else
                {
                    totMinutes_str = totalMin_str;
                }


                string total_OTHour = totHour_str + ":" + totMinutes_str;
                string total_OTHour_str = total_OTHour.Substring(0, 5);
                AutoDTable.Rows[intRow]["Total Hours"] = total_OTHour_str;
                //TimeSpan tot_DT = AutoDTable.Rows[intRow]["Total Hours"];
                //  Convert.ToDateTime(AutoDTable.Rows[intRow]["Total Hours"]);
                // var tot_var = tot_DT.ToString("hh:mm");
                DataCells.Rows[k]["Total Hours"] = total_OTHour_str;
                DataCells.Rows[k]["DeptName"] = AutoDTable.Rows[intRow]["DeptName"];
                DataCells.Rows[k]["EmployeeNo"] = AutoDTable.Rows[intRow]["EmpNo"];
                DataCells.Rows[k]["ExistingCode"] = AutoDTable.Rows[intRow]["ExistingCode"];
                DataCells.Rows[k]["FirstName"] = AutoDTable.Rows[intRow]["FirstName"];







                intI += 1;
                k += 1;
            }


        }




    }
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    public string Right_Val(string Value, int Length)
    {
        // Recreate a RIGHT function for string manipulation
        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }



    protected void Button3_Click(object sender, EventArgs e)
    {

       
    }

}