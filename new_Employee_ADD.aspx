﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="new_Employee_ADD.aspx.cs" Inherits="new_Employee_ADD" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
    
             $(document).ready(function () {
            $('#tableEmployee').dataTable();
        });
        </script>
        
         <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate> 
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">New Employee</li>
                       <h4>
                       </h4>
                        <h4>
                       </h4>
                        </h4> 
                    </ol>
                </div>
                
<div id="main-wrapper" class="container">
<div class="row">
       <div class="col-md-20">
          <div class="col-md-12">
           <div class="panel panel-white">
            <div class="panel panel-primary">
            <div class="panel-heading clearfix">
                <h3 class="panel-title">New Employee</h3>
            </div>
            </div>
           
       <form class="form-horizontal">
          <div class="panel-body">
            <div class="col-md-20">
            <div class="row">
            
             
              <asp:Button ID="btnNEWEMPentry" class="btn btn-success" runat="server" 
                  Text="NEW EMPLOYEE ENTRY" onclick="btnNEWEMPentry_Click"  />
              </div>
              </div>
             
           <div class="form-group row"></div>
              
              <div class="col-md-12">
              <div class="row">
              
                <%--  <div class="table-responsive">--%>
        <asp:Repeater ID="rptrEmployee" runat="server" onitemcommand="Repeater1_ItemCommand">
                    <HeaderTemplate>
                       <table id="tableEmployee" class="display table">
                    <thead>
                        <tr>
                        <th>MachineID</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Gender</th>
                        <th>DeptName</th>
                        <th>Designation</th>
                        <th>DateOfBirth</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        
                       </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                 <td>
                    <%# DataBinder.Eval(Container.DataItem, "MachineID")%>
                    </td>
                 
                    <td>
                    <%# DataBinder.Eval(Container.DataItem, "FirstName")%>
                    </td>
                    <td>
                    <%# DataBinder.Eval(Container.DataItem, "CatName")%>
                    </td>
                    <td><%# DataBinder.Eval(Container.DataItem, "Gender")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "DeptName")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "Designation")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "DOB")%></td>
                    
                     <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("MachineID") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("MachineID") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
                        </div>
             
              
              
              </div>
           </form>
              </div>
              
                            
                            </div>
                            </div>
         
         </div>
              
              
</div>
      </ContentTemplate>
 </asp:UpdatePanel>

</asp:Content>

