﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;



public partial class new_emp1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();
    AdolscentDetailsClass objAdlDetails = new AdolscentDetailsClass();
    ExperienceDetailsClass objExpDetails = new ExperienceDetailsClass();
    

    EmployeeDetailsEpayClass objEpayEmpDetails = new EmployeeDetailsEpayClass();
    OfficialProfileEPayClass objEpayProfDetails = new OfficialProfileEPayClass();
    SalaryMasterEpayClass objEpaySalaryMst = new SalaryMasterEpayClass();
    DocumentClass objdoc = new DocumentClass();

    bool ErrFlag = false;
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SSQL;
    string fn;
    string MachineID;
    string SessionSpay;
    string SessionEpay;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionSpay = Session["SessionSpay"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            MachineID = Session["MachineID"].ToString();
            //MachineID = Session["MachineID"].ToString();

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Employee Details";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");

                DropDown_Department();
                Dropdown_Desgination();
                DropDown_EmpType();
                DropDown_WagesType();
                DropDown_Bank();
                DropDown_Weekoff();
                //123
                Drop_Qulify();


                if (MachineID != "")
                {
                    txtMachineID.Text = MachineID;
                    DisplayEmployeeDetails();
                    btn_save.Text = "Update";
                }
                else
                {
                    btn_save.Text = "Save";
                }

            }

        }

    }
    
    //123
    public void Drop_Qulify()
    {
        DataTable dt = new DataTable();
        string Query = "Select QualificationCd,QualificationNm from [" + SessionEpay + "]..MstQualification order by QualificationCd";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        ddlQulify.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["QualificationCd"] = 0;
        dr["QualificationNm"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        ddlQulify.DataTextField = "QualificationNm";
        ddlQulify.DataValueField = "QualificationCd";
        ddlQulify.DataBind();

    }
    protected void txtdateofbirth_TextChanged(object sender, EventArgs e)
    {
        //try
        //{
        //    if (txtdateofbirth.Text != "")
        //    {
        //        int dt = System.DateTime.Now.Year;
        //        string date1Day = this.txtdateofbirth.Text.Remove(2);
        //        string date1Month = this.txtdateofbirth.Text.Substring(3, 2);
        //        string date1Year = this.txtdateofbirth.Text.Substring(6);
        //        int date2year = Convert.ToInt32(date1Year.ToString());
        //        int datediff = dt - date2year;

        //        if (datediff >= 0)
        //        {
        //            string age = Convert.ToString(datediff);
        //            txtage.Text = age.ToString();

        //        }
        //        else
        //        {
        //            bool ErrFlag = false;
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Give Valid Date');", true);
        //            ErrFlag = true;
        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Give Valid Date');", true);

        //}


    }

    public void AgeCalculation()
    {
        try
        {
            if (txtdateofbirth.Text != "")
            {
                int dt = System.DateTime.Now.Year;
                string date1Day = this.txtdateofbirth.Text.Remove(2);
                string date1Month = this.txtdateofbirth.Text.Substring(3, 2);
                string date1Year = this.txtdateofbirth.Text.Substring(6);
                int date2year = Convert.ToInt32(date1Year.ToString());
                int datediff = dt - date2year;

                if (datediff >= 0)
                {
                    string age = Convert.ToString(datediff);
                    txtage.Text = age.ToString();

                }
                else
                {
                    bool ErrFlag = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Give Valid Date');", true);
                    ErrFlag = true;
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Give Valid Date');", true);

        }
    }

   
    public void DropDown_Department()
    {
        DataTable dt = new DataTable();
        dt = objdata.Dropdown_Department();
        ddlDepartment.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["DeptCode"] = 0;
        dr["DeptName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptCode";
        ddlDepartment.DataBind();
       
    }

    public void Dropdown_Desgination()
    {
        DataTable dt = new DataTable();
        dt = objdata.Dropdown_Desgination();
        ddlDesignation.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["DesignNo"] = 0;
        dr["DesignName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        ddlDesignation.DataTextField = "DesignName";
        ddlDesignation.DataValueField = "DesignNo";
        ddlDesignation.DataBind();
       
    }

    public void DropDown_EmpType()
    {
        DataTable dt = new DataTable();
        dt = objdata.DropDown_EmpType();
        ddlShiftType.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["TypeID"] = 0;
        dr["TypeName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        ddlShiftType.DataTextField = "TypeName";
        ddlShiftType.DataValueField = "TypeID";
        ddlShiftType.DataBind();
       
    }

    public void DropDown_WagesType()
    {
        DataTable dt = new DataTable();
        dt = objdata.DropDown_WagesType();
        ddlWagesType.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["WagesTypeNo"] = 0;
        dr["WagesTypeName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "WagesTypeName";
        ddlWagesType.DataValueField = "WagesTypeNo";
        ddlWagesType.DataBind();
      
    }
    public void DropDown_Weekoff()
    {
        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = "select Weekoff from MstWeekoff";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlWeekOff.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["Weekoff"] = "--Select--";
        dr["Weekoff"] = "--Select--";
        dt.Rows.InsertAt(dr, 0);
        ddlWeekOff.DataTextField = "Weekoff";
        ddlWeekOff.DataValueField = "Weekoff";
        ddlWeekOff.DataBind();

 
    }
    public void DropDown_Bank()
    {
        DataTable dt = new DataTable();
        string Query = "Select BankCode,BankName from MstBank where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        ddlBankCode.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["BankCode"] = "--Select--";
        dr["BankCode"] = "--Select--";
        dt.Rows.InsertAt(dr, 0);
        ddlBankCode.DataTextField = "BankCode";
        ddlBankCode.DataValueField = "BankCode";
        ddlBankCode.DataBind();


    }
    protected void rbtnSalayThtough_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtnSalayThtough.SelectedValue == "1")
        {
            ddlBankCode.Enabled = false;
            txtbankName.Enabled = false;
            txtIFSCCode.Enabled = false;
            txtAccNo.Enabled = false;
        }
        else
        {
            ddlBankCode.Enabled = true;
            txtbankName.Enabled = true;
            txtIFSCCode.Enabled = true;
            txtAccNo.Enabled = true;

        }
    }
    protected void ddlBankCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string Query = "Select BankName,IFCcode from MstBank where BankCode='" + ddlBankCode.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtbankName.Text = dt.Rows[0]["BankName"].ToString();
            txtIFSCCode.Text = dt.Rows[0]["IFCcode"].ToString();
        }
    }



    protected void btn_save_Click(object sender, EventArgs e)
    {
       
        AgeCalculation();
        if (btn_save.Text == "Update")
        {
            objEmpDetails.MachineID_Encry = UTF8Encryption(txtMachineID.Text);
            objEmpDetails.Ccode = SessionCcode;
            objEmpDetails.Lcode = SessionLcode;
            objEmpDetails.EmpStatus = "1";
            objEmpDetails.EmpType = "a";
            objEmpDetails.AdminUser = "s";
            objEmpDetails.Category = ddlCategory.SelectedItem.Text;
            objEmpDetails.SubCategory = ddlsubcategory.SelectedItem.Text;
            objEmpDetails.MachineID = txtMachineID.Text;
            objEmpDetails.ExistingNo = txtExistingNo.Text;
            objEmpDetails.TokenNo = txtTicketNo.Text;
            objEmpDetails.FirstName = txtfirstname.Text;
            objEmpDetails.Initial = txtlastname.Text;
            //DateTime dob = Convert.ToDateTime(txtdateofbirth.Text);
            objEmpDetails.DOB = txtdateofbirth.Text;
            objEmpDetails.Age = txtage.Text;
            objEmpDetails.Gender = ddlGender.SelectedItem.Text;
            objEmpDetails.MaritalStatus = ddlmMaritalStatus.SelectedItem.Text;
            objEmpDetails.Nationality = txtNationality.Text;
            objEmpDetails.Religion = txtReligion.Text;
            objEmpDetails.BloodGroup = ddlBloodGroup.SelectedItem.Text;
            objEmpDetails.Height = txtHeight.Text;
            objEmpDetails.Weight = txtWeight.Text;
            objEmpDetails.Handicapped = ddlPhysicalChng.SelectedItem.Text;
            objEmpDetails.DOJ = txtdateofJoin.Text;
            objEmpDetails.IsActive = ddlActive.SelectedItem.Text;
            objEmpDetails.Department = ddlDepartment.SelectedItem.Text;
            objEmpDetails.Desgination = ddlDesignation.SelectedItem.Text;
            if (ddlCategory.SelectedItem.Text == "LABOUR")
            {
                objEmpDetails.ShiftType = "SHIFT";
            }
            else
            {
                objEmpDetails.ShiftType = "GENERAL";
            }
            
            objEmpDetails.StdWorkingHrs = txtStdWHr.Text;
            objEmpDetails.OTEligible = ddlOTEligible.SelectedItem.Text;
            objEmpDetails.OTHrs = txtOTHr.Text;
            objEmpDetails.WeekOff = ddlWeekOff.SelectedItem.Text;
            objEmpDetails.WagesType = ddlWagesType.SelectedItem.Text;
            objEmpDetails.BaseSalary = txtBasic.Text;
            objEmpDetails.PFEligible = ddlPFEligible.SelectedItem.Text;
            objEmpDetails.PFNumber = txtPFNum.Text;
            objEmpDetails.PFdoj = txtPFDOJ.Text;
            objEmpDetails.ESIEligible = ddlESIEligible.SelectedItem.Text;
            objEmpDetails.ESINumber = txtESINum.Text;
            objEmpDetails.ESIdoj = txtESIDOJ.Text;
            objEmpDetails.HostelRmNo = txtHstRoomNo.Text;
            objEmpDetails.BusRoute = txtBusRoute.Text;
            objEmpDetails.BusNo = txtBusNum.Text;
            objEmpDetails.RecuritThr = txtRecThrgh.Text;
            objEmpDetails.BankName = txtbankName.Text;
            objEmpDetails.BranchCode = ddlBankCode.SelectedValue;
            objEmpDetails.AccountNo = txtAccNo.Text;
            objEmpDetails.IFSCCode = txtIFSCCode.Text;
            objEmpDetails.Qualification = ddlQulify.SelectedValue;
            objEmpDetails.Nominee = txtNominee.Text;
            objEmpDetails.FamilyDetails = txtFatherName.Text + " " + txtMotherName.Text;
            objEmpDetails.GuardianName = txtGuardName.Text;
            objEmpDetails.PermanentAddr = txtPermanentAddr.Text;
            objEmpDetails.PermanentTlk = txtpermanentTlk.Text;
            objEmpDetails.PermanentDst = txtPermanentDst.Text;
            objEmpDetails.TempAddress = txtTempAddr.Text;
            objEmpDetails.TempTlk = txtTempTlk.Text;
            objEmpDetails.TempDst = txtTempDst.Text;
            objEmpDetails.IdentiMark1 = txtIMark1.Text;
            objEmpDetails.IdentiMark2 = txtIMark2.Text;
            objEmpDetails.EmpMobile = txtEmpMobileNum.Text;
            objEmpDetails.ParentsMobile1 = txtParentMob1.Text;
            objEmpDetails.ParentsMobile2 = txtParentMob2.Text;
            objEmpDetails.Reference = txtRefName.Text;
            objEmpDetails.BasicSalary = txtBasic.Text;
            objEmpDetails.Allowance1amt = txtAllowance1.Text;
            objEmpDetails.Allowance2amt = txtAllowance2.Text;
            objEmpDetails.Deduction1amt = txtDeduction1.Text;
            objEmpDetails.Deduction2amt = txtDeduction2.Text;
            objEmpDetails.PFS = txtPFSal.Text;
            objEmpDetails.Incentive_Eligible = ddlIncen_Eligible.SelectedValue;


            if (AdminChkBx.Checked == true)
            {
                objEmpDetails.IsNonAdmin = "1";
            }
            else
            {
                objEmpDetails.IsNonAdmin = "0";
            }

            objdata.UpdateEmployeeDetails(objEmpDetails);




            //objdata.EpayUpdateSalaryMasterDetails(objEpaySalaryMst);

            EpayEmployeeDetails();
            EpayOfficialDetails();
            EpaySalaryDetails();
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Update Successfully');", true);
            Response.Redirect("new_Employee_ADD.aspx");
            EpayEmployeeDetails();
            EpayOfficialDetails();
            EpaySalaryDetails();

        }
        else
        {

            DataTable dtdduplication = new DataTable();
            SSQL = "select * from Employee_Mst where MachineID='" + txtMachineID.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dtdduplication = objdata.ReturnMultipleValue(SSQL);
            if (dtdduplication.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Machine ID Already Exist');", true);
                ErrFlag = true;
            }
            else
            {
                objEmpDetails.MachineID_Encry = UTF8Encryption(txtMachineID.Text);
                objEmpDetails.Ccode = SessionCcode;
                objEmpDetails.Lcode = SessionLcode;
                objEmpDetails.EmpStatus = "1";
                objEmpDetails.EmpType = "a";
                objEmpDetails.AdminUser = "s";
                objEmpDetails.Category = ddlCategory.SelectedItem.Text;
                objEmpDetails.SubCategory = ddlsubcategory.SelectedItem.Text;
                objEmpDetails.MachineID = txtMachineID.Text;
                objEmpDetails.ExistingNo = txtExistingNo.Text;
                objEmpDetails.TokenNo = txtTicketNo.Text;
                objEmpDetails.FirstName = txtfirstname.Text;
                objEmpDetails.Initial = txtlastname.Text;
                DateTime dob = Convert.ToDateTime(txtdateofbirth.Text);
                objEmpDetails.DOB = dob.ToShortDateString();
                objEmpDetails.Age = txtage.Text;
                objEmpDetails.Gender = ddlGender.SelectedItem.Text;
                objEmpDetails.MaritalStatus = ddlmMaritalStatus.SelectedItem.Text;
                objEmpDetails.Nationality = txtNationality.Text;
                objEmpDetails.Religion = txtReligion.Text;
                objEmpDetails.BloodGroup = ddlBloodGroup.SelectedItem.Text;
                objEmpDetails.Height = txtHeight.Text;
                objEmpDetails.Weight = txtWeight.Text;
                objEmpDetails.Handicapped = ddlPhysicalChng.SelectedItem.Text;
                objEmpDetails.DOJ = txtdateofJoin.Text;
                objEmpDetails.IsActive = ddlActive.SelectedItem.Text;
                objEmpDetails.Department = ddlDepartment.SelectedItem.Text;
                objEmpDetails.Desgination = ddlDesignation.SelectedItem.Text;
                objEmpDetails.ShiftType = ddlShiftType.SelectedItem.Text;
                objEmpDetails.StdWorkingHrs = txtStdWHr.Text;
                objEmpDetails.OTEligible = ddlOTEligible.SelectedItem.Text;
                objEmpDetails.OTHrs = txtOTHr.Text;
                objEmpDetails.WeekOff = ddlWeekOff.SelectedItem.Text;
                objEmpDetails.WagesType = ddlWagesType.SelectedItem.Text;
                objEmpDetails.BaseSalary = txtBasic.Text;
                objEmpDetails.PFEligible = ddlPFEligible.SelectedItem.Text;
                objEmpDetails.PFNumber = txtPFNum.Text;
                objEmpDetails.PFdoj = txtPFDOJ.Text;
                objEmpDetails.ESIEligible = ddlESIEligible.SelectedItem.Text;
                objEmpDetails.ESINumber = txtESINum.Text;
                objEmpDetails.ESIdoj = txtESIDOJ.Text;
                objEmpDetails.HostelRmNo = txtHstRoomNo.Text;
                objEmpDetails.BusRoute = txtBusRoute.Text;
                objEmpDetails.BusNo = txtBusNum.Text;
                objEmpDetails.RecuritThr = txtRecThrgh.Text;
                objEmpDetails.BankName = txtbankName.Text;
                objEmpDetails.BranchCode = ddlBankCode.SelectedValue;
                objEmpDetails.AccountNo = txtAccNo.Text;
                objEmpDetails.IFSCCode = txtIFSCCode.Text;
                objEmpDetails.Qualification = ddlQulify.SelectedValue;
                objEmpDetails.Nominee = txtNominee.Text;
                objEmpDetails.FamilyDetails = txtFatherName.Text + " " + txtMotherName.Text;
                objEmpDetails.GuardianName = txtGuardName.Text;
                objEmpDetails.PermanentAddr = txtPermanentAddr.Text;
                objEmpDetails.PermanentTlk = txtpermanentTlk.Text;
                objEmpDetails.PermanentDst = txtPermanentDst.Text;
                objEmpDetails.TempAddress = txtTempAddr.Text;
                objEmpDetails.TempTlk = txtTempTlk.Text;
                objEmpDetails.TempDst = txtTempDst.Text;
                objEmpDetails.IdentiMark1 = txtIMark1.Text;
                objEmpDetails.IdentiMark2 = txtIMark2.Text;
                objEmpDetails.EmpMobile = txtEmpMobileNum.Text;
                objEmpDetails.ParentsMobile1 = txtParentMob1.Text;
                objEmpDetails.ParentsMobile2 = txtParentMob2.Text;
                objEmpDetails.Reference = txtRefName.Text;
                objEmpDetails.BasicSalary = txtBasic.Text;
                objEmpDetails.Allowance1amt = txtAllowance1.Text;
                objEmpDetails.Allowance2amt = txtAllowance2.Text;
                objEmpDetails.Deduction1amt = txtDeduction1.Text;
                objEmpDetails.Deduction2amt = txtDeduction2.Text;
                objEmpDetails.PFS = txtPFSal.Text;
                if (AdminChkBx.Checked == true)
                {
                    objEmpDetails.IsNonAdmin = "1";
                }
                else
                {
                    objEmpDetails.IsNonAdmin = "0";
                }

                objdata.EmployeeDetails(objEmpDetails);

                // EpayEmployeeDetails();

                EpayEmployeeDetails();
                EpayOfficialDetails();
                EpaySalaryDetails();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Save All Successfully');", true);
                Response.Redirect("new_Employee_ADD.aspx");
            }
            EpayEmployeeDetails();
            EpayOfficialDetails();
            EpaySalaryDetails();
        }
       

    }
    public void EpayEmployeeDetails()
    {

        ///Check Existing Code if Available or not
        DataTable dt = new DataTable();
        DataTable dtInsert = new DataTable();
        string InsertQuery = "";

        string WagesType = "";
        string EmployeeType = "";
        string Gender = "";
        string Qulification = "";
        string DeptCode = "";
        string StafforLabour = "";
        string Status = "R";
        string IsValid = "Y";
        string IsLeave = "N";
        string ActiveMode = "Y";
        string OnRoll = "1";
        string MarriedStatus = "";
        string ContractType = "0";
        string Query = "";
        string ExistingCode = "";
        string ActivateMode = "";
        string Worker_Incentive = "";

        string MonthYear = System.DateTime.Now.ToString("MMyyyy");
        string GetDate = System.DateTime.Now.ToString("dd/MM/yyyy");
        string StartCode = "";

        StartCode = "EMP";
        Worker_Incentive = ddlIncen_Eligible.SelectedValue; 





        //Get EmployeeType
        Query = "Select EmpTypeCd from [" + SessionEpay + "]..MstEmployeeType where EmpType='" + ddlWagesType.SelectedItem.Text + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0) { EmployeeType = dt.Rows[0]["EmpTypeCd"].ToString(); } else { EmployeeType = "0"; }


        //Get Gender Type
        if (ddlGender.SelectedValue == "2") { Gender = "1"; }
        else if (ddlGender.SelectedValue == "4") { Gender = "2"; } else { Gender = "0"; }

        //Qualification Type
        Query = "Select QualificationCd from [" + SessionEpay + "]..MstQualification where QualificationCd='" + ddlQulify.SelectedValue + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            Qulification = dt.Rows[0]["QualificationCd"].ToString();
        }
        else
        {
            Query = "Insert into [" + SessionEpay + "]..MstQualification(QualificationNm) Values ('" + ddlQulify.SelectedItem.Text.Trim() + "')";
            dt = objdata.RptEmployeeMultipleDetails(Query);

            Query = "Select QualificationCd from [" + SessionEpay + "]..MstQualification where QualificationCd='" + ddlQulify.SelectedValue + "'";
            dt = objdata.RptEmployeeMultipleDetails(Query);
            if (dt.Rows.Count > 0) { Qulification = dt.Rows[0]["QualificationCd"].ToString(); }
        }

        //if (txtQualify.Text == "") { Qulification = "1"; } else { Qulification = txtQualify.Text; }


        //Get Department Code
        Query = "Select DepartmentCd from [" + SessionEpay + "]..MstDepartment where DepartmentNm='" + ddlDepartment.SelectedItem.Text.Trim() + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);

        if (dt.Rows.Count > 0)
        {
            DeptCode = dt.Rows[0]["DepartmentCd"].ToString();
        }
        else
        {
            //Insert Department
            Query = "Insert into [" + SessionEpay + "]..MstDepartment(DepartmentNm) Values ('" + ddlDepartment.SelectedValue + "')";
            dt = objdata.RptEmployeeMultipleDetails(Query);

            Query = "Select DepartmentCd from [" + SessionEpay + "]..MstDepartment where DepartmentNm='" + ddlDepartment.SelectedValue + "'";
            dt = objdata.RptEmployeeMultipleDetails(Query);
            if (dt.Rows.Count > 0) { DeptCode = dt.Rows[0]["DepartmentCd"].ToString(); }

        }

        //Get StaffOrLabour Details
        if (ddlCategory.SelectedValue == "2") { StafforLabour = "S"; } else { StafforLabour = "L"; }

        //Get Married Details
        if (ddlmMaritalStatus.SelectedValue == "2") { MarriedStatus = "U"; } else { MarriedStatus = "M"; }

        //ActiveMode
        if (ddlActive.SelectedValue == "2") { ActivateMode = "Y"; } else { ActivateMode = "N"; }



        //Get Employee from Employee Details
        Query = "Select * from [" + SessionEpay + "]..EmployeeDetails where ExisistingCode='" + txtMachineID.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dtInsert = objdata.RptEmployeeMultipleDetails(Query);
        if (dtInsert.Rows.Count > 0)
        {
            ExistingCode = dtInsert.Rows[0]["EmpNo"].ToString();

            string TempID = dtInsert.Rows[0]["EmpNo"].ToString();
            Query = "delete from [" + SessionEpay + "]..EmployeeDetails where EmpNo='" + TempID + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            dtInsert = objdata.RptEmployeeMultipleDetails(Query);

            Query = "delete from [" + SessionEpay + "]..Officialprofile where EmpNo='" + TempID + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            dtInsert = objdata.RptEmployeeMultipleDetails(Query);


            Query = "delete from [" + SessionEpay + "]..SalaryMaster where EmpNo='" + TempID + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            dtInsert = objdata.RptEmployeeMultipleDetails(Query);
        }
        else
        {

            ExistingCode = StartCode + MonthYear + txtMachineID.Text;

        }
        //Insert Table 
        Query = "Insert into [" + SessionEpay + "]..EmployeeDetails(EmpNo,MachineNo,ExisistingCode,EmpName,FatherName,Gender,DOB,PSAdd1,PSAdd2,PSDistrict,PSTaluk,PSState, ";
        Query = Query + "Phone,Mobile,Qualification,Department,Designation,EmployeeType,CreatedDate,IsValid,StafforLabor,IsLeave,ActivateMode,Status,RoleCode,Ccode,Lcode,MartialStatus,Initial,ContractType,OldID,BiometricID,WorkerIncentive) Values( ";
        Query = Query + "'" + ExistingCode + "','" + ExistingCode + "','" + txtMachineID.Text + "','" + txtfirstname.Text + "','" + txtFatherName.Text + "','" + Gender + "',convert(datetime,'" + txtdateofbirth.Text + "',103),'" + txtPermanentAddr.Text + "','" + txtPermanentAddr.Text + "','" + txtpermanentTlk.Text + "','" + txtPermanentDst.Text + "','TamilNadu', ";
        Query = Query + "'-','" + txtEmpMobileNum.Text + "','" + Qulification + "','" + DeptCode + "','" + ddlDesignation.SelectedItem.Text + "','" + EmployeeType + "',convert(datetime,'" + GetDate + "',103),'Y','" + StafforLabour + "','N','" + ActivateMode + "','R','1','" + SessionCcode + "','" + SessionLcode + "','" + MarriedStatus + "','F','0','" + txtMachineID.Text + "','" + txtMachineID.Text + "','" + Worker_Incentive + "')";
        dtInsert = objdata.RptEmployeeMultipleDetails(Query);



    }
    public void EpayOfficialDetails()
    {
        string EligiblePF = "";
        string EligibleESI = "";
        string PFNumber = "";
        string ESINumber = "";
        string SalaryThtough = "";
        string Wages = "";
        string EligibleOT = "";
        string InsuranceCampName = "0";
        string BankName = "";
        string Branch = "";
        string AccNo = "";
        string FinacialYear = "2016";
        string ProfileType = "1";

        string GetDate = System.DateTime.Now.ToString("dd/MM/yyyy");

        DataTable dt = new DataTable();
        DataTable dtInsert = new DataTable();
        string Query = "";

        if (ddlOTEligible.SelectedValue == "2")
        {
            EligibleOT = "1";
        }
        else
        {
            EligibleOT = "2";
        }

        if (ddlESIEligible.SelectedValue == "2") { EligibleESI = "1"; ESINumber = txtESINum.Text; }
        else { EligibleESI = "2"; ESINumber = "0"; }

        if (ddlPFEligible.SelectedValue == "2") { EligiblePF = "1"; PFNumber = txtPFNum.Text; }
        else { EligiblePF = "2"; PFNumber = "0"; }

        if (rbtnSalayThtough.SelectedValue == "1")
        {
            SalaryThtough = "1";
            BankName = "";
            Branch = "";
            AccNo = "0";
        }
        else
        {
            SalaryThtough = "2";
            BankName = ddlBankCode.SelectedValue;
            Branch = txtbankName.Text;
            AccNo = txtAccNo.Text;

        }

        if (ddlWagesType.SelectedItem.Text == "LABOUR")
        {
            Wages = "2";
        }
        else if (ddlWagesType.SelectedItem.Text == "STAFF")
        {
            Wages = "2";
        }
        else
        {
            Wages = "1";
        }

        Query = "Select * from [" + SessionEpay + "]..EmployeeDetails where BiometricID='" + txtMachineID.Text + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);

        for (int i = 0; i <= dt.Rows.Count - 1; i++)
        {
            Query = "Select * from [" + SessionEpay + "]..Officialprofile where EmpNo='" + dt.Rows[i]["EmpNo"].ToString().Trim() + "'";
            dtInsert = objdata.RptEmployeeMultipleDetails(Query);

            if (dtInsert.Rows.Count > 0)
            {
                Query = "delete from [" + SessionEpay + "]..Officialprofile where EmpNo='" + dt.Rows[i]["EmpNo"].ToString().Trim() + "'";
                dtInsert = objdata.RptEmployeeMultipleDetails(Query);
            }

            Query = "Insert into [" + SessionEpay + "]..Officialprofile(EmpNo,MachineNo,Dateofjoining,ProbationPeriod,Confirmationdate,Insurancecompanyname,Eligibleforovertime, ";
            Query = Query + "Salarythrough,BankName,Branchname,AccNo,ExpiryDate,Wagestype,FinancialYear,CreatedDate,ContractPeriod,BasicSalary,ElgibleESI,ESICnumber,ContractStartDate,ContractEndDate,ProfileType,EligiblePF,PFnumber,Ccode,Lcode) Values( ";
            Query = Query + "'" + dt.Rows[i]["EmpNo"].ToString().Trim() + "','" + dt.Rows[i]["MachineNo"].ToString().Trim() + "',convert(datetime,'" + txtdateofJoin.Text + "',103),'0',convert(datetime,'" + GetDate + "',103),'" + InsuranceCampName + "','" + EligibleOT + "', ";
            Query = Query + "'" + SalaryThtough + "','" + BankName + "','" + Branch + "','" + AccNo + "',convert(datetime,'" + GetDate + "',103),'" + Wages + "','" + FinacialYear + "',convert(datetime,'" + GetDate + "',103),'1','0.0','" + EligibleESI + "','" + ESINumber + "',convert(datetime,'" + GetDate + "',103),convert(datetime,'" + GetDate + "',103),'1','" + EligiblePF + "','" + PFNumber + "','" + SessionCcode + "','" + SessionLcode + "')";
            dtInsert = objdata.RptEmployeeMultipleDetails(Query);

        }



    }
    public void EpaySalaryDetails()
    {
        string FDA = "0.00";
        string VDA = "0.00";
        string HRA = "0.00";
        string Total = "0.00";
        string UnionCharge = "0.00";

        DataTable dt = new DataTable();
        DataTable dtSalary = new DataTable();
        string Query = "";

        Query = "Select EmpNo from [" + SessionEpay + "]..EmployeeDetails where BiometricID='" + txtMachineID.Text + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        for (int i = 0; i <= dt.Rows.Count - 1; i++)
        {
            Query = "Select * from [" + SessionEpay + "]..SalaryMaster where EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "'";
            dtSalary = objdata.RptEmployeeMultipleDetails(Query);

            if (dt.Rows.Count > 0)
            {
                Query = "delete from [" + SessionEpay + "]..SalaryMaster where EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "'";
                dtSalary = objdata.RptEmployeeMultipleDetails(Query);
            }


            Query = "Insert into [" + SessionEpay + "]..SalaryMaster(EmpNo,Alllowance1amt,Allowance2amt,CreatedDate,ModifiedDate,Deduction1,Deduction2,Ccode,Lcode,Base,FDA,VDA,HRA,Total,Unioncharges,PFS) Values( ";
            Query = Query + "'" + dt.Rows[i]["EmpNo"].ToString().Trim() + "','" + txtAllowance1.Text + "','" + txtAllowance2.Text + "',convert(datetime,'25/04/2016',103),convert(datetime,'25/04/2016',103),'" + txtDeduction1.Text + "','" + txtDeduction2.Text + "','" + SessionCcode + "','" + SessionLcode + "','" + txtBasic.Text + "','" + FDA + "','" + VDA + "','" + HRA + "','" + Total + "','" + UnionCharge + "','" + txtPFSal.Text + "')";
            dtSalary = objdata.RptEmployeeMultipleDetails(Query);
        }
    }

  

    protected void txtMachineID_TextChanged(object sender, EventArgs e)
    {
        //bool ErrFlag = false;
        //DataTable dtd = new DataTable();
        //dtd = objdata.CheckAndReturn(txtMachineID.Text, SessionCcode, SessionLcode);
        //if (dtd.Rows.Count > 0)
        //{
        //    if (ErrFlag == false)
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('MachineID already Exist');", true);
        //    }
        //}
        //if (ErrFlag == true)
        //{
        //    Response.Redirect("new_Employee_ADD.aspx");
        //}
        
    }

    public void DisplayEmployeeDetails()
    {
        DataTable dtd = new DataTable();
        dtd = objdata.CheckAndReturn(txtMachineID.Text, SessionCcode, SessionLcode);
        if (dtd.Rows.Count > 0)
        {
            string category = dtd.Rows[0]["CatName"].ToString();
            if (category.ToString() == "STAFF")
            {
                ddlCategory.SelectedIndex = 1;
            }
            else if (category.ToString() == "LABOUR")
            {
                ddlCategory.SelectedIndex = 2;
            }

            string Subcategory = dtd.Rows[0]["SubCatName"].ToString();

            if (Subcategory.ToString() == "INSIDER")
            {
                ddlsubcategory.SelectedIndex = 1;
            }
            else if (Subcategory.ToString() == "OUTSIDER")
            {
                ddlsubcategory.SelectedIndex = 2;
            }

            string UNIT_Folder = "";
            DataTable DT_Photo = new DataTable();
            string SS = "Select *from Photo_Path_Det";
            DT_Photo = objdata.RptEmployeeMultipleDetails(SS);

            string PhotoDet1 = "";
            if (DT_Photo.Rows.Count != 0)
            {
                PhotoDet1 = DT_Photo.Rows[0]["Photo_Path"].ToString();
            }

            if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet1 + "/UNIT_I/"; }
            if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = PhotoDet1 + "/UNIT_II/"; }
            if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = PhotoDet1 + "/UNIT_III/"; }
            if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = PhotoDet1 + "/UNIT_IV/"; }

            //string path_1 = "~/" + UNIT_Folder + "/Photos/" + txtTokenID.Text + ".jpg";
            string path_1 = UNIT_Folder + "/Photos/" + txtMachineID.Text + ".jpg";
            DirectoryInfo di = new DirectoryInfo(path_1);
            byte[] imageBytes = new byte[0];
            //File Check
            //if (File.Exists(Server.MapPath(path_1)))
            if (File.Exists((path_1)))
            {
                //txtExistingCode.Text = path_1;
                Image3.Visible = true;
                //Image3.ImageUrl = (@"" + path_1);

                Image3.ImageUrl = "Handler.ashx?f=" + path_1 + "";
                //imageBytes = File.ReadAllBytes(di);
                //imageBytes = File.ReadAllBytes(@"" + path_1);
                //PictureBox imageControl = new PictureBox();

                //imageControl.Width = 400;

                //imageControl.Height = 400;
                //Bitmap image = new Bitmap(path_1);
                //imageControl.Image = (System.Drawing.Image)image;
                //Image3.ImageUrl = ResolveUrl(path_1);
                //img1.Src = (path_1);
            }
            else
            {
                //txtExistingCode.Text = path_1 + " Not Match";
                Image3.Visible = true;
                Image3.ImageUrl = "~/assets/images/No_Image.jpg";
                //img1.Src = "~/assets/images/No_Image.jpg";
            }

            ////string path_2 = "~/" + UNIT_Folder + "/ParentPhoto/" + txtTokenID.Text + ".jpg";
            //string path_2 = UNIT_Folder + "/ParentPhoto/" + txtMachineID.Text + ".jpg";
            ////File Check
            //if (File.Exists((path_2)))
            //{
            //    Image1.Visible = true;
            //    //Image1.ImageUrl = (path_2);
            //    Image1.ImageUrl = "Handler.ashx?f=" + path_2 + "";
            //    //img1.Src = (path_2);
            //}
            //else
            //{
            //    Image1.Visible = true;
            //    Image1.ImageUrl = "~/assets/images/No_Image.jpg";
            //    //img1.Src = "~/assets/images/No_Image.jpg";
            //}

            //ddlsubcategory.SelectedItem.Text = dtd.Rows[0]["SubCatName"].ToString();


            txtMachineID.Text = dtd.Rows[0]["MachineID"].ToString();
            txtExistingNo.Text = dtd.Rows[0]["ExistingCode"].ToString();
            txtTicketNo.Text = dtd.Rows[0]["EmpNo"].ToString();
            txtfirstname.Text = dtd.Rows[0]["FirstName"].ToString();
            txtlastname.Text = dtd.Rows[0]["MiddleInitial"].ToString();
            txtdateofbirth.Text = dtd.Rows[0]["BirthDate"].ToString();
            txtage.Text = dtd.Rows[0]["Age"].ToString();

            ddlIncen_Eligible.SelectedValue = dtd.Rows[0]["Incentive_Eligible"].ToString();
           


            string gender = dtd.Rows[0]["Gender"].ToString();
            if (gender.ToString() == "Male")
            {
                ddlGender.SelectedIndex = 1;
            }
            else
            {
                ddlGender.SelectedIndex = 2;
            }

            



            string martial = dtd.Rows[0]["MaritalStatus"].ToString();
            if (martial.ToString() == "Single")
            {
                ddlmMaritalStatus.SelectedIndex = 1;
            }
            else if (martial.ToString() == "Married")
            {
                ddlmMaritalStatus.SelectedIndex = 2;
            }
            else if (martial.ToString() == "Divorced")
            {
                ddlmMaritalStatus.SelectedIndex = 3;
            }
            else if (martial.ToString() == "None")
            {
                ddlmMaritalStatus.SelectedIndex = 4;
            }
            txtNationality.Text = dtd.Rows[0]["Nationality"].ToString();
            txtReligion.Text = dtd.Rows[0]["Religion"].ToString();

            string bloodg = dtd.Rows[0]["BloodGroup"].ToString();

            if (bloodg.ToString() == "A+")
            {
                ddlBloodGroup.SelectedIndex = 1;
            }
            else if (bloodg.ToString() == "B+")
            {
                ddlBloodGroup.SelectedIndex = 2;
            }
            else if (bloodg.ToString() == "B-")
            {
                ddlBloodGroup.SelectedIndex = 3;
            }
            else if (bloodg.ToString() == "AB+")
            {
                ddlBloodGroup.SelectedIndex = 4;
            }
            else if (bloodg.ToString() == "AB-")
            {
                ddlBloodGroup.SelectedIndex = 5;
            }
            else if (bloodg.ToString() == "O+")
            {
                ddlBloodGroup.SelectedIndex = 6;
            }
            else if (bloodg.ToString() == "O-")
            {
                ddlBloodGroup.SelectedIndex = 7;
            }



            ddlBloodGroup.SelectedItem.Text = dtd.Rows[0]["BloodGroup"].ToString();
            txtHeight.Text = dtd.Rows[0]["Height"].ToString();
            txtWeight.Text = dtd.Rows[0]["Weight"].ToString();

            string handica = dtd.Rows[0]["Handicapped"].ToString();

            if (handica.ToString() == "Yes")
            {
                ddlPhysicalChng.SelectedIndex = 1;
            }
            else if (handica.ToString() == "No")
            {
                ddlPhysicalChng.SelectedIndex = 2;
            }

            txtdateofJoin.Text = dtd.Rows[0]["DOJ"].ToString();

            string active = dtd.Rows[0]["IsActive"].ToString();
            if (active.ToString().Trim() == "YES")
            {
                ddlActive.SelectedIndex = 1;
            }
            else if (active.ToString().Trim() == "Yes")
            {
                ddlActive.SelectedIndex = 1;
            }
            else if (active.ToString() == "No")
            {
                ddlActive.SelectedIndex = 2;
            }


            string admin = dtd.Rows[0]["IsNonAdmin"].ToString();
            if (admin.ToString() == "0")
            {
                AdminChkBx.Checked = false;
            }
            else
            {
                AdminChkBx.Checked = true;
            }

            //DataTable dt = new DataTable();
            //dt = objdata.dropdown_loc(ddlCompanyCode.SelectedValue);
            //ddlLocationCode.DataSource = dt;
            //DataRow dr = dt.NewRow();
            //dr["LocCode"] = "Location Name";
            //dr["Location"] = "Location Name";
            //dt.Rows.InsertAt(dr, 0);
            //ddlLocationCode.DataTextField = "Location";
            //ddlLocationCode.DataValueField = "LocCode";
            //ddlLocationCode.DataBind();


            DataTable dt = new DataTable();
            string dept = dtd.Rows[0]["DeptName"].ToString();
            SSQL = "select DeptCode from Department_Mst where DeptName='" + dept + "'";
            dt = objdata.ReturnMultipleValue(SSQL);
            if (dt.Rows.Count > 0)
            {

                string deptt = dt.Rows[0]["DeptCode"].ToString();
                if (deptt.ToString() != "")
                {
                    ddlDepartment.SelectedValue = deptt;
                }
            }

            DataTable dtdes = new DataTable();
            string desgin = dtd.Rows[0]["Designation"].ToString();
            SSQL = "select DesignNo from Designation_Mst where DesignName='" + desgin + "'";
            dtdes = objdata.ReturnMultipleValue(SSQL);
            if (dtdes.Rows.Count > 0)
            {
                string des = dtdes.Rows[0]["DesignNo"].ToString();
                if (des.ToString() != "")
                {
                    ddlDesignation.SelectedValue = des;
                }
            }

            txtStdWHr.Text = dtd.Rows[0]["StdWrkHrs"].ToString();

            string otEligible = dtd.Rows[0]["OTEligible"].ToString();
            if (otEligible.ToString() == "Yes")
            {
                ddlOTEligible.SelectedIndex = 1;
            }
            else if (otEligible.ToString() == "No")
            {
                ddlOTEligible.SelectedIndex = 2;
            }

            txtOTHr.Text = dtd.Rows[0]["OT_Hours"].ToString();

            string Weekoff = dtd.Rows[0]["WeekOff"].ToString();
            if (dtd.Rows[0]["WeekOff"].ToString() == "None")
            {
                ddlWeekOff.SelectedIndex = 1;
            }
            else if (dtd.Rows[0]["WeekOff"].ToString() == "Sunday")
            {
                ddlWeekOff.SelectedIndex = 2;
            }
            else if (dtd.Rows[0]["WeekOff"].ToString() == "Monday")
            {
                ddlWeekOff.SelectedIndex = 3;
            }
            else if (dtd.Rows[0]["WeekOff"].ToString() == "Tuesday")
            {
                ddlWeekOff.SelectedIndex = 4;
            }
            else if (dtd.Rows[0]["WeekOff"].ToString() == "Wednesday")
            {
                ddlWeekOff.SelectedIndex = 5;
            }
            else if (dtd.Rows[0]["WeekOff"].ToString() == "Thursday")
            {
                ddlWeekOff.SelectedIndex = 6;
            }
            else if (dtd.Rows[0]["WeekOff"].ToString() == "Fridayday")
            {
                ddlWeekOff.SelectedIndex = 7;
            }
            else if (dtd.Rows[0]["WeekOff"].ToString() == "Saturday")
            {
                ddlWeekOff.SelectedIndex = 8;
            }
            else
            {
                ddlWeekOff.SelectedIndex = 1;
            }

            DataTable wgstypedt = new DataTable();
            string wgstype = dtd.Rows[0]["Wages"].ToString();
            SSQL = "select WagesTypeNo from WagesType_Mst where WagesTypeName='" + wgstype + "'";
            
            wgstypedt = objdata.ReturnMultipleValue(SSQL);

            if (wgstypedt.Rows.Count > 0)
            {
                string wgs = wgstypedt.Rows[0]["WagesTypeNo"].ToString();
                if (wgs.ToString() != "")
                {
                    ddlWagesType.SelectedValue = wgs;
                }
            }

            DataTable dtshift = new DataTable();
            string emptye = dtd.Rows[0]["ShiftType"].ToString();
            SSQL = "select TypeID from EmpType_Mst where TypeName='" + emptye + "'";
            dtshift = objdata.ReturnMultipleValue(SSQL);
            if (dtshift.Rows.Count > 0)
            {

                string shift = dtshift.Rows[0]["TypeID"].ToString();
                if (shift.ToString() != "")
                {
                    ddlShiftType.SelectedValue = shift;
                }


            }

            //ddlWagesType.SelectedItem.Text = dtd.Rows[0]["Wages"].ToString();
            string PF_Eligible = dtd.Rows[0]["Eligible_PF"].ToString();
            if (PF_Eligible.ToString() == "Yes")
            {
                ddlPFEligible.SelectedIndex = 1;
            }
            else if (PF_Eligible.ToString() == "No")
            {
                ddlPFEligible.SelectedIndex = 2;
            }

            txtPFNum.Text = dtd.Rows[0]["PFNo"].ToString();
            txtPFDOJ.Text = dtd.Rows[0]["PFDOJ"].ToString();

            string ESI_Eligible = dtd.Rows[0]["Eligible_ESI"].ToString();
            if (ESI_Eligible.ToString() == "Yes")
            {
                ddlESIEligible.SelectedIndex = 1;
            }
            else if (ESI_Eligible.ToString() == "No")
            {
                ddlESIEligible.SelectedIndex = 2;
            }

            //txtESICode.Text = dtd.Rows[0]["ESICode"].ToString();
            txtESINum.Text = dtd.Rows[0]["ESINo"].ToString();
            txtESIDOJ.Text = dtd.Rows[0]["ESIDOJ"].ToString();

            txtHstRoomNo.Text = dtd.Rows[0]["ESIDOJ"].ToString();
            txtBusRoute.Text = dtd.Rows[0]["BusRoute"].ToString();
            txtBusNum.Text = dtd.Rows[0]["BusNo"].ToString();
            txtRecThrgh.Text = dtd.Rows[0]["RecuritmentThro"].ToString();
            txtRecMobile.Text = dtd.Rows[0]["RecutersMob"].ToString();

            txtbankName.Text = dtd.Rows[0]["BankName"].ToString();
            ddlBankCode.SelectedValue = dtd.Rows[0]["BranchCode"].ToString();
            txtAccNo.Text = dtd.Rows[0]["AccountNo"].ToString();
            txtIFSCCode.Text = dtd.Rows[0]["IFSC_Code"].ToString();



            ddlQulify.SelectedValue = dtd.Rows[0]["Qualification"].ToString();
            txtNominee.Text = dtd.Rows[0]["Nominee"].ToString();
            //string s = dtd.Rows[0]["FamilyDetails"].ToString();

            //string[] items = s.Split(' ');

            //string ssFname = items[0];
            //if (items[1] != "")
            //{
            //    string ssMname = items[1];
            //    txtMotherName.Text = ssMname;
            //}
            //txtFatherName.Text = ssFname;

           // txtGuardName.Text = dtd.Rows[0]["GuardianName"].ToString();
            txtPermanentAddr.Text = dtd.Rows[0]["Address1"].ToString();
            //txtpermanentTlk.Text = dtd.Rows[0]["Permanent_Tlk"].ToString();
            txtPermanentDst.Text = dtd.Rows[0]["Permanent_Dist"].ToString();
            txtTempAddr.Text = dtd.Rows[0]["Address2"].ToString();
            txtTempDst.Text = dtd.Rows[0]["Present_Dist"].ToString();
            //txtTempTlk.Text = dtd.Rows[0]["Present_Tlk"].ToString();
            txtIMark1.Text = dtd.Rows[0]["IDMark1"].ToString();
            txtIMark2.Text = dtd.Rows[0]["IDMark2"].ToString();
            txtParentMob1.Text = dtd.Rows[0]["parentsMobile"].ToString();
            txtParentMob2.Text = dtd.Rows[0]["ParentsPhone"].ToString();
            txtEmpMobileNum.Text = dtd.Rows[0]["EmployeeMobile"].ToString();
            txtEmpMobile.Text = dtd.Rows[0]["EmployeeMobile"].ToString();

            txtBasic.Text = dtd.Rows[0]["BasicSalary"].ToString();
            txtDeduction1.Text = dtd.Rows[0]["Deduction1amt"].ToString();
            txtDeduction2.Text = dtd.Rows[0]["Deduction2amt"].ToString();
            txtAllowance1.Text = dtd.Rows[0]["Allowance1amt"].ToString();
            txtAllowance2.Text = dtd.Rows[0]["Allowance2amt"].ToString();
            txtPFSal.Text = dtd.Rows[0]["PFS"].ToString();


            if (txtDeduction1.Text == "")
            {
                txtDeduction1.Text = "0";
            }
             if (txtDeduction2.Text == "")
            {
                txtDeduction2.Text = "0";
            } 
             if(txtAllowance1.Text=="")
            {
                txtAllowance1.Text = "0";
            }
             if (txtAllowance2.Text == "")
            {
                txtAllowance2.Text = "0";
            }
             if (txtPFSal.Text == "")
            {
                txtPFSal.Text = "0";
            }
             if (txtBasic.Text == "")
            {
                txtBasic.Text = "0";
            }
                



            DataTable DtbAdol = new DataTable();
            SSQL = "select * from Adolcent_Emp_Det where MachineID='" + txtMachineID.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            DtbAdol = objdata.ReturnMultipleValue(SSQL);
            if (DtbAdol.Rows.Count > 0)
            {
                txtAdlCerftType.Text = DtbAdol.Rows[0]["Certificate_Type"].ToString();
                txtAdlCerftNo.Text = DtbAdol.Rows[0]["Certificate_No"].ToString();
                txtAdlIssuseDate.Text = DtbAdol.Rows[0]["Certificate_Date"].ToString();
                txtAdlNextDueDate.Text = DtbAdol.Rows[0]["Next_Due_Date"].ToString();
                txtEmpMobile.Text = DtbAdol.Rows[0]["Next_Due_Date"].ToString();
                txtRemarks.Text = DtbAdol.Rows[0]["Remarks"].ToString();
            }


            //DataTable DtbBasic = new DataTable();
            //SSQL = "select * from [S_EPay]..SalaryMaster  where EmpNo='" + txtMachineID.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            //DtbBasic = objdata.ReturnMultipleValue(SSQL);
            //if (DtbBasic.Rows.Count > 0)
            //{
                
            //}

            CreateUserDisplay();
            ExperienceDisplay();
        }



    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        if (txtMachineID.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Machine No');", true);
            ErrFlag = true;
        }

        else if (txtTicketNo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter TicketNo');", true);
            ErrFlag = true;
        }

        else if (txtExistingNo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Existing Number');", true);
            ErrFlag = true;
        }
        else
        {

            objExpDetails.Ccode = SessionCcode;
            objExpDetails.Lcode = SessionLcode;
            objExpDetails.TokenNo = txtTicketNo.Text;
            objExpDetails.ExistingNo = txtExistingNo.Text;
            objExpDetails.MachineID = txtMachineID.Text;
            objExpDetails.YearExp = txtExpTol.Text;
            objExpDetails.CompName = txtExpComNam.Text;
            objExpDetails.Designation = txtExpDesgination.Text;
            objExpDetails.Department = txtExpDept.Text;
            objExpDetails.PeriodFrom = txtPeriodFrom.Text;
            objExpDetails.PeriodTo = txtPeriodTo.Text;
            objExpDetails.ContactName = txtRefName.Text;
            objExpDetails.Mobile = txtRefMobile.Text;
            objdata.ExperienceDetails(objExpDetails);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Experience Details Save Successfully');", true);
            ExperienceDisplay();
        }

    }

    public void ExperienceDisplay()
    {
        string EmpNo = txtTicketNo.Text;
        DataTable dtdDisplay = new DataTable();
        dtdDisplay = objdata.ExperienceDisplay(EmpNo);
        if (dtdDisplay.Rows.Count > 0)
        {
            rptrCustomer.DataSource = dtdDisplay;
            rptrCustomer.DataBind();
        }

    }

    private void EditRepeaterData(string id)
    {
        img11.Visible = true;
        img11.ImageUrl = "";
        DataTable dtdDocument = new DataTable();
        SSQL = "select* from Document_Mst where SNo = '" + id + "'";
        dtdDocument = objdata.ReturnMultipleValue(SSQL);
        if (dtdDocument.Rows.Count > 0)
        {
            string filename = dtdDocument.Rows[0]["pathfile"].ToString();
            FetchFiles(filename);
        }
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteExperience(id);
                break;
                      
        }
    }

    public void DeleteExperience(string SNo)
    {
        DataTable dtdDisplay = new DataTable();
        dtdDisplay = objdata.DeleteExperience(SNo);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete Successfully');", true);
        ExperienceDisplay();

    }

    protected void Repeater2_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(id);
                break;
            case ("Edit"):
                EditRepeaterData(id);
                break;
        }
    }


    public void DeleteRepeaterData(string e)
    {
        DataTable dtdDisplay = new DataTable();
        string s = "Delete Document_Mst where SNo='" + e + "'";
        dtdDisplay = objdata.ReturnMultipleValue(s);

        string path = Server.MapPath("Images//" + e);

        FileInfo file = new FileInfo(path);
        file.Refresh();
        if (file.Exists)
        {
            file.Delete();
        }

        rptrCustomer.DataSource = dtdDisplay;
        rptrCustomer.DataBind();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete Successfully');", true);
        CreateUserDisplay();
    }


    public void CreateUserDisplay()
    {
        DataTable dtdDisplay = new DataTable();
        SSQL = "select * from Document_Mst where MachineID='" + txtMachineID.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dtdDisplay = objdata.ReturnMultipleValue(SSQL);

        Repeater2.DataSource = dtdDisplay;
        Repeater2.DataBind();
    }

    private void FetchFiles(string path)
    {
        FileInfo fi = new FileInfo(path);
        if (fi.Exists)
        {
          img11.ImageUrl = Page.ResolveUrl("~/Images/") + fi.Name; 
            // You just need to pass name
            //YOu need to make path like ~/ImageFiles/maui4.jpg
        }
    }



    protected void btndocumentsave_Click(object sender, EventArgs e)
    {
         if (txtMachineID.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Machine No');", true);
            ErrFlag = true;
        }

        else if (txtTicketNo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter TicketNo');", true);
            ErrFlag = true;
        }

        else if (txtfirstname.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Name');", true);
            ErrFlag = true;
        }
        else
        {
            if (filUpload.HasFile)
            {
                string filename = txtMachineID.Text + txtDocNo.Text + ".jpg";
                filUpload.SaveAs(Server.MapPath("Images/" + filename));
                string pathname = Server.MapPath("Images/" + filename);
               

                objdoc.Pathfile = pathname.ToString();

                objdoc.Ccode = SessionCcode.ToString();
                objdoc.Lcode = SessionLcode.ToString();
                objdoc.EmployeeNo = txtTicketNo.Text;
                objdoc.MachineID = txtMachineID.Text;
                objdoc.EmployeeName = txtfirstname.Text;
                objdoc.DocumentType = ddlDocType.SelectedItem.Text;
                objdoc.DocumentNo = txtDocNo.Text;
                objdoc.DocumentDesc = txtDocDesc.Text;
                objdata.SaveDocumentReg(objdoc);
                CreateUserDisplay();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Document Save Successfully');", true);
                CreateUserDisplay();

                ddlDocType.SelectedIndex = 0;
                txtDocNo.Text = "";
                txtDocDesc.Text = "";
                
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Upload File');", true);
            }
        }
    }
    protected void rdbYes_CheckedChanged(object sender, EventArgs e)
    {
        if (rdbYes.Checked == true)
        {

            rdbNo.Checked = false;
            txtAdlCerftType.Enabled=true;
            txtAdlCerftNo.Enabled= true;
            txtAdlIssuseDate.Enabled= true;
            txtAdlNextDueDate.Enabled= true;
            txtEmpMobile.Enabled= true;
            txtRemarks.Enabled= true;

            //TypeofCertificate.Enabled= true;
            //Certificateno.Enabled= true;
            //IssueDate.Enabled= true;
            //NextDate.Enabled= true;
            //EmployeeMobile.Enabled= true;
            //Remarks.Enabled= true;
        }
    }
    protected void rdbNo_CheckedChanged(object sender, EventArgs e)
    {
        if (rdbNo.Checked == true)
        {
            rdbYes.Checked = false;
            txtAdlCerftType.Enabled = false;
            txtAdlCerftNo.Enabled= false;
            txtAdlIssuseDate.Enabled= false;
            txtAdlNextDueDate.Enabled= false;
            txtEmpMobile.Enabled= false;
            txtRemarks.Enabled= false;

            //TypeofCertificate.Enabled= false;
            //Certificateno.Enabled= false;
            //IssueDate.Enabled= false;
            //NextDate.Enabled= false;
            //EmployeeMobile.Enabled= false;
            //Remarks.Enabled= false;
            
        }
    }
    protected void btnAdolescent_Click(object sender, EventArgs e)
    {
        if (txtMachineID.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Machine No');", true);
            ErrFlag = true;
        }

        else if (txtExistingNo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter TicketNo');", true);
            ErrFlag = true;
        }

        else if (txtdateofbirth.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Name');", true);
            ErrFlag = true;
        }
        else
        {
            DataTable dtdAdolcent = new DataTable();
            SSQL = "select ExistingCode,MachineID from Adolcent_Emp_Det where ExistingCode = '" + txtExistingNo.Text +"' and CompCode='"+ SessionCcode +"' and LocCode='"+ SessionLcode +"'";
            dtdAdolcent = objdata.ReturnMultipleValue(SSQL);
            if (dtdAdolcent.Rows.Count <= 0)
            {
                objAdlDetails.Ccode = SessionCcode;
                objAdlDetails.Lcode = SessionLcode;
                objAdlDetails.ExistingNo = txtExistingNo.Text;
                objAdlDetails.MachineID = txtMachineID.Text;
                objAdlDetails.DOB = txtdateofbirth.Text;
                objAdlDetails.AdlCertificateNo = txtAdlCerftNo.Text;
                objAdlDetails.AdlIssuseDate = txtAdlIssuseDate.Text;
                objAdlDetails.AdlCertificateType = txtAdlCerftType.Text;
                objAdlDetails.AdlNextDueDate = txtAdlNextDueDate.Text;
                objAdlDetails.AdlRemarks = txtRemarks.Text;
                objdata.AdolescentDetails(objAdlDetails);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Adolescent Details Save Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Already Exist');", true);
                ErrFlag = true;
            }

        }

    }

    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    protected void chbsameAddress_CheckedChanged(object sender, EventArgs e)
    {
        if (chbsameAddress.Checked == true)
        {
            txtTempAddr.Text = txtPermanentAddr.Text;
            txtTempDst.Text = txtPermanentDst.Text;
            txtTempTlk.Text = txtpermanentTlk.Text;
        }
        else if (chbsameAddress.Checked == false)
        {
            txtTempAddr.Text = "";
            txtTempDst.Text = "";
            txtTempTlk.Text = "";
        }

    }
    protected void txtPeriodTo_TextChanged(object sender, EventArgs e)
    {
        DateTime frmdate = Convert.ToDateTime(txtPeriodFrom.Text);
        DateTime todate = Convert.ToDateTime(txtPeriodTo.Text);
        int diff = todate.Year - frmdate.Year;
        int monthdiff = todate.Month - frmdate.Month;
        string year = Convert.ToString(diff);
        string month = Convert.ToString(monthdiff);

        txtExpTol.Text = year + " " + "Year" + " " + month + " " + "Month"; 
    }
    protected void ddlOTEligible_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlOTEligible.SelectedItem.Text == "Yes")
        {
            txtOTHr.Enabled = true;
        }
        else
        {
            txtOTHr.Enabled = false;
        }
    }
    protected void ddlESIEligible_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlESIEligible.SelectedItem.Text == "Yes")
        {
            txtESINum.Enabled = true;
            txtESIDOJ.Enabled = true;
         }
        else
        {
            txtESINum.Enabled = false;
            txtESIDOJ.Enabled = false;
            txtESINum.Text = "";
            txtESIDOJ.Text = "";
        }
    }
    protected void ddlPFEligible_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPFEligible.SelectedItem.Text == "Yes")
        {
            txtPFNum.Enabled = true;
            txtPFDOJ.Enabled = true;
        }
        else
        {
            txtPFNum.Enabled = false;
            txtPFDOJ.Enabled = false;
        }

    }
}
