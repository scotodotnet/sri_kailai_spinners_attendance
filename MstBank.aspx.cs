﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class MstBank : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    UserCreationClass objUsercreation = new UserCreationClass();
    DataTable dtdDisplay = new DataTable();
    string SessionSpay;
    string SessionEpay;

    static int ss;
    bool ErrFlag = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionSpay = Session["SessionSpay"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Bank Details";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");


            }
            Load_Data();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string Query = "";

        Query = "Select * from ["+SessionSpay+"]..MstBank where BankCode='" + txtBankCode.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            Query = "delete from ["+SessionSpay+"]..MstBank where BankCode='" + txtBankCode.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(Query);
        }
        Query = "Insert into ["+SessionSpay+"]..MstBank(BankCode,BankName,IFCcode,Ccode,Lcode) Values( ";
        Query = Query + "'" + txtBankCode.Text + "','" + txtBankName.Text + "','" + txtIFC.Text + "','" + SessionCcode + "','" + SessionLcode + "')";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Successfully Added');", true);
        Clear();
        Load_Data();

    }
    public void Clear()
    {
        txtIFC.Text = "";
        txtBankName.Text = "";
        txtBankCode.Text = "";

    }
    protected void BtnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select BankCode,BankName,IFCcode from ["+SessionSpay+"]..MstBank ";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;

        Repeater1.DataBind();
    }
    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        string Query = "";
        Query = "Select BankCode,BankName,IFCcode from ["+SessionSpay+"]..MstBank where BankCode='" + e.CommandName.ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtBankCode.Text = dt.Rows[0]["BankCode"].ToString();
            txtBankName.Text = dt.Rows[0]["BankName"].ToString();
            txtIFC.Text = dt.Rows[0]["IFCcode"].ToString();
        }
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        string Query = "";
        Query = "Select * from ["+SessionSpay+"]..MstBank where BankCode='" + e.CommandName.ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            Query = "delete from ["+SessionSpay+"]..MstBank where BankCode='" + e.CommandName.ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(Query);
        }
        Load_Data();
    }
}
