﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using Altius.BusinessAccessLayer.BALDataAccess;



public partial class Excelpayroll : System.Web.UI.Page
{
  
    DateTime dtime;
    DateTime dtime1;
    DateTime date1;
    DateTime date2 = new DateTime();
    string Encry_MachineID;
    DataTable AutoDataTable = new DataTable();
                    bool isPresent = false;
                    string Time_IN_Str;
                    string Time_Out_Str;
                    int j=0;
                    Double time_Check_dbl =0;
                    string Date_Value_Str1;
                    DateTime InTime_Check;
                    DateTime InTime_Check1;
                    DateTime InToTime_Check;
                    TimeSpan InTime_TimeSpan;
                    string From_Time_Str;

                    DataTable mLocalDS = new DataTable();
                
                    string Emp_Total_Work_Time_1 = "00:00";
                    string Machine_ID_Str = "";
                    string OT_Week_OFF_Machine_No = null;
                    string Date_Value_Str = "";
                    string Total_Time_get = "";
                    string Final_OT_Work_Time_1 = "00:00";
                    Decimal Month_Mid_Join_Total_Days_Count;
                
                    string[] Time_Minus_Value_Check;

                    //DateTime InTime_Check = default(DateTime);
                    //DateTime InToTime_Check = default(DateTime);
                    //TimeSpan InTime_TimeSpan = default(TimeSpan);
                    //string From_Time_Str = "";
                    string To_Time_Str = "";
                    DataTable DS_Time = new DataTable();
                    DataTable DS_InTime = new DataTable();
                    string Final_InTime = "";
                    string Final_OutTime = "";
                    string Final_Shift = "";
                    DataTable Shift_DS_Change = new DataTable();
                    Int32 K = 0;
                    bool Shift_Check_blb = false;
                    string Shift_Start_Time_Change = null;
                    string Shift_End_Time_Change = null;
                    string Employee_Time_Change = "";
                    DateTime ShiftdateStartIN_Change = default(DateTime);
                    DateTime ShiftdateEndIN_Change = default(DateTime);
                    DateTime EmpdateIN_Change = default(DateTime);

    string Fin_Year;
    string Months_Full_Str;
    //string Months_Full_Str;
    string Date_Col_Str;
    string Month_Int;
    string Spin_Wages;
    string Wages;
    string Spin_Machine_ID_Str;
    string[] Att_Date_Check;
    string Att_Already_Date_Check;
    string Att_Year_Check;
    string Month_Name_Change;
    string halfPresent;
   
    //string Att_Year_Check = "";
    string Token_No_Get;
    TimeSpan ts4 = new TimeSpan();
   
    Decimal Total_Days_Count;
    double Final_Count;
  
    double Present_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;
    double FullNight_Shift_Count;
    double NFH_Week_Off_Minus_Days;

    Boolean NFH_Day_Check = false;
    Boolean Check_Week_Off_DOJ = false;
    int aintI = 1;
    int aintK = 1;
    int aintCol;
    int Appsent_Count;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionCompanyName;
    string SessionLocationName;

    //String Att_Already_Date_Check;
    //String Att_Year_Check;
    //String[] Att_Date_Check;
    BALDataAccess objdata = new BALDataAccess();
    int shiftCount = 0;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string todate;
    string fromdate;
    string SSQL;
    string Emp_WH_Day;
    //string Spin_Machine_ID_Str = "";
    string SessionUserType;
    DateTime WH_DOJ_Date_Format;
    DateTime Week_Off_Date;

    DataTable Datacells = new DataTable();
    DataTable mEmployeeDS = new DataTable();
    DataTable dsEmployee = new DataTable();
   // DataTable AutoDataTable = new DataTable();
    Double NFH_Final_Disp_Days;

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Payroll Attendance";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();

            DataColumn auto = new DataColumn();
            auto.AutoIncrement = true;
            auto.AutoIncrementSeed = 1;


            AutoDataTable.Columns.Add("MachineID");
            AutoDataTable.Columns.Add("Token No");
            AutoDataTable.Columns.Add("EmpName");
            AutoDataTable.Columns.Add("H.Allowed");
            //AutoDataTable.Columns.Add("Days");
            AutoDataTable.Columns.Add("N/FH");
            AutoDataTable.Columns.Add("OT Days");
            AutoDataTable.Columns.Add("SPG Allow");
            AutoDataTable.Columns.Add("Canteen Days Minus");
            AutoDataTable.Columns.Add("OT Hours");
            AutoDataTable.Columns.Add("W.H");
            AutoDataTable.Columns.Add("Fixed W.Days");
            AutoDataTable.Columns.Add("NFH W.Days");
            AutoDataTable.Columns.Add("Total Month Days");
            AutoDataTable.Columns.Add("MachineID_Encrypt");

            fromdate = Request.QueryString["FromDate"].ToString();
            todate = Request.QueryString["ToDate"].ToString();
            Spin_Wages = Request.QueryString["Wages"].ToString();
            Wages = Request.QueryString["Wages"].ToString();


            DataTable dtIPaddress = new DataTable();
            dtIPaddress = objdata.BelowFourHours(SessionCcode.ToString(), SessionLcode.ToString());


            if (dtIPaddress.Rows.Count > 0)
            {
                for (int i = 0; i < dtIPaddress.Rows.Count; i++)
                {
                    if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                    {
                        mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                    else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                    {
                        mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                }
            }
            Payroll_Att_Fill();
            Write_payroll_attenance();
        }

    }

    protected void UploadDataTableToExcel(DataTable dtRecords)
    {
        string XlsPath = Server.MapPath(@"~/Add_data/test.xls");
        string attachment = string.Empty;
        if (XlsPath.IndexOf("\\") != -1)
        {
            string[] strFileName = XlsPath.Split(new char[] { '\\' });
            attachment = "attachment; filename=" + strFileName[strFileName.Length - 1];
        }
        else
            attachment = "attachment; filename=" + XlsPath;
        try
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            string tab = string.Empty;

            foreach (DataColumn datacol in dtRecords.Columns)
            {
                Response.Write(tab + datacol.ColumnName);
                tab = "\t";
            }
            Response.Write("\n");

            foreach (DataRow dr in dtRecords.Rows)
            {
                tab = "";
                for (int j = 0; j < dtRecords.Columns.Count; j++)
                {
                    Response.Write(tab + Convert.ToString(dr[j]));
                    tab = "\t";
                }

                Response.Write("\n");
            }
            Response.End();
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }
    }


   




    public void Payroll_Att_Fill()
    {
        if (fromdate != "" && todate != "")
        {

            SSQL = "";
            SSQL = "select MachineID,MachineID_Encrypt";
            SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + ",ExistingCode";
            SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "' And IsActive='Yes' and Wages='" + Wages + "'";

            //if (SessionUserType == "2")
            //{
            //    SSQL = SSQL + " And IsNonAdmin='1'";
            //}



            dsEmployee = objdata.ReturnMultipleValue(SSQL);
            int i1 = 0;
            if (dsEmployee.Rows.Count > 0)
            {
                //AutoDataTable.Rows.Count = 0;
                //AutoDataTable.Columns.Count = 0;
                //int rcount = dsEmployee.Rows.Count + 3;
                //int ccount = dsEmployee.Columns.Count + 1;
                //int ccounted = dsEmployee.Columns.Count;

                //AutoDataTable.Rows.Add();
                DataColumn dc = new DataColumn();

              


                for (int j = 0; j < dsEmployee.Rows.Count; j++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    
                 


                    AutoDataTable.Rows[i1][0] = dsEmployee.Rows[j]["MachineID"].ToString();

                    AutoDataTable.Rows[i1][1] = dsEmployee.Rows[j]["ExistingCode"].ToString();
                    
                    //Datacells.Rows[i1][0] = dsEmployee.Rows[j]["MachineID"].ToString();
                    AutoDataTable.Rows[i1][13] = dsEmployee.Rows[j]["MachineID_Encrypt"].ToString();
                    //Datacells.Rows[i1][13] = dsEmployee.Rows[j]["MachineID_Encrypt"].ToString();
                    AutoDataTable.Rows[i1][2] = dsEmployee.Rows[j]["FirstName"].ToString();
                    //Datacells.Rows[i1][2] = dsEmployee.Rows[j]["FirstName"].ToString();
                    Encry_MachineID = dsEmployee.Rows[j]["MachineID_Encrypt"].ToString();
                    i1++;
                 }
                //for (int i = 0; i < dsEmployee.Rows.Count; i++)
                //{
                //    for (int j = 0; j < dsEmployee.Columns.Count; j++)
                //    {
                //        AutoDataTable.Rows[i + 3][j] = dsEmployee.Rows[i][j].ToString();
                //    }
                //}
            }


            //SSQL = "";
            //SSQL = "select ShiftDesc,StartIN,StartIN_Days,EndIN,EndIn_Days,StartOut,StartOut_Days,EndOut,EndOut_Days";
            //SSQL = SSQL + " from Shift_Mst Where CompCode='" + SessionCcode.ToString() + "'";
            //SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "'";
            //SSQL = SSQL + " And (ShiftDesc Like '%Shift%' Or ShiftDesc Like '%GENERAL%')";
            //DataTable mDataTable = objdata.ReturnMultipleValue(SSQL);
            //if (mDataTable.Rows.Count <= 0)
            //{
            //    return;
            //}
            //shiftCount = mDataTable.Rows.Count * 2;
            //DateTime date1 = Convert.ToDateTime(Txtfrom.Text);
            //DateTime date2 = Convert.ToDateTime(TxtTo.Text);
            //TimeSpan iColVal = date2 - date1.AddDays(1);
            //String mStartIN = "";
            //String mEndIN = "";
            //String mStartIN_Days = "";
            //String mEndIN_Days = "";
            //String mStartOUT = "";
            //String mEndOUT = "";
            //String mStartOUT_Days = "";
            //String mEndOUT_Days = "";

            //DataTable mLocalDS = new DataTable();
            //for (int iCol2 = 0; iCol2 >= mDataTable.Rows.Count; iCol2--)
            //{
            //    mStartIN = mDataTable.Rows[iCol2]["StartIN"].ToString();
            //    mEndIN = mDataTable.Rows[iCol2]["EndIN"].ToString();
            //    mStartIN_Days = mDataTable.Rows[iCol2]["StartIN_Days"].ToString();
            //    mEndIN_Days = mDataTable.Rows[iCol2]["EndIn_Days"].ToString();

            //    mStartOUT = mDataTable.Rows[iCol2]["StartOUT"].ToString().ToString();
            //    mEndOUT = mDataTable.Rows[iCol2]["EndOUT"].ToString().ToString();
            //    mStartOUT_Days = mDataTable.Rows[iCol2]["StartOUT_Days"].ToString();
            //    mEndOUT_Days = mDataTable.Rows[iCol2]["EndOUT_Days"].ToString();

            //    int at = AutoDataTable.Columns.Count - 1;

            //    AutoDataTable.Rows[0][at] = fromdate.ToString();
            //    AutoDataTable.Rows[1][at] = mDataTable.Rows[iCol2]["ShiftDesc"];
            //    AutoDataTable.Rows[2][at] = "IN";

            //    SSQL = "";
            //    SSQL = "Select MachineID,Min(TimeIN) as [TimeIN] from LogTime_IN Where Compcode='" + SessionCcode.ToString() + "'";
            //    SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "'";
            //    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
            //    SSQL = SSQL + " And TimeIN <='" + date2.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
            //    SSQL = SSQL + " Group By MachineID";
            //    SSQL = SSQL + " Order By Min(TimeIN)";


            //    mLocalDS = objdata.ReturnMultipleValue(SSQL);

            //    foreach (DataRow dr in mLocalDS.Rows)
            //    {
            //        //dr["MachineID"] = Decryption(dr["MachineID"]);
            //        dr["MachineID"] = (dr["MachineID"]);
            //    }

            //    if (mLocalDS.Rows.Count > 0)
            //    {
            //        for (int dsRow = 0; dsRow <= mLocalDS.Rows.Count - 1; dsRow++)
            //        {
            //            for (int gridRow = 3; gridRow <= AutoDataTable.Rows.Count; gridRow--)
            //            {
            //                if (AutoDataTable.Rows[gridRow][1] == mLocalDS.Rows[dsRow]["MachineID"])
            //                {
            //                    AutoDataTable.Rows[gridRow][1] = mLocalDS.Rows[dsRow]["TimeIN"];
            //                    goto a;
            //                }
            //            }
            //        a:
            //            continue;
            //        }
            //    }

            //    DataColumn dc = new DataColumn();
            //    AutoDataTable.Columns.Add(dc);
            //    at = AutoDataTable.Columns.Count - 1;

            //    DataColumn dc1 = new DataColumn();
            //    AutoDataTable.Columns.Add(dc1);

            //    AutoDataTable.Rows[0][at] = fromdate.ToString();
            //    AutoDataTable.Rows[1][at] = mDataTable.Rows[iCol2]["ShiftDesc"];
            //    AutoDataTable.Rows[2][at] = "OUT";

            //    SSQL = "";
            //    SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT Where Compcode='" + SessionCcode.ToString() + "'";
            //    SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "'";
            //    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
            //    SSQL = SSQL + " And TimeOUT <='" + date2.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
            //    SSQL = SSQL + " Group By MachineID";
            //    SSQL = SSQL + " Order By Max(TimeOUT)";

            //    mLocalDS = objdata.ReturnMultipleValue(SSQL);

            //    if (mLocalDS.Rows.Count > 0)
            //    {
            //        for (int gridRow = 3; gridRow <= AutoDataTable.Rows.Count; gridRow--)
            //        {

            //            for (int dsRow = 0; dsRow <= mLocalDS.Rows.Count - 1; dsRow++)
            //            {

            //                if (AutoDataTable.Rows[gridRow][0].ToString() == mLocalDS.Rows[dsRow]["MachineID"].ToString())
            //                {
            //                    AutoDataTable.Rows[gridRow][at] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[dsRow]["TimeOUT"]);
            //                    goto bb;
            //                }
            //            }
            //        bb:
            //            continue;
            //        }
            //    }

            //    DataColumn dc2 = new DataColumn();
            //    AutoDataTable.Columns.Add(dc2);

            //    date1 = date1.AddDays(1);
            //    while (date1 > date2)
            //    {

            //    }
           // }

        }
    }




    public void Write_payroll_attenance()
    {

        
        
        try
        {

            DataColumn auto = new DataColumn();
            auto.AutoIncrement = true;
            auto.AutoIncrementSeed = 1;



            Datacells.Columns.Add("MachineID");
            Datacells.Columns.Add("Token No");
            Datacells.Columns.Add("EmpName");
            Datacells.Columns.Add("H.Allowed");
            //Datacells.Columns.Add("Days");
            Datacells.Columns.Add("N/FH");
            Datacells.Columns.Add("OT Days");
            Datacells.Columns.Add("SPG Allow");
            Datacells.Columns.Add("Canteen Days Minus");
            Datacells.Columns.Add("OT Hours");
            Datacells.Columns.Add("W.H");
            Datacells.Columns.Add("Fixed W.Days");
            Datacells.Columns.Add("NFH W.Days");
            Datacells.Columns.Add("Total Month Days");
            Datacells.Columns.Add("MachineID_Encrypt");
            Datacells.Columns.Add("Total");

            int aintCol = 4;
            int Month_Name_Change = 1;

           
            //string ds1 = string.Format("{dd/MM/yyyy}", Txtfrom.Text);
            date1 = Convert.ToDateTime(fromdate);
           // string dd =string.Format("{dd/MM/yyyy}",TxtTo.Text);
            date2 = Convert.ToDateTime(todate);
            int dayCount = (int)((date2 - date1).TotalDays);
            int daysAdded = 0;
            Att_Already_Date_Check = "";

            AutoDataTable.Columns.Add("Days");

            while (dayCount >= 0)
            {
                DateTime d1 = date1.AddDays(daysAdded);
                string date_str = Convert.ToString(d1);

                Att_Date_Check = date_str.Split('/');
                if (aintCol == 3)
                {
                    Att_Already_Date_Check = Att_Date_Check[1];
                    Att_Year_Check = Att_Date_Check[2];

                    Month_Name_Change = Convert.ToUInt16(Att_Already_Date_Check);
                    aintCol = aintCol + 1;
                }
                else
                {
                }

                dayCount = dayCount - 1;
                daysAdded = daysAdded + 1;
            }
            aintI = 0;
            aintK = 1;

            for (int intRow = 0; intRow < AutoDataTable.Rows.Count; intRow++)
            {
                aintK = 1;

                Datacells.NewRow();
                Datacells.Rows.Add();
                
                //for (int j = 0; j < dsEmployee.Rows.Count; j++)
                //{
                //    AutoDataTable.NewRow();
                //    AutoDataTable.Rows.Add();
                //    AutoDataTable.Rows[i1][0] = dsEmployee.Rows[j]["MachineID"].ToString();
                //    AutoDataTable.Rows[i1][2] = dsEmployee.Rows[j]["FirstName"].ToString();
                //    i1++;
                //}

                for (aintCol = 0; aintCol <= 1; aintCol++)
                {
                    aintK += 1;
                }

                string Emp_WH_Day;
                DataTable DS_WH = new DataTable();
                string DOJ_Date_Str;
                SSQL = "Select * from Employee_MST where MachineID='" + AutoDataTable.Rows[intRow][0].ToString() + "' And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                //if (SessionUserType == "2")
                //{
                //    SSQL = SSQL + " And IsNonAdmin='1'";
                //}

                
                DS_WH = objdata.ReturnMultipleValue(SSQL);

                if (DS_WH.Rows.Count != 0)
                {
                    Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                    DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();

                }
                else
                {
                    Emp_WH_Day = "";
                    DOJ_Date_Str = "";
                }

                int colIndex = aintK;

                //decimal Present_Count = 0;
                decimal Month_WH_Count = 0;
                //decimal Present_WH_Count = 0;
                //Int32 Appsent_Count = 0;
                //decimal Final_Count = 0;
                decimal Total_Days_Count = 0;
                //decimal Month_Mid_Join_Total_Days_Count = 0;

                string Already_Date_Check = null;
                string Year_Check = null;
                Int32 Days_Insert_RowVal = 0;

                //decimal FullNight_Shift_Count = 0;
                // Int32 NFH_Days_Count = 0;
                //decimal NFH_Days_Present_Count = 0;
                Already_Date_Check = "";
                Year_Check = "";


                for (aintCol = 0; aintCol < daysAdded; aintCol++)
                {


                    Spin_Machine_ID_Str = AutoDataTable.Rows[aintI]["MachineID"].ToString();

                    // Machine_ID_Str = Datacells.Rows[aintI]["MachineID"].ToString();
                    Machine_ID_Str = AutoDataTable.Rows[aintI]["MachineID_Encrypt"].ToString();
                    OT_Week_OFF_Machine_No = AutoDataTable.Rows[aintI]["MachineID"].ToString();

                    dtime = date1.AddDays(aintCol);
                    dtime1 = date1.AddDays(aintCol).AddDays(1);
                    Date_Value_Str = String.Format(Convert.ToString(dtime), "yyyy/MM/dd");
                    Date_Value_Str1 = String.Format(Convert.ToString(dtime1), "yyyy/MM/dd");

                    DataTable mLocalDS1 = new DataTable();

                    //ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                    //ss = ss + " Where LT.Compcode='" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    //ss = ss + "And LT.TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    //ss = ss + "And LT.TimeIN <='" + dtime1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    //ss = ss + "AND em.ShiftType='" + ddlshifttype.SelectedItem.Text + "' And EM.Compcode='" + SessionCcode.ToString() + "'";
                    //ss = ss + "And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsActive='yes' Group By LT.MachineID Order By Min(LT.TimeIN)";





                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);


                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Time_IN_Str = "";
                    }

                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);

                    if (mLocalDS1.Rows.Count <= 0)
                    {
                        Time_Out_Str = "";
                    }


                    if (mLocalDS.Rows.Count != 0)
                    {

                        //string InTime_Check = mLocalDS.Rows[0]["TimeIN"].ToString());
                        //string InTime_Check_str = String.Format("HH:mm:ss", InTime_Check);
                        //InTime_Check = Convert.ToDateTime(AutoDTable.Rows[iRow2][7].ToString());
                        //InToTime_Check = InTime_Check.AddHours(2);

                        //string s1 = InTime_Check.ToString("HH:mm:ss");
                        //string s2 = InToTime_Check.ToString("HH:mm:ss");

                        //string InTime_Check_str = String.Format("HH:mm:ss", InTime_Check);
                        //string InToTime_Check_str = String.Format("HH:mm:ss", InToTime_Check);
                        //InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
                        //From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                        //InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
                        //To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;


                        InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"]);

                        InToTime_Check = InTime_Check.AddHours(2);

                        string s1 = InTime_Check.ToString("HH:mm:ss");
                        string s2 = InToTime_Check.ToString("HH:mm:ss");

                        //string InTime_Check_str = String.Format("HH:mm:ss", s1);
                        //string InToTime_Check_str = String.Format("HH:mm:ss", s2);

                        InTime_TimeSpan = TimeSpan.Parse(s1);
                        // From_Time_Str = Convert.ToString(InTime_TimeSpan.Hour) + ":" + Convert.ToString(InTime_TimeSpan.Minute);
                        From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                        TimeSpan InTime_TimeSpan1 = TimeSpan.Parse(s2);
                        To_Time_Str = InTime_TimeSpan1.Hours + ":" + InTime_TimeSpan1.Minutes;

                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + From_Time_Str + "' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";
                        DS_Time = objdata.ReturnMultipleValue(SSQL);

                        if (DS_Time.Rows.Count != 0)
                        {
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                            DS_InTime = objdata.ReturnMultipleValue(SSQL);

                            if (DS_InTime.Rows.Count != 0)
                            {
                                Final_InTime = DS_InTime.Rows[0][0].ToString();
                                SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                                Shift_DS_Change = objdata.ReturnMultipleValue(SSQL);
                                Shift_Check_blb = false;

                                for (int k = 0; k > Shift_DS_Change.Rows.Count; k--)
                                {
                                    Shift_Start_Time_Change = Date_Value_Str.ToString() + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();

                                    if (Shift_DS_Change.Rows[K]["EndIN_Days"].ToString() == "1")
                                    {
                                        Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                    }
                                    else
                                    {
                                        Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                    }

                                    ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                    ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                    EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);

                                    if (EmpdateIN_Change >= ShiftdateStartIN_Change & EmpdateIN_Change <= ShiftdateEndIN_Change)
                                    {
                                        Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                        break;
                                    }
                                }
                                if (Shift_Check_blb == true)
                                {
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                                    mLocalDS = objdata.ReturnMultipleValue(SSQL);

                                    if (Final_Shift == "SHIFT2")
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
                                    }
                                    else
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "17:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                                    }

                                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                                }
                                else
                                {
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

                                    mLocalDS = objdata.ReturnMultipleValue(SSQL);

                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

                                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);

                                }
                            }
                            else
                            {

                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

                                mLocalDS = objdata.ReturnMultipleValue(SSQL);

                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

                                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);

                            }
                        }
                        else
                        {
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                            mLocalDS = objdata.ReturnMultipleValue(SSQL);


                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                            mLocalDS1 = objdata.ReturnMultipleValue(SSQL);

                        }
                    }


                    if (mLocalDS.Rows.Count > 1)
                    {
                        for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();

                            if (mLocalDS1.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_Out_Str = "";
                            }


                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                            }
                            if (Time_IN_Str == "" || Time_Out_Str == "")
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                                        
                            date1 = System.Convert.ToDateTime(Time_IN_Str);
                            date2 = System.Convert.ToDateTime(Time_Out_Str);

                            TimeSpan ts = new TimeSpan();
                            ts = date2.Subtract(date1);
                            Total_Time_get = Convert.ToString(ts.Hours);
                            ts4 = ts4.Add(ts);
                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;



                            string tsfour = Convert.ToString(ts4.Minutes);
                            string tsfour1 = Convert.ToString(ts4.Hours);



                            if (Left_Val(tsfour, 1) == "-" | Left_Val(tsfour1, 1) == "-" | Emp_Total_Work_Time_1 == "0:0")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date2.Subtract(date1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = ts.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
                                Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                else
                                {
                                    time_Check_dbl = Convert.ToInt16(time_Check_dbl) + Convert.ToInt16(Total_Time_get.ToString());
                                }
                            }
                            }
                        }
                    }
                    else
                    {
                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout <= mLocalDS1.Rows.Count - 1; tout++)
                        {
                            if (mLocalDS1.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";
                            }
                            else
                            {
                                Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                            }
                        }

                        if (Time_IN_Str == "" || Time_Out_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {

                         
                           date1 = System.Convert.ToDateTime(Time_IN_Str);
                           date2 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts;
                            ts = date2.Subtract(date1);
                            Total_Time_get = ts.Hours.ToString();
                            ts4 = ts4.Add(ts);

                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = ts.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
                                Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";

                                }
                            }
                            else
                            {
                                time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
                            }
                        }
                    }
                    string Emp_Total_Work_Time;
                    string Final_OT_Work_Time;
                    string Final_OT_Work_Time_Val;
                    //String Emp_Total_Work_Time;
                    string Employee_Week_Name;
                    string Assign_Week_Name;
                    //mLocalDS == null;

                    Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                    SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "'";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Assign_Week_Name = "";
                    }
                    else
                    {
                        Assign_Week_Name = "";
                    }
                    Boolean NFH_Day_Check = false;
                    DateTime NFH_Date;
                    DateTime DOJ_Date_Format;
                    DateTime Date_Value = new DateTime();

                    Date_Value = Convert.ToDateTime(Date_Value_Str.ToString());
                    string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value.ToString("yyyy/MM/dd") + "'";

                   // string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                    mLocalDS = objdata.ReturnMultipleValue(qry_nfh);

                    if (mLocalDS.Rows.Count > 0)
                    {
                        if (DOJ_Date_Str != "")
                        {
                            NFH_Date = Convert.ToDateTime(mLocalDS.Rows[0]["NFHDate"]);
                            // NFH_Date = mLocalDS.Rows[0]["NFHDate"];
                            DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str.ToString());
                            if (DOJ_Date_Format.Date < NFH_Date.Date)
                            {
                                NFH_Day_Check = true;
                                NFH_Days_Count = NFH_Days_Count + 1;
                            }
                            else
                            {

                            }
                        }
                        else
                        {
                            NFH_Day_Check = true;
                            NFH_Days_Count = NFH_Days_Count + 1;

                        }
                    }
                    else
                    {
                    }


                    if (Employee_Week_Name.ToString() == Assign_Week_Name.ToString())
                    {

                    }
                    else
                    {
                        double Calculate_Attd_Work_Time = 0;
                        double Calculate_Attd_Work_Time_half = 0;
                        SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and Calculate_Work_Hours <> '' And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And IsActive='Yes'";
                        //if (SessionUserType == "2")
                        //{
                        //    SSQL = SSQL + " And IsNonAdmin='1'";
                        //}

                        
                        mLocalDS = objdata.ReturnMultipleValue(SSQL);
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Calculate_Attd_Work_Time = 7;
                        }
                        else
                        {
                            //Calculate_Attd_Work_Time = (!string.IsNullOrEmpty(mLocalDS.Rows[0]["Calculate_Work_Hours"]) ? mLocalDS.Rows[0]["Calculate_Work_Hours"] : 0);
                            if (Calculate_Attd_Work_Time == 0 || Calculate_Attd_Work_Time == null)
                            {
                                Calculate_Attd_Work_Time = 7;
                            }
                            else
                            {
                                Calculate_Attd_Work_Time = 7;
                            }

                        }

                        Calculate_Attd_Work_Time_half = 4.0;
                        halfPresent = "0";
                        if (time_Check_dbl >= Calculate_Attd_Work_Time_half & time_Check_dbl < Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "1";
                        }

                        if (time_Check_dbl >= Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "0";
                        }
                    }

                    string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                    if (Emp_WH_Day == Attn_Date_Day)
                    {
                        System.DateTime Week_Off_Date = default(System.DateTime);
                        System.DateTime WH_DOJ_Date_Format = default(System.DateTime);
                        bool Check_Week_Off_DOJ = false;


                        if (Wages == "STAFF" | Wages == "Watch & Ward")
                        {
                            if (!string.IsNullOrEmpty(DOJ_Date_Str))
                            {
                                Week_Off_Date = Convert.ToDateTime(Date_Value_Str.ToString());

                                WH_DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str.ToString());
                                if (WH_DOJ_Date_Format.Date <= Week_Off_Date.Date)
                                {
                                    Check_Week_Off_DOJ = true;
                                }

                                else
                                {
                                    Check_Week_Off_DOJ = false;
                                }
                            }
                            else
                            {
                                Check_Week_Off_DOJ = true;
                            }

                        }
                        else
                        {
                            Check_Week_Off_DOJ = true;
                        }


                        if (Check_Week_Off_DOJ == true)
                        {
                            Month_WH_Count = Month_WH_Count + 1;
                            if (isPresent == true)
                            {
                                if (NFH_Day_Check == false)
                                {
                                    if (halfPresent == "1")
                                    {
                                        Present_WH_Count = Convert.ToDouble(Present_WH_Count + 0.5);
                                    }
                                    else if (halfPresent == "0")
                                    {
                                        Present_WH_Count = Present_WH_Count + 1;
                                    }
                                }
                            }
                        }
                        else
                        {
                            isPresent = false;
                        }

                    }

                    if (isPresent == true)
                    {

                        if (halfPresent == "1")
                        {
                            Present_Count = Convert.ToDouble(Present_Count) + 0.5;
                            if (NFH_Day_Check == true)
                            {
                                NFH_Days_Present_Count = Convert.ToDouble(NFH_Days_Present_Count) + 0.5;
                            }
                        }
                        else if (halfPresent == "0")
                        {
                            Present_Count = Present_Count + 1;
                            if (NFH_Day_Check == true)
                            {
                                NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                            }
                        }

                        bool Check_Spinning_Eligible = false;
                        DataTable DS_Spinning_Incv = new DataTable();


                        DateTime datech = Convert.ToDateTime(Date_Value_Str.ToString());

                        //Month_Int = Month(Date_Value_Str);
                        Month_Int = Convert.ToString(datech.Month);
                        int year_Int1 = Convert.ToInt16(datech.Year);
                        //int Month_Int1 = Convert.ToInt16(Month_Int);
                        int Month_Int1 = Convert.ToInt16(Month_Int);
                        if (Month_Int1 >= 4)
                        {
                            Fin_Year = year_Int1 + "-" + year_Int1 + 1;
                        }
                        else
                        {
                            Fin_Year = year_Int1 - 1 + "-" + year_Int1;
                        }


                        if (Month_Int == "1")
                        {
                            Months_Full_Str = "JAN";
                        }
                        else if (Month_Int == "2")
                        {
                            Months_Full_Str = "FEB";
                        }
                        else if (Month_Int == "3")
                        {
                            Months_Full_Str = "MAR";
                        }
                        else if (Month_Int == "4")
                        {
                            Months_Full_Str = "APR";
                        }
                        else if (Month_Int == "5")
                        {
                            Months_Full_Str = "MAY";
                        }
                        else if (Month_Int == "6")
                        {
                            Months_Full_Str = "JUN";
                        }
                        else if (Month_Int == "7")
                        {
                            Months_Full_Str = "JUL";
                        }
                        else if (Month_Int == "8")
                        {
                            Months_Full_Str = "AUG";
                        }
                        else if (Month_Int == "9")
                        {
                            Months_Full_Str = "SEP";
                        }
                        else if (Month_Int == "10")
                        {
                            Months_Full_Str = "OCT";
                        }
                        else if (Month_Int == "11")
                        {
                            Months_Full_Str = "NOV";
                        }
                        else if (Month_Int == "12")
                        {
                            Months_Full_Str = "DEC";
                        }

                        Months_Full_Str = Left_Val(Months_Full_Str, 3);
                        int Date_Col_Str = datech.Day;

                        //Date_Col_Str = DateAndTime.Day(Date_Value_Str);
                        SSQL = "Select * from SpinIncentiveDetPart where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Months='" + Months_Full_Str + "' And FinYear='" + Fin_Year + "' And Wages='" + Spin_Wages + "' And MachineID='" + Spin_Machine_ID_Str + "'";
                        DS_Spinning_Incv = objdata.ReturnMultipleValue(SSQL);

                        if (DS_Spinning_Incv.Rows.Count != 0)
                        {
                            Check_Spinning_Eligible = Convert.ToBoolean(DS_Spinning_Incv.Rows[0]["D" + Date_Col_Str].ToString());
                        }
                        else
                        {
                            Check_Spinning_Eligible = false;
                        }

                        if (Spin_Wages.ToString() == "HOSTEL")
                        {
                            if (Check_Spinning_Eligible == true)
                            {
                                if (halfPresent == "1")
                                {
                                    FullNight_Shift_Count = FullNight_Shift_Count + 0.5;
                                }
                                else if (halfPresent == "0")
                                {
                                    FullNight_Shift_Count = FullNight_Shift_Count + 1;
                                }

                            }
                        }
                        if (Spin_Wages.ToString() == "REGULAR" & Check_Spinning_Eligible == true)
                        {

                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                            mLocalDS = objdata.ReturnMultipleValue(SSQL);


                            InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
                            InToTime_Check = InTime_Check.AddHours(2);
                            InTime_Check = Convert.ToDateTime(String.Format("{HH:mm:ss}", InTime_Check));
                            InToTime_Check = Convert.ToDateTime(String.Format("{HH:mm:ss}", InToTime_Check));
                            InTime_TimeSpan = TimeSpan.Parse(InTime_Check.ToString());
                            From_Time_Str = Convert.ToString(InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes);
                            InTime_TimeSpan = TimeSpan.Parse(InToTime_Check.ToString());
                            To_Time_Str = Convert.ToString(InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes);


                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + From_Time_Str + "' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";
                            DS_Time = objdata.ReturnMultipleValue(SSQL);

                            if (DS_Time.Rows.Count != 0)
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                                DS_InTime = objdata.ReturnMultipleValue(SSQL);

                                if (DS_InTime.Rows.Count != 0)
                                {
                                    Final_InTime = DS_InTime.Rows[0][0].ToString();
                                    SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                                    Shift_DS_Change = objdata.ReturnMultipleValue(SSQL);
                                    Shift_Check_blb = false;



                                    for (K = 0; K <= Shift_DS_Change.Rows.Count - 1; K++)
                                    {
                                        Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
                                        if (Shift_DS_Change.Rows[K]["EndIN_Days"].ToString() == "1")
                                        {
                                            Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                        }
                                        else
                                        {
                                            Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                        }

                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                        EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                        if (EmpdateIN_Change >= ShiftdateStartIN_Change & EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                            Shift_Check_blb = true;
                                            break;
                                        }
                                    }

                                    if (Shift_Check_blb == true)
                                    {
                                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                                        mLocalDS = objdata.ReturnMultipleValue(SSQL);
                                    }
                                    else
                                    {
                                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                                        mLocalDS = objdata.ReturnMultipleValue(SSQL);
                                    }
                                }
                                else
                                {
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                                }

                            }
                            else
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                                mLocalDS = objdata.ReturnMultipleValue(SSQL);


                            }


                            string Shift_Start_Time;
                            string Shift_End_Time;
                            string Employee_Time;
                            DataTable Shift_Ds = new DataTable();
                            Boolean Shift_Add_or_Check = false;
                            DateTime ShiftdateStartIN;
                            DateTime ShiftdateEndIN;
                            DateTime EmpdateIN = new DateTime();

                            SSQL = "Select * from Shift_Mst where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc='SHIFT2'";
                            Shift_Ds = objdata.ReturnMultipleValue(SSQL);

                            if (Shift_Ds.Rows.Count != 0)
                            {

                                Shift_Start_Time = Date_Value_Str + " " + Shift_Ds.Rows[0]["StartIN"].ToString();
                                Shift_End_Time = Date_Value_Str + " " + Shift_Ds.Rows[0]["EndIN"].ToString();

                                if (mLocalDS.Rows.Count > 1)
                                {
                                    for (int x = 0; x <= mLocalDS.Rows.Count - 1; x++)
                                    {
                                        Employee_Time = mLocalDS.Rows[0]["TimeIN"].ToString();
                                        ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                                        ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);
                                        EmpdateIN = System.Convert.ToDateTime(Employee_Time);

                                        if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                                        {
                                            if (halfPresent == "1")
                                            {
                                                FullNight_Shift_Count = FullNight_Shift_Count + 0.5;
                                            }
                                            else if (halfPresent == "0")
                                            {
                                                FullNight_Shift_Count = FullNight_Shift_Count + 1;
                                            }

                                            Shift_Add_or_Check = true;
                                        }
                                    }
                                }
                                else
                                {
                                    Employee_Time = mLocalDS.Rows[0]["TimeIN"].ToString();
                                    ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                                    ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);
                                    EmpdateIN = System.Convert.ToDateTime(Employee_Time);

                                    if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                                    {
                                        if (halfPresent == "1")
                                        {
                                            FullNight_Shift_Count = FullNight_Shift_Count + 0.5;
                                        }
                                        else if (halfPresent == "0")
                                        {
                                            FullNight_Shift_Count = FullNight_Shift_Count + 1;
                                        }
                                        Shift_Add_or_Check = true;
                                    }

                                }
                            }


                            if (Shift_Add_or_Check = false)
                            {
                                SSQL = "Select * from Shift_Mst where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc='SHIFT3'";
                                Shift_Ds = objdata.ReturnMultipleValue(SSQL);
                                if (Shift_Ds.Rows.Count != 0)
                                {
                                    Shift_Start_Time = Date_Value_Str + " " + Shift_Ds.Rows[0]["StartIN"].ToString();
                                    Shift_End_Time = Date_Value_Str1 + " " + Shift_Ds.Rows[0]["EndIN"].ToString();


                                    if (mLocalDS.Rows.Count > 1)
                                    {
                                        for (int x = 0; x <= mLocalDS.Rows.Count - 1; x++)
                                        {
                                            Employee_Time = mLocalDS.Rows[0]["TimeIN"].ToString();
                                            ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                                            ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);
                                            EmpdateIN = System.Convert.ToDateTime(Employee_Time);

                                            if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                                            {
                                                if (halfPresent == "1")
                                                {
                                                    FullNight_Shift_Count = FullNight_Shift_Count + 0.5;
                                                }
                                                else if (halfPresent == "0")
                                                {
                                                    FullNight_Shift_Count = FullNight_Shift_Count + 1;
                                                }
                                                Shift_Add_or_Check = true;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Employee_Time = mLocalDS.Rows[0]["TimeIN"].ToString();
                                        ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                                        ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);
                                        EmpdateIN = System.Convert.ToDateTime(Employee_Time);


                                        if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                                        {
                                            if (halfPresent == "1")
                                            {
                                                FullNight_Shift_Count = FullNight_Shift_Count + 0.5;
                                            }
                                            else if (halfPresent == "0")
                                            {
                                                FullNight_Shift_Count = FullNight_Shift_Count + 1;
                                            }
                                            Shift_Add_or_Check = true;
                                        }

                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Appsent_Count = Appsent_Count + 1;
                    }


                    colIndex += shiftCount;
                    aintK += 1;

                    System.DateTime Report_Date = default(System.DateTime);
                    System.DateTime DOJ_Date_Format_Check = default(System.DateTime);
                    if (Wages == "STAFF" | Wages == "Watch & Ward")
                    {
                        if (!string.IsNullOrEmpty(DOJ_Date_Str))
                        {
                            Report_Date = Convert.ToDateTime(Date_Value_Str.ToString());
                            DOJ_Date_Format_Check = Convert.ToDateTime(DOJ_Date_Str.ToString());
                            if (DOJ_Date_Format_Check.Date <= Report_Date.Date)
                            {
                                Month_Mid_Join_Total_Days_Count = Month_Mid_Join_Total_Days_Count + 1;
                            }
                            else
                            {
                            }
                        }
                        else
                        {
                            Month_Mid_Join_Total_Days_Count = Month_Mid_Join_Total_Days_Count + 1;
                        }
                        Total_Days_Count = Total_Days_Count + 1;
                    }
                    else
                    {
                        Total_Days_Count = Total_Days_Count + 1;
                    }
                }


                Final_Count = Present_Count;
                if (NFH_Days_Count == 0)
                {
                    NFH_Days_Present_Count = 0;
                }
                string Token_No;
                string Machine_ID_Str_Excel;
                DataTable Token_DS = new DataTable();



                Machine_ID_Str_Excel = Datacells.Rows[aintI][1].ToString();

                SSQL = "Select * from Employee_Mst where MachineID='" + Datacells.Rows[aintI][1].ToString() + "' and Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
                //if (SessionUserType == "2")
                //{
                //    SSQL = SSQL + " And IsNonAdmin='1'";
                //}

                
                Token_DS = objdata.ReturnMultipleValue(SSQL);
                if (Token_DS.Rows.Count != 0)
                {
                    Token_No_Get = Token_DS.Rows[0]["ExistingCode"].ToString();
                    Int32 Fixed_Work_Days = 0;
                }

                if (Wages == "STAFF" | Wages == "Watch & Ward")
                {
                    Fixed_Work_Days = Month_Mid_Join_Total_Days_Count - Month_WH_Count;
                    Final_Count = Final_Count - Present_WH_Count;
                    Final_Count = Final_Count - NFH_Days_Present_Count;
                    Present_WH_Count = Present_WH_Count + NFH_Days_Present_Count;
                }
                else
                {
                    Fixed_Work_Days = 0;
                    Final_Count = Final_Count - Present_WH_Count;
                    Final_Count = Final_Count - NFH_Days_Present_Count;
                    Present_WH_Count = Present_WH_Count;
                }



                string NFH_Year_Str = "0";
                string NFH_Month_Str = "";
                string NFH_Rule_OLD = "";
                int NFH_Display_Days = 0;
                string NFH_Double_Wages_Rule = "";
                DataTable mDataSet = new DataTable();
                NFH_Year_Str = Convert.ToString(date1.Year);
                NFH_Month_Str = Convert.ToString(date1.Month);
                SSQL = "Select * from NFH_Rule_Det where NFHYear='" + NFH_Year_Str + "' And Months='" + NFH_Month_Str + "'";
                mDataSet = objdata.ReturnMultipleValue(SSQL);

                if (mDataSet.Rows.Count != 0)
                {
                    if (mDataSet.Rows[0]["OLD_Rule"].ToString() == "True")
                    {
                        NFH_Rule_OLD = "True";
                    }
                    else
                    {
                        NFH_Rule_OLD = "False";
                    }
                    if (mDataSet.Rows[0]["NFH_Wages_Rule"].ToString() == "True")
                    {
                        NFH_Double_Wages_Rule = "True";
                    }
                    else
                    {
                        NFH_Double_Wages_Rule = "False";
                    }

                    if (mDataSet.Rows[0]["NFH_Disp_Days"].ToString() == "")
                    {
                        NFH_Display_Days = 0;
                    }
                    else
                    {
                        NFH_Display_Days = Convert.ToInt16(mDataSet.Rows[0]["NFH_Disp_Days"].ToString());
                    }



                    //NFH_Display_Days = (!string.IsNullOrEmpty(mDataSet.Rows[0]["NFH_Disp_Days"].ToString) ? mDataSet.Rows[0]["NFH_Disp_Days"] : 0);
                }
                if (Final_Count == 0)
                {
                    NFH_Days_Count = 0;
                }

                if (NFH_Rule_OLD == "True" & Final_Count != 0 & NFH_Days_Count != 0 & NFH_Display_Days != 0)
                {

                    //double NFH_Week_Off_Minus_Days = 0;
                    //double NFH_Final_Disp_Days = 0;

                    if (NFH_Display_Days != 0)
                        NFH_Week_Off_Minus_Days = Convert.ToDouble(NFH_Display_Days - NFH_Days_Count);
                    //Double nfh = Convert.ToDouble(NFH_Week_Off_Minus_Days);

                    Decimal nfh = Convert.ToDecimal(NFH_Week_Off_Minus_Days);

                    //int nfh = Convert.ToInt16(NFH_Week_Off_Minus_Days);

                    NFH_Final_Disp_Days = Convert.ToDouble(nfh) + NFH_Days_Count;
                    if (NFH_Week_Off_Minus_Days <= Present_WH_Count)
                    {
                        Present_WH_Count = Present_WH_Count - NFH_Week_Off_Minus_Days;
                    }
                    else
                    {
                        double Balance_NFH_Days_Minus_IN_WorkDays = 0;
                        Balance_NFH_Days_Minus_IN_WorkDays = NFH_Week_Off_Minus_Days - Present_WH_Count;

                        Final_Count = Final_Count - Balance_NFH_Days_Minus_IN_WorkDays;
                        Present_WH_Count = 0;
                    }
                    NFH_Days_Count = NFH_Final_Disp_Days;
                }
                else
                {

                }

                if (NFH_Double_Wages_Rule == "true")
                {
                    NFH_Days_Present_Count = NFH_Days_Present_Count;
                }
                else
                {
                    NFH_Days_Present_Count = 0;
                }

                if (Final_Count != 0)
                {

                    Datacells.Rows[aintI][0] = AutoDataTable.Rows[intRow]["MachineID"].ToString();
                    Datacells.Rows[aintI][2] = AutoDataTable.Rows[intRow]["EmpName"].ToString();
                    Datacells.Rows[aintI][13] = AutoDataTable.Rows[intRow]["MachineID_Encrypt"].ToString();
                    Datacells.Rows[aintI][1] = AutoDataTable.Rows[intRow]["Token No"].ToString();
                   

                    //Datacells.Rows[aintI][1] = Machine_ID_Str_Excel;
                    Datacells.Rows[aintI][3] = "0";
                    Datacells.Rows[aintI][4] = Final_Count;
                    Datacells.Rows[aintI][5] = Final_Count;
                    Datacells.Rows[aintI][6] = "0";
                    Datacells.Rows[aintI][7] = NFH_Days_Count;
                    Datacells.Rows[aintI][9] = FullNight_Shift_Count;
                    Datacells.Rows[aintI][10] = 0;
                    Datacells.Rows[aintI][11] = 0;

                    if (Wages == "STAFF" || Wages == "Watch & Ward")
                    {

                        Datacells.Rows[aintI][8] = "0";
                        Datacells.Rows[aintI][12] = Present_WH_Count;
                        Datacells.Rows[aintI][13] = Fixed_Work_Days;
                    }
                    else
                    {
                        Datacells.Rows[aintI][8] = Present_WH_Count;
                        Datacells.Rows[aintI][12] = 0;
                        Datacells.Rows[aintI][13] = 0;
                    }

                    Datacells.Rows[aintI][14] = NFH_Days_Present_Count;
                    Datacells.Rows[aintI][15] = Total_Days_Count;
                }
                else
                {
                    Datacells.Rows[aintI][0] = AutoDataTable.Rows[intRow]["MachineID"].ToString();
                    Datacells.Rows[aintI][2] = AutoDataTable.Rows[intRow]["EmpName"].ToString();
                    Datacells.Rows[aintI][14] = AutoDataTable.Rows[intRow]["MachineID_Encrypt"].ToString();
                    Datacells.Rows[aintI][1] = AutoDataTable.Rows[intRow]["Token No"].ToString();
                   
                    //Datacells.Rows[aintI][1] = Machine_ID_Str_Excel;
                    //Datacells.Rows[aintI][2] = Token_No_Get;
                    Datacells.Rows[aintI][3] = "0";
                    Datacells.Rows[aintI][4] = Final_Count;
                    Datacells.Rows[aintI][5] = Final_Count;
                    Datacells.Rows[aintI][6] = 0;
                    Datacells.Rows[aintI][7] = NFH_Days_Count;
                    Datacells.Rows[aintI][9] = FullNight_Shift_Count;
                    Datacells.Rows[aintI][10] = 0;
                    Datacells.Rows[aintI][11] = 0;

                    if (Wages == "STAFF" || Wages == "Watch & Ward")
                    {
                        Datacells.Rows[aintI][8] = 0;
                        Datacells.Rows[aintI][12] = Present_WH_Count;
                        Datacells.Rows[aintI][13] = Fixed_Work_Days;
                    }
                    else
                    {

                        Datacells.Rows[aintI][8] = Present_WH_Count;
                        Datacells.Rows[aintI][12] = 0;
                        Datacells.Rows[aintI][13] = 0;
                    }

                    Datacells.Rows[aintI][14] = NFH_Days_Present_Count;
                    Datacells.Rows[aintI][15] = Total_Days_Count;


                }
               
                aintI = aintI + 1;
                Total_Days_Count = 0;
                Final_Count = 0;
                Appsent_Count = 0;
                Present_Count = 0;
                Fixed_Work_Days = 0;
                Month_WH_Count = 0;
                Present_WH_Count = 0;
                NFH_Days_Count = 0;
                NFH_Days_Present_Count = 0;
                
            }


        }

       
        catch (Exception e)
        {
        }

        UploadDataTableToExcel(Datacells);
    }

}




                  

                         
                                
                               



                                  

                             



                        



                    

                
       

        
    

   

               
               
                
               
                   
                   
                    

       

            
               
         

    