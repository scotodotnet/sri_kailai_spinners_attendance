﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;


public partial class Leave_Details : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    ManualAttendanceClass objManual = new ManualAttendanceClass();
    bool ErrFlag = false;
    string SSQL;
    string MonthDays_Get;
    DateTime fromdate;
    DateTime todate;
    DataTable dtdDisplay = new DataTable();
    string deptname;

    DataTable mDataSet = new DataTable();
  

    int leave_Mst; 
    int leave; 
    int Remaining;
    static string tt;
    static string Auto_ID;
    static string id;
    string leavetype1;
    string Timing = "";
    int HOUR;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
        

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Leave Details";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("LeaveManagement"));
                li.Attributes.Add("class", "droplink active open");

                //Dropdown_Company();
                //Dropdown_Location();
                DropDown_TokenNumber();
                DropDown_LeaveTypeMst();
                DisplayLeaveReg();
                Financial_Year();
                Dis_Hours();
            }
        }
    }

    public void DisplayLeaveReg()
    {
        DataTable dtDisplay = new DataTable();
        SSQL = "select * from Leave_Register_Mst";
        dtdDisplay = objdata.ReturnMultipleValue(SSQL);
        rptrCustomer.DataSource = dtdDisplay;
        rptrCustomer.DataBind();
      

     }

    public void Financial_Year()
    {
        String CurrentYear1;
        int CurrentYear;
        CurrentYear1 = DateTime.Now.Year.ToString();
        CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());
        if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
        {
            CurrentYear = CurrentYear - 1;
        }
        for (int i = 0; i < 1; i++)
        {
            tt = CurrentYear1;
            CurrentYear = CurrentYear - 1;
            string cy = Convert.ToString(CurrentYear);
            CurrentYear1 = cy;

        }
    }

    

    public void DropDown_LeaveTypeMst()
    {
        DataTable dt = new DataTable();
        dt = objdata.dropdown_LeaveType();
        ddlLeaveType.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["LeaveType"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        for (int i = 0; i < dt.Rows.Count; i++) 
        {
            ddlLeaveType.Items.Add(dt.Rows[i]["LeaveType"].ToString());
        }

    }
    public void DropDown_TokenNumber()
    {
        DataTable dt = new DataTable();
        dt = objdata.dropdown_TokenNumber(SessionCcode, SessionLcode);
        ddlTicketno.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["EmpName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlTicketno.Items.Add(dt.Rows[i]["EmpName"].ToString());
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        string Encrpt_ID = UTF8Encryption(ddlTicketno.SelectedItem.Text);

        DataTable dtbLeave = new DataTable();

        if (ddlTicketno.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Your TicketNo');", true);
            ErrFlag = true;
        }

        else if (ddlLeaveType.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Your Leave Type');", true);
            ErrFlag = true;
        }


        else if (btnSave.Text == "Update")
        {
            if (ddlLeaveType.Text == "PM")
            {
                HOUR = Convert.ToInt32(txtdays.Text);

                if (rdbEvening.Checked == true)
                {
                    Timing = "PM";
                }
                else
                {
                    Timing = "AM";
                }
            }

            int currentleave = Convert.ToInt32(txtTotalDays.Text);

            DataTable dtdUpdate = new DataTable();
            SSQL = "select*from Leave_Register_Mst where Auto_ID='" + Auto_ID + "';";
            dtdUpdate = objdata.ReturnMultipleValue(SSQL);
            string rr = dtdUpdate.Rows[0]["LeaveAllowedDays"].ToString();
            string dd = dtdUpdate.Rows[0]["LeaveRemainDays"].ToString();


            int leaveallowed = Convert.ToInt32(rr.ToString());
            if (leaveallowed < currentleave)
            {
                ddlTicketno.Enabled = true;
                btnSave.Text = "Save";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Sorry You Have Limited Leave');", true);
                ErrFlag = true;
            }
            else
            {

                ddlTicketno.Enabled = true;
                DataTable dtdatatable = new DataTable();
                SSQL = "update Leave_Register_Mst set EmpNo = '" + ddlTicketno.SelectedItem.Text + "'";
                SSQL = SSQL + ",ExistingCode = '" + txtExistingCode.Text + "',Machine_No = '" + ddlTicketno.SelectedItem.Text + "'";
                SSQL = SSQL + ",Machine_Encrypt = '" + Encrpt_ID + "',EmpName = '" + txtEmpName.Text + "'";
                SSQL = SSQL + ",FromDate = '" + txtFromDate.Text + "',ToDate = '" + txtToDate.Text + "',TotalDays = '" + txtTotalDays.Text + "'";
                SSQL = SSQL + ",LeaveType = '" + ddlLeaveType.SelectedItem.Text + "',LeaveDesc = '" + txtLeaveDesc.Text + "'";
                SSQL = SSQL + ",LeaveStatus = 'N',LeaveAllowedDays = '" + rr + "',LeaveRemainDays = '" + dd + "',FinancialYear='" + tt + "' where EmpNo='" + ddlTicketno.SelectedItem.Text + "' and Auto_ID='" + Auto_ID + "' and Hours='" + HOUR + "' and Timing='" + Timing + "' ";
                dtdatatable = objdata.ReturnMultipleValue(SSQL);


                Clear();
                DisplayLeaveReg();
                ddlTicketno.Enabled = true;
                btnSave.Text = "Save";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Updated Successfully');", true);
                ErrFlag = true;
            }
        }

        else
        {
            if (ddlLeaveType.Text == "PM")
            {
                HOUR = Convert.ToInt32(txtdays.Text);

                if (rdbEvening.Checked == true)
                {
                    Timing = "PM";
                }
                else
                {
                    Timing = "AM";
                }
            }
            DataTable dtdGender = new DataTable();

            SSQL = "select Gender,CatName from Employee_Mst where EmpNo='" + ddlTicketno.SelectedItem.Text + "'";
            SSQL = SSQL + " and CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'";

            dtdGender = objdata.ReturnMultipleValue(SSQL);
            if (dtdGender.Rows[0]["CatName"].ToString() != "")
            {
                deptname = dtdGender.Rows[0]["CatName"].ToString();
            }
            DataTable dtdDays = new DataTable();
            string leavetype = ddlLeaveType.SelectedItem.Text;
            SSQL = "select Days from LeaveType_Mst where LeaveType='" + leavetype.ToString() + "' and CatName= '" + deptname + "'";
            dtdDays = objdata.ReturnMultipleValue(SSQL);
            if (dtdDays.Rows.Count > 0)
            {
                string alloweddays = dtdDays.Rows[0]["Days"].ToString();
                leave_Mst = Convert.ToInt32(alloweddays.ToString());
                leave = Convert.ToInt32(txtTotalDays.Text);
                if (leave_Mst < leave)
                {
                    Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Sorry You Have Only Limited Leave');", true);
                    ErrFlag = true;

                }
            }



            string ss = "select Min(LeaveRemainDays) As Leave from Leave_Register_Mst where EmpNo='" + ddlTicketno.SelectedItem.Text + "' and CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "' and FinancialYear= '" + tt.ToString() + "'";
            DataTable dtdleave = new DataTable();
            dtdleave = objdata.ReturnMultipleValue(ss);

            if (dtdleave.Rows.Count > 0)
            {
                string remainday = dtdleave.Rows[0]["Leave"].ToString();
                if (remainday != "")
                {
                    int rday = Convert.ToInt16(remainday.ToString());
                    if (rday >= leave)
                    {
                        int days = Convert.ToInt16(remainday.ToString());
                        if (days > 0)
                        {
                            leave_Mst = Convert.ToInt32(remainday.ToString());
                            leave = Convert.ToInt32(txtTotalDays.Text);
                            Remaining = leave_Mst - leave;

                            DataTable dtdata = new DataTable();
                            SSQL = "select Machine_No from Leave_Register_Mst where Machine_No = '" + ddlTicketno.SelectedItem.Text + "'and FromDate = '" + txtFromDate.Text + "'and ToDate='" + txtToDate.Text + "'";
                            dtdata = objdata.ReturnMultipleValue(SSQL);

                            if (dtdata.Rows.Count <= 0)
                            {
                                SSQL = "Insert Into Leave_Register_Mst(CompCode,LocCode,EmpNo,ExistingCode,Machine_No,Machine_Encrypt,EmpName";
                                SSQL = SSQL + ",FromDate,ToDate,TotalDays,LeaveType,LeaveDesc,LeaveStatus,From_Date_Dt,To_Date_Dt,LeaveAllowedDays,LeaveRemainDays,FinancialYear,Hours,Timing) Values('" + SessionCcode + "'";
                                SSQL = SSQL + ",'" + SessionLcode + "','" + ddlTicketno.SelectedItem.Text + "','" + txtExistingCode.Text + "','" + ddlTicketno.SelectedItem.Text + "'";
                                SSQL = SSQL + ",'" + UTF8Encryption(ddlTicketno.SelectedItem.Text) + "','" + txtEmpName.Text + "','" + txtFromDate.Text + "','" + txtToDate.Text + "'";
                                SSQL = SSQL + ",'" + txtTotalDays.Text + "','" + ddlLeaveType.SelectedItem.Text + "','" + txtLeaveDesc.Text + "','N','" + (Convert.ToDateTime(txtFromDate.Text).AddDays(0).ToString("dd-MMM-yyyy")) + "','" + (Convert.ToDateTime(txtToDate.Text).AddDays(0).ToString("dd-MMM-yyyy")) + "'";
                                SSQL = SSQL + ",'" + leave_Mst + "','" + Remaining + "','" + tt + "','" + HOUR + "','" + Timing + "')";
                                dtbLeave = objdata.ReturnMultipleValue(SSQL);

                                Clear();
                                DisplayLeaveReg();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Save Successfully');", true);
                                ErrFlag = true;

                            }
                            else
                            {
                                Clear();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Already Exist');", true);
                                ErrFlag = true;

                            }
                        }

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Sorry You Have Only Limited Leave');", true);
                        ErrFlag = true;
                        Clear();
                    }
                }
                else
                {
                    DataTable dtdinsert = new DataTable();
                    SSQL = "select Days from LeaveType_Mst where LeaveType='" + leavetype.ToString() + "' and CatName= '" + deptname + "' and FinancialYear= '" + tt + "'";
                    SSQL = SSQL + "and CompCode='" + SessionCcode + "' and LocCode = '" + SessionLcode + "'";
                    dtdinsert = objdata.ReturnMultipleValue(SSQL);
                    if (dtdinsert.Rows.Count > 0)
                    {
                        string da = dtdinsert.Rows[0]["Days"].ToString();
                        if (da.ToString() != "")
                        {
                            int leave_Mst = Convert.ToInt32(da);
                            string daa = txtTotalDays.Text;
                            int leave = Convert.ToInt32(daa);
                            Remaining = leave_Mst - leave;

                            SSQL = "Insert Into Leave_Register_Mst(CompCode,LocCode,EmpNo,ExistingCode,Machine_No,Machine_Encrypt,EmpName";
                            SSQL = SSQL + ",FromDate,ToDate,TotalDays,LeaveType,LeaveDesc,LeaveStatus,From_Date_Dt,To_Date_Dt,LeaveAllowedDays,LeaveRemainDays,FinancialYear,Hours,Timing) Values('" + SessionCcode + "'";
                            SSQL = SSQL + ",'" + SessionLcode + "','" + ddlTicketno.SelectedItem.Text + "','" + txtExistingCode.Text + "','" + ddlTicketno.SelectedItem.Text + "'";
                            SSQL = SSQL + ",'" + UTF8Encryption(ddlTicketno.SelectedItem.Text) + "','" + txtEmpName.Text + "','" + txtFromDate.Text + "','" + txtToDate.Text + "'";
                            SSQL = SSQL + ",'" + txtTotalDays.Text + "','" + ddlLeaveType.SelectedItem.Text + "','" + txtLeaveDesc.Text + "','N','" + (Convert.ToDateTime(txtFromDate.Text).AddDays(0).ToString("dd-MMM-yyyy")) + "','" + (Convert.ToDateTime(txtToDate.Text).AddDays(0).ToString("dd-MMM-yyyy")) + "'";
                            SSQL = SSQL + ",'" + leave_Mst + "','" + Remaining + "','" + tt + "','" + HOUR + "','" + Timing + "')";
                            dtbLeave = objdata.ReturnMultipleValue(SSQL);

                            Clear();
                            DisplayLeaveReg();

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Save Successfully');", true);
                            ErrFlag = true;


                        }

                    }
                }

            }
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    public void Dis_Hours()
    {
        if (ddlLeaveType.Text != "PM" )
        {
            txtdays.Enabled = false;
            rdbMorning.Enabled = false;
            rdbEvening.Enabled = false;
        }
        else
        {
            txtdays.Enabled = true;
            rdbMorning.Enabled = true;
            rdbEvening.Enabled = true;
        }
    }
    public void Clear()
    {
        ddlTicketno.Enabled = true;
        txtExistingCode.Text = "";
        txtFromDate.Text = "";
        txtLeaveDesc.Text = "";
        ddlTicketno.SelectedIndex = 0;
        //txtMachineNo.Text = "";
        txtToDate.Text = "";
        txtTotalDays.Text = "";
        ddlLeaveType.SelectedIndex = 0;
        txtEmpName.Text = "";
        ddlTicketno.Items.Clear();
        ddlLeaveType.Items.Clear();

        btnSave.Text = "Save";

        DropDown_TokenNumber();
        DropDown_LeaveTypeMst();
       
     
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
       string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
       id = commandArgs[0];
       Auto_ID = commandArgs[1];
  
       
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(id, Auto_ID);
                break;
            case ("Edit"):
                EditRepeaterData(id, Auto_ID);
                break;
        }
    }

    private void EditRepeaterData(string id,string Auto_ID)
    {
       
        DataTable Dtd = new DataTable();
        SSQL = "select * from Leave_Register_Mst where EmpNo='"+ id +"' and Auto_ID='"+ Auto_ID +"'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        if (Dtd.Rows.Count > 0)
        {
            ddlTicketno.SelectedItem.Text = Dtd.Rows[0]["EmpNo"].ToString();
            txtExistingCode.Text = Dtd.Rows[0]["ExistingCode"].ToString();
            txtEmpName.Text = Dtd.Rows[0]["EmpName"].ToString();
            txtFromDate.Text = Dtd.Rows[0]["FromDate"].ToString();
            txtToDate.Text = Dtd.Rows[0]["ToDate"].ToString();
            txtTotalDays.Text = Dtd.Rows[0]["TotalDays"].ToString();
            ddlLeaveType.SelectedItem.Text = Dtd.Rows[0]["LeaveType"].ToString();
            txtLeaveDesc.Text = Dtd.Rows[0]["LeaveDesc"].ToString();
            DisplayLeaveReg();
            btnSave.Text = "Update";
            ddlTicketno.Enabled = false;      
        }

    }
    private void DeleteRepeaterData(string id, string Auto_ID)
    {
      
        DataTable Dtd = new DataTable();
        SSQL = "Delete Leave_Register_Mst where EmpNo='" + id + "' and Auto_ID = '" + Auto_ID + "'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        DisplayLeaveReg();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
        ErrFlag = true;
    }


    protected void ddlTicketno_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dted = new DataTable();
        dted = objdata.Manual_Data(ddlTicketno.SelectedItem.Text);
        if (dted.Rows.Count > 0)
        {
            txtExistingCode.Text = dted.Rows[0]["ExistingCode"].ToString();
            txtEmpName.Text = dted.Rows[0]["FirstName"].ToString();
            string gender = dted.Rows[0]["Gender"].ToString();
       
            if (gender == "Male")
            {

                ddlLeaveType.Items.Clear();

                DataTable dt = new DataTable();
                dt = objdata.DropDown_LeaveTypeMale();
                ddlLeaveType.DataSource = dt;
                DataRow dr = dt.NewRow();
                dr["LeaveType"] = "- select -";
                dt.Rows.InsertAt(dr, 0);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ddlLeaveType.Items.Add(dt.Rows[i]["LeaveType"].ToString());
                }
           }
          
        }
    }
    protected void txtToDate_TextChanged(object sender, EventArgs e)
    {
        fromdate = Convert.ToDateTime(txtFromDate.Text);
        todate = Convert.ToDateTime(txtToDate.Text);
        int dayCount = (int)((todate - fromdate).TotalDays) + 1;
        txtTotalDays.Text = Convert.ToString(dayCount.ToString());
    }

    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
    protected void rdbMorning_CheckedChanged(object sender, EventArgs e)
    {
        rdbEvening.Checked = false;
    }
    protected void rdbEvening_CheckedChanged(object sender, EventArgs e)
    {
        rdbMorning.Checked = false;
    }
    protected void ddlLeaveType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Dis_Hours();

    }
}
     