﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;




public partial class ManualAttendance_Add : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    ManualAttendanceClass objManual = new ManualAttendanceClass();
    string SSQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
        
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Manual Attendance";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                li.Attributes.Add("class", "droplink active open");
                DisplayManualatterndance();
            }

            DisplayManualatterndance();

        }
    }

    public void DisplayManualatterndance()
    {
        DataTable dtDisplay = new DataTable();
        dtDisplay = objdata.DisplayManualAttend(SessionCcode, SessionLcode);

        rptrCustomer.DataSource = dtDisplay;
        rptrCustomer.DataBind();
    }

    protected void btnNEWEMPentry_Click(object sender, EventArgs e)
    {
        Response.Redirect("manual_attendance.aspx");
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string id1 = commandArgs[0];
        string id2 = commandArgs[1];

        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(id1, id2);
                break;

        }
    }

    private void DeleteRepeaterData(string id1, string id2)
    {


     

        DateTime Dateid = DateTime.ParseExact(id2, "MM/dd/yyyy", null);

        
        DataTable Dtd = new DataTable();
        SSQL = "Delete ManAttn_Details where EmpNo='" + id1 + "' and AttnDate = '" + id2 + "'";
        objdata.ReturnMultipleValue(SSQL);
        //SSQL = "Delete LogTime_IN where MachineID='" + UTF8Encryption(id1) + "' and TimeIN Like '" + Dateid.ToString("yyyy-dd-MM h:mm tt") +"'";




        SSQL = "Delete LogTime_IN where MachineID='" + UTF8Encryption(id1) + "'";
        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And TimeIN >='" + Dateid.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeIN <='" + Dateid.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59'";
        objdata.ReturnMultipleValue(SSQL);




        SSQL = "Delete LogTime_OUT where MachineID='" + UTF8Encryption(id1) + "'";
        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And TimeOUT >='" + Dateid.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + Dateid.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59'";
        objdata.ReturnMultipleValue(SSQL);

        DisplayManualatterndance();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
    }

    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

}
