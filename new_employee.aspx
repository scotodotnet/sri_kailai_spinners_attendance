﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="new_employee.aspx.cs" Inherits="new_emp1" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#tableEmployee').dataTable();
                }
            });
        };
    </script>

    <script type="text/javascript">
        function showimagepreview(input) {
         alert("called")
         if (input.files && input.files[0]) {
             
             var filerdr = new FileReader();
             filerdr.onload = function (e) {
                 $('#img1').attr('src', e.target.result);
             }
             filerdr.readAsDataURL(input.files[0]);
         }
     }
     </script>





    <style type="text/css">
        .CalendarCSS {
            background-color: White;
            color: Black;
            border: 1px solid #646464;
            font-size: xx-large;
            font-weight: bold;
            margin-bottom: 4px;
            margin-top: 2px;
        }
    </style>
    <style type="text/css">
        .ajax__calendar_container {
            z-index: 1000;
            background-color: White;
        }
    </style>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script type="text/javascript">
        function showimagepreview(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <link href="bootstrap.min.css" rel="stylesheet" />
    <script src="bootstrap.min.js"></script>
    <script src="jquery-1.11.1.min.js"></script>
    <script src="jquery.dataTables.min.js"></script>
    <link href="jquery.dataTables.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $.extend($.fn.dataTableExt.oStdClasses, {
                "sFilterInput": "form-control",
                "sLengthSelect": "form-control"
            });
            $('#example').DataTable();
        });
    </script>
    <script language="javascript" type="text/javascript">


        $(function () {
            $('#btn6').click(function () {
                $('.nav-tabs a[href="#tab6"]').tab('show');
                var tab = "tab7";
                $('.nav-tabs a[href="#' + tab + '"]').tab('show');
                return true;
            })
        })

        $(function () {
            $('#btn5').click(function () {
                $('.nav-tabs a[href="#tab5"]').tab('show');
                var tab = "tab6";
                $('.nav-tabs a[href="#' + tab + '"]').tab('show');
                return true;
            })
        })

        $(function () {
            $('#btn4').click(function () {

//var tempDocType=document.getElementById('<%=ddlDocType.ClientID %>').value;
//var tempDocNo=$("#<%=txtDocNo.ClientID %>").val();
//var tempDocDesc=$("#<%=txtDocDesc.ClientID %>").val();

                //if((tempDocType!="1")&(tempDocNo!="")&(tempDocDesc!=""))
                //{
                $('.nav-tabs a[href="#tab4"]').tab('show');
                var tab = "tab5";
                $('.nav-tabs a[href="#' + tab + '"]').tab('show');
                return true;
                //}
                //else {
                //if (tempDocType == "1") {
                //alert("Please Select Document Type" + "\n");
                //}
                //else if (tempDocNo == "") {
                //alert("Please Enter Document No" + "\n");
                //}
                //else if (tempDocDesc == "") {
                //alert("Please Enter Document Description" + "\n");
                //}
                //else {
                //return "";
                //}

                //}

            })
        })



        $(function () {
            $('#btn3').click(function () {

//var tempGuardName=$("#<%=txtGuardName.ClientID %>").val();
                var tempPermAddr = $("#<%=txtPermanentAddr.ClientID %>").val();
                var tempPermTlk = $("#<%=txtpermanentTlk.ClientID %>").val();
                var tempPermDst = $("#<%=txtPermanentDst.ClientID %>").val();
                var tempEMPMobNum = $("#<%=txtEmpMobileNum.ClientID %>").val();
//var tempParentMob1=$("#<%=txtParentMob1.ClientID %>").val();
                if ((tempPermAddr != "") & (tempPermTlk != "") & (tempPermDst != "") & (tempEMPMobNum != "")) {
                    $('.nav-tabs a[href="#tab3"]').tab('show');
                    var tab = "tab4";
                    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
                    return true;
                }
                else {
                    if (tempPermAddr == "") {
                        alert("Please Enter Permanent Address" + "\n");
                    }
                    else if (tempPermTlk == "") {
                        alert("Please Enter Permanent Taluk" + "\n");
                    }
                    else if (tempPermDst == "") {
                        alert("Please Enter Permanent District" + "\n");
                    }
                    else if (tempEMPMobNum == "") {
                        alert("Please Enter Employee Mobile Number" + "\n");
                    }
                    else {
                        return "";
                    }


                }
            })
        })


        $(function () {
            $('#btn2').click(function () {
                var tempDept = document.getElementById('<%=ddlDepartment.ClientID %>');
                var tempDeptText = tempDept.options[tempDept.selectedIndex].text;
                var tempDesgn = document.getElementById('<%=ddlDesignation.ClientID %>');
                var tempDesgnText = tempDesgn.options[tempDesgn.selectedIndex].text;
                var tempShftType = document.getElementById('<%=ddlShiftType.ClientID %>');
                var tempShftTypeText = tempShftType.options[tempShftType.selectedIndex].text;
                var tempOTElgble = document.getElementById('<%=ddlOTEligible.ClientID %>');
                var tempOTElgbleText = tempOTElgble.options[tempOTElgble.selectedIndex].text;
                var tempWgType = document.getElementById('<%=ddlWagesType.ClientID %>');
                var tempWgTypeText = tempWgType.options[tempWgType.selectedIndex].text;
                var tempPF = document.getElementById('<%=ddlPFEligible.ClientID %>')
                var tempPFText = tempPF.options[tempPF.selectedIndex].text;
                var tempESI = document.getElementById('<%=ddlESIEligible.ClientID %>')
                var tempESIText = tempESI.options[tempESI.selectedIndex].text;


                if ((tempDeptText != "- select -") & (tempPFText != "- select -") & (tempESIText != "- select -") & (tempDesgnText != "- select -") & (tempShftTypeText != "- select -") & (tempOTElgbleText != "- select -") & (tempWgTypeText != "- select -")) {
                    $('.nav-tabs a[href="#tab2"]').tab('show');
                    var tab = "tab3";
                    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
                    return true;

                }

                else if (tempDeptText == "- select -") {
                    alert("Please Select Department" + "\n");
                }
                else if (tempPFText == "- select -") {
                    alert("Please Select PF" + "\n");
                }
                else if (tempESIText == "- select -") {
                    alert("Please Select ESI" + "\n");
                }
                else if (tempDesgnText == "- select -") {
                    alert("Please Select Designation" + "\n");
                }
                else if (tempShftTypeText == "- select -") {
                    alert("Please Select Shift Type" + "\n");
                }
                else if (tempOTElgbleText == "- select -") {
                    alert("Please Select OT Eligible" + "\n");
                }
                else if (tempWgTypeText == "- select -") {
                    alert("Please Select WagesType" + "\n");
                }


                else {
                    return "";
                }
            })
        })




        $(function () {
            $('#btn1').click(function () {
                var tempCatgry = document.getElementById('<%=ddlCategory.ClientID %>').value;
//var tempsubCatgry=document.getElementById('<%=ddlsubcategory.ClientID %>').value;
                var tempMachID = $("#<%=txtMachineID.ClientID %>").val();
                var tempExstNo = $("#<%=txtExistingNo.ClientID %>").val();
                var tempTickNo = $("#<%=txtTicketNo.ClientID %>").val();
                var tempfirstname = $("#<%=txtfirstname.ClientID %>").val();
                var templastname = $("#<%=txtlastname.ClientID %>").val();
                var tempDOB = $("#<%=txtdateofbirth.ClientID %>").val();
                var temp

                    = document.getElementById('<%=ddlGender.ClientID %>').value;
                var tempDOJ = $("#<%=txtdateofJoin.ClientID %>").val();
                var tempActive = document.getElementById('<%=ddlActive.ClientID %>').value;


                if ((tempCatgry != "1") & (tempMachID != "") & (tempExstNo != "") & (tempTickNo != "") & (tempfirstname != "") & (templastname != "") & (tempDOB != "") & (tempGender != "1") & (tempDOJ != "") & (tempActive != "1")) {
                    $('.nav-tabs a[href="#tab1"]').tab('show');
                    var tab = "tab2";
                    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
                    return true;
                }
                else {
                    if (tempCatgry == "1") {
                        alert("Please Select Category" + "\n");
                    }
                    //else if (tempsubCatgry == "1") {
                    //alert("Please Select SubCategory" + "\n");
                    //}
                    else if (tempMachID == "") {
                        alert("Please Enter MachineID" + "\n");
                    }
                    else if (tempExstNo == "") {
                        alert("Please Enter ExistingNo" + "\n");
                    }
                    else if (tempTickNo == "") {
                        alert("Please Enter TicketNo" + "\n");
                    }
                    else if (tempfirstname == "") {
                        alert("Please Enter Firstname" + "\n");
                    }
                    else if (templastname == "") {
                        alert("Please Enter Lastname" + "\n");
                    }
                    else if (tempDOB == "") {
                        alert("Please Enter Date Of Birth" + "\n");
                    }
                    else if (tempGender == "1") {
                        alert("Please select Gender" + "\n");
                    }
                    else if (tempDOJ == "") {
                        alert("Please Enter Date Of Join" + "\n");
                    }
                    else if (tempActive == "1") {
                        alert("Please select Active" + "\n");
                    }
                    else {
                        return "";
                    }

                }
            })
        })

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">New Employee</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
    <div class="col-md-9">
        <div class="panel panel-white">
        <div class="panel panel-primary">
            <div class="panel-heading clearfix">
                <h3 class="panel-title">New Employee</h3>
            </div>
            </div>
           
            <div class="panel-body">
               <div id="rootwizard">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Basic</a></li>
                        <li role="presentation"><a href="#tab2" data-toggle="tab"><i class="fa fa-building m-r-xs"></i>Official</a></li>
                        <li role="presentation"><a href="#tab3" data-toggle="tab"><i class="fa fa-smile-o m-r-xs"></i>Personal</a></li>
                        <li role="presentation"><a href="#tab4" data-toggle="tab"><i class="fa fa-image m-r-xs"></i>Documents</a></li>
						<li role="presentation"><a href="#tab5" data-toggle="tab"><i class="fa fa-certificate m-r-xs"></i>Experience</a></li>
						<li role="presentation"><a href="#tab6" data-toggle="tab"><i class="fa fa-check m-r-xs"></i>Adolescent</a></li>
						<li role="presentation"><a href="#tab7" data-toggle="tab"><i class="fa fa-money m-r-xs"></i>Salary</a></li>	
                    </ul>
                   
             <div class="progress progress-sm m-t-sm">
                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                </div>
             </div>             
          
                 
                 <form id="wizardForm">
               
                            <div class="tab-content">
                            
                             <div class="tab-pane active fade in" id="tab1">
                                  <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                                  <ContentTemplate>
                                
                                
                                    <div class="row m-b-lg">
                                        
                                        <div class="col-md-12">
                                            <div class="row">
											<div class="form-group col-md-4">
                                                <label for="exampleInputName">Category</label>
                                                    <asp:DropDownList ID="ddlCategory" runat="server" class="form-control col-md-6">
                                                        <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="1">STAFF</asp:ListItem>
                                                        <asp:ListItem Value="3" Text="2">LABOUR</asp:ListItem>
                                                   </asp:DropDownList>  <span class="mandatory">*</span>      
					                          </div>
                                              <div class="form-group col-md-4">
                                                    <label for="exampleInputName">Sub Category</label>
                                                            <asp:DropDownList ID="ddlsubcategory" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="1">INSIDER</asp:ListItem>
                                                            <asp:ListItem Value="3" Text="2">OUTSIDER</asp:ListItem>
                                                            </asp:DropDownList>
                                                           <%-- <span class="mandatory">*</span> --%>
                                               </div>
                                               <div class="form-group col-md-4">
                                                    <label for="exampleInputName">Machine ID</label>
                                                    <asp:TextBox ID="txtMachineID" class="form-control col-md-6" runat="server" onkeypress="return NumberOnly()"
                                                        MaxLength="9" AutoPostBack="True" ontextchanged="txtMachineID_TextChanged"></asp:TextBox><span class="mandatory">*</span> 
											   </div>
											</div>
										</div>

										<div class="col-md-12">
                                            <div class="row">
												<div class="form-group col-md-4">
                                                    <label for="exampleInputName">Existing Number</label>
                                                    <asp:TextBox ID="txtExistingNo" class="form-control col-md-6" runat="server"></asp:TextBox> 
                                                   <span class="mandatory">*</span> 
												</div>
                                                <div class="form-group col-md-4">
                                                    <label for="exampleInputName">Ticket Number</label>
                                                    <asp:TextBox ID="txtTicketNo" class="form-control col-md-6" runat="server" onkeypress="return NumberOnly()"></asp:TextBox>
                                                    <span class="mandatory">*</span> 
                                                </div>
                                            </div>
										</div>
										
                                        <div class="col-md-12"><hr></div>

                                        <div class="col-md-12">
                                            <div class="row">
												<div class="form-group col-md-4">
                                                    <label for="exampleInputName">First Name</label>
                                                    <asp:TextBox ID="txtfirstname" class="form-control col-md-6" runat="server"  onkeyup="checkNum()"></asp:TextBox>
                                                    <span class="mandatory">*</span> 
												</div>
                                                <div class="form-group col-md-4">
                                                    <label for="exampleInputName">Last Name / Initial</label>
                                                    <asp:TextBox ID="txtlastname" class="form-control col-md-6" runat="server"  onkeyup="checkNum()"></asp:TextBox>
                                                    <span class="mandatory">*</span> 
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="exampleInputName" >Date of Birth</label><span class="mandatory">*</span> 
                                                    <asp:TextBox ID="txtdateofbirth" class="form-control col-md-6" 
                                                        ontextchanged="txtdateofbirth_TextChanged" runat="server" 
                                                       ></asp:TextBox>
                                                 <cc1:CalendarExtender ID="CalendarExtender2" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtdateofbirth">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtdateofbirth" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
                                                 
                                                 
                                                 <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtdateofbirth" ID="RegularExpressionValidator2"
                                                  ValidationExpression = "^[\s\S]{10,}$" runat="server" ErrorMessage="Minimum 10 characters required."></asp:RegularExpressionValidator>
                                                                                                                                                
                                                 </div>
                                                                                               													
										   </div>
										</div>  
                                        

										<div class="col-md-12">
                                            <div class="row">
												 <div class="form-group col-md-2">
													<label for="exampleInputName">Age</label>
                                                    <asp:TextBox ID="txtage" class="form-control col-md-6" runat="server" 
                                                         AutoPostBack = "True"></asp:TextBox>
												</div>
												<div class="form-group col-md-2">
													<label for="exampleInputName">Gender</label>
                                                            <asp:DropDownList ID="ddlGender" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="1">Male</asp:ListItem>
                                                            <asp:ListItem Value="4" Text="3">Female</asp:ListItem>
                                                            </asp:DropDownList><span class="mandatory">*</span> 
												</div>			
                                                <div class="form-group col-md-4">
                                                    <label for="exampleInputName">Marital Status</label>
                                                            <asp:DropDownList ID="ddlmMaritalStatus" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="1">Single</asp:ListItem>
                                                            <asp:ListItem Value="3" Text="2">Married</asp:ListItem>
                                                            <asp:ListItem Value="4" Text="3">Divorced</asp:ListItem>
                                                            <asp:ListItem Value="5" Text="3">None</asp:ListItem>
                                                            
                                                            </asp:DropDownList>
                                                </div>
                                                
											    <div class="form-group col-md-4">
                                                    <label for="exampleInputName">Nationality</label>
                                                    <asp:TextBox ID="txtNationality" class="form-control col-md-6" runat="server" Text="Indian"></asp:TextBox>
                                                </div>
											</div>
										</div>

										<div class="col-md-12">
                                            <div class="row">
												
                                                <div class="form-group col-md-4">
                                                    <label for="exampleInputName">Religion</label>
                                                    <asp:TextBox ID="txtReligion" class="form-control col-md-6" runat="server"  onkeyup="checkNum()"></asp:TextBox>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="exampleInputName" >Blood Group</label>
                                                            <asp:DropDownList ID="ddlBloodGroup" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="A+"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="A-"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="B+"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="B-"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="AB+"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="AB-"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="O+"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="O-"></asp:ListItem>
                                                            </asp:DropDownList>
												</div>
											    
											</div>
										</div>
                                        
                                        <div class="col-md-12"><hr></div>

										<div class="col-md-12">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label for="exampleInputName">Height</label>
                                                    <div class="input-group m-b-sm">
                                                          <span class="input-group-addon" id="basic-addon1">cms</span>
                                                          <asp:TextBox ID="txtHeight" class="form-control col-md-6" runat="server" onkeypress="return NumberOnly()"></asp:TextBox>
                                                    </div>
                                                </div>
												
                                                <div class="form-group col-md-4">
                                                    <label for="exampleInputName">Weight</label>
                                                     <div class="input-group m-b-sm">
                                                          <span class="input-group-addon" id="basic-addon1">kg</span>
                                                          <asp:TextBox ID="txtWeight" class="form-control col-md-6" runat="server" onkeypress="return NumberOnly()"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="exampleInputName">Physically Challenged</label>
                                                            <asp:DropDownList ID="ddlPhysicalChng" runat="server" class="form-control col-md-6">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="Yes"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="No"></asp:ListItem>
                                                            </asp:DropDownList>
												</div>
											
											</div>
										</div>

										<div class="col-md-12">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label for="exampleInputName" >Date of Join</label><span class="mandatory">*</span>
                                                   <asp:TextBox ID="txtdateofJoin" class="form-control col-md-6"
                                                        runat="server" ></asp:TextBox> 
                                                      
                                                     <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtdateofJoin">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtdateofJoin" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
                                                 
                                                   <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtdateofJoin" ID="RegularExpressionValidator1"
                                                  ValidationExpression = "^[\s\S]{10,}$" runat="server" ErrorMessage="Minimum 10 characters required."></asp:RegularExpressionValidator>

                                                     
												</div>
												<div class="form-group col-md-4">
                                                    <label for="exampleInputName">Active</label><span class="mandatory">*</span>
                                                            <asp:DropDownList ID="ddlActive" runat="server" 
                                                        class="form-control col-md-6" AutoPostBack="true">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="Yes"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="No"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="Resigned"></asp:ListItem>
                                                            </asp:DropDownList>
												</div>
												<div class="form-group col-md-4">
                                                    <asp:Label ID="lblDateOfRegister" runat="server" Visible="False" for="exampleInputName" Text="Date of Resignation" AutoPostBack="true"></asp:Label>
												    <asp:TextBox ID="txtDateOfResignation" class="form-control date-picker" 
                                                        runat="server" Visible="False" AutoPostBack="true"></asp:TextBox>
                                                </div>
                                                
											</div>
										</div>
										
										<div class="col-md-12">
                                            <div class="row">
                                                <div class="form-group col-md-4">
												      <%--<div class="checker" class="form-control col-md-6" id="uniform-closeButton"><span><input id="closeButton" type="checkbox" value="checked" class="input-mini"></span></div> Is - Non Admin User--%>
												      <asp:CheckBox ID="AdminChkBx" runat="server" text="Is Non Admin User"/>
                                                </div>
                                                
                                          	</div>
										</div>
										
										</div>
										
										
										  </ContentTemplate>
                                </asp:UpdatePanel> 
										   
								            <ul class="pager wizard">
                                                <li class="next"><a href="#" id="btn1" class="btn btn-default">Next</a></li>
                                             </ul>  
                                                                                                                                                    
		                 </div> <!-- tab1-end -->	
										
							 <div class="tab-pane fade" id="tab2">
									
							      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                       <ContentTemplate>
									
									
                                                    <div class="row">
                                                      
                                                       <div class="col-md-12">
                                                            <div class="row">
																<div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Department</label>
                                                                        <asp:DropDownList ID="ddlDepartment" runat="server" class="form-control col-md-6">
                                                                       
                                                                        </asp:DropDownList><span class="mandatory">*</span> 
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Designation</label>
                                                                        <asp:DropDownList ID="ddlDesignation" runat="server" class="form-control col-md-6">
                                                                       
                                                                        </asp:DropDownList><span class="mandatory">*</span> 
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName" >Employee Type</label>
                                                                        <asp:DropDownList ID="ddlShiftType" runat="server" class="form-control col-md-6">
                                                                       
                                                                        </asp:DropDownList><span class="mandatory">*</span> 
																</div>
															
															</div>
														</div>

														<div class="col-md-12">
                                                            <div class="row">
																<div class="form-group col-md-4">
																<label for="example_Incentive">Incentive Eligible</label>
																<asp:DropDownList ID="ddlIncen_Eligible" runat="server" 
                                                                        class="form-control col-md-3" AutoPostBack="True" >
                                                                       
                                                                        <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="Yes"></asp:ListItem>
                                                                        <asp:ListItem Value="3" Text="No"></asp:ListItem>
                                                                        </asp:DropDownList><span class="mandatory">*</span> 
                                                                   
                                                                </div>
                                                                <div class="form-group col-md-2">
                                                                    <label for="exampleInputName">OT Eligible</label>
                                                                        <asp:DropDownList ID="ddlOTEligible" runat="server" 
                                                                        class="form-control col-md-3" AutoPostBack="True" 
                                                                        onselectedindexchanged="ddlOTEligible_SelectedIndexChanged">
                                                                        <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="Yes"></asp:ListItem>
                                                                        <asp:ListItem Value="3" Text="No"></asp:ListItem>
                                                                        </asp:DropDownList><span class="mandatory">*</span> 
                                                                </div>
                                                                <div class="form-group col-md-2">
                                                                    <label for="exampleInputName">OT Hours</label>
                                                                    <asp:TextBox ID="txtOTHr" class="form-control col-md-3" runat="server"></asp:TextBox>
																</div>
															    <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Weekly-Off</label>
                                                                        <asp:DropDownList ID="ddlWeekOff" runat="server" class="form-control col-md-6">
                                                                       <%-- <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="None"></asp:ListItem>
                                                                        <asp:ListItem Value="3" Text="Sunday"></asp:ListItem>
                                                                        <asp:ListItem Value="4" Text="Monday"></asp:ListItem>
                                                                        <asp:ListItem Value="5" Text="Tuesday"></asp:ListItem>
                                                                        <asp:ListItem Value="6" Text="Wednesday"></asp:ListItem>
                                                                        <asp:ListItem Value="7" Text="Thursday"></asp:ListItem>
                                                                        <asp:ListItem Value="8" Text="Fridayday"></asp:ListItem>
                                                                        <asp:ListItem Value="9" Text="Saturday"></asp:ListItem>--%>
                                                                        </asp:DropDownList>
                                                                        <%--<span class="mandatory">*</span> --%>
																</div>
															</div>
														</div>
                                                        
                                                        <div class="col-md-12"><hr></div>

														<div class="col-md-12">
                                                            <div class="row">
																<div class="form-group col-md-4">
																	<label for="exampleInputName">Wages Type</label>
																	    <asp:DropDownList ID="ddlWagesType" runat="server" class="form-control col-md-6">
                                                                        
                                                                        </asp:DropDownList><span class="mandatory">*</span> 
																</div>
                                                                <%--<div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Base Salary</label>
                                                                    <asp:TextBox ID="txtBaseSal" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                                    <span class="mandatory">*</span> 
                                                                </div>--%>
                                                                <div class="form-group col-md-4">
																	<label for="exampleInputName">PF Eligible</label>
																	    <asp:DropDownList ID="ddlPFEligible" runat="server" 
                                                                        class="form-control col-md-6" AutoPostBack="True" 
                                                                        onselectedindexchanged="ddlPFEligible_SelectedIndexChanged">
                                                                        <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="Yes"></asp:ListItem>
                                                                        <asp:ListItem Value="3" Text="No"></asp:ListItem>
                                                                        </asp:DropDownList><span class="mandatory">*</span> 
                                                                        <%--<span class="mandatory">*</span> --%>
                                                                </div>
                                                                
                                                                <div class="form-group col-md-4">
																	<label for="exampleInputName">ESI Eligible</label>
																	    <asp:DropDownList ID="ddlESIEligible" runat="server" 
                                                                        class="form-control col-md-6" AutoPostBack="True" 
                                                                        onselectedindexchanged="ddlESIEligible_SelectedIndexChanged">
                                                                        <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="Yes"></asp:ListItem>
                                                                        <asp:ListItem Value="3" Text="No"></asp:ListItem>
                                                                        </asp:DropDownList><span class="mandatory">*</span> 
                                                                       <%-- <span class="mandatory">*</span> --%>
                                                                </div>
															
                                                                
															</div>
														</div>

														<div class="col-md-12">
                                                            <div class="row">
																<div class="form-group col-md-4">
                                                                    <label for="exampleInputName">PF Number</label>
                                                                    <asp:TextBox ID="txtPFNum" class="form-control col-md-6" runat="server" onkeypress="return NumberOnly()" Text="0"></asp:TextBox>
                                                                  <%--  <span class="mandatory">*</span> --%>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">PF / Date of Join</label>
                                                                     <asp:TextBox ID="txtPFDOJ" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                                      <cc1:CalendarExtender ID="CalendarExtender4" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtPFDOJ">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtPFDOJ" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
                                                 
                                                   <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtPFDOJ" ID="RegularExpressionValidator3"
                                                  ValidationExpression = "^[\s\S]{10,}$" runat="server" ErrorMessage="Minimum 10 characters required."></asp:RegularExpressionValidator>

                                                 
                                                 
                                                 
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                 <label for="exampleInputName">Std Working Hours</label>
                                                                    <asp:TextBox ID="txtStdWHr" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                                
                                                                </div>
                                                                
                                                                
                                                                
                                                                
															</div>
														</div>

														<div class="col-md-12">
                                                            <div class="row">
                                                              <%-- <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">ESI Code</label>
                                                                    <asp:TextBox ID="txtESICode" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                                </div>--%>
                                                               <div class="form-group col-md-4">
                                                                    <label for="exampleInputName" >ESI Number</label>
                                                                    <asp:TextBox ID="txtESINum" class="form-control col-md-6" runat="server" onkeypress="return NumberOnly()" Text="0"></asp:TextBox>
                                                                    <%--<span class="mandatory">*</span> --%>
																</div>
																
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">ESI / Date of Join</label><%--<span class="mandatory">*</span> --%>
                                                                      <asp:TextBox ID="txtESIDOJ" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                                      
                                                     <cc1:CalendarExtender ID="CalendarExtender5" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtESIDOJ">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtESIDOJ" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
                                                 
                                                   <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtESIDOJ" ID="RegularExpressionValidator4"
                                                  ValidationExpression = "^[\s\S]{10,}$" runat="server" ErrorMessage="Minimum 10 characters required."></asp:RegularExpressionValidator>

                                                                      
                                                                </div>
                                                                
															</div>
														</div>

                                                        <div class="col-md-12"><hr></div>
                                                        
														<div class="col-md-12">
                                                            <div class="row">
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName" >Hostel Room No</label>
                                                                    <asp:TextBox ID="txtHstRoomNo" class="form-control col-md-6" runat="server" onkeypress="return NumberOnly()"></asp:TextBox>
																</div>
																
                                                                <div class="form-group col-md-4">
																	<label for="exampleInputName">Bus Route</label>
																	<asp:TextBox ID="txtBusRoute" class="form-control col-md-6" runat="server"  onkeyup="checkNum()"></asp:TextBox>
                                                                </div>
                                                                
																
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Bus Number</label>
                                                                    <asp:TextBox ID="txtBusNum" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                                </div>

                                                            </div>
														</div>

														

														<div class="col-md-12">
                                                            <div class="row">
                                                               <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Recuritment Through</label>
                                                                    <asp:TextBox ID="txtRecThrgh" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Recuritment Mobile</label>
                                                                    <asp:TextBox ID="txtRecMobile" class="form-control col-md-6" runat="server"  MaxLength="10" onblur="checkLength(this)"></asp:TextBox>
                                                                </div>
                                                              </div>
                                                         </div>
                                                               
                                                           
                                                          
                                                         <div class="col-md-12"><hr></div>
                                                        
                                                         <div class="col-md-12">
                                                         <div class="row">
                                                         
                                                            <label for="exampleInputName">Salary  Through</label>
                                                             <asp:RadioButtonList ID="rbtnSalayThtough" runat="server" RepeatDirection="Horizontal" onselectedindexchanged="rbtnSalayThtough_SelectedIndexChanged" AutoPostBack="true">
                                                                <asp:ListItem Text="Cash" Value="1" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="Bank" Value="2"></asp:ListItem>
                                                             </asp:RadioButtonList>
                                                         </div>
                                                            <div class="row">
                                                               <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Bank Code</label>
                                                                   
                                                                   <asp:DropDownList ID="ddlBankCode" runat="server" class="form-control col-md-6" Enabled="false" OnSelectedIndexChanged="ddlBankCode_SelectedIndexChanged" AutoPostBack="true" >
                                                                   </asp:DropDownList >
                                                                </div>
																<div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Bank Name</label>
                                                                    <asp:TextBox ID="txtbankName" class="form-control col-md-6" runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
                                                             
                                                               <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">IFSC Code</label>
                                                                    <asp:TextBox ID="txtIFSCCode" class="form-control col-md-6" runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
															
															</div>
														</div>
                                                      
                                                       <div class="col-md-12">
                                                            <div class="row">
																
                                                                
                                                                 <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Account No.</label>
                                                                    <asp:TextBox ID="txtAccNo" class="form-control col-md-6" runat="server"  Enabled="false"></asp:TextBox>
																</div>
															</div>
															
														</div>           
										          
										           </div>
										          
                                      </ContentTemplate>
                                  </asp:UpdatePanel> 
                                  
                                   <ul class="pager wizard">
                                      <li class="previous"><a href="#" class="btn btn-default">Previous</a></li>
                                      <li class="next"><a href="#" id="btn2" class="btn btn-default">Next</a></li>
                                   </ul>	
                                  
                            </div><!-- tab2-end -->	
                                      
                             <div class="tab-pane fade" id="tab3">
                                      
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                      
                                                    <div class="row">
                                                      
                                                       <div class="col-md-12">
                                                            <div class="row">
																<div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Qualification</label>
                                                                    
                                                                    <asp:DropDownList ID="ddlQulify"  class="form-control col-md-6" runat="server" >
                                                                    </asp:DropDownList>
                                                                    <%--<asp:TextBox ID="txtQualify" class="form-control col-md-6" runat="server"  onkeyup="checkNum()"></asp:TextBox>--%>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Nominee</label>
                                                                    <asp:TextBox ID="txtNominee" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName" >Father Name</label>
                                                                    <asp:TextBox ID="txtFatherName" class="form-control col-md-6" runat="server"  onkeyup="checkNum()"></asp:TextBox>
																</div>
															
															</div>
														</div>

														<div class="col-md-12">
                                                            <div class="row">
																<div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Mother Name</label>
                                                                    <asp:TextBox ID="txtMotherName" class="form-control col-md-6" runat="server"  onkeyup="checkNum()"></asp:TextBox>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Guardian name</label>
                                                                    <asp:TextBox ID="txtGuardName" class="form-control col-md-6" runat="server"  onkeyup="checkNum()"></asp:TextBox>
                                                                   <%-- <span class="mandatory">*</span> --%>
                                                                </div>
                                                           </div>
														</div>
                                                        
                                                        <div class="col-md-12"><hr></div>

														<div class="col-md-12">
                                                            <div class="row">
																<div class="form-group col-md-4">
																	<label for="exampleInputName">Permenant Address</label>
																	    <asp:TextBox ID="txtPermanentAddr" TextMode="MultiLine" Height="70" class="form-control col-md-6" runat="server"></asp:TextBox>
																	    <span class="mandatory">*</span> 
														         </div>
                                                                  <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Permanent (Taluk)</label>
                                                                    <asp:TextBox ID="txtpermanentTlk" class="form-control col-md-6" runat="server"  onkeyup="checkNum()"></asp:TextBox><span class="mandatory">*</span> 
                                                                  </div>
                                                                  <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Permanent (District)</label>
                                                                    <asp:TextBox ID="txtPermanentDst" class="form-control col-md-6" runat="server"  onkeyup="checkNum()"></asp:TextBox><span class="mandatory">*</span> 
                                                                  </div>
                                                               
															</div>
														</div>
														
														<div class="col-md-12">
                                                            <div class="row">
                                                                <div class="form-group col-md-4">
                                                                    <asp:CheckBox ID="chbsameAddress" runat="server" Text="Same as Permenant" 
                                                                        AutoPostBack="true" 
                                                                        oncheckedchanged="chbsameAddress_CheckedChanged"/>
                                                                
												                    <%--  <div class="checker" class="form-control col-md-6" id="Div1"><span><input id="Checkbox1" type="checkbox" value="checked" class="input-mini"></span></div>Same as Permenant--%>
                                                                </div>
                                          	                </div>
										                </div>
														
														<div class="col-md-12">
                                                            <div class="row">
                                                               <div class="form-group col-md-4">
																	<label for="exampleInputName">Temporary Address</label>
																	    <asp:TextBox ID="txtTempAddr" TextMode="MultiLine" Height="70" class="form-control col-md-6" runat="server" AutoPostBack="true"></asp:TextBox>
														        </div>
														          <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Temporary (Taluk)</label>
                                                                    <asp:TextBox ID="txtTempTlk" class="form-control col-md-6" runat="server"  onkeyup="checkNum()" AutoPostBack="true"></asp:TextBox>
                                                                  </div>
                                                                  <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Temporary (District)</label>
                                                                    <asp:TextBox ID="txtTempDst" class="form-control col-md-6" runat="server"  onkeyup="checkNum()" AutoPostBack="true"></asp:TextBox>
                                                                  </div>
                                                                
															</div>
														</div>
                                                        
                                                        <div class="col-md-12"><hr></div>
                                                        
														<div class="col-md-12">
                                                            <div class="row">
                                                                <div class="form-group col-md-4">
																	<label for="exampleInputName">Identification Mark1</label>
																	<asp:TextBox ID="txtIMark1" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                                </div>
																<div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Identification Mark2</label>
                                                                    <asp:TextBox ID="txtIMark2" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Employee Mobile Number</label>
                                                                    <asp:TextBox ID="txtEmpMobileNum" class="form-control col-md-6" runat="server" onkeypress="return NumberOnly(this)" MaxLength="10"  onblur="checkLength(this)"></asp:TextBox>
                                                               
                                                                    <span class="mandatory">*</span> 
                                                                </div>
                                                               
															</div>
														</div>

														<div class="col-md-12">
                                                            <div class="row">
                                                               <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Parents Mobile1</label>
                                                                    <asp:TextBox ID="txtParentMob1" class="form-control col-md-6" runat="server" onkeypress="return NumberOnly()"  MaxLength="10" onblur="checkLength(this)"></asp:TextBox>
                                                                   <%-- <span class="mandatory">*</span> --%>
																</div>
															    <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Parents Mobile2</label>
                                                                    <asp:TextBox ID="txtParentMob2" class="form-control col-md-6" runat="server" onkeypress="return NumberOnly()" MaxLength="10" onblur="checkLength(this)"></asp:TextBox>
                                                                </div>
                                                              
															</div>
															
														</div>
 
                                                           
									  </div>
								  	       </ContentTemplate>
                                       </asp:UpdatePanel> 
									  
									     <ul class="pager wizard">
                                              <li class="previous"><a href="#" class="btn btn-default">Previous</a></li>
                                              <li class="next"><a href="#" id="btn3" class="btn btn-default">Next</a></li>
                                        </ul>
									  
									  
									  
                            </div><!-- tab3-end -->
                            
                             <div class="tab-pane fade" id="tab4">
                            
                                 <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                   
                                                    <div class="row">
                                                      
                                                       <div class="col-md-12">
                                                            <div class="row">
																<div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Document Type</label>
                                                                        <asp:DropDownList ID="ddlDocType" runat="server" class="form-control col-md-6">
                                                                        <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="Adhar Card"></asp:ListItem>
                                                                        <asp:ListItem Value="3" Text="Voter Card"></asp:ListItem> 
                                                                        <asp:ListItem Value="4" Text="Ration Card"></asp:ListItem>
                                                                        <asp:ListItem Value="5" Text="Pan Card"></asp:ListItem> 
                                                                        <asp:ListItem Value="6" Text="Smart Card"></asp:ListItem> 
                                                                        <asp:ListItem Value="7" Text="Passport"></asp:ListItem> 
                                                                        <asp:ListItem Value="8" Text="Driving Licence"></asp:ListItem>
                                                                        </asp:DropDownList><span class="mandatory">*</span> 
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Document No</label>
                                                                    <asp:TextBox ID="txtDocNo" class="form-control col-md-6" runat="server" onkeypress="return NumberOnly()"></asp:TextBox>
                                                                    <span class="mandatory">*</span> 
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName" >Document Description</label>
                                                                    <asp:TextBox ID="txtDocDesc" TextMode="MultiLine" Height="70" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                                    <span class="mandatory">*</span> 
																</div>
                                                          </div>
														</div>

														<div class="col-md-12">
                                                            <div class="row">
                                                            	<div class="form-group col-md-4">
													    		  <asp:FileUpload ID="filUpload" runat="server" class="btn btn-default btn-rounded" name="filUpload" onchange="showimagepreview(this)"/>
														        </div>
                                                                <div class="form-group col-md-4">
                                                                   
                                                                    <asp:Image ID="img11" runat="server" height="100%" width="100%" 
                                                                        BorderStyle="None" visible="false"/>
                                                                    <img id="img1" alt="" height="100%" width="100%" />  
                                                               </div>
                                                                <div class="form-group col-md-4">
                                                                  <asp:Button ID="btndocumentsave" class="btn btn-success" runat="server" 
                                                                        Text="ADD" onclick="btndocumentsave_Click"/>
                                                                </div>
                                                             </div>
														</div>
														                                                       
                                                        <div class="col-md-12"><hr></div>
                                                        
                                                         
                                                        
                               <div class="col-md-12">
                               <div class="panel-body">                       
                                
                                <div class="table-responsive">
    
                              <asp:Repeater ID="Repeater2" runat="server" onitemcommand="Repeater2_ItemCommand">
            <HeaderTemplate>
                
                  <table id="tableCustomer" class="display table" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                                                 
                                                    <th>EmpNo</th>
                                                    <th>EmpName</th>
                                                    <th>DocNo</th>
                                                    <th>DocType</th> 
                                                    <th>PathFile</th>
                                                    <th>View Image</th>
                                                    <th>Delete</th>
                                                    
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    
                   <%-- <td>
                        <%# DataBinder.Eval(Container.DataItem, "SNo")%>
                    </td>--%>
                    
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "EmpNo")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "EmpName")%>
                    </td>
                   <td>
                        <%# DataBinder.Eval(Container.DataItem, "DocNo")%>
                    </td>
                     <td>
                        <%# DataBinder.Eval(Container.DataItem, "DocType")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "pathfile")%>
                    </td>
                    <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("SNo") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton>
                    </td>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("SNo") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
                                
                                
                                
                                
                                
                                
                                 </div>
                         </div>
                    	</div>
                         	
                                             </ContentTemplate>
                                              <Triggers>
                                                    <asp:PostBackTrigger ControlID="btndocumentsave"  />
                                                   
                                             </Triggers>
                                                       
                                 </asp:UpdatePanel> 
                                
                        
                         
                                                             
							
								
								
								 <ul class="pager wizard">
                                     <li class="previous"><a href="#" class="btn btn-default">Previous</a></li>
                                     <li class="next"><a href="#" id="btn4" class="btn btn-default">Next</a></li>
                                 </ul>
							
                            </div><!-- tab4-end -->
                            
                             <div class="tab-pane fade" id="tab5">
                            
                                 <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            
                                                    <div class="row">
                                                    
                                                    <div class="col-md-12">
                                                            <div class="row">
																<div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Period From</label>
                                                                    <asp:TextBox ID="txtPeriodFrom" class="form-control" runat="server"></asp:TextBox>
                                                                     <cc1:CalendarExtender ID="CalendarExtender12" runat="server" 
                                                                      CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                                                 TargetControlID="txtPeriodFrom">
                                                                     </cc1:CalendarExtender>
                                                                     <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" 
                                                                       FilterMode="ValidChars" 
                                                                       FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                                       TargetControlID="txtPeriodFrom" ValidChars="0123456789-/">
                                                                  </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Period To</label>
                                                                    <asp:TextBox ID="txtPeriodTo" class="form-control" runat="server" 
                                                                        ontextchanged="txtPeriodTo_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" 
                                                                      CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                                                 TargetControlID="txtPeriodTo">
                                                                     </cc1:CalendarExtender>
                                                                     <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" 
                                                                       FilterMode="ValidChars" 
                                                                       FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                                       TargetControlID="txtPeriodTo" ValidChars="0123456789-/">
                                                                  </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Total Experince</label>
                                                                    <asp:TextBox ID="txtExpTol" class="form-control" runat="server"></asp:TextBox>
																</div>
															
															</div>
														</div>
                                                      
                                                       <div class="col-md-12">
                                                            <div class="row">
																<div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Company Name</label>
                                                                    <asp:TextBox ID="txtExpComNam" class="form-control col-md-6" runat="server" onkeyup="checkNum()"></asp:TextBox>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Designation</label>
                                                                    <asp:TextBox ID="txtExpDesgination" class="form-control col-md-6" runat="server" onkeyup="checkNum()"></asp:TextBox>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Department</label>
                                                                    <asp:TextBox ID="txtExpDept" class="form-control col-md-6" runat="server" onkeyup="checkNum()"></asp:TextBox>
																</div>
															
															</div>
														</div>
														
														<div class="col-md-12">
                                                            <div class="row">
																<div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Refrence Name</label>
                                                                    <asp:TextBox ID="txtRefName" class="form-control col-md-6" runat="server"  onkeyup="checkNum()"></asp:TextBox>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Refrence Mobile</label>
                                                                    <asp:TextBox ID="txtRefMobile" class="form-control col-md-6" runat="server" onkeypress="return NumberOnly()"  MaxLength="10" onblur="checkLength(this)"></asp:TextBox>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Refrence Email</label>
                                                                    <asp:TextBox ID="txtRefMail" class="form-control col-md-6" runat="server"></asp:TextBox>
																</div>
															
															</div>
														</div>

														<div class="col-md-12">
                                                            <div class="row">
                                                                <div class="form-group col-md-5"></div>
																
                                                                <div class="form-group col-md-4">
                                                                <br>
                                                                 <asp:Button ID="Button2" class="btn btn-success" runat="server" Text="Add" 
                                                                        onclick="Button2_Click"/>
                                                                </div>
                                                           </div>
														</div>
														                                                       
                                                        <div class="col-md-12"><hr></div>
                               <div class="col-md-12">
                               <div class="panel-body">                       
                                <div class="table-responsive">
                                
                                <asp:Repeater ID="rptrCustomer" runat="server" onitemcommand="Repeater1_ItemCommand">
                                  
                                 <HeaderTemplate>
                                        <table id="example" class="display table" style="width: 100%; ">
                                        <thead>
                                            <tr>
                                                <th>EmpNo</th>
                                                <th>PeriodFrom</th>
                                                <th>PeriodTo</th>
                                                <th>TotalExp</th>
                                                <th>CompName</th>
                                             </tr>
                                        </thead>
                                    
                                 </HeaderTemplate>
                                <ItemTemplate>
                                <tbody>
                                                <tr>
                                              <td>
                                                 <%# DataBinder.Eval(Container.DataItem, "EmpNo")%>
                                              </td>
                                              <td>
                                                 <%# DataBinder.Eval(Container.DataItem, "PeriodFrom")%>
                                              </td>
                                              <td>
                                                 <%# DataBinder.Eval(Container.DataItem, "PeriodTo")%>
                                              </td>
                                              <td>
                                                 <%# DataBinder.Eval(Container.DataItem, "YearsExp")%>
                                              </td>
                                              <td>
                                                 <%# DataBinder.Eval(Container.DataItem, "CompanyName")%>
                                              </td>
                                              <%--<td>                                         
                                              <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("SNo") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                                              </td>--%>
                                              <td>
                                              <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("SNo") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                                              </td>
                                            
                                              </tr>
                                                                                                                                              
                                            </tbody>
                                         </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>                                       
                                           
            </asp:Repeater>
                                    </div>
                                    
                                    
                         </div>
                         </div>
                         
								</div>
								
								</ContentTemplate>
                                       </asp:UpdatePanel> 
                              
                                        <ul class="pager wizard">
                                           <li class="previous"><a href="#" class="btn btn-default">Previous</a></li>
                                           <li class="next"><a href="#" id="btn5" class="btn btn-default">Next</a></li>
                                        </ul>    
                                       
                                       
                            </div><!-- tab5 end -->
                            
                            
							 <div class="tab-pane fade" id="tab6">
							 
							 <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                            <ContentTemplate>
							 
                                                    <div class="row">
                                                    
                                                    <div class="col-md-12">
                                                            <div class="row">
																<div class="form-group col-md-6 m-t-lg">
																	<label class="col-sm-4">Adolescent <span class="mandatory">*</span></label>
                                                                   
                                                                    
                                                                    <asp:RadioButton ID="rdbYes" runat="server" Text ="Yes" 
                                                                        oncheckedchanged="rdbYes_CheckedChanged" AutoPostBack="true"/>
																	<asp:RadioButton ID="rdbNo" runat="server" Text ="No" 
                                                                        oncheckedchanged="rdbNo_CheckedChanged" AutoPostBack="true"/> 
                                                                   
                                                                </div>
                                                                
															</div>
														</div>
														<br>
														
														<div class="col-md-12">
                                                            <div class="row">
																<div class="form-group col-md-4">
																  <label for="exampleInputName">Type of Certificate<span class="mandatory">*</span></label>
																 
																  <%-- <asp:Label runat="server" Text="Type of Certificate" for="exampleInputName" id="TypeofCertificate"></asp:Label>--%>
                                                                   <asp:TextBox ID="txtAdlCerftType" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                                
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                 <label for="exampleInputName">Certificate No<span class="mandatory">*</span></label>
                                                                <%--  <asp:Label runat="server" Text="Certificate No" for="exampleInputName" id="Certificateno"></asp:Label>--%>
                                                                  <asp:TextBox ID="txtAdlCerftNo" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                 <label for="exampleInputName">Issue Date<span class="mandatory">*</span></label>
                                                              <%--   <asp:Label runat="server" Text="Issue Date" for="exampleInputName" id="IssueDate"></asp:Label>--%>
                                                                 <asp:TextBox ID="txtAdlIssuseDate" class="form-control col-md-6"  runat="server"></asp:TextBox>
                                                                  <cc1:CalendarExtender ID="CalendarExtender6" runat="server" 
                                                                      CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                                                 TargetControlID="txtAdlIssuseDate">
                                                                     </cc1:CalendarExtender>
                                                                     <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" 
                                                                       FilterMode="ValidChars" 
                                                                       FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                                       TargetControlID="txtAdlIssuseDate" ValidChars="0123456789-/">
                                                                  </cc1:FilteredTextBoxExtender>
                                                                 
                                                                </div>
                                                                
															</div>
														</div>
														
														<div class="col-md-12">
                                                            <div class="row">
                                                            
																<div class="form-group col-md-4">
																    <label for="exampleInputName">Next Due Date<span class="mandatory">*</span></label>
                                                                   <%-- <asp:Label runat="server" Text="Next Due Date" for="exampleInputName" id = "NextDate"></asp:Label>--%>
													                <asp:TextBox ID="txtAdlNextDueDate" class="form-control date-picker" runat="server"></asp:TextBox>
													                 <cc1:CalendarExtender ID="CalendarExtender7" runat="server" 
                                                                      CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                                                 TargetControlID="txtAdlNextDueDate">
                                                                     </cc1:CalendarExtender>
                                                                     <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" 
                                                                       FilterMode="ValidChars" 
                                                                       FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                                       TargetControlID="txtAdlNextDueDate" ValidChars="0123456789-/">
                                                                  </cc1:FilteredTextBoxExtender>
													                
                                                                 </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputName">Employee Mobile<span class="mandatory">*</span></label>
                                                                     <%-- <asp:Label runat="server" Text="Employee Mobile" for="exampleInputName" id="EmployeeMobile"></asp:Label>--%>
                                                                      <asp:TextBox ID="txtEmpMobile" class="form-control col-md-6" runat="server"  MaxLength="10" onblur="checkLength(this)"></asp:TextBox>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                   <label for="exampleInputName">Remarks<span class="mandatory">*</span></label>
                                                                    <%--<asp:Label runat="server" Text="Remarks" for="exampleInputName" id="Remarks"></asp:Label>--%>
                                                                    <asp:TextBox ID="txtRemarks" TextMode="MultiLine" Height="70" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                                </div>
                                                                
															</div>
														</div>
                                                      
                                                       <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="form-group col-md-5"></div>
																<div class="form-group col-md-4">
                                                                <br>
                                                                <asp:Button ID="btnAdolescent" class="btn btn-success" runat="server" Text="Add" onclick="btnAdolescent_Click" 
                                                                         />
                                                               </div>
														</div>     
														</div>
														

														
														                                                       
                                                       <%-- <div class="col-md-12"><hr></div>--%>
                               
                         
                                                            
								</div>
								</ContentTemplate>
                                       </asp:UpdatePanel> 
								
								  <ul class="pager wizard">
                                     <li class="previous"><a href="#" class="btn btn-default">Previous</a></li>
                                     <li class="next"><a href="#" id="btn6" class="btn btn-default">Next</a></li>
                                  </ul>
								
								
                            </div><!-- tab6 -->
                            
                             <div class="tab-pane fade" id="tab7">
           <asp:UpdatePanel ID="UpdatePanel6" runat="server">
           <ContentTemplate>
    
    
            <div class="row">
				        <div class="col-sm-6">
				          <div class="row">
						    <div class="form-group col-sm-12">
						        <h3>Earnings</h3>
                                <hr>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-10">
                                    <label for="exampleInputName">Basic<span class="mandatory">*</span> </label>
                                    <asp:TextBox ID="txtBasic" Text="0.0" class="form-control" runat="server"></asp:TextBox>
                                   
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-10">
                                    <label for="exampleInputName">Allowance1</label>
                                    <asp:TextBox ID="txtAllowance1" class="form-control" Text="0.0" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-10">
                                    <label for="exampleInputName">Allowance2</label>
                                    <asp:TextBox ID="txtAllowance2" Text="0.0" class="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            
                          </div> <!-- row end -->
						</div><!-- col-6 end -->
						   
						  
					<div class="col-sm-6">
						<div class="row">
						    <div class="form-group col-sm-12">
						        <h3>Deductions</h3>
                                <hr>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-10">
                                    <label for="exampleInputName">Deduction1</label>
                                    <asp:TextBox ID="txtDeduction1" Text="0.0" class="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-10">
                                    <label for="exampleInputName">Deduction2</label>
                                    <asp:TextBox ID="txtDeduction2" Text="0.0" class="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-10">
                                    <label for="exampleInputName">Salary<span class="mandatory">*</span></label>
                                    <asp:TextBox ID="txtPFSal" Text="0.0" class="form-control" runat="server"></asp:TextBox> 
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                                                        
						</div> <!-- row end -->
						  
						         
				    </div><!-- col-6 end -->
				            
			 
        <%--<div class="col-md-12">
            <div class="row">
			   <div class="form-group col-md-5"></div>
               <div class="form-group col-md-4">
                <br>
                <asp:Button ID="Button5" class="btn btn-success" runat="server" Text="Add" />
                </div>
                <div class="form-group col-md-3"></div>
            </div>
		</div>--%>
														
				<div class="col-md-12"><hr></div>
                     
                
     </div>  
     
     
          </ContentTemplate>
        </asp:UpdatePanel> 
     
     <ul class="pager wizard">
         <li class="previous"><a href="#" class="btn btn-default">Previous</a></li>
       <%--<li class="next"><a href="#" class="btn btn-default">Next</a></li>--%>
       
    </ul>
     
      <div class="col-md-12">
            <div class="row">
			   <div class="form-group col-md-5"></div>
               <div class="form-group col-md-4">
                <br>
              <asp:Button ID="btn_save" class="btn btn-info" runat="server" Text="Save" onclick="btn_save_Click" />    
                </div>
                <div class="form-group col-md-3"></div>
            </div>
		</div>
     
                              
                                      
	</div><!-- tab7 -->
                   <%--</div><!-- row-m-b-lg-end -->
                            </div><!-- table-pane-end -->--%>
                  </div><!-- table-content-end -->
         </form><!-- form-end -->
						
						
            
        </div> <!-- rootwizard  end -->
        </div><!-- panel-body end -->
        </div><!-- panel panel-white end -->
        </div><!-- col-lg-9 end-->
<!-- Employee Photo start-->
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-white" style="height: 100%;">
                <div class="panel panel-primary">
                    <div class="panel-heading clearfix">
                        <h3 class="panel-title">Employee Photo</h3>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="profile-image-container">
                        <%--<img src="assets/images/avatar4.png" alt="">--%>

                        <div class="media" style="margin-top: -19px;">
                            <a class="media-center" href="javascript:;" style="float: initial ;">
                                <asp:Image ID="Image3" runat="server" class="media-object" Style="width: 158px; height: 161px;" Visible="false" ImageUrl="~/assets/images/avatar4.png" />
                                <img id="img1" alt="" height="100%" width="100%"  />
                            </a>
                            <asp:FileUpload ID="FileUpload1" runat="server" onchange="showimagepreview(this)" />
                            <%--<asp:Button ID="btnUpload" Text="Upload" runat="server" OnClick="Upload" Style="display: none" />--%>
                        </div>

                    </div>
                    <h3 class="text-center">
                        <asp:Label ID="lblEmpName" runat="server" Text="Employee Name"></asp:Label></h3>
                    <p class="text-center">
                        <asp:Label ID="lblTkNo" runat="server" Text="Ticket Number"></asp:Label></p>

                    <hr>
                    <%--<button class="btn btn-success btn-block"><i class="fa fa-plus m-r-xs"></i>Upload Photo</button>--%>
                </div>
            </div>
        </div><!-- Employee Photo end-->       
 	    

</div><!-- col-md-12 end -->
</div><!-- row end -->
</div><!-- main-wrapper end -->
</asp:Content>