﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class AbsentReport : System.Web.UI.Page
{
    string SSQL;
    DataTable dsEmployee = new DataTable();
    string Shift;
    string Date;
    string Date2;
    string SessionCcode;
    string SessionLcode;
    DataTable mDataTable = new DataTable();
    DataTable AutoDTable = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable dsEmployeee = new DataTable();
    DataTable DataCell = new DataTable();
    string Machine_Encry;
    Boolean isPresent = false;
    string Machine_ID_Str = "";
    string OT_Week_OFF_Machine_No = "";
    DateTime Date_Value_Str;
    DataTable mLocalDS = new DataTable();
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    string Total_Time_get = "";
    Int32 j = 0;
    double time_Check_dbl = 0;
    string ModeType;
    string ShiftType1;
    string ddlShiftType;
    DateTime date1;
    DateTime date2;
    

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionUserType;


    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Absent Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }


            //ModeType = Request.QueryString["ModeType"].ToString();
            //ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            //ddlShiftType = Request.QueryString["ddlShiftType"].ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();



            //ModeType = Request.QueryString["ModeType"].ToString();
            //ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            //Date = Request.QueryString["Date"].ToString();
            //ddlShiftType = Request.QueryString["Shift"].ToString();
            //Date2 = Request.QueryString["Date2"].ToString();
            //SessionCcode = Request.QueryString["SessionCcode"].ToString();
            //SessionLcode = Request.QueryString["SessionLcode"].ToString();

            Fill_AbsentDays();

            WriteAbsent();


            grid.DataSource = DataCell;
            grid.DataBind();
            string attachment = "attachment;filename=ABSENT REPORT DAYWISE.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write("<table>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">ABSENT REPORT DAYWISE &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\"> Date -" + Date + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }



    }

    public void Fill_AbsentDays()
    {
      
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("EmpNo");
        AutoDTable.Columns.Add("ExistingCode");
        AutoDTable.Columns.Add("DeptName");
        AutoDTable.Columns.Add("FirstName");
        AutoDTable.Columns.Add("MachineID_Encrypt");

        try
        {
            //SSQL = "";
            //SSQL = "select isnull(DeptName,'') as [DeptName], Cast(MachineID As int) As MachineID";
            //SSQL = SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
            //SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
            //SSQL = SSQL + ",isnull(MachineID_Encrypt,'') as [MachineID_Encrypt]";
            //SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode + "'";
            //SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'";
            //SSQL = SSQL + " Order By DeptName, MachineID";


            SSQL = "";
            SSQL = "Select Distinct EM.DeptName,EM.MachineID,EM.EmpNo,EM.ExistingCode,EM.MachineID_Encrypt,";
            SSQL = SSQL + " (EM.FirstName + '.'+ EM.MiddleInitial) as [FirstName] from Employee_Mst EM,LogTime_IN MA where";
            SSQL = SSQL + " EM.Compcode=MA.CompCode And EM.LocCode=MA.LocCode And EM.MachineID_Encrypt=MA.MachineID";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";
            SSQL = SSQL + " And MA.CompCode='" + SessionCcode + "' And MA.LocCode='" + SessionLcode + "'";
            //if (SessionUserType == "2")
            //{
            //    SSQL = SSQL + " And EM.IsNonAdmin='1'";
            //}
            SSQL = SSQL + " Order By EM.DeptName, EM.MachineID";


            dsEmployee = objdata.ReturnMultipleValue(SSQL);
            if (dsEmployee.Rows.Count <= 0)
            {
            }

            else
            {
                int i1 = 0;
                for (int j1 = 0; j1 < dsEmployee.Rows.Count; j1++)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[i1][0] = dsEmployee.Rows[j1]["MachineID"].ToString();
                    AutoDTable.Rows[i1][1] = dsEmployee.Rows[j1]["EmpNo"].ToString();
                    AutoDTable.Rows[i1][2] = dsEmployee.Rows[j1]["ExistingCode"].ToString();
                    AutoDTable.Rows[i1][3] = dsEmployee.Rows[j1]["DeptName"].ToString();
                    AutoDTable.Rows[i1][4] = dsEmployee.Rows[j1]["FirstName"].ToString();
                    AutoDTable.Rows[i1][5] = dsEmployee.Rows[j1]["MachineID_Encrypt"].ToString();
                    i1++;
                }
            }

        }
        catch (Exception e)
        {

        }
    }


    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }



    public void WriteAbsent()
    {
        Date_Value_Str = Convert.ToDateTime(Date.ToString());



       
        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("EmpNo");
        DataCell.Columns.Add("ExistingCode");
        DataCell.Columns.Add("DeptName");
        DataCell.Columns.Add("FirstName");

        try
        {
            DataCell.Columns.Add(Date);
            int i1 = 0;

            for (int j1 = 0; j1 < AutoDTable.Rows.Count; j1++)
            {
                DataCell.NewRow();
                DataCell.Rows.Add();
                DataCell.Rows[i1]["DeptName"] = AutoDTable.Rows[j1]["DeptName"].ToString();
                DataCell.Rows[i1]["MachineID"] = AutoDTable.Rows[j1]["MachineID"].ToString();
                DataCell.Rows[i1]["EmpNo"] = AutoDTable.Rows[j1]["EmpNo"].ToString();
                DataCell.Rows[i1]["ExistingCode"] = AutoDTable.Rows[j1]["ExistingCode"].ToString();
                DataCell.Rows[i1]["FirstName"] = AutoDTable.Rows[j1]["FirstName"].ToString();
                i1++;
            }

            for (int intCol = 0; intCol < DataCell.Rows.Count; intCol++)
            {

                isPresent = false;
                Machine_Encry = AutoDTable.Rows[intCol]["MachineID_Encrypt"].ToString();
                OT_Week_OFF_Machine_No = AutoDTable.Rows[intCol]["MachineID"].ToString();

                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_Encry + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And TimeIN >='" + Date_Value_Str.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeIN <='" + Date_Value_Str.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeIN ASC";

                mLocalDS = objdata.ReturnMultipleValue(SSQL);

                if (mLocalDS.Rows.Count <= 0)
                {
                    Time_IN_Str = "";
                }
                else
                {
                    Time_IN_Str = mLocalDS.Rows[0]["TimeIN"].ToString();
                }

                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_Encry + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + Date_Value_Str.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeOUT Desc";

                mLocalDS = objdata.ReturnMultipleValue(SSQL);

                if (mLocalDS.Rows.Count <= 0)
                {
                    Time_Out_Str = "";
                }
                else
                {
                    Time_Out_Str = mLocalDS.Rows[0]["TimeOUT"].ToString();
                }

                if (Time_IN_Str == "" || Time_Out_Str == "")
                {
                    time_Check_dbl = 0;
                }
                else
                {
                    date1 = Convert.ToDateTime(Time_IN_Str.ToString());
                 

                    date2 = Convert.ToDateTime(Time_Out_Str.ToString());

                    TimeSpan ts = new TimeSpan();
                    ts = date2.Subtract(date1);

                    Total_Time_get = Convert.ToString(ts.Hours);

                    if (Left_Val(Total_Time_get, 1) == "-")
                    {
                        date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                        ts = date2.Subtract(date1);
                        ts = date2.Subtract(date1);
                        Total_Time_get = Convert.ToString(ts.Hours);

                        if (Total_Time_get == "0")
                        {
                            time_Check_dbl = Convert.ToDouble(Total_Time_get);
                        }
                        else
                        {
                           string ss = Total_Time_get + "." + ts.Minutes;
                           time_Check_dbl = Convert.ToDouble(ss.ToString());
                        }
                    }
                    else
                    {
                        if (Total_Time_get == "0")
                        {
                            time_Check_dbl = Convert.ToDouble(Total_Time_get);
                        }
                        else
                        {
                            string ss = Total_Time_get + "." + ts.Minutes;
                            time_Check_dbl = Convert.ToDouble(ss.ToString());
                        }

                    }
                }

                double Calculate_Attd_Work_Time = 0;
                SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and Calculate_Work_Hours <> '' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
                mLocalDS = objdata.ReturnMultipleValue(SSQL);
                if (mLocalDS.Rows.Count <= 0)
                {
                    Calculate_Attd_Work_Time = 7.3;
                }
                else
                {
                    string worktime = mLocalDS.Rows[0]["Calculate_Work_Hours"].ToString();

                    Calculate_Attd_Work_Time = Convert.ToDouble(worktime);

                    if (Calculate_Attd_Work_Time == 0)
                    {
                        Calculate_Attd_Work_Time = 7.3;
                    }
                    else
                    {

                    }
                }

                if (time_Check_dbl >= Calculate_Attd_Work_Time)
                {
                    isPresent = true;
                }



                string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value_Str.AddDays(0).ToString("yyyy/MM/dd") + "'";
                mLocalDS = objdata.ReturnMultipleValue(qry_nfh);
                if (mLocalDS.Rows.Count > 0)
                {
                    DataCell.Rows[intCol][Date] = "NH";
                }
                else
                {
                    string Employee_Week_Name = "";
                    string Assign_Week_Name = "";
                    mLocalDS = null;
                    Employee_Week_Name = Date_Value_Str.DayOfWeek.ToString();
                    SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "'";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Assign_Week_Name = "";
                    }
                    else
                    {
                        Assign_Week_Name = mLocalDS.Rows[0]["WeekOff"].ToString();
                    }
                    if (Employee_Week_Name.ToString() == Assign_Week_Name.ToString())
                    {
                        //DataCell.Rows[intCol].Delete();
                    }
                    else
                    {
                        if (isPresent == true)
                        {
                            //DataCell.Rows[intCol].Delete();
                        }
                        else
                        {
                            DataCell.Rows[intCol][Date] = "A";
                        }
                    }

                }
            }


        }

        catch (Exception ex)
        {
        }
    }
}
    
    