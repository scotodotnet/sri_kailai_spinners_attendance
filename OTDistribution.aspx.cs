﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Web.UI;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;


public partial class OTDistribution : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    string SSQL = "";
   

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Employee Details";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                li.Attributes.Add("class", "droplink active open");

                IPAddress();
                Financial_Year();
                Drop_EmpCode();

            }
        }
    }

    public void IPAddress()
    {
        ddlIPAddr.Items.Clear();
        ddlIPAddr.Items.Add("-select-");
       
        DataTable dtc = new DataTable();
        dtc = objdata.IPAddressALL(SessionCcode, SessionLcode);

        for (int i = 0; i <= dtc.Rows.Count - 1; i++)
        {

            ddlIPAddr.Items.Add(dtc.Rows[i]["IPAddress"].ToString());
        }
    }



    public void Financial_Year()
    {
        String CurrentYear1;
        int CurrentYear;
        CurrentYear1 = DateTime.Now.Year.ToString();
        CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());
        if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
        {
            CurrentYear = CurrentYear - 1;
        }
        ddlFinancialYear.Items.Add("-select-");
        for (int i = 0; i < 11; i++)
        {
            string tt = (CurrentYear1 + "-" + (CurrentYear - 1));
            ddlFinancialYear.Items.Add(tt.ToString());
            CurrentYear = CurrentYear - 1;
            string cy = Convert.ToString(CurrentYear);
            CurrentYear1 = cy;
        }
    }
    public void clearfunction()
    {
        ddlIPAddr.SelectedIndex = 0;
        ddlMonth.SelectedIndex = 0;
        ddlFinancialYear.SelectedIndex = 0;
        TxtDept.Text = "";
        TxtDesgn.Text = "";
        ddlEmpNo.SelectedIndex = 0;
        TxtMachineNo.Text = "";
        TxtFrmDate.Text = "";
        TxtToDate.Text = "";
        TxtEmpName.Text = "";
        TxtWgsType.Text = "";
                
    }


    protected void btnClear_Click(object sender, EventArgs e)
    {
        clearfunction();
    }


    public void Drop_EmpCode()
    {
        DataTable dt = new DataTable();

        SSQL = "";
        SSQL = "Select EmpNo As EmpNo From Employee_Mst";
        SSQL = SSQL + " Where CompCode='" + SessionCcode + "'";
        SSQL = SSQL + " and LocCode='" + SessionLcode + "' And IsActive='Yes'";
        //If UCase(mvarUserType) = UCase("IF User") Then
        //    SSQL = SSQL & " And PFNo <> '' And PFNo IS Not Null And PFNo <> '0' And PFNo <> 'o' And PFNo <> 'A'"
        //    SSQL = SSQL & " And PFNo <> 'NULL'"
        //End If

        //If mUserLocation <> "" Then
        //SSQL = SSQL + " and IsNonAdmin='0'";
        // End If

        SSQL = SSQL + " Order By EmpNo Asc";

        dt = objdata.ReturnMultipleValue(SSQL);
        if (dt.Rows.Count > 0)
        {
            ddlEmpNo.Items.Clear();
            ddlEmpNo.Items.Add("-select-");
          
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlEmpNo.Items.Add(dt.Rows[i]["EmpNo"].ToString());
            }
        }
    }


    protected void ddlEmpNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string s = ddlEmpNo.SelectedItem.Text;
        DataTable dted = new DataTable();
        SSQL="Select FirstName,MachineID,DeptName,Wages,Designation from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and EmpNo='" + ddlEmpNo.SelectedItem.Text + "'";

        dted = objdata.ReturnMultipleValue(SSQL);
        if (dted.Rows.Count > 0)
        {
            TxtEmpName.Text = dted.Rows[0]["FirstName"].ToString();
            TxtMachineNo.Text = dted.Rows[0]["MachineID"].ToString();
            TxtDept.Text = dted.Rows[0]["DeptName"].ToString();
            TxtWgsType.Text = dted.Rows[0]["Wages"].ToString();
            TxtDesgn.Text = dted.Rows[0]["Designation"].ToString();
        }
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {
            if (fileUpload.HasFile)
            {
            
            string fileName = Server.MapPath(fileUpload.PostedFile.FileName);
            string fileExtension = System.IO.Path.GetExtension(this.fileUpload.PostedFile.FileName);
            string fname = fileUpload.FileName;
            string fileLocation = Server.MapPath("Upload/" + fname);
            fileUpload.SaveAs(Server.MapPath("Upload/" + fileUpload.FileName));

            //Check whether file extension is xls or xslx
            string connectionString = "";
            if (fileExtension == ".xls")
            {
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\""; 
            }
            else if (fileExtension == ".xlsx")
            {
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            }

            //Create OleDB Connection and OleDb Command

            OleDbConnection con = new OleDbConnection(connectionString);
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Connection = con;
            OleDbDataAdapter dAdapter = new OleDbDataAdapter(cmd);
            DataTable DSet = new DataTable();
            con.Open();
            DataTable dtExcelSheetName = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            string getExcelSheetName = dtExcelSheetName.Rows[0]["Table_Name"].ToString();
            cmd.CommandText = "SELECT * FROM [" + getExcelSheetName +"]";
            dAdapter.SelectCommand = cmd;
            dAdapter.Fill(DSet);
            con.Close();
                    //OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                    //objDataAdapter.SelectCommand = command;
                    //DataSet ds = new DataSet();
                    DataTable mdSet=new DataTable();
                    DataTable dsEmployee=new DataTable();
                    DataTable mSaveStatus = new DataTable();

                    //objDataAdapter.Fill(ds);
                    //DataTable DSet = new DataTable();
                    //DSet = ds.Tables[0];
                    //for (int i = 0; i < DSet.Columns.Count; i++)
                    //{
                    //    DSet.Columns[i].ColumnName = DSet.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                    //}
                    //string constr = ConfigurationManager.AppSettings["ConnectionString"];

                    if (DSet.Rows.Count > 0)
                    {

                        for (int intRow = 0; intRow < DSet.Rows.Count; intRow++)
                        {

                          

                            string MachineID = DSet.Rows[intRow]["MachineID"].ToString();
                            string HrSal_str = DSet.Rows[intRow][5].ToString();
                            
                            string NoHrs_str = String.Format("{0:HH\\:mm}", DSet.Rows[intRow][6]);
                           
                            string NetAmt_str = DSet.Rows[intRow][7].ToString();

                            string EmpNo;
                            string EmpName;
                            string DeptName;
                            string[] year;
                            year=ddlFinancialYear.SelectedItem.Text.Split('-');

                            SSQL = "";
                            SSQL = SSQL + "select EmpNo,EmpName,Department from [S_EPay]..EmployeeDetails where BiometricID='" + MachineID + "'";
                            mdSet = objdata.ReturnMultipleValue(SSQL);

                             if (mdSet.Rows.Count == 0)
                             {
                             }
                            else
                             {

                                EmpNo = mdSet.Rows[0][0].ToString();
                                EmpName = mdSet.Rows[0][1].ToString();
                                DeptName = mdSet.Rows[0][2].ToString();


                                SSQL = "";
                                SSQL = SSQL + "select * from [S_EPay]..Overtime OT inner join [S_EPay]..EmployeeDetails ED on OT.EmpNo=ED.EmpNo where ED.BiometricID='" + MachineID + "' and OT.Month='" + ddlMonth.SelectedItem.Text + "' and OT.Financialyr='" + year[0] + "' and  FromDate=convert(datetime,'" + TxtFrmDate.Text + "',103) and Todate=convert(datetime,'" + TxtToDate.Text + "',103)";
                                dsEmployee = objdata.ReturnMultipleValue(SSQL);



                                   if(dsEmployee.Rows.Count == 0)
                                   {
                                    SSQL = "";
                                    SSQL = SSQL + " insert into [S_EPay]..Overtime (EmpNo,EmpName,Department,HrSalary,NoHrs,Month,ChkManual,NetAmount,Financialyr,Ccode,Lcode,TransDate,FromDate,Todate) ";
                                    SSQL = SSQL + "values('" + EmpNo + "','" + EmpName + "','" + DeptName + "','" + HrSal_str + "', ";
                                    SSQL = SSQL + "'" + NoHrs_str + "','" + ddlMonth.SelectedItem.Text + "','" + 0 + "','" + NetAmt_str + "', ";
                                    SSQL = SSQL + "'" + year[0] + "','" + SessionCcode + "','" + SessionLcode + "',convert(datetime,'" + TxtFrmDate.Text + "',103),convert(datetime,'" + TxtFrmDate.Text + "',103),convert(datetime,'" + TxtToDate.Text + "',103))";

                                    mSaveStatus = objdata.ReturnMultipleValue(SSQL);
                                   }

                                    else
                                   {
                                    SSQL = "";
                                    SSQL = SSQL + " update [S_EPay]..Overtime  set HrSalary='" + HrSal_str + "',NoHrs='" + NoHrs_str + "',NetAmount='" + NetAmt_str + "' ";
                                    SSQL = SSQL + " where EmpNo='" + EmpNo + "' and EmpName='" + EmpName + "' and Month='" + ddlMonth.SelectedItem.Text + "' and Financialyr='" + year[0] + "'";
                                    SSQL = SSQL + " and  FromDate=convert(datetime,'" + TxtFrmDate.Text + "',103) and Todate=convert(datetime,'" + TxtToDate + "',103) and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";


                                    mSaveStatus = objdata.ReturnMultipleValue(SSQL);
                                   }

                              }
                         }

                    }
                }
            }
        
        catch (Exception ex)
        {

        }
    }
}
